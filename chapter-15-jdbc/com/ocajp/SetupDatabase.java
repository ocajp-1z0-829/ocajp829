package com.ocajp;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.Types;
import java.util.Arrays;


public class SetupDatabase {

    public static void main(String[] args) throws Exception {
        String url = "jdbc:hsqldb:file:zoo";

        try (Connection conn = DriverManager.getConnection(url);
             //Connection c1 = new Co
             Statement stmt = conn.createStatement()) {

            dropExisting(conn);
            createTables(conn);
            createStoredProcedures(conn);

            conn.rollback();

            printCount(conn, "SELECT count(*) FROM names");

            callSPReadENames(conn);
            callSPReadENamesByLetter(conn);
            callMagicNumberWithOutputParam(conn);
            callDoubleNumberWithInOutParam(conn);
        }
    }

    /**
     * 16
     *
     * @param conn
     * @throws SQLException
     */
    private static void callDoubleNumberWithInOutParam(Connection conn) throws SQLException {
        var sql = "{call double_number(?)}";
        try (var cs = conn.prepareCall(sql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY)) {
            cs.setInt(1, 8);
            cs.registerOutParameter(1, Types.INTEGER);
            cs.execute();
            System.out.println(cs.getInt("num"));
        }
    }

    /**
     * 42
     *
     * @param conn
     * @throws SQLException
     */
    private static void callMagicNumberWithOutputParam(Connection conn) throws SQLException {
        var sql = "{?= call magic_number(?) }";
        try (var cs = conn.prepareCall(sql)) {
            cs.registerOutParameter(1, Types.INTEGER);
            cs.execute();
            System.out.println(cs.getInt("num"));
        }
    }

    /**
     * Zelda
     * Zoe
     *
     * @param conn
     * @throws SQLException
     */
    private static void callSPReadENamesByLetter(Connection conn) throws SQLException {
        var sql = "{call read_names_by_letter(?)}";
        try (var cs = conn.prepareCall(sql)) {
            cs.setString("prefix", "Z"); //OR cs.setString(1, "Z");
            try (var rs = cs.executeQuery()) {
                while (rs.next()) {
                    System.out.println(rs.getString(3));
                }
            }
        }
    }

    /**
     * Elsa
     * Ester
     * Eddie
     *
     * @param conn
     * @throws SQLException
     */
    private static void callSPReadENames(Connection conn) throws SQLException {
        String sql = "{call read_e_names()}";
        try (CallableStatement cs = conn.prepareCall(sql); ResultSet rs = cs.executeQuery()) {
            while (rs.next()) {
                System.out.println(rs.getString(3));
            }
        }
    }

    private static void dropExisting(Connection conn) throws SQLException {
        run(conn, "DROP PROCEDURE read_e_names IF EXISTS");
        run(conn, "DROP PROCEDURE read_names_by_letter IF EXISTS");
        run(conn, "DROP PROCEDURE magic_number IF EXISTS");
        run(conn, "DROP PROCEDURE double_number IF EXISTS");
        run(conn, "DROP TABLE names IF EXISTS");
        run(conn, "DROP TABLE exhibits IF EXISTS");
    }

    private static void createTables(Connection conn) throws SQLException {
        run(conn, """
                CREATE TABLE exhibits (
                  id INTEGER PRIMARY KEY,
                  name VARCHAR(255),
                  num_acres DECIMAL(4,1))""");

        run(conn, """
                CREATE TABLE names (
                   id INTEGER PRIMARY KEY,
                   species_id integer REFERENCES exhibits (id),
                   name VARCHAR(255))""");

        run(conn, "INSERT INTO exhibits VALUES (1, 'African Elephant', 7.5)");
        run(conn, "INSERT INTO exhibits VALUES (2, 'Zebra', 1.2)");

        run(conn, "INSERT INTO names VALUES (1, 1, 'Elsa')");
        run(conn, "INSERT INTO names VALUES (2, 2, 'Zelda')");
        run(conn, "INSERT INTO names VALUES (3, 1, 'Ester')");
        run(conn, "INSERT INTO names VALUES (4, 1, 'Eddie')");
        run(conn, "INSERT INTO names VALUES (5, 2, 'Zoe')");
    }

    private static void createStoredProcedures(Connection conn) throws SQLException {
        String noParams = """
                CREATE PROCEDURE read_e_names()
                READS SQL DATA DYNAMIC RESULT SETS 1
                BEGIN ATOMIC
                DECLARE result CURSOR WITH RETURN FOR SELECT * FROM names WHERE LOWER(name) LIKE 'e%';
                OPEN result;
                END""";

        String inParam = """
                CREATE PROCEDURE read_names_by_letter(IN prefix VARCHAR(10))
                READS SQL DATA DYNAMIC RESULT SETS 1
                BEGIN ATOMIC
                DECLARE result CURSOR WITH RETURN FOR
                SELECT * FROM names WHERE LOWER(name) LIKE CONCAT(LOWER(prefix), '%');
                OPEN result;
                END""";

        String inOutParam = """
                CREATE PROCEDURE double_number(INOUT num INT) READS SQL DATA
                  DYNAMIC RESULT SETS 1
                  BEGIN ATOMIC 
                  SET num = num * 2; 
                  END""";

        String outParam = """
                CREATE PROCEDURE magic_number(OUT num INT) READS SQL DATA
                      BEGIN ATOMIC
                     SET num = 42;
                      END""";

        run(conn, noParams);
        run(conn, inParam);
        run(conn, outParam);
        run(conn, inOutParam);
    }

    private static void run(Connection conn, String sql) throws SQLException {
        try (PreparedStatement ps = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)) {
            ps.executeUpdate();
        }
    }

    private static void printCount(Connection conn, String sql) throws SQLException {
        try (PreparedStatement ps = conn.prepareStatement(sql); ResultSet rs = ps.executeQuery()) {
            rs.next();
            System.out.println(rs.getInt(1));
        }
    }
}

class UdemyJDBC {
    private static void callProcedureWithOneParam() {
        String url = "jdbc:postgresql://localhost/dbpostgres";
        String procedureCall = "{call read_phone_by_name(?)}"; //param John
        try (Connection conn = DriverManager.getConnection(url, "luka", "luka123"); CallableStatement cs = conn.prepareCall(procedureCall);) {
            cs.setString(1, "John");
            boolean hasResults = cs.execute();
            if (hasResults) {
                ResultSet rs = cs.getResultSet();
                while (rs.next()) {
                    System.out.println(rs.getString("first_name") + ": " + rs.getString("phone"));
                }
            } else {
                System.out.println("No results.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void callProcedureWithTwoParams() {
        String url = "jdbc:postgresql://localhost/dbpostgres";
        String procedureCall = "{call read_phone_by_name(?, ?)}"; //param John, VARCHAR
        try (Connection conn = DriverManager.getConnection(url); CallableStatement cs = conn.prepareCall(procedureCall)) {
            cs.setString(1, "John");
            cs.registerOutParameter(2, Types.VARCHAR);
            cs.execute();
            String phone = cs.getString(2);
            System.out.println("Phone number for John: " + phone);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void connectToDb() {
        String url = "jdbc:postgresql://localhost/dbpostgres?user=postgres&password=12345";
        try (Connection conn = DriverManager.getConnection(url)) {
            if (conn != null) {
                System.out.println("Connected to the database!");
            } else {
                System.out.println("Failed to make connection");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    private static void insertToDb() {
        String url = "jdbc:postgresql://localhost/dbpostgres";
        String query = "INSERT INTO contacts (first_name, last_name, email, phone_number) VALUES (?,?,?,?)";
        try (Connection conn = DriverManager.getConnection(url, "postgres", "12345"); PreparedStatement ps = conn.prepareStatement(query);) {
            ps.setString(1, "John");
            ps.setString(2, "Wayne");
            ps.setString(3, "john@wayne.com");
            ps.setString(4, "555-783-5252");
            int affectedRows = ps.executeUpdate();
            if (affectedRows > 0) {
                System.out.println("A new record was inserted successfully");
            } else {
                System.out.println("No record was inserted");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    private static void selectFromDb() {
        String url = "jdbc:postgresql://localhost/dbpostgres";
        String query = "SELECT * FROM contacts";
        try (Connection conn = DriverManager.getConnection(url, "postgres", "12345"); PreparedStatement ps = conn.prepareStatement(query); ResultSet rs = ps.executeQuery();) {
            while (rs.next()) {
                int id = rs.getInt("id");
                String firstName = rs.getString("first_name");
                String lastName = rs.getString("last_name");
                System.out.println(id + ": " + firstName + " " + lastName);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void main(String[] args) {
        connectToDb();
    }
}

class Ocajp {
    public static void main(String[] args) {
        try (Connection conn = DriverManager.getConnection("jdbc:hsqldb:file:zoo")) {
            System.out.println(conn.getMetaData().allProceduresAreCallable());
            System.out.println(conn.getMetaData().getUserName());
            System.out.println(conn);

            run(conn, "DROP PROCEDURE read_e_names IF EXISTS");
            run(conn, "DROP PROCEDURE read_names_by_letter IF EXISTS");
            run(conn, "DROP PROCEDURE magic_number IF EXISTS");
            run(conn, "DROP PROCEDURE double_number IF EXISTS");
            run(conn, "DROP TABLE names IF EXISTS");
            run(conn, "DROP TABLE exhibits IF EXISTS");

            String createTableExhibits = """
                    CREATE TABLE exhibits (
                      id INTEGER PRIMARY KEY,
                      name VARCHAR(255),
                      num_acres DECIMAL(4,1))""";
            try (var ps = conn.prepareStatement(createTableExhibits)) {
                ps.execute();
                System.out.println("Table exhibits was created!");
            }

            String createTableNames = """
                    CREATE TABLE names (
                       id INTEGER PRIMARY KEY,
                       species_id integer REFERENCES exhibits (id),
                       name VARCHAR(255))""";
            try (var ps = conn.prepareStatement(createTableNames)) {
                ps.execute();
                System.out.println("Table names was created!");
            }

            var insertSql = "INSERT INTO exhibits VALUES(10, 'Deer', 3)";
            var updateSql = "UPDATE exhibits SET name = '' " + "WHERE name = 'None'";
            var deleteSql = "DELETE FROM exhibits WHERE id = 10";
            var sql = "SELECT * FROM exhibits";

            try (var ps = conn.prepareStatement(insertSql)) {
                int result = ps.executeUpdate();
                System.out.println(result); // 1
            }

            try (var ps = conn.prepareStatement(updateSql)) {
                int result = ps.executeUpdate();
                System.out.println(result); // 0
            }

            try (var ps = conn.prepareStatement(sql); ResultSet rs = ps.executeQuery()) {
                System.out.println(rs);
            }

            try (var ps = conn.prepareStatement(deleteSql)) {
                int result = ps.executeUpdate();
                System.out.println(result); // 1
            }

            insertSql = "INSERT INTO exhibits VALUES(1, 'Deer', 3)";
            try (var ps = conn.prepareStatement(insertSql)) {
                boolean isResultSet = ps.execute();
                if (isResultSet) {
                    try (ResultSet rs = ps.getResultSet()) {
                        System.out.println("ran a query");
                    }
                } else {
                    int result = ps.getUpdateCount();
                    System.out.println("ran an update");
                }
            }

            var sql2 = "INSERT INTO names VALUES(?, ?, ?)";
            try (var ps = conn.prepareStatement(sql2)) {
                ps.setInt(1, 20);
                ps.setInt(2, 1);
                ps.setString(3, "Ester");
                ps.executeUpdate();
                ps.setInt(1, 21);
                ps.setString(3, "Elias");
                ps.executeUpdate();
            }

            register(conn, 100, 1, "Elias", "Ester");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void register(Connection conn, int firstKey, int type, String... names) throws SQLException {
        var sql = "INSERT INTO names VALUES(?, ?, ?)";
        var nextIndex = firstKey;
        try (var ps = conn.prepareStatement(sql)) {
            ps.setInt(2, type);
            for (var name : names) {
                ps.setInt(1, nextIndex);
                ps.setString(3, name);
                ps.addBatch();
                nextIndex++;
            }
            int[] result = ps.executeBatch();
            System.out.println(Arrays.toString(result));
        }
    }

    public static void register(Connection conn, int key, int type, String name) throws SQLException {
        String sql = "INSERT INTO names VALUES(?, ?, ?)";
        try (PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setInt(1, key);
            ps.setString(3, name);
            ps.setInt(2, type);
            ps.executeUpdate();
        }
    }

    private static void run(Connection conn, String sql) throws SQLException {
        try (PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.executeUpdate();
        }
    }
}

class Tran {
    public static void main(String[] args) throws SQLException {
        Connection conn2 = DriverManager.getConnection("jdbc:hsqldb:file:zoo");
        var v = conn2.prepareStatement("");
        var r = v.executeQuery();
        r.next();
        r.getInt(1);
        r.close();
        v.close();
        conn2.close();

        try (Connection conn = DriverManager.getConnection("jdbc:hsqldb:file:zoo")) {
            conn.setAutoCommit(false);
            Savepoint sp1 = conn.setSavepoint();
            String updateSql = """
                    UPDATE exhibits 
                    SET num_acres = num_acres + ? 
                    WHERE name = ?""";

            var elephantRowsUpdated = updateRow(conn, updateSql, 5, "African Elephant");

            Savepoint sp2 = conn.setSavepoint("second savepoint");
            conn.rollback(sp2);
            var zebraRowsUpdated = updateRow(conn, updateSql, -5, "Zebra");
            var errZebraRowsUpdated = false;
            System.out.println(elephantRowsUpdated + " " + zebraRowsUpdated);
            conn.rollback(sp1);
            if (!elephantRowsUpdated || !zebraRowsUpdated) {
                conn.rollback();
            } else {
                String selectSql = """
                        SELECT COUNT(*) 
                        FROM exhibits 
                        WHERE num_acres <= 0""";
                try (PreparedStatement ps = conn.prepareStatement(selectSql); ResultSet rs = ps.executeQuery()) {

                    rs.next();
                    int count = rs.getInt(1);
                    System.out.println("count " + count);
                    if (count == 0) {
                        conn.commit();
                    } else {
                        conn.rollback();
                    }
                }
            }
        }
    }

    private static boolean updateRow(Connection conn, String updateSql, int numToAdd, String name)

            throws SQLException {

        try (PreparedStatement ps = conn.prepareStatement(updateSql)) {
            ps.setInt(1, numToAdd);
            ps.setString(2, name);
            return ps.executeUpdate() > 0;
        }
    }
}

/*
class Test {
    public static void main(String[] args) {
        var sql = "UPDATE food SET amount = amount + 1";
        try (var conn = DriverManager.getConnection("jdbc:hsqldb:file:zoo");
             var ps = conn.prepareStatement(sql)) {
            var result = ps.executeUpdate();
            System.out.println(result);
        }
    }
}*/

class Enthuware {
    public static void main(String[] args) {
        String url = "jdbc:hsqldb:file:zoo";
        try (Connection conn = DriverManager.getConnection(url);
             Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)) {
            var rs = stmt.executeQuery("");
            rs.absolute(1);
            rs.updateString(1, "111");
            rs.updateRow();
            rs.refreshRow();

            rs.moveToInsertRow();
            rs.updateString(2, "Java in 24 hours");
            rs.insertRow();
            rs.moveToCurrentRow();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}