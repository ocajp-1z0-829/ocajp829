package com.ocajp.jdk17.constructors.packageD;


//import com.ocajp.jdk17.constructors.packageA.A; //'com.ocajp.jdk17.constructors.packageA.A' is not public in 'com
// .ocajp.jdk17.constructors.packageA'. Cannot be accessed from outside package


//import com.ocajp.jdk17.constructors.packageB.B; //'com.ocajp.jdk17.constructors.packageB.B' is not public in 'com
// .ocajp.jdk17.constructors.packageB'. Cannot be accessed from outside package


//import com.ocajp.jdk17.constructors.packageB.B; //Cannot inherit from final 'com.ocajp.jdk17.constructors.packageB.B'

//import com.ocajp.jdk17.constructors.packageA.E; //There is no default constructor available in 'com.ocajp.jdk17
// .constructors.packageA.E'

import com.ocajp.jdk17.constructors.packageC.D;

public class C extends D {
}
