package com.ocajp.jdk17.constructors;


public class Animal {
  static {
    System.out.println("A");
  }
}

/**
 * A
 * B
 * C
 * new Hippo
 * new Hippo
 * new Hippo
 */
class Hippo extends Animal {
  static {
    System.out.println("B");
  }

  public Hippo() {
    System.out.println("new Hippo");
  }

  public static void main(String[] args) {
    System.out.println("C");
    new Hippo();
    new Hippo();
    new Hippo();
  }
}

/**
 * C
 * A
 * B
 * new Hippo
 * new Hippo
 */
class HippoFriend {
  public static void main(String[] args) {
    System.out.println("C");
    new Hippo();
    new Hippo();
  }
}

class A {
  static {
    System.out.print("Hello from");
  }
  static String name = " A";
}

class B extends A {
  static {
    System.out.print(" B");
  }
}

class C extends B {
  static {
    System.out.print(" C");
  }
}

/**
 * Ссылка на стат поле вызывает только тот класс где это поле декларировано
 */
class T {
  public static void main(String[] args) {
    System.out.print(C.name); //Hello from A
    System.out.println();
    System.out.println(new C().name); //B C A
  }
}