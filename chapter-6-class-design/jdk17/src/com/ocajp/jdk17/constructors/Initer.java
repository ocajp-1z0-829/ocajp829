package com.ocajp.jdk17.constructors;

/**
 * Все вызывается по порядку следования.
 * Статика
 * Потом обычный блок
 */
public class Initer {

  //При запуске иниц статика
  static
  {
    k = 3; //изначально k = 0
    System.out.println(Initer.k);
    //System.out.println(k); //Illegal forward reference потому что переменная объявлена ниже
  }

  //При инициализации, создании объекта
  {
    m = 5; //изначально m = 0
    System.out.println(this.m);
    System.out.println(m = 7);
    //System.out.println(m); //Illegal forward reference потому что переменная объявлена ниже
  }

  static int k = 2;

  int m = 1;

  public Initer(int m) {
    this.m = m;
  }

  public Initer() {
  }

  public static void main(String[] args) {
    System.out.println(new Initer().m);
    System.out.println(k);
  }
}

class IniterDescendant extends Initer {

  public IniterDescendant() {
    super(1);
  }
}

class MouseHouseError {
  private final int volume;
  //private final String type; //Variable 'type' might not have been initialized
  {
    this.volume = 10;
  }
  public MouseHouseError(String type) {
    this();
    //this.type = type;
  }
  public MouseHouseError() { // DOES NOT COMPILE
    //this(null);
    //this.volume = 2; // DOES NOT COMPILE Variable 'volume' might already have been assigned to
  }
}

class GiraffeFamily {
  static {
    System.out.print("A"); //1
  }

  {
    System.out.print("B"); //3 8
  }

  public GiraffeFamily(String name) {
    this(1);
    System.out.print("C"); //5 10
  }

  public GiraffeFamily() {
    System.out.print("D");
  }

  public GiraffeFamily(int stripes) {
    System.out.print("E"); //4 9
  }
}

class Okapi extends GiraffeFamily {
  static {
    System.out.print("F"); //2
  }

  {
    System.out.print("H"); //6 11
  }

  public Okapi(int stripes) {
    super("sugar");
    System.out.print("G"); //7 12
  }

  public static void main(String[] grass) {
    new Okapi(1);
    System.out.println();
    new Okapi(2);
    System.out.println();
  }
}

class Beetle {

  public static void testRef() {
    System.out.println("Beetle");
  }

  private String getSize() {
    return "Undefined";
  } }
class RhinocerosBeetle extends Beetle {

  public static void testRef() {
    System.out.println("RhinocerosBeetle");
  }

  private int getSize() {
    return 5;
  }

  public static void main(String[] args) {
    RhinocerosBeetle r = new RhinocerosBeetle();
    r.testRef();
    Beetle b = r;
    b.testRef();
  }
}