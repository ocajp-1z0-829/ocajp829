package com.ocajp.jdk17.constructors;

public class WebinarConstructor {
  int a;

  public WebinarConstructor() {
    this.a = 1;
  }

  private WebinarConstructor(int a) {
    this();
    this.a = 2;
  }

  public static void main(String[] args) {
    //this.a = 5; // cannot be referenced from a static context
    WebinarConstructor c = new WebinarConstructor(3);
    c.test();
  }

  public void test() {
    this.a = a;
    System.out.println(this);
    //this(); //Call to 'this()' must be first statement in constructor body
  }

  @Override
  public String toString() {
    return "WebinarConstructor{" + "a=" + a + '}';
  }
}

class Test {
  final static int a, b; //статика уже должна быть проиниц сразу или в стат блоке

  final int c, d;
  {
    //a = 10; //Variable 'a' might already have been assigned to
  }

  static {
    b = 10;
    a = 10;
  }
  static {
    //c = 10; //Non-static field 'c' cannot be referenced from a static context
  }
  {
    c = 10;
  }

  Test() {
    //b = 10; //Variable 'b' might already have been assigned to
    d = 10;
  }
}