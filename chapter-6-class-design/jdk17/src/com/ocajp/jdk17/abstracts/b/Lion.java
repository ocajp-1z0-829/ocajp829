package com.ocajp.jdk17.abstracts.b;

/**
 * Class 'Lion' must either be declared abstract or implement abstract method 'getName()' in 'Animal'
 */
class Lion {// extends BigCat {
  public String getName() {
    return "Lion";
  }
  public void roar() {
    System.out.println("The Lion lets out a loud ROAR!");
  }
}

abstract class Mammal { //Illegal combination of modifiers: 'final' and 'abstract'
  abstract CharSequence chew();
  public Mammal() {
    System.out.println(chew()); // Does this line compile?
  }
}
class Platypus extends Mammal {
  String chew() { return "yummy!"; }
  public static void main(String[] args) {
    new Platypus();
  }
}

interface ff {
  void df();
}