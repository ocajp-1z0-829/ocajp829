package com.ocajp.jdk17.abstracts;

public abstract class Mammal {
  abstract void showHorn();
  abstract void eatLeaf();
}
abstract class Rhino extends Mammal {
  void showHorn() {} // Inherited from Mammal
}
class BlackRhino extends Rhino {
  void eatLeaf() {} // Inherited from Mammal
}