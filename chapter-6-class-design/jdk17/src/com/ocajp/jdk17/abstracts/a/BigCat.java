package com.ocajp.jdk17.abstracts.a;

abstract class Animal {
  abstract String getName();
}
abstract public class BigCat extends Animal {
  protected abstract void roar();
}

class Lion extends BigCat {
  public String getName() {
    return "Lion";
  }
  public void roar() {
    System.out.println("The Lion lets out a loud ROAR!");
  }
}