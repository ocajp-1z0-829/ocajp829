package com.ocajp.jdk17.abstracts;

public class AbstractClass {
  public static void main(String[] p) {
    Canine w = new Fox();
    w.bark(); // Squeak!
  }
}

abstract class Canine {
  public abstract String getSound();

  public void bark() {
    System.out.println(getSound());
  }

  public static void main(String[] p) {
    Canine w = new Canine() {
      @Override
      public String getSound() {
        return "Test abstract instant";
      }
    };
    w.bark(); // Squeak!
  }
}

class Wolf extends Canine {
  public String getSound() {
    return "Wooooooof!";
  }
}

class Fox extends Canine {
  public String getSound() {
    return "Squeak!";
  }
}

class Coyote extends Canine {
  public String getSound() {
    return "Roar!";
  }
}

abstract class Llama {
  public void chew() {}
}

//public class Egret { // DOES NOT COMPILE Class 'Egret' must either be declared abstract or implement abstract method
  // 'peck()' in 'Egret'
  //public abstract void peck();
//}

abstract class Elephant {
  abstract private class SleepsAlot {
    abstract int sleep();
  }

  class ElephantImpl extends SleepsAlot {
    @Override
    int sleep() {
      return 0;
    }
  }

  public void sleep() {
    ElephantImpl elephant = new ElephantImpl();
    elephant.sleep();
  }
}