package com.ocajp.jdk17.test;

public class Test {

}

class BirdSeed {
  private int numberBags;
  boolean call;

  public BirdSeed() {
    this(2);
    new BirdSeed(2);
    call = false;
    new BirdSeed(2);
  }

  public BirdSeed(int numberBags) {
    this.numberBags = numberBags;
  }

  public static void main(String[] args) {
    var seed = new BirdSeed();
    System.out.print(seed.numberBags);
  }

  static final void t1(){}
  static private void t2(){}
  private final void t6(){}
  //static abstract void t3(){} //Illegal combination of modifiers: 'abstract' and 'static'
  //private abstract void t4(){} //Illegal combination of modifiers: 'abstract' and 'private'
  //abstract final void t5(){} //Illegal combination of modifiers: 'abstract' and 'final'
}

class Mammal {
  public Mammal(int age) {
    System.out.print("Mammal");
  }

  private void sneeze() {
  }
}

class Platypus extends Mammal {
  public Platypus() {
    super(0); //There is no default constructor available in 'com.ocajp.jdk17.test.Mammal'
    System.out.print("Platypus");
  }

  public static void main(String[] args) {
    new Mammal(5);
  }

  int sneeze() {
    return 1;
  }
}

class Speedster {
  int numSpots;
}
class Cheetah extends Speedster {
  int numSpots;
  public Cheetah(int numSpots) {
    super.numSpots = numSpots;
  }
  public static void main(String[] args) {
    Speedster s = new Cheetah(50);
    System.out.print(s.numSpots);
  }
}

final class Moose {
  //private final int antlers; //Variable 'antlers' might not have been initialized
}

abstract class Bird2 {
  protected Bird2() {
    System.out.print("Wow-");
  }

  private final void fly() {
    System.out.println("Bird");
  }

  public Number t(Number n) {
    return 1;
  }
}

class Pelican extends Bird2 {
  public Pelican() {
    System.out.print("Oh-");
  }

  //@Override
  public Number t(Integer n) { //Method does not override method from its superclass
    return 1;
  }

  @Override
  public Integer t(Number n) {
    return 1;
  }

  public static void main(String[] args) {
    Bird2 chirp = new Pelican();
    //chirp.fly(); //'fly()' has private access in 'com.ocajp.jdk17.test.Bird'

    var ch = new Pelican();
    ch.fly();
  }

  protected void fly() {
    System.out.println("Pelican");
  }
}

class Howler {
  public Howler(long shadow) {
    this(3);
    //this((short)1);
  }

  private Howler(int moon) {
    super();
  }
}

class Wolf extends Howler {
  protected Wolf(String stars) {
    super(2L);
  }

  public Wolf() {
    this("");
    //this(null);
  }
}

class Bird {
  int feathers = 0;

  Bird(int x) {
    this.feathers = x;
  }

  Bird fly() {
    return new Bird(1);
  }
}

class Parrot extends Bird {
  protected Parrot(int y) {
    super(y);
  }

  protected Parrot fly() {
    return new Parrot(2);
  }
}

class Macaw extends Parrot {
  public Macaw(int z) {
    super(z);
  }

  public static void main(String... sing) {
    Bird p = new Macaw(4);
    System.out.print(((Parrot) p.fly()).feathers);
  }

  public Macaw fly() {
    return new Macaw(3);
  }
}


class Person {
  static String name;

  void setName(String q) {
    name = q;
  }
}

class Child extends Person {
  static String name;

  void setName(String w) {
    name = w;
  }

  public static void main(String[] p) {
    final Child m = new Child();
    final Person t = m;
    m.name = "Elysia";
    t.name = "Sophia";
    m.setName("Webby");
    t.setName("Olivia");
    System.out.println(m.name + " " + t.name);
  }
}

abstract class T3 {
  abstract Number t3(Number number);
}
class T4 extends T3 {
  @Override
  Integer t3(Number number) {
    return null;
  }
}

abstract class Whale {
  int a;
  public static void main(String[] args) {
    Whale whale = new Orca();
    //whale.dive(3); //'dive()' in 'com.ocajp.jdk17.test.Whale' cannot be applied to '(int)'
  }

  public Whale(int a) {
  }

  public abstract void dive();
}

class Orca extends Whale {
  static public int MAX = 3;
  int b;
  public Orca(int b, int a) {
    super(b);
  }

  public Orca() {
    super(1);
  }

  public Orca(int b) {
    //this(b, 1); //Call to 'super()' must be first statement in constructor body
    super(1);
  }

  //public Orca() {}//There is no default constructor available in 'com.ocajp.jdk17.test.Whale'

  //private Orca() {} //Modifier 'final' not allowed here

  public void dive() {
    System.out.println("Orca diving");
  }

  public void dive(int... depth) {
    System.out.println("Orca diving deeper " + MAX);
  }
}

class M {
  private final int f;

  M(int f) {
    //this.f = f; //Variable 'f' might already have been assigned to
  }

  {
    f = 5;
  }

  public static int roundV(double arg) {
    return 0;
  }

public void se(Object o) {
  //this.object = o; //Cannot assign a value to final variable 'object'
}
  //private final Object object;


}

class M2 {
  public static void main(String[] args) {
    M.roundV(1);
    new com.ocajp.jdk17.test.M(1).roundV(1);
    new Short("4"); //Cannot resolve constructor 'Short(int)'
    new Short((short) 4);
    Short.valueOf((short) 4);
  }
}
