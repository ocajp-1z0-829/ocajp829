package com.ocajp.jdk17.immutable;

import java.util.ArrayList;
import java.util.List;

/**
 * 1. Mark the class as final or make all of the constructors private.
 * 2. Mark all the instance variables private and final.
 * 3. Don’t define any setter methods.
 * 4. Don’t allow referenced mutable objects to be modified.
 * 5. Use a constructor to set all properties of the object, making a copy if needed.
 */
final class Animal { // Not an immutable object declaration
  private final ArrayList<String> favoriteFoods;

/*  public DefensiveAnimal(ArrayList<String> favoriteFoods) {
    if (favoriteFoods == null || favoriteFoods.size() == 0)
      throw new RuntimeException("favoriteFoods is required");
    this.favoriteFoods = favoriteFoods;
  }*/

  public Animal(List<String> favoriteFoods) {
    if (favoriteFoods == null || favoriteFoods.size() == 0)
      throw new RuntimeException("favoriteFoods is required");
    this.favoriteFoods = new ArrayList<String>(favoriteFoods);
  }

  public int getFavoriteFoodsCount() {
    return favoriteFoods.size();
  }

  public String getFavoriteFoodsItem(int index) {
    return favoriteFoods.get(index);
  }
}

class Main {
  public static void main(String[] args) {
    var favorites = new ArrayList<String>();
    favorites.add("Apples");
    var zebra = new Animal(favorites); // Caller still has access to favorites
    System.out.println(zebra.getFavoriteFoodsItem(0)); // [Apples]
    favorites.clear();
    favorites.add("Chocolate Chip Cookies");
    System.out.println(zebra.getFavoriteFoodsItem(0)); // [Apples]
  }
}