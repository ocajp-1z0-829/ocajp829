package com.ocajp.jdk17;

public class Tests {
}

class Q6 {
    public final class Moose {
        //private final int antlers; //Variable 'antlers' might not have been initialized
    }
    public class Caribou { private int antlers = 10;
    }
    public class Reindeer {
        private final int antlers = 5;
    }
    public final class Elk {}
    public final class Deer {
        private final Object o = new Object();
    }
}

class Q11 {
    StringBuilder value = new StringBuilder("t");
    {
        value.append("a"); }
    { value.append("c"); }
    private Q11() {
        value.append("b"); }
    public Q11(String s) { this();
        value.append(s); }
    public Q11(CharSequence p) { value.append(p);
    }
    public static void main(String[] args) {
        Object bear = new Q11();
        bear = new Q11("f"); System.out.println(((Q11)bear).value);
    } }

class Q12 {
        public Q12(Integer x) {}
        protected static Integer chew() throws Exception {
            System.out.println("Rodent is chewing");
            return 1;
        }
}
class Q12_2 extends Q12 {
    public Q12_2(Integer x) {
        super(x);
    }

    public static Integer chew() throws RuntimeException { System.out.println("Beaver is chewing on wood"); return 2;
} }

