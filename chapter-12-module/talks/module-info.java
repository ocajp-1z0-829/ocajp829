module zoo.animal.talks {
  //exports zoo.animal.talks.content;
  opens zoo.animal.talks.content to zoo.staff;
  opens zoo.animal.talks.media;
  opens zoo.animal.talks.schedule;
  //requires zoo.animal.feeding;
  //requires zoo.animal.care;
  requires transitive zoo.animal.care;
}
