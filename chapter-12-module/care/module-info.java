module zoo.animal.care {
  exports zoo.animal.care.medical;
  opens zoo.animal.care.medical;
  //requires zoo.animal.feeding;
  requires transitive zoo.animal.feeding;
}
