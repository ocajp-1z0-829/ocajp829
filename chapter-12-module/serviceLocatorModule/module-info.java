module zoo.tours.reservations {
  exports zoo.tours.reservations;
  requires zoo.tours.api; //need for compiler and need for uses
  uses zoo.tours.api.Tour; //need for lookup. References service
  requires java.base;
  requires jdk.attach;
}