module zoo.tours.agency {
  requires zoo.tours.api; //module containing the interface as a dependency
  provides zoo.tours.api.Tour with zoo.tours.agency.TourImpl; //This allows us to specify that we provide an
  //provides zoo.tours.api.Tour with zoo.tours.agency.TourImpl2; //This allows us to specify that we provide an
  // implementation of the interface with a specific implementation class
}