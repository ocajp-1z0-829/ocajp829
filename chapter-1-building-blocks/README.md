# chapter-1-building-blocks

# Sources https://habr.com/ru/articles/125210/

## The method name and parameter types are called the method signature

```java
// Therefore, the method signature is numberVisitors(int).
public int numberVisitors(int month){
    return 10;
    }
```

## both final modifiers are optional, and the main() method is a valid entry point with or without them.

```java
public final static void main(final String[]args){}
```

## сложные вопросы
![img.png](img.png)
```
Q31B D G

Q31A Train object can share both the TrainDriver and its Carriage objects
with other Train objects, when it is not using them. In other words, they can
outlive the Train object. This is an example of aggregation. However, a Train
object owns the array object used for handling its carriages. The lifetime of an array
object is nested in the lifetime of its Train object. This is an example of
composition.
```

---

![img_1.png](img_1.png)

D F
Feedback
The parent directory (or location) of the package must be specified. Only (d) and
(f) do that. (d) specifies the current directory first, but there is no file
top/sub/Q31A.class under the current directory. Searching under ../bin (i.e.,
/proj/bin) finds the file top/sub/Q31A.class.

---

![img_3.png](img_3.png)

if object obj1 can access object obj2 that is eligible for garbage collection when obj1 is also eligible for garbage collection

если объект obj1 может получить доступ к объекту obj2, который имеет право на сборку мусора, когда obj1 также имеет право на сборку мусора

Feedback
An object is eligible for garbage collection only if all remaining references to the
object are from other objects that are also eligible for garbage collection. Therefore,
if an object obj2 is eligible for garbage collection and object obj1 contains a
reference to it, then object obj1 must also be eligible for garbage collection. Java
does not have a keyword delete. An object will not necessarily be garbage
collected immediately after it becomes unreachable, but the object will be eligible
for garbage collection. Circular references do not prevent objects from being
garbage collected; only reachable references do. An object is not eligible for
garbage collection as long as the object can be accessed by any live thread. An
object that is eligible for garbage collection can be made non-eligible if the
finalize() method of the object creates a reachable reference to the object.

---


---

# jar

Compile
```
javac HelloWorld.java
```

Run .class
```
java -classpath . HelloWorld
```

Compile and save to dir using key -d and bin directory
```
javac -d bin src/HelloWorld.java

```

Run ./bin .class
```
java -classpath ./bin HelloWorld

```

# package

Compile
```
javac -d bin com/test/HelloWorld.java
```

Run
```
java -classpath ./bin com/test/HelloWorld
```

## Compile many classes using -sourcepath
```
javac -sourcepath ./src -d bin src/com/test/HelloWorld.java
```

Run
```
java -classpath ./bin com/test/HelloWorld
```

## Debugger with key -g
```
javac -g -sourcepath ./src -d bin src/com/test/HelloWorld.java
```

Run debugger
```
jdb -classpath bin -sourcepath src com.test.HelloWorld
```

In debugger stop on line 9
```
stop at com.test.Calculator:9
```

Continue
```
run
```

Show current piece of code
```
main[1] list
```

What is variable a
```
main[1] print a
```

Continue
```
main[1] cont
```

## Archive. С помощью ключа -C мы запустили программу в каталоге bin.
```
jar cvf calculator.jar -C bin .
```

Check what is inside
```
javap -c -classpath calculator.jar com.test.calculator.Calculator

//Без ключа -c программа выдаст только список переменных и методов (если использовать -private, то всех).
javap -private  -classpath calculator.jar com.test.calculator.Calculator
```

## Documentation
```
mkdir doc	
javadoc -d doc -charset utf-8  -sourcepath src -author -subpackages com.test.calculator

```

Если требуется подписать свою библиотеку цифровой подписью, на помощь придут keytool и jarsigner
```
keytool -genkey -keyalg rsa -keysize 2048 -alias qwertokey -keystore path/to/qwerto.keystore

keytool -certreq -file path/to/qwertokey.crt -alias qwertokey -keystore path/to/qwerto.keystore

keytool -import -trustcacerts -keystore path/to/qwert.keystore -alias qwertokey -file path/to/qwertokey.cer

jarsigner -keystore path/to/qwerto.keystore calculator.jar qwertokey

jarsigner -verify -verbose -certs -keystore path/to/qwerto.keystore calculator.jar

cd HelloWorld
javac -sourcepath src -d bin -classpath path/to/calculator.jar src/com/qwertovsky/helloworld/HelloWorld.java

```

# Chapter 15 JDBC

* 'C:\JAVA_UTILS\openjdk-17.0.1\bin\javac' -cp hsqldb-2.7.2.jar -d ./bin src/com/ocajp/SetupDatabase.java
* 'C:\JAVA_UTILS\openjdk-17.0.1\bin\javac' -cp ".;lib\hsqldb-2.7.2.jar" src/com/ocajp/SetupDatabase.java
* 'C:\JAVA_UTILS\openjdk-17.0.1\bin\javac' -cp ".;lib\hsqldb-2.7.2.jar;./src/" src/com/ocajp/SetupDatabase.java
* 'C:\JAVA_UTILS\openjdk-17.0.1\bin\java' -cp ".;lib\hsqldb-2.7.2.jar" src/com/ocajp/SetupDatabase.java
* 'C:\JAVA_UTILS\openjdk-17.0.1\bin\java' -cp ".;lib\hsqldb-2.7.2.jar;./src/" com.ocajp.SetupDatabase
* 'C:\JAVA_UTILS\openjdk-17.0.1\bin\java' -cp ".;lib\hsqldb-2.7.2.jar;./src/" com.ocajp.Ocajp


# Webinar 4

Keywords
![img_2.png](img_2.png)

String к ним не относится


