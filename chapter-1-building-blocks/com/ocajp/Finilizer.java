package com.ocajp;

import java.util.ArrayList;
import java.util.List;

public class Finilizer {
  private static List objects = new ArrayList();
  @Override
  protected void finalize() throws Throwable {
    System.out.println("Calling finalize()");
    throw new NullPointerException();
    //objects.add(this); //Don't do like this because GC will be shocked before deleting unused object from heap
  }

  public static void main(String[] args) throws Throwable {
    Finilizer f = new Finilizer();
    //f.finalize();
  }
}
