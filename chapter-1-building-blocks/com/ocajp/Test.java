package com.ocajp;

public class Test {

  public Test() {
    super();
    //this();
  }

  /**
   * B, D, E, H. A var cannot be initialized with a null value without a type, but it can be
   * assigned a null value later if the underlying type is not a primitive. For these reasons, option
   * H is correct, but options A and C are incorrect. Options B and D are correct as the underlying types are String
   * and Integer, respectively. Option E is correct as this is a valid
   * numeric expression. You might know that dividing by zero produces a runtime exception, but
   * the question was only about whether the code compiled. Finally, options F and G are incorrect as var cannot be
   * used in a multiple-variable assignment.
   */
  public static void main(String[] args) {
    //var spring = null;
    var fallStr = "leaves";
    //var evening = 2; evening = null;
    var night = Integer.valueOf(3);
    var day = 1/0;
    //var winter = 12, cold;
    //var fall = 2, autumn = 2;
    var morning = ""; morning = null;
  }
}

class Sb {
  public static void main(String[] args) {
    StringBuilder stringBuilder = new StringBuilder();
    System.out.println(stringBuilder.length());
    System.out.println(stringBuilder.capacity());
    stringBuilder.ensureCapacity(20);
    System.out.println(stringBuilder.capacity());
    stringBuilder.ensureCapacity(50);
    System.out.println(stringBuilder.capacity());
    stringBuilder.append("Aaaab");
    System.out.println(stringBuilder);
    stringBuilder.reverse();
    System.out.println(stringBuilder);
    stringBuilder.insert(1, "Test", 2, 4);
    System.out.println(stringBuilder);
    stringBuilder.delete(2, 10);
    System.out.println(stringBuilder);
  }
}

class Q7 {
  private int numForks;
  public static void main(String[] args) {
    int numKnives;
    System.out.print("""
          "# forks = " + numForks +
            " # knives = " + numKnives +
          # cups = 0"""); }
}

class Q8 {
  public static void main(String[] args) {
    //var spring = null;
    var fall = "leaves";
    //var evening = 2; evening = null;
    var night = Integer.valueOf("3");
    //var day1 = 1/0;
    var day2 = 0/0.0;
    System.out.println(day2); //NaN
    var day = 1/0.0;
    System.out.println(day);
    //var winter = 12, cold;
    //var fall2 = 2, autumn = 2;
    var morning = ""; morning = """
            null""";
  }
}

class Q10 {
  public static void main(String[] args) {
    var m = 9___6;
    System.out.println(m);
  }
}

class Q12 {
  //int gills = 0, double weight=2;
  int gills = 0; double weight=2;
  { int fins = gills; }
  //static void print(int length = 3) {
  void print(int length) {
    System.out.println(gills);
    System.out.println(weight);
    //System.out.println(fins);
    System.out.println(length);
}

  public static void main(String[] args) {
    Q12 q12 = new Q12();
    q12.print(1);
  }
}

class Q16 {
  public static void main(String[] args) {
    var blocky = """ 
      squirrel \s
      pigeon \
      termite""";
    System.out.print(blocky);
  }
}

class Q21 {
  int count;
  { System.out.print(count+"-"); }
  { count++; }
  public Q21() {
    count = 4;
    System.out.print(2+"-");
  }
  public static void main(String[] args) {
    System.out.print(7+"-");
    var s = new Salmon();
    System.out.print(s.count+"-"); }
}

class Q22 {
  public static void main(String[] args) {
        int Amount = 0b11;
        System.out.println(Amount);
        //int amount = 9L;
        int amount = 0xE;
        System.out.println(amount);
        //amount = 1_2.0;
        amount = 0b101;
        System.out.println(amount);
        //double amountD = 1_0_.0;
        double amountD = 9_2.1_2;
        //amountD = 1_2_.0_0;
      System.out.println(amountD);
  }
}