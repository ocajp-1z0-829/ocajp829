package com.ocajp.webinar3;

//import com.ocajp.webinar3.helpers.NotPublicClass; //not public class is not imported
import java.lang.*; // this is imported default

//import java.lang.System.out; // error
import static java.lang.System.out;
        import static java.lang.Integer.*;
//import java.nio.*.*; // no good, not working
import java.nio.file.Paths;
//import java.util.Date; //import java.util.Date collides with another import statement
//import java.sql.Date;

        import static java.util.Collections.binarySearch;
import static java.util.Arrays.binarySearch; // no error because signatures are different in method import static java.util.Collections.binarySearch;

// call
/**
 * packages
 */
public class Lesson3 {

  public static void main(String[] args) {
    out.println(toHexString(42));
    Paths.get("name");
    binarySearch(new int[]{1}, 3); // ambiguous method call in use import static com.ocajp.webinar3.helpers.Auxiliary
    // .binarySearch;
    //file.Paths paths; //import java.nio.*; //not good, dont see file.Paths
    out.println(day);
    //Date date; //the type Date is ambiguous cause of java.util.*, and java.sql.* indefinite
    //NotPublicClass notPublicClass; //not public class is not imported
  }
  private static  String day = "Sunday";

  public static int binarySearch(int[] a, int key) {
    return -1;
  }
}

/*
public class B {
  public void answer() {
    //private String p = "protected"; //private is not allowed, final is allowed
    //System.out.println(p);
  }
}*/
