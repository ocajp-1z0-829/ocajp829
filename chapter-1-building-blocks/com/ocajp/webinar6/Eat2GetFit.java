package com.ocajp.webinar6;
//javac Eat2GetFit.java
//java Eat2GetFit 100 100 100
public abstract class Eat2GetFit { // line 1
  static double caloriesPerNight = 0f; //line 2

  public static void main(String[] args) {
    int weight, height, age;
    if (  !(args.length < 3) ) { //line 3
      weight = Integer.parseInt(args[0]);
      height = Integer.parseInt(args[1]);
      age = Integer.parseInt(args[2]);
    } else {
      System.out.println("weight height age");
    }
    //caloriesPerNight = weight * height / age * (int) 100; //line 4 compilation error
    System.out.println("Stay below " + caloriesPerNight);
  }
}
