package com.ocajp.webinar6;

public class Test {

  //Eat2GetFit eat2GetFit = new Eat2GetFit() {} //нельзя инстанциировать абстрактные классы
  char ch = 'c';
  //static double d = ch; //не видит ch

  public static void main(String[] args) {
    char ch = 'a';
    double d = ch;

    System.out.println(d);
  }
}

class Parser {
  String input = "0123";
  public void parseMe(String str) {
    int output = 0;
    try {
      String input = str;
      output = Integer.parseInt(input);
    } catch (IllegalArgumentException e) {
      System.out.println("Wrong argument!");
    }
    //input: 0123, output: 2013
    System.out.println("input: " + input + ", output: " + output);
  }

  public static void main(String[] args) {
    Parser p = new Parser();
    p.parseMe("2013");
  }
}

class Vars {
  int z;

  public static void main(String[] args) {
    Vars obj = new Vars();
    int z = 3; //line 1
    System.out.print(z); //line 2
    obj.doStuff();
    System.out.print(z); //line 3
    System.out.println(obj.z); //line 4
  }

  void doStuff() {
    int z = 2; // line 5
    doStuff(z);
    System.out.print(z); //line 6
  }

  void doStuff(int zzz) {
    z = 1;
  }
}

