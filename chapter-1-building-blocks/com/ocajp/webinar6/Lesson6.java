package com.ocajp.webinar6;

public class Lesson6 {

  int z;
  static {int z = 1;} //если так же сделать локально то будет ошибка то что z уже есть

  public static void main(String[] args) {
    ///z = 1;

    Phone p1 = null;
    System.out.println(p1.softKeyboard); // не будет NPE

    char digit = 'z';

    switch (digit) {
      case 'a' :
        //int i; // не будет работать в default, действует на весь switch
        {int i;} //будет работать в default, действует только в {}
      default: int i; //Variable 'i' is already defined in the scope
    }

  }

  public void testBlockInit() {
    int z;
    //{int z = 1;} //в инстансе не ругается, локальная ругается
  }
}

class Phone {
  static boolean softKeyboard = true;
}

class Dark {
  int x = 3;

  public static void main(String[] args) {
    new Dark().go1();
  }

  void go1() {
    //int x; //так как неинициализирована не скомпилируется
    int x = 0;
    go2(++x);//так как неинициализирована не скомпилируется
  }

  void go2(int y) { //1
    int x = ++y; //2
    System.out.println(x);
  }


}