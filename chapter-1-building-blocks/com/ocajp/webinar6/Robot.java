package com.ocajp.webinar6;

public class Robot {
  static String weight = "A lot";  //class variable
  /* default */ double ageMonths = 5, ageDays = 2; //instance variables

  private static boolean success = true; //class variable
  public void main2(String[] args) {
    final String retries = "1"; //local variable
    //P1
    System.out.println(weight);
    System.out.println(success);
  }

  public static void main(String[] args) {
    Robot robot = new Robot();
    robot.main2(args);
  }
}
