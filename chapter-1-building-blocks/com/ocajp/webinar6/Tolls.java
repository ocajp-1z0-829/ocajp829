package com.ocajp.webinar6;

/**
 * The code compiles and runs without issue, so Options A and B are incorrect. The question relies on your ability to
 * understand variable scope. The variable today has local scope
 * to the method in which it is executed. The variable tomorrow is re-declared in the method,
 * but the reference used on line 7 is to the instance variable with a value of 10. Finally, the
 * variable tomorrow is static. While using an instance reference to access a static variable
 * is not recommended, it does not prevent the variable from being read. The result is line 7
 * evaluates and prints (20 + 10 + 1) = 31, making C the correct answer.
 */
public class Tolls {
  private static int yesterday = 1;
  int tomorrow = 10;

  public static void main(String[] args) {
    Tolls tolls = new Tolls();
    int today = 20, tomorrow = 40;
    System.out.println(today + tolls.tomorrow + tolls.yesterday);
  }
}
