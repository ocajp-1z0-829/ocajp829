package com.ocajp.webinar6;

//Fields and blocks are run first in order, setting number
//to 3 and then 4. Then the constructor runs, setting number to 5
public class Egg {
  public Egg() {
    number = 5;//3
  }
  public static void main(String[] args) {
    Egg egg = new Egg();
    System.out.println(egg.number);//4, result is 5
  }
  private int number = 3; //1
  { number = 4; } //2
}
