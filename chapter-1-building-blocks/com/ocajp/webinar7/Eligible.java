package com.ocajp.webinar7;

/**
 * It is difficult to say how many objects are eligible for garbage collection when
 * control reaches (1), because some of the eligible objects may have already been
 * finalized.
 */
public class Eligible {
  public static void main(String[] args) {
    for (int i = 0; i < 5; i++) {
      Eligible obj = new Eligible();
      new Eligible();
    }
    System.gc();
  }
}
