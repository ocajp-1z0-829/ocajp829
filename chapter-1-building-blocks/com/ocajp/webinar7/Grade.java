package com.ocajp.webinar7;

/**
 * It is not guaranteed if and when garbage collection will occur, nor in which order
 * the objects will be finalized. However, it is guaranteed that the finalization of an
 * object will be run only once. Hence, (e) cannot possibly be a result from running
 * the program.
 */
public class Grade {
  private char grade;

  public Grade(char grade) {
    this.grade = grade;
  }

  @Override
  public void finalize() throws Throwable {
    System.out.println(grade);
    super.finalize();
  }

  public static void main(String[] args) {
    new Grade('A'); new Grade('F');
    System.gc();
  }
}
