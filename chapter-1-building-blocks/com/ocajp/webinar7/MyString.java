package com.ocajp.webinar7;

/**
 * It is not guaranteed if and when garbage collection will occur, nor in which order
 * the objects will be finalized. However, it is guaranteed that the finalization of an
 * object will be run only once. Hence, (e) cannot possibly be a result from running
 * the program.
 */
public class MyString {
  private String str;

  public MyString(String str) {
    this.str = str;
  }

  @Override
  protected void finalize() throws Throwable {
    System.out.println(str);
    super.finalize();
  }

  public void concat(String str2) {
    this.str.concat(str2);
  }

  public static void main(String[] args) {
    new MyString("A").concat("B");
    System.gc();
  }
}
