package com.ocajp.webinar7;

/**
 * Сначала поля, статические поля и статические блоки инициализации по порядку
 * Затем нестатические блоки инициализации по порядку при вызове экземпляра класса
 * Затем конструктор
 */
public class Initialization {
  public static void main(String[] args) {
    System.out.println(b);
    System.out.println(a);
    System.out.println(new Initialization().b); //without it b will be 1, but not 3
    System.out.println(Initialization.b);
  }

  //int b = a++;

  static int a;

  {
     //b++;
  }

  {
    b++; //b = 1 + 1; System.out.println(new Initialization().b); //step 1
  }

  static int b = ++a; //a = 1; b = 1

  {
    b++; //b = 2 + 1; System.out.println(new Initialization().b); //step 2
  }
}
