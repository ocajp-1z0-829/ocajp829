package com.ocajp.webinar2;

import java.util.HashMap;
import java.util.Map;

public class Lesson2 {
  public static void main2(String[] args) {
    String name = "/* Harry */ Paul";
    System.out.println(name);

    String name2 = /* Harry */ "Paul";
    System.out.println(name2);

    //error
    //    String name3 = "Shre /* ya
    //                      */ Paul";
    //    System.out.println(name3);

    /*
     * // anteater
     */
    // bear
    // // cat
    // /* dog */
    /* elephant */
    /*
     * /* ferret */
    //     */ //error occurred if uncomment */

    //System.out.print(); //such method without params does not exist
    System.out.printf("% 6d", 3); //works formatter
  }

  public final static void main(final String[] args) {
    System.out.println("final success");
  }

}
