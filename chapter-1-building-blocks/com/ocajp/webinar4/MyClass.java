package com.ocajp.webinar4;

public class MyClass {
  public static void main(String[] args) {
    String a, b, c;
    c = new String("mouse");
    a = new String("cat");
    b = a;
    a = new String("dog");
    c = b;
    System.out.println("a " + a);
    System.out.println("b " + b);
    System.out.println("c " + c);
  }
}
