package com.ocajp.webinar4;

public class Test {
  public static void main(String[] args) {
    int[] arr = null;
    System.out.println(
        arr); //Переменная arr ссылочного типа (ведь массивы относятся к типу Object), так что ее вполне допустимо
    // сбросить в null.

    System.out.println(2 / 3 + 5 / 7);
    System.out.println(0x10 * 1L * 300.0);

    float f = -1232342342.21f;
    double d = -1232342342.33333d;
    System.out.println(f + " " + d);

  }
}

class NegativeModulo {
  static void getModulo(float f) { // он является double по умолчанию
    System.out.println(-f % -f);
  }

  public static void main(String[] args) {
    //getModulo(2.0); //ведь литералы с плавающей точкой имеют тип double по умолчанию
    getModulo(2.0f); //-0.0
    getModulo((float) 2.0); //-0.0
  }
}