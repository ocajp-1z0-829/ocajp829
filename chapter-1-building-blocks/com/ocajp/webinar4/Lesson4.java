package com.ocajp.webinar4;

public class Lesson4 {
  public static void main(String[] args) {
    //return, goto, default //keywords in java
    System.out.println(0x10 + 10 + 010 + 0b10);

    //---
    double d1 = .2e2; //20.0
    double d2 = 2.e2; //200.0
    double d3 = 2.0e2; //200.0
    double d4 = (long) 2.e2; //200.0
    //double d5 = (Long) 2.e2; //200.0 //cannot cast double to long 2-этапное преобразование
    double d7 = 0E0D; //0.0 (0 e в степени 0, е^0 = 1)
    int d8 = 0XE0D; //3597 в десятеричной

    System.out.println(d1 + " " + d2 + " " + d3 + " " + d4 + " " + d7 + " " + d8);

    //--- Suffixes

    double c1 = 010.f; //10.0
    double c2 = 010f; //10.0
    double c3 = 010; //8.0
    System.out.println(c1 + " " + c2 + " " + c3);

    //--- Borders

    System.out.println(-10.0D / 0.0);
    System.out.println(10.0D / 0.0);
    System.out.println(0.0D / -0.0);
    System.out.println(0.0D / 0.0);

    // Octal

    double x1 = 01234.; //01234.0
    double x2 = 01234; //668.0
    //int nine = 09; // there is not in octal system (Integer number too large)
    //int eight = 08; // there is not in octal system (Integer number too large)
    System.out.println(x1 + " " + x2);

    //Underscores
    double d = 0_1234.567;
    System.out.println(d);

    //----

    int _5 = 10; //10
    int x = _5; //10

    System.out.println(_5 + " " + x);

    // Symbols

    char ch1 = '\u0000';
    char ch2 = '\uffff';
    int ach1 = ch1;
    int ach2 = ch2;
    System.out.println(ch1 + " " + ach1 + " " + ch2 + " " + ach2);

    //ch\u0061r a = 'a'; //a компилятор примет
    //char \u0061 = 'a'; //a компилятор примет


  }

}
