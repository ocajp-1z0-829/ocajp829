package com.ocajp;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class StringBlock1 {

  public static void main(java.lang.String[] args) {
    String eyeTest = "\"Java Study Guide\"\n by Scott &\sJeanne";
    //String eyeTest2 = "\"""Java \ Study Guide\"\n by Scott &\sJeanne"; //not works cause of """ and \
    System.out.println(eyeTest);

    //Start text block
    String textBlock = """
        "Java Study Guide"
            by Scott & Jeanne""";
    System.out.println(textBlock);

    String pyramid = """
          *
         *\s*
        * * *
        """;
    System.out.println(pyramid);

    String pyramid2 = """
          *
         *\s*\
        * * *""";
    System.out.println(pyramid2); //* ** * * Omits new line on that line

    //
    //String block = """doe"""; //new line after """ is important
    String block = """
        doe""";
    System.out.println(block);

    //
    String block2 = """
        doe \
        deer""";
    System.out.println(block2); //doe deer

    //
    String block3 = """
        doe \n
        deer
        """;
    System.out.println(block3); //4 lines \n makes one more

    //
    String block4 = """
        "doe\"\"\"
        \"deer\"""
        """;
    System.out.print("*" + block4 + "*"); // if using """ inside it is not works, only works with \"""


  }
}

class KitchenSink {
  private static int numForksStatic;
  private int numForks;

  public static void main(String[] args) {
    int numKnives;
    /**
     * The output includes: # cups = 0.
     * The output includes one or more lines that begin with whitespace.
     */
    System.out.printf("""
        "# forks = " + numForks +
        "# forks = " + %s +
        " # knives = " + numKnives +
        # cups = 0""", numForksStatic);

    System.out.println();

    /**
     * There are two lines. One starts with squirrel, and the other
     * starts with pigeon. Remember that a backslash means to skip the line break. Option D
     * is also correct as \s means to keep whitespace. In a text block, incidental indentation is
     * ignored, making option F incorrect.
     */
    var blocky = """
        squirrel \s
        pigeon \
        termite""";
    System.out.print(blocky);
  }
}

class TestMap {
  int id;
  int test;
  String name;

  public int getId() {
    return id;
  }

  public int getTest() {
    return test;
  }

  public String getName() {
    return name;
  }

  @Override
  public String toString() {
    return "TestMap{" + "id=" + id + ", test=" + test + ", name='" + name + '\'' + '}';
  }
}

class Start {
  public static void main(String[] args) {
    List<TestMap> testMapList = new ArrayList<>();
    TestMap testMap = new TestMap();
    testMap.id = 1;
    testMap.test = 2;
    testMap.name = "Name 1";
    testMapList.add(testMap);

    TestMap testMap1 = new TestMap();
    testMap1.id = 10;
    testMap1.test = 20;
    testMap1.name = "Name Name 10000";
    testMapList.add(testMap1);

    TestMap testMap2 = new TestMap();
    testMap2.id = 100;
    testMap2.test = 200;
    testMap2.name = "Name Name NAme 10000000";
    testMapList.add(testMap2);

    Map<Integer, List<TestMap>> integerListMap = testMapList.stream()
        .collect(Collectors.groupingBy(TestMap::getId));

    System.out.println(integerListMap);
  }
}