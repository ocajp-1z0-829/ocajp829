package com.ocajp;

public class Lesson8 {
  public void findAnswer(boolean check) {
    int answer;
    int otherAnswer;
    int onlyOneBranch;
    if (true) {
      onlyOneBranch = 1;
      answer = 1;
    } else {
      answer = 2;
    }
    System.out.println(answer);
    System.out.println(onlyOneBranch); // DOES NOT COMPILE except true value in if
  }
}
