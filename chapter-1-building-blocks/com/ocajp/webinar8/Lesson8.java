package com.ocajp.webinar8;

import java.util.ArrayList;
import java.util.Iterator;

public class Lesson8 {
  static Byte aByte = new Byte("10");
  static Boolean aBoolean = new Boolean("true");
  static Boolean bBoolean = new Boolean(Boolean.parseBoolean("true"));
  static Double aDouble = new Double("10.98");
  static Float aFloat = new Float("10");
  static int i = new Integer("1").intValue(); //int
  static int i2 = Integer.parseInt("1"); //int

  //
  static Integer integer1 = new Integer(10);
  static Integer integer2 = new Integer(10);
  static Integer integer3 = Integer.valueOf(128); // > -128... 127
  static Integer integer4 = Integer.valueOf(128); // > -128... 127

  //
  static ArrayList list = new ArrayList();

  public static void main(String[] args) throws InstantiationException, IllegalAccessException {
    Character character = new Character('a');
    character.charValue();
    character.compareTo('b');

    Boolean bool = new Boolean("true");
    bool.booleanValue();

    //Void voidWrapper = Void.TYPE.newInstance();
    //System.out.println(voidWrapper);

    final int yF = 10;
    int xNotF = 20;
    //yF = xNotF + 10; //final is constant
    //
    final int[] favoriteNumbers = new int[10];
    favoriteNumbers[0] = 10;
    favoriteNumbers[1] = 20;
    //favoriteNumbers = null; // DOES NOT COMPILE //final is constant

    //
    boolean b10, b11;
    String s10 = "1", s11;
    //double d10, double d11; //not working
    int i10;
    int i11;
    //int i12; i13; //not working

    System.out.println(integer1 == integer2);
    System.out.println(integer3 == integer4);
    System.out.println();
    //
    list.add(new Double(12.12D));
    list.add(new Double(24.24F));
    Double total = Double.valueOf(0.0D);
    for (Iterator iterator = list.iterator(); iterator.hasNext(); ) {
      Double d = (Double) iterator.next();
      total = total.doubleValue() + d.doubleValue();
    }
    System.out.println(total);
    //

    System.out.println();
    System.out.println("other:");
    System.out.println(aByte);
    System.out.println(aBoolean);
    System.out.println(aDouble);
    System.out.println(aFloat);
    System.out.println(i);
    System.out.println(i2);
    System.out.println(bBoolean);

    //
    System.out.println();
    Byte b2 = Byte.valueOf("1010", 2); //8+2=10
    Short sh8 = Short.valueOf("12", 8); //8*1+2*1=10
    Integer int16 = Integer.valueOf("-a", 16); //A = 10, B = 11, C = 12, D = 13, E = 14, F = 15
    Long l16 = Long.valueOf("-a", 16); // -10
    System.out.println(b2 + " " + sh8 + " " + int16 + " " + l16);
    System.out.println(int16.doubleValue());
    System.out.println();
    //
    Float.valueOf((float) 1.0);
    //Float.valueOf(1.0); //not working f needed
    new Float(1.0);
    //
    System.out.println(Integer.sum(1, 2));
    System.out.println(Integer.max(1, 2));
    System.out.println(Integer.min(1, 2));

    //
    //int num, String value; // not working
    //
    System.out.println();
    System.out.println(Integer.toBinaryString(-41));
    System.out.println(Integer.toBinaryString(41));
    System.out.println(Integer.toString(-41, 2));
    System.out.println(Integer.toString(41, 2));
  }
}

class Q {
  String str = "null";
  int a = 12;

  public Q(String String) {
    this.str = String;
  }

  public Q(int a) {
    this.a = a;
  }

  public static void main(String[] args) {
    new Q("Hi!").println();
    new Q(123).println();
  }

  void println() {
    System.out.print(str + " " + a + " ");
  }


}

class ObjectFields {
  static int cVar;
  int iVar;

  public static void main(String[] args) {
    ObjectFields of = new ObjectFields();
    of.iVar = 100;
    ObjectFields.cVar = 200;
    System.out.println("iVar " + of.iVar + ", cVar " + of.cVar);
    //iVar = 200; //не статический контекст компилятор не понимает
    cVar = 300;
    //this.iVar = 200; //psvm не привязан ни к какому объекту, непонятно какого объекта
    //this.cVar = 400;
    System.out.println("iVar " + of.iVar + ", cVar " + of.cVar);
    of.iVar = 100;
    of.cVar += 200;
    ObjectFields.cVar += 300;
    System.out.println("iVar " + of.iVar + ", cVar " + of.cVar);
    of.setFields();
    System.out.println("iVar " + of.iVar + ", cVar " + of.cVar);
  }

  void setFields() {
    this.iVar = 22;
    this.cVar = 22;
  }
}

