package com.ocajp.webinar8;

public class Test {

  public void a() {
    a = 2;
    System.out.println(a);
  }

  public Test(int a) {
    this.a = a;
  }

  int a;

  public static void main(String[] args) {
    Integer integer = new Integer(1);
    int i = new Integer(1);
    double d = 5.0f;
    new Test(1).a();


    int Amount1 = 0b11;
    //int amount2 = 9L;
    int amount3 = 0xE;
    //int amount4 = 1_2.0;
    //double amount5 = 1_0_.0;
    int amount6 = 0b101;
    double amount7 = 9_2.1_2;
    //double amount8 = 1_2_.0_0
  }
}

class Parser {
  String input = "2013";

  public static void main(String[] args) {
    Parser p = new Parser();
    p.parseMe("0123");
  }

  public void parseMe(String str) {
    int output = 0;
    try {
      String input = str;
      output = Integer.parseInt(input);
    } catch (IllegalArgumentException e) {
      System.out.println("Wrong argument!");
    }
    System.out.println("input: " + input + ", output: " + output);
  }
}
