package com.ocajp.webinar8;

/**
 * Which of the following is the output of this code, assuming it runs to completion
 * If there was a finalize() method, this would be a different story. However, the
 * method here is finalizer. Tricky! That’s just a normal method that doesn’t get called
 * automatically. Therefore clean is never output.
 */
public class Toy {
  public static void main(String[] args) {
    Toy car = new Toy();
    car.play();
    System.gc();
    Toy doll = new Toy();
    doll.play();
  }

  public void finalizer() {
    System.out.print("clean-");
  }

  /**
   * Remember that garbage collection is not guaranteed to run on demand. If it doesn’t
   * run at all, Option play-play- would be output. If it runs at the requested point, Option play-play-clean- would be
   * output. If it runs right at the end of the main() method, Option play-play-clean-clean- would be output. Option
   * A is the correct answer because play is definitely called twice. Note that you are unlikely
   * to see all these scenarios if you run this code because we have not used enough memory for
   * garbage collection to be worth running. However, you still need to be able to answer what
   * could happen regardless of it being unlikely.
   *
   */
  @Override
  public void finalize() {
    System.out.print("clean-");
  }

  public void play() {
    System.out.print("play-");
  }
}
