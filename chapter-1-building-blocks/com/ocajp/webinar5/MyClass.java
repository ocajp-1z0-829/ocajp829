package com.ocajp.webinar5;

public class MyClass {
  public static void main(String[] args) {
    char c;
    int i;
    c = 'a';
    c++;
    i = c;
    i++;
    //c = i; //explicit (char) needed
    c++;
  }
}
