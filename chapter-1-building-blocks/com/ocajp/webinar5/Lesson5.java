package com.ocajp.webinar5;

public class Lesson5 {

  static int x1;  static int x, y = x + 1, z; //будет работать

  public static void main(String[] args) {
    //int x, y = x + 1, z; //не будет работать
    //int j, k = m + 3, l, m = 1; //не будет работать

    int j, k = 1, l, m = k + 3;

    int y = 10;
    //int x1; //если в методе то нужно инициализировать
    int res = y + x1;

    //int i = 129L; //implicit не работает для long, float, double
    //char ch = 30L; //не работает

    //---
    byte a = 3;
    byte b = 8;
    //byte c = a + b; //не работает думает int переменная, исключение final константы использовать
    byte c = (byte) (a + b); //с кастами работает

    final byte ab = 3;
    final byte bb = 8;
    byte cb = ab + bb; //c final константами работает но должен удовлетворять range

    if (5==5.0f) { // 5 расширяется до float, и получается 5.0
      System.out.println(true);
    }

    if (5==5.0) { // 5 расширяется до double, и получается 5.0
      System.out.println(true);
    }

    byte b3 = 3;
    //b3 = b3 + 7; //не работает надо b3 = (byte) (b3 + 7);
    b3 += 7; //в составном операторе заложен под капотом каст. Это равно b3 = (byte) (b3 + 7);

    long e = 123;
    short e2 = 123;
    e = e2;


    // primitive type never throw cast exception
    // References throws cast exception ClassCastException

    Object obj = "Upcast me"; //Widening Object <-- String
    String str = (String) obj; //Norrowing req cast: String <-- Object
    System.out.println(str);

    System.out.println("Hello".equals((String) null));


  }




}
