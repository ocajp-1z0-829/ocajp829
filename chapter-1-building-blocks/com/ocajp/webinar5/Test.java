package com.ocajp.webinar5;

import java.util.Arrays;

public class Test {
  public static void main(String[] args) {
    String[][] astr = new String[2][];
    System.out.println(Arrays.toString(astr));
    astr[0] = new String[2];
    System.out.println(Arrays.toString(astr));
    astr[1] = new String[3];
    System.out.println(Arrays.toString(astr));
    int a = 42;
    for (int i = 0; i < astr.length; i++) {
      for (int j = 0; j < astr.length; j++) {
        System.out.println(astr[i][j]);
        astr[i][j] = "" + a;
        a++;
      }
    }

    for (String[] e : astr) {
      for (String c : e) {
        System.out.print(c + " ");
      }
      System.out.println();
    }
  }
}

class Square {
  final static float squareIt (String str) {
    //return (float) parseDouble(str) * parseDouble(str); //second parseDouble(str) must be cust to float
    return 0.0f;
  }
  public static void main(String[] args) {

    System.out.println(new Square().squareIt("2.0"));

    double[] darr = {11, 22.0, 'a', 33.0f};
    System.out.println(Arrays.toString(darr));
  }
}

class Choises {
  private boolean flag = true;
  private int number;

  public void show() {
    while (flag) {
      if (number % 2 == 0) {
        number += 4;
        System.out.println(number + " ");
      } else {
        number *= 2;
        System.out.println(number + " ");
      }
      if (number > 16) {
        break;
      }
    }
  }

  public static void main(String[] args) {
    new Choises().show();
  }
}