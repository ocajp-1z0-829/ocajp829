package com.ocajp;

/**
 * Local variable type inference works with local variables and not instance variables.
 * Вывод типа локальной переменной работает с локальными переменными, а не с переменными экземпляра.
 */
public class VarVariables {
  //var name = "Hello"; // DOES NOT COMPILE
  public static void main(String[] args) {
    //var question; // DOES NOT COMPILE
    //question = 1; // DOES NOT COMPILE

    //var day = 1/0; // by zero

    var num1 = Long.parseLong("100");
    var num2 = Long.valueOf("100");
    System.out.println(Long.max(num1, num2));

    /**
     * The compiler is being asked to infer the type of null. This could be any reference type
     */
    //var n = null; // DOES NOT COMPILE

  }


  /**
   * Be on the lookout for var used with constructors, method parameters, or instance variables
   * var is only used for local variable type inference
   */
  //public int addition(var a, var b) { // DOES NOT COMPILE
  //return a + b;
  //}
}


class Zoo {
  public void whatTypeAmI() {
    var name = "Hello";
    var size = 7;
  }

  public void reassignment() {
    var number = 7;
    number = 4;
    //number = "five"; // DOES NOT COMPILE

  }
}

class Var {
  public void var() {
    var var = "var";
  }

  public void Var() {
    Var var = new Var();
  }
}

class PoliceBox {
  String color;
  long age;

  public static void main(String[] time) {
    var p = new PoliceBox();
    var q = new PoliceBox();
    p.color = "green";
    p.age = 1400;
    p = q;
    System.out.println("Q1=" + q.color);
    System.out.println("Q2=" + q.age);
    System.out.println("P1=" + p.color);
    System.out.println("P2=" + p.age);
  }

  public void PoliceBox() {
    color = "blue";
    age = 1200;
  }
}

class Salmon {
  int count;

  {
    System.out.print(count + "-");
  }

  {
    count++;
  }

  public Salmon() {
    count = 4;
    System.out.print(2 + "-");
  }

  public static void main(String[] args) {
    System.out.print(7 + "-");
    var s = new Salmon();
    System.out.print(s.count + "-");
  }

}