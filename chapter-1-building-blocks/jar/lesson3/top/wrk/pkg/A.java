//javac -cp . pkg/A.java or javac -cp .:pkg pkg/A.java
package pkg;
class A {
	B b;
	public static void main(String... args) {
		System.err.println("Class A using B. Test lesson 3");
	}
}