package com.ocajp.webinar3while;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Test {
  public static void main(String[] args) {
    char[] wolf = {'W', 'e', 'b', 'b', 'y'};

    System.out.println("A");
    int q = wolf.length;
    for (; ; ) {
      System.out.print(wolf[--q]);
      if (q == 0)
        break;
    }

    System.out.println("\nB");
    for (int m = wolf.length - 1; m >= 0; --m)
      System.out.print(wolf[m]);

    System.out.println("\nC");
    //for (int z = 0; z< wolf.length;z++)
    //System.out.print(wolf[wolf.length-z]); //java.lang.ArrayIndexOutOfBoundsException: 5

    System.out.println("\nD");
    int x = wolf.length - 1;
    for (int j = 0; x >= 0 && j == 0; x--)
      System.out.print(wolf[x]);

    System.out.println("\nE");
    final int r = wolf.length;
    //for (int w = r-1; r>-1; w=r-1) //infinity
    //System.out.print(wolf[w]);

    System.out.println("\nF");
    //           step 1     step 2
    //for (int i = wolf.length; i>0; --i) //java.lang.ArrayIndexOutOfBoundsException: 5
    //step 3
    //System.out.print(wolf[i]);
  }

}

class Q1 {
  public static void main(String[] args) {
    Map<String, String> stringStringMap = new HashMap<>();
    //for(Map.Entry e : stringStringMap) {} //foreach not applicable to type 'java.util.Map<String,String>'
  }
}

class Q22 {
  public static void main(String[] args) {
    String[] arr1 = new String[4];
    System.out.println(Arrays.toString(arr1));
    String[] arr2 = {"a", "b", "c", "d", null};
    arr1 = arr2;
    for (String e : arr1) {
      System.out.print(e + " : ");
    }
  }
}
