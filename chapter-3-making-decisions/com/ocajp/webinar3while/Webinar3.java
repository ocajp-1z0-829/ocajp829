package com.ocajp.webinar3while;

/**
 * False константы и литералы false в цикле while дадут comperr т.к. недостижимый код
 */
public class Webinar3 {
  public static void main(String[] args) {
    boolean b1, b2;
    int a = 0;
    while (b1 = b2 = false) {
      System.out.println("test1");
      break;
    }
    while (!!true) {
      System.out.println("test2");
      break;}
    //while (!true) {break;} //Unreachable statement
    while (a == 0 ?false:true) {
      System.out.println("test3");
    }
    while (new TestInner().equals("?!") ); //&& new Test() == "?!" //Comp error
    while (new TestInner().equals("?!")) {
      System.out.println("test4");
    }
  }
}

class DoWhile {
  public static void main(String[] args) {
    do {
      System.out.println("At least once");
    }
    while (false);

    do
      ;//'while' expected без ; нельзя. 2 ;; тоже запрещены
    while (false);

    do
      ;//boolean y = true; //Declaration not allowed here
    while (false);

    do {
      boolean y = true;
    }
    while (false);

    boolean x = false;
    do
      x = true;
    while (false);

    //do {
      //boolean y = true;
    //}
    //while (y); //out of scoup

    int i = 10;
    do
      System.out.println(i++);
    while (i<15);
    System.out.println();
    do
      System.out.println(++i);
    while (i<20);
  }

}

class For {
  public static void main(String[] args) {
    int i, j, s, count = 10;
    for (i = 0, j = 1, System.out.println("This is legal!"); i < 3 && j < 4; i++, j++) {
      System.out.println("loop " + i + " " + j);
    }

    for (int l = 0; l < 3; l++);
    for (int l = 0; l < 3; l++) {}

    //for (System.out.println("Not working"), int l = 0; l < 3; l++) {} //not legal sout

    System.out.println("---");
    i = 0; s = 0; count = 10;
    for (i = 0, s = 0; i < count; i++) {s+=i;}
    System.out.println(s);
    i = 0; s = 0; count = 10;
    for (i = 0, s = 0; i < count; s+=i, i++);
      System.out.println(s);
    i = 0; count = 10;
    for (i = 0; i < count; s+=i++)
      System.out.println(s);
    i = 0; s = 0; count = 10;
    for (;i < count; s+=i){i++;}
      System.out.println(s);
    i = 0; s = 0; count = 10;
    for (; i < count;){s+=i;i++;}
      System.out.println(s);
    i = 0; s = 0; count = 10;
    for (; ; ) {s+=i++; if (i>count) break;}
      System.out.println(s);
    System.out.println();


    for (int c = 0; c<0 ; c++) {}

    //      step 1      step 2,5   step 4
    for (boolean b = true;b; b = !b) {
      //3
      System.out.println(b);
    }
  }
}

/**
 * a
 * Happy 1
 * b
 * Happy 2
 */
class ForIncrementStatements {
  public static void main(String[] args) {
    String line = "ab";
    //    шаг 1      шаг 2,5,8            шаг 4,7
    for (int i = 0; i < line.length(); ++i, printMethod(i)) {
      //шаг 3,6
      System.out.println(line.charAt(i));
    }
  }
  private static void printMethod(int i) {
    System.out.println("Happy " + i);
  }
}

class Infinity {
  public static void main(String[] args) {
    for (int i = 0; i < 0; i++)
      i = 0;

    //for (; ; )
      System.out.println(";true;"); // после него все Unreachable statement

    for (int l=1; l < 10; l++)
      l--;

    for (int k=0; k<10;)//int k=0; k<10; тут ничего не делает
      k++; //тут увеличивает и infinity не происходит
  }
}

class TestInner {

}
