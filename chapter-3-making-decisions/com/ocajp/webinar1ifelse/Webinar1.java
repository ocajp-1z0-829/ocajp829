package com.ocajp.webinar1ifelse;

public class Webinar1 {
  static int hourOfDay = 15;

  public static void main(String[] args) {
    if (hourOfDay < 15) {
      System.out.println("Good afternoon");
    } else if (hourOfDay < 11) {
      System.out.println("Good morning");
    } else {
      System.out.println("Good evening");
    }

    if (1 > 0)
      //String str1 = "1 > 0"; //Declaration not allowed here. Смысла нет говорит компилятор

      if (1 > 0) {
        String str1 = "1 > 0";
      } else {
        String str2 = "?!";
      }

    System.out.println("Break можно использовать с label и block");

    label:
    if (true)
      break label; //работает только с лейблами
    //if (true) break; //Break outside switch or loop

    int i = 1, j = 1;
    label:
    if (j > 0) {
      if (i > 0)
        break label;
      i++;
    }

    block:
    {
      break block;
      //i++; //Unreachable statement
    }

    if (false)
      ;
    else
      ;

    i = 1;
    label:
    {
      i++;
      {
        if (i > 0) break label;
        i++;
      }
      i++;
    }
    System.out.println(i);//2

    i = 1;
    j = 1;
    label:
    {
      i++;
      if (j == 0){
        if (i > 0) break label;
        i++;
      }
      i++;
    }
    System.out.println(i); //3
  }


}

class Test {
  public static void main(String[] args) {
    String s = "";
    boolean b1 = true;
    boolean b2 = false;
    if ((b2 = false) | (21%5) > 2) s+="x"; // false | 1 > 2
    if (b1 || (b2 = true)) s+="y"; // только b1 проверяет, дальше не идет
    if (b2 == true) s += "z"; // false
    System.out.println(s); //y
  }
}

class Spock {
  public static void main(String[] args) {
    System.out.println(10%2.3);
    System.out.println(goHome());
    int mask = 0;
    int count = 0;
    if ( ((5<7) || (++count < 10)) | mask++ < 10 ) mask = mask+1; //true | true
    if ( (6 > 8) ^ false) mask = mask+10; //false^false = false
    if ( !(mask > 1) && ++count > 1) mask = mask+100; //false и дальше не проверяет
    System.out.println(mask + " " + count); //2, 0 новая строка не относится к if
  }

  static int goHome() {
    try {
      System.out.println("1");
      return -1;
    } catch (Exception e) {
      System.out.println("2");
      return -2;
    } finally {
      System.out.println("3");
      return -3;
    }
    //return 55; //недостижимый код
  }
}