package com.ocajp;

import java.util.List;

public class PatternMatching {

  public static void main(String[] args) {
    compareIntegers(4);
    printIntegersGreaterThan5(6);
    printOnlyIntegers(25);

    /**
     * The type of the pattern variable must be a subtype of the variable on the left side of the
     * expression. It also cannot be the same type
     */
    Integer value = 123;
    if (value instanceof Integer) {
    }
    //if(value instanceof Integer data) {} // DOES NOT COMPILE

    Number num = 124;
    if (num instanceof List) {
    }
    if (num instanceof List data) {
    }


  }

  /**
   * If the input does not inherit Integer, the data variable is undefined. Since the compiler
   * cannot guarantee that data is an instance of Integer, data is not in scope, and the code
   * does not compile
   * @param number
   */
  static void printIntegersOrNumbersGreaterThan5(Number number) {
    //if(number instanceof Integer data || data.compareTo(5)>0)
      //System.out.print(data);
  }

  static void printIntegerTwice(Number number) {
    if (number instanceof Integer data)
      System.out.print(data.intValue());
    //System.out.print(data.intValue()); // DOES NOT COMPILE
  }

  /**
   * The
   * method returns if the input does not inherit Integer. This means that when the last line of
   * the method is reached, the input must inherit Integer, and therefore data stays in scope
   * even after the if statement ends.
   * @param number
   */
  static void printOnlyIntegers(Number number) {
    if (!(number instanceof Integer data))
      return;
    System.out.println();
    System.out.println();
    System.out.println();
    System.out.println(data.intValue());

    //second explanation
    if (!(number instanceof Integer data2))
      return;
    else
      System.out.println();
    System.out.println(data2.intValue());

    if (number instanceof Integer data3)
      System.out.println(data3.intValue());
    else
      System.out.println();
    System.out.println();
    return;

  }


  //new
  static void compareIntegers(final Number number) {
    if (number instanceof Integer data) {
      System.out.print(data.compareTo(5));
    }

    if (number instanceof Integer data) {
      data = 10;
      System.out.println(data);
    }

    if (number instanceof final Integer data) {
      //data = 10; // DOES NOT COMPILE
    }

  }

  static void printIntegersGreaterThan5(Number number) {
    if (number instanceof Integer data && data.compareTo(5) > 0) {
      System.out.print(data);
    }
  }

}

class BeforePatternMatching {
  public static void main(String[] args) {

  }

  //old
  static void compareIntegers(Number number) {
    if (number instanceof Integer) {
      Integer data = (Integer) number;
      System.out.print(data.compareTo(5));
    }
  }
}
