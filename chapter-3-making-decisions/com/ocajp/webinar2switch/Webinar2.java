package com.ocajp.webinar2switch;

import java.time.DayOfWeek;

public class Webinar2 {
  public static void main(String[] args) {
    String day = "SUN";
    switch (day) {}

    int month = 3;
    switch (month) {
      case 1 | 2: //побитовое сложение
        System.out.println(1 | 2);
    }

    byte b = -128;
    switch (b) {
      //case 128: //Provided: int
      case (byte) 128:
        System.out.println("!");
    }

    short size = 4;
    final int small = 15;
    final int big = 1_000_000; //проверяется на этапе компиляции

    switch (size) {
      case small:
      case 1+2:
      //case big: //Provided: int слишком большое значение не влазит в short
    }
  }
}

/**
 * I am not OK.
 * You are OK.
 * It's OK.
 * 2
 * 2
 */
class Test {

  private DayOfWeek getWeekDay(int day, final int thursday) {
    int otherDay = day;
    int Sunday = 0;
    switch (otherDay) {
      default:
      //case 1: continue; //Continue outside of loop. Не поддерживается в кейсах
      //case thursday: return DayOfWeek.THURSDAY; //Constant expression required
      case 2: break;
      //case 2: break; //дубли запрещены
      case 3: return DayOfWeek.SUNDAY;
      //case Sunday: return DayOfWeek.SUNDAY; //Constant expression required
      //case DayOfWeek.MONDAY: return DayOfWeek.MONDAY; //Provided: DayOfWeek. Required type: int
    }
    return DayOfWeek.FRIDAY;
  }

  final static int a1 = 0;
  final static Integer i1 = 0;
  public static void main(String[] args) {
    int x = 777;
    StringBuilder str = new StringBuilder("Java");
    switch (x) {
      case -123: str.append("S");
      case a1: str.append("E");
      //case i1: str.append("8"); //Constant expression required, классы-оболочки не считаются константами времени компиляции – даже когда их помечают модификатором final
    }
    System.out.println(str);

    final int iLoc = 3;
    switch (6) {
      case 1:
      case iLoc:
      case 2 * iLoc:
        System.out.println("I am not OK.");
      default:
        System.out.println("You are OK.");
      case 4:
        System.out.println("It's OK.");
    }

    String instrument = "violin";
    final String CELLO = "cello";
    String viola = "viola";
    int p = -1;
    switch (instrument) {
      case "bass" : break;
      case CELLO: p++;
      default: p++;
      case "VIOLIN": p++;
      case "viola" : ++p; break;
    }
    System.out.println(p); //2 до break


    int a = 10;
    Float f = (float)(a % 4.d); //2.0
    Short s = (short)(int)(long)(float)(a % 4.d); //2

    System.out.println(s);
  }

  int test;
  int a = 10;
  final int getInt() {
    return 4;
  }
  int run() { return 42; }
  void switchMe(int firstName, final int lastName) {
    switch ( run() ) {}
    switch (test) {
      //case a: // Constant expression required. Должно быть final, потому что может поменять значение
      //case getInt(): //Constant expression required. не финальная переменная на этапе компиляции
      //case firstName: //Constant expression required
      //case lastName: //Constant expression required
      default:
    }

    final int aConstant = 1;
    final int b;
    b = 2;
    int x = 0;
    switch (x) {
      case aConstant: // ok
      //case b: // Constant expression required. Необходимо сразу инициализировать final переменную. Должна быть
        // константа на этапе компиляции
    }

    final String string = "String"; //литерал в свитче отработает корректно
    //final String string = new String("String"); //не отработает в switch так как на этапе компиляции не финал.
    // Constant expression required
    switch (string){
      case string:
        System.out.println(string);
    }
  }
}

/**
 * seven eight six
 * a is not a digit
 * two  five
 */
class Digits {
  public static void main(String[] args) {
    System.out.println(digitToString('7') + " " + digitToString('8') + " " + digitToString('6'));
    System.out.println(digitToString('2') + " " + digitToString('a') + " " + digitToString('5'));

  }

  public static String digitToString(char digit) {
    String str = "";
    switch (digit) {
      case '1': str = "one"; break;
      case '2': str = "two"; break;
      case '3': str = "three"; break;
      case '4': str = "four"; break;
      case '5': str = "five"; break;
      case '6': str = "six"; break;
      default: System.out.println(digit + " is not a digit"); //возвращает пустую строку, печатает текст
      case '7': str = "seven"; break;
      case '8': str = "eight"; break;
      case '9': str = "nine"; break;
      case '0': str = "zero"; break;
    }
    return str;
  }
}

class Switch {
  public static void main(String[] args) {
    int i = 1;
    label1:
    switch (i) {
      case 1:
        label2:
        if (i == 1) break label2;
        else System.out.println("else");
        System.out.println("i");
        break label1;
      default: System.out.println("default");
      case 4: {
        int x = 5;
      }
      case 2:
        int x = 1;
      case 3:
        //int x = 2; //нельзя если не брать в {} объявление переменной выше.
        //System.out.println(x); // также x = 1 он не увидит. Знает только что была объявлена переменная x и Variable
        // 'x' might not have been initialized
        x = 2;
        System.out.println(x);
    }
  }
}

class Switcheroo {
  public static void main(String[] args) {
    final String TOM1 = "Tom";
          String TOM2 = "Tom";
    final String TOM3 = new String("Tom");
    System.out.println('T' + 'o' + 'm' + TOM1); //304Tom
    switch ("TomTom") {
      default:
        System.out.println("Whatever!");
        break;
      case "TomTom": //работает
      //case TOM1 + TOM1: //работает
      //case "Tom" + TOM1: //работает
      //case "T" + 'o' + 'm' + TOM1: //работает

      //case "TomTom": //Duplicate label 'TomTom'
      //case TOM1 + TOM2: //Constant expression required
      //case TOM1 + TOM3: //Constant expression required
      //case TOM2 + TOM3: //Constant expression required
      //case "Tom" + TOM2: //Constant expression required
      //case "Tom" + TOM3: //Constant expression required
      //case 'T' + 'o' + 'm' + TOM1:
        System.out.println("Hi, TomTom!");
    }
  }
}

