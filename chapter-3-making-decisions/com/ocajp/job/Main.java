package com.ocajp.job;

import com.ocajp.job.abstractPack.AbstractInterfaceTestImpl;
import com.ocajp.job.impl.Impl;
import com.ocajp.job.inter.InterfaceTest;

public class Main { //sp100 3 v 1
  public static void main(String[] args) throws Exception {
    InterfaceTest interfaceTest = new AbstractInterfaceTestImpl();
    interfaceTest.updateMedia(null, "", new byte[] {111, 12});
  }
}
