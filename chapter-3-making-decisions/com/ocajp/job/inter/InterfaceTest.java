package com.ocajp.job.inter;

public interface InterfaceTest {
  default void updateMedia(final Object requestEntity, final String em, final byte[] mediaContent)
      throws Exception {}
}
