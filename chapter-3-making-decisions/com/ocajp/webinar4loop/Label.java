package com.ocajp.webinar4loop;

public class Label {
  public static void main(String[] args) {
    int frog = 15; //labels forbidden
    BAD_IDEA: if (frog>10);
    EVEN_WORSE_IDEA: {
      frog++;
    }
    TRY_CATCH_LABEL: try {
      label2: throw new NullPointerException();
    } catch (NullPointerException e) {
      break TRY_CATCH_LABEL;
    } finally {
    }

    //label: if (true) break; //Break outside switch or loop
    label: if (true) break label;
    label: for (;;) break label;
    label: switch (frog) {}
    label: frog = 42;
    label: frog++;
    getLabel();


    Label: label: if (true)
      for (int j = 0; j<10;j++) {
        //continue label; //Not a loop label: 'label'. continue не может использовать метку от if
        break label; // выйдет из if
        //break; //выйдет только из for
      }
      System.out.println("If");

      do {break;} while (true);
      if (true) {
        //break; //Break outside switch or loop
      }
      switch (1) {
        default: break;
      }
      for (;true;)break;

      //if (true) break; //Break outside switch or loop
      for (;true;) if(true) break;

      L1: if (frog > 0) {
       // L1: System.out.println(frog); //Label 'L1' already in use
      }
  }

  private static String getLabel() {
    label: return "Label!";
  }
}
