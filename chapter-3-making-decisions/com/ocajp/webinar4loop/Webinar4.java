package com.ocajp.webinar4loop;


public class Webinar4 {
  private static void run() {
    System.out.println("Сайт курса:");
    http://otus.ru
    return; //перед return разрешены метки. http: это метка, //otus.ru это комментарий
  }

  public static void main(String[] args) {
    run();

    int lizard = 0;
    do {
      lizard++;
    } while(false);
    System.out.println(lizard); // 1

    //while (false) System.out.println(); //Unreachable statement
    //while (false) {System.out.println();}

    int pen = 2;
    int pigs = 5;
    while(pen < 10) {
      pigs++;
      pen++;
    }
    System.out.println(pen + " pen and pigs " + pigs);

    int i = 0, j = 4;
    int x = 3, y =0, z=5;
    for (i = 0; i< x; i++) {
      do {
        int k = 0;
        while (k<z) {
          System.out.print(k + " ");
          k++;
        }
        System.out.println("");
        j--;
      } while (y>=j); //итого выполнится только 1 раз, итого доходит до значения 1
      System.out.println("-----------");
    }
  }
}
