package com.ocajp.webinar4loop;

public class Test {
  public static void main(String[] args) {
    int count = 0;
    BUNNY: for (int row = 1; row <= 3; row++)
      RABBIT: for (int col = 0; col < 3; col++) {
      if ((col + row) % 2 == 0)
        //break;
        //break RABBIT;
        continue BUNNY;
      count++;
      }
    System.out.println(count);

    System.out.println();
    count = 0;
    ROW_LOOP: for (int row = 1; row <=3; row++)
      for (int col = 1; col <= 2; col++) {
        if(row * col % 2 == 0) continue ROW_LOOP;
        count++;
      }
    System.out.println(count);
  }
}

class Tester {
  static int count = 0; //не сбрасывается результат
  public int count(int qty) {
    //if (qty <=0) break; // без label не сработает с if
    label: if (qty <=0) break label;
    else {
      for (int i =0; i<qty; i++) {
        count+=i;
      }
    }
    return count;
  }

  public static void main(String[] args) {
    Tester t = new Tester();
    System.out.print(t.count(5)); //10
    System.out.println(t.count(3)); //13

  }
}
