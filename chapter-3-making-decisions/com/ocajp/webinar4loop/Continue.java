package com.ocajp.webinar4loop;

public class Continue {
  public static void main(String[] args) {
    CLEANING: for (char stables = 'a'; stables<='d';stables++){
    for (int leopard = 1; leopard <4; leopard++) { //CLEANING переходит сюда
      if(stables == 'b' || leopard == 2) {
        continue CLEANING;
      }
      System.out.println("Cleaning: " + stables + "," +leopard);
    }
    }

    CLEANING: for (char stables = 'a'; stables<='d';stables++){
      for (int leopard = 1; leopard <4; leopard++) { //CLEANING переходит сюда
        if(stables == 'b' || leopard == 2) { //continue переводит сюда
          continue;
        }
        System.out.println("Cleaning: " + stables + "," +leopard);
      }
    }
    System.out.println();

    int[][] squareMatrix = {{4,3,5},{2,1,6},{9,7,8}};
    int sum = 0;
    outer: for (int i = 0; i<squareMatrix.length;++i) {
      for (int j = 0; j<squareMatrix[i].length; ++j) {
        if (j==i) continue;
        System.out.println("[" + i + ", " + j + "] : " + squareMatrix[i][j]);
        sum+=squareMatrix[i][j];
        if(sum>10) continue outer; //после 9 завершает цикл, возвращает в outer
      }
    }
    System.out.println("sum: " + sum);
  }
}
