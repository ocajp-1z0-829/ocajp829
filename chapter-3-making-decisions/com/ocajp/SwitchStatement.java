package com.ocajp;

/**
 * Notice that boolean, long, float, and double are excluded from
 * switch statements, as are their associated Boolean, Long, Float, and
 * Double classes. The reasons are varied, such as boolean having too
 * small a range of values and floating-point numbers having quite a wide
 * range of values
 */
public class SwitchStatement {
  public static void main(String[] args) {

    getCookies();

    Integer month = 5;
    switch(month) {}
    printDayOfWeek(month);
    System.out.println();

    printSeason(month);
    System.out.println();

    int animal = 1;
    switch(animal) {
      case 1,2: System.out.print("Lion");
      case 3: System.out.print("Tiger");
    }
    System.out.println();

    switch(animal) {
      case 1: case 2: System.out.print("Lion");
      case 3: System.out.print("Tiger");
    }
  }

  void doesNotCompile() {
    int month = 5;

    /**
     * The first switch statement does not compile because it is missing parentheses around the
     * switch variable.
     */
    //switch month { // DOES NOT COMPILE
      //case 1: System.out.print("January");
    //}

    /**
     * The second statement does not compile because it is missing braces around
     * the switch body
     */
    //switch(month) // DOES NOT COMPILE
      //case 1: System.out.print("January");

    /**
     *  The third statement does not compile because a comma (,) should be used
     * to separate combined case statements, not a colon (:).
     */
    switch(month) {
      //case 1: 2: System.out.print("January"); // DOES NOT COMPILE
    }

    final Car car = new Car();
    /**
     * Incompatible types. Found: 'com.ocajp.Car', required: 'char, byte, short, int, Character, Byte, Short, Integer, String, or an enum'
     */
    //switch (car) {}


    /**
     * Notice that the second case expression returns a short, but that can be implicitly cast
     * to an int. In this manner, the values have to be consistent with size, but they do not all
     * have to be the same data type. The last three case expressions do not compile because each
     * returns a type that cannot be assigned to the int variable
     */
    int measurement = 10;
    int size = switch(measurement) {
      case 5 -> 1;
      case 10 -> (short)2;
      default -> 5;
      //case 20 -> "3"; // DOES NOT COMPILE
      //case 40 -> 4L; // DOES NOT COMPILE
      //case 50 -> null; // DOES NOT COMPILE
    };

    int fish = 5;
    int length = 12;
    var name = switch(fish) {
      case 1 -> "Goldfish";
      case 2 -> {yield "dr";} // DOES NOT COMPILE
      case 3 -> {
        if(length > 10) yield "Blobfish";
        else {
        yield "f"; // DOES NOT COMPILE
      }}
      default -> "Swordfish";
      };


  }

  static public void printDayOfWeek(int day) {
    switch(day) {
      case 0:
        System.out.print("Sunday");
        break;
      case 1:
        System.out.print("Monday");
        break;
      case 2:
        System.out.print("Tuesday");
        break;
      case 3:
        System.out.print("Wednesday");
        break;
      case 4:
        System.out.print("Thursday");
        break;
      case 5:
        System.out.print("Friday");
        break;
      case 6:
        System.out.print("Saturday");
        break;
      default:
        System.out.print("Invalid value");
        break;
    } }

  static public void printSeason(int month) {
    switch(month) {
      case 1, 2, 3: System.out.print("Winter");
      case 4, 5, 6: System.out.print("Spring");
      default: System.out.print("Unknown");
      case 7, 8, 9: System.out.print("Summer");
      case 10, 11, 12: System.out.print("Fall");
    } }

  static final int getCookies() { return 4; }
  void feedAnimals() {
    final int bananas = 1;
    int apples = 2; //Constant expression required
    int numberOfAnimals = 3;
    final int cookies = getCookies(); //Constant expression required
    switch(numberOfAnimals) {
      case bananas:
      //case apples: // DOES NOT COMPILE, Constant expression required
      //case getCookies(): // DOES NOT COMPILE, Constant expression required
      //case cookies : // DOES NOT COMPILE, Constant expression required
      case 3 * 5 :
    } }
}

class SwitchExpression {
  enum Season {WINTER, SPRING, SUMMER, FALL}
  static String getWeather(Season value) {
    return switch(value) {
      case WINTER -> "Cold";
      case SPRING -> "Rainy";
      case SUMMER -> "Hot";
      case FALL -> "Warm";
    };
  }

  void printReptile(int category) {
    var type = switch(category) {
      case 1,2 -> "Snake";
      case 3,4 -> "Lizard";
      case 5,6 -> "Turtle";
      case 7,8 -> "Alligator";
      default -> throw new IllegalStateException("Unexpected value: " + category);
    };
    System.out.print(type);
  }

  public static void main(String[] args) {
    printDayOfWeek(1);
    System.out.println(getWeather(Season.SPRING));
  }
  public static void printDayOfWeek(int day) {
    var result = switch(day) {
      case 0 -> "Sunday";
      case 1 -> "Monday";
      case 2 -> "Tuesday";
      case 3 -> "Wednesday";
      case 4 -> "Thursday";
      case 5 -> "Friday";
      case 6 -> "Saturday";
      default -> "Invalid value";
    };
    System.out.print(result);

    /**
     * Semicolon is missing
     */
    //var result2 = switch("bear") {
      //case 30 -> "Grizzly" //;
      //default -> "Panda" //;
    //} //;

    var result2 = switch(22) {
      case 30 -> "Grizzly";
      default -> "Panda";
    };

    System.out.println();
    switch(1) {
      case 1, 2, 3 -> {
        System.out.print("Winter");
        break; //'break' statement is unnecessary
      }
      case 4, 5, 6 -> System.out.print("Spring");
      case 7, 8, 9 -> System.out.print("Summer");
      case 10, 11, 12 -> System.out.print("Fall");
    }

    /**
     * Ключевое слово yield эквивалентно оператору return в выражении switch и используется, чтобы избежать
     * двусмысленности относительно того, имели ли вы в виду выход из блока или метода вокруг
     * выражения switch.
     */
    System.out.println();
    int fish = 5;
    int length = 12;
    var name = switch(fish) {
      case 1 -> "Goldfish";
      case 2 -> {yield "Trout";}
      case 3 -> {
        if(length > 10) yield "Blobfish";
        else yield "Green";
      }
      default ->  {yield "Swordfish";}
    };
    System.out.println(name);
  }

}


class Car {}