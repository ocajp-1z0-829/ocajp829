package com.ocajp;

import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.List;

public class Test {
  public static void main(String[] args) {

    System.out.println();

    printType(1);
    System.out.println();

    for(var penguin : new int[2])
      System.out.println(penguin);
    var ostrich = new Character[3];
    for(var emu : ostrich)
      System.out.println(emu);
    List<Integer> parrots = new ArrayList<Integer>();
    for(var macaw : parrots)
      System.out.println(macaw);
  }

  /**
   * Line 22 does not compile because Long is not a compatible type for a switch statement
   * or expression. Line 23 does not compile because it is missing a semicolon after "Jane" and
   * a yield statement. Line 24 does not compile because it contains an extra semicolon at the
   * end. Finally, lines 25 and 26 do not compile because they use the same case value. At least
   * one of them would need to be changed for the code to compile. Since four lines need to be
   * corrected, option E is correct
   *
   *  21: void findZookeeper(Long id) {
   *  22: System.out.print(switch(id) {
   *  23: case 10 -> {"Jane"}
   *  24: case 20 -> {yield "Lisa";};
   *  25: case 30 -> "Kelly";
   *  26: case 30 -> "Sarah";
   *  27: default -> "Unassigned";
   *  28: });
   *  29: }
   *
   * @param id
   */
  static void findZookeeper(Long id) {
    //System.out.print(switch(id) {
    System.out.print(switch(1) {
     case 10 -> {yield "Jane";}
     case 20 -> {yield "Lisa";}//;
     case 30 -> "Kelly";
     //case 30 -> "Sarah"; //Duplicate label '30'
     default -> "Unassigned";
     });
    }

  void getFish(Object fish) {
    if (!(fish instanceof String guppy)) {
      System.out.print("Eat!");
    }
     //else if (!(fish instanceof String guppy)) { //Variable 'guppy' is already defined in the scope
     //throw new RuntimeException();
     //}
     System.out.print("Swim!");
    }

  static String checkStr(String voltage) {
    final String s = "10";
    String z = switch (s) {
      case "Q" -> "3";
      case "10"-> "19";
      case "E" -> "great";
        default -> "";
    };
    return z;
  }

  static void printType(Object o) {
    final char a = 'A', e = 'E';
    char grade = 'B';
    switch (grade) {
      default:
      case a:
      case 'B': case 'C': System.out.print("great ");
      case 'D': System.out.print("good "); break;
      case e:
      case 'F': System.out.print("not good ");
    }
    System.out.println();
    String test = "test";
    switch (test) {
      default:
      case "Q":
      case "W": case "E": System.out.print("great ");
    }

    if(o instanceof Integer bat) {
       System.out.print("int");
      } else if(o instanceof Integer bat && bat < 10) {
       System.out.print("small int");
       //} else if(o instanceof Long bat || bat <= 20) { // || bat can be error
       //System.out.print("long");
       //} default: { //: expected and Case statement outside switch
       //System.out.print("unknown");
       }
     }

  private DayOfWeek getWeekDay(int day, final int thursday) {
    int otherDay = day;
     int Sunday = 0;
     switch(otherDay) {
     default:
     //case 1: continue;
     //case thursday: return DayOfWeek.THURSDAY; //Constant expression required. Since not a compile-time constant since
     // any int value can be passed as a parameter
     case 2,10: break;
     //case Sunday: return DayOfWeek.SUNDAY; //Constant expression required
     //case DayOfWeek.MONDAY: return DayOfWeek.MONDAY; //Provided: DayOfWeek
     }
    return DayOfWeek.FRIDAY;
     }

}

class Q5 {
    public static void main(String[] args) {
        List<Integer> myFavoriteNumbers = new ArrayList<>();
        myFavoriteNumbers.add(10);
        myFavoriteNumbers.add(14);
        for (var a : myFavoriteNumbers) {
            System.out.print(a + ", ");
            break; }
        for (int b : myFavoriteNumbers) {
            continue;
            //System.out.print(b + ", ");
        }
        for (Object c : myFavoriteNumbers)
            System.out.print(c + ", ");
    }
}

class Q8 {
        void printType(Object o) {
            if (o instanceof Integer bat) {
                System.out.print("int");
            } else if (o instanceof Integer bat && bat < 10) {
                System.out.print("small int");
            //} else if (o instanceof Long bat || bat <= 20) {
              //  System.out.print("long");
            } else {
                System.out.print("unknown");
            }
        }
}

class Q15 {
    public static void main(String[] args) {
        final char a = 'A', e = 'E'; char grade = 'B';
        switch (grade) {
            default:
            case a:
            case 'B', 'C': System.out.print("great ");
            case '1': case '2': System.out.print("great ");
            case 'D': System.out.print("good "); break;
            case e:
            case 'F': System.out.print("not good ");
        }
    }
}

class Q21 {
    public static void main(String[] args) {
        int height = 1;
        L1: while(height++ <10) {
            long humidity = 12;
            L2: do {
                if(humidity-- % 12 == 0)
                    break L2;
                int temperature = 30;
                L3: for( ; ; ) {
                    temperature++;
                    if(temperature>50)
                        continue L2;
                }
            } while (humidity > 4);
        }
    }
}

class Q21_2 {
    public static void main(String[] args) {
        int height = 1;
        L1: while(height++ <10) {
            long humidity = 12;
            L2: do {
                if(humidity-- % 12 == 0)
                    continue L2;
                int temperature = 30;
                L3: for( ; ; ) {
                    temperature++;
                    if(temperature>50)
                        continue L2;
                }
            } while (humidity > 4);
        }
    }
}

class Q27 {
    public static void main(String[] args) {
        int height = 1;
        byte amphibian = (byte) height;
        amphibian = 1;
        String name = "Frog";
        String color = switch(amphibian) {
            case 1 -> { yield "Red"; }
            //case 2 -> { if(name.equals("Frog")) yield "Green"; }
            case 3 -> { yield "Purple"; }
            default -> throw new RuntimeException();
        }; System.out.print(color);
    }
}

class Q28 {
    public static void main(String[] args) {
    }
    static void getFish(Object fish) {
        if (!(fish instanceof String guppy))
            System.out.print("Eat!");
        //else if (!(fish instanceof String guppy)) {
          //  throw new RuntimeException(); }
        System.out.print("Swim!");
    }
}

class Q29 {
    public static void main(String[] args) {
        int y = -2;
        do System.out.print(++y + " ");
        while(y <= 5);

        while (y <= 0);
        System.out.println();
        y = -2;
        while (y <= 5) System.out.print(++y + " ");
    }
}