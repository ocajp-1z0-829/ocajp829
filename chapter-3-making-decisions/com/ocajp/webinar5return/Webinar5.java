package com.ocajp.webinar5return;

public class Webinar5 {
  public static void main(String[] args) {
    boolean x = false;
    //final boolean x = false;
    //while (false) {} // Unreachable statement
    while (x) {}
    while (!(x=true)) {}
  }

  public static void cicles(String[] args) {
    //for (;;) {}
    //for (;;) if (args.length > 0) break; //Unreachable statement. недоступно из-за 5 строки
    try {for (;;){}}
    catch (Throwable t) { System.out.println("Caught!"); }
    finally {}
    System.out.println("Out!");
  }
}
