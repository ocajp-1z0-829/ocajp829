package com.ocajp.webinar5return;

import java.util.ArrayList;

public class Test {
  public static void main(String[] args) {
    for (int k=0;k<5;k++){
      System.out.print(k);
    }
    System.out.println();
    for (int k=1;k<=5;k++){
      System.out.print(k);
    }
    System.out.println();
    int k=0; do{
      System.out.print(k);
    } while (k++ <5);
    System.out.println();
    k=0;
    while (k++ <5) {
      System.out.print(k);
    }

    ArrayList<Integer> taxis = new ArrayList<>();
    int[] taxis2 = new int[]{};
    StringBuilder taxis3 = new StringBuilder();
    for (Object obj : taxis) {}
    for (Object obj : taxis2) {}
    //for (Object obj : taxis3) {} //foreach not applicable to type 'java.lang.StringBuilder'

    Double[][] d = new Double[][]{};

  }
}
