package com.ocajp;

public class Loop {
  public static void main(String[] args) {
    for (var counter = 5; counter > 0; counter--) {
      System.out.print(counter + " ");
    }
    System.out.println();
    int x = 0;
    for(long y = 0, z = 4; x < 5 && y < 10; x++, y++) {
      System.out.print(y + " "); }
    System.out.print(x + " ");

    //int x = 0;
    //for(long y = 0, int z = 4; x < 5; x++) // DOES NOT COMPILE
    //System.out.print(y + " ");

    //for(long y = 0, x = 4; x < 5 && y < 10; x++, y++) //Variable 'x' is already defined in the scope
      //System.out.print(y + " ");
    //System.out.print(x); // DOES NOT COMPILE
    System.out.println();
    int hungryHippopotamus = 8;
    while(hungryHippopotamus>0) {
      do {
        hungryHippopotamus -= 2;
        System.out.println(hungryHippopotamus);
      } while (hungryHippopotamus>5);
      hungryHippopotamus--;
      System.out.print(hungryHippopotamus+", ");
    }

  }


}
