import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

class AtomicDoubleVsAtomicReference {
  private static AtomicReference<Double> sum = new AtomicReference<>();

  public static void main(String[] args) throws InterruptedException {
    for (int k = 0; k < 5; k++) {
      sum.set(0d);
      ExecutorService es = Executors.newFixedThreadPool(50);
      for (int i = 1; i <= 50; i++) {
        int finalI = i;
        es.execute(() -> {
          System.out.println(Thread.currentThread().getName());
          sum.accumulateAndGet(Math.pow(1.5, finalI),
              (d1, d2) -> d1 + d2);
        });
      }
      es.shutdown();
      es.awaitTermination(10, TimeUnit.MINUTES);
      System.out.println(sum.get());
    }
  }
}

class NoAtomicReferenceExample {
  private static Double sum;

  public static void main(String[] args) throws InterruptedException {
    for (int k = 0; k < 5; k++) {
      sum = 0d;
      ExecutorService es = Executors.newFixedThreadPool(50);
      for (int i = 1; i <= 50; i++) {
        int finalI = i;
        es.execute(() -> {
          sum += Math.pow(1.5, finalI);
        });
      }
      es.shutdown();
      es.awaitTermination(10, TimeUnit.MINUTES);
      System.out.println(sum);
    }
  }
}

class AtomicReferenceBigDecimal {
  private static AtomicReference<BigDecimal> sum = new AtomicReference<>();

  public static void main(String[] args) throws InterruptedException {
    for (int k = 0; k < 5; k++) {
      sum.set(BigDecimal.ZERO);
      ExecutorService es = Executors.newFixedThreadPool(50);
      for (int i = 1; i <= 50; i++) {
        int finalI = i;
        es.execute(() -> {
          sum.accumulateAndGet(new BigDecimal(1.5).pow(finalI),
              (bg1, bg2) -> bg1.add(bg2));
        });
      }
      es.shutdown();
      es.awaitTermination(10, TimeUnit.MINUTES);
      System.out.println(sum.get().setScale(2, RoundingMode.CEILING));
    }
  }
}

/**
 * com.google.common.util.concurrent.AtomicDouble.AtomicDouble(double initialValue)
 */
/*
class AtomicDoubleTest {
	public static void main(String[] args) throws InterruptedException {
		AtomicDouble sum = new AtomicDouble(0);
		for (int k = 0; k < 5; k++) {
			sum.set(0);
		          ExecutorService es = Executors.newFixedThreadPool(50);
		          for (int i = 1; i <= 50; i++) {
		              int finalI = i;
		              es.execute(() -> {
		                  sum.addAndGet(Math.pow(1.5, finalI));
		              });
		          }
		          es.shutdown();
		          es.awaitTermination(10, TimeUnit.MINUTES);
		          System.out.println(sum);
		      }
	}
}*/
