import java.util.concurrent.*;

public class SingleThreadExecutorTest {
    /**
     * System.out.println("begin");
     * new Thread(printInventory).start();
     * new Thread(printRecords).start();
     * new Thread(printInventory).start();
     * System.out.println("end");
         */
    public static void main(String[] args) {
        Runnable printInventory = () -> {
            System.out.println(Thread.currentThread().getName());
            System.out.println("Printing zoo inventory");
            };
        Runnable printRecords = () -> {
            System.out.println(Thread.currentThread().getName());
            for (int i = 0; i < 3; i++) System.out.println("Printing record: " + i);
        };

        ExecutorService service = Executors.newSingleThreadExecutor();
        try {
            System.out.println(Thread.currentThread().getName());
            System.out.println("begin");
            service.execute(printInventory);
            service.execute(printRecords);
            service.execute(printInventory);
            System.out.println("end");
        } finally {
            service.shutdown();
        }
    }
}

class CallableTest {
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        ExecutorService service = Executors.newSingleThreadExecutor();
        Future<?> future = service.submit(() -> System.out.println("Hello")); //Hello
        Thread.sleep(1000);
        System.out.println(future.isDone()); //true
        System.out.println(future.get()); //null
        service.shutdown(); // без shutdown программа не выключается
    }
}

class CheckResultsFuture {
    private static int counter = 0;
    public static void main(String[] unused) throws Exception {
        ExecutorService service = Executors.newSingleThreadExecutor();
        try {
            Future<?> result = service.submit(() -> {
                for(int i = 0; i < 1_000000_000; i++)
                counter++;
            });
            result.get(10, TimeUnit.SECONDS); // Returns null for Runnable
            System.out.println("Reached!");
        } catch (TimeoutException e) { System.out.println("Not reached in time");
        } finally { service.shutdown();
        }} }

class CallableTest2 {
    public static void main(String[] args) {
        var service = Executors.newSingleThreadExecutor();
        try {
            Future<Integer> result = service.submit(() -> 30 + 11);
            System.out.println(result.get()); // 41
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } finally {
            service.shutdown();
        }
    }
}

class AwaitTerminationTest {
    public static void main(String[] args) {
        ExecutorService service = Executors.newSingleThreadExecutor();
        Future<?> result;
        try {
            result = service.submit(() -> {
                for(int i = 0; i < 1_000000_000; i++) {}
            });
        } finally {
            service.shutdown(); }
        try {
            //System.out.println(result.get());
            boolean b = service.awaitTermination(1, TimeUnit.NANOSECONDS);// Check whether all tasks are finished
            System.out.println(b);
            System.out.println(result.get());
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }
        if(service.isTerminated()) System.out.println("Finished!");
   else System.out.println("At least one task is still running");
    }
}