public class ThreadAndRunnableTest {
    public static void main(String[] args) {
        Runnable printInventory = () -> System.out.println("Printing zoo inventory");
        Runnable printRecords = () -> {
            for (int i = 0; i < 3; i++) System.out.println("Printing record: " + i);
        };

        new Thread(() -> System.out.print("Hello")).start();
        System.out.print("World");

        System.out.println();
        System.out.println("begin");
        new Thread(printInventory).start();
        new Thread(printRecords).start();
        new Thread(printInventory).start();
        System.out.println("end");
    }
}

class RunTest {

    public static void main(String[] args) {
        Runnable printInventory = () -> System.out.println("Printing zoo inventory");
        Runnable printRecords = () -> {
            for (int i = 0; i < 3; i++) System.out.println("Printing record: " + i);
        };

        System.out.println("begin");
        new Thread(printInventory).run();
        new Thread(printRecords).run();
        new Thread(printInventory).run();
        System.out.println("end");
    }
}

class Zoo {
    public static void pause() {
        try {
            Thread.sleep(2_000);
        } catch (InterruptedException e) {

        }
        System.out.println("Thread finished!");

    }
    public static void main(String[] unused) {
        var job = new Thread(() -> pause());
        System.out.println(job.getState()); // NEW
        //job.setDaemon(true); // завершает программу по Main method finished!
        job.start();
        System.out.println(job.getState()); // RUNNABLE
        System.out.println("Main method finished!");
        System.out.println(job.getState()); // TIMED_WAITING OR RUNNABLE
    } }

class CheckResults {
    private static int counter = 0;
    public static void main(String[] args) {
        new Thread(() -> {
            for(int i = 0; i < 1_000_000; i++) {
                counter++;
            }
        }).start();
        while(counter < 1_000_000) {
            System.out.println("Not reached yet: ");
        }
        System.out.println("Reached: "+counter); }}

class CheckResultsWithSleep {
    private static int counter = 0;
    public static void main(String[] a) {
    new Thread(() -> {
        for(int i = 0; i < 1_000_000; i++) {
            System.out.println(counter);
            counter++;
        }
    }).start();
    while(counter < 1_000_000) {
        System.out.println("Not reached yet"); try {
            Thread.sleep(1_000); // 1 SECOND
            } catch (InterruptedException e) {
            System.out.println("Interrupted!"); }
    }
    System.out.println("Reached: "+counter); }}

class CheckResultsWithSleepAndInterrupt {
    private static int counter = 0;
    public static void main(String[] a) {
        final var mainThread = Thread.currentThread();
        new Thread(() -> {
            for(int i = 0; i < 1_000_000; i++) counter++;
            mainThread.interrupt();
            System.out.println(mainThread.getState());
        }).start();
        while(counter < 1_000_000) {
            System.out.println("Not reached yet");
            try {
                Thread.sleep(1_000); // 1 SECOND
                System.out.println(mainThread.getState());
            } catch (InterruptedException e) {
                    System.out.println("Interrupted!");
                System.out.println(mainThread.getState());
            } }
        System.out.println("Reached: "+counter);
        System.out.println(mainThread.getState());

    }
}

class NotifyTest {
    public static void main(String[]args) throws InterruptedException{
        Object lock=new Object();
        // task будет ждать, пока его не оповестят через lock
        Runnable task=()->{
            synchronized(lock){
                try{
                    lock.wait();
                }catch(InterruptedException e){
                    System.out.println("interrupted");
                }
            }
            // После оповещения нас мы будем ждать, пока сможем взять лок
            System.out.println("thread");
        };
        Thread taskThread=new Thread(task);
        taskThread.start();
        // Ждём и после этого забираем себе лок, оповещаем и отдаём лок
        Thread.currentThread().sleep(3000);
        System.out.println("main");
        synchronized(lock){
            lock.notify();
        }
    }

}