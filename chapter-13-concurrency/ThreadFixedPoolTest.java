import java.util.concurrent.*;

public class ThreadFixedPoolTest {
            private static int sheepCount = 0;
            private void incrementAndReport() {
                System.out.print((++sheepCount)+" "); }
            public static void main(String[] args) {
                ExecutorService service = Executors.newFixedThreadPool(20);
                try {
                    ThreadFixedPoolTest manager = new ThreadFixedPoolTest();
                for(int i = 0; i < 10; i++)
                    service.submit(() -> manager.incrementAndReport()); }
                finally {
                    service.shutdown(); } } }

class SheepManager {
    private int sheepCount = 0;
    private void incrementAndReport() {
        //synchronized(this) { // 1 2 3 4 5 6 7 8 9 10
            System.out.print((++sheepCount)+" ");
        //}
    }
    public static void main(String[] args) {
        ExecutorService service = Executors.newFixedThreadPool(20);
        try {
            var manager = new SheepManager();
            for(int i = 0; i < 10; i++) { // 5 6 9 7 3 2 4 10 1 8
                synchronized (manager) {
                    service.submit(() -> manager.incrementAndReport());
                }
            }
        }
        finally {
            service.shutdown();
        }
    }

    static void dance2() {
        synchronized(SheepManager.class) {
            System.out.print("Time to dance!");
        }
    }
    static synchronized void dance() {
        System.out.print("Time to dance!"); }
}

