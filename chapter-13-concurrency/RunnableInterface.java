@FunctionalInterface
public interface RunnableInterface {
    void run();
}