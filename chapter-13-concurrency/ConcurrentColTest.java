import java.util.*;
import java.util.concurrent.*;

public class ConcurrentColTest {
    public static void main(String[] args) {
        Map<String, String> concurrentHashMap = new ConcurrentHashMap<>();
        ConcurrentMap<String, String> objectObjectConcurrentHashMap = new ConcurrentHashMap<>();
        Queue<String> concurrentLinkedQueue = new ConcurrentLinkedQueue<>();
        Map<Object, Object> concurrentSkipListMap1 = new ConcurrentSkipListMap<>();
        SortedMap<Object, Object> concurrentSkipListMap2 = new ConcurrentSkipListMap<>();
        NavigableMap<Object, Object> concurrentSkipListMap3 = new ConcurrentSkipListMap<>();
        ConcurrentMap<Object, Object> concurrentSkipListMap4 = new ConcurrentSkipListMap<>();
        ConcurrentNavigableMap<Object, Object> concurrentSkipListMap5 = new ConcurrentSkipListMap<>();
        Set<Object> concurrentSkipListSet1 = new ConcurrentSkipListSet<>();
        SortedSet<Object> concurrentSkipListSet2 = new ConcurrentSkipListSet<>();
        NavigableSet<Object> concurrentSkipListSet3 = new ConcurrentSkipListSet<>();
        List<Object> copyOnWriteArrayList = new CopyOnWriteArrayList<>();
        Set<Object> copyOnWriteArraySet = new CopyOnWriteArraySet<>();
        Queue<Object> linkedBlockingQueue1 = new LinkedBlockingQueue<>();
        BlockingQueue<Object> linkedBlockingQueue2 = new LinkedBlockingQueue<>();

    }

    public int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            int secondNum = target - nums[i];
            if (map.containsKey(secondNum)) {
                return new int[]{i, map.get(secondNum)};
            }
            map.put(nums[i], i);
        }
        return new int[]{};
    }
}

class LinkedBlockingQueueTest {
    public static void main(String[] args) {
        LinkedBlockingQueue<Integer> favNumbers = new LinkedBlockingQueue<>();
        List<Integer> integers = List.of(1, 2, 3, 4, 5, 6, 7, 8);
        System.out.println("Size: " + favNumbers.size()); // Size:
        var exec = Executors.newCachedThreadPool();
        try {
            for (int i : integers) {
                exec.submit(() -> System.out.print(favNumbers.offer(i) + " "));
            }
        } finally {
            exec.shutdown();
        }
        System.out.println("favNumbers: " + favNumbers); //

    }
}

class ConcurrentLinkedQueueTest {
    public static void main(String[] args) {
        //Map<Integer, Integer> favNumbers = Collections.synchronizedMap(Map.of(4, 3, 42, 2)); //ERRor
        ConcurrentLinkedQueue<Integer> favNumbers = new ConcurrentLinkedQueue<>();
        favNumbers.addAll(List.of(4, 3, 42, 2, 1, 2, 0, 8));
        List<Integer> integers = List.of(1, 2, 3, 4, 5, 6, 7, 8);
        System.out.println("Size: " + favNumbers.size()); // Size:
        var exec = Executors.newCachedThreadPool();
        try {
            for (int i : integers) {
                exec.submit(() -> System.out.print(favNumbers.offer(i) + " "));
            }
        } finally {
            exec.shutdown();
        }
        System.out.println("favNumbers: " + favNumbers); //
    }
}

class SynchronizedNavigableMapTest {
    public static void main(String[] args) {
        //Map<Integer, Integer> favNumbers = Collections.synchronizedMap(Map.of(4, 3, 42, 2)); //ERRor
        SortedMap<Integer, Integer> favNumbers = Collections.synchronizedSortedMap(new ConcurrentSkipListMap<>(Map.of(4, 3, 42, 2, 1, 2, 0, 8)));
        for (var n : favNumbers.entrySet()) {
            System.out.print(n + " "); // 4 3 42
            favNumbers.put(n.getKey() + 1, n.getValue() + 1);
        }
        System.out.println(favNumbers);
        System.out.println("Size: " + favNumbers.size()); // Size: 6
    }
}

class SynchronizedNavMapTest {
    public static void main(String[] args) {
        //Map<Integer, Integer> favNumbers = Collections.synchronizedMap(Map.of(4, 3, 42, 2)); //ERRor
        NavigableMap<Integer, Integer> favNumbers = Collections.synchronizedNavigableMap(new ConcurrentSkipListMap<>(Map.of(4, 3, 42, 2, 1, 2, 0, 8)));
        for (var n : favNumbers.entrySet()) {
            System.out.print(n + " "); // 4 3 42
            favNumbers.put(n.getKey() + 1, n.getValue() + 1);
        }
        System.out.println(favNumbers);
        System.out.println("Size: " + favNumbers.size()); // Size: 6
    }
}

class SynchronizedMapTest {
    public static void main(String[] args) {
        //Map<Integer, Integer> favNumbers = Collections.synchronizedMap(Map.of(4, 3, 42, 2)); //ERRor
        Map<Integer, Integer> favNumbers = Collections.synchronizedMap(new ConcurrentHashMap<>(Map.of(4, 3, 42, 2, 1, 2, 0, 8)));
        for (var n : favNumbers.entrySet()) {
            System.out.print(n + " "); // 4 3 42
            favNumbers.put(n.getKey() + 1, n.getValue() + 1);
            favNumbers.remove(n);
        }
        System.out.println(favNumbers);
        System.out.println("Size: " + favNumbers.size()); // Size: 6
    }
}

class SyncCopyOnWriteArrayListTest {
    public static void main(String[] args) {
        List<Integer> favNumbers = Collections.synchronizedList(new CopyOnWriteArrayList<>(List.of(4, 3, 42)));
        for (var n : favNumbers) {
            System.out.print(n + " "); // 4 3 42
            favNumbers.add(n + 1);
            favNumbers.remove(0);
        }
        System.out.println(favNumbers);
        System.out.println("Size: " + favNumbers.size()); // Size: 6
    }
}

class CopyOnWriteArraySetTest {
    public static void main(String[] args) {
        Set<Integer> favNumbers = new CopyOnWriteArraySet<>(Set.of(4, 3, 42));
        for (var n : favNumbers) {
            System.out.print(n + " "); // 4 3 42
            favNumbers.add(n+1); }
        System.out.println(favNumbers);
        System.out.println("Size: " + favNumbers.size()); // Size: 6
    }
}

class CopyOnWriteArrayListTest {
    public static void main(String[] args) {
        List<Integer> favNumbers = new CopyOnWriteArrayList<>(List.of(4, 3, 42));
        for (var n : favNumbers) {
            System.out.print(n + " "); // 4 3 42
            favNumbers.add(n+1); }
        System.out.println(favNumbers);
        System.out.println("Size: " + favNumbers.size()); // Size: 6
    }
}

class ConcModException {
    public static void main(String[] args) {
        var foodData = new HashMap<String, Integer>();
        foodData.put("penguin", 1);
        foodData.put("flamingo", 2);
        for(String key: foodData.keySet())
            foodData.remove(key);
    }
}

class ConcurrentHashMapTest {
    public static void main(String[] args) {
        var foodData = new ConcurrentHashMap<String, Integer>();
        foodData.put("penguin", 1);
        foodData.put("flamingo", 2);
        for(String key: foodData.keySet())
            foodData.remove(key);
        System.out.println(foodData);
    }
}