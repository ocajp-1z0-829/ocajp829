import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Executors;

public class CyclicBarrierTest {
    public static void main(String[] args) {

    }
}

class CustomLionPenManager {
    private void removeLions() {System.out.println("Removing lions: " + Thread.currentThread().getName()); }
    private void cleanPen() {System.out.println("Cleaning the pen: " + Thread.currentThread().getName()); }
    private void addLions() {System.out.println("Adding lions: " + Thread.currentThread().getName()); }
    public void performTask() {
        removeLions();
        cleanPen();
        addLions();
    }
    public static void main(String[] args) {
        var service = Executors.newFixedThreadPool(4);
        try {
            var manager = new CustomLionPenManager();
            for (int i = 0; i < 4; i++)
                service.submit(() -> manager.performTask());
        } finally {
            service.shutdown();
        }
    }
}

class LionPenManager {
    private void removeLions() { System.out.println("Removing lions: " + Thread.currentThread().getName()); }
    private void cleanPen() { System.out.println("Cleaning the pen: " + Thread.currentThread().getName()); }
    private void addLions() { System.out.println("Adding lions: " + Thread.currentThread().getName()); }
    public void performTask(CyclicBarrier c1, CyclicBarrier c2) {
        try {
            removeLions();
            c1.await();
            cleanPen();
            c2.await();
            addLions();
        } catch (InterruptedException | BrokenBarrierException e) {
            System.out.println("Error: " + e.getMessage());
        } }
    public static void main(String[] args) {
        var service = Executors.newFixedThreadPool(4); // если 3-уйдет в безконечность, потоков мб больше
        try {
            var manager = new LionPenManager();
            var c1 = new CyclicBarrier(4); //только 2 типа конструкторов, если 3-уйдет в безконечность
            var c2 = new CyclicBarrier(4, () -> System.out.println("*** Pen Cleaned!"));
            for (int i = 0; i < 10; i++)
                service.submit(() -> manager.performTask(c1, c2));
            //for (int i = 0; i < 1; i++)
            //    service.submit(() -> manager.performTask(c1, c2));
        } finally { service.shutdown();
        }} }