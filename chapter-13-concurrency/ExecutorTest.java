import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecutorTest {
  private static final Executor executor = Executors.newFixedThreadPool(4);
  private volatile List<String> list = new ArrayList<>();

  public static void main(String[] args) {
    synchronized (ExecutorTest.class) {
      System.out.println("static object on static method");
    }
    ExecutorTest executorTest = new ExecutorTest();
    executor.execute(() -> executorTest.testArr1());
    executor.execute(executorTest::testArr2);
    ((ExecutorService) executor).shutdown();
    System.out.println("Shutdown");
    executorTest.printer(executorTest.list);
  }

  private void testArr1() {
    synchronized (ExecutorTest.class) {
      System.out.println("static object on non static method");
    }
    System.out.println(Thread.currentThread().getName());
    for (int i = 0; i <= 1000; i++) {
      list.add(Thread.currentThread().getName() + " " + i);
    }
  }

  private void testArr2() {
    System.out.println(Thread.currentThread().getName());
    for (int i = 0; i <= 1000; i++) {
      list.add(Thread.currentThread().getName() + " " + i);
    }
  }

  private void printer(List<String> list) {
    list.forEach(System.out::println);
    System.out.println(list.size());
  }
}
