import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ReentrantLockTest {
    public static void main(String[] args) {
        // Implementation #2 with a Lock
        Lock lock = new ReentrantLock();
        printHello(lock);
    }

    public static void printHello(Lock lock) {
        try {
            lock.lock();
            System.out.println("Hello");
        } finally {
            lock.unlock();
        }
    }
}

class TryLockTest {
    public static void main(String[] args) {
        Lock lock = new ReentrantLock(true);
        new Thread(() -> printHello(lock)).start();
        new Thread(() -> printHello(lock)).start();
        new Thread(() -> printHello(lock)).start();
        new Thread(() -> printHello(lock)).start();
        new Thread(() -> printHello(lock)).start();
        new Thread(() -> printHello(lock)).start();
        try {
            Thread.sleep(1);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        if(lock.tryLock()) {
            try {
                System.out.println("Lock obtained, entering protected code: " + Thread.currentThread().getName());
            } finally {
                lock.unlock();
            }
        } else {
            System.out.println("Unable to acquire lock, doing something else: " + Thread.currentThread().getName() ); }
    }

    public static void printHello(Lock lock) {
        try {
            lock.lock();
            System.out.println("Hello");
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } finally {
            lock.unlock();
        }

        try {
            if(lock.tryLock(10, TimeUnit.SECONDS)) {
                try {
                    System.out.println("Lock obtained, entering protected code: " + Thread.currentThread().getName());
                } finally {
                    lock.unlock();
                }
            } else {
                System.out.println("Unable to acquire lock, doing something else: " + Thread.currentThread().getName() ); }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}

class LockAcquareTest {
    public static void main(String[] args) {
        Lock lock = new ReentrantLock();
        lock.tryLock();
        lock.tryLock();
        lock.tryLock();
        if(lock.tryLock()) {
            try {
                lock.lock();
                System.out.println("Lock obtained, entering protected code");
            } finally {
                lock.unlock();
            }
        }

        new Thread(() -> System.out.println(lock.tryLock())).start(); // false
        new Thread(() -> System.out.println(lock.tryLock())).start(); // false
        new Thread(() -> System.out.println(lock.tryLock())).start(); // false
        new Thread(() -> System.out.println(lock.tryLock())).start(); // false
        new Thread(() -> System.out.println(lock.tryLock())).start(); // false
    }
}

class TryLockMyTest {
    public static void main(String[] args) {
        Lock lock = new ReentrantLock(true);
        new Thread(() -> printHello(lock)).start();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        new Thread(() -> printHello(lock)).start();
        new Thread(() -> printHello(lock)).start();
        new Thread(() -> printHello(lock)).start();
        new Thread(() -> printHello(lock)).start();
        new Thread(() -> printHello(lock)).start();
    }

    public static void printHello(Lock lock) {
        try {
            System.out.println(lock.tryLock());
            //System.out.println(lock.tryLock());
            Thread.sleep(100);
            System.out.println(Thread.currentThread().getName());
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } finally {
            lock.unlock(); //Exception in thread "Thread-5"
            //lock.unlock(); //Сколько tryLock() столько и unlock

        }
        //true false false false false false
        //Thread-4 Thread-2 Thread-0 Thread-1 Thread-3 Thread-5
        //Exception in thread "Thread-5" java.lang.IllegalMonitorStateException
    }
}

