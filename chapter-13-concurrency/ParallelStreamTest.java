import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.stream.Stream;

class StreamTest {
    public static void main(String[] args) {
        long start = System.currentTimeMillis(); List.of(1,2,3,4,5)
                .stream()
                .map(w -> doWork(w))
                .forEach(s -> System.out.print(s + " "));
        System.out.println();
        var timeTaken = (System.currentTimeMillis()-start)/1000;
        System.out.println("Time: "+timeTaken+" seconds");
    }

    private static int doWork(int input) {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {}
            return input; }
}

public class ParallelStreamTest {
    public static void main(String[] args) {
        long start = System.currentTimeMillis(); List.of(1,2,3,4,5)
                .parallelStream()
                .map(w -> doWork(w))
                .forEachOrdered(s -> System.out.print(s + " "));
        System.out.println();
        var timeTaken = (System.currentTimeMillis()-start)/1000;
        System.out.println("Time: "+timeTaken+" seconds");
    }

    private static int doWork(int input) {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {}
        return input; }
}

class ReductionTest {
    public static void main(String[] args) {
        System.out.print(List.of(1,2,3,4,5,6) // 4
                .parallelStream() .findAny() .get());

        System.out.print(List.of(1,2,3,4,5,6) // 1
                .parallelStream() .findFirst() .get());

        System.out.print(List.of(1,2,3,4,5,6) //3 порядок сохранен
                .parallelStream() .limit(3).skip(2).findFirst() .get());

        System.out.println(List.of('w', 'o', 'l', 'f') .parallelStream()
                .reduce("",
                        (s1,c) -> s1 + c,
                        (s2,s3) -> s2 + s3)); // wolf

        System.out.println(List.of(1,2,3,4,5,6)
                .parallelStream()
                .reduce(0, (a, b) -> (a - b))); // PROBLEMATIC ACCUMULATOR

        System.out.println(List.of(1,2,3,4,5,6)
                .stream()
                .reduce(0, (a, b) -> (a - b))); // PROBLEMATIC ACCUMULATOR

        System.out.println(List.of(1,2,3,4,5,6)
                .parallelStream()
                .reduce(0, (a, b) -> (a - b),  (a, b) -> (a - b))); // PROBLEMATIC ACCUMULATOR

        System.out.println(List.of("w","o","l","f") .parallelStream()
                .reduce("X", String::concat)); // XwXoXlXf

        System.out.println(List.of("w","o","l","f") .parallelStream()
                .reduce(" ", (s1,c) -> s1 + c, (s2,s3) -> s2 + s3)); // w o l f

        System.out.println(List.of("w","o","l","f") .stream()
                .reduce("X", String::concat)); // Xwolf
    }
}

class CollectTest {
    public static void main(String[] args) {
        Stream<String> stream = Stream.of("w", "o", "l", "f").parallel();
        SortedSet<String> set = stream.collect(
                ConcurrentSkipListSet::new,
                Set::add,
                Set::addAll);
        System.out.println(set); // [f, l, o, w]

        stream = Stream.of("w", "o", "l", "f").parallel();
        Set<String> set2 = stream.collect(
                CopyOnWriteArraySet::new,
                Set::add,
                Set::addAll);
        System.out.println(set2); // [w, o, l, f]

        stream = Stream.of("w", "o", "l", "f").parallel();
        Set<String> set3 = stream.collect(
                HashSet::new,
                Set::add,
                Set::addAll);
        System.out.println(set3); // [f, w, l, o]
    }
}