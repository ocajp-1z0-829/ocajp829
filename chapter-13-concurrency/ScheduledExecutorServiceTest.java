import java.util.concurrent.*;

public class ScheduledExecutorServiceTest {
    public static void main(String[] args) {
        ScheduledExecutorService service
                = Executors.newSingleThreadScheduledExecutor();
        Runnable task1 = () -> System.out.println("Runnable schedule");
        Callable<String> task2 = () -> "Callable schedule";
        Runnable task3 = () -> System.out.println("Runnable scheduleWithFixedDelay");
        Runnable task4 = () -> System.out.println("Runnable scheduleAtFixedRate");
        ScheduledFuture<?> r1 = service.schedule(task1, 1, TimeUnit.SECONDS);
        ScheduledFuture<?> r2 = service.schedule(task2, 1, TimeUnit.SECONDS);
        try {
            System.out.println(r2.get());
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
        //ScheduledFuture<?> r3 = service.scheduleWithFixedDelay(task3, 3, 1, TimeUnit.SECONDS);
        //ScheduledFuture<?> r4 = service.scheduleAtFixedRate(task4, 5, 5, TimeUnit.SECONDS);
        System.out.println(r1.getDelay(TimeUnit.SECONDS));
        System.out.println(r2.getDelay(TimeUnit.SECONDS));
        //System.out.println(r3.getDelay(TimeUnit.SECONDS));
        //System.out.println(r4.getDelay(TimeUnit.SECONDS)); /* 2 4 0 0 OR 0 0 2 4*/

        service.scheduleWithFixedDelay(task3, 0, 2, TimeUnit.SECONDS); //method creates a new task only after the previous task has finished
        service.scheduleAtFixedRate(task4, 5, 1, TimeUnit.SECONDS); //not wait finish
    }
}
