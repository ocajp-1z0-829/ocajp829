import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.*;

public class Tests {
    public static void main(String[] args) {
    }
}

class Q2 {
        private Lock vault = new ReentrantLock();
        private int total = 0;
        public void deposit(int value) {
            try {
                //vault.lock();
                System.out.println(vault.tryLock());
                System.out.println(total);
                total += value;
            } finally {
                vault.unlock();
            }
        }
        public static void main(String[] unused) {
            var bank = new Q2();
            IntStream.range(1, 10).parallel()
                .forEach(s -> bank.deposit(s));
            System.out.println(bank.total);
        }
}

class Q4 {
    public static void main(String[] args) {
        ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
                service.scheduleWithFixedDelay(() -> {
                    System.out.println("Open Zoo"); // w2
                }, 0, 1, TimeUnit.MINUTES);
        var result = service.submit(() -> // w3
                System.out.println("Wake Staff"));
        try {
            System.out.println(result.get());
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }
    }
}

class Q5 {
    public static void main(String[] args) {
        var value1 = new AtomicLong(0);
        final long[] value2 = {0};
        IntStream.iterate(1, i -> 1).limit(100).parallel()
                .forEach(i -> value1.incrementAndGet());
        IntStream.iterate(1, i -> 1).limit(100).parallel()
                .forEach(i -> ++value2[0]);
        System.out.println(value1+" "+value2[0]);
    }
}

class Q8 {
    private static int counter;
    public static void countIceCreamFlavors() {
        counter = 0;
        Runnable task = () -> counter++;
        LongStream.range(0, 500)//.parallel() //485
                .forEach(m -> new Thread(task).run()); //start() //499
        System.out.println(counter); //500
    }

    public static void main(String[] args) {
        countIceCreamFlavors();
    }
}

class Q10 {
    public static void main(String[] args) {
        List<Integer> lions = new ArrayList<>(List.of(1,2,3));
        List<Integer> tigers = new CopyOnWriteArrayList<>(lions);
        Set<Integer> bears = new ConcurrentSkipListSet<>();
        bears.addAll(lions);
        for(Integer item: tigers)
            tigers.add(4); // x1
        for(Integer item: bears)
            bears.add(5); // x2
        System.out.println(lions.size() + " " + tigers.size() + " " + bears.size());
    }
}

class Q11 {
    public static void main(String[] args) {
        Integer i1 = List.of(1, 2, 3, 4, 5).stream().findAny().get();
        synchronized(i1) { // y1
            Integer i2 = List.of(6, 7, 8, 9, 10) .parallelStream()
                    .sorted()
                    .findAny().get(); // y2
            System.out.println(i1 + " " + i2);
        }
    }
}

class Q12 {
    public static void main(String[] args) {
        ExecutorService service = Executors.newFixedThreadPool(4);
        try {
            service.execute(() -> takeNap());
            service.execute(() -> takeNap());
            service.execute(() -> takeNap());
        } finally { service.shutdown();
        }
        try {
            service.awaitTermination(2, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println("DONE!");
    }

    private static void takeNap() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}

class Q13 {
    public static void main(String[] args) {
        //System.out.print(List.of("duck","flamingo","pelican").parallelStream().parallel() // q1
                //.reduce(0, //c1.length() берет первое значение то есть 0
                        //(c1, c2) -> c1.length() + c2.length(), // q2 Cannot resolve method 'length' in 'Integer'
                        //(s1, s2) -> s1 + s2)); // q3
    }
}

class Q14 {
    public static void main(String[] args) {
        Object o1 = new Object();
        Object o2 = new Object();
        var service = Executors.newFixedThreadPool(2);
        var f1 = service.submit(() -> {
            synchronized (o1) {
                synchronized (o2) { System.out.print("Tortoise"); }
            } });
        var f2 = service.submit(() -> {
            synchronized (o2) {
                synchronized (o1) { System.out.print("Hare"); } }
        });
        try {
            f1.get();
            f2.get();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }
    }
}

class Q15 {
    public static void main(String[] args) {
        var cats = Stream.of("leopard", "lynx", "ocelot", "puma") .parallel();
        var bears = Stream.of("panda","grizzly","polar").parallel();
        System.out.println(Stream.of(cats, bears).flatMap(s -> s).isParallel());
        var data = Stream.of(cats,bears).flatMap(s -> s)
                .collect(Collectors.groupingByConcurrent(s -> !s.startsWith("p")));
        System.out.println(data.get(false).size() + " " + data.get(true).size());
    }
}

class Q16 {
    private volatile int fuel;
    private void launch(int checks) {
        var p = new ArrayList<Thread>();
        for (int i = 0; i < checks; i++)
            p.add(new Thread(() -> fuel++));
        System.out.println(Thread.currentThread().getState());
        p.forEach(Thread::interrupt);
        System.out.println(Thread.currentThread().getState());
        p.forEach(Thread::start);
        System.out.println(Thread.currentThread().getState());
        p.forEach(Thread::interrupt);
        System.out.println(Thread.currentThread().getState());
    }
    public static void main(String[] args) throws Exception {
        var ship = new Q16();
        ship.launch(100);
        Thread.sleep(60*100);
        System.out.print(ship.fuel);
    }
}

class Q19 {
    public static void main(String[] args) {
        var s = Executors.newScheduledThreadPool(10);
        DoubleStream.of(3.14159, 2.71828) // b1
                .forEach(c -> s.submit( // b2
                        () -> System.out.println((Thread.currentThread().getName() + ": " + 10 * c)))); // b3
        s.execute(() -> System.out.println(Thread.currentThread().getName() + ": " + "Printed"));
        //s.shutdown();
    }
}

class Q20 {
    static int countSout = 0;
    static int count = 0;
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        var service = Executors.newSingleThreadExecutor();
        try {
            var r = new ArrayList<Future<?>>();
            IntStream.iterate(0,i -> i+1).limit(5).forEach(
                    i -> {
                        try {
                            System.out.println(service.submit(() -> countSout++).get());
                        } catch (InterruptedException e) {
                            throw new RuntimeException(e);
                        } catch (ExecutionException e) {
                            throw new RuntimeException(e);
                        }
                    } // n1
            );
            IntStream.iterate(0,i -> i+1).limit(5).forEach(
                    i -> r.add(service.submit(() -> {count++;})) // n1
            );
            for(Future<?> result : r) {
                System.out.print(result.get()+" "); // n2
            }
        } finally { service.shutdown(); }
    }}

class Q21 {
    public static void main(String[] args) {
        var data = List.of(List.of(1,2), List.of(3,4), List.of(5,6));
        data.stream() // p1
            .flatMap(s -> s.stream()).findFirst() // p2
            .ifPresent(System.out::print);
        System.out.println();
        data.stream().parallel() // p1
                .flatMap(s -> s.stream()).findFirst() // p2
                .ifPresent(System.out::print);
    }
}

class Q22 {
    private AtomicInteger s1 = new AtomicInteger(0); // w1
    private int s2 = 0;
    private void countSheep() throws InterruptedException {
        var service = Executors.newSingleThreadExecutor(); // w2
        try {
            for (int i = 0; i < 100; i++)
                service.execute(() -> {
                    s1.getAndIncrement(); s2++; }); // w3
            Thread.sleep(60*100);
            System.out.println(s1 + " " + s2);
    } finally { service.shutdown(); } }
public static void main(String... nap) throws InterruptedException {
        new Q22().countSheep();
}}

class Q23 {
        public static void await(CyclicBarrier cb) { // j1
            try {
                cb.await();
            } catch (Exception e) {}
        }
        public static void main(String[] args) {
            var cb = new CyclicBarrier(10,
                    () -> System.out.println("Stock Room Full!")); // j2
                    IntStream.iterate(1, i -> 1).limit(10).parallel()
                            .forEach(i -> await(cb)); // j3
        }
}

/**
 * 24. A, F. The class compiles without issue, so option A is correct. Since getInstance() is a static method and sellTickets() is an instance method,
 * lines k1 and k4 synchro- nize on different objects, making option D incorrect. The class is not thread-safe because the addTickets() method is not synchronized,
 * and option E is incorrect. One thread could call sellTickets() while another thread calls addTickets(), possibly resulting in bad data.
 * Finally, option F is correct because the getInstance() method is synchronized. Since the constructor is private,
 * this method is the only way to create an instance of TicketManager outside the class. The first thread to enter the method will set the instance variable,
 * and all other threads will use the existing value. This is a singleton pattern.
 */
final class Q24 {
    private int tickets;
    private static Q24 instance;
    private Q24() {}
    static synchronized Q24 getInstance() { // k1
        if (instance==null)
            instance = new Q24(); // k2
        return instance;
    }
    public int getTicketCount() { return tickets; }
    public void addTickets(int value) {tickets += value;} // k3
    public void sellTickets(int value) {
        synchronized (this) { tickets -= value;
    }}
    public static void main(String[] args) {
        Q24 q24 = Q24.getInstance();
        q24.addTickets(10);
        System.out.println(q24.getTicketCount()); //10
        Q24 q24two = Q24.getInstance();
        q24two.sellTickets(8);
        System.out.println(q24two.getTicketCount()); //2
    }
}

class Q25 {
        public static void performCount(int animal) {
            // IMPLEMENTATION OMITTED
        }
        public static void printResults(Future<?> f) {
            try {
                System.out.println(f.get(1, TimeUnit.DAYS)); // o1
            } catch (Exception e) {
                System.out.println("Exception!");
            }
        }
            public static void main(String[] args) throws Exception {
                final var r = new ArrayList<Future<?>>();
                ExecutorService s = Executors.newSingleThreadExecutor();
                try {
                    for(int i = 0; i < 10; i++) {
                        final int animal = i;
                        r.add(s.submit(() -> performCount(animal))); // o2
                    }
                    r.forEach(f -> printResults(f));
                }
                finally { s.shutdown(); }
            }
}