package com.ocajp.jdk17.nested.anon;

public class ZooGiftShop {
  public int admission(int basePrice) {
    SaleTodayOnly sale = new SaleTodayOnly() { //anonymous class
      int dollarsOff() {
        return 3;
      }
    }; // Don't forget the semicolon!
    return basePrice - sale.dollarsOff();
  }

  public int admissionInterface(int basePrice) {
    SaleTodayOnlyInterface sale = new SaleTodayOnlyInterface() {
      public int dollarsOff() {
        return 3;
      }
    };
    return basePrice - sale.dollarsOff();
  }

  interface SaleTodayOnlyInterface {
    int dollarsOff();
  }

  abstract class SaleTodayOnly {
    abstract int dollarsOff();
  }
}

class Gorilla {
  interface Climb {}
  Climb climbing = new Climb() {};
}