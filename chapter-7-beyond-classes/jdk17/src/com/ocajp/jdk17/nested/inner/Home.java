package com.ocajp.jdk17.nested.inner;

public class Home {
  private String greeting = "Hi"; // Outer class instance variable

  public static void main(String[] args) {
    var home = new Home(); // Create the outer class instance
    home.enterRoom();
    Home.Room.greet("me");
    Room room = new Home().new Room(); // Create the inner class instance
    room.enter();
    Room room2 = home.new Room(); // Create the inner class instance
    //new Room(); //annot be referenced from a static context
  }

  public void enterRoom() { // Instance method in outer class
    var room = new Room(); // Create the inner class instance
    room.enter();
    new Room();
  }

  public class Room { // Inner class declaration
    public int repeat = 3;

    private static void greet(String message) {
      System.out.println(message);
    }

    public void enter() {
      for (int i = 0; i < repeat; i++) {
        greet(greeting);
      }
    }
  }
}