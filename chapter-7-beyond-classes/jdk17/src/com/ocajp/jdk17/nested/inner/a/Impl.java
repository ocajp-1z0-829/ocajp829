package com.ocajp.jdk17.nested.inner.a;

import com.ocajp.jdk17.nested.inner.Home;
import com.ocajp.jdk17.nested.staticNested.Park;

//public class Impl extends Home.Room { //has protected acces
public class Impl extends Home {

  public void r () {
    Room r = new Home.Room();
  }

  public static void main(String[] args) {
    Home home = new Home();
    //Room r = new Home.Room(); //'com.ocajp.jdk17.nested.inner.Home' is not an enclosing class
    //Room room = home.new Room(); //'com.ocajp.jdk17.nested.inner.Home.Room' has protected access in 'com.ocajp.jdk17.nested.inner.Home'
    //room.enter();

    Park.Ride p = new Park.Ride();
    Park park = new Park();
    //Park.Ride pr = park.new Ride(); //Qualified new of static class
  }
}
