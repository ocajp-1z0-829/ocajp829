package com.ocajp.jdk17.nested.inner;

public class Fox {
  public static void visitFriend() {
    //new Den(); // DOES NOT COMPILE cannot be referenced from a static context
    new Fox().new Den();
  }

  public void goHome() {
    new Den();
  }

  private class Den {
  }
}

class Squirrel {
  public void visitFox() {
    //new Den(); // DOES NOT COMPILE private
    //new Fox().new Den(); // has private access
  }
}