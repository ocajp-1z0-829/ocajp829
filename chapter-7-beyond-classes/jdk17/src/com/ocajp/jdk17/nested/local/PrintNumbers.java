package com.ocajp.jdk17.nested.local;

public class PrintNumbers {
  private int length = 5;

  public static void main(String[] args) {
    var printer = new PrintNumbers();
    printer.calculate(); // 100
  }

  public void calculate() {
    final int width = 20;
    int h = 1;
    //h = 5; //Variable 'h' is accessed from within inner class, needs to be final or effectively final
    class Calculator {
      public void multiply() {
        System.out.print(length * width * h);
      }
    }
    var calculator = new Calculator();
    calculator.multiply();
  }

  public void processData() {
    final int length = 5;
    int width = 10;
    int height = 2;
    class VolumeCalculator {
      public int multiply() {
        return length * width * height; // DOES NOT COMPILE
      }
    }
    //width = 2; //Variable 'width' is accessed from within inner class, needs to be final or effectively final
  }
}