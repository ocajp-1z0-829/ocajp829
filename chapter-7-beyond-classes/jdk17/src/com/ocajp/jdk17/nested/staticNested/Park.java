package com.ocajp.jdk17.nested.staticNested;

public class Park {
  public static void main(String[] args) {
    var ride = new Ride();
    System.out.println(ride.price);
  }

  public static class Ride {
    private int price = 6;
  }
}
