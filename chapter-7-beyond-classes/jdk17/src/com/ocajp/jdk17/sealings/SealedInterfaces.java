package com.ocajp.jdk17.sealings;

public non-sealed class SealedInterfaces implements Swims {
}

sealed interface Swims permits Duck, Floats, SealedInterfaces, Swan, SwimsExtended {}

non-sealed interface SwimsExtended extends Swims {}
// Classes permitted to implement sealed interface
final class Duck implements Swims {}
final class Swan implements Swims {}
// Interface permitted to extend sealed interface
non-sealed interface Floats extends Swims {}

class C implements Floats {}