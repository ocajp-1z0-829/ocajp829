package com.ocajp.jdk17.sealings;

/**
 * A sealed class is a class that restricts which other classes may directly
 * extend it. These are brand new to Java 17
 */
public class SealedClasses {
}

sealed class Bear permits Kodiak, Panda {}

final class Kodiak extends Bear {}

non-sealed class Panda extends Bear {}

//class Tmp extends Bear { } //Tmp is not allowed in the sealed hierarchy

//class Tmp extends Kodiak {} //Cannot inherit from final 'com.ocajp.jdk17.sealings.Kodiak'

class Tmp extends Panda {}

//class sealed Frog permits GlassFrog {} //'sealed' is a restricted identifier and cannot be used for type declarations

abstract sealed class Wolf permits Timber {}
final class Timber extends Wolf {}
//final class MyWolf extends Wolf {} //MyWolf is not allowed in the sealed hierarchy

sealed class Mammal permits Equine {}
sealed class Equine extends Mammal permits Zebra {}
final class Zebra extends Equine {}

sealed class Snake {}
final class Cobra extends Snake {}

sealed class Snake2 {
  final class Cobra2 extends Snake2 {}
}

class Ghost {
  public static void boo() {
    System.out.println("Not scared");
  }
  protected  /*final*/ class Spirit {
    public void boo() {
      System.out.println("Booo!!!");
    }
  }
  public static void main(String... haunt) {
    var g = new Ghost().new Spirit() {}; //Cannot inherit from final 'com.ocajp.jdk17.sealings.Ghost.Spirit'
    g.boo();
    new Ghost().boo();
    //new Spirit().boo();
    Ghost.boo();
  }
}

class Ostrich {
 private static int count;
 static class OstrichWrangler {
  public int stampede() {
       return count;
       } } }

class Deer {
  enum Food {APPLES, BERRIES, GRASS}
  protected static class Diet {
    private Food getFavorite() {
      return Food.BERRIES;
    }
  }
  public static void main(String[] seasons) {
    System.out.print(switch(new Diet().getFavorite()) { //'com.ocajp.jdk17.sealings.Deer.this' cannot be referenced from a static context
      case APPLES -> "a";
      case BERRIES -> "b";
      default -> "c";
    });
  } }

class Bear2 {
  enum FOOD {
    BERRIES {
      @Override
      public boolean isHealthy() {
        return false;
      }
    }, INSECTS {
      public boolean isHealthy() { return true; }},
    FISH {
      @Override
      public boolean isHealthy() {
        return false;
      }
    }, ROOTS {
      @Override
      public boolean isHealthy() {
        return false;
      }
    }, COOKIES {
      @Override
      public boolean isHealthy() {
        return false;
      }
    }, HONEY;
    public boolean isHealthy(){ return true;}
  }
  public static void main(String[] args) {
    System.out.print(FOOD.INSECTS);
    System.out.print(FOOD.INSECTS.ordinal());
    System.out.print(FOOD.INSECTS.isHealthy());
    System.out.print(FOOD.COOKIES.isHealthy());
  }
}

class Lion {
  class Cub {}
  static class Den {}
  static void rest() {
    var d = new Den();
    Lion.Den g = new Lion.Den();
    Lion.Cub c = new Lion().new Cub();
    //Lion.Cub b = new Cub(); //cannot be referenced from a static context
    //var h = new Cub(); //cannot be referenced from a static context
  } }

interface Swim {
  default void perform() { System.out.print("Swim!"); }
}
interface Dance {
  default void perform() { System.out.print("Dance!"); }
}
class Penguin implements Swim, Dance {
  public void perform() { System.out.print("Smile!"); }
  private void doShow() {
    //Swim.perform();//Non-static method 'perform()' cannot be referenced from a static context
    Swim.super.perform();
    Penguin.this.perform();
    //Penguin.super.perform(); //Cannot resolve method 'perform' in 'Object'
    Swim.super.perform();
  }
  public static void main(String[] eggs) {
    new Penguin().doShow();
  }
}

interface BigCat {
 abstract String getName();
 //static int hunt() { getName(); return 5; } //Non-static method 'getName()' cannot be referenced from a static context
 default void climb() { rest(); }
 private void roar() { getName(); climb(); /*hunt();*/ }
 //private static boolean sneak() { roar(); return true; } //Non-static method 'roar()' cannot be referenced from a
  // static context
 private int rest() { return 2; };
 }

class Zebra2 {
  private int x = 24;
  public Zebra2(){}
  public int hunt() {
    String message = "x is ";
    abstract class Stripes {
      private int x = 0;
      public void print() {
        System.out.println(message + Zebra2.this.x);
        System.out.println(Zebra2.super.getClass().getSimpleName());
        System.out.println(Zebra2.class.getSimpleName());
      }
    }
    var s = new Stripes() {};
    s.print();
    return x;
  }
  public static void main(String[] args) {
    new Zebra2().hunt();
  }
}

enum Animals {
  MAMMAL(true), INVERTEBRATE(Boolean.FALSE), BIRD(false), REPTILE(false), AMPHIBIAN(false), FISH(false) {
    public int swim() {
      return 4;
    }
  }, A(true);
  final boolean hasHair;
  Animals(boolean hasHair) {
    this.hasHair = hasHair;
  }
  public boolean hasHair() {
    return hasHair;
  }
  public int swim() {
    return 0;
  }
}

//abstract class Camel { void travel(); } //Missing method body, or declare abstract
//interface EatsGrass { private abstract int chew(); } //Illegal combination of modifiers: 'abstract' and 'private'
abstract class Elephant {
  abstract private class SleepsAlot {
    abstract int sleep();
  } }
//class Eagle { abstract soar(); } //Modifier 'abstract' not allowed here
interface Spider { default void crawl() {} }

sealed class Bird {
  public final class Flamingo extends Bird {}
}
sealed class Monkey {}
//class EmperorTamarin extends Monkey {}
non-sealed class Mandrill extends Monkey {}
//sealed class Friendly extends Mandrill permits Silly {} //Invalid permits clause: 'Silly' must directly extend
// 'Friendly'
final class Silly {}