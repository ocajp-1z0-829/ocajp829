package com.ocajp.jdk17.sealings.zoo;

//import com.ocajp.jdk17.sealings.impl.Emperor; //Class is not allowed to extend sealed class from another package
//public sealed class Penguin permits Emperor {} //Sealed class must have subclasses

public sealed class Penguin permits Emperor2 /*, Emperor // Class is not allowed to extend sealed class from another package*/ {}
