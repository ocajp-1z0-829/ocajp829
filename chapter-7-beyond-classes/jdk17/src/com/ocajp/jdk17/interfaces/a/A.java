package com.ocajp.jdk17.interfaces.a;

public interface A {
  default double getD() {
    return 10.0;
  }

  default int getI() {
    return 10;
  }
}
