package com.ocajp.jdk17.interfaces;

import java.util.ArrayList;

public class StaticInheritance {
}

class K {
  int x = 1;
  static int y = 2;
  static void m() {}
}

class L extends K {
  static int x = 3;
  int y = 4;
  //void m() {}
}

interface M {
  void method1();
  static void method2() {}
  default void m3(){}
}

interface N extends M {
  //static void method1(){};
  void method2();
  abstract void m3();

  public static void main(String[] args) {
    N a = null;
    ArrayList arrayList = new ArrayList();
    System.out.println(a instanceof Object);
    System.out.println(arrayList instanceof Object);
  }
}