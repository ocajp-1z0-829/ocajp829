package com.ocajp.jdk17.interfaces;

public abstract interface Interfaces {
  public static final int MINIMUM_DEPTH = 2;

  public abstract Float getSpeed(int age);
}

abstract interface Interfaces2 {
  static final int MINIMUM_DEPTH = 2;
  static int sleep(){ return 0;};
  abstract Float getSpeed(int age);
}

interface Int2Extended extends Interfaces2 {
  default double sleep() { return 0;}
}

class Interfaces2Impl implements Interfaces2, Int2Extended {

  public final double sleep() {return 0;}

  @Override
  public Float getSpeed(int age) {
    return 0f;
  }
}
interface Interfaces3 {
  int MINIMUM_DEPTH = 2;

  Float getSpeed(int age);
}

interface Herbivore { public void eatPlants(); }
interface Omnivore { public void eatPlants(); }
class Bear implements Herbivore, Omnivore {
  public void eatPlants() {
    System.out.println("Eating plants");
  }
}

abstract class Husky { // abstract required in class declaration
  //void play(); // abstract required in method declaration
}


interface Walk {
  public default int getSpeed() { return 5; }
}
interface Run {
  default int getSpeed() { return 10; }
}
class Cat implements Walk, Run {
  public int getSpeed() {
    return 1;
  }

  public int getWalkSpeed() {
    Hop.getJumpHeight();
    return Walk.super.getSpeed();
  }
}

interface Hop {
  static int getJumpHeight() {
    return 8;
  }
}

class Bunny implements Hop, Schedule{
  public void printDetails() {
    //System.out.println(getJumpHeight()); // DOES NOT COMPILE
  }

  @Override
  public void wakeUp() {
    Schedule.super.wakeUp();
  }
}

interface Schedule {
 /* void workOut() {
    checkTime(18); //Interface abstract methods cannot have body
  }*/

  static void workOut() {
    checkTime(18);
  }

  private static void checkTime(int hour) {
    if (hour > 17) {
      System.out.println("You're late!");
    } else {
      System.out.println("You have " + (17 - hour) + " hours left " + "to make the appointment");
    }
  }

  default void wakeUp() {
    checkTime(7);
  }

  private void haveBreakfast() {
    checkTime(9);
  }
}

interface ZooRenovation {
  public String projectName();
  abstract String status();
  default void printStatus() {
    System.out.print("The " + projectName() + " project " + status());
  } }

class Priv {
  private Priv() {
  }
}
//class PrivExtended extends Priv {}