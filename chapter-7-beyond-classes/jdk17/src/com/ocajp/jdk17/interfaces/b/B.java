package com.ocajp.jdk17.interfaces.b;

public interface B {
  default double getD() {
    return 10.0;
  }

  default int getI() {
    return 10;
  }
}
