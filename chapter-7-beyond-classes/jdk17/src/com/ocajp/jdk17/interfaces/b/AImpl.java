package com.ocajp.jdk17.interfaces.b;

import com.ocajp.jdk17.interfaces.a.A;

/**
 * com.ocajp.jdk17.interfaces.b.AImpl inherits unrelated defaults for getI() from types com.ocajp.jdk17.interfaces.a
 * .A and com.ocajp.jdk17.interfaces.b.B
 *
 * If a class inherits two or more default methods with the same method signature, then the
 * class must override the method
 */
public class AImpl implements A, B {

  @Override
  public double getD() {
    return A.super.getD();
  }

  @Override
  public int getI() {
    return A.super.getI();
  }
}
