package com.ocajp.jdk17.enums;

public enum Season {
  WINTER, SPRING, SUMMER, FALL
}

enum Season2 {
  WINTER, SPRING, SUMMER, FALL;
}

class SeasonImpl {
  public static void main(String[] args) {
    for (var season : Season.values()) {
      System.out.println(season.name() + " " + season.ordinal());
    }

    Season s = Season.valueOf("SUMMER"); // SUMMER
    //Season t = Season.valueOf("summer"); // IllegalArgumentException

    switch (s) {
      case WINTER:
        System.out.print("Get out the sled!");
        break;
      case SUMMER:
        System.out.print("Time for the pool!");
        break;
      default:
        System.out.print("Is it summer yet?");
    }

    var message = switch (s) {
      case WINTER -> "Get out the sled!";
      case SUMMER -> "Time for the pool!";
      default -> "Is it summer yet?";
    };
    System.out.println(message);
  }
}

enum SeasonWithConstructor {
  WINTER("Low"), SPRING("Medium"), SUMMER("High"), FALL("Medium");
  private final String expectedVisitors;

  private SeasonWithConstructor(String expectedVisitors) {
    this.expectedVisitors = expectedVisitors;
  }

  public void printExpectedVisitors() {
    System.out.println(expectedVisitors);
  }
}

enum OnlyOne {
  ONCE(true), SECOND(false);
  private OnlyOne(boolean b) {
    System.out.print("constructing " + b + ",");
  }
}

class PrintTheOne {
  public static void main(String[] args) {
    System.out.print("begin,");
    OnlyOne firstCall = OnlyOne.ONCE; // Prints constructing,
    OnlyOne secondCall = OnlyOne.SECOND; // Prints constructing,
    OnlyOne third = OnlyOne.SECOND; // Doesn't print anything
    OnlyOne a = OnlyOne.ONCE; // Doesn't print anything
    OnlyOne tshird = OnlyOne.ONCE; // Doesn't print anything
    OnlyOne tdhird = OnlyOne.ONCE; // Doesn't print anything
    //OnlyOne fourth = OnlyOne.SECOND; // Doesn't print anything
    System.out.println("end");
    //SeasonWithConstructor.SUMMER.printExpectedVisitors();
  }
}

enum Season3 {
  WINTER {
    public String getHours() { return "10am-3pm"; }
  },
  SPRING {
    public String getHours() { return "9am-5pm"; }
  },
  SUMMER {
    public String getHours() { return "9am-7pm"; }
  },
  FALL {
    public String getHours() { return "9am-5pm"; }
  };
  public abstract String getHours();
}

class Season3Impl {
  public static void main(String[] args) {
    System.out.println(Season3.SPRING.getHours());
    System.out.println(Season4.WINTER.getAverageTemperature());
    System.out.println(Season5.FALL.getHours());
  }
}

interface Weather { int getAverageTemperature(); }
enum Season4 implements Weather {
  WINTER, SPRING, SUMMER, FALL;
  public int getAverageTemperature() { return 30; }
}

enum Season5 {
  WINTER {
    public String getHours() { return "10am-3pm"; }
  },
  SUMMER {
    public String getHours() { return "9am-7pm"; }
  },
  SPRING, FALL;
  public String getHours() { return "9am-5pm"; }
}

class Favorites {
  enum Flavors {
    VANILLA, CHOCOLATE, STRAWBERRY;
    static final Flavors DEFAULT = STRAWBERRY;
    }
  public static void main(String[] args) {
    for(final var e : Flavors.values())
      System.out.print(e.ordinal()+" ");
  }
}