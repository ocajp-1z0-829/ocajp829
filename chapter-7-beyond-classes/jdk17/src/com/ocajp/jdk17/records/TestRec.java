package com.ocajp.jdk17.records;

public record TestRec(int i) {
  public TestRec {
    i = 5;
    i = 1;
  }
  private static int e;
  public static final int q = 0;
  static {

  }

  class T implements I{
    public static void main() {
      TestRec t = new TestRec(1);
      //t.i = 4;
      System.out.println(new TestRec(1));
    }
  }

  public static void main(String[] args) {
    T.main();
  }
  interface I {}
}
