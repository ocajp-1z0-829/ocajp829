package com.ocajp.jdk17.records;

public record Record() {
}

final class CranePlaint {
  private final int numberEggs;
  private final String name;

  public CranePlaint(int numberEggs, String name) {
    if (numberEggs >= 0) {
      this.numberEggs = numberEggs; // guard condition
    } else {
      throw new IllegalArgumentException();
    }
    this.name = name;
  }

  public int getNumberEggs() { // getter
    return numberEggs;
  }

  public String getName() { // getter
    return name;
  }
}

record Crane(int numberEggs, String name) { }

class CraneImpl {
  public static void main(String[] args) {
    var mommy = new Crane(4, "Cammy");
    System.out.println(mommy.numberEggs()); // 4
    //mommy.numberEggs() = 5; //Variable expected
    System.out.println(mommy.numberEggs()); // 4
    System.out.println(mommy.name()); // Cammy

    //var mommy1 = new Crane("Cammy", 4); // DOES NOT COMPILE
    //var mommy2 = new Crane("Cammy"); // DOES NOT COMPILE

    var father = new Crane(0, "Craig");
    System.out.println(father); // Crane[numberEggs=0, name=Craig]
    var copy = new Crane(0, "Craig");
    System.out.println(copy); // Crane[numberEggs=0, name=Craig]
    System.out.println(father.equals(copy)); // true
    System.out.println(father.hashCode() + ", " + copy.hashCode()); // 1007, 1007

    System.out.println(new CraneWithoutParams()); //CraneWithoutParams[]
  }
}

record CraneWithoutParams() {}

final record CraneFinalDefault(int numberEggs, String name) implements Bird {
  public CraneFinalDefault(int numberEggs, String name) {
    if (numberEggs < 0) throw new IllegalArgumentException();
    this.numberEggs = numberEggs;
    this.name = name;
  }

}

interface Bird {}

/**
 * No parentheses or constructor parameters
 *
 */
final record CraneCompactConstructor(int numberEggs, String name) {
  public CraneCompactConstructor {
    if (numberEggs < 0) throw new IllegalArgumentException();
    name = name.toUpperCase();
  }
}

record CraneCompactConstructorNoNeedThis(int numberEggs, String name) {
  public CraneCompactConstructorNoNeedThis {
    //this.numberEggs = 10; // DOES NOT COMPILE Cannot assign a value to final variable 'numberEggs'
    numberEggs = 10;
  }
}

record CraneOverloadConstructor(int numberEggs, String name) {
  public CraneOverloadConstructor(String firstName, String lastName) {
    this(0, firstName + " " + lastName);
    System.out.println("test");
    //this.name = ""; //Variable 'name' might already have been assigned to
  }
  //public CraneOverloadConstructor(String lastName) { //Non-canonical record constructor must delegate to another
    // constructor
    //this.name = lastName;
    //numberEggs = 4;
  //}
  public CraneOverloadConstructor(int numberEggs, String firstName, String lastName) {
    this(numberEggs + 1, firstName + " " + lastName);
    numberEggs = 10; // NO EFFECT (applies to parameter, not instance field)
    //this.numberEggs = 20; // DOES NOT COMPILE
  }

  private static Season s;
  private static int type = 10;
  protected static final int type3 = 10;
  //public int size; // DOES NOT COMPILE Instance field is not allowed in record
  //private boolean friendly; // DOES NOT COMPILE Instance field is not allowed in record
  @Override public int numberEggs() { return 10; }
  @Override public String toString() { return name; }

  private void f() {
    switch (s) {
      case WINTER -> {System.out.println();}
    }
  }

}

enum Season {
  WINTER, SUMMER, SPRING, FALL
}
