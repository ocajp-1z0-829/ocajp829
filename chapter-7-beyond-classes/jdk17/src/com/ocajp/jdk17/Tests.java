package com.ocajp.jdk17;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Tests {
}

class Q4 {
    public static sealed class ArmoredAnimal permits Armadillo {
        public ArmoredAnimal(int size) {}
        @Override public String toString() {
            return "Strong";
        }
        public void main(String[] a) {
            var c = new Armadillo(10, null);
            System.out.println(c);
        }
    }
    static final class Armadillo extends ArmoredAnimal {
        @Override public String toString() {
            return "Cute";
        }
        public Armadillo(int size, String name) {
            super(size); }
        }
}

class Q6 {
    //int _;
    int _a;
    int __;
    public abstract interface Herbivore {
        int amount = 10;
        public void eatGrass();
        //public abstract int chew() { return 13; }
    }
    //abstract class IsAPlant extends Herbivore { Object eatGrass(int season) { return null; }}
}

class Q10 {
    interface Walk {
        public static List move() {
            return null;
        }
    }
    interface Run extends Walk {
        public Number move();
    }
    class Leopard implements Walk {
            public ArrayList move() { // X
                return null;
        }
    }
    class Panther implements Run {
        public Integer move() { // Z
            return null;
        }
    }
}

class Q13 {
    public class Weather { enum Seasons {
        WINTER, SPRING, SUMMER, FALL }
        public static void main(String[] args) {
            Seasons v = null;
            switch (v) {
                case SPRING -> System.out.print("s");
                case WINTER -> System.out.print("w");
                //case Seasons.SUMMER -> System.out.print("m"); //No need qualifier An enum switch case label must be the unqualified name of an enumeration constant
                default -> System.out.println("missing data"); }
        }
    }
}

class Q15 {
        public static void boo() {
            System.out.println("Not scared");
        }
        public final class Spirit {
            public void boo() {
                System.out.println("Booo!!!"); }
        }
        public static void main(String... haunt) {
            //var g = new Q15().new Spirit() {}; //Cannot inherit from final 'com.ocajp.jdk17.Q15.Spirit'
        }
}

 class Q16 {
    private int count;
    static class OstrichWrangler {
        public int stampede() {
            //return count; //Non-static field 'count' cannot be referenced from a static context
            return 1;
        }
    }
    public static void main(String[] args) {}
}

interface Q17 {
    int amount = 10;
    static boolean gather = true;
    static void eatGrass() {}
    //int findMore() { return 2; }
    public default float rest() { return 2; }
    //protected int chew() { return 13; }
    private static void eatLeaves() {}
}

class Q18 {
        enum Food {APPLES, BERRIES, GRASS}
        protected class Diet {
            private Food getFavorite() {
                return Food.BERRIES;
            } }
        public static void main(String[] seasons) {
            //'com.ocajp.jdk17.Q18.this' cannot be referenced from a static context
            //System.out.print(switch(new Diet().getFavorite()) {case APPLES -> "a";case BERRIES -> "b";default -> "c";});
        }
}

class Q19 {
    enum FOOD {
        BERRIES {
            @Override
            public boolean isHealthy() {
                return false;
            }
        },
        INSECTS {
            public boolean isHealthy() {
                return true; }
        };
        //FISH, ROOTS, COOKIES, HONEY;
        public abstract boolean isHealthy();
    }
    public static void main(String[] args) {
        System.out.print(FOOD.INSECTS);
        System.out.print(FOOD.INSECTS.ordinal());
        //System.out.print(FOOD.INSECTS.isHealthy());
        //System.out.print(FOOD.COOKIES.isHealthy());
    }
}

record Q21(int size, String brand, LocalDate expires) {
    public static int MAX_STORAGE = 100;
    public Q21() {
        this(500, "Acme", LocalDate.now());
    }
    public Q21 { //compact constructor
        size = MAX_STORAGE;
        if(expires.isAfter(LocalDate.now()))
            throw new RuntimeException();
        throw new RuntimeException();
        //size = 10; //Cannot assign a value to final variable 'size'
    }

    public static void main(String[] args) {
        Q21 q21 = new Q21(MAX_STORAGE, "Acme", LocalDate.now());
        System.out.println(q21);
    }
}

interface Swim { default void perform() { System.out.print("Swim!"); }}
interface Dance { default void perform() { System.out.print("Dance!"); } }
class Q23 implements Swim, Dance {
    @Override
    public void perform() { System.out.print("Smile!"); }
    private void doShow() {
            Swim.super.perform();
        }
        public static void main(String[] eggs) {
            new Q23().doShow();
        }
}

class Q26 {
    public enum Animals {
        MAMMAL(true), INVERTEBRATE(Boolean.FALSE), BIRD(false),
        REPTILE(false), AMPHIBIAN(false), FISH(false) {
            public int swim() { return 4; }
            };
        final boolean hasHair;
        Animals(boolean hasHair) {
            this.hasHair = hasHair;
        }
        public boolean hasHair() { return hasHair; }
        public int swim() { return 0; }
    }
}

class Q30 {
    sealed class Bird {
        public final class Flamingo extends Bird {}
    }
    sealed class Monkey {}
    //class EmperorTamarin extends Monkey {} // Modifier 'sealed', 'non-sealed' or 'final' expected
    non-sealed class Mandrill extends Monkey {}
    //sealed class Friendly extends Mandrill permits Silly {} //Modifier 'sealed', 'non-sealed' or 'final' expected
    final class Silly {}
}