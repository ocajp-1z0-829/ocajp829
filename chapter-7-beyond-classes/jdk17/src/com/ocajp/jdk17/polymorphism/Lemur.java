package com.ocajp.jdk17.polymorphism;

class Primate {
  public boolean hasHair() {
    return true;
  }
}
interface HasTail {
  public abstract boolean isTailStriped();
}
public class Lemur extends Primate implements HasTail {
  public boolean isTailStriped() {
    return false;
  }

  public int age = 10;

  public static void main(String[] args) {
    Lemur lemur = new Lemur();
    System.out.println(lemur.age);
    HasTail hasTail = lemur;
    System.out.println(hasTail.isTailStriped());
    Primate primate = lemur;
    System.out.println(primate.hasHair());
  }
}

class Bird {}

class Fish {
  public static void main(String[] args) {
    Fish fish = new Fish();
    //Bird bird = (Bird)fish; // DOES NOT COMPILE inconvertible types; cannot cast
  }
}

interface Canine {
}

interface Dog {
}

class Wolf implements Canine {
}

class BadCasts {
  public static void main(String[] args) {
    Wolf wolfy = new Wolf();
    Dog badWolf = (Dog) wolfy;
  }
}

class Rodent {
}

class Capybara extends Rodent {
  public static void main(String[] args) {
    Rodent rodent = new Rodent();
    if (rodent instanceof Capybara c) {
      var capybara = (Capybara) rodent; // ClassCastException не всякое млекопитающее крокодил
      var capybara1 = c; // ClassCastException не всякое млекопитающее крокодил
    }
    Capybara c2 = new Capybara();
    Rodent r2 = c2;
    var c3 = (Capybara) r2; //OK
  }
}

class Bird2 {}
class Fish2 {
  public static void main(String[] args) {
    Fish2 fish = new Fish2();
    //if (fish instanceof Bird2 b) { // DOES NOT COMPILE Inconvertible types; cannot cast
      // Do stuff
    //}
  }
}

class Penguin {
  public int getHeight() { return 3; }
  public void printInfo() {
    System.out.print(this.getHeight());
  }
}
class EmperorPenguin extends Penguin {
  public int getHeight() { return 8; }
  public static void main(String []fish) {
    new EmperorPenguin().printInfo(); // 8
  }
}

class EmperorPenguin2 extends Penguin {
  public int getHeight() { return 8; }
  public void printInfo() {
    System.out.print(super.getHeight());
  }
  public static void main(String []fish) {
    new EmperorPenguin().printInfo(); // 3
  }
}

class PenguinHidden {
  public static int getHeight() { return 3; }
  public void printInfo() {
    System.out.println(this.getHeight());
  }
}
class CrestedPenguinHidden extends PenguinHidden {
  public static int getHeight() { return 8; }
  public static void main(String... fish) {
    new CrestedPenguinHidden().printInfo();
  }
}

class Marsupial {
  protected int age = 2;

  public static boolean isBiped() {
    return false;
  }
}

class Kangaroo extends Marsupial {
  protected int age = 6;

  public static boolean isBiped() {
    return true;
  }

  public static void main(String[] args) {
    Kangaroo joey = new Kangaroo();
    Marsupial moey = joey;
    System.out.println(joey.isBiped());
    System.out.println(moey.isBiped());
    System.out.println(joey.age);
    System.out.println(moey.age);
  }
}