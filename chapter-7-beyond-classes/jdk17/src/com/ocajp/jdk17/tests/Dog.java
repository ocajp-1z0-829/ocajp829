package com.ocajp.jdk17.tests;

import com.ocajp.jdk17.nested.inner.A;

public interface Dog extends CanBark, HasVocalCords {
  abstract int chew();
}

interface CanBark extends HasVocalCords {
  public void bark();
}

interface HasVocalCords {
  public abstract void makeSound();
}

class Main implements Dog, CanBark, HasVocalCords {
  public static void main(String[] args) {
    System.out.println("OK");
  }

  @Override
  public int chew() {
    return(0);
  }

  @Override
  public void bark() {

  }

  @Override
  public void makeSound() {

  }
}

interface Employee {}

interface Printable extends Employee {
  String print();
}

class ProgrammerEmp {
  public String print() {
    return("Hello world");
  }
}

abstract class C extends Auth {

}

class Auth extends ProgrammerEmp implements Printable, Employee {
  @Override //Printable
  public String print() {
    return "Hi Test";
  }

  public static void main(String[] args) {
    Auth a = new Auth();
    System.out.println(a.print());

    Byte a1 = 100;
    Short a2 = 200;
    Long a3 = a1 + (long) a2;
    //String a4 = (String) (a3 + a2); //Inconvertible types; cannot cast 'long' to 'java.lang.String'
  }
}
