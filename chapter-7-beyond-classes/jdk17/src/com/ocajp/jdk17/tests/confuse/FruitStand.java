package com.ocajp.jdk17.tests.confuse;

import java.io.FileNotFoundException;
import java.util.List;

interface Apple {}

interface Orange {}

class Gala implements Apple {}

class Tangerine implements Orange {}

final class Citrus extends Tangerine {}

public class FruitStand {
  public static void main(String[] args) {
    Gala g = new Gala();
    Tangerine t = new Tangerine();
    Citrus c = new Citrus();
    //System.out.println(t instanceof Gala); //Inconvertible types; cannot cast
    System.out.println(c instanceof Tangerine); //Condition 'c instanceof Tangerine' is always 'true'
    System.out.println(g instanceof Apple); //Condition 'g instanceof Apple' is always 'true'
    System.out.println(t instanceof Apple); //Condition 't instanceof Apple' is always 'false'
    System.out.println(t instanceof List); //Condition 't instanceof Apple' is always 'false'
    //System.out.println(c instanceof Apple); //Inconvertible types; cannot cast

    M o = null;
    System.out.println((o instanceof M) && (!(o instanceof N)));
  }
}

class L{}
class M extends L{}
class N extends M{}

class Test extends Pl {
  public Test() {
    super();
  }

  public static void main(String[] args) {
    System.out.println(new Test().getClass().getSimpleName());
    L[] a1 = {};
    L[] a2 = a1;
    M[] b = {};

    a1 = b; // B является подтипом A

    b = (M[]) a1; //выполняет эксплицитный даункаст, присваивая более широкий объект a1 ссылочной переменной более
    // узкого типа, что абсолютно допустимо, ак как a1 фактически указывает обратно на b, здесь мы имеем т.н. круговую ссылку (‛circular reference’).

    //b = (M[]) a2; //бросает CCE, так как a2 по-прежнему указывает на все тот же объект, на который ранее указывала
    // a1, вот почему массив a2 имеет тип A, а ведь мы не можем сказать, что A IS-A B.
  }
}

abstract class Pl {}

abstract class Ball {
  protected final int size;
  public Ball(int size) {
    this.size = size;
  }
}
interface Equipment {}

/**
 * Since SoccerBall extends Ball and implements Equipment, it can be explicitly cast to any of
 * those types
 */
class SoccerBall extends Ball implements Equipment {
  private int size = 3;
  public SoccerBall() {
    super(5);
  }
  public Ball get() {return this;}
  public static void main(String[] args) {
    Equipment equipment = (Equipment)(Ball)new SoccerBall().get();
    System.out.println(((SoccerBall)equipment).size);
  }
}

interface Canfly {
  public void fly(); // {}
}
/*final*/ class Bird {
  protected void dance() throws FileNotFoundException {}
  public int fly(int speed) {return(1);}
}
class Eagle extends Bird implements Canfly {
  public final void dance() {}
  @Override //Canfly
  public void fly() {}
}