package com.ocajp.jdk17.tests;

public class TestWebinar {

}

class Programmer {
  void print() {
    System.out.println("Programmer - Mala Gupta");
  }
}

class Author extends Programmer {
  void print() {
    System.out.println("Programmer - Mala Gupta");
  }
}

class TestEJava {
  public static void main(String[] args) {
    Programmer p = new Programmer();
    //Programmer b = ((Author) new Programmer()); //Casting 'new Programmer()' to 'Author' will produce
    // 'ClassCastException' for any non-null value
    Author b = ((Author) new Programmer()); //нельзя изначально более широкое базовое кастить в производное
    p.print();
    b.print();
  }
}

class BaseQ27 {
  String var = "EJava";
  void printVar() {
    System.out.println(var);
  }
}

class DerivedQ27 extends BaseQ27 {
  String var = "Guru";
  void printVar() {
    System.out.println(var);
  }
}

interface HClass {}

abstract class HClassImpl implements HClass {}

class MainQ27 {
  public static void main(String[] args) {
    BaseQ27 baseQ27 = new BaseQ27();
    BaseQ27 derived = new DerivedQ27();
    System.out.println(baseQ27.var);
    System.out.println(derived.var);
    baseQ27.printVar();
    derived.printVar();

  }
}
