package com.ocajp.jdk17.tests;

public class Test {
}

//Q1
record Iguana(int age) {
  //private static final int age = 10; //Variable 'age' is already defined in the scope
}

final record Gecko() {}

//abstract record Chameleon() { //Modifier 'abstract' not allowed here
  //private static String name; }

record BeardedDragon(boolean fun) { //fun() это метод getter
  @Override public boolean fun() { return false; } }

record Newt(long size) {
  @Override public boolean equals(Object obj) { return false; }
  public void setSize(long size) {
    //this.size = size; //Cannot assign a value to final variable 'size'
  } }

//Q2
interface CanHop {}
class Frog implements CanHop {
  public static void main(String[] args) {
    Frog frog = new TurtleFrog();
    //BrazilianHornedFrog frog2 = new TurtleFrog(); //Required type: BrazilianHornedFrog, Provided: TurtleFrog
    TurtleFrog frog3 = new TurtleFrog();
    var frog4 = new TurtleFrog();
    CanHop frog5 = new TurtleFrog();
  }
}
class BrazilianHornedFrog extends Frog {}
class TurtleFrog extends Frog {}

sealed class ArmoredAnimal permits Armadillo {
  public ArmoredAnimal(int size) {}
  @Override public String toString() { return "Strong"; }
  public static void main(String[] a) {
    var c = new Armadillo(10, null);
    System.out.println(c);
  }
}
final class Armadillo extends ArmoredAnimal {
  @Override public String toString() { return "Cute"; }
  public Armadillo(int size, String name) {
    super(size);
  }
}

class Movie {
  private int butter = 5;
  private Movie() {}
  protected class Popcorn {
    public static int butter = 10;
    private Popcorn() {}
    public void startMovie() {
      System.out.println(butter);
    }
  }
  public static void main(String[] args) {
    var movie = new Movie();
    movie.new Popcorn();
    Movie.Popcorn in = new Movie().new Popcorn();
    in.startMovie();
  }
}

class Weather {
  enum Seasons {
    WINTER, SPRING, SUMMER, FALL
  }
  public static void main(String[] args) {
    Seasons v = null;
    switch (v) {
      case SPRING -> System.out.print("s");
      case WINTER -> System.out.print("w");
      case SUMMER -> System.out.print("m");
      default -> System.out.println("missing data"); }
  }
}