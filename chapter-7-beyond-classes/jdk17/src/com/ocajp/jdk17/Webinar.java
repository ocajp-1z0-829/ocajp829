package com.ocajp.jdk17;

public class Webinar {
}

class Base {
  int type = 0;
  public void run(){};
}

class Derived extends Base {
  public Derived() {
    //int type = 10;
    type = 10;
  }
  int type = 1; //без этой переменной сокрытия переменной не будет

  public void run() {
    System.out.println("running type = " + this.type);
  }

  public static void main(String ...args) {
    Base b = new Derived();
    b.run();
    System.out.println("type = " + b.type);
  }
}

class A { int i=10; int m1() {return i;}}
class B extends A { int i=20; int m1() {return i;}}
class C extends B { int i=30; int m1() {return i;}}
class T {
  public static void main(String[] args) {
    C o1 = new C();
    B o2 = o1;
    System.out.println(o1.m1());
    System.out.println(o1.i);
    System.out.println(o2.m1());
    System.out.println(o2.i);
  }
}

class Parent {
  Parent() {
    greet();
  }

  void greet() {
    System.out.print(" Hello! ");
  }
}

class Child extends Parent {
  String str = " Hi! ";

  public static void main(String[] args) {
    Parent obj = new Child();
    obj.greet(); //null Hi!
  }

  void greet() {
    System.out.print(str);
  }
}