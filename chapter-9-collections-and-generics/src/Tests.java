import java.io.FileNotFoundException;
import java.util.*;

class Q2 {
    public static void main(String[] args) {
        List<?> q = List.of("mouse", "parrot"); //immutable <Object>
        System.out.println(q);
        System.out.println(q.getClass().getName());

        var v = List.of("mouse", "parrot"); //List<String>
        System.out.println(v.get(0).getClass());

        //q.removeIf(String::isEmpty); // OBJECT CALL
        //q.removeIf(s -> s.length() == 4); // OBJECT CALL
        v.removeIf(String::isEmpty); // UnsupportedOperationException
        v.removeIf(s -> s.length() == 4); // UnsupportedOperationException
    }
}

public class Tests {
    public static void main(String[] args) {
        //HashSet<Number> hs = new HashSet<Integer>();
        HashSet<? super ClassCastException> set = new HashSet<Exception>();
        //List<> list = new ArrayList<String>();
        //List<Object> values = new HashSet<Object>();
        //List<Object> objects = new ArrayList<? extends Object>();
        Map<String, ? extends Number> hm = new HashMap<String, Integer>();
    }
}

record Hello<T>(T t) {
    public Hello(T t) { this.t = t; }
    private <T> void println(T message) {
        System.out.print(t + "-" + message);
    }
    public static void main(String[] args) {
        new Hello<String>("hi").println(1);
        new Hello("hola").println(true);
    } }

record Platypus(String name, int beakLength) {
@Override public String toString() {return "" + beakLength;}
public static void main(String[] args) {
    Platypus p1 = new Platypus("Paula", 3);
    Platypus p2 = new Platypus("Peter", 5);
    Platypus p3 = new Platypus("Peter", 7);
    List<Platypus> list = Arrays.asList(p1, p2, p3);
    Collections.sort(list, Comparator.comparing(Platypus::beakLength));
    System.out.println(list); //[3, 5, 7]
    Collections.sort(list, Comparator.comparing(Platypus::beakLength).reversed());
    System.out.println(list); //[7, 5, 3]
    Collections.sort(list, Comparator.comparing(Platypus::name).thenComparing(Platypus::beakLength));
    System.out.println(list); //[3, 5, 7]
    //incorrect because it sorts by name in ascending order and only reverses the beak length of those with the same name
    Collections.sort(list, Comparator.comparing(Platypus::name)
            .thenComparing(/**/Comparator.comparing(Platypus::beakLength).reversed())/**/);
    System.out.println(list); //[3, 7, 5]
    //Collections.sort(list, Comparator.comparing(Platypus::name).thenComparingNumber(Platypus::beakLength).reversed());
    Collections.sort(list, Comparator.comparing(Platypus::name).thenComparingInt(Platypus::beakLength).reversed());
    System.out.println(list); //[7, 5, 3]
}}

class MyComparator implements Comparator<String> {
    public int compare(String a, String b) {
        return b.toLowerCase().compareTo(a.toLowerCase()); //-48 1 -48
    }

    public static void main(String[] args) {
        String[] values = {"ABb", "aab", "123"};
        Arrays.sort(values, new MyComparator());
        for (var s : values)
            System.out.print(s + " ");
    }
}

class Helper {
    public static <U extends Exception> void printException(U u) {
        System.out.println(u.getMessage());
    }

public static void main(String[] args) {
        Helper.printException(new FileNotFoundException("A"));
        Helper.printException(new Exception("B"));
        //Helper.<Throwable>printException(new Exception("C"));
        Helper.<NullPointerException>printException(new NullPointerException("D"));
        //Helper.printException(new Throwable("E"));
} }

record Sorted(int num, String text) implements Comparable<Sorted>, Comparator<Sorted> {
    public String toString() { return "" + num; }
    public int compareTo(Sorted s) {
        return text.compareTo(s.text);
        }
    public int compare(Sorted s1, Sorted s2) {
        return s1.num - s2.num;
        }
    public static void main(String[] args) {
        var s1 = new Sorted(88, "a");
        var s2 = new Sorted(55, "b");
        var t1 = new TreeSet<Sorted>();
        t1.add(s1); t1.add(s2);
        var t2 = new TreeSet<Sorted>(s1);
        t2.add(s1); t2.add(s2);
        System.out.println(t1 + " " + t2);

        Comparator<Integer> c1 = (o1, o2) -> o2 - o1;
        Comparator<Integer> c2 = Comparator.naturalOrder();
        Comparator<Integer> c3 = Comparator.reverseOrder();
        var list = Arrays.asList(5, 4, 7, 2); Collections.sort(list,  c2 );
        Collections.reverse(list);
        Collections.reverse(list);
        System.out.println(Collections.binarySearch(list, 2));
        }


}


class W {}
class X extends W {}
class Y extends X {}
class Z<Y> {
// INSERT CODE HERE
public void main(String[] args) {
    W w1 = new W();
    W w2 = new X();
    //W w3 = new Y();
    //Y y1 = new W();
    //Y y2 = new X();
    //Y y1 = new Y();
}
    //public static<T> T identity(T t) { return t;}
}

class Q17 {
    public static void main(String[] args) {
        var map = Map.of(1,2, 3, 6);
        var list = List.copyOf(map.entrySet()); // List<Entry<Integer, Integer>>
        List<Integer> one = List.of(8, 16, 2);
        System.out.println(one);
        var copy = List.copyOf(one);
        System.out.println(copy);
        var copyOfCopy = List.copyOf(copy);
        var thirdCopy = new ArrayList<>(copyOfCopy);
        //list.replaceAll(x -> x * 2);
        one.replaceAll(x -> x * 2); //UnsupportedOperationException
        thirdCopy.replaceAll(x -> x * 2);
        System.out.println(thirdCopy);
    }
}