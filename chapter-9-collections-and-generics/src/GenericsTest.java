import java.awt.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.util.ArrayList;
import java.util.List;

public class GenericsTest {
}

class Mammal {
    public List<CharSequence> play() {
        return new ArrayList<>();
    }
    public CharSequence sleep() {
        return new String();
    }
}
class Monkey extends Mammal {
    public ArrayList<CharSequence> play() {
        return new ArrayList<>();
    }
}
class Goat extends Mammal {
    //public List<String> play() {
      //  return new ArrayList<>();
    //} // DOES NOT COMPILE
    public String sleep() {
        return new String();
    }
}

class Box {
    public static <T> void prepare(T t) {
        System.out.println("Preparing " + t); }
    public static <T> T identity(T t) { return t; }
    //public static T noGood(T t) { return t; } // DOES NOT COMPILE
    //public static void noGood(T t) { return t; } // DOES NOT COMPILE
    public static <T> Crate<T> ship(T t) {
        System.out.println("Shipping " + t);
        return new Crate<T>();
    }
    public static <T> Crate shipOk(T t) {
        System.out.println("Shipping " + t);
        return new Crate();
    }

    public static void main(String[] args) {
        Box.<String>ship("package");
        Box.ship("package");
        Box.<String[]>ship(args);
    }
}

class Crate<T> {
}

/**
 * On line 1, T is Robot because that is what gets referenced when constructing a Crate.
 * On line 2, T is String because that is what is passed to the method
 */
class TrickyCrate<T> {
    public <T> T tricky(T t) { return t;}
    public static String crateName() {
        TrickyCrate<Robot> crate = new TrickyCrate<>();
        return crate.tricky("bot");
    }

    public static void main(String[] args) {
        System.out.println(crateName());
    }
}

record CrateRecord<T>(T contents) {
//record CrateRecord(T contents) { //// DOES NOT COMPILE
    @Override
    public T contents() {
        if (contents == null)
            throw new IllegalStateException("missing contents"); return contents;
    }
    static class Sparrow extends Bird { }
    static class Bird { }
    public static void printList(List<Object> list) {
        for (Object x: list)
            System.out.println(x); }
    public static void main(String[] args) {
        List<String> keywords = new ArrayList<>();
        keywords.add("java");
        //printList(keywords); // DOES NOT COMPILE
        List<Integer> numbers = new ArrayList<>();
        numbers.add(Integer.valueOf(42));
        //List<Object> objects = numbers; // DOES NOT COMPILE
        //objects.add("forty two");
        printListOk(keywords);
        System.out.println(keywords.get(0));

        List<?> x1 = new ArrayList<>();
        var x2 = new ArrayList<>();

        total(new ArrayList<Integer>());
    }

    public static long total(List<? extends Number> list) { long count = 0;
        for (Number number: list)
            count += number.longValue(); return count;
    }

    public static void printListOk(List<?> list) { for (Object x: list)
        System.out.println(x); }


        //List<? extends Bird> birds = new ArrayList<Bird>();
        //birds.add(new Sparrow()); // DOES NOT COMPILE
        //birds.add(new Bird()); // DOES NOT COMPILE
        public static void main2(String[] args) {
        //anyFlyer(new ArrayList<Goose>()); //Provided: ArrayList<Goose>
        anyFlyer(new ArrayList<Flyer>());
        //anyFlyer(new ArrayList<HangGlider>()); //Provided: ArrayList <HangGlider>
        groupOfFlyers(new ArrayList<Goose>());
        groupOfFlyers(new ArrayList<Flyer>());
        groupOfFlyers(new ArrayList<HangGlider>());

            List<? super IOException> exceptions = new ArrayList<Exception>();
            //exceptions.add(new Exception()); // DOES NOT COMPILE capture of ? super IOException
            exceptions.add(new IOException());
            exceptions.add(new FileNotFoundException());

            List<?> list1 = new ArrayList<A>();
            list1.add(null);
            //list1.add(new A());
            List<?> list2 = new ArrayList<B>();
            List<?> list3 = new ArrayList<C>();
            List<? extends A> list21 = new ArrayList<A>();
            List<? extends A> list22 = new ArrayList<B>();
            List<? extends A> list23 = new ArrayList<C>();
            List<? super A> list31 = new ArrayList<A>();
            //List<? super B> list5 = new ArrayList<? super B>(); //Wildcard type '? super B' cannot be instantiated directly
            //List<? super A> list32 = new ArrayList<B>(); //<? super A>
            //List<? super A> list33 = new ArrayList<C>(); //<? super A>
            //List<? extends B> list4 = new ArrayList<A>(); // DOES NOT COMPILE <? extends B>
            //List<? super B> list5 = new ArrayList<A>();
            //List<?> list6 = new ArrayList<? extends A>(); // DOES NOT COMPILE Wildcard type '? extends A' cannot be instantiated directly
            //<B extends A> B third(List<B> list) { return new B();} // DOES NOT COMPILE
    }

    <T> T first(List<? extends T> list) { return list.get(0);}
    //<T> <? extends T> second(List<? extends T> list) { return list.get(0);} // DOES NOT COMPILE Cannot return a value from a method with void result type
    //<B extends A> B third(List<B> list) { return new B();} // DOES NOT COMPILE
    //<X> void fifth(List<X super B> list) {} // DOES NOT COMPILE
    void fourth(List<? super B> list) {}
    private static void anyFlyer(List<Flyer> flyer) {}
    private static void groupOfFlyers(List<? extends Flyer> flyer) {}
}
interface Flyer { void fly(); }
class HangGlider implements Flyer { public void fly() {} }
class Goose implements Flyer { public void fly() {} }

class A {}
class B extends A {
    public class HashSet<T> {
    }
}
class C extends B {}

class LowerBound {
    public static void main(String[] args) {
        List<String> strings = new ArrayList<String>();
        strings.add("tweet");
        List<Object> objects = new ArrayList<Object>(strings);
        addSound(strings);
        addSound(objects);
        System.out.println(objects); //[tweet, quack]
        System.out.println(strings); //[tweet, quack]
        List<? super IOException> exceptions = new ArrayList<Exception>();
        //exceptions.add(new Exception()); // DOES NOT COMPILE capture of ? super IOException
        exceptions.add(new IOException());
        exceptions.add(new FileNotFoundException());
    }
    public static void addSound(List<? super String> list) { list.add("quack");
    }

}

class BoundTest {
    static void addSoundUnbounded(List<?> list) {
        //list.add("quack"); //capture of String immutable
    }
    static void addSoundUpperBounded(List<? extends Object> list) {
        //list.add("quack"); //capture of ? extends Object immutable
    }
    static void addSoundLowerBounded(List<? super String> list) {
        list.add("quack");
        //list.add('s'); //capture of ? super String
    }
    static void checkLowerBoundProblem() {
        List<? super IOException> exceptions = new ArrayList<Exception>();
        exceptions.add(new IOException());
        exceptions.add(new FileNotFoundException());
        exceptions.add(new FileAlreadyExistsException(""));
        //exceptions.add(new Exception()); //capture of ? super IOException
    }
    static void checkUpperBoundProblem() {
        List<? extends CrateRecord.Bird> birds = new ArrayList<CrateRecord.Bird>();
        //birds.add(new CrateRecord.Sparrow()); immutable
        //birds.add(new CrateRecord.Bird());
    }
}