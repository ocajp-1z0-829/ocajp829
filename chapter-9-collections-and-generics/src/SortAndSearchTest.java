import java.util.*;

public class SortAndSearchTest {
}
 class SortRabbits {
    static record Rabbit(int id) {}
    public static void main(String[] args) {
        List<Rabbit> rabbits = new ArrayList<>();
        rabbits.add(new Rabbit(3));
        //rabbits.add(new Rabbit(1)); Collections.sort(rabbits); // DOES NOT COMPILE
        Comparator<Rabbit> c = (r1, r2) -> r1.id - r2.id;
        Collections.sort(rabbits, c);
        System.out.println(rabbits); // [Rabbit[id=1], Rabbit[id=3]]

        Comparator<Rabbit> c1 = (r1, r2) -> r1.id - r2.id;
        Collections.sort(rabbits, c1);
        Collections.reverse(rabbits);
        System.out.println(rabbits); // [Rabbit[id=3], Rabbit[id=1]]

        var names = Arrays.asList("Fluffy", "Hoppy");
        Comparator<String> c2 = Comparator.reverseOrder();
        var index = Collections.binarySearch(names, "Hoppy", c2);
        System.out.println(index); //undefined or -1
    } }

class UseTreeSet {
    static class Rabbit{ int id; }
    public static void main(String[] args) {
        Set<Duck> ducks = new TreeSet<>();
        ducks.add(new Duck("Duck", 1));
        //Set<Rabbit> rabbits = new TreeSet<>();
        //class UseTreeSet$Rabbit cannot be cast to class java.lang.Comparable (UseTreeSet$Rabbit is in unnamed module of loader 'app'; java.lang.Comparable is in module java.base of loader 'bootstrap')
        //rabbits.add(new Rabbit()); // ClassCastException

        //Set<Rabbit> rabbits = new TreeSet<>(); //CCE
        Set<Rabbit> rabbits = new TreeSet<>((r1, r2) -> r1.id - r2.id);
        rabbits.add(new Rabbit());
    }}


