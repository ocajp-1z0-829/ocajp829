import java.util.ArrayList;
import java.util.Collections;

public class ComparableTest {
}

class DuckComparable implements Comparable<DuckComparable> {
    private String name;
    public DuckComparable(String name) {
        this.name = name; }
    public String toString() { return name;
    }
    public int compareTo(DuckComparable d) {
        //Since Duck is comparing objects of type String and the String class already has a compareTo() method
        return name.compareTo(d.name); // sorts ascendingly by name
    }
    public static void main(String[] args) {
        var ducks = new ArrayList<DuckComparable>();
        ducks.add(new DuckComparable("Quack"));
        ducks.add(new DuckComparable("Puddles"));
        Collections.sort(ducks); // sort by name
        System.out.println(ducks); // [Puddles, Quack]
    }}

class Animal implements Comparable<Animal> {
    private int id;
    public int compareTo(Animal a) {
        //Remember that id - a.id sorts in ascending order, and a.id - id sorts in descending order.
        return id - a.id; // sorts ascending by id
    }
    public static void main(String[] args) {
        var a1 = new Animal();
        var a2 = new Animal();
        a1.id = 5;
        a2.id = 7;
        System.out.println(a1.compareTo(a2)); //-2
        System.out.println(a1.compareTo(a1)); //0
        System.out.println(a2.compareTo(a1)); //2
        System.out.println(Integer.compare(a1.id, a2.id)); //-1
    } }

class LegacyDuck implements Comparable { private String name;
    public int compareTo(Object obj) {
        LegacyDuck d = (LegacyDuck) obj; // cast because no generics
        return name.compareTo(d.name); }
}

class MissingDuck implements Comparable<MissingDuck> { private String name;
    public int compareTo(MissingDuck quack) {
        if (quack == null)
            throw new IllegalArgumentException("Poorly formed duck!");
        if (this.name == null && quack.name == null)
            return 0;
        else if (this.name == null) return -1;
        else if (quack.name == null) return 1;
        else return name.compareTo(quack.name);
    } }

/**
 * You might be sorting Product objects by name, but names are not unique.
 * The compareTo() method does not have to be consistent with equals.
 * One way to fix that is to use a Comparator to define the sort elsewhere
 */
class Product implements Comparable<Product> {
    private int id;
    private String name;
    public int hashCode() { return id; }
    public boolean equals(Object obj) {
        if(!(obj instanceof Product)) return false;
        var other = (Product) obj;
        return this.id == other.id;
    }
    public int compareTo(Product obj) {
        return this.name.compareTo(obj.name); }
    public static void main(String[] args) {
        var a1 = new Product();
        var a2 = new Product();
        a1.id = 5;
        a1.name = "Gamma";
        a2.id = 5;
        a2.name ="Alfa";
        System.out.println(a1.compareTo(a2)); //6
        System.out.println(a1.equals(a2)); // true
        System.out.println(a1.compareTo(a1)); //0
        System.out.println(a2.compareTo(a1)); //-6
        System.out.println(Integer.compare(a1.id, a2.id)); //0
    }
}

