package collection;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class SetTest {
    public static void main(String[] args) {
        Set<Character> letters = Set.of('z', 'o' /*, 'o'*/); //IllegalArgumentException: duplicate element: o
        Set<Character> copy = Set.copyOf(letters);

        Set<Integer> set = new TreeSet<>();
        boolean b1 = set.add(66); // true
        boolean b2 = set.add(10); // true
        boolean b3 = set.add(66); // false
        boolean b4 = set.add(8); // true
        set.forEach(System.out::println);

        Set<Integer> set1 = new HashSet<>();
        boolean b11 = set1.add(66); // true
        boolean b21 = set1.add(10); // true
        boolean b31 = set1.add(66); // false
        boolean b41 = set1.add(8); // true
        set1.forEach(System.out::println);


    }
}
