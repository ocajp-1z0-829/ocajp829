package collection;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;

public class QueueTest {
    public static void main(String[] args) {
        Queue<Integer> queue = new LinkedList<>();
        queue.add(10);
        queue.add(4);
        System.out.println(queue.remove()); // 10
        System.out.println(queue.peek()); // 4
        System.out.println(queue.element()); // 4
        System.out.println(queue.size()); // 1
        System.out.println(queue.poll()); // 4
        System.out.println(queue.poll()); // null
        System.out.println(queue.size()); // 0
        //System.out.println(queue.element()); // NoSuchElementException
        System.out.println(queue.peek()); //  null
        queue.offer(null);
        queue.add(null);
        //System.out.println(queue.remove()); // NoSuchElementException
    }
}

class DequeTest {
    public static void main(String[] args) {
        Deque<Integer> deque = new LinkedList<>();
        System.out.println(deque.offerFirst(10)); // true
        System.out.println(deque.offerLast(4)); // 10
        System.out.println(deque.offerLast(null)); // true
        deque.addLast(null); // void
        System.out.println(deque.peekFirst()); // 10
        System.out.println(deque.peekLast()); // 4
        System.out.println(deque.pollFirst()); // 10
        System.out.println(deque.pollLast()); // 4
        System.out.println(deque.pollFirst()); // null
        //System.out.println(deque.removeFirst()); // NoSuchElementException
        //System.out.println(deque.removeLast()); // NoSuchElementException
        System.out.println(deque.peekFirst()); // null
        //System.out.println(deque.getFirst()); // NoSuchElementException

    }
}

class StackTest {
    public static void main(String[] args) {
        Deque<Integer> stack = new ArrayDeque<>();
        stack.push(10);
        stack.push(null);
        System.out.println(stack.peek()); //4
        System.out.println(stack.poll()); //4
        System.out.println(stack.pop()); // 10
        //System.out.println(stack.pop()); // NoSuchElementException
        System.out.println(stack.poll()); // null
        System.out.println(stack.peek()); // null
    }
}