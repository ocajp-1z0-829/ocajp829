package collection;

import java.util.*;

public class CollectionTest {
    public static void main(String[] args) {
        Collection<String> birds = new ArrayList<>(); birds.add("hawk"); // [hawk]
        System.out.println(birds.contains("hawk")); // true
        System.out.println(birds.add("huaway")); // true
        System.out.println(birds.add("robin")); // false
        System.out.println(birds.removeIf(b -> b.startsWith("h")));
        System.out.println(birds.size());

        Collection<String> set = new HashSet<>();
        set.add("Wand");
        set.add("");
        set.removeIf(String::isEmpty); // s -> s.isEmpty()
        System.out.println(set); // [Wand]

        Collection<String> cats = List.of("Annie", "Ripley"); cats.forEach(System.out::println);
        cats.forEach(c -> System.out.println(c));

    }
}
