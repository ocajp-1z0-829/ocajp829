package collection;

import java.util.ArrayList;
import java.util.List;

public class ListTest {
    public static void main(String[] args) {

        List<String> list2 = new ArrayList<>();
        list2.add("hawk");
        list2.add("robin");
        Object[] objectArray = list2.toArray();
        String[] stringArray = list2.toArray(new String[0]);
        list2.clear();
        System.out.println(objectArray.length); // 2
        System.out.println(stringArray.length); // 2
        System.out.println(list2);

    }
}
