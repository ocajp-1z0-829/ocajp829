package collection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class HowListWorks {
    /**
     * Если коротко, то у каждого объекта типа AbstractList есть счётчик modCount. Каждая операция, которая изменяет список, такие как добавление элементов, удаление элементов, очистка списка, и т.п., этот счетчик инкрементирует.
     *
     * Те операции, которым нужно, чтобы список в ходе выполнения операции не менялся, например операция equals, сохраняют значение modCount до начала выполнения операции, и в конце операции обязательно сравнивают с текущим значением modCount. Если значения не совпали, то выбрасывается ConcurrentModificationException.
     *
     * Самый простой пример - вычисление хэш-кода:
     *
     *     public int hashCode() {
     *         int expectedModCount = modCount;
     *         int hash = hashCodeRange(0, size);
     *         checkForComodification(expectedModCount);
     *         return hash;
     *     }
     * Метод checkForComodification выбросит исключение, если текущее значение modCount не совпадёт с expectedModCount.
     *
     * Поищите по тексту классов выражение modCount++ - вы найдёте все те места, в которых меняется состояние объекта. Все те места, где ожидается неизменность объекта в ходе выполнения операции, содержат вызов checkForComodification.
     *
     * Как избежать?
     *
     * Простой, но дорогой в эксплуатации способ - обернуть все операции с коллекцией в RWLock. Сложный и недостоверный способ - прогнать вашу программу через статический анализатор и поискать race conditions. Если не найдёт, то скрестить пальцы и верить, что коллизий не будет :)
     */
    protected transient int modCount = 0;
    private static final int DEFAULT_CAPACITY = 10;
    private static final Object[] EMPTY_ELEMENTDATA = new Object[0];
    private static final Object[] DEFAULTCAPACITY_EMPTY_ELEMENTDATA = new Object[0];
    transient Object[] elementData;
    private int size;

    public HowListWorks(int initialCapacity) {
        if (initialCapacity > 0) {
            this.elementData = new Object[initialCapacity];
        } else {
            if (initialCapacity != 0) {
                throw new IllegalArgumentException("Illegal Capacity: " + initialCapacity);
            }

            this.elementData = EMPTY_ELEMENTDATA;
        }

    }

    public HowListWorks() {
        this.elementData = DEFAULTCAPACITY_EMPTY_ELEMENTDATA;
    }

    public static void main(String[] args) {
        String w1 = "One";
        String w2 = "Two";
        HowListWorks list = new HowListWorks();
        list.add(w1);
        list.add(w2);
    }

    public boolean add(String e) {
        ++this.modCount;
        this.add(e, this.elementData, this.size);
        return true;
    }

    private void add(String e, Object[] elementData, int s) {
        if (s == elementData.length) {
            elementData = this.grow();
        }

        elementData[s] = e;
        this.size = s + 1;
    }

    public void add(int index, String element) {
        this.rangeCheckForAdd(index);
        ++this.modCount;
        int s;
        Object[] elementData;
        if ((s = this.size) == (elementData = this.elementData).length) {
            elementData = this.grow();
        }

        System.arraycopy(elementData, index, elementData, index + 1, s - index);
        elementData[index] = element;
        this.size = s + 1;
    }

    private void rangeCheckForAdd(int index) {
        if (index > this.size || index < 0) {
            throw new IndexOutOfBoundsException(index);
        }
    }


    public void trimToSize() {
        ++this.modCount;
        if (this.size < this.elementData.length) {
            this.elementData = this.size == 0 ? EMPTY_ELEMENTDATA : Arrays.copyOf(this.elementData, this.size);
        }

    }

    public void ensureCapacity(int minCapacity) {
        if (minCapacity > this.elementData.length && (this.elementData != DEFAULTCAPACITY_EMPTY_ELEMENTDATA || minCapacity > 10)) {
            ++this.modCount;
            this.grow(minCapacity);
        }

    }

    private Object[] grow() {
        return this.grow(this.size + 1);
    }

    private Object[] grow(int minCapacity) {
        int oldCapacity = this.elementData.length;
        if (oldCapacity <= 0 && this.elementData == DEFAULTCAPACITY_EMPTY_ELEMENTDATA) {
            return this.elementData = new Object[Math.max(10, minCapacity)];
        } else {
            int newCapacity = newLength(oldCapacity, minCapacity - oldCapacity, oldCapacity >> 1);
            return this.elementData = Arrays.copyOf(this.elementData, newCapacity);
        }
    }

    public static int newLength(int oldLength, int minGrowth, int prefGrowth) {
        int prefLength = oldLength + Math.max(minGrowth, prefGrowth);
        return 0 < prefLength && prefLength <= 2147483639 ? prefLength : hugeLength(oldLength, minGrowth);
    }

    private static int hugeLength(int oldLength, int minGrowth) {
        int minLength = oldLength + minGrowth;
        if (minLength < 0) {
            throw new OutOfMemoryError("Required array length " + oldLength + " + " + minGrowth + " is too large");
        } else {
            return minLength <= 2147483639 ? 2147483639 : minLength;
        }
    }
}
