package collection;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.BiFunction;

public class MapClass {
    public static void main(String[] args) {
        System.out.println(Map.of("key1", "value1", "key2", "value2"));

        System.out.println(Map.ofEntries(
                Map.entry("key1", "value1"), Map.entry("key2", "value2")));

        Map<String, String> map = new HashMap<>();
        map.put("koala", "bamboo");
        map.put("lion", "meat");
        map.put("giraffe", "leaf");
        String food = map.get("koala"); // bamboo
        for (String key: map.keySet())
        System.out.print(key + ","); // koala,giraffe,lion,

        Map<String, String> treeMap = new TreeMap<>();
        treeMap.put("koala", "bamboo"); treeMap.put("lion", "meat"); treeMap.put("giraffe", "leaf");
        String treeMapFood = treeMap.get("koala"); // bamboo
        for (String key: treeMap.keySet())
        System.out.print(key + ","); // giraffe,koala,lion,
        System.out.println(treeMap.values());

        //System.out.println(map.contains("lion")); // DOES NOT COMPILE
        System.out.println(map.containsKey("lion")); // true
        System.out.println(map.containsValue("lion")); // false
        System.out.println(map.size()); // 3
        map.clear(); System.out.println(map.size()); // 0
        System.out.println(map.isEmpty()); // true

        BiFunction<String, String, String> mapper = (v1, v2) -> null;
        Map<String, String> favorites = new HashMap<>();
        favorites.put("Jenny", "Bus Tour");
        favorites.put("Tom", "Bus Tour");
        favorites.merge("Jenny", "Skyride", mapper);
        favorites.merge("Sam", "Skyride", mapper);
        System.out.println(favorites); // {Tom=Bus Tour, Sam=Skyride}
    }
}
