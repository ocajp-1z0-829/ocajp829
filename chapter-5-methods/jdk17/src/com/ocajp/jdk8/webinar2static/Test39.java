package com.ocajp.jdk8.webinar2static;

import java.util.Random;

//abstract class Test39 {
//final class Test39 {
public class Test39 {
//static class Test39 { //Modifier 'static' not allowed here
//protected class Test39 { //Modifier 'protected' not allowed here
//private class Test39 { //Modifier 'private' not allowed here
  static int a;
  final Object[] fudge = { null };
  //abstract int i; //Modifier 'abstract' not allowed here
  native void sneeze();
  static final double PI = 3.14;
  public static void main(String[] args) {
    System.out.println(howMany(true, true, true));
    //System.out.println(howMany(true, {true, true})); //Array initializer is not allowed here
    System.out.println(howMany(true, new boolean[2]));

    double i = new Random().nextGaussian();
    System.out.println(i);

    System.out.println(howMany(true));
  }

  public static int howMany(boolean b, boolean... args) {
    return args.length;
  }
}

class Q30 {
  static int[] a = {1,2,3,4};
  int[] b = {5,6,7,8};
  public static void main(String[] args) {
    Q30 t1 = new Q30(), t2 = new Q30();
    t1.a[3] = 10;
    System.out.println(t1.a[3]);
    t1.b[3] = 20;
    System.out.println(t1.a[3] + " " + t1.b[3]);
    t2.a[3] = 30;
    System.out.println(t1.a[3] + " " + t1.b[3] + " " + t2.a[3]);
    t2.b[3] = 40;
    System.out.println(
        t1.a[3] + " " + t1.b[3] + " " + t2.a[3] + " " + t2.b[3]
    );
  }
}

/**
 * Статический инициализатор не имеет доступа к нестатическим переменным инстанса
 */
class Q7 {
  final String value1 = "red";
  static String value2 = "blue";
  String value3 = "yellow";
  {
    //value1 = "green"; //Cannot assign a value to final variable 'value1'
    value2 = "purple";
    value3 = "orange";
  }

  static {
    //static String t = ""; //Modifier 'static' not allowed here
    //value1 = "magneta"; //Non-static field 'value1' cannot be referenced from a static context
    value2 = "cyan";
    //value3 = "turquoise"; //Non-static field 'value3' cannot be referenced from a static context
  }
  public static void main(String[] args) {
    Q7 q7 = new Q7();
  }
}

class Q29 {
  //private static final double value; //Variable 'value' might not have been initialized
  private static double value;
  public static int getValue() {
    return (int) value;
  }
  public static void main(String[] args) {
    System.out.println(getValue());
    System.out.println(value);
  }
}