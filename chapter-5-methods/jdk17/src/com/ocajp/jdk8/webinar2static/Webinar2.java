package com.ocajp.jdk8.webinar2static;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Статические члены класса создаются для класса
 * Члены инстанса для каждого объекта
 * Поэтому возникает ошибка при использование нестатических в статических даже переменных
 * Статический инициализатор не имеет доступа к нестатическим переменным инстанса
 */
public class Webinar2 {
  public static int count;
  public static void main(String[] args) {
    List l= Arrays.asList();
    l.remove(0); //UnsupportedOperationException
  }
  public int total;
  //public static double average = total / count; //Non-static field 'total' cannot be referenced from a static context
}
