package com.ocajp.jdk8.webinar3overloadandclasses;

class Message {
  String text = "Hello, world";
}
class MySuperclass {
  Message msg = new Message();
}

public class MyClass extends MySuperclass {
  public static void main(String[] args) {
    MyClass myClass = new MyClass();
    myClass.print();
  }

  public void print() {
    //final var c;// Cannot infer type: 'var' on variable without initializer
    final var e = 4;
    System.out.printf("%s %s %s",
        //text; //Cannot resolve symbol 'text'
        //Message.text; //Non-static field 'text' cannot be referenced from a static context
        msg.text,
        this.msg.text,
        super.msg.text
        //this.super.msg.text; //Cannot resolve symbol 'super'

    );
  }
}
