package com.ocajp.jdk8.webinar3overloadandclasses;

import java.util.List;

public class Webinar3 {
  private static void flipFlop(String str, int i, Integer iRef) {
    System.out.println("String, int, Integer");
  }

  private static void flipFlop(String str, int i, int iRef) {
    System.out.println("String, int, int");
  }

  private static void flipFlop(String str, int i) {
    System.out.println("String, int");
  }

  public static void main(String[] args) {
    flipFlop("", 0, 0);
    flipFlop("", 0, (Integer) 0);
    //flipFlop("", (Integer) 0, 0); //Ambiguous method call. Both
    //flipFlop("", (Integer) 0, (Integer) 0); //Ambiguous method call. Both
    flipFlop("", (Integer) 0);

    String t = null;
    System.out.print((String) null);
    System.out.print(t);
    //System.out.println(null); //Ambiguous method call. Both
  }

  public void fly(int numMiles) {}
  public void fly(Integer numMiles) {}

  //public void walk(List<String> strings){} //'walk(List<String>)' clashes with 'walk(List<Integer>)'; both methods
  // have same erasure
  public void walk(List<Integer> strings){}
}

/**
 * Java может делать только одно преобразование
 * н-р в Integer или в Long, но не два int->long->Long как здесь play(4);
 */
class TooManyConversions {
  public static void play(Long l) {}
  public static void play(Long... l) {}

  public static void main(String[] args) {
    //play(4); //Cannot resolve method 'play(int)'
    play(4L);
  }
}

class OverloadedMethod {
  public static void main(String[] args) {
    OverloadedMethod om = new OverloadedMethod();
    //om.test(null); //Ambiguous method call. Both char[] and Integer
  }

  private void test(Object o) {
    System.out.println("Object");
    //this = new OverloadedMethod();

  }

  private void test(char[] ch) {
    System.out.println("char[]");
  }

  private void test(Integer i) {
    System.out.println("Integer");
  }
}