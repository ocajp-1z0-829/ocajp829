package com.ocajp.jdk8.webinar3overloadandclasses;

public class Beetle extends Insect {
  protected int numberOfLegs = 6;
  short age = 3;
  public void printData() {
    System.out.println(this.label); //buggy
    System.out.println(super.label); //buggy
    System.out.println(this.age); //3
    //System.out.println(super.age); //Cannot resolve symbol 'age'
    System.out.println(numberOfLegs); //6
  }

  public static void main(String[] args) {
    new Beetle().printData();
  }
}

class Insect {
  protected int numberOfLegs = 4;
  String label = "buggy";
}