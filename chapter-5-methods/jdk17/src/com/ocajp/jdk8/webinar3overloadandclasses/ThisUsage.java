package com.ocajp.jdk8.webinar3overloadandclasses;

public class ThisUsage {
  int planets;
  static int suns;
  //static int e = planets / suns; //Non-static field 'planets' cannot be referenced from a static context
  //static final int c = planets / suns; //Non-static field 'planets' cannot be referenced from a static context
  public void gaze() {
    int i;
    i = this.planets;
    i = this.suns;
    //this = new ThisUsage(); //this is final
    //this.i = 4; // i is local
    this.suns = planets;
    int f = suns / planets;
  }
  public static void main(String[] args) {
    ThisUsage thisUsage = new ThisUsage();
  }
}
