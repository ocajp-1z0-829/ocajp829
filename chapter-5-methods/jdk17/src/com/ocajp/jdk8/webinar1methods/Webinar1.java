package com.ocajp.jdk8.webinar1methods;

public class Webinar1 {
  public static void main(String[] args) {
    vararg(new int[]{1, 2, 3}); //int это не объект
  }
  static void vararg(Object... o) {
    System.out.println("Object: " + o);
  }

}

class ParkTrip {
  public void skip1() {}
  //default void skip2() {} // DOES NOT COMPILE Extension methods can only be used within an interface
  //void public skip3() {} // DOES NOT COMPILE Invalid method declaration; return type required
  void skip4() {}
}

class Exercise {
  public void bike1() {}
  public final void bike2() {}
  public static final void bike3() {}
  public final static void bike4() {}
  //public modifier void bike5() {} // DOES NOT COMPILE
  //public void final bike6() {} // DOES NOT COMPILE
  final public void bike7() {}
}

class __ {
  void __() {

  }
} //As of Java 9, '_' is a keyword, and may not be used as an identifier