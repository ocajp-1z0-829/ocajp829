package com.ocajp.jdk8.webinar1methods.packageB;

import com.ocajp.jdk8.webinar1methods.packageA.A;
import java.util.List;

abstract /*final*/ class B extends A { //Illegal combination of modifiers: 'final' and 'abstract'
  static void stat() {
    System.out.println(cat);
  }

  @Override
  public void doIt() {
    System.out.println("B");
  }

  void action(A obj1, B obj2, C obj3) { //reftype ABC
    System.out.println(cat);
    pf = 10;
    /**
     * obj1.pf из того же пакета где A? - нет, из Наследника - да, из того же наследника - нет.
     * Наследник B, а тут дергаем на том же самом A, A не наследник A;
     */
    //obj1.pf = 10;
    obj2.pf = 10; //дергаем из наследника - да, на наследнике от A - да
    obj3.pf = 10; //дергаем из наследника - да, на наследнике от A - да, через C -> B -> A
    //System.out.println(obj3.privat);//'privat' has private access in 'com.ocajp.jdk8.webinar1methods.packageB.C'
  }
}

class C extends B {
  private  String privat = "private";

  public static void main(String[] args) {
    C c = new C();
    c.action(new A(), null, new C());
  }

  @Override
  public void doIt() {
    System.out.println("C");
  }

  void action(A obj1, B obj2, C obj3) {
    pf = 10; //наследует от B который наследнует от A.
    //obj1.pf = 10; //из того же пакета что A - нет, это не наследник это предок, должен быть наследником A
    //obj2.pf = 10; //из того же пакета что A - нет, это не наследник это предок, должен быть наследником A
    obj3.pf = 10; //у себя же, из наслденика в наследнике

    doIt(); //C
    super.doIt(); //B
    //super.super.doIt(); //Cannot resolve symbol 'super'
    //this.super.doIt(); //Cannot resolve symbol 'super'
    //A.this.doIt(); //A' is not an enclosing class
    ((A) this).doIt(); //C
  }
}

/**
 * Отлетает так как находится в другом пакете и не является наследником A
 */
class D {
  void action(A obj1, B obj2, C obj3) {
    //pf = 10; //Cannot resolve symbol 'pf'
    //obj1.pf = 10; //'pf' has protected access in 'com.ocajp.jdk8.webinar1methods.packageA.A'
    //obj2.pf = 10;
    //obj3.pf = 10;
  }
}
