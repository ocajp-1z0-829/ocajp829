package com.ocajp.jdk8.webinar1methods.pond.goose;

import com.ocajp.jdk8.webinar1methods.pond.shore.Bird;

public class Goose extends Bird {
  public void helpGooseSwim() {
    Goose other = new Goose();
    other.floatInWater();
    System.out.println(other.text);
  }

  /**
   * только наследование или один пакет позволит использовать protected члены класса при этом смотрит по reftype
   */
  public void helpOtherBirdSwim() {
    Bird other = new Goose(); //смотрит по reftype
    //other.floatInWater(); //'floatInWater()' has protected access in 'com.ocajp.jdk8.webinar1methods.pond.shore.Bird'
    //System.out.println(other.text); //'text' has protected access in 'com.ocajp.jdk8.webinar1methods.pond.shore.Bird'
  }
}
