package com.ocajp.jdk8.webinar1methods.pond.duck;

import com.ocajp.jdk8.webinar1methods.pond.goose.Goose;

public class GooseWatcher {
  public void watch() {
    Goose goose = new Goose();
    //goose.floatInWater(); //'floatInWater()' has protected access in 'com.ocajp.jdk8.webinar1methods.pond.shore.Bird'
  }
}
