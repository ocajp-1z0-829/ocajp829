package com.ocajp.jdk8.webinar1methods.pond.shore;

public class Bird {
  protected String text = "floating";
  protected void floatInWater() {
    System.out.println(text);
  }
}
