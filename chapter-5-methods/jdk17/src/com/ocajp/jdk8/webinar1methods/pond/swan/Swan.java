package com.ocajp.jdk8.webinar1methods.pond.swan;

import com.ocajp.jdk8.webinar1methods.pond.shore.Bird;

public class Swan extends Bird {
  public void swim() {
    floatInWater();
    System.out.println(text);
  }

  public void helpOtherSwanSwim() {
    Swan other = new Swan();
    other.floatInWater();
    System.out.println(other.text);
  }

  /**
   * только наследование или один пакет позволит использовать protected члены класса при этом смотрит по reftype
   */
  public void helpOtherBirdSwim() {
    Bird other = new Bird();
    //other.floatInWater(); //'floatInWater()' has protected access in 'com.ocajp.jdk8.webinar1methods.pond.shore.Bird'
    //System.out.println(other.text); //'text' has protected access in 'com.ocajp.jdk8.webinar1methods.pond.shore.Bird'
  }
}