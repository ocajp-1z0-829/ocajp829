package com.ocajp.jdk8.webinar1methods.b;

import com.ocajp.jdk8.webinar1methods.a.A;

public class B extends A {
  public static void main(String[] args) {
    A a = new A(); //AA
    B b = new B(); //BB
    A c = new B(); //AB
    System.out.println(b.x);
    //System.out.println(a.x);
    //System.out.println(c.x);
    System.out.println(((B) c).x);
  }
}
