package com.ocajp.jdk17;

public class Static {
  public static void main(String[] args) {
    System.out.println(Snake.hiss); //2
    Snake.hiss = 4;
    System.out.println(Snake.hiss); //4
    Snake snake1 = new Snake();
    Snake snake2 = new Snake();
    snake1.hiss = 6;
    snake2.hiss = 5;
    System.out.println(Snake.hiss); //5
  }
}

