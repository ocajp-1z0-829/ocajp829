package com.ocajp.jdk17;

public class Varargs {
  public static void walk1(int... steps) {
    int[] step2 = steps; // Not necessary, but shows steps is of type int[]
    System.out.print(step2.length);
  }

  public static void walkDog(int start, int... steps) {
      System.out.println(steps.length); //NPE is walkDog(1, null);
    }

  public static void main(String[] args) {
    walk1(1, 2, 3);
    walk1();

    walkDog(1); // 0
    walkDog(1, 2); // 1
    walkDog(1, 2, 3); // 2
    walkDog(1, new int[] {4, 5}); // 2

    //Since null isn’t an int, Java treats it as an array reference that happens to be null
    //walkDog(1, null); //NPE Cannot read the array length because "steps" is null
  }
}
