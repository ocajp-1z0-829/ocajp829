package com.ocajp.jdk17;

/**
 * Labels are not allowed for methods and fields
 */
public class Test {
  public static void main(String[] args) {
    juggle(true, true, true);
    //juggle(true, {true, true}); //Array initializer is not allowed here
    juggle(true, new boolean[2]);
  }

  public static final int juggle(boolean b, boolean... b2) {
    return b2.length;
  }

  final void method() {
  }

  class StringBuilders {
    public static StringBuilder work(StringBuilder a, StringBuilder b) {
      a = new StringBuilder("a");
      b.append("b");
      return a;
    }

    public static void main(String[] args) {
      var s1 = new StringBuilder("s1");
      var s2 = new StringBuilder("s2");
      var s3 = work(s1, s2);
      System.out.println("s1 = " + s1);
      System.out.println("s2 = " + s2);
      System.out.println("s3 = " + s3);
    }
  }

  class Order3 {
    static String value2 = "blue";

    static {
      //value1 = "green"; //Non-static field 'value1' cannot be referenced from a static context
      value2 = "purple";
      //value3 = "orange"; //Non-static field 'value3' cannot be referenced from a static context
      //value1 = "magenta"; //Non-static field 'value1' cannot be referenced from a static context
      value2 = "cyan";
      //value3 = "turquoise"; //Non-static field 'value3' cannot be referenced from a static context
    }

    final String value1 = "red";
    String value3 = "yellow";

    {
      //value1 = "green"; //Cannot assign a value to final variable 'value1'
      value2 = "purple";
      value3 = "orange";
      //value1 = "magenta"; //Cannot assign a value to final variable 'value1'
      value2 = "cyan";
      value3 = "turquoise";
    }
  }
}