package com.ocajp.jdk17;

/**
 * The compiler does not apply a default value to final variables, though. A
 * final instance or final static variable must receive a value when it is
 * declared or as part of initialization.
 */
public class PolarBear {
  final int age = 10;
  final int fishEaten;
  //final int fish; //Variable 'fish' might not have been initialized
  final String name;
  { fishEaten = 10; }
  public PolarBear() {
    //this(true); //Variable 'name' might already have been assigned to
    name = "Robert";
  }

  public PolarBear(boolean is) {
    name = "Robert";
  }
}

class TestPolarBear {
  public static void main(String[] args) {
    PolarBear polarBear = new PolarBear();
    System.out.println(polarBear);
    PolarBear polarBear2 = new PolarBear();
    System.out.println(polarBear2);
  }
}