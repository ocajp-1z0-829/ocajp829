package com.ocajp.jdk17;

public class VarargsDemo {
  public static void main(String[] args) {
    fun();
    fun(1);
  }

  //varargs method with float datatype
  static void fun(float... x) {
    System.out.println("float varargs");
  }

  //varargs method with int datatype
  static void fun(int... x) {
    System.out.println("int varargs");
  }

  //varargs method with double datatype
  static void fun(double... x) {
    System.out.println("double varargs");
  }

  //varargs method with double datatype
  static void fun(Object x) {
    System.out.println("Object");
  }

  //static void fun(int n, int ... a) {} //Ambiguous method call. fun(1);

  //static void fun(boolean ... a){} //Ambiguous method call. fun(); Both because both the data types are different
}

class OverloadedMethod{
  public void test(String str) {
    System.out.println("String");
  }

  public void test(StringBuffer obj) {
    System.out.println("Object");
  }

  public void test(long lng) {
    System.out.println("Long");
  }

  public void test(Integer integer) {
    System.out.println("Integer");
  }

  public static void main(String[] args) {
    OverloadedMethod obj = new OverloadedMethod();
    obj.test(1);
    //obj.test(null); //Ambiguous method call
  }
}

class OverloadedMethod2{
  public void test(Integer i) {
    System.out.println("Integer");
  }
  public void test(Long l) {
    System.out.println("Long");
  }

  /**
   * Compile time error. It is breaking rule "You cannot widen and then box"
   * For short primitive type to be resolved to either Integer or Long wrapper class, it has to
   * first widen short to int and then box it to Integer, which is not possible.
   * @param args
   */
  public static void main(String[] args) {
    OverloadedMethod2 obj = new OverloadedMethod2();
    short s = 10;
    //obj.test(s); //Cannot resolve method 'test(short)'
    obj.test((int) s);
  }
}

class OverloadedMethod3 {
  public void test(Character c) {
    System.out.println("Character");
  }
  public void test(Integer i) {
    System.out.println("Integer");
  }
  public void test(Object o) {
    System.out.println("Object");
  }

  /**
   * You cannot widen and then box" but "You can box and then widen
   * @param args
   */
  public static void main(String[] args) {
    OverloadedMethod3 obj = new OverloadedMethod3();
    byte b = 10;
    obj.test(b);
  }
}

