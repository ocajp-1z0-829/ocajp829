package com.ocajp.jdk17;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class TestConcurrency {
  public static void main(String[] args) throws InterruptedException {
    ExecutorService service = null;
    Runnable task2 = () -> {
      for (int i = 0; i < 3; i++) {
        try {
          Thread.sleep(3000);
          System.out.println("test: " + Thread.currentThread().getName());
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    };
    try {
      service = Executors.newSingleThreadExecutor();
      service.execute(task2);
      service.execute(task2);
      service.execute(task2);
    } finally {
      if (service != null)
        service.shutdown();
    }
    service.awaitTermination(2, TimeUnit.SECONDS);
    System.out.println("Done");
  }
}
