package com.ocajp.jdk17;

import java.util.List;

public class Methods {
  public static void main(String[] args) {
    List d = (List) new Object();
    //System.out.println(d); //ClassCastException: class java.lang.Object cannot be cast to class java.util.List
  }

  String $_hike8(int a) {
    if (1 < 2) return "orange";
    final int[] friends = new int[5];
    int[] friends2 = friends;
    friends2 = null;

    return "apple"; // COMPILER WARNING
  }
}
