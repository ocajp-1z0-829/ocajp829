package com.ocajp;

import java.util.concurrent.locks.Lock;

public class Tests {
    final int i1 = 4;
    //final int i2;
    //final static int s1;
    final static int s2 = 3;

    public static void main(String[] args) {
        final var var = 1;
    }
}

class Q4_Q7 {
    public static void main(String[] args) {
        final int song1 = 6;
        final long song2 = 6;
        final double song3 = 6;
        final Integer song4 = 6;
        //final Long song5 = 6;
        //final Double song6 = 6;

        System.out.println(juggle(true, true, true));
        //juggle(true, {true, true});
        System.out.println(juggle(true, new boolean[2]));
    }

    public static int juggle(boolean b, boolean... b2) {
        return b2.length;
    }
}

class Q13 {
    public static class RopeSwing {
        private static Rope rope1 = new Rope();
        private static Rope rope2 = new Rope();

        {
            System.out.println(rope1.length);
        }

        public static void main(String[] args) {
            rope1.length = 2;
            rope2.length = 8;
            System.out.println(rope1.length);
        }
    }

    public static class Rope {
        public static int length = 0;
    }
}