package streams;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.BinaryOperator;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TerminalOperationsTest {
    public static void main(String[] args) {
        Stream<Double> randoms = Stream.generate(Math::random); //infinite
        Stream<Integer> oddNumbers = Stream.iterate(1, n -> n + 2); //infinite
        Stream<Integer> oddNumberUnder100 = Stream.iterate(1, //1 seed
                n -> n < 100, //2 4... Predicate to specify when done
                n -> n + 2); //3 5... UnaryOperator to get next value
        oddNumberUnder100.forEach(System.out::println);
        //System.out.println(oddNumberUnder100.count()); //Stream has already been linked or consumed

        Stream<String> s = Stream.of("monkey", "gorilla", "bonobo");
        System.out.println(s.count()); // 3

        s = Stream.of("ape", "gorilla", "bonobo");
        Optional<String> min = s.min((s1, s2) -> s1.length() - s2.length());
        min.ifPresent(System.out::println); // ape

        Optional<?> minEmpty = Stream.empty().min((s1, s2) -> 0);
        System.out.println(minEmpty.isPresent()); // false

        s = Stream.of("monkey", "gorilla", "bonobo");
        Stream<String> infinite = Stream.generate(() -> "chimp");
        s.findAny().ifPresent(System.out::println); // monkey (usually)
        infinite.findAny().ifPresent(System.out::println); // chimp

        var list = List.of("monkey", "2", "chimp");
        infinite = Stream.generate(() -> "chimp");
        Predicate<String> pred = x -> Character.isLetter(x.charAt(0));
        System.out.println(list.stream().anyMatch(pred)); // true
        System.out.println(list.stream().allMatch(pred)); // false
        System.out.println(list.stream().noneMatch(pred)); // false
        System.out.println(infinite.anyMatch(pred)); // true
        //System.out.println(infinite.noneMatch(pred)); // false
        //System.out.println(infinite.allMatch(pred)); // infinite
    }
}

class RedactionTest {
    public static void main(String[] args) {
        var array = new String[] { "w", "o", "l", "f" };
        var result = "";
        for (var s: array)
            result = result + s;
        System.out.println(result); // wolf

        Stream<String> stream = Stream.of("w", "o", "l", "f");
        String word = stream.reduce("", (s, c) -> s + c);
        System.out.println(word); // wolf
        stream = Stream.of("w", "o", "l", "f");
        word = stream.reduce("", String::concat);
        System.out.println(word); // wolf

        Stream<Integer> streamInt = Stream.of(3, 5, 6);
        System.out.println(streamInt.reduce(1, (a, b) -> a*b)); // 90

        BinaryOperator<Integer> op = (a, b) -> a * b;
        Stream<Integer> empty = Stream.empty();
        Stream<Integer> oneElement = Stream.of(3);
        Stream<Integer> threeElements = Stream.of(3, 5, 6);
        empty.reduce(op).ifPresent(System.out::println); // no output
        oneElement.reduce(op).ifPresent(System.out::println); // 3
        threeElements.reduce(op).ifPresent(System.out::println); // 90

        stream = Stream.of("w", "o", "l", "f!");
        int length = stream.reduce(0, (i, s) -> i+s.length(), (a, b) -> a+b);
        System.out.println(length); // 5
    }
}

class CollectorTest {
    public static void main(String[] args) {
        Stream<String> stream = Stream.of("w", "o", "l", "f");
        StringBuilder word = stream.collect(
                StringBuilder::new,
                StringBuilder::append,
                StringBuilder::append);
        System.out.println(word); // wolf

        stream = Stream.of("w", "o", "l", "f");
        TreeSet<String> set = stream.collect( TreeSet::new,
                TreeSet::add,
                TreeSet::addAll);
        System.out.println(set); // [f, l, o, w]

        stream = Stream.of("w", "o", "l", "f");
        set = stream.collect(Collectors.toCollection(TreeSet::new));
        System.out.println(set); // [f, l, o, w]

        stream = Stream.of("w", "o", "l", "f");
        Set<String> setStr = stream.collect(Collectors.toSet());
        System.out.println(setStr); // [f, w, l, o]
    }
}