package streams;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

public class InternediateOperations {
    public static void main(String[] args) {
        Stream<String> s = Stream.of("monkey", "gorilla", "bonobo");
        s.filter(x -> x.startsWith("m"))
                .forEach(System.out::print); // monkey
        System.out.println();

        s = Stream.of("duck", "duck", "duck", "goose"); s.distinct()
                .forEach(System.out::print); // duckgoose
        System.out.println();

        Stream<Integer> integerStream = Stream.iterate(1, n -> n + 1);
        integerStream.skip(5).limit(2).forEach(System.out::print); // 67
        System.out.println();

        List<String> zero = List.of();
        var one = List.of("Bonobo");
        var two = List.of("Mama Gorilla", "Baby Gorilla");
        Stream<List<String>> animals = Stream.of(zero, one, two);
        animals.flatMap(m -> m.stream()) .forEach(System.out::println);

        var one1 = Stream.of("Bonobo");
        var two1 = Stream.of("Mama Gorilla", "Baby Gorilla");
        Stream.concat(one1, two1) .forEach(System.out::println);

        s = Stream.of("brown-", "bear-");
        s.sorted()
                .forEach(System.out::print); // bear-brown-

        s = Stream.of("brown bear-", "grizzly-");
        s.sorted(Comparator.reverseOrder())
                .forEach(System.out::print); // grizzly-brown bear-

        s = Stream.of("brown bear-", "grizzly-");
        //s.sorted(() -> Comparator.reverseOrder()); // DOES NOT COMPILE
        //s.sorted(Comparator::reverseOrder); // DOES NOT COMPILE
        System.out.println();

        var stream = Stream.of("black bear", "brown bear", "grizzly");
        long count = stream.filter(str -> str.startsWith("g"))
                .peek(System.out::println).count(); // grizzly
        System.out.println(count); // 1

        var numbers = new ArrayList<>();
        var letters = new ArrayList<>();
        numbers.add(1);
        letters.add('a');
        Stream<List<?>> streams = Stream.of(numbers, letters);
        streams.forEach(a -> System.out.println(a.get(0).getClass())); // 11

        Stream<List<?>> bad = Stream.of(numbers, letters);
        bad.peek(x -> x.remove(0))
                .map(List::size).forEach(System.out::print); // 00
    }
}
