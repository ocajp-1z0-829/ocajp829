package streams;

import com.sun.source.tree.Tree;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class CollectorsTest {
    public static void main(String[] args) {
        var ohMy = Stream.of("lions", "tigers", "bears");
        String resultStr = ohMy.collect(Collectors.joining(", "));
        System.out.println(resultStr); // lions, tigers, bears

        ohMy = Stream.of("lions", "tigers", "bears");
        Double resultD = ohMy.collect(Collectors.averagingLong(String::length));
        System.out.println(resultD); // 5.333333333333333

        ohMy = Stream.of("lions", "tigers", "bears");
        TreeSet<String> result = ohMy
                .filter(s -> s.startsWith("t"))
                .collect(Collectors.toCollection(TreeSet::new));
        System.out.println(result); // [tigers]

        ohMy = Stream.of("lions", "tigers", "bears");
        Map<String, Integer> map = ohMy.collect(
                Collectors.toMap(s -> s, String::length));
        System.out.println(map); // {lions=5, bears=5, tigers=6}

        ohMy = Stream.of("lions", "tigers", "bears");
        Map<Integer, Long> mapIL = ohMy.collect(
                Collectors.groupingBy( String::length, Collectors.counting()));
        System.out.println(mapIL); // {5=2, 6=1}

        ohMy = Stream.of("lions", "tigers", "bears");
        Map<Boolean, Set<String>> mapBS = ohMy.collect(
                Collectors.partitioningBy( s -> s.length() <= 7, Collectors.toSet()));
        System.out.println(mapBS); // {false=[], true=[lions, tigers, bears]}

        ohMy = Stream.of("lions", "tigers", "bears", "abcdefg");
        Map<Integer, Optional<Character>> mapIOC = ohMy.collect(
                Collectors.groupingBy(
                        String::length,
                        Collectors.mapping(
                                s -> s.charAt(0),
                                Collectors.minBy((a, b) -> a - b)
                        )
                )
        );
        System.out.println(mapIOC); // {5=Optional[b], 6=Optional[t]}

        var list = List.of("x", "y", "z");
        Separations separations = list.stream()
                .collect(Collectors.teeing(
                        Collectors.joining(" "),
                        Collectors.joining(","),
                        (s, c) -> new Separations(s, c)));
        System.out.println(separations);

        System.out.println(Math.pow(16, 2)*4 + Math.pow(16,1)*4 + Math.pow(16,0)*4);
        System.out.println(Integer.parseInt("444", 16));
        System.out.println(Math.pow(16, 1)*3 + Math.pow(16, 0)*2);

        Stream.of(1, 2, 3, 4, 2, 5)
                .dropWhile(x -> x >= 3)
                .forEach(System.out::println);

        IntStream.range(0, 100000)
                .parallel()
                .filter(x -> x % 10000 == 0)
                .map(x -> x / 10000)
                .forEach(System.out::print);
                // 5, 6, 7, 3, 4, 8, 0, 9, 1, 2
        System.out.println();
        IntStream.range(0, 100000)
                .parallel()
                .filter(x -> x % 10000 == 0)
                .map(x -> x / 10000)
                .forEachOrdered(System.out::print);
                // 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
    }
}

record Separations(String spaceSeparated, String commaSeparated) {}