package streams;

import java.util.List;
import java.util.Spliterator;
import java.util.stream.Stream;

public class SpliteratorTest {
    public static void main(String[] args) {
        var stream = List.of("bird-", "bunny-", "cat-", "dog-", "fish-", "lamb-", "mouse-");
        Spliterator<String> originalBagOfFood = stream.spliterator();
        System.out.println(originalBagOfFood.getExactSizeIfKnown()); // 7

        Spliterator<String> emmasBag = originalBagOfFood.trySplit();
        emmasBag.forEachRemaining(System.out::print); // bird-bunny-cat-
        System.out.println();

        Spliterator<String> jillsBag = originalBagOfFood.trySplit();
        jillsBag.tryAdvance(System.out::print); // dog-
        System.out.println();
        jillsBag.forEachRemaining(System.out::print); // fish-
        System.out.println();
        originalBagOfFood.forEachRemaining(System.out::print); // lamb-mouse-
        System.out.println();

        var originalBag = Stream.iterate(1, n -> ++n).spliterator();
        Spliterator<Integer> newBag = originalBag.trySplit();
        newBag.tryAdvance(System.out::print); // 1
        newBag.tryAdvance(System.out::print); // 2
        newBag.tryAdvance(System.out::print); // 3
        newBag.tryAdvance(System.out::print); // 4
    }
}
