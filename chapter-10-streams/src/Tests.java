import streams.PrimitiveStreamsTest;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.*;

public class Tests {
    public static void main(String[] args) {
        var stream = Stream.iterate("", (s) -> s + "1");
        System.out.println(stream.limit(2).map(x -> x + "2").collect(Collectors.joining()));

        Predicate<String> predicate = s -> s.length()> 3;
        var streamIter = Stream.iterate("-",
                s -> ! s.isEmpty(), (s) -> s + s);
        var b1 = streamIter.noneMatch(predicate);
        streamIter = Stream.iterate("-",
                s -> ! s.isEmpty(), (s) -> s + s);
        var b2 = streamIter.allMatch(predicate);
        streamIter = Stream.iterate("-",
                s -> ! s.isEmpty(), (s) -> s + s);
        var b3 = streamIter.anyMatch(predicate);
        System.out.println(b1 + " " + b2 + " " + b3);
    }
}

class Q1 {
    public static void main(String[] args) {
        var stream = Stream.iterate("", (s) -> s + "1");
        stream.limit(3).forEach(System.out::println);
        System.out.println();
        //stream.limit(2).peek(System.out::print).map(x -> x + "2")
          //      .peek(a -> System.out.println()).forEach(System.out::print);
    }
}

class Q2 {
    public static void main(String[] args) {
        Predicate<String> predicate = s -> s.startsWith("g");
        var stream1 = Stream.generate(() -> "growl!");
        var stream2 = Stream.generate(() -> "growl!");
        var stream3 = Stream.generate(() -> "growl!");
        var b1 = stream1.anyMatch(predicate);
        System.out.println(b1); // true
        var b2 = stream2.allMatch(predicate);
        System.out.println(b2); // hangs as infinite
        var b3 = stream3.noneMatch(predicate);
        System.out.println(b3); // false
    }
}

class Q3 {
    public static void main(String[] args) {
        /**
         * IllegalStateException: stream has already been operated upon or closed
         */
        Predicate<String> predicate = s -> s.length()> 3;
        var stream = Stream.iterate("-",
                s -> ! s.isEmpty(), (s) -> s + s);
        var b1 = stream.peek(System.out::println).noneMatch(predicate); // - -- ----
        System.out.println(b1); // false
        var b2 = stream.peek(System.out::println).anyMatch(predicate); // - -- ----
        System.out.println(b2); //false
        var b3 = stream.peek(System.out::println).allMatch(predicate); // -
        System.out.println(b3); //false
    }
}

class Q5 {
    public static void main(String[] args) {
        double result1 = LongStream.of(6L, 8L, 10L)
                .mapToInt(x -> (int) x)
                .boxed()
                .collect(Collectors.groupingBy(x -> x))
                .keySet()
                .stream()
                .collect(Collectors.averagingInt(x -> x));
        System.out.println(result1);

        double result2 = LongStream.of(6L, 8L, 10L)
                .mapToInt(x -> (int) x)
                .boxed()
                .collect(Collectors.groupingBy(x -> x))
                .keySet()
                .stream()
                .collect(Collectors.averagingInt(x -> x));
        System.out.println(result2);

        double result3 = LongStream.of(6L, 8L, 10L)
                .mapToInt(x -> (int) x)
                .boxed()
                .collect(Collectors.groupingBy(x -> x))
                .keySet()
                .stream()
                .collect(Collectors.averagingInt(x -> x));
        System.out.println(result3);

        double result4 = LongStream.of(6L, 8L, 10L)
                .mapToInt(x -> (int) x)
                .boxed()
                .collect(Collectors.groupingBy(x -> x, Collectors.toSet()))
                .keySet()
                .stream()
                .collect(Collectors.averagingInt(x -> x));
        System.out.println(result4);

        double result5 = LongStream.of(6L, 8L, 10L)
                .mapToInt(x -> (int) x) //LongToIntFunction
                .boxed()
                .collect(Collectors.groupingBy(x -> x, Collectors.toSet()))
                .keySet()
                .stream()
                .collect(Collectors.averagingInt(x -> x));
        System.out.println(result5);

        double result6 = LongStream.of(6L, 8L, 10L)
                .mapToInt(x -> (int) x)
                .boxed()
                .collect(Collectors.groupingBy(x -> x, Collectors.toSet()))
                .keySet()
                .stream()
                .collect(Collectors.averagingInt(x -> x));
        System.out.println(result6);
    }
}

class Q6 {
    public static void main(String[] args) {
        var s = Stream.generate(() -> "meow");
        //var match = s.anyMatch(String::isEmpty); // infinite
        //var match = s.noneMatch(String::isEmpty); // infinite
        var match = s.allMatch(String::isEmpty); // false
        System.out.println(match);
    }
}

class Q11 {
    public static void main(String[] args) {
        System.out.println(Stream.iterate(1, x -> x++)
                .limit(5).map(x -> String.valueOf(x)).collect(Collectors.joining())); //11111

        System.out.println(Stream.iterate(1, x -> ++x)
                .limit(5).map(x -> String.valueOf(x)).collect(Collectors.joining()));
    }
}

class Q12 {
    public static void main(String[] args) {
        Set<String> birds = Set.of("oriole", "flamingo");
        Stream.concat(birds.stream(), birds.stream())
                .sorted()
                .distinct()
                .findAny() .ifPresent(System.out::println);
    }
}

class Q13 {
    public static void main(String[] args) {
        List<Integer> x1 = List.of(1, 2, 3);
        List<Integer> x2 = List.of(4, 5, 6);
        List<Integer> x3 = List.of();
        Stream.of(x1, x2, x3) //.map(x -> x + 1)
            .flatMap(Collection::stream).forEach(System.out::print);
    }
}

class Q14 {
    public static void main(String[] args) {
        Stream<Integer> s = Stream.of(1);
        //IntStream is = s.boxed(); //Cannot resolve method 'boxed' in 'Stream'
        DoubleStream ds = s.mapToDouble(x -> x);
        //Stream<Integer> s2 = s.mapToInt(x -> x); //double cannot be converted to in & Provided IntStream
        //s2.forEach(System.out::print);
    }
}

class Q16 {
    public static void main(String[] args) {
        Predicate<String> empty = String::isEmpty;
        Predicate<String> notEmpty = empty.negate();

        var result = Stream.generate(() -> "")
         .limit(10)
         .filter(notEmpty)
         .collect(Collectors.groupingBy(k -> k))
         .entrySet()
         .stream()
                .peek(System.out::println)
                .map(Map.Entry::getValue)
                .flatMap(Collection::stream)
                .peek(System.out::println)
                .collect(Collectors.partitioningBy(notEmpty)); //
                //.collect(Collectors.groupingBy(n -> n)); {}
        System.out.println(result);
    }
}

class Q18 {
    record Sesame(String name, boolean human) {
        @Override public String toString() { return name();
        } }
    record Page(List<Sesame> list, long count) {}

    public static void main(String[] args) {
        var monsters = Stream.of(new Sesame("Elmo", false));
        var people = Stream.of(new Sesame("Abby", true));
        printPage(monsters, people);
    }

    private static void printPage(Stream<Sesame> monsters, Stream<Sesame> people) {
        Page page = Stream.concat(monsters, people)
                .collect(Collectors.teeing(
                    Collectors.filtering(s -> s.name().startsWith("E"),
                            Collectors.toList()),
                Collectors.counting(),
                (l, c) -> new Page(l, c))); //Page[list=[Elmo], count=2]
        System.out.println(page);
    }
}

class Q19 {
    public static void main(String[] args) {
        System.out.println("Test 1");
        List<Integer> x = IntStream.range(1, 6)
                .mapToObj(i -> i)
                .collect(Collectors.toList());
        x.forEach(System.out::println);

        System.out.println("Test 2");
        IntStream.range(1, 6);

        System.out.println("Test 3");
        IntStream.range(1, 6).forEach(System.out::println);

        System.out.println("Test 4");
        IntStream.range(1, 6)
                .mapToObj(i -> i).forEach(System.out::println);
    }
}

class Q20 {
    public static void main(String[] args) {
        Optional<String> opt = Optional.empty();
        System.out.println(opt.orElse("")); //OK
        System.out.println(opt.orElseGet(() -> "")); //OK
        //System.out.println(opt.orElseThrow()); //NoSuchElementException
        //opt.orElseThrow(() -> throw new Exception()); //comperr throw
        System.out.println(opt.orElseThrow(RuntimeException::new)); //RuntimeException
        try {
            opt.orElseThrow(() -> new Exception());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        opt.get(); //NoSuchElementException
        //opt.get(""); //comperr ""
    }
}

class Q21 {
    public static void main(String[] args) {
        var spliterator = Stream.generate(() -> "x")
                .spliterator();
        spliterator.tryAdvance(System.out::println);
        var split = spliterator.trySplit();
        split.tryAdvance(System.out::println);
    }
}

