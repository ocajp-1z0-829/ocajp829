package udemy.part2;

import java.time.DayOfWeek;
import java.util.Arrays;

public class Part2 {
    public static void main(String[] args) {
        String fName = "Joshua";
        String lName = "Bloch";
        System.out.println(fName = lName);
    }
}

class Q2 {
    public static void main(String[] args) {
        String s1 = "OCP";
        String s2 = "ocp";
        System.out.println(s1.contentEquals(s1));
        System.out.println(s1.contentEquals(s2));
    }
}

class Q10 {
    public static void main(String[] args) {
        String str = " "; //single space
        boolean b1 = str.isEmpty();
        boolean b2 = str.isBlank();
        System.out.println(b1 + " : " + b2); //Line n1

        str.strip(); //Line n2
        b1 = str.isEmpty();
        b2 = str.isBlank();
        System.out.println(b1 + " : " + b2); //Line n3

        String str1 = "1Z0-829";
        String str2 = str1 + "";
        System.out.println(str1 == str2);
    }
}

class Q15_ {
    public static void main(String[] args) {
        //System.out.println(null);
    }
}

class Q16_ {
    public static void main(String[] args) {
        //short x = 7, y = 200;
        //int x = 7, y = 200;
        //long x = 7, y = 200;
        //float x = 7, y = 200;
        double x = 7, y = 200;
        System.out.println(String.valueOf(x + y));
        System.out.println(String.valueOf(x + y).length());
    }
}

class Q19 {
    public static void main(String[] args) {
        var str = "PANIC";
        var sb = new StringBuilder("THET");
        System.out.println(str.replace("N", sb)); //Line n1
    }
}

class Q20 {
    public static void main(String[] args) {
        var flag1 = "Java" == "Java".replace('J', 'J'); //Line n1
        var flag2 = "Java" == "Java".replace("J", "J"); //Line n2
        System.out.println(flag1 && flag2);
    }
}

class Q25 {
    public static void main(String[] args) {
        String [] arr1 = {null, null};
        System.out.println("1. " + String.join("::", arr1)); //1. null::null
        String [] arr2 = {};
        System.out.println("2. " + String.join("-", arr2)); //2.
        String [] arr3 = null;
        //System.out.println("3. " + String.join(".", arr3)); //NPE
        System.out.println("4. " + String.join(".")); //3.
        String strNull = null;
        System.out.println("5. " + String.join(".", strNull)); //"null"
        //System.out.println("6. " + String.join(".", null)); COMPERR
    }
}

class Q26 {
    public static void main(String[] args) {
        var str = "USER GENERATED";
        str = String.join("-", str.split(" "));
        System.out.println(str);
    }
}

class Q27 {
    public static void main(String[] args) {
        var str = "BEVERAGE";
        var arr = str.split("E", 3);
        System.out.println(String.join(".", arr));
        System.out.println(Arrays.toString(":".split(":", -2))); //returns ["",""] as pattern is applied max times, which is 3.
        System.out.println(Arrays.toString(":".split(":")));
        System.out.println(Arrays.toString(":".split(":", 0)));
    }
}

class Q28 {
    public static void main(String[] args) {
        String txt = "an";
        System.out.println(Arrays.toString(txt.split("an")));
        System.out.println(txt.split("an").length);
    }
}

class Q33 {
    public static void main(String [] args) {
        var tb1 = """
            Let the \
            race begin.\
        """;
        var tb2 = """
            Let the race begin.""";
        var tb3 = "Let the race begin.";

        System.out.println(tb1);
        System.out.println(tb2);
        System.out.println(tb3);
        System.out.print(tb1 == tb2);
        System.out.print(tb2 == tb3);
        System.out.print(tb1 == tb3);
    }
}

class Q34 {
    public static void main(String[] args) {
        System.out.println("""
            "Q31A",\
            "Q31B",\
            "C".\
            """);
        System.out.println("""
            \"Q31A\",\
            \"Q31B\",\
            \"C\".\
            """);
        System.out.println(String.join(",", "\"Q31A\"", "\"Q31B\"", "\"C\"") + ".");
    }
}

class Q36 {
    public static void main(String[] args) {
        var multiline = """
                SUN \s
                MON \s
                TUE \s
                """;
        System.out.println(multiline);
        multiline.indent(2).lines().forEach(System.out::println);

        multiline.indent(2)
                .lines()
                .map(s -> ">" + s + "<")
                .forEach(System.out::println);

    }
}

class Q38 {
    public static void main(String[] args) {
        System.out.println("I\sam\sthat\sI\sam.");

        //System.out.println("""I\sam\sthat\sI\sam.""");

        System.out.println("""
            I\
             am\
             that\
             I\
             am.\
            """);

        System.out.println("""
            I\sam\sthat\sI\sam.""");
    }
}

class Q39 {
    public static void main(String [] args) {
        var text = """
            I gave him $200 and \
            he returned me €120, \
            so I am left with £80.""";

        System.out.println(
                text.transform(Q39::removeCurrencySymbols)
                        .transform(String::toUpperCase)
                        .formatted("¥", "¥", "¥"));
    }

    private static String removeCurrencySymbols(String s) {
        return s.replaceAll("\\$", "%s")
                .replaceAll("€", "%s")
                .replaceAll("£", "%s");
    }
}
class Q42 {
    public static void main(String[] args) {
        var str = "Game on"; //Line n1
        var sb = new StringBuilder(str); //Line n2

        System.out.println(str.contentEquals(sb)); //Line n3
        //System.out.println(sb.contentEquals(str)); //Line n4
        System.out.println(sb.equals(str)); //Line n5
        System.out.println(str.equals(sb)); //Line n6
        System.out.println(sb.equals(new StringBuilder(str))); //Line n5
        System.out.println(str.equals(sb.toString())); //Line n6
        System.out.println(sb.toString().equals(str)); //Line n5
    }
}

class SpecialString {
    String str;
    SpecialString(String str) {
        this.str = str;
    }
}
class Q45 {
    public static void main(String[] args) {
        Object [] arr = new Object[4];
        for(int i = 1; i <=3; i++) {
            switch(i) {
                case 1:
                    arr[i] = new String("Java");
                    break;
                case 2:
                    arr[i] = new StringBuilder("Java");
                    break;
                case 3:
                    arr[i] = new SpecialString("Java");
                    break;
            }
        }
        for(Object obj : arr) {
            System.out.println(obj);
        }
    }
}

class Q50 {
    public static void main(String[] args) {
        var sb = new StringBuilder();
        //System.out.println(sb.append(null).length());
    }
}

class Q51 {
    public static void main(String[] args) {
        var sb = new StringBuilder("TOMATO");
        //System.out.println(sb.reverse().replace("O", "Q31A")); //Line n1
        System.out.println(sb.reverse().replace(0,1, "Q31A").replace(4,5, "Q31A")); //Line n1
    }
}


class Q52 {
    public static void main(String[] args) {
        var sb = new StringBuilder(20); //Line n1
        System.out.println(sb.capacity());
        sb.append("Q31A".repeat(25)); //Line n2
        System.out.println(sb.toString().length()); //Line n3
        System.out.println(sb.capacity());

        sb.ensureCapacity(43);
        System.out.println(sb);
        System.out.println(sb.capacity());
        System.out.println(sb.length());
        sb.setLength(10); //Line n4
        System.out.println(sb);
        System.out.println(sb.toString().length()); //Line n5
        System.out.println(sb.capacity());
    }
}

class Q59 {
    static final boolean DEBUG = false;

    public static void main(String[] args) {
        var x = 10; //Line n1

        if (DEBUG) System.out.println();
        if (false)
            System.out.println(x); //Line n2
        System.out.println("HELLO"); //Line n3

        //while (false) System.out.println(x);
        //while (DEBUG) System.out.println(x);
    }
}

class Q61 {
    public static void main(String[] args) {
        char var = 65535;
        var a = 5;
        var x = 10;
        switch(x) {
            case 10:
                a *= 2;
            case 20:
                a *= 3;
            case 30:
                a *= 4;
        }
        System.out.println(a);
    }
}

class Q68 {
    public static void main(String[] args) {
        /*INSERT*/
        char var1 = '7';
        Character var2 = '7';
        char var3 = 7;
        Character varC4= 7;
        Integer var5 = 7;
        System.out.println(var3);
        switch(varC4) {
            case 7:
                System.out.println("Lucky no. 7");
                break;
            default:
                System.out.println("DEFAULT");
        }
    }
}

class Q69 {
    public static void main(String[] args) {
        //Integer day = '3';
        int day = '3';
        switch(day) {
            case '3':
                System.out.println("BUY 2 GET 1 FREE");
                break;
            default:
                System.out.println("SORRY!!! NO SALE");
        }
    }
}

enum CURRENCY {
    DOLLAR, POUND, YEN
}
class Q72 {
    public static void main(String[] args) {
        var amount = 1000;
        var curr = CURRENCY.DOLLAR;
        switch (curr) {
            case DOLLAR:
                String sign = "$";
                System.out.println(sign + amount);
            case POUND:
                //System.out.println(sign + amount); //Variable 'sign' might not have been initialized
            case YEN:
                //String sign = "¥"; //Variable 'sign' is already defined in the scope
                //System.out.println(sign + amount);
        }
    }
}

class Q73 {
    public static void main(String[] args) {
        var x = 5;
        var msg = switch(x) {
            case 1,3,5,7,9 -> "Odd Number";
            case 0,2,4,6,8 -> "Even Number";
            default -> "Not in range";
        };
        System.out.println(msg);
    }
}

class Q75 {
    public static void main(String[] args) {
        var dayofWeek = DayOfWeek.SUNDAY;
        var text = switch(dayofWeek) {
            case SATURDAY -> "REST";
            case SUNDAY -> throw new IllegalArgumentException("NOT AVAILABLE");
            default  -> "WORK";
        };
        System.out.println(text);
    }
}

class Q76 {
    public static void main(String[] args) {
        int jobCode = 0;
        String status = switch(jobCode) {
            case -1 -> {
                String temp = "FAILED";
                yield temp;
            }
            case 0 -> "RUNNING";
            case 1 -> {
                yield "SUCCESS";
            }
            //case 100 -> yield "TEST";
            //case 100 -> {return "TEST";}
            default -> "NA";
        };
        System.out.println(status);
    }
}

class Q77 {
    public static void main(String[] args) {
        int x = 100;

        String txt = switch(x) { //Line n1
            case 10 -> "TEN"; //Line n2
            case 100 -> switch(10) { //Line n3
                default -> "HUNDRED"; //Line n4
            }; //Line n5
            default -> "NA"; //Line n6
        }; //Line n7

        System.out.println(txt);
    }
}

class Q78 {
    public static void main(String[] args) {
        int day = 5;
        String dayName = switch(day) {
            case 1 : yield "SUNDAY";
            case 2 : yield "MONDAY";
            case 3 : yield "TUESDAY";
            case 4 : yield "WEDNESDAY";
            case 5 : yield "THURSDAY";
            case 6 : {yield "FRIDAY";}
            case 7 : yield "SATURDAY";
            default : yield "NA";
        };
        System.out.println(dayName);
    }
}

class Q81 {
    public static void main(String[] args) {
        int x = 4;
        switch (x) {
            default -> System.out.println("Unknown");
            case 1 -> System.out.println("x is equal to 1");
            case 2 -> {System.out.println("x is equal to 2"); break;}
            case 3 -> System.out.println("x is equal to 3");
        }
    }
}

class Q83 {
    public static void main(String[] args) {
        var a = 5;
        var x = "+";
        switch(x) {
            case "+" -> a = a + 2; //Line n1
            case "-" -> { a = a - 3; break; } //Line n2
            case "/" -> a = a / 4; //Line n3
            //case "*" : a = a * 3; //Line n4
        }
        System.out.println(a);
    }
}

class Q85 {
    //final static boolean flag = false;
    public static void main(String[] args) {
        final boolean flag;
        flag = false;
        while(flag) {
            System.out.println("Good Night!");
        }
    }
}

class Q86 {
    public static void main(String[] args) {
        int x = 5;
        while (x < 10)
            System.out.println(x);
        x++;
    }
}

class Q87 {
    public static void main(String[] args) {
        final boolean flag = true;
        do {
            System.out.print(100);
        } while (flag); //Line n1

        //System.out.println(200); //Line n2
    }
}

class Q89 {
    public static void main(String[] args) {
        int start = 1;
        int sum = 0;
        do {
            if(start % 2 == 0) {
                continue;
            }
            sum += start; //Line n1
        } while(++start <= 10);
        System.out.println(sum);
    }
}

class Q90 {
    public static void main(String[] args) {
        for (var var = 0; var <= 2; var++) {
            System.out.println(var);
        }
        //Line n1
    }
}



class Q92 {
    public static void main(String[] args) {
        LABEL:
        //for:
        for (int i = 2; i <= 100; i = i + 2) {
            for (int j = 1; j <= 10; j++) {
                System.out.print(i * j + "\t");
            }
            System.out.println();
            if (i == 10) {
                break LABEL; //for;
            }
        }
    }
}

class Q93 {
    public static void main(String[] args) {
        int i;
        outer:
        do {
            i = 5;
            inner:
            while (true) {
                System.out.println(i--);
                if (i == 4) {
                    break outer;
                }
            }
        } while (true);
    }
}

class Q97 {
    public static void main(String[] args) {
        var var = 0; //Line n1
        var: for (var = 0; var < 3; var++) {  //Line n2
            if (var % 2 == 0) {
                continue var; //Line n3
            }
            var++; //Line n4
            System.out.println(var);
        }
        System.out.println(var);
    }
}

class Q98 {
    public static void main(String[] args) {
        var: while (true) { //Line n1
            i: for (int i = 0; i <= 2; i++) {
                if(i == 2) {
                   break var;
                }
            }
        }
        System.out.println("THINK DIFFERENT"); //Line n2
    }
}

class Q102 {
    public static void main(String[] args) {
        int ctr = 100;
        one: for (var i = 0; i < 10; i++) {
            two: for (var j = 0; j < 7; j++) {
                three: while (true) {
                    ctr++;
                    if (i > j) {
                        break one;
                    } else if (i == j) {
                        break two;
                    } else {
                        break three;
                    }
                }
            }
        }
        System.out.println(ctr);
    }
}

class Q103 {
    public static void main(String[] args) {
        var i = 1;
        var j = 5;
        var k = 0;
        Q31A: while(true) {
            i++;
            Q31B: while(true) {
                j--;
                C: while(true) {
                    k += i + j;
                    if(i == j)
                        break Q31A;
                    else if (i > j)
                        continue Q31A;
                    else
                        continue Q31B;
                }
            }
        }
        System.out.println(k);
    }
}

class Q105 {
    public static void main(String[] args) {
        int i = 0;
        for(System.out.print(i++); i < 2; System.out.print(i++)) {
            System.out.print(i);
        }
    }
}

class Q106 {
    public static void main(String[] args) {
        int i = 0;
        String res = null;
        for(String [] s = {"Q31A", "Q31B", "C", "D"};;res = String.join(".", s)) { //Line n1
            System.out.println(res);
            if(i++ == 0) {
                System.out.println("continue");
                continue;
            }
            else{
                System.out.println("break");
                break;
            }
        }
        System.out.println(res); //Line n2
    }
}