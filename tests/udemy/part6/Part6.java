package udemy.part6;

import java.util.*;

public class Part6 {
}

//
class Q13 {
        private static <T extends Number> void print(T t) {
            System.out.println(t.intValue());
        }

        public static void main(String[] args) {
            /*INSERT*/
            print(Integer.valueOf(1));
            print(Double.valueOf(5.5));
        }
}

//
class Q14 {
    public static void main(String[] args) {
        List<? extends String> list = new ArrayList<>(Arrays.asList("A", "E", "I", "O")); //Line n1
        //list.add("U"); //Line n2
        for(String str : list) {
            System.out.print(str);
        }
        list.forEach(System.out::print);
    }
}

class Q15 {
    public static void main(String[] args) {
        List<? super String> list = new ArrayList<>();
        list.add("A");
        list.add("B");
        //for(String str : list) {
          //  System.out.print(str);
        //}
    }
}

class Q17<T> {
    T [] obj;

    public Q17() {
        //obj = new T[100];
    }

    public T [] get() {
        return obj;
    }

    public static void main(String[] args) {
        Q17<String> test = new Q17<>();
        String [] arr = test.get();
        System.out.println(arr.length);
    }
}

class Q18<T> {
 //   static T obj;
}

class Q20 {
    public static void main(String[] args) {
        List<String> list1 = new ArrayList<>();
        list1.add("A");
        list1.add("B");

        List<? extends Object> list2 = list1;
        list2.remove("A"); //Line n1
        //list2.add("C"); //Line n2

        System.out.println(list2);
    }
}

class Q26 {
    public static void main(String[] args) {
        List<String> list;
        list = new ArrayList<>(); //Line n1
        list.add("A");
        list.add("E");
        list.add("I");
        list.add("O");
        list.add("U");
        List<String> listSub = list.subList(0, 4);
        listSub.set(2, "1");
        System.out.println(listSub);

        list.addAll(list.subList(0, 4)); //Line n2
        System.out.println(list);
    }
}

class Q27 {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("C");
        list.add("Z");
        list.add("A");
        list.add("R");
        list.subList(1, 2).clear();
        System.out.println(String.join("", list));
    }
}
//
class Q30Employee {
    private String name;
    private int age;

    Q30Employee(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String toString() {
        return "Employee[" + name + ", " + age + "]";
    }

    public boolean equals(Object obj) {
        if(obj instanceof Q30Employee) {
            Q30Employee emp = (Q30Employee)obj;
            if(this.name.equals(emp.name) && this.age == emp.age) {
                return true;
            }
        }
        return false;
    }
}

class Q30 {
    public static void main(String[] args) {
        List<Q30Employee> employees = new ArrayList<>();
        employees.add(new Q30Employee("William", 25));
        employees.add(new Q30Employee("William", 27));
        employees.add(new Q30Employee("William", 25));
        employees.add(new Q30Employee("William", 25));

        employees.remove(new Q30Employee("William", 25));

        for(Q30Employee emp : employees) {
            System.out.println(emp);
        }
    }
}

class Q33 {
    public static void main(String[] args) {
        List<String> sports = new ArrayList<>();
        sports.add("Windsurfing");
        sports.add("Aerobics");
        sports.add("Archery");
        sports.add("Diving");

        Iterator<String> iterator = sports.iterator();
        while(iterator.hasNext()) {
            String sport = iterator.next();
            if(sport.startsWith("A")) {
                iterator.remove();
            }
        }

        System.out.println(sports);
    }
}

class Q34 {
    public static void main(String[] args) {
        List<String> objects = new ArrayList<>();
        objects.add("Watch");
        objects.add("Arrow");
        objects.add("Anchor");
        objects.add("Drum");

        ListIterator<String> iterator = objects.listIterator();
        while(iterator.hasNext()) {
            if(iterator.next().startsWith("A")) {
                iterator.remove();
            }
        }

        System.out.println(objects);
    }
}

class Q36 {
    public static void main(String[] args) {
        List<String> list = Arrays.asList("T", "S", "R", "I", "F");
        ListIterator<String> iter = list.listIterator(5);
        while(iter.hasPrevious()) {
            System.out.print(iter.previous());
        }
    }
}

class Q38 {
    public static void main(String[] args) {
        List<String> colors = new ArrayList<>();
        colors.add("RED");
        colors.add("GREEN");
        colors.add("BLUE");
        Iterator<String> iter = colors.iterator();
        while(iter.hasNext()) {
            iter.next();

            iter.remove();
        }
        System.out.println(colors.size());
    }
}

class Q41 {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("ONE");
        list.add("TWO");
        list.add("THREE");
        list.add("THREE");

        if(list.remove(2) != null) {
            list.remove("THREE");
        }

        System.out.println(list);
    }
}

class Q42 {
    public static void main(String[] args) {
        Boolean [] arr = new Boolean[2];
        List<Boolean> list = new ArrayList<>();
        list.add(arr[0]);
        list.add(arr[1]);

        if(list.remove(0)) {
            list.remove(1);
        }

        System.out.println(list);
    }
}

class Q46 {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>(4);
        System.out.println(list);
        list.add(0, "MOVE");
        list.add(2, "ON");

        System.out.println(list);
    }
}

class Q53 {
    public static void main(String[] args) {
        List<Character> list = new ArrayList<>();
        list.add(0, 'E');
        list.add('X');
        list.add(1, 'P');
        list.add(3, 'O');

        if(list.contains('O')) {
            list.remove('O');
        }

        for(char ch : list) {
            System.out.print(ch);
        }
    }
}

interface Sellable {}
abstract class Animal {}
class Mammal extends Animal{}
class Rabbit extends Mammal implements Sellable{}

class Q54 {
    {
        List<Animal> list = new ArrayList<>();
        list.add(new Rabbit());
    }
    {
        List<Animal> list = new ArrayList<>();
        list.add(new Mammal());
    }
    {
        List<Mammal> list = new ArrayList<>();
        list.add(new Rabbit());
    }
    {
        List<Sellable> list = new ArrayList<>();
        //list.add(new Mammal());
    }
    {
        List<Sellable> list = new ArrayList<>();
        list.add(new Rabbit());
    }
}
//
class Q65 {
    public static void main(String[] args) {
        List list = new ArrayList<String>();
        list.add(1);
        list.add("2");
        list.add(null);
        list.forEach(System.out::print);
    }
}

class Q69 {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>(Arrays.asList(1,2,3,4,5,6,7,8,9,10));
        list.removeIf(i -> i % 2 == 1);
        System.out.println(list);
    }
}

class Q72 {
    public static void main(String[] args) {
        var list1 = List.of(); //Line n1
        var list2 = List.of("A"); //Line n2
        //list1 = list2; //Line n3
        System.out.println(list1.size()); //Line n4
    }
}

class Q73 {
    public static void main(String[] args) {
        var list1 = new ArrayList<>(List.of("A", "B", "1", "2"));
        var list2 = new ArrayList<>(List.of("B", "C", "A"));
        list1.retainAll(list2);
        System.out.println(list1.size());
    }
}

class Test {
    public static void main(String[] args) {
        var list = new ArrayList<String>(); //Line n1
        list.add("A");
        list.add("M");
        System.out.println(list);
        var list_of_list = List.of(list); //Line n2
        list_of_list.get(0).set(1, "N"); //Line n3
        list_of_list.get(0).forEach(System.out::print); //Line n4
    }
}

class Q75 {
    public static void main(String[] args) {
        var list1 = Arrays.asList('A', 'B', 'D');
        var list2 = List.of('A', 'B', 'C');
        /*INSERT*/
        list2 = list1;
        list1.set(2, 'C');

        System.out.println(list1.equals(list2));
    }
}