package udemy.part7;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class Part7 {
}

class Q1 {
    private String name;
    private int age;

    Q1(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String toString() {
        return "Student[" + name + ", " + age + "]";
    }

    //public int hashCode() {
        //return Objects.hash(name, age);
    //}

    public boolean equals(Object obj) {
        if(obj instanceof Q1) {
            Q1 stud = (Q1)obj;
            return this.name.equals(stud.name) && this.age == stud.age;
        }
        return false;
    }
}
class Q1_ {
    public static void main(String[] args) {
        Set<Q1> students = new HashSet<>();
        students.add(new Q1("Bill", 25));
        students.add(new Q1("Bill", 25));
        students.add(new Q1("Bill", 26));

        System.out.println(students.size());
    }
}
//
record Q2(String name, int age) implements Comparable<Q2> {
    public int compareTo(Q2 stud) {
        return this.name.compareTo(stud.name);
    }
}
class Q2_ {
    public static void main(String[] args) {
        Set<Q2> students = new TreeSet<>();
        students.add(new Q2("Samuel", 21));
        students.add(new Q2("Samuel", 21));
        students.add(new Q2("Samuel", 21));

        System.out.println(students.size());
    }
}
//

record Q3_(String name, int age) {
    public static int compareByName(Q3_ s1, Q3_ s2) {
        return s1.name().compareTo(s2.name());
    }
}

class Q3 {
    public static void main(String[] args) {
        Set<Q3_> students = new TreeSet<>(Q3_::compareByName);
        students.add(new Q3_("Martha", 16));
        students.add(new Q3_("Martha", 17));
        students.add(new Q3_("Martha", 18));

        System.out.println(students.size());
    }
}

class Q5 {
    public static void main(String[] args) {
        Set<String> set = new TreeSet<>(Arrays.asList(null,null,null));
        long count = set.stream().count();
        System.out.println(count);
    }
}

class Q6 {
    public static void main(String[] args) {
        Set<String> set = new HashSet<>(Arrays.asList(null,null,null));
        long count = set.stream().count();
        System.out.println(count);
    }
}

class Q7 {
    public static void main(String[] args) {
        NavigableSet<String> set = new TreeSet<>(Arrays.asList("red", "green", "blue", "gray"));
        System.out.println(set.ceiling("gray"));
        System.out.println(set.floor("gray"));
        System.out.println(set.higher("gray"));
        System.out.println(set.lower("gray"));
        System.out.println(Math.floor(1.6));
        System.out.println(Math.ceil(1.6));
    }
}

class Q8 {
    public static void main(String[] args) {
        Set<Integer> set = Set.of(10, null, 20, 10, 40, null); //Line n1
        System.out.println(set.size());
    }
}
//
class Q9 {
    public static void main(String[] args) {
        Set<String> set = Set.of("A", "E", "I", "I", null, "O", "U");
        System.out.println(set.size());
    }
}
//
class Q12 {
    public static void main(String[] args) {
        var list = List.of("A", "E", "I", "O", "U"); //Line n1
        var set1 = Set.copyOf(list); //Line n2

        var map = Map.of(1, "U", 2, "O", 3, "I", 4, "E", 5, "A"); //Line n3
        var set2 = Set.copyOf(map.values()); //Line n4

        System.out.println(set1.equals(set2)); //Line n5
    }
}

class Q13 {
    public static void main(String[] args) {
        var sb1 = new StringBuilder("A");
        var sb2 = new StringBuilder("B");
        var set1 = Set.of(sb1, sb2); //Line n1
        var set2 = Set.copyOf(set1); //Line n2
        System.out.println((set1 == set2) + ":" + set1.equals(set2)); //Line n3
    }
}
class Q13_ {
    public static void main(String[] args) {
        var sb1 = new String("A");
        var sb2 = new String("B");
        var l1 = List.of(sb1, sb2); //Line n1
        var l2 = List.copyOf(l1); //Line n2
        System.out.println((l1 == l2) + ":" + l1.equals(l2)); //Line n3
    }
}
//
enum Q15TrafficLight {
    RED, YELLOW, GREEN
}
class Q15 {
    public static void main(String[] args) {
        Map<Q15TrafficLight, String> map = new TreeMap<>();
        map.put(Q15TrafficLight.GREEN, "GO");
        map.put(Q15TrafficLight.RED, "STOP");
        map.put(Q15TrafficLight.YELLOW, "READY TO STOP");

        for(String msg : map.values()) {
            System.out.println(msg);
        }
    }
}
class Q16 {
    public static void main(String[] args) {
        Map<Q15TrafficLight, String> map = new TreeMap<>();
        map.put(Q15TrafficLight.GREEN, "GO");
        map.put(Q15TrafficLight.RED, "STOP");
        map.put(Q15TrafficLight.YELLOW, "STOP IN 3 Seconds");
        map.put(Q15TrafficLight.YELLOW, "READY TO STOP");
        for(String msg : map.values()) {
            System.out.println(msg);
        }
    }
}

class Q17 {
    public static void main(String[] args) {
        Map<Integer, String> map = new LinkedHashMap<>();
        map.put(null, "zero");
        map.put(1, "one");
        System.out.println(map);
    }
}

class Q18 {
    public static void main(String[] args) {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(25, "Pune");
        map.put(32, "Mumbai");
        map.put(11, "Sri Nagar");
        map.put(39, "Chennai");

        System.out.println(map.headMap(25, true));
        System.out.println(map.headMap(25));

        System.out.println(map.tailMap(25));

    }
}

class Q24 {
    public static void main(String[] args) {
        Deque<Integer> deque = new ArrayDeque<>();
        deque.add(100);
        deque.add(200);
        deque.addFirst(300);
        deque.addLast(400);
        deque.remove(200);

        System.out.println(deque.size());
        System.out.println(deque.getFirst());
        System.out.println(deque.size());
    }
}

class Q25 {
    public static void main(String[] args) {
        Deque<Boolean> deque = new ArrayDeque<>();
        deque.push(Boolean.valueOf("abc"));
        deque.push(Boolean.valueOf("tRuE"));
        deque.push(Boolean.valueOf("FALSE"));
        deque.push(true);
        System.out.println(deque.pop() + ":" + deque.peek() + ":" + deque.size());
    }
}

class Q26 {
    public static void main(String[] args) {
        List<String> list = new LinkedList<>();
        list.add("ONE");
        list.add("TWO");
        list.remove(1);
        System.out.println(list);

        Queue<String> queue = new LinkedList<>();
        queue.add("ONE");
        queue.add("TWO");
        queue.remove();
        System.out.println(queue);
    }
}

class Q27 {
    public static void main(String[] args) {
        Deque<Character> chars = new ArrayDeque<>();
        chars.add('A');
        chars.add('B');
        chars.remove();
        chars.add('C');
        chars.remove();

        System.out.println(chars);
    }
}

class Q29 {
    public static void main(String[] args) throws Exception {
        List<String> list = Arrays.asList("oca", null, "ocp", "java", "null"); //Line n1
        Deque<String> deque = new ArrayDeque<String>(list); //Line n2
        System.out.println(deque.size()); //Line n3
    }
}

class Q32 {
    public static void main(String[] args) {
        String [] arr = {"**", "***", "*", "*****", "****"};
        Arrays.sort(arr, (s1, s2) -> s1.length()-s2.length());
        for(String str : arr) {
            System.out.println(str);
        }
    }
}

class Q37 {
    public static void main(String[] args) {
        List<String> list = Arrays.asList("M", "R", "A", "P");
        Collections.sort(list, null);
        System.out.println(list);
    }
}

class Q39 {
    public static void main(String[] args) {
        List<String> emails = Arrays.asList("udayan@outlook.com", "sachin@outlook.com", "sachin@gmail.com",
                "udayan@gmail.com");
        Collections.sort(emails, Comparator.comparing(str -> str.substring(str.indexOf("@") + 1)));
        for(String email : emails) {
            System.out.println(email);
        }
    }
}

class Q41 {
    public static void main(String[] args) {
        List<String> list = Arrays.asList("#####", "#", "##", "####", "###");
        Comparator<String> comp = Comparator.comparing(s -> s);
        Collections.sort(list, comp.reversed());
        printCodes(list);

    }

    private static void printCodes(List<String> list) {
        for (String str : list) {
            System.out.println(str);
        }
    }
}

//
interface Q50Equality {
    boolean equals(Object obj);
}

class Q50 implements Q50Equality {
    String n = "";

    @Override
    public boolean equals(Object obj) {
        return this.n.equals(((Q50) obj).n);
    }

    public static void main(String[] args) {
        Q50Equality q1 = new Q50();
        Q50Equality q2 = new Q50();
        System.out.println(q1.equals(q2));
        //Q50Equality eq = x -> true;
//        System.out.println(eq.equals(null));
    }
}
//
interface Q53Parent {
    default void earn() {
        System.out.println("Earning for the family");
    }

    private void sout() {
        System.out.println("Test private");
    }

    static void plan() {
        planRetirement();
        planChildrenEducation();
    }

    private static void planChildrenEducation() {
        //valid codes
    }

    private static void planRetirement() {
        //valid codes
    }

    String toString();
    }

interface Q53Child extends Q53Parent {
    void play();
}

class Q53_ {
    public static void main(String[] args) {
        Q53Child child = () -> System.out.println("PLAYING CRICKET..."); //Line n1
        child.play(); //Line n2
    }
}
//

interface Q58_ {
    void calculate();
}
class Q58 {
    int i = 1;
    Q58_ obj = () -> {
        int i = 10;
        i++;
        System.out.println(i);
        return;
    };
    public static void main(String[] args) {
        Q58_ obj = () -> {
            int i = 10;
            i++;
            System.out.println(i);
            return;
        };
        obj.calculate();
    }
}
//
class Q65 {
    public static void main(String[] args) {
        Collection<Integer> list = List.of(100, 100, 100); //Line n1
        Collection<Integer> set = new LinkedHashSet<>(list); //Line n2

        set.forEach((s) -> System.out.print(s)); //Line n3
    }
}

//
interface Q68 {
    String print();

    //default String toString() {
      //  return "*";
    //}

}

class Q68_ {
    public static void main(String[] args) {
        Q68 obj = () -> "PRINTER";
        System.out.print(obj);
        System.out.print(obj.print());
    }
}
//

interface Q69<T extends Integer> {
    T operate(T x, T y);
}
class Q69_ {
    public static void main(String[] args) {
        Q69 o1 = (x, y) -> x + y;
        System.out.println(o1.operate(5, 10));
    }
}
//

interface Q70<T> {
    public abstract T operation(T t1, T t2);
}

class Q70_ {
    public static void main(String[] args) {
        Q70 q = new Q70<String>() {
            public String operation(String s1, String s2) {
                return s1 + s2;
            }
        };
        System.out.println(q);
        System.out.println(q.operation("2", "3"));

        System.out.println(new Q70<String>() {
            public String operation(String s1, String s2) {
                return s1 + s2;
            }
        });

        Q70<String> operator = (s1, s2) -> s1 + s2;
        System.out.println(operator.operation("5", "10"));

        System.out.println((Q70<String>)(String s1, String s2) -> s1 + s2);
        System.out.println((Q70<String>)(s1, s2) -> s1 + s2);
        System.out.println((Q70<String>)(s1, s2) -> { return s1 + s2; });
    }
}

//

@FunctionalInterface
interface Printer {
    void print();
}

class Util {
    public static void printCurrentTime() {
        //Format to display date
        DateFormat dateFormat
                = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        //Create a date object representing
        // current date and time
        Date date = new Date();

        //prints the date object in above format
        System.out.println(dateFormat.format(date));
    }
}

class Q72_ {
    public static void main(String[] args) {
        Printer creator =
                () -> Util.printCurrentTime();
        creator.print();
        creator = Util::printCurrentTime;
        creator.print();
    }
}

//

@FunctionalInterface
interface Q73Operator {
    void operate(int i, int j);
}

class Q73Calculator {
    public static void add(int i, int j) {
        System.out.println(i + j);
    }
}

class Q73_ {
    public static void main(String[] args) {
        Q73Operator opr1 = (i, j) -> System.out.println(i + j); //Line n1
        opr1.operate(15, 25);
        //Q73Operator opr = Q73Calculator.add(i, j);
        Q73Operator opr2 = Q73Calculator::add;
        //Q73Operator opr3 = (Integer i, Integer j) -> Q73Calculator.add(i, j);
    }
}

//

interface Q74Func {
    int f(int i, int j, int k);
}

class Q74Test {
    private static int cube(int i, int j, int k) {
        return i * j * k;
    }

    public static void main(String[] args) {
        //Q74Func obj = (byte x, byte y, byte z) -> x * y * z /* INSERT */;
        Q74Func obj = Q74Test::cube;
        System.out.println(obj.f(10, 20, 30));
    }
}

//
class Q76Test {
    public static void main(String[] args) {
        Q76ObjectCreator<Integer> obj = Integer::valueOf;
        System.out.println(obj.create("603"));
    }
}
//interface ObjectCreator {
  //  Integer create(String str);
//}
interface Q76ObjectCreator<T> {
    T create(String str);
}

//

interface Q79Creator<T> {
    T create();
}

abstract class Q79Gift {
    public Q79Gift() {
        super();
    }

    public String toString() {
        return "Gift";
    }
}

class Q79Book extends Q79Gift {
    public Q79Book() {
        super();
    }

    public String toString() {
        return "Book";
    }
}

class Q79Test {
    public static void main(String[] args) {
        //Q79Creator<Q79Gift> obj = Q79Gift::new;
        //System.out.println(obj.create());
    }
}
//
interface Q80Creator<T> {
    T create(String type);
}
interface Q80Custom<T> {
    T create(String name, int age);
}
enum Q80AnimalType {
    MAMMAL, BIRD, REPTILE;
}

abstract class Q80Animal {
    private Q80AnimalType type;

    public Q80Animal(Q80AnimalType type) {
        this.type = type;
    }

    public Q80Animal() {
    }

    public Q80AnimalType getType() {
        return type;
    }
}

class Q80Dog extends Q80Animal {
    public Q80Dog(String type) {
        super(Q80AnimalType.valueOf(type.toUpperCase())); //Line n1
    }
    public Q80Dog(String name, int age) {
        System.out.println(name + " " + age);
    }
}

class Q80Test {
    public static void main(String[] args) {
        Q80Creator<Q80Dog> obj = Q80Dog::new; //Line n2
        var d = obj.create("mammal"); //Line n3
        System.out.println(d.getType()); //Line n4
        Q80Custom dog = Q80Dog::new;
        dog.create("name", 1);

    }
}

//

interface Q82Creator<T, R> {
    R create(T t);
}

abstract class Q82Animal {
    abstract void eat();
}

class Q82Dog extends Q82Animal {
    private String name;

    public Q82Dog(String s) {
        this.name = s;
    }

    void eat() {
        System.out.println(this.name + " eats biscuits.");
    }
}

class Q82_ {
    public static void main(String[] args) {
        Q82Creator<String, Q82Animal> obj = Q82Dog::new; //Line n1
        Q82Animal cooper = obj.create("Cooper");//Line n2
        cooper.eat();
    }
}

//

interface Q84DoubleToByte {
    byte getValue();
}

class Q84 {
    public static void main(String[] args) {
        Q84DoubleToByte obj = Double.valueOf("123")::byteValue;
        System.out.println(obj.getValue());
    }
}

//

interface Q90Printable {
    void print(Q90Printer p1, Q90Printer p2);
}

class Q90Printer {
    public static void print(Q90Printer p1, Q90Printer p2) {
        System.out.println(p1.toString() + "$$" + p2.toString());
    }

    public void print(Q90Printer p) {
        System.out.println(p.toString());
    }

    public String toString() {
        return "Hello";
    }
}

 class Q90Test {
    public static void main(String[] args) {
        //Q90Printable obj = Q90Printer::print;
        //Q90Printable obj = (p1, p2) -> p1.print(p2);
        Q90Printable obj = (p1, p2) -> Q90Printer.print(p1, p2);
        obj.print(new Q90Printer(), new Q90Printer());
    }
}

//

class Q96 {
    public static void main(String[] args) {
        Supplier<StringBuilder> supplier = () -> new StringBuilder(" olleH")
                .reverse().append("!dlroW").reverse();
        System.out.println(">" + supplier.get() + "<");
    }
}

//

class Q98Document {
    void printAuthor() {
        System.out.println("Document-Author");
    }
}

class Q98RFP extends Q98Document {
    @Override
    void printAuthor() {
        System.out.println("RFP-Author");
    }
}

 class Q98 {
    public static void main(String[] args) {
        check(Q98Document::new);
        check(Q98RFP::new);
    }

     //private static void check(Supplier<? super Q98Document> supplier) {
     //private static void check(Supplier<Q98RFP> supplier) {
     //private static void check(Supplier<? extends Q98RFP> supplier) {
     //private static void check(Supplier<? super Q98RFP> supplier) {
     //private static void check(Supplier supplier) {
     //private static void check(Supplier<Q98Document> supplier) {
     private static void check(Supplier<? extends Q98Document> supplier) {
        supplier.get().printAuthor();
    }
}

class Q100 {
    public static void main(String[] args) {
        Function<char [], String> obj = String::new; //Line n1
        String s = obj.apply(new char[] {'j', 'a', 'v', 'a'}); //Line n2
        System.out.println(s);
    }
}

//

record Q113Point(int x, int y) {
    public boolean equals(Object obj) {
        if(obj != null) {
            return this.y == ((Q113Point)obj).y + 1;
        }
        return false;
    }
}

class Q113 {
    public static void main(String[] args) {
        Predicate<Q113Point> predicate = Predicate.isEqual(new Q113Point(2, 3));
        System.out.println(predicate.test(new Q113Point(2, 2 /*INSERT*/)));
        System.out.println(predicate.test(new Q113Point(-1, 2 /*INSERT*/)));
    }
}