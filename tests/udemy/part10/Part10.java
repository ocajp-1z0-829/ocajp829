package udemy.part10;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Part10 {
    public static void main(String[] args) {

    }
}

class Q1 {
    public static void main(String[] args) throws IOException {
        System.setOut(new PrintStream("tests/udemy/part10/err.log"));
        try {
            System.out.println("ONE");
            System.out.println(1 / 0);
        } catch (ArithmeticException e) {
            System.out.println("catch");
            System.err.println("TWO");
        }
    }
}

/**
 * javac Part10.java -d .
 * java udemy.part10.Q2
 */
class Q2 {
    public static void main(String[] args) {
        var console = System.console();
        if(console != null) {
            console.format("%d %<x", 10);
        }
    }
}

/**
 * javac Part10.java -d .
 * java udemy.part10.Q3
 */
class Q3 {
    public static void main(String[] args) {
        var console = System.console();
        if(console != null) {
            char[] chars = console.readPassword();
            console.format("%d %x", 10);
        }
    }
}

class Q4 {
    public static void main(String[] args) {
        System.out.printf("%2$d + %1$d", 10, 20);
    }
}

class Q5 {
    public static void main(String[] args) {
        System.out.printf("%2$d + %1$d", 10, 20, 30);
        System.out.println();
        System.out.println(String.format("%2$d + %1$d", 10, 20, 30));
    }
}

class Q6 {
    public static void main(String[] args) {
        System.out.format("A%nB%nC");
    }
}

class Q10 {
    public static void main(String[] args) throws IOException {
        var console = System.console();
        console.writer().printf("Enter a number between 1 and 7: "); //Line n1
        var num = Integer.parseInt("" + (char)console.reader().read()); //Line n2
        var flag = IntStream.rangeClosed(1, 7).anyMatch(i -> i == num);
        if(flag)
            console.printf("*".repeat(num)); //Line n3
        else
            console.writer().format("INVALID"); //Line n4
    }
}

class Q11 {
    public static void main(String[] args) {
        File file = new File("tests/udemy/part10/A/B/C");
        file.mkdirs();
    }
}

class Q13 {
    public static void main(String[] args) {
        var dir = new File("tests/udemy/part10" + System.getProperty("path.separator") + "A");
        System.out.println(dir.mkdir());
        var dir1 = new File("tests/udemy/part10" + System.getProperty("file.separator") + "A");
        System.out.println(dir1.mkdir());
    }
}

class Q14 {
    public static void main(String[] args) {
        var dir = new File("F:" + File.separator + "A" + File.separator + "B");
        System.out.println(dir.getParentFile());
        System.out.println(dir.getParent());
        System.out.println(dir.getParentFile().getParent());
    }
}

class Q16 {
    public static void main(String[] args) throws IOException {
        deleteFiles(new File("tests/udemy/part10/data"));
    }

    public static void delWithWalk() throws IOException {
        try (var dirStream = Files.walk(Paths.get("tests/udemy/part10/data"))) {
            dirStream
                    .map(Path::toFile)
                    .sorted(Comparator.reverseOrder())
                    .forEach(File::delete);
        }
    }

    public static void deleteFiles(File dir) throws IOException {
        var list = dir.listFiles();
        if (list != null && list.length > 0) {
            for (var file : list) {
                if (file.isDirectory()) {
                    deleteFiles(file);
                } else {
                    File fileParent = file.getParentFile();
                    if ("data".equals(fileParent.getName())) {
                        System.out.println(file.getName());
                        System.out.println(Files.deleteIfExists(file.toPath()));
                    }
                }
            }
        }
    }
}

class Q17 {
    public static void main(String[] args) throws IOException {
        var file = new File("tests/udemy/part10/data.txt");
        try(
                var os = new DataOutputStream(new FileOutputStream(file));
                var is = new DataInputStream((new FileInputStream(file)))
        ) {
            os.writeChars("JAVA");
            System.out.println(is.readChar());
        }
    }
}

class Q18 {
    public static void main(String[] args) {
        try(var writer = new BufferedWriter(new FileWriter("tests/udemy/part10/data.txt")))
        {
            writer.close();
            writer.newLine();
        } catch(IOException e) {
            System.out.println("IOException");
        }
    }
}

class Q19 {
    public static void main(String[] args) throws IOException {
        var bw = new BufferedWriter(new FileWriter("tests/udemy/part10/data.txt"));
        try(var writer = bw) { //Line 8

        } finally {
            bw.flush(); //Line 11
        }
    }
}

class Q20 {
    public static void main(String[] args) throws IOException {
        try(var br = new BufferedReader(new InputStreamReader(System.in));) {
            System.out.print("Enter any number between 1 and 10 2 times: ");
            var num = br.read();
            System.out.println(num);
            System.out.println((char) num);
            System.out.println();
            var s = br.readLine();
            System.out.println(Integer.parseInt(s));
        }
    }
}

class Q21 {
    public static void main(String[] args) throws IOException {
        var file = new File("tests/udemy/part10/f1.txt");
        var fileWriter = new FileWriter("tests/udemy/part10/f2.txt");
        var printWriter = new PrintWriter("tests/udemy/part10/f3.txt");
        var dfs = new FileOutputStream("tests/udemy/part10/f4.txt");
    }
}

class Q22 {
    public static void main(String[] args) throws IOException {
        var file = new File("tests/udemy/part10/f1.txt");
        //var fileWriter = new FileWriter("tests/udemy/part10/folder1/f2.txt");
        //var printWriter = new PrintWriter("tests/udemy/part10/folder1/f3.txt");
        //var dfs = new FileOutputStream("tests/udemy/part10/folder1/f4.txt");
    }
}

class Q23 {
    public static void main(String[] args) {
        try(var pw = new PrintWriter("tests/udemy/part10/f1.txt"))
        {
            pw.close();
            pw.write(1);
        } catch(IOException e) {
            System.out.println("IOException");
        }
    }
}

class Q24 {
    public static void main(String[] args) throws IOException {
        var bos = new BufferedOutputStream(new FileOutputStream("tests/udemy/part10/f1.txt"));
        bos.write(2);
        bos.close();
    }
}

class Q25 {
    public static void main(String[] args) throws IOException {
        try (var fis = new FileInputStream("tests/udemy/part10/f1.txt");
             var fos = new FileOutputStream("tests/udemy/part10/f2.txt")) {
            int res;
            var arr = new byte[500000]; //Line 10
            while((res = fis.read(arr)) != -1){ //Line 11
                fos.write(arr, 0, res); //Line 12
            }
        }
    }
}

class Q26 {
    public static void main(String[] args) throws IOException {
        try (var reader = new InputStreamReader(new FileInputStream("tests/udemy/part10/f1.txt"))) {
            while (reader.ready()) {
                reader.skip(1);
                reader.skip(1);
                System.out.print((char) reader.read());
            }
        }
    }
}

class Q27 {
    public static void main(String[] args) throws FileNotFoundException {
        var text = """
                Hello
                World
                """;

        try(var ps = new PrintStream("tests/udemy/part10/f1.txt")) {
            /*INSERT*/
            ps.writeBytes(text.getBytes());

            //ps.write(text.getBytes());

            byte [] arr = text.getBytes();
            ps.write(arr, 0, arr.length);

            for(var b : text.getBytes())
                ps.write(b);
        }
    }
}

//
class Q34Person {
    private String name;
    private int age;

    public Q34Person() {
        this.age = 1;
    }

    public Q34Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }
}

class Q34Student extends Q34Person implements Serializable {
    private String course;

    public Q34Student(String name, int age, String course) {
        super(name, age);
        this.course = course;
    }

    public String getCourse() {
        return course;
    }
}

class Q34 {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        var stud = new Q34Student("Aamir", 25, "Computer Science");
        try (var oos = new ObjectOutputStream(new FileOutputStream(("tests/udemy/part10/stud.ser")));
             var ois = new ObjectInputStream(new FileInputStream("tests/udemy/part10/stud.ser")))
        {
            oos.writeObject(stud);

            var s = (Q34Student) ois.readObject();
            System.out.printf("%s, %d, %s", s.getName(), s.getAge(), s.getCourse());
        }
    }
}

//

class Q36Product implements Serializable {
    int i = 100;

    private static final ObjectStreamField[] serialPersistentFields = {
            new ObjectStreamField("name", String.class),
            new ObjectStreamField("geners", String[].class),
            new ObjectStreamField("i", int.class)
    };

    private void writeObject(ObjectOutputStream s) throws IOException {
        ObjectOutputStream.PutField fields = s.putFields();
        fields.put("name", "BOOK");
        fields.put("geners", new String [] {"Fiction", "Mystery", "Thriller"});
        fields.put("i", i);

        s.writeFields();
    }

    private void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException, ClassCastException {
        ObjectInputStream.GetField fields = s.readFields();
        System.out.println(fields.get("name", "NOVELS"));
        System.out.println(((String[])fields.get("geners", new String[]{"F", "M", "T"}))[1]);
        System.out.println(fields.get("i", 1));
    }
}

class Q36 {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        var product = new Q36Product();
        try (var oos = new ObjectOutputStream(new FileOutputStream(("tests/udemy/part10/stud.ser")));
             var ois = new ObjectInputStream(new FileInputStream("tests/udemy/part10/stud.ser")))
        {
            oos.writeObject(product);

            var s = (Q36Product)ois.readObject();
            System.out.println(s.i);
        }
    }
}

class Q38 {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        var optional = Optional.of(List.of("O", "N"));
        try (var oos = new ObjectOutputStream(new FileOutputStream(("tests/udemy/part10/stud.ser")));
             var ois = new ObjectInputStream(new FileInputStream("tests/udemy/part10/stud.ser")))
        {
            oos.writeObject(optional);

            var object = (Optional<?>)ois.readObject();
            System.out.println(object.get());
        }
    }
}

//
class Q39Store implements Serializable {
    int i;
    Double d;
    String s;
    StringBuilder sb;
    static Object object = new Object() {
        public String toString() {
            return "OBJECT";
        }
    };
    List<String> colors = new ArrayList<>(List.of("R", "G", "B"));

    Q39Store(int i, Double d) {
        this.i = i;
        this.d = d;
    }
}

class Q39 {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        var store = new Q39Store(10, Double.parseDouble("100.0"));
        try (var oos = new ObjectOutputStream(new FileOutputStream(("tests/udemy/part10/Book.java")));
             var ois = new ObjectInputStream(new FileInputStream("tests/udemy/part10/Book.java")))
        {
            oos.writeObject(store);

            var s = (Q39Store)ois.readObject();
            System.out.println(s.i + ":" + s.d + ":" + s.s + ":" + s.sb + ":" + s.object + ":" + s.colors) ;
        }
    }
}
//

///Users/admin/IdeaProjects/ocajp829/tests/udemy/part10/stud.ser
class Q40 {
    public static void main(String[] args) throws IOException {
        var file = Paths.get("tests/./././././udemy/part10/udemy/../stud.ser");
        System.out.println(file.toRealPath());
        System.out.println(file.toAbsolutePath());
    }
}

class Q44 {
    public static void main(String[] args) {
        var file = Paths.get("Book.java");
        System.out.println(file.toAbsolutePath());
    }
}

class Q45 {
    public static void main(String[] args) {
        var file1 = Paths.get("tests/./././././udemy/part10/udemy/../C");
        var file2 = Paths.get("Book.java");
        file1.resolve(file2);
        //tests/./././././udemy/part10/udemy/../C/Book.java
        System.out.println(file1.resolve(file2));
        //tests/./././././udemy/part10/udemy/../Book.java
        System.out.println(file1.resolveSibling(file2));
    }
}

class Q46 {
    public static void main(String[] args) {
        var file1 = Paths.get("tests/udemy/part10/A/B").toAbsolutePath();
        var file2 = Paths.get("tests/udemy/part10/A/B/Book.java").toAbsolutePath();
        System.out.println(file1.resolve(file2).equals(file1.resolveSibling(file2)));
    }
}

class Test {
    public static void main(String[] args) {
        var path = Paths.get("tests/udemy/part10/Book.java");
        /*INSERT*/
        for(var p : path) {
            System.out.println(p);
        }

        for(var i = 0; i < path.getNameCount(); i++) {
            System.out.println(path.getName(i));
        }

        var iterator = path.iterator();
        while(iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        path.forEach(System.out::println);
    }
}

class Q50 {
    public static void main(String[] args) throws IOException {
        var path = Paths.get("//A/B/C/");
        System.out.printf("%d, %s, %s", path.getNameCount(), path.getFileName(), path.getName(2));
    }
}

class Q51 {
    public static void main(String[] args) {
        var path = Paths.get("/A");
        System.out.println(path.getRoot()); // /
        System.out.println(path.getParent()); // /
        System.out.println(path.getRoot().equals(path.getParent()));
    }
}

class Q52 {
    public static void main(String[] args) throws IOException{
        var path = Paths.get("tests/udemy/part10/Book.java");
        var size1 = Files.size(path);

        var file = new File("tests/udemy/.../part10/Book1java");
        System.out.println(file.toPath().normalize());
        var size2 = file.length();
        System.out.println(size1);
        System.out.println(size2);

        System.out.println(size1 == size2);
    }
}

class Q56 {
    public static void main(String[] args) {
        var path1 = Paths.get("tests/udemy/part10");
        var path2 = Paths.get("tests");
        System.out.println(path1.relativize(path2));
        System.out.println(path2.relativize(path1));
    }
}

class Q57 {
    public static void main(String[] args) throws IOException {
        var root = Paths.get("tests");
        BiPredicate<Path, BasicFileAttributes> predicate = (p, a) -> {
            System.out.println(p.getFileName().toString().endsWith(".class"));
            return p.toString().endsWith(".class");
        };
        try(var paths = Files.find(root, 2, predicate))
        {
            paths.forEach(System.out::println);
        }
    }
}

class Q60 {
    public static void main(String[] args) throws IOException {
        /*INSERT*/
        Files.lines(Paths.get("tests/udemy/part10/Book.java")).forEach(System.out::println);
        Files.readAllLines(Paths.get("tests/udemy/part10/Book.java")).forEach(System.out::println);
        Files.readAllLines(Paths.get("tests/udemy/part10/Book.java"))
                .stream()
                .forEach(System.out::println);
    }
}

class Q61 {
    public static void main(String[] args) throws IOException {
        var files = Files.list(Paths.get(System.getProperty("user.home")));
        files.forEach(System.out::println);
    }
}

class Q62 {
    public static void main(String[] args) throws IOException {
        var paths = Files.walk(Paths.get("tests/udemy/part10/test"));
        paths.filter(path -> !Files.isDirectory(path)).forEach(
                path -> {
                    System.out.println(path);
                    try {
                        Files.readAllLines(path, StandardCharsets.ISO_8859_1)
                                .stream()
                                .forEach(System.out::println);
                    } catch (IOException e) {
                        System.out.println("FAILED");
                    }
                }
        );
    }
}

//

@FunctionalInterface
interface Q63Log {
    void log(String s);

    /**
     * @deprecated This method has been deprecated,
     * use log(String) method instead
     */
    @Deprecated(since = "9", forRemoval = true)
    default void print(String s) {
        System.out.println(s);
    }
}

class Q63 {
    public static void main(String[] args) {
        Q63Log l = s -> System.out.println(s.toUpperCase());
        l.print("hello");
    }
}

//

class Q64Animal {
    @Deprecated
    public void eat() { //Line n1
        System.out.println("Generic Animal eating");
    }
    @Deprecated
    public void speak() { //Line n2
        System.out.println("Generic Animal speaking");
    }
}

class Q64Dog extends Q64Animal {
    @Override
    public void eat() { //Line n3
        System.out.println("Dog is eating biscuits");
    }

    @Override
    public void speak() { //Line n4
        System.out.println("Dog is barking");
    }
}

class Q64 {
    public static void main(String[] args) {
        Q64Dog animal = new Q64Dog();
        animal.speak(); //Line n5
        animal.eat(); //Line n6
    }
}
//

class Q66Util {
    /**
     * @deprecated This method has been deprecated,
     * use add(int...) method instead
     */
    @Deprecated(since = "10")
    //Line n2
    public static int add (int n1, int n2) {
        return n1 + n2;
    }

    public static int add(int... nums) {
        var sum = 0;
        for(int i : nums)
            sum += i;
        return sum;
    }
}

//Line n3
//@SuppressWarnings({"deprecated"})
class Q66 {
    //Line n4
    @SuppressWarnings({"deprecation"})
    public static void main(String[] args) {
        System.out.println(Q66Util.add(10));
        System.out.println(Q66Util.add(10, 20));
        System.out.println(Q66Util.add(10, 20, 30));
    }
}
//

class Q67StringUtil {
    /**
     * @deprecated This method has been deprecated,
     * use rev(String) method instead
     */
    @Deprecated(since = "11", forRemoval = true)
    public static String reverse(String str) {
        return str.chars() //IntStream
                .mapToObj(c -> (char)c) //Convert int to char
                .reduce("", (s1,s2) -> s2+s1, (s1,s2) -> s2+s1);

    }

    public static String rev(String str) {
        return new StringBuilder(str).reverse().toString();
    }
}

class Q67 {
    //Line n1
    @SuppressWarnings({"deprecated", "removal"})
    public static void main(String[] args) {
        System.out.println(Q67StringUtil.rev("HELLO")); //Line n2
        System.out.println(Q67StringUtil.reverse("HELLO")); //Line n3
    }
}

class Q68 {
    @SafeVarargs Q68(String... strings) {} //Line n1

    //@SafeVarargs public Q68(String msg) {} //Line n2

    //@SafeVarargs public void greet(String... msg) {} //Line n3

    @SafeVarargs private void print(String... strings) {} //Line n4

    @SafeVarargs final Integer sum(Integer... integers) {return null;} //Line n5
}

class Q69 {
    /*Insert-1*/
    @SuppressWarnings({"unchecked", "deprecation"})
    public static void main(String[] args) {
        System.out.println(combine(List.of(1, 2), List.of(3, 4)));
    }


    /*Insert-2*/
    @SafeVarargs
    public static List<Integer> combine(List<Integer>... list) {
        return Arrays.stream(list)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }
}

class Q70 {
    /*INSERT-1*/
    //@SuppressWarnings("unchecked")
    public static void main(String[] args) {
        List<Integer> list = create();
    }

    /*INSERT-2*/
    @SuppressWarnings("unchecked")
    private static List create() {
        return new ArrayList();
    }
}

class Q74 {
    public static void main(String[] args) {
        System.out.println(Paths.get("A", "..", "B", ".."));
        var path = Paths.get("A", "..", "B", "..").normalize();
        System.out.println(path);
        System.out.println(path.getNameCount());
        System.out.println(path.getName(0).toString().length());
    }
}

class Q75 {
    public static void main(String[] args) {
        System.out.println(Paths.get(".", "..", ".", "..", "..", "."));
        var path = Paths.get("tests",".", "..", ".", "..", "..", ".").normalize();
        System.out.println(path);
        System.out.println(path.getNameCount());
        System.out.println(path.getName(0).toString().length());
    }
}

class Q76 {
    public static void main(String[] args) {
        var path = Paths.get("/", "..", ".", "..").normalize();
        System.out.println(path);
        System.out.println(path.getRoot());
        System.out.println(path.getName(0));
        System.out.println(path.getNameCount() + ":" + path.getName(0).toString().length());
    }
}

class Q77 {
    public static void main(String[] args) throws IOException{
        var path = Paths.get("tests/udemy");
        System.out.println(Files.isDirectory(path));
        System.out.println(path.toFile().isDirectory());
        System.out.println(Files.getAttribute(path, "isDirectory"));

        System.out.println(Files.getAttribute(path, "creationTime"));
        System.out.println(Files.readAttributes(path, "*"));
        System.out.println(Files.readAttributes(path, "*").get("creationTime"));
        System.out.println(Files.readAttributes(path, BasicFileAttributes.class).creationTime());
        /*INSERT*/
    }
}

class Q82 {
    public static void main(String[] args) throws IOException{
        var src = Paths.get("tests/udemy/part10/Book.java");
        var tgt = Paths.get("tests/udemy/part10/test3");
        var copy = Files.copy(src, tgt);
        System.out.println(Files.isSameFile(src, copy));
        System.out.println(Files.isSameFile(tgt, copy));
    }
}

class Q84 {
    public static void main(String[] args) throws IOException {
        var src = Paths.get("tests/udemy/part10/Book.java");
        var tgt = Paths.get("tests/udemy/part10/test/Book.java");
        Files.copy(src, tgt);
    }
}

class Q85 {
    public static void main(String[] args) throws IOException{
        var src = Paths.get("tests/udemy/part10/Book.java");
        try(var reader = Files.newBufferedReader(src))
        {
            String str = null;
            while((str = reader.readLine()) != null) {
                System.out.println(str);
            }
        }
    }
}

class Q86 {
    public static void main(String[] args) {
        try {
            new File("tests/udemy/part10/test2/t1.txt"); //Line n1
            new FileWriter("tests/udemy/part10/test2/t2.txt"); //Line n2
            new PrintWriter("tests/udemy/part10/test2/t3.txt"); //Line n3
            new BufferedWriter(new FileWriter(new File("tests/udemy/part10/test2/t4.txt"))); //Line n4
            Files.newBufferedWriter(Paths.get("tests/udemy/part10/test2", "t5.txt")); //Line n5
            Files.newBufferedWriter(Paths.get("tests", "udemy/part10/test2", "t6.txt"), StandardOpenOption.CREATE); //Line n6
            Files.newBufferedWriter(Paths.get("tests/udemy/part10/test2/t7.txt"), StandardOpenOption.CREATE_NEW); //Line n7
            Files.newBufferedWriter(Paths.get("tests/udemy/part10/test2/t8.txt"), StandardOpenOption.WRITE); //Line n8
        } catch (Exception e) {}

        System.out.println(new File("tests/udemy/part10/test2").listFiles().length);
    }
}