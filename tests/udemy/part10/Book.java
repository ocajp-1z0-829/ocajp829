package udemy.part10;

import java.io.IOException;
import java.nio.file.Paths;

class Book {
    public static void main(String[] args) throws IOException {
        var path = Paths.get("//A/B/C/");
        System.out.printf("%d, %s, %s", path.getNameCount(), path.getFileName(), path.getName(2));
    }
}