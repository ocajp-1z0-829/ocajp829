package udemy.part4;

import java.awt.event.ActionListener;
import java.time.Duration;
import java.time.LocalDate;
import java.util.Comparator;

public class Part4 {
    record C(int n) {
        int calc() {
            return n;
        }
    }
}

//
record Q2Point2D(int x, int y) {
    public Q2Point2D(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
class Q2 {
    public static void main(String[] args) {
        var p1 = new Q2Point2D(-4, -5);
        var p2 = new Q2Point2D(-4, -5);
        System.out.println(p1.equals(p2) + ":" + (p1.hashCode() == p2.hashCode()));
    }
}
//

class Q3 {
    public static void main(String[] args) {
        Record p1 = new Q2Point2D(10, 20); //Line n1
        System.out.println(
                //p1 instanceof udemy.part4.Q2Point2D p ?
                //p.getX() + p.getY() : "NOT a Point2D"
        ); //Line n2
    }
}
//

record Q6Ticket1(int id, LocalDate date) {
    Q6Ticket1(int id, LocalDate date) {
        if(id < 10001 || id > 100001) {
            throw new IllegalArgumentException(String.format("Id = %s is not valid", id));
        }
        if(date.isBefore(LocalDate.of(2022, 12, 31)) ||
                date.isAfter(LocalDate.of(2023, 1, 1))) {
            throw new IllegalArgumentException(String.format("Date = %s is not valid", date.toString()));
        }
        this.id = id;
        this.date = date;
    }
}
record Q6Ticket2(int id, LocalDate date) {
    Q6Ticket2 {
        if(id < 10001 || id > 100001) {
            throw new IllegalArgumentException(String.format("Id = %s is not valid", id));
        }
        if(date.isBefore(LocalDate.of(2022, 12, 31)) ||
                date.isAfter(LocalDate.of(2023, 1, 1))) {
            throw new IllegalArgumentException(String.format("Date = %s is not valid", date.toString()));
        }
    }
}
class Q6 {
    public static void main(String[] args) {
        System.out.println(new Q6Ticket1(10001, LocalDate.of(2022, 12, 31)));
        System.out.println(new Q6Ticket1(100001, LocalDate.of(2023, 1, 1)));
        System.out.println(new Q6Ticket1(10000, LocalDate.of(2022, 12, 31)));
        System.out.println(new Q6Ticket1(100002, LocalDate.of(2023, 1, 1)));
        System.out.println(new Q6Ticket1(10001, LocalDate.of(2022, 12, 30)));
        System.out.println(new Q6Ticket1(100001, LocalDate.of(2023, 1, 2)));
    }
}
//

class Q9 {
    record R1() {
        R1 {
            System.out.println("10");
        }
        static {
            System.out.println("20");
        }
        //{
          //  System.out.println("30");
        //}
        public String toString() {
            return "40";
        }
    }
    public static void main(String[] args) {
        System.out.println(new R1());
    }
}

class Q10 {
    public static void main(String[] args) {
        record Color(int r, int g, int b){
            public Color {
                if(r < 0 || r > 255)
                    throw new IllegalArgumentException("r value is not between 0 and 255");
                if(g < 0 || g > 255)
                    throw new IllegalArgumentException("g value is not between 0 and 255");
                if(b < 0 || b > 255)
                    throw new IllegalArgumentException("b value is not between 0 and 255");
            }
        }

        var c1 = new Color(20, 100, 255);
        var c2 = new Color(100, 200, 0);
        System.out.println(new Color(c1.r() + c2.r(), c1.g() + c2.g(), c1.b() + c2.b()));
    }
}
//
record Q11(String title, int noOfQues, Duration duration) {
    public String log() { //Line n1
        return this.toString().toUpperCase();
    }
}
class Q11_ {
    public static void main(String[] args) {
        var obj = new Q11("ocp: 1z0-829", 50, Duration.ofMinutes(90));
        System.out.println(obj.log()); //Line n2
    }
}
//
record Q12(Duration duration) {
    public Q12(int minutes) {
        this(Duration.ofMinutes(minutes));
    }
}
class Q12_ {
    public static void main(String[] args) {
        var timer = new Q12(10);
        System.out.println(timer);
    }
}
//
class Q13 {
    public void produce() {
        enum Child2 {ZERO, ONE} //Line n2
        interface Child3 { public void play(); } //Line n3
        class Child4 implements Child3 {public void play(){} } //Line n4
        System.out.println("DONE");
        record Child1(String name) implements Child3 {
            @Override
            public void play() {

            }
        } //Line n1
    }
}
class Q13_ {
    public static void main(String[] args) {
        new Q13().produce();
    }
}
//

class Q14 {
    final static int x = 10;
    public void find() {
        //final int x = 10;
        record Calculator(int num){ //Line n1
            int calc() {
                return num * x; //Line n2
            }
        }
        System.out.println(new Calculator(10).calc()); //Line n3
    }
}
class Q14_ {
    public static void main(String[] args) {
        new Q14().find(); //Line n4
    }
}
//

interface Q15 {
    String msg = "CONSISTENCY";
    public default void quoteOfTheDay() {
        record Quote(){ //Line n1
            String combine(String str) {
                return msg + "=" + str; //Line n2
            }
        }
        System.out.println(new Quote().combine("SUCCESS")); //Line n3
    }
}
class Q15_ {
    public static void main(String[] args) {
        new Q15(){}.quoteOfTheDay(); //Line n4
    }
}
//

class Q16 {
    public void print(final String message) {
        interface Printable { //Line n1
            public void print();
        }

        record Printer() implements Printable { //Line n2
            public void print() {
                //System.out.print(message.toUpperCase()); //Line n3
            }
        }
    }
    public static void main(String[] args) {
        new Q16().print("Life won't wait");
    }
}
//

class Q22 {
    Q22() {
        System.out.println(101);
    }
}
class Q22SubClass extends Q22 {
    //final
    Q22SubClass() {
        System.out.println(202);
    }
}
class Q22_ {
    public static void main(String[] args) {
        System.out.println(new Q22SubClass());
    }
}
//

abstract class Q33 {
    int num = 100;
    String operation = null;

    protected abstract void help();

    void log() {
        System.out.println("Helper-log");
    }
}
class Q33_ extends Q33 {
    private int num = 200;
    protected String operation = "LOGGING";

    protected void help() {
        System.out.println("LogHelper-help");
    }

    void log() {
        System.out.println("LogHelper-log");
    }

    public static void main(String [] args) {
        new Q33_().help();
    }
}
//
class Q41 {
    public String num = "10"; //Line n1
}

class Q41Sub extends Q41 {
    protected int num = 20; //Line n2
}

class Q41_ {
    public static void main(String[] args) {
        Q41 obj = new Q41Sub();
        System.out.println(obj.num += 2); //Line n3
    }
}
//

class Q42 {
    int var = 1000; // Line n1

    int getVar() {
        return var;
    }
}

class Q42Child extends Q42 {
    //private int var = 2000; // Line n2

    int getVar() {
        return super.var; //Line n3
    }
}

class Q42_ {
    public static void main(String[] args) {
        Q42Child obj = new Q42Child(); // Line n4
        System.out.println(obj.var); // Line n5
    }
}
//

interface Q64 {
    int salePercentage = 85;

    public static String salePercentage() {
        return salePercentage + "%";
    }
}
class Q64_ implements Q64 {}
class Q64__ {
    public static void main(String[] args) {
        Q64 [] arr = new Q64[2];
        for(Q64 b : arr) {
            System.out.println(b.salePercentage); //Line n1
            //System.out.println(b.salePercentage()); //Line n2
            System.out.println(Q64.salePercentage()); //Line n2
        }

        Q64_ [] books = new Q64_[2];
        for(Q64_ b : books) {
            System.out.println(b.salePercentage); //Line n3
            //System.out.println(b.salePercentage()); //Line n4
        }
    }
}
//

class Q92 {
    public static void main(String[] args) {
        extractInt(10.99);
        extractInt(23);
        extractInt(null);
    }

    private static void extractInt(Object obj) {
        if(obj instanceof Double d)
            System.out.println(d.intValue());
        else if(obj instanceof Integer d)
            System.out.println(d.intValue());
        else if (null instanceof String) {
            System.out.println("null");
        }
    }
}

//