package udemy.part9;

import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.*;
import java.util.stream.*;

public class Part9 {
}
class Q1 {
    public static void main(String[] args) {
        IntStream stream = new Random().ints(1, 7).limit(2);
        System.out.println(stream.max().getAsInt());
        new Random().ints(1, 7).limit(2).forEach(System.out::print);
    }
}

class Q2 {
    private static boolean isDirection(int ch) {
        return switch(ch) {
            case 'N', 'E', 'W', 'S' -> true;
            default -> false;
        };
    }

    public static void main(String[] args) {
        String str = "North East West South";
        str.chars()
                .filter(Q2::isDirection)
                .forEach(c -> System.out.print((char)c));
    }
}

class Q4 {
    public static void main(String[] args) {
        System.out.println(IntStream.range(10,1).count());
    }
}

class Q5 {
    public static void main(String[] args) {
        System.out.println(IntStream.range(-10, -10).count());
        System.out.println(IntStream.rangeClosed(-10, -10).count());
        IntStream.rangeClosed(-10, -10).forEach(System.out::println);
    }
}

class Q6 {
    public static void main(String[] args) {
        IntUnaryOperator opr = i -> i * i * i;
        int result = IntStream.range(1, 5)
                .map(opr)
                .sum();
        System.out.println(result);
    }
}

class Q7 {
    public static void main(String[] args) {
        IntStream stream = "OCP".chars();
        stream.forEach(c -> System.out.print((char)c));
        System.out.println(stream.count());
    }
}

class Q8 {
    public static void main(String[] args) {
        for(var i = 1; i <= 5; i++) {
            System.out.println("*".repeat(i));
        }

        Stream.iterate("*", s -> s += "*").limit(5)
                .forEach(System.out::println);

        Stream.iterate("*", s -> s.length() <= 5, s -> s += "*")
                .forEach(System.out::println);

        IntStream.range(1, 6)
                //.map(i -> "*".repeat(i))
                .forEach(System.out::println);

        IntStream.range(1, 6)
                .mapToObj("*"::repeat)
                .forEach(System.out::println);
    }
}

class Q10 {
    public static void main(String[] args) {
        IntStream stream = IntStream.generate(() -> new Random().nextInt(100)).limit(5);
        stream.filter(i -> i > 0 && i < 10).findFirst().stream();
        stream.filter(i -> i > 0 && i < 10).findFirst().ifPresent(System.out::println);
        stream.filter(i -> i > 0 && i < 10).findFirst().getAsInt();
    }
}

//

class Q11MyException extends Exception{}
class Q11 {
    public static void main(String[] args) throws Q11MyException {
        OptionalInt optional = OptionalInt.empty();
        System.out.println(optional.orElseThrow(Q11MyException::new));
    }
}

class Q13 {
    public static void main(String[] args) {
        LongStream.rangeClosed(51,75)
                .filter(l -> l % 5 == 0)
                .forEach(l -> System.out.print(l + " "));
    }
}
//

record Q17Employee(String name, double salary) {
    @Override
    public String toString() {
        return "{" + name + ", " + salary + "}";
    }

    public static int salaryCompare(double d1, double d2) {
        return Double.valueOf(d2).compareTo(d1);
    }
}

class Q17Test {
    public static void main(String[] args) {
        Stream<Q17Employee> employees = Stream.of(
                new Q17Employee("Aurora", 10000),
                new Q17Employee("Naomi", 12000),
                new Q17Employee("Hailey", 7000)
        );

        highestSalary(employees);
    }

    private static void highestSalary(Stream<Q17Employee> emp) {
        System.out.println(emp.map(e -> e.salary())
                .max(Q17Employee::salaryCompare));
    }
}

//

class Q18 {
    public static void main(String[] args) {
        var list = List.of(10, 20, 8);

        System.out.println(list.stream().max(Comparator.comparing(a -> a)).get()); //Line 1

        System.out.println(list.stream().max(Integer::compareTo).get()); //Line 2

        System.out.println(list.stream().max((i, j) -> 1).get()); //Line 3
        System.out.println(list.stream().max(Integer::max).get()); //Line 3
    }
}

class Q20 {
    public static void main(String[] args) {
        Stream<Integer> stream = Arrays.asList(1,2,3,4,5).stream();
        //System.out.println(stream.sum());
    }
}

class Q22 {
    public static void main(String[] args) {
        Stream<Integer> stream = Arrays.asList(1,2,3,4,5).stream();
        System.out.println(stream.mapToInt(i -> i)
                .average().getAsDouble());
               // .getAsInt());
    }
}

class Q23 {
    public static void main(String[] args) {
        LongStream stream = LongStream.empty();
        System.out.println(stream.average());
    }
}

class Q24 {
    public static void main(String[] args) {
        IntStream stream = IntStream.rangeClosed(1, 20)
                .filter(i -> i % 2 == 0);
        System.out.println(stream.summaryStatistics());
    }
}

class Q25 {
    public static void main(String[] args) {
        String text = "I am going to pass OCP exam in first attempt";
        Stream<String> stream = Arrays.stream(text.split(" "));
        IntSummaryStatistics stat = stream.mapToInt(s -> s.length()).summaryStatistics();
        System.out.println(stat.getMax());
    }
}

class Q26 {
    public static void main(String[] args) {
        int res = 1;
        IntStream stream = IntStream.rangeClosed(1, 5);

        System.out.println(stream.reduce(1, (i, j) -> i * j));
        System.out.println(stream.reduce(res, (i, j) -> i * j));
        /*INSERT*/
    }
}

class Q27 {
    public static void main(String[] args) {
        int res = 1;
        IntStream stream = IntStream.rangeClosed(1, 4);

        System.out.println(stream.reduce(res++, (i, j) -> i * j));
    }
}

class Q29 {
    public static void main(String[] args) {
        Stream<Double> stream = Arrays.asList(1.8, 2.2, 3.5).stream();
        System.out.println(stream.reduce((d1, d2) -> d1 + d2)); //Line n1
    }
}

class Q31 {
    public static void main(String[] args) {
        var list = List.of(false, Boolean.valueOf(null), Boolean.valueOf("1"), Boolean.valueOf("0"));
        System.out.println(list);
        BinaryOperator<Boolean> operator = (b1, b2) -> b1 || b2;
        System.out.println(list.stream().reduce(false, operator));
    }
}

class Q33 {
    public static void main(String[] args) {
        Stream.of(2, 4, 6, 8, 10, 12, 100)
                .filter(Predicate.isEqual(100))
                .reduce((i1, i2) -> i1 + i2)
                .stream()
                .forEach(System.out::println);
    }
}

//
record Q37Person(int id, String name) {}

 class Q37Test {
    public static void main(String[] args) {
        Q37Person p1 = new Q37Person(1010, "Sean");
        Q37Person p2 = new Q37Person(2843, "Rob");
        Q37Person p3 = new Q37Person(1111, "Lucy");

        Stream<Q37Person> stream = Stream.of(p1, p2, p3);
        Map<Integer, Q37Person> map = stream.collect(Collectors.toMap(p -> p.id(), Function.identity()));
        //Map<Integer, Q37Person> map = stream.collect(Collectors.toMap(p -> p.id(), p -> p));
        //Map<Integer, Q37Person> map = stream.collect(Collectors.toCollection(TreeMap::new));
        System.out.println(map.size());
    }
}
//

class Q38 {
    public static void main(String[] args) {
        var stream = Stream.of(9, 2, 1, 8, 9, 2, 9);
        System.out.println(stream.distinct()
                .map(i -> "" + i)
                .collect(Collectors.joining())
        );
    }
}

class Q39 {
    public static void main(String[] args) {
        var a = DoubleStream.iterate(Double.valueOf(1.0), i -> i <= 3.0, i -> i + 1);
        var b = a.mapToObj(i -> "" + i)
                .collect(Collectors.joining(", "));
        System.out.println(b);
    }
}

class Q40 {
    public static void main(String[] args) {
        String str = Stream.of("an", "and", "after", "or", "before")
                .takeWhile(s -> s.length() < 4)
                .collect(Collectors.joining(", "));
        System.out.println(str);
    }
}

class Q41 {
    public static void main(String[] args) {
        String str = Stream.of("a", "an", "and", "alas", "after")
                .dropWhile(s -> s.length() > 4)
                .collect(Collectors.joining(", "));
        System.out.println(str);
    }
}

//

record Q42Certification(String studId, String test, int marks) {
    public String toString() {
        return "{" + studId + ", " + test + ", " + marks + "}";
    }
}
 class Q42CsTest {
    public static void main(String[] args) {
        Q42Certification c1 = new Q42Certification("S001", "OCA", 87);
        Q42Certification c2 = new Q42Certification("S002", "OCA", 82);
        Q42Certification c3 = new Q42Certification("S001", "OCP", 79);
        Q42Certification c4 = new Q42Certification("S002", "OCP", 89);
        Q42Certification c5 = new Q42Certification("S003", "OCA", 60);
        Q42Certification c6 = new Q42Certification("S004", "OCA", 88);

        Stream<Q42Certification> stream = Stream.of(c1, c2, c3, c4, c5, c6);
        Map<Boolean, List<Q42Certification>> map =
                stream.collect(Collectors.partitioningBy(s -> s.equals("OCA")));
        System.out.println(map.get(true));
        System.out.println(map);
    }
}

class Q47 {
    public static void main(String[] args) {
        var strs1 = Stream.of("E", "a");
        var strs2 = Stream.of("I", "u", "O");

        var list1 = strs1.map(s -> s.toUpperCase())
                .sorted()
                .toList();
        var list2 = strs2.map(s -> s.toUpperCase())
                .sorted()
                .collect(Collectors.toList());

        list1.addAll(list2);

        System.out.println(list1.stream()
                .collect(Collectors.joining(", ")));
    }
}

class Q48 {
    public static void main(String[] args) {
        var stream = Stream.of("r", "t", "a");

        var list = stream.map(s -> s.toUpperCase())
                .toList();

        list.sort(String::compareTo);

        System.out.println(list);
    }
}

class Q49 {
    public static void main(String[] args) {
        var numbers = Stream.of(100, 200, 300, 400, 500);

        Collector<Integer, ?, Long> downstream1 = Collectors.summingLong(x -> Long.valueOf(x)); //Long::valueOf
        Collector<Integer, ?, Long> downstream2 = Collectors.counting();
        BiFunction<Long, Long, Long> merger = (sum, count) -> sum/count;

        var average = numbers.collect(Collectors.teeing(downstream1, downstream2, merger));
        System.out.println(average);
    }
}

class Q50 {
    public static void main(String[] args) {
        Stream<Integer> ints = Stream.of(11, 22, 22, 33);
        var result = ints.collect(
                Collectors.teeing(
                        Collectors.minBy(Integer::compare),
                        Collectors.summingInt(Integer::intValue),
                        (x, y) -> y - x.orElse(0)));
        System.out.println(result);
    }
}

//

record Q51Student(int id, String name, int marks) {}

class Q51Test {
    public static void main(String[] args) {
        var students = Stream.of(
                new Q51Student(1001, "Sharon", 925),
                new Q51Student(1002, "Amy", 875),
                new Q51Student(1003, "Ryan", 875),
                new Q51Student(1004, "Eric", 900));

        var result = students.collect(Collectors.teeing(
                Collectors.minBy(Comparator.comparing(Q51Student::marks)),
                Collectors.maxBy(Comparator.comparing(Q51Student::marks)),
                (r1, r2) -> r1.get().marks() + r2.get().marks())
        );

        System.out.println(result);
    }
}

class Q60 {
    public static void main(String[] args) {
        var list = List.of("S", "P", "I", "R", "I", "T");
        list.forEach(System.out::print);
        list.stream().forEach(System.out::print);
        list.stream().map(Function.identity()).forEach(System.out::print);
        list.parallelStream().forEachOrdered(System.out::print);
        System.out.println(list.stream().collect(Collectors.joining()));
    }
}

class Q66 {
    public static void main(String[] args) {
        List<Integer> list = Collections.synchronizedList(new ArrayList<>());
        IntStream stream = IntStream.rangeClosed(1, 7);
        stream.parallel().map(x -> {
            list.add(x); //Line 13
            return x;
        }).forEach(System.out::print); //Line 15
        System.out.println();
        list.forEach(System.out::print); //Line 17
    }
}

class Q68 {
    public static void main(String[] args) {
        var s1 = List.of("A", "E", "I", "O", "U").stream()
                .reduce("_", String::concat);
        var s2 = List.of("A", "E", "I", "O", "U").parallelStream()
                .reduce("_", String::concat);
        System.out.println(s1);
        System.out.println(s2);
        System.out.println(s1.equals(s2));
    }
}

class Q69 {
    public static void main(String[] args) {
        var str1 = List.of("S", "P", "O", "R", "T").stream()
                .reduce("", String::concat);
        var str2 = List.of("S", "P", "O", "R", "T").parallelStream()
                .reduce("", String::concat);
        System.out.println(str1.equals(str2));
    }
}

class Q70 {
    public static void main(String[] args) {
        var str1 = Stream.iterate(1, k -> k <= 10, i -> i + 1)
                .reduce("", (i, s) -> i + s, (s1, s2) -> s1 + s2);
        var str2 = Stream.iterate(1, k -> k <= 10, i -> i + 1)
                .parallel()
                .reduce("", (i, s) -> i + s, (s1, s2) -> s1 + s2);
        System.out.println(str1);
        System.out.println(str2);
        System.out.println(str1.equals(str2));
    }
}

//
class Q72Book {
    String isbn;
    double price;

    Q72Book(String isbn, double price) {
        this.isbn = isbn;
        this.price = price;
    }

    public String toString() {
        return "Book[" + isbn + ":" + price + "]";
    }
}

class Q72 {
    public static void main(String[] args) {
        List<Q72Book> books = new ArrayList<>();
        books.add(new Q72Book("9781976704031", 9.99));
        books.add(new Q72Book("9781976704032", 15.99));

        Q72Book b = books.stream().reduce(new Q72Book("9781976704033", 0.0), (b1, b2) -> {
            b1.price = b1.price + b2.price;
            return new Q72Book(b1.isbn, b1.price);
        });

        books.add(b);
        books.parallelStream().reduce((x, y) -> x.price > y.price ? x : y)
                .ifPresent(System.out::println);
    }
}
//

class Q75Counter implements Runnable {
    private static int i = 3;

    public void run() {
        System.out.print(i--);
    }
}

 class Q75 {
    public static void main(String[] args) {
        var t1 = new Thread(new Q75Counter());
        var t2 = new Thread(new Q75Counter());
        var t3 = new Thread(new Q75Counter());
        var threads = new Thread[] {t1, t2, t3};
        for(var thread : threads) {
            thread.start();
        }
    }
}

class Q76Caller implements Callable<Void> {
    String str;

    public Q76Caller(String s) {
        this.str = s;
    }

    public Void call() throws Exception {
        System.out.println(str.toUpperCase());
        return null;
    }
}

class Q76 {
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        var es = Executors.newSingleThreadExecutor();
        var future = es.submit(new Q76Caller("Call"));
        System.out.println(future.get());
        es.shutdown();
    }
}
//

class Q77MyCallable implements Callable<Integer> {
    private Integer i;

    public Q77MyCallable(Integer i) {
        this.i = i;
    }

    public Integer call() throws Exception {
        return --i;
    }
}

class Q77 {
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        var es = Executors.newSingleThreadExecutor();
        var callable = new Q77MyCallable(100);
        System.out.println(es.submit(callable).get());
        System.out.println(es.submit(callable).get());
        es.shutdown();
    }
}

//

class Q79MyCallable implements Callable<Integer> {
    private Integer i;

    public Q79MyCallable(Integer i) {
        this.i = i;
    }

    public Integer call() throws Exception {
        return --i;
    }
}

class Q79MyThread extends Thread {
    private int i;

    Q79MyThread(int i) {
        this.i = i;
    }

    public void run() {
        i++;
    }
}

 class Q79 {
    public static void main(String[] args) throws ExecutionException, InterruptedException{
        var es = Executors.newSingleThreadExecutor();
        var callable = new Q79MyCallable(10);
        var thread = new Q79MyThread(10);
        System.out.println(es.submit(callable).get());
        System.out.println(es.submit(thread).get());
        es.shutdown();
    }
}

//
class Q80 {
    public static void main(String [] args) throws InterruptedException, ExecutionException, TimeoutException {
        var es = Executors.newSingleThreadExecutor();
        var f1 = es.submit(() -> {});
        var f2 = es.submit(() -> 2);
        var f3 = es.submit(() -> {
            System.out.println("Test");
        }, 3);
        System.out.println(f1.get(1, TimeUnit.HOURS));
        System.out.println(f2.get(2, TimeUnit.HOURS));
        System.out.println(f3.get(3, TimeUnit.HOURS));
        es.shutdown();
    }
}

//

class Q81 {
    public static void main(String [] args) throws InterruptedException, ExecutionException, TimeoutException {
        Callable<String> r = () -> {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "ATTACK";
        };

        var es = Executors.newSingleThreadExecutor();
        var f = es.submit(r);
        System.out.println(f.get(1000, TimeUnit.MILLISECONDS));
        es.shutdown();
    }
}

class Q82 {
    public static void main(String [] args) throws Exception {
        Callable<String> r = () -> {
            int sum = 0;
            for(int i = 1; i < 1000; i++) {
                int random = new Random().nextInt(50);
                System.out.println(random);
                sum += i / random;
            }
            return "" + sum;
        };

        var es = Executors.newSingleThreadExecutor();
        var f = es.submit(r);
        System.out.println(f.get()); //Line n1
        es.shutdown();
    }
}
//

class Q83MyThread implements Runnable {
    private String str;

    Q83MyThread(String str) {
        this.str = str;
    }

    public void run() {
        System.out.println(str.toUpperCase());
    }
}

 class Q83 {
    public static void main(String[] args) throws ExecutionException, InterruptedException{
        var es = Executors.newSingleThreadExecutor();
        var thread = new Q83MyThread("ocp");
        var future = es.submit(thread);
        var tmp = (Integer) future.get(); //Line 22
        System.out.println(tmp);
        es.shutdown();
    }
}

//
class Q84 {
    static int var = 0; //Line n1
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        var es = Executors.newFixedThreadPool(10);
        var results = new Future<?> [3];

        Stream.of("A", "R", "M")
                .toList()
                .forEach(str -> results[var++] = es.submit(() -> str)); //Line n2

        for(var x = 0; x < results.length; x++)
            System.out.print(results[x].get() + "");

        es.shutdown();
    }
}

class Q85 {
    private void increment(int start, int end) throws InterruptedException, ExecutionException {
        var es = Executors.newFixedThreadPool(10);
        var results = new Future<?>[26];
        IntStream.rangeClosed(start, end)
                .parallel()
                .forEach(x -> results[x] = es.submit(() -> new String(new char[]{(char)(x + 65)})));
        System.out.println(results[1].get());
        es.shutdown();
    }

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        new Q85().increment(0, 25);
    }
}

//

class Q86Printer implements Runnable {
    public void run() {
        System.out.println("Printing");
    }
}

class Q86Test {
    public static void main(String[] args) {
        var es = Executors.newFixedThreadPool(1);
        es.submit(new Q86Printer());
        es.execute(new Q86Printer());

        /*INSERT*/
        es.shutdown();
    }
}

class Q87 {
    static int ctr = 0;
    public static void main(String[] args) throws InterruptedException,
            ExecutionException {
        var es = Executors.newFixedThreadPool(3);
        es.execute(() -> ctr++); //Line n1
        es.submit(() -> ctr++); //Line n2
        var s = es.submit(() -> {}, "$".repeat(ctr++)); //Line n3
        System.out.println(s.get());
        es.shutdown();
    }
}

class Q90 {
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        var es = Executors.newSingleThreadExecutor();
        var f = es.submit(() -> "HELLO");
        System.out.println(f.get());
        es.shutdown();
    }
}

class Q91 {
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        var es = Executors.newSingleThreadExecutor();
        //Future<String> f = es.execute(() -> "HELLO");
        //System.out.println(f.get());
        es.shutdown();
    }
}

class Q92 {
    public static void main(String[] args) {
        var es = Executors.newSingleThreadExecutor();
        es.execute(() -> System.out.println("HELLO"));
        es.shutdown();
    }
}

class Test {
    public static void main(String[] args) {
        var list1 = List.of("Melon", "Apple", "Banana", "Mango");
        var list2 = new CopyOnWriteArrayList<>(list1);
        for(String s : list2) {
            if(s.startsWith("M")){
                list2.remove(s);
            }
        }
        System.out.println(list1);
        System.out.println(list2);
    }
}

class Q96 {
    public static void main(String[] args) throws InterruptedException {
        Callable<String> c = new Callable<String>() {
            @Override
            public String call() throws Exception {
                try {
                    System.out.println(Thread.currentThread().getName());
                    Thread.sleep(3000);
                } catch(InterruptedException e) {}
                return "HELLO";
            }
        };

        var es = Executors.newFixedThreadPool(10);
        var list = Arrays.asList(c,c,c,c,c);
        var futures = es.invokeAll(list);
        System.out.println(futures.size());
        es.shutdown();
    }
}

//
class Q97Task1 extends RecursiveTask {
    @Override
    protected Long compute() {
        return null;
    }
}
class Q97Task2 extends RecursiveTask<Long> {
    @Override
    protected Long compute() {
        return null;
    }
}
class Q97Task3 extends RecursiveTask<Object> {
    @Override
    protected Long compute() {
        return null;
    }
}

//

class Q98Adder extends RecursiveAction {
    private int from;
    private int to;
    int total = 0;

    Q98Adder(int from, int to) {
        this.from = from;
        this.to = to;
    }

    @Override
    protected void compute() {
        if ((to - from) <= 4) {
            int sum = 0;
            for(int i = from; i <= to; i++) {
                sum += i;
            }
            total+=sum;
        } else {
            int mid = (from + to) / 2;
            Q98Adder first = new Q98Adder(from, mid);
            Q98Adder second = new Q98Adder(mid + 1, to);
            invokeAll(first, second);
        }
    }
}

class Q98 {
    public static void main(String[] args) {
        Q98Adder adder = new Q98Adder(1, 20); //Line 34
        ForkJoinPool pool = new ForkJoinPool(4);
        pool.invoke(adder);
        System.out.println(adder.total);
    }
}

//

class Q100Adder {
    private static long LIMIT = 1000000000;
    private static final int THREADS = 100;

    static class AdderTask extends RecursiveTask<Long> {
        long from, to;

        public AdderTask(long from, long to) {
            this.from = from;
            this.to = to;
        }

        @Override
        protected Long compute() {
            if ((to - from) <= LIMIT/THREADS) {
                long localSum = 0;
                for(long i = from; i <= to; i++) {
                    localSum += i;
                }
                return localSum;
            }
            else {
                long mid = (from + to) / 2;
                AdderTask first = new AdderTask(from, mid);
                AdderTask second = new AdderTask(mid + 1, to);
                first.fork();
                /*INSERT*/
                return second.compute() + first.join();
            }
        }
    }

    public static void main(String[] args) {
        ForkJoinPool pool = new ForkJoinPool(THREADS);
        long sum = pool.invoke(new AdderTask(1, LIMIT));
        System.out.printf("sum of the number from %d to %d is %d %n", 1, LIMIT, sum);
    }
}
//
class Q105Player extends Thread {
    CyclicBarrier cb;

    public Q105Player(CyclicBarrier cb) {
        this.cb = cb;
    }

    public void run() {
        try {
            cb.await();
        } catch (InterruptedException | BrokenBarrierException e) {}
    }
}

class Q105Match implements Runnable {
    public void run() {
        System.out.println("Match is starting...");
    }
}

 class Q105 {
    public static void main(String[] args) {
        var match = new Q105Match();
        var cb = new CyclicBarrier(2, match);
        var es = Executors.newFixedThreadPool(1);
        es.execute(new Q105Player(cb));
        es.execute(new Q105Player(cb));
        es.shutdown();
    }
}

//

class Q106Player extends Thread {
    CyclicBarrier cb;

    public Q106Player(){
        super();
    }

    public Q106Player(CyclicBarrier cb) {
        this.cb = cb;
        this.start();
    }

    public void run() {
        try {
            cb.await();
        } catch (InterruptedException | BrokenBarrierException e) {}
    }
}

class Q106Match implements Runnable {
    public void run() {
        System.out.println("Match is starting...");
    }
}

class Q106 {
    public static void main(String[] args) {
        var match = new Q106Match();
        var cb = new CyclicBarrier(2, match);
        var p1 = new Q106Player(cb);
        var p2 = new Q106Player(cb);
        /*INSERT*/
    }
}
//

class Q107Tour {
    CyclicBarrier cb;

    Q107Tour(CyclicBarrier cb) {
        this.cb = cb;
        try {
            cb.await();
        } catch(InterruptedException | BrokenBarrierException e) {}
    }
}

class Q107 {
    public static void main(String[] args) throws InterruptedException, BrokenBarrierException {
        var cb = new CyclicBarrier(2, () -> System.out.println("START...")); //Line n1
        Thread t = new Thread(() -> System.out.println(new Q107Tour(cb)));
        t.start();
        var tour = new Q107Tour(cb); //Line n3
        System.out.println("END..."); //Line n4
    }
}

class Q108 {
    public static void main(String[] args) throws InterruptedException, BrokenBarrierException {
        var cb = new CyclicBarrier(6, () -> System.out.println("HURRAH!"));
        var es = Executors.newFixedThreadPool(5);
        IntStream.rangeClosed(1, 6)
                .parallel()
                .forEach(x -> es.submit(() -> cb.await()));
        es.shutdown();
    }
}

//

class Q109 {
    public static void main(String[] args) throws InterruptedException {
        String s1 = "LOCK1";
        String s2 = "LOCK2";
        List<String> list = new ArrayList<>();
        Runnable r1 = new Runnable() {
            @Override
            public void run() {
                synchronized (s2) {
                    synchronized (s1) {
                        list.add("IN");
                    }
                }
            }
        };

        Runnable r2 = new Runnable() {
            @Override
            public void run() {
                synchronized (s1) {
                    synchronized (s2) {
                        list.add("OUT");
                    }
                }
            }
        };

        var es = Executors.newFixedThreadPool(4);
        es.submit(r2);
        es.submit(r1);
        es.shutdown();
        es.awaitTermination(2, TimeUnit.MILLISECONDS);
        System.out.println(String.join("-", list).isBlank() ? "BLANK" : String.join("-", list));
    }
}

//
class Q111Accumulator {
    private List<Integer> list = new ArrayList<>();

    public synchronized void accumulate(int i) {
        list.add(i);
    }

    public List<Integer> getList() {
        return list;
    }
}

class Q111 {
    public static void main(String [] args) {
        var s = Executors.newFixedThreadPool(1000);
        var a = new Q111Accumulator();
        for(var i =1 ; i <= 10000; i++) {
            var x = i;
            s.execute(() -> a.accumulate(x));
        }
        s.shutdown();
        System.out.println(a.getList().size());
    }
}

class Q113 {
public static void main(String [] args) {
    var ai = new AtomicInteger(10);
    System.out.println(ai.addAndGet(1) + ":" + ai);
    System.out.println(ai.incrementAndGet() + ":" + ai.get());

    /*INSERT*/
}
}

class Q114Counter implements Runnable {
    private static AtomicInteger ai = new AtomicInteger(3);

    public void run() {
        System.out.print(ai.getAndDecrement());
    }
}

class Q114 {
    public static void main(String[] args) {
        var t1 = new Thread(new Q114Counter());
        var t2 = new Thread(new Q114Counter());
        var t3 = new Thread(new Q114Counter());
        Thread[] threads = {t1, t2, t3};
        for(var thread : threads) {
            thread.start();
        }
    }
}