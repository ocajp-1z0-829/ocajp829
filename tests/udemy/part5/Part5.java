package udemy.part5;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.util.Arrays;
import java.util.Scanner;

public class Part5 {
}

interface ILogger {
void log();
}

class Q3 {
    public static void main(String[] args) {
        ILogger [] loggers = new ILogger[2]; //Line n1
        System.out.println(loggers);
        for(ILogger logger : loggers)
            logger.log(); //Line n2
    }
}

class Q4 {
    public void read() throws FileNotFoundException {}
}
class Q4_ {
    String Q4_;
    public void Q4_() {}
}
class Q4__ {
    private void print() {
        //private String msg = "HELLO";
        //System.out.println(msg);
    }
}

class Q16 {
    public static void convert(String s)
            throws IllegalArgumentException, RuntimeException, Exception {
        if(s.length() == 0) {
            throw new RuntimeException("LENGTH SHOULD BE GREATER THAN 0");
        }
    }
    public static void main(String [] args) {
        try {
            convert("");
        }
        catch(RuntimeException e) { //Line n1
            System.out.println(e.getMessage()); //Line n2
        } //Line n3
        catch(Exception e) {
            e.printStackTrace();
        }
    }
}

class Q17 {
    public static void main(String[] args) {
        try {
            try {
                System.out.println(args[1]); //Line n1
            } catch(RuntimeException e) {
                System.out.print("INHALE-"); //Line n2
                //IndexOutOfBoundsException ee = new IndexOutOfBoundsException("Hi");
                throw e; //Line n3
            } finally {
                System.out.print("EXHALE-"); //Line n4
            }
        } catch(RuntimeException e) {
            System.out.println(e);
            System.out.print("INHALE-"); //Line n5
        } finally {
            System.out.print("EXHALE"); //Line n6
        }
    }
}

class Q18 {
    private static void div(int i, int j) {
        try {
            System.out.println(i / j);
        } catch(ArithmeticException e) {
            Exception ex = new Exception(e);
            try {
                throw ex;
            } catch (Exception exc) {
                throw new RuntimeException(exc);
            }
        }
    }
    public static void main(String[] args) {
        try {
            div(5, 0);
        } catch(Exception e) {
            System.out.println("END");
        }
    }
}

class Q19 {
    private static void div(int i, int j) {
        try {
            System.out.println(i / j);
        } catch(ArithmeticException e) {
            throw (RuntimeException)e;
        }
    }

    public static void main(String[] args) {
        try {
            div(5, 0);
        } catch(ArithmeticException e) {
            System.out.println("AE");
        } catch(RuntimeException e) {
            System.out.println("RE");
        }
    }
}

class Q20 {
    private static void m() throws SQLException {
        try {
            throw new SQLException();
        } catch (Exception e) {
            System.out.println(e);
            throw e;
        }
    }

    public static void main(String[] args) {
        try {
            m();
        } catch(SQLException e) {
            System.out.println("CAUGHT SUCCESSFULLY");
        }
    }
}

class Q21 {
    private static void getData() throws SQLException {
        try {
            throw new SQLException();
        } catch (Exception e) {
            e = new SQLException();
            try {
                throw e;
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    public static void main(String[] args) {
        try {
            getData();
        } catch(SQLException e) {
            System.out.println("SQL");
        }
    }
}

class Q22 {
    private static void checkData() throws SQLException {
        try {
            throw new SQLException();
        } catch (Exception e) {
            e = null; //Line n1
            try {
                throw e; //Line n2
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    public static void main(String[] args) {
        try {
            checkData(); //Line n3
        } catch(SQLException e) {
            System.out.println("NOT AVAILABLE");
        }
    }
}

class Q23 {
    private static void getReport() throws SQLException {
        try {
            throw new SQLException();
        } catch (Exception e) {
            throw null; //Line n1
        }
    }

    public static void main(String[] args) {
        try {
            getReport(); //Line n2
        } catch(SQLException e) {
            System.out.println("REPORT ERROR");
        }
    }
}

//
abstract class Q29 {
    public abstract void print() throws IOException;
}

class Q29_ extends Q29 {
    @Override
    public void print() throws IOException {
        throw new FileNotFoundException();
    }
}

class Q29__ {
    public static void main(String[] args) {
        Q29 b = new Q29_();
        try {
            b.print();
        } catch (FileNotFoundException e) {
            System.out.print("AWE");
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            System.out.print("SOME");
        }
    }
}

//
interface Q31 {
    void m1() throws java.io.IOException;
}
class Q31_ implements Q31 {
    @Override
    public void m1() {
        try {
            throw new Exception();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
//
class Q32 {
    static void print() throws IOException { //Line n1
        throw new IOException(); //Line n2
    }
}

class Q32_ {
    public static void main(String[] args) { //Line n3
        try {
            Q32.print(); //Line n4
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
//

class Q34 {
    public static void main(String[] args) {
        try {
            play();
            return;
        } catch(Exception ex) {
            System.out.println(ex.getMessage());
            return;
        } finally {
            System.err.println("MATCH ABANDONED");
        }
        //System.out.println("DONE");
    }

    static void play() throws Exception {
        throw new Exception("INJURED");
    }
}

//

class Q36 {
    public static void main(String[] args) {
        try {
            find();
        } catch(Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    static void find() throws Exception {
        try {
            System.out.print(1);
            throw new FileNotFoundException("FNF");
        } catch(FileNotFoundException ex) {
            System.out.print(2);
            throw new IOException("IO");
        } catch(IOException ex) {
            System.out.print(3);
            throw new Exception("EXP");
        } finally {
            System.out.print(4);
            throw new Exception("FINALLY");
        }
    }
}
//

class Q37 {
    public static void main(String[] args) {
        try {
            check();
        } catch(RuntimeException e) {
            System.out.println(e.getClass().getName()); //Line n1
        }
    }

    private static void check() {
        try {
            RuntimeException re = new RuntimeException(); //Line n2
            throw re; //Line n3
        } catch(RuntimeException e) {
            System.out.println(1);
            System.out.println(e);
            ArithmeticException ex = (ArithmeticException)e; //Line n4
            System.out.println(2);
            throw ex;
        }
    }
}
//

interface Q38 {
void multiply(int... x) throws SQLException;
}

class Q38Calc implements Q38 {
    //public void multiply(int... x) throws SQLException /*INSERT*/ {
    //public void multiply(int... x) throws Error /*INSERT*/ {
    //public void multiply(int... x) throws SQLWarning /*INSERT*/ {
    //public void multiply(int... x) throws RuntimeException /*INSERT*/ {
    public void multiply(int... x) throws NullPointerException /*INSERT*/ {
    }
}
class Q38_ {
    public static void main(String[] args) {
        try {
            Q38 obj = new Q38Calc(); //Line n1
            obj.multiply(1, 2, 3);
        } catch(SQLException e) {
            System.out.println(e);
        }
    }
}

//

class Q44 {
    Q44() throws IOException {
        System.out.print(1);
    }
}

class Q44_ extends Q44 {
    Q44_() throws IOException {
        System.out.println(2);
    }
    //Q44_() throws FileNotFoundException {
      //  System.out.print(2);
    //}
}

class Q44__ {
    public static void main(String[] args) throws Exception {
        new Q44_();
    }
}

//

class Q52 {
    public static void main(String[] args) {
        try(Scanner scan = new Scanner(System.in)) {
            String s = scan.nextLine();
            System.out.println(s);
            //scan = null;
        }
    }
}

//



//

class Q54 {
    public static void main(String[] args) {
        System.out.print("Enter some text: ");
        try(Scanner scan = new Scanner(System.in)) {
            String s = scan.nextLine();
            System.out.println(s);
            scan.close();
            scan.close();
            //scan.nextLine();
        }
    }
}

//

class Q55 {
    public static void main(String[] args) {
        try(FileReader fr = new FileReader("C:/temp.txt")) {

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
//

class Q56 {
    public static void main(String[] args) {
        try(PrintWriter writer = new PrintWriter(System.out)) {
            writer.println("Hello");
        } catch(Exception ex) {
            //writer.close();
        }
    }
}

class Q57 {
    public static void main(String[] args) {
        try(PrintWriter writer = null) {
            System.out.println("HELLO");
        }
    }
}

class Q58 {
    public static void main(String[] args) {
        //try(PrintWriter writer;) {
          //  writer = new PrintWriter(System.out);
            //writer.println("HELLO");
        //}
    }
}

//

class Q59MyResource implements AutoCloseable {
    @Override
    public void close() throws IOException {
        throw new IOException("IOException");
    }

    public void execute() throws SQLException {
        throw new SQLException("SQLException");
    }
}

class Q59 {
    public static void main(String[] args) {
        try(Q59MyResource resource = new Q59MyResource()) {
            resource.execute();
        } catch(Exception e) {
            System.out.println(e.getSuppressed().length);
            System.out.println(e.getSuppressed()[0].getMessage());
            System.out.println(e);
        }
    }
}

//
class Q63 implements AutoCloseable {
    public void close() {
        System.out.println("CLOSE");
    }
}

class Q63_ {
    public static void main(String[] args) {
        try(Q63 r = null) {
            //r = new Q63();
            System.out.println("HELLO");
        }
    }
}
//
class Q66 {
    public static void main(String[] args) {
        int [] arr1 = {5, 10, 15};
        int [] arr2 = {'A', 'B'};
        arr1 = arr2;
        System.out.println(arr1.length + arr2.length);
    }
}
class Q67 {
    public static void main(String[] args) {
        int[] arr1 = { 10, 100, 1000 }; //Line n1
        char[] arr2 = { 'x', 'y', 'z' }; //Line n2
        //arr1 = arr2; // Line n3
        for (int i = 0; i < arr1.length; i++) {
            System.out.print(arr1[i] + " "); //Line n4
        }
    }
}
class Q68 {
    public static void main(String[] args) {
        int a = 1, b = 4, c = 5;
        System.out.println(a = b = c = a + b + c);
        System.out.println(a = b = c = a + b + c);
        String arr1 [], arr2, arr3 = null; //Line n1
        arr1 = new String[2];
        arr1[0] = "A";
        arr1[1] = "B";
        System.out.println(arr1);
        System.out.println(arr2 = "");
        System.out.println(arr3);
        //arr2 = arr3 = arr1; //Line n2
        //System.out.println(String.join("-", arr2)); //Line n3
    }
}

class Q69 {
    public static void main(String[] args) {
        byte [] arr = new byte[0];
        System.out.println(arr[0]);
    }
}

class Q71 {
    public static void main(String[] args) {
        String [] arr = {"I", "N", "S", "E", "R", "T"};
        //for(int n = 0; n < arr.length; n += 1 /*INSERT*/) {
        //for(int n = 0; n <= arr.length; n += 1 /*INSERT*/) {
        //for(int n = 1; n < arr.length; n += 2 /*INSERT*/) {
        for(int n = 1; n <= arr.length; n += 2/*INSERT*/) {
            if (n % 2 == 0) {
                continue;
            }
            System.out.print(n);
            System.out.println(arr[n]); //Line n1
        }
    }
}

class Q78 {
    public static void main(String[] args) {
        int [] arr = {10, 20, 30}; //Line n1
        int i = 0;
        //arr[0] = (arr[2] = 40);
        arr[i++] = arr[++i] = 40; //Line n2
        for(var x : arr) //Line n3
            System.out.println(x);
    }
}

class Q80 {
    public static void main(String[] args) {
        var x = new int[]{1};
        var y = new int[]{2};
        var z = new int[]{10};
        System.out.println((x = y)[0] + y[0]);
        System.out.println((x = y = z)[0] + y[0] + z[0]);
    }
}

class Q82 {
    public static void main(String[] args) {
        var arr = new Double[2];
        System.out.println(arr[0] + arr[1]);
    }
}

class Q83 {
    public static void main(String[] args) {
        int[] a1 = { 10, 15, 20 };
        int[] a2 = { 10, 20, 30 };
        System.out.println(Arrays.mismatch(a1, a2));
        System.out.println();
        System.out.println(Arrays.mismatch(a1, a2) + Arrays.compare(a1, a2));
    }
}

class Q86 {
    public static void main(String[] args) {
        try {
            System.out.print(Arrays.compare(new char[] { 'A' }, null));
        } catch (Exception ex) {
            System.out.print('A');
        }

        try {
            System.out.print(Arrays.mismatch(null, new int[] { 1 }));
        } catch (Exception ex) {
            System.out.print('B');
        }

        try {
            System.out.print(Arrays.equals(new short[] { 10 }, null));
        } catch (Exception ex) {
            System.out.print('C');
        }
    }
}

class Q87 {
    public static void main(String[] args) {
        java.util.Arrays.compare(new int [] {10}, null);
        //java.util.Arrays.compare(null, null);
        //java.util.Arrays.mismatch(new char[] {'1'}, null);
        java.util.Arrays.equals(null, new boolean[] {true});
    }
}

class Q89 {
    public static void main(String[] args) {
        char [] arr1 = {'A', 'V'};
        char [] arr2 = {'A', 'A', 'A', 'A'};
        System.out.println(Arrays.compare(arr1, arr2));
    }
}

class Q90 {
public static void main(String[] args) {
    int [] array1 = {};
    int [] array2 = {100, 200};
    System.out.print(Arrays.compare(array1, array2));
    System.out.print(Arrays.mismatch(array1, array2));
}
}

class Q92 {
    public static void main(String[] args) {
        String [] str1 = {"A", "AA", "ABCD"};
        String [] str2 = {"A", "AA", "ABCB"};
        System.out.print(Arrays.mismatch(str1, str2));
        System.out.print(Arrays.compare(str1, str2));
    }
}

class Q93 {
    public static void main(String[] args) {
        StringBuilder[] array1 = { new StringBuilder("Manners"), new StringBuilder("maketh"), new StringBuilder("Man") };
        StringBuilder[] array2 = { new StringBuilder("Manners"), new StringBuilder("maketh"), new StringBuilder("man") };
        System.out.print(Arrays.mismatch(array1, array2));
        System.out.print(Arrays.compare(array1, array2));
    }
}

class Q94 {
    public static void main(String[] args) {
        String [] array1 = {"OCP", "11", null};
        String [] array2 = {"OCP", "11"};
        String [] array3 = {null, "OCP", "11"};
        String [] array4 = {"0"};
        String [] array5 = {"1", "1", "100000", "Abc"};


        System.out.println(Arrays.compare(new String[]{null, null}, new String[]{null}));
        System.out.print(Arrays.compare(array1, array2));
        System.out.print(Arrays.compare(array2, array3));
        System.out.print(Arrays.compare(array3, array2));
        System.out.println();
        System.out.print(Arrays.compare(array4, array5));
    }
}

class Q95 {
    public static void main(String[] args) {
        int [] arr1 = new int[8];
        int [][] arr2 = new int[8][8];
        int [] arr3 [] = new int[8][];
        int arr4[][] = new int[][]{{}};
        System.out.println(Arrays.toString(arr1));
        System.out.println(Arrays.deepToString(arr2));
        System.out.println(Arrays.deepToString(arr3));
        //System.out.println(arr3[0].length);
        System.out.println(Arrays.deepToString(arr4));
    }
}

class Q100 {
    public static void main(String[] args) {
        var arr1 = new int[]{10};
        var arr2 = new String[][] {};
        var arr3 = new char[][] {{}};
        //var arr4 = {10, 20, 30};
        var arr5 = new String[][] {new String[]{"LOOK"}, new String[] {"UP"}};
        //var[] arr6 = new int[] {2, 3, 4};
        System.out.println(Arrays.toString(arr1));
        System.out.println(Arrays.toString(arr2));
        System.out.println(Arrays.deepToString(arr3));
        System.out.println(Arrays.deepToString(arr5));
    }
}

class Q101 {
    public static void main(String[] args) {
        var arr = new int[][]{}; //Line n1
        arr[1][4] = 100;
        arr[6][6] = 200;
        arr[3][6] = 300;
    }
}

//
class Q103 implements AutoCloseable {
    public void execute() {
        System.out.println("Executing");
    }

    @Override
    public void close() throws Exception {
        System.out.println("Closing");
    }
}

class Q103_ {
    public static void main(String[] args) {
        try(Q103 resource = new Q103()) {
            resource.execute();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
//
class Q104 implements AutoCloseable {
    @Override
    public void close() {
        System.out.println("Closing");
    }
}

class Q104_ {
    public static void main(String[] args) {
        try(AutoCloseable resource = new Q104()) {

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
//