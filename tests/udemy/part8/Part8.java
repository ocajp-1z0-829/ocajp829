package udemy.part8;

import java.util.*;
import java.util.function.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Part8 {
}

class Q2 {
    public static void main(String[] args) {
        Consumer<Integer> consumer = System.out::print;
        Integer i = 5;
        consumer.andThen(consumer).accept(i++); //Line n1
    }
}
//
record Q3Counter() {
    static int count = 1;
}

class Q3 {
    public static void main(String[] args) {
        Consumer<Integer> add = i -> Q3Counter.count += i;
        Consumer<Integer> print = System.out::println;
        add.andThen(print).accept(10); //Line n1
    }
}
//

interface Q4StringConsumer extends Consumer<String> {
    @Override
    public default void accept(String s) {
        System.out.println(s.toUpperCase());
    }
}

class Q4 {
    public static void main(String[] args) {
        //Q5StringConsumer consumer = s -> System.out.println(s.toLowerCase());
        Q4StringConsumer consumer = new Q4StringConsumer() {
            @Override
            public void accept(String s) {
                System.out.println(s.toLowerCase());
            }
        };
        List<String> list = Arrays.asList("Dr", "Mr", "Miss", "Mrs");
        list.forEach(consumer);
    }
}
//
class Q7 {
    public static void main(String[] args) {
        //BooleanSupplier bs = Boolean.TRUE;
    }
}

class Q11 {
    public static void main(String[] args) {
        Character ch = 65;
        System.out.println(ch);

        String s = "AaaAaaAa";
        s = s.replace("a", "b");
        System.out.println(s);

        BiFunction<Integer, Integer, Character> compFunc = (i, j) -> (char) (i + j);
        System.out.println(compFunc.apply(0, 65));
    }
}

class Test {
    public static void main(String[] args) {
        Consumer<Integer> printer = i -> System.out.println("$" + i + ".00");
        BiConsumer<List<Integer>, Consumer<Integer>> obj = (list, cons) -> list.forEach(cons);
        obj.accept(List.of(10, 15, 20), printer);
    }
}

class Q17 {
    public static void main(String[] args) {
        String text = "Aa aA aB Ba aC Ca";
        System.out.println(text.replace('a', 'b'));
        ToIntFunction<String> func = text::indexOf;
        System.out.println(func.applyAsInt("a"));
    }
}

class Q18 {
    public static void main(String[] args) {
        final String password = "Oracle";
        UnaryOperator<String> opr1 = s -> s.replace('a', '@'); //Line n1
        UnaryOperator<String> opr2 = s -> password.concat(s); //Line n2
        System.out.println("Password: " + opr1.apply(opr2.apply("!"))); //Line n3
    }
}


class Q20 {
    public static void main(String[] args) {
        UnaryOperator<String> opr1 =  s -> {
            System.out.print(1);
            return s.toUpperCase();
        };
        UnaryOperator<String> opr2 = s -> {
            System.out.print(2);
            return new StringBuilder(s).reverse().toString();
        };
        UnaryOperator<String> opr3 = s -> {
            System.out.print(3);
            return s.substring(2);
        };

        System.out.println(opr1.andThen(opr2).compose(opr3).apply("Test")); //Line n1
        System.out.println(opr1.apply("amar")); //Line n2
    }
}

class Q21 {
    public static void main(String[] args) {
        BinaryOperator<String> operator = BinaryOperator.maxBy((s1, s2) -> s2.length() - s1.length()); //Line n1
        var list = List.of(List.of("*", "****"), List.of("##", "#####"), List.of("$$$", "$$$$$$")); //Line n2
        for(var l : list) { //Line n3
            System.out.println(operator.apply(l.get(0), l.get(1)));
        }
    }
}

class Q22 {
    public static void main(String[] args) {
        BinaryOperator<List<String>> operator = BinaryOperator.minBy((s1, s2) -> s1.size() - s2.size()); //Line n1
        var list = List.of(List.of("CAPRE ", "DIEM ", "!"), List.of("BON ", "VOYAGE")); //Line n2
        operator.apply(list.get(0), list.get(1)).forEach(System.out::print); //Line n3
    }
}

class Q23 {
    public static void main(String[] args) {
        BiPredicate<String, String> predicate = String::contains;
        BiFunction<String, String, Boolean> func = (str1, str2) -> {
            return predicate.test(str1, str2) ? true : false;
        };

        System.out.println(predicate.test("Tomato", "at"));
        System.out.println(func.apply("Tomato", "at"));
    }
}

class Q25 {
    public static void main(String[] args) {
        DoubleFunction<DoubleUnaryOperator> func = m -> n -> m + n; //Line n1
        System.out.println(func.apply(11).applyAsDouble(24)); //Line n2
    }
}

class Q26 {
    public static void main(String[] args) {
        LongFunction<LongUnaryOperator> func = a -> b -> b - a; //Line n1
        System.out.println(calc(func.apply(100), 50)); //Line n2
    }

    private static long calc(LongUnaryOperator op, long val) {
        return op.applyAsLong(val);
    }
}

class Q27 {
    public static void main(String[] args) {
        System.out.println(create(new char[]{'o', 'u', 't'}, (Function<char[], String>)String::new).length()); //Line n1
        System.out.println(create(new char[]{'o', 'u', 't'}, () -> new String()).length());
    }

    private static String create(char [] c, Function<char[], String> func) { //Line n2
        return func.apply(c);
    }

    private static String create(char [] c, Supplier<String> supplier) { //Line n3
        return supplier.get();
    }
}

class Q28 {
    public static void main(String[] args) {
        Set<Integer> set = new HashSet<>();
        //add(set, s -> s.add(10)); //Line n1
        //add(set, s -> s.add(20)); //Line n2
        add(set, (Consumer<Set<Integer>>)s -> s.add(10)); //Line n1 => Matches to method at Line n3
        add(set, (Predicate<Set<Integer>>)s -> s.add(20)); //Line n2 => Matches to method at Line n4

        add(set, s -> {s.add(10); return;}); //Line n1 => Matches to method at Line n3
        add(set, s -> {return s.add(20);}); //Line n2 => Matches to method at Line n4

        Consumer<Set<Integer>> consumer = s -> s.add(10);
        add(set, consumer); //Line n1 => Matches to method at Line n3

        Predicate<Set<Integer>> predicate = s -> s.add(20);
        add(set, predicate); //Line n2 => Matches to method at Line n4

        System.out.println(set.size());
        System.out.println(set);
    }

    private static void add(Set<Integer> set, Consumer<Set<Integer>> consumer) { //Line n3
        consumer.accept(set);
    }

    private static void add(Set<Integer> set, Predicate<Set<Integer>> predicate) { //Line n4
        predicate.test(set);
    }
}

//

class Q30 {
    public static void main(String[] args) {
        Integer i = 10;
        List<Integer> list = new ArrayList<>();
        list.add(i);
        list.add(i *= 2);
        list.add(i);

        list.removeIf(j -> j == 10);

        System.out.println(list);
    }
}

class Q36 {
    public static void main(String[] args) {
        var list = new ArrayList<String>(List.of("T", "O", "A", "S", "L", "M")); //Line n1
        list.sort((var x1, var x2) -> -2 * x1.compareTo(x2)); //Line n2
        list.forEach(System.out::print); //Line n3
    }
}

class Q42 {
    public static void main(String[] args) {
        var map = new LinkedHashMap<Integer, String>();
        map.put(1,  null);
        map.put(2,  "TWO");
        map.put(3, "THREE");
        map.merge(1, "ONE", String::concat); //Line n1
        map.merge(2, "2", (s1, s2) -> s2 + ":" + s1); //Line n2
        map.merge(3, "3", (s1, s2) -> null); //Line n3
        System.out.println(map);
    }
}

class Q45 {
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(10, 100, 1000);
        list.replaceAll(i -> -(i++));

        System.out.println(list);
    }
}

class Q46 {
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList (2, 3, 4);
        UnaryOperator<Long> operator = s -> s*s*s;
        //list.replaceAll(operator);
        list.forEach(System.out::println);
    }
}

class Q48 {
    public static void main(String[] args) {
        Stream.<StringBuilder>of()
                .map(s -> s.reverse())
                .forEach(System.out::println);

        Stream.<StringBuilder>of()
                .peek(s -> System.out.println(s.length()))
                .map(StringBuilder::getClass)
                .forEach(System.out::println);
    }
}

class Q49 {
    public static void main(String[] args) {
        Stream.of(true, false, true)
                .map(b -> b.toString().toUpperCase())
                .map(a -> a).count();

        Stream.of(true, false, true)
                .map(b -> b.toString().toUpperCase())
                .peek(System.out::println).toArray();
    }
}

class Q51 {
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(-80, 100, -40, 25, 200);
        Predicate<Integer> predicate = num -> {
            int ctr = 1;
            boolean result = num > 0;
            System.out.print(ctr++ + ".");
            return result;
        };

        list.stream()
                .filter(predicate)
                .count();
    }
}

class Q55 {
    public static void main(String[] args) {
        List<String> list = Arrays.asList("M", "R", "A", "P");
        Collections.sort(list, null);
        list.stream()
                .peek(System.out::print).toArray();
    }
}

class Q58 {
public static void main(String[] args) {
    String [] arr1 = {"Virat", "Rohit", "Shikhar", "Dhoni"};
    String [] arr2 = {"Bumrah", "Pandya", "Sami"};
    String [] arr3 = {};

    Stream<String[]> stream = Stream.of(arr1, arr2, arr3);
    stream.flatMap(s -> Stream.of(s)) //Arrays.stream(s)
            .sorted()
            .forEach(s -> System.out.print(s + " "));
    }
}

class Q59 {
    public static void main(String[] args) {
        Stream<String> stream = Stream.of("ocp");
        stream.flatMapToInt(s -> s.chars())
                .forEach(i -> System.out.print((char)i));
    }
}

class Q60 {
    public static void main(String[] args) {
        long seed = 10;
        var stream = Stream.iterate(seed, i -> i <= 12, i -> i + 2); //Line n1
        LongFunction<LongUnaryOperator> func = m -> n -> n / m; //Line n2
        LongFunction<LongUnaryOperator> func1 = (m) -> {
            return (n) -> {
                return n / m;
            };
        };
        LongFunction<LongUnaryOperator> func2 = new LongFunction<LongUnaryOperator>() {
            @Override
            public LongUnaryOperator apply(long m) {
                LongUnaryOperator operator = new LongUnaryOperator() {
                    @Override
                    public long applyAsLong(long n) {
                        System.out.println(n + " " + m);
                        return n / m;
                    }
                };
                return operator;
            }
        };
        stream.mapToLong(i -> i).map(func2.apply(2)).forEach(System.out::println); //Line n3
    }
}

class Q61 {
    public static void main(String[] args) {
        /* INSERT */
        //UnaryOperator<Character> operator1 = c -> c.charValue() + 1;
        //UnaryOperator<Character> operator2 = c -> c + 1;
        //UnaryOperator<Integer> operator3 = c -> c + 1;
        //Function<Character, Integer> operator4 = x -> x + 1;
        UnaryOperator<Character> operator5 = c -> (char)(c.charValue() + 1);
        Function<Character, Character> operator6 = x -> (char)(x + 1);

        var vowels = List.of('A', 'E', 'I', 'O', 'U');
        vowels.stream().map(x -> operator6.apply(x)).forEach(System.out::print); //Line n1
    }
}

//
record MyString(String str) {}
class Q62 {
    public static void main(String[] args) {
        var list = List.of(new MyString("Y"),
                new MyString("E"), new MyString("S"));

        list.stream()
                .map(s -> s)
                .forEach(System.out::print);

        list.stream()
                .map(s -> s.str())
                .forEach(System.out::print);
    }
}
//

class Q65 {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        System.out.println(list.stream().anyMatch(s -> s.length() > 0));
        System.out.println(list.stream().allMatch(s -> s.length() > 0));
        System.out.println(list.stream().noneMatch(s -> s.length() > 0));
    }
}

class Q66 {
    public static void main(String[] args) {
        int ref = 10;
        List<Integer> list = new ArrayList<>();
        list.stream()
                .anyMatch(i -> {
                    System.out.println("HELLO");
                    return i > ref;
                });
    }
}

class Q68 {
    public static void main(String[] args) {
        Stream<Integer> stream = Stream.iterate(1, i -> {
            System.out.println(i);
            return i + 1;});
        System.out.println(stream.anyMatch(i -> i > 1));
        System.out.println(Stream.iterate(1, i -> {
            System.out.println(i);
            return i + 1;}).allMatch(i -> i > 1));
        System.out.println(Stream.iterate(1, i -> {
            System.out.println(i);
            return i + 1;}).noneMatch(i -> i > 1));
    }
}

class Q69 {
    public static void main(String[] args) {
        Stream<Double> stream = Stream.generate(() -> Double.valueOf("1.0")).limit(10);
        System.out.println(stream
                .filter(d -> d > 2)
                        .peek(System.out::println)
                .allMatch(d -> d == 2));
    }
}

class Q70 {
    public static void main(String[] args) {
        var stream = Stream.of(1, 2, 3);

        var result = stream.<String>mapMulti((x, y) -> {
            y.accept(x + "");
            y.accept(x * x + "");
            y.accept(x * x * x + "");
        }).collect(Collectors.joining(", "));

        System.out.println(result);
    }
}

class Q71 {
    public static void main(String[] args) {
        var stream = Stream.of(
                new StringBuilder("S"),
                new StringBuffer("L"),
                new String("A"),
                "S",
                new StringBuilder("H").toString());

        var result = stream.<String>mapMulti((cs, con) -> {
            if(cs instanceof String s)
                con.accept(s);
        }).collect(Collectors.joining());

        System.out.println(result);
    }
}

class Q72 {
    public static void main(String[] args) {
        Optional<Integer> optional = Optional.of(null); //Line n1 NPE
        System.out.println(optional.orElse(-1)); //Line n2
    }
}

class Q75 {
    public static void main(String[] args) {
        Optional<Integer> optional = Optional.ofNullable(null);
        System.out.println(optional);
    }
}

class Q78 {
    public static void main(String[] args) {
        Optional<Integer> optional = Stream.of(10).findFirst();
        System.out.println(optional);
    }
}

class Q80 {
public static void main(String[] args) {
    List<String> list = new ArrayList<>(Arrays.asList("Z", "Y", "X"));
    System.out.println(list.stream()
            .sorted()
            .findFirst()
            .get());
    System.out.println(list.get(2));
}
}

record Q85Toy(String name) {
    @Override
    public String toString() {
        return "Toy[" + name +"]";
    }

    @Override
    public boolean equals(Object obj) {
        return true;
    }

    @Override
    public int hashCode() {
        return 0;
    }
}

class Q85 {
    public static void main(String[] args) {
        Stream<Q85Toy> stream = Stream.of(new Q85Toy("BALL"), new Q85Toy("BAT"));
        System.out.println(Arrays.toString(Stream.of(new Q85Toy("BALL"), new Q85Toy("BAT")).toArray()));
        Optional<Q85Toy> optional = stream.distinct().peek(System.out::println).findFirst();
        System.out.println(optional.isPresent() + " : " + optional.get());
    }
}

class Q87 {
    public static void main(String[] args) {
        var oceanAnimals = List.of("Shark", "Squid", "Whale", "Starfish", "Lobster");
        oceanAnimals.stream()
                .parallel()
                .filter(s -> s.contains("e"))
                .findFirst()
                .ifPresentOrElse(s -> System.out.println("FOUND " + s + "!"),
                        () -> System.out.println("NOT FOUND!"));
    }
}

class Q88 {
    public static void main(String[] args) {
        var names = List.of("John", "william", "Claire", "HOPE", "Clark", "Hunter", "Kirk");

        search(names, "jack")
                .or(() -> search(names, "Jon"))
                .or(() -> search(names, "hope"))
                .or(() -> search(names, "Clark"))
                .stream()
                .forEach(System.out::print);
    }

    static Optional<String> search(List<String> list, String textToSearch) {
        return list.stream()
                .filter(s -> s.equalsIgnoreCase(textToSearch))
                .findFirst();
    }
}

class Q91 {
    public static void main(String[] args) {
        Optional optional = Optional.empty();
        System.out.println(optional.get());
        //System.out.println(optional.orElse(new RuntimeException()));
        //System.out.println(optional.orElseGet(() -> new RuntimeException()));
        System.out.println(optional.orElseThrow());
        optional.ifPresentOrElse(System.out::print, () -> {throw new RuntimeException();});
    }
}

class Q94 {
    public static void main(String[] args) {
        Stream<String> stream = Stream.of("d", "cc", "bbb", "aaaa");
        stream.sorted((s1,s2) -> s1.length() - s2.length()).forEach(System.out::println);
        //stream.sorted().forEach(System.out::println);
    }
}