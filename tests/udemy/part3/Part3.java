package udemy.part3;

import java.util.ArrayList;

public class Part3 {
    public static void main(String[] args) {
        Runtime.getRuntime().gc();
        System.gc();
    }
}

class Q4 {
    //static var arr = new Boolean[1];
    public static void main(String[] args) {
        var arr = new Boolean[1];
        //arr[0] = false;
        if(arr[0]) { //java.lang.Boolean.booleanValue() NPE
            System.out.println(true);
        } else {
            System.out.println(false);
        }
    }

}

//
class Q8 {
    public static void main(String [] args) {
        new Q8_().new Inner().printName();
    }
}
class Q8_ {
    private String name = "NOW OR NEVER";
    //Insert inner class definition here
    class Inner {
        //private String name = "NOW";
        public void printName() {
            System.out.println(Q8_.this.name);
            System.out.println(name);
            //System.out.println(this.name);
        }
    }
}
//

class Q9 {
    class A {
        void m() {
            System.out.println("OVER AND OUT");
        }
    }
    public static void main(String [] args) {
        //Insert statement here
        A a1 = new Q9().new A();
        a1.m();

        A a2 = new Q9().new A();
        a2.m();

        var a5 = new Q9().new A();
        a5.m();
    }
}

//
class Q12 {
    Q12() {
        System.out.print(1);
    }
    class B {
        B() {
            System.out.print(2);
        }
    }
}
class Q12_ {
    public static void main(String [] args) {
        //B obj = new Q12().new B();
        Q12.B obj = new Q12().new B();
    }
}

//
class Q13 {
    public static void m1() { //Line n1
        System.out.println("Foo : m1()");
    }
    class Bar {
        public static void m1() { //Line n2
            System.out.println("Bar : m1()");
        }
    }
}
class Q13_ {
    public static void main(String [] args) {
        var foo = new Q13(); //Line n3
        var bar = foo.new Bar(); //Line n4
        bar.m1(); //Line n5
        Q13.Bar.m1();
    }
}
//

class Q15 {
    class Y {
        private void m() {
            System.out.println("INNER");
        }
    }

    public void invokeInner() {
        var obj = new Y(); //Line n1
        obj = this.new Y(); //Line n1
        obj.m(); //Line n2
    }
}
class Q15_ {
    public static void main(String[] args) {
        new Q15().invokeInner();
    }
}
//

class Q17 {
    static { //static initializer block
        System.out.print("A");
    }
    class Bar {
        static { //static initializer block
            System.out.print("B");
        }
    }
}

class Q17_ {
    public static void main(String [] args) {
        new Q17().new Bar();
    }
}
//

class Q19 {
    private int z = 0;
    public void print(int x) {
        class Inner {
            public void getX() {
                System.out.println(x);
                //System.out.println(++x); // mb effectively final
            }
        }
        Inner inner = new Inner();
        inner.getX();
    }
}
class Q19_ {
    public static void main(String[] args) {
        new Q19().print(100);
    }
}
//

class Q21 {
    public static void sayHello() {}
    static {
        class Inner {
            /*INSERT*/
            {
                System.out.println("HELLO");
            }
            static {
                System.out.println("HELLO");
            }
            Inner() {
                System.out.println("HELLO");
            }
            Inner(String s) {
                System.out.println(s);
            }
        }
        new Inner();
        System.out.println("Outer");
    }
}

class Q21_ {
    public static void main(String[] args) {
        Q21 q21 = new Q21();
        Q21.sayHello();
    }
}
//

class Q22 {
    public void print(String name) {
        class B {
            B() {
                System.out.println(name); //Line n1
            }
        }
        B obj = new B();
    }
    //B obj = new B(); //Line n2
    //Q22.B obj = new Q22().new B(); //Line n2
}

class Q22_ {
    public static void main(String[] args) {
        new Q22().print("OCP"); //Line n3
    }
}
//

class Q24 {
    public void printMessage() {
        System.out.println("Hello!");
    }
}

class Q24_ {
    public static void main(String[] args) {
        Q24 msg = new Q24() {
            @Override
            public void printMessage() {
                super.printMessage();
            }
        }; //Line n1
        msg.printMessage(); //Line n2
    }
}
//
class Q25 {
    public void sayHello() {
        System.out.println("Hello!");
    }
}

class Q25_ {
    public static void main(String[] args) {
        Q25 obj = new Q25() {
            public void SayHello() {
                System.out.println("HELLO!");
            }
        };
        obj.sayHello();
    }
}
//
class Q26 {
    public void log() {
        System.out.println("GO FOR IT");
    }
}

class Q26_ {
    public static void main(String[] args) {
        Q26 obj = new Q26() {
            public void Log() {
                System.out.println("LET IT BE");
            }
        };
        obj.log();
        //obj.Log();
    }
}
//
interface Q27_I1{}
interface Q27_I2{}
class Q27 implements Q27_I1, Q27_I2 {
    public void getType() {
        System.out.println("LASER");
    }
}

class Q27_ {
    public static void main(String[] args) {
        //Q27 obj = new Q27() { //Line n1
        var obj = new Q27() { //Line n1
            public void GetType() { //Line n2
                System.out.println("INKJET");
            }
        };
        obj.GetType(); //Line n3
    }
}
//

class Q32 {
    public static void main(String [] args) {
        System.out.println(new Object() {
            public String toString() {
                return "ANONYMOUS";
            }
        });
    }
}

//
enum Q36_ShapeType {
    CIRCLE, SQUARE, RECTANGLE;
}
abstract class Q36_Shape {
    private Q36_ShapeType type = Q36_ShapeType.SQUARE; //default ShapeType
    Q36_Shape(Q36_ShapeType type) {
        this.type = type;
    }
    public Q36_ShapeType getType() {
        return type;
    }
    abstract void draw();
}
class Q36_ {
    public static void main(String[] args) {
        Q36_Shape shape = new Q36_Shape(null) {
            @Override
            void draw() {
                System.out.println("Drawing a " + getType());
            }
        };
        shape.draw();
    }
}
//

class Q37 {
    static class B {

    }
}
class Q37_ {
    /*INSERT*/
    //B obj1 = new B();
    //B obj2 = new Q37.B();
    Q37.B obj1 = new Q37.B();
    //Q37.B obj2 = new Q37().new B();
}
//
class Q38 {
    private static class B {
        private void log() {
            System.out.println("BE THE CHANGE");
        }
    }
    public static void main(String[] args) {
        /*INSERT*/
        B obj1 = new B();
        obj1.log();
        //B obj3 = new Q38.new B();
        //obj3.log();
        B obj2 = new B();
        obj2.log();
        //Q38.B obj4 = new Q38().new B();
        //obj4.log();
        var obj5 = new B();
        obj5.log();
        new B().log();
    }
}
//
class Q39 {
    abstract static class Animal { //Line n1
        abstract void eat();
    }
    static class Dog extends Animal { //Line n2
        void eat() { //Line n3
            System.out.println("DOG EATS BISCUITS");
        }
    }
}
class Q39_ {
    public static void main(String[] args) {
        Q39.Animal animal = new Q39.Dog(); //Line n4
        animal.eat();
    }
}
//
class Q45 {
    private interface I1 {
        void m1();
    }

    public static void main(String[] args) {
        I1 i = new I1() {
            @Override
            public void m1() {
                System.out.println("FINISH");
            }
        };
        i.m1();
    }
}
//
interface Q46 {
    void m1();

    public static interface I2 {
        void m2();
    }

    public abstract static class A1 {
        public abstract void m3();
    }

    public static class A2 {
        public void m4() {
            System.out.println(4);
        }
    }
}
//
class Q57 {
    int calculate(int i1, int i2) {
        return i1 + i2;
    }

    double calculate(byte b1, byte b2) {
        return b1 % b2;
    }
}
class Q57_ {
    public static void main(String[] args) {
        byte b = 100;
        int i = 20;
        System.out.println(new Q57().calculate(b, i));
        System.out.println(new Q57().calculate(b, b));
    }
}

class Q68 {
    private static void m(int i) {
        System.out.print(1);
    }

    private static void m(int i1, int i2) {
        System.out.print(2);
    }

    private static void m(char... args) {
        System.out.print(3);
    }

    public static void main(String... args) {
        m('A');
        m('A', 'B');
        m('A', 'B', 'C');
        m('A', 'B', 'C', 'D');
    }
}

class Q77 {
    static int a = 10000;
    static {
        a = -a--;
    }
    {
        a = -a++;
    }

    public static void main(String[] args) {
        System.out.println(a);
    }
}

class Q78 {
    static int num;
    static int den;
    {
        num = 100;
        den = 10;
    }
    static {
        num = num/den;
    }
}

class Q78_ {
    public static void main(String[] args) {
        System.out.println(Q78.num);
    }
}

class Q79 {
    static Double d1;
    static int x = d1.intValue();

    public static void main(String[] args) {
        System.out.println("HELLO");
    }
}

class Q80 {
    static Character obj;
    char c = obj.charValue();

    public static void main(String[] args) {
        System.out.println("DREAMS COME TRUE");
    }
}

class Q82 {
    Integer i = 10; //Line n1
    {
        Integer i = 2; //Line n2
    }
    public static void main(String[] args) {
        System.out.println(new Q82().i); //Line n3
    }
    { i--; } //Line n4
}

final class Q87{}
abstract class Q87_{}

class Q88 {
    public static void main(String args[]) {
        double area = 5.7;
        String color = null;
        if (area < 7)
            color = "BLUE";

        System.out.println(color);
    }
}
class Q89 {
    public static void main(String[] args) {
        double price = 90000;
        String model;
        if(price > 100000) {
            model = "Tesla Model X";
        } else if(price <= 100000) {
            model = "Tesla Model S";
        } else {
            model = "else";
        }
        System.out.println(model);
    }
}

class Q90 {
    public static void main(String[] args) {
        //short [] args = new short[]{50, 50};
        short [] args1 = new short[]{50, 50};
        args1[0] = 5;
        args1[1] = 10;
        System.out.println("[" + args1[0] + ", " + args1[1] + "]");
    }
}

class Q91 {
    static String msg; //Line n1
    public static void main(String[] args) {
        String msg = null; //Line n2
        if(args.length > 0) {
            msg = args[0]; //Line n3
        }
        System.out.println(msg); //Line n4
    }
}
class Q92 {
    static String str = "KEEP IT "; //Line n1
    public static void main(String[] args) {
        String str = Q92.str + "SIMPLE"; //Line n2
        System.out.println(str);
    }
}
//
class Q93 {
    private int height;
    private int width;

    public Q93(int height, int width) {
        this.height = height;
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }
}

class Q93_ {
    public static void main(String[] args) {
        //private int i = 100;
         int i = 100;
        //private int j = 200;
         int j = 200;
        Q93 rect = new Q93(i, j);
        System.out.println(rect.getHeight() + ", " + rect.getWidth());
    }
}
//
class Q95 {
    static String var = "FRIENDS"; //Line n1
    public static void main(String[] args) {
        int var = (var = Q95.var.length()); //Line n2
        System.out.println(var); //Line n3
    }
}

class Q97 {
    public static void main(String[] args) {
        //var[] arr1 = new String[2];
        //var num;
        //num = 10;
        //var msg = null;
        //private var y = 100;
        //var arr2 = {1, 2, 3};

        var arr2 = new int[]{1, 2, 3};
        final var str = "Hello";
        for (var i = 0; i <2; i++) {
            System.out.println(i);
        }
        String [] arr = {"A", "E", "I", "O", "U"};
        for(var x : arr) {
            System.out.println(x);
        }
    }
}

class Q98 {
    //private var place = "Unknown";  //Line n1
    //public static final var DISTANCE = 200; //Line n2

    public static void main(String[] args) {
        var list1 = new ArrayList<>(); //Line n3

        var list2 = new ArrayList(); //Line n4

        //var lambda1 = () -> System.out.println("Hello"); //Line n5

        var var = 100; //Line n6
    }
}

class Q101 {
    public static void main(String[] args) {
        var m = 10; //Line n1
        var n = 20; //Line n2
        /*INSERT*/
        //byte p = m = n = 10; //Line n3
        //short p = m = n = 10; //Line n3
        float p = m = n = 10; //Line n3
        double p_ = m = n = 10; //Line n3
        var p1 = m = n = 10; //Line n3
        System.out.println(m + n + p1); //Line n4
        int p2 = m = n = 20; //Line n3
        System.out.println(m + n + p2); //Line n4
        long p3 = m = n = 30; //Line n3
        System.out.println(m + n + p3); //Line n4
    }
}