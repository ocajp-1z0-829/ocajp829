package udemy.part11;

import java.util.ListResourceBundle;

public class Q72MyResourceBundle_fr extends ListResourceBundle {
    @Override
    protected Object[][] getContents() {
        Object [][] arr = {{"surprise", 1001}};
        return arr;
    }
}