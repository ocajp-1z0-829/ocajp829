package udemy.part11;

import java.io.FileInputStream;
import java.io.IOException;
import java.math.RoundingMode;
import java.sql.*;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.FormatStyle;
import java.time.format.TextStyle;
import java.util.*;
import java.util.stream.Stream;

public class Part11 {
}

class Q1 {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        var query = "Select * FROM EMPLOYEE";
        try (
                var con = DriverManager.getConnection("jdbc:postgresql://localhost/testdb", "sa", "sa");
             var stmt = con.createStatement();
             var rs = stmt.executeQuery(query);) {
            System.out.println(rs.getMetaData().getColumnCount()); //Line 11
        }
    }
}

class Q2 {
    public static void main(String[] args) {
        try {
            Class.forName("org.postgresql.Driver");
            var con = DriverManager.getConnection("jdbc:postgresql://localhost/testdb", "sa", "sa");
            var query = "Select * FROM EMPLOYEE";
            var stmt = con.createStatement();
            var rs = stmt.executeQuery(query);
            while (rs.next()) {
                System.out.println("ID: " + rs.getInt("IDD"));
                System.out.println("First Name: " + rs.getString("FIRSTNAME"));
                System.out.println("Last Name: " + rs.getString("LASTNAME"));
                System.out.println("Salary: " + rs.getDouble("SALARY"));
            }
            rs.close();
            stmt.close();
            con.close();
        } catch (SQLException ex) {
            System.out.println("An Error Occurred!");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}

class Q3 {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        Connection connection = null;
        Statement stmt = null;
        Class.forName("org.postgresql.Driver");
        try (var con = DriverManager.getConnection("jdbc:postgresql://localhost/testdb", "sa", "sa");)
        {
            connection = con;
            stmt = connection.createStatement();
            con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            stmt.executeUpdate("INSERT INTO EMPLOYEE VALUES(101, 'John', 'Smith', 12000)");
        }
          stmt.close();
        connection.close();
    }
}

class Q8 {
    public static void main(String[] args) throws ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        var url = "jdbc:postgresql://localhost/testdb";
        var user = "sa";
        var password = "sa";
        var query = "Select ID, FIRSTNAME, LASTNAME, SALARY FROM EMPLOYEE WHERE SALARY > 14900 ORDER BY ID";

        try (var con = DriverManager.getConnection(url, user, password);
             var stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
             var rs = stmt.executeQuery(query);) {
            rs.absolute(2);
            rs.updateDouble("SALARY", 20000);
            rs.updateRow();
        } catch (SQLException ex) {
            System.out.println("ERROR");
        }
    }
}

class Q10 {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        var url = "jdbc:postgresql://localhost/testdb";
        var user = "sa";
        var password = "sa";
        var query = "Select ID, FIRSTNAME, LASTNAME, SALARY FROM EMPLOYEE ORDER BY ID";

        try (var con = DriverManager.getConnection(url, user, password);
             var stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
             var rs = stmt.executeQuery(query);) {
            rs.absolute(-2);
            rs.relative(-2);
            System.out.println(rs.getInt(1));
        }
    }
}

/**
 * `con.createStatement();` would return the Statement object of type TYPE_FORWARD_ONLY and have a concurrency level of CONCUR_READ_ONLY. Hence, by default ResultSet is not updatable.
 *
 * `rs.deleteRow();` throws an exception at runtime.
 *
 * To update the ResultSet in any manner (insert, update or delete), the ResultSet must come from a Statement that was created with a ResultSet type of ResultSet.CONCUR_UPDATABLE.
 *
 * NOTE: If you want to successfully delete the row, then replace `con.createStatement();` with `con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);`
 * OR
 * `con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);`
 *
 * If you use, `con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);`, then
 * `rs.absolute(3);` and `rs.relative(-1);` would throw an exception at runtime.
 */
class Q11 {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        var url = "jdbc:postgresql://localhost/testdb";
        var user = "sa";
        var password = "sa";
        var query = "Select ID, FIRSTNAME, LASTNAME, SALARY FROM EMPLOYEE ORDER BY ID";

        try (var con = DriverManager.getConnection(url, user, password);
             //var stmt = con.createStatement();
             //var stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
             var stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
             var rs = stmt.executeQuery(query);) {
            rs.absolute(3);
            rs.relative(-1);
            rs.deleteRow();
        }
    }
}

class Q12 {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        var url = "jdbc:postgresql://localhost/testdb";
        var user = "sa";
        var password = "sa";
        var query = "Select ID, FIRSTNAME, LASTNAME, SALARY FROM EMPLOYEE ORDER BY ID";

        try (var con = DriverManager.getConnection(url, user, password);
             var stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
             var rs = stmt.executeQuery(query);) {
            rs.moveToInsertRow();
            rs.updateInt(1, 107);
            rs.updateString(2, "Smita");
            rs.updateString(3, "Jain");
            rs.updateObject(4, 16000);
            rs.insertRow();
        }
    }
}

class Q14 {
    public static void main(String[] args) throws Exception {
        Class.forName("org.postgresql.Driver");
        var url = "jdbc:postgresql://localhost/testdb";
        var user = "sa";
        var password = "sa";
        var query = "Select ID, FIRSTNAME, LASTNAME, SALARY FROM EMPLOYEE ORDER BY ID";
        try (var con = DriverManager.getConnection(url, user, password);
             var stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);
        ) {
            var rs = stmt.executeQuery(query);
            rs.moveToInsertRow();
            rs.updateInt(1, 105);
            rs.updateString(2, "Chris");
            rs.updateString(3, "Lee");
            rs.updateDouble(4, 16000);
            rs.updateNull(1);
            rs.refreshRow(); //Line n1
            rs.insertRow(); //Line n2
            rs.last();
            System.out.println(rs.getInt(1)); //Line n3
        }
    }
}

class Q15 {
    public static void main(String[] args) throws Exception {
        Class.forName("org.postgresql.Driver");
        var url = "jdbc:postgresql://localhost/testdb";
        var user = "sa";
        var password = "sa";
        var query = "Select ID, FIRSTNAME, LASTNAME, SALARY FROM EMPLOYEE ORDER BY ID";
        try (var con = DriverManager.getConnection(url, user, password);
             var stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ) {
            var rs = stmt.executeQuery(query);
            rs.absolute(1);
            rs.moveToInsertRow();
            rs.updateInt(1, 105);
            rs.updateString(2, "Chris");
            rs.updateString(3, "Morris");
            rs.updateDouble(4, 25000);
            rs.deleteRow();
            rs.refreshRow();
            System.out.println(rs.getInt(1));
        }
    }
}

class Q20 {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        var url = "jdbc:postgresql://localhost/testdb";
        var prop = new Properties();
        //prop.put("username", "sa");
        prop.put("user", "sa");
        prop.put("password", "sa");
        var query = "Select ID, FIRSTNAME, LASTNAME, SALARY FROM EMPLOYEE ORDER BY ID";

        try (var con = DriverManager.getConnection(url, prop);
             var stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
             var rs = stmt.executeQuery(query);) {
            rs.absolute(1);
            rs.absolute(1);
            rs.relative(2);
            System.out.println(rs.getString(2));
        }
    }
}

class Q24 {
    public static void main(String[] args) throws SQLException {
        var url = "jdbc:mysql://localhost:3306/ocp";
        var user = "root";
        var password = "password";
        var query = "Select msg1 as msg, msg2 as msg FROM MESSAGES";
        try (var con = DriverManager.getConnection(url, user, password);
             var stmt = con.createStatement();
             var stmt1 = con.prepareStatement("", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
             var rs = stmt.executeQuery(query);) {
            rs.absolute(1);
            System.out.println(rs.getString("msg"));
            System.out.println(rs.getString("msg"));
        }
    }
}


class Q29 {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        var url = "jdbc:postgresql://localhost/testdb";
        var query = "DELETE FROM MESSAGES";
        try (var con = DriverManager.getConnection(url, "sa", "sa");
             var stmt = con.createStatement())
        {
            System.out.println(stmt.executeUpdate(query));
            System.out.println(stmt.execute(query));
            //System.out.println(stmt.executeQuery(query));
            System.out.println(stmt.getUpdateCount());
        }
    }
}

class Q30 {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        var url = "jdbc:postgresql://localhost/testdb";
        var user = "sa";
        var password = "sa";
        var query = "Select * from EMPLOYEE";
        Connection con = DriverManager.getConnection(url, user, password);
        try (var stmt = con.createStatement())
        {
            var rs = stmt.executeQuery(query);
            while (rs.next()) {
                System.out.println(rs.getString(2));
            }
        }
    }
}

class Q34 {
    public static void main(String[] args) throws Exception {
        Class.forName("org.postgresql.Driver");
        var url = "jdbc:postgresql://localhost/testdb";
        var user = "sa";
        var password = "sa";
        var query = "Select ID, FIRSTNAME, LASTNAME, SALARY FROM EMPLOYEE";
        try (var con = DriverManager.getConnection(url, user, password);
             var stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ) {
            var rs = stmt.executeQuery(query);
            rs.afterLast();
            while (rs.previous()) {
                rs.updateDouble(4, rs.getDouble(4) + 1000);
                rs.updateRow();
            }

            rs = stmt.executeQuery(query);
            while(rs.next()) {
                System.out.println(rs.getDouble(4));
            }
        }
    }
}

class Q36 {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        var url = "jdbc:postgresql://localhost/testdb";
        var user = "sa";
        var password = "sa";
        try (var con = DriverManager.getConnection(url, user, password);
             var stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ) {
            stmt.executeUpdate("INSERT INTO LOG VALUES(1001, 'Login Successful')");
            stmt.executeUpdate("INSERT INTO LOG VALUES(1002, 'Login Failure')");

            con.setAutoCommit(false);

            stmt.executeUpdate("INSERT INTO LOG VALUES(1003, 'Not Authorized')");
        }
    }
}

class Q37 {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        var url = "jdbc:postgresql://localhost/testdb";
        var user = "sa";
        var password = "sa";
        var query = "Select ID, FIRSTNAME, LASTNAME, SALARY FROM EMPLOYEE ORDER BY ID";
        try (var con = DriverManager.getConnection(url, user, password);
             var stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        ) {
            var rs = stmt.executeQuery(query);
            rs.relative(3);
            rs.relative(-3);
            rs.relative(1);
            System.out.println(rs.getInt(1));
        }
    }
}

class Q38 {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        var url = "jdbc:postgresql://localhost/testdb";
        var user = "sa";
        var password = "sa";
        try (var con = DriverManager.getConnection(url, user, password);
             var stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        ) {
            var res1 = stmt.executeQuery("SELECT * FROM EMPLOYEE ORDER BY ID");
            var res2 = stmt.executeQuery("SELECT * FROM EMPLOYEE ORDER BY ID DESC");
            res1.next();
            System.out.println(res1.getInt(1));
            res2.next();
            System.out.println(res2.getInt(1));
        }
    }
}

class Q40 {
    public static void main(String[] args) throws Exception {
        Class.forName("org.postgresql.Driver");
        var url = "jdbc:postgresql://localhost/testdb";
        var user = "sa";
        var password = "sa";
        try (var con = DriverManager.getConnection(url, user, password);
             var ps = con.prepareStatement("INSERT INTO LOG VALUES(?, ?)");
             var ps2 = con.prepareStatement("SELECT ID FROM LOG");
        ) {
            ps.setInt(1, 1001);
            ps.setObject(1, 1002);
            ps.setObject(1, 1003, Types.INTEGER);
            ps.setString(2, "SUCCESS");
            ps.executeUpdate();

            var rs = ps2.executeQuery();
            while(rs.next()) {
                System.out.println(rs.getInt(1));
            }
        }
    }
}

class Q42 {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        var url = "jdbc:postgresql://localhost/testdb";
        var user = "sa";
        var password = "sa";
        var list = new ArrayList<Integer>();

        try (var con = DriverManager.getConnection(url, user, password);
             var ps = con.prepareStatement("Select ID FROM LOG");) {
            try (var rs = ps.executeQuery();) {
                while(rs.next())
                    /*INSERT*/
                    list.add(rs.getInt(1));
                    //list.add(rs.getInt("ID"));
                    //list.add(rs.getObject(1, Integer.class));
                    //list.add(rs.getObject(1));
                    //list.add(rs.getObject("ID"));
            }
        }
        System.out.println(list.stream().mapToInt(x -> x).sum());
    }
}

class Q47 {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        var url = "jdbc:postgresql://localhost/testdb";
        var user = "sa";
        var password = "sa";
        var sql = "{call getEmployeeEarningMoreThan(?,?)}";

        try (var con = DriverManager.getConnection(url, user, password);
             var cs = con.prepareCall(sql);) {
            cs.setDouble(1, 15000);
            /*INSERT*/
            cs.registerOutParameter(2, Types.INTEGER);
            cs.execute();
            System.out.println(cs.getInt(2));
        }
    }
}

class Q50 {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        var url = "jdbc:postgresql://localhost/testdb";
        var user = "sa";
        var password = "sa";
        try (var con = DriverManager.getConnection(url, user, password);
             var stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ) {
            stmt.executeUpdate("INSERT INTO LOG VALUES(1001, 'Login Successful')"); //Line n1
            stmt.executeUpdate("INSERT INTO LOG VALUES(1002, 'Login Failure')"); //Line n2

            con.commit(); //Line n3

            stmt.executeUpdate("INSERT INTO LOG VALUES(1003, 'Not Authorized')"); //Line n4
        }
    }
}

class Q53 {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        try (var con = DriverManager.getConnection("jdbc:postgresql://localhost/testdb", "sa", "sa");
             var stmt = con.createStatement();){
            con.setAutoCommit(false);

            stmt.executeUpdate("""
                    INSERT INTO EMPLOYEE
                    VALUES(101, 'John', 'Smith', 12000)
                    """);

            var sp101 = con.setSavepoint();

            stmt.executeUpdate("""
                    INSERT INTO EMPLOYEE
                    VALUES(102, 'Sean', 'Smith', 15000)
                    """);

            con.rollback(sp101);

            con.commit();

        }
    }
}

class Q54 {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        try (var con = DriverManager.getConnection("jdbc:postgresql://localhost/testdb", "sa", "sa");
             var stmt = con.createStatement();){
            con.setAutoCommit(false);

            stmt.executeUpdate("""
                    INSERT INTO EMPLOYEE
                    VALUES(101, 'John', 'Smith', 12000)
                    """);
            var sp101 = con.setSavepoint("101");

            stmt.executeUpdate("""
                    INSERT INTO EMPLOYEE
                    VALUES(102, 'Sean', 'Smith', 15000)
                    """);
            var sp102 = con.setSavepoint("102");
            con.rollback(sp101);

            stmt.executeUpdate("""
                    INSERT INTO EMPLOYEE
                    VALUES(103, 'Regina', 'Williams', 15500)
                    """);
            var sp103 = con.setSavepoint("103");
            con.commit();
        }
    }
}

class Q57 {
    public static void main(String[] args) {
        var locale = new Locale("temp", "UNKNOWN"); //Line 7
        System.out.println(locale.getLanguage() + ":" + locale.getCountry()); //Line 8
        System.out.println(locale); //Line 9
    }
}

class Q58 {
    public static void main(String[] args) {
        System.out.println(Locale.getDefault());
    }
}

class Q59 {
    public static void main(String[] args) {
        Locale l1 = Locale.US;
        Locale l5 = new Locale("en", "US");
        System.out.println(l1);
        System.out.println(l5);
    }
}

class Q60 {
    public static void main(String[] args) {
        var l1 = new Locale.Builder().setLanguage("en").setRegion("US").build();
        var l2 = Locale.US;
        var l3 = new Locale("en");

        System.out.println(l1.equals(l2));
        System.out.println(l2.equals(l3));
    }
}

class Q61 {
    public static void main(String[] args) {
        Locale [] loc = Locale.getAvailableLocales();

        Object [] locale = Locale.getAvailableLocales();

        System.out.println(Arrays.toString(loc));

    }
}

class Q64 {
    public static void main(String[] args) {
        var loc = new Locale("it", "IT");
        System.out.println(loc.getDisplayCountry()); //Line 8
        System.out.println(loc.getDisplayCountry(new Locale("ru", "RU"))); //Line 9
        System.out.println(loc.getDisplayLanguage()); //Line 10
        System.out.println(loc.getDisplayLanguage(Locale.CHINA)); //Line 11
    }
}

class Q65 {
    public static void main(String[] args) {
        var loc = Locale.ENGLISH;
        System.out.println(loc.getDisplayCountry());
        System.out.println(loc.getDisplayLanguage());
    }
}

class Q66 {
    public static void main(String[] args) {
        var loc = Locale.ENGLISH;
        System.out.println(new Locale(null));

        System.out.println(loc.getDisplayCountry());
    }
}

class Q69 {
    public static void main(String[] args) throws IOException {
        Properties prop = new Properties ();
        FileInputStream fis = new FileInputStream ("tests/udemy/part11/Message.properties");
        prop.load(fis);
        System.out.println(prop.getProperty("key1"));
        System.out.println(prop.getProperty("key2", "Good Day!"));
        System.out.println(prop.getProperty("key3", "Good Day!"));
        System.out.println(prop.getProperty("key4"));
    }
}

class Q71 {
    public static void main(String[] args) throws IOException {
        var prop = new Properties ();
        var fis = new FileInputStream ("C:\\Message.properties");
        prop.load(fis);
        System.out.println(prop.get("key1")); //Line n1
        //System.out.println(prop.get("key2", "Good Day!")); //Line n2
        System.out.println(prop.getProperty("key3", "Good Day!")); //Line n3
        System.out.println(prop.getProperty("key4")); //Line n4
    }
}

//

class Q72 {
    public static void main(String[] args) throws ClassNotFoundException {
        Class.forName("udemy.part11.Q72MyResourceBundle");
        Locale.setDefault(new Locale("fr", "IT"));
        var loc = new Locale("en", "US");
        var rb = ResourceBundle.getBundle("udemy.part11.Q72MyResourceBundle", loc);
        System.out.println(rb.getObject("surprise"));
    }
}

class Q79 {
    public static void main(String[] args) {
        var loc = new Locale("en", "US");
        var bundle = ResourceBundle.getBundle("udemy.part11.RB", loc);
        var enumeration = bundle.getKeys();
        while (enumeration.hasMoreElements()) {
            var key = enumeration.nextElement();
            var val = bundle.getString(key);
            System.out.println(key + "=" + val);
        }
    }
}

class Q81 {
    public static void main(String[] args) {
        var loc = new Locale("en", "US");
        var nf = NumberFormat.getCurrencyInstance(loc);
        System.out.printf("Amount %s is in %s" , nf.format(10), nf.getCurrency());
        System.out.println();
        System.out.println(nf.getRoundingMode());
    }
}

class Q82 {
    public static void main(String[] args) {
        System.out.println(NumberFormat.getCurrencyInstance().format(5));
        System.out.println(NumberFormat.getCurrencyInstance(java.util.Locale.US).format(5));
    }
}

class Q83 {
    public static void main(String [] args) {
        var nf = NumberFormat.getIntegerInstance();
        System.out.println(Stream.of(nf.format(10.50), nf.format(11.50), nf.format(10.51))
                .mapToInt(x -> Integer.parseInt(x))
                .sum());
        System.out.println(Math.round(10.49));
        System.out.println(Math.round(10.50));
        System.out.println(Math.round(11.50));
        System.out.println(Math.round(10.51));
        System.out.println();
        System.out.println(nf.format(10.50));
        System.out.println(nf.format(11.50));
        System.out.println(nf.format(10.51));
        System.out.println(nf.format(10.49));
    }
}

class Q85 {
    public static void main(String [] args) throws ParseException {
        var nf = NumberFormat.getIntegerInstance();
        Stream.of(nf.parse("10.49"), nf.parse("10.50"), nf.parse("10.51"))
                .map(x -> "" + x)
                .forEachOrdered(System.out::print);
    }
}

class Q87 {
    public static void main(String [] args)  {
        var df1 = new DecimalFormat("###,##,#");
        var df2 = new DecimalFormat("##,###,#");
        System.out.println(df1.format(1231));
        System.out.println(df2.format(1231));
        System.out.println(df1.format(1231).equals(df2.format(1231)));
    }
}

class Q88 {
    public static void main(String [] args)  {
        var df1 = new DecimalFormat("###,##,#");
        var df2 = new DecimalFormat("000,00,0");

        /*INSERT*/
        System.out.println(df1.format(87654).equals(df2.format(87654)));
        System.out.println(df1.format(87654));
        System.out.println(df2.format(87654));
        System.out.println(df1.format(1).equals(df2.format(1)));
        System.out.println(df1.format(1234567).equals(df2.format(1234567)));
        System.out.println(df1.format(123456).equals(df2.format(123456)));
    }
}

class Q89 {
public static void main(String [] args) {
    Locale.setDefault(Locale.US);
    var nf = NumberFormat.getCompactNumberInstance();
    System.out.println(nf.format(592_92));
    nf.setMaximumFractionDigits(2);
    System.out.println(nf.format(592_92));
}
}

class Q90 {
    public static void main(String [] args) {
        Locale.setDefault(Locale.US);
        var nf = NumberFormat.getCompactNumberInstance();
        System.out.println(nf.format(5698));
    }
}

class Q91 {
    public static void main(String [] args) {
        var date = LocalDate.of(1987, 9, 1);
        var str = date.format(DateTimeFormatter.ISO_DATE_TIME);
        System.out.println("Date is: " + str);
    }
}

class Q92 {
    public static void main(String [] args) {
        var date = LocalDate.of(2022, 11, 4);
        var formatter = DateTimeFormatter.ofPattern("dd-MM-uuuu");
        System.out.println(formatter.format(date));
        System.out.println(formatter.format(date).equals(date.format(formatter)));
    }
}

class Q93 {
    public static void main(String [] args) {
        var valDay = LocalDate.of(2022, 2, 14);
        var formatter = DateTimeFormatter.ofPattern("DD-MM-uuuu");
        System.out.println(valDay.format(formatter));
    }
}

class Q94 {
    public static void main(String [] args) {
        var date = LocalDate.of(2021, 11, 4);
        var formatter = DateTimeFormatter.ofPattern("D-MM-uuuu");
        System.out.println(formatter.format(date));
    }
}

class Q95 {
    public static void main(String [] args) {
        var date = LocalDate.of(2023, Month.FEBRUARY, 1);
        var formatter = DateTimeFormatter.ofPattern("DD'nd day of' uuuu");
        System.out.println(formatter.format(date));
    }
}

class Q96 {
    public static void main(String [] args) {
        var date = LocalDateTime.of(2025, Month.JANUARY, 1, 10, 10);
        var formatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL);
        System.out.println(formatter.format(date));
    }
}

class Q97 {
public static void main(String [] args) {
    Locale.setDefault(new Locale("en", "US"));
    var date = LocalDate.parse("2022-09-10");
    System.out.println(date.format(DateTimeFormatter.ofPattern("dd-MMM-yyyy")));
}
}

class Q98 {
    public static void main(String [] args) {
        var date = LocalDate.now();
        var time = LocalTime.now();
        var dateTime = LocalDateTime.of(date, time);
        var zonedDateTime = ZonedDateTime.now();

        /*INSERT*/
        System.out.println(zonedDateTime.format(DateTimeFormatter.ofPattern("'It''s' dd-mm-yyyy")));

    }
}

class Q99 {
    public static void main(String[] args) throws ParseException {
        var dateFormatter = DateTimeFormatter.ofPattern("dd-MM-uuuu"); //Line n1
        System.out.println(dateFormatter.parse("10-5-2019")); //Line n2
        var currFormatter = NumberFormat.getCurrencyInstance(Locale.US); //Line n3
        System.out.println(currFormatter.parse("$7.00")); //Line n4
    }
}

class Q105 {
    public static void main(String [] args) {
        Locale.setDefault(Locale.US);
        var formatter = new DateTimeFormatterBuilder()
                .appendDayPeriodText(TextStyle.FULL)
                .toFormatter();
        System.out.println(LocalDateTime.now().format(formatter));
        System.out.println(LocalDateTime.now());
        System.out.println(formatter.format(LocalTime.of(17, 59, 0)));
    }
}
class Q108 {
    public static void main(String [] args)  {
        var str = "\"{0} x {0} x {0}\" = {1}";
        System.out.println(MessageFormat.format(str, 3, 3*3*3));
    }
}

class Q110 {
    public static void main(String [] args)  {
        var mf = new MessageFormat("'{0}' + '{1}' = '{2}'");
        System.out.println(mf.format(new Integer[] {
                2,3,5,8
        }));
    }
}