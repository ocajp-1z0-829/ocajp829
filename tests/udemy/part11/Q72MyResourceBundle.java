package udemy.part11;

import java.util.ListResourceBundle;

public class Q72MyResourceBundle extends ListResourceBundle {
    @Override
    protected Object[][] getContents() {
        Object [][] arr = {{"surprise", "SURPRISE!"}};
        return arr;
    }
}