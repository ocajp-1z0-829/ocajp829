package udemy.part11;

import java.util.ListResourceBundle;

public class Q72MyResourceBundle_en_CA extends ListResourceBundle {
    @Override
    protected Object[][] getContents() {
        Object [][] arr = {{"surprise", 12.64}};
        return arr;
    }
}