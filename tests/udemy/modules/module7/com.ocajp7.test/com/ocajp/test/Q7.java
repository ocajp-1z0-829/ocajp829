package com.ocajp.test;

/**
 * "C:\JAVA_UTILS\openjdk-17.0.1\bin\javac.exe" -d classes --module-source-path module7 --module com.ocajp7.test
 * "C:\JAVA_UTILS\openjdk-17.0.1\bin\java.exe" -p classes --module com.ocajp7.test/com.ocajp.test.Q7
 *
 */
public class Q7 {
    public static void main(String... args) {
	var module = "MODULE"; //Line n1
	var requires = "REQUIRES"; //Line n2
	var exports = "EXPORTS"; //Line n3

	System.out.println(String.join(":", module, requires, exports)); //Line n4 MODULE:REQUIRES:EXPORTS
    }
}
