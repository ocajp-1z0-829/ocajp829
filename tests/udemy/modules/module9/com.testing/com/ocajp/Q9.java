package com.ocajp;

import java.io.IOException;
import java.sql.SQLException;

/**
 * "C:\JAVA_UTILS\openjdk-17.0.1\bin\javac.exe" -d classes --module-source-path module7 --module com.ocajp7.test
 * "C:\JAVA_UTILS\openjdk-17.0.1\bin\java.exe" -p classes --module com.ocajp7.test/com.ocajp.test.Q7
 *
 */
public class Q9 {
    public static void main(String[] args) {
	try {
	    save();
	    log();
	} catch(IOException | SQLException ex) {}
    }

    private static void save() throws IOException {}

    private static void log() throws SQLException {}
}
