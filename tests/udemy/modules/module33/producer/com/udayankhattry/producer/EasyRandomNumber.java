package com.udayankhattry.producer;

import com.udayankhattry.service.RandomNumber;

public class EasyRandomNumber implements RandomNumber {
    public int getNumber() {
	return new java.util.Random().ints().findFirst().getAsInt();
    }
}