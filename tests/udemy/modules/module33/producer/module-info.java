//"C:\JAVA_UTILS\openjdk-17.0.1\bin\javac.exe" -d classes --module-source-path module33 -m service,consumer
//"C:\JAVA_UTILS\openjdk-17.0.1\bin\java.exe" -p classes -m consumer/com.udayankhattry.service.test.TestService
//"C:\JAVA_UTILS\openjdk-17.0.1\bin\javac.exe" -d classes --module-source-path module33 -m producer
import com.udayankhattry.service.RandomNumber;
import com.udayankhattry.producer.EasyRandomNumber;

module producer {
    requires service;
    provides RandomNumber with EasyRandomNumber;
}