package com.udayankhattry.service.test;

import java.util.ServiceLoader;
import com.udayankhattry.service.RandomNumber;

public class TestService {
    public static void main(String[] args) {
	ServiceLoader<RandomNumber> sl = ServiceLoader.load(RandomNumber.class);
	sl.findFirst().ifPresent(x -> System.out.println(x.getNumber()));
    }
}