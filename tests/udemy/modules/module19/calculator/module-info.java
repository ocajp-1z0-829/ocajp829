//"C:\JAVA_UTILS\openjdk-17.0.1\bin\javac.exe" -d out/calc-module module19/calculator/com/udayankhattry/ocp/Calculator.java module19/calculator/module-info.java
//javac -d out --module-source-path src --module calculator
//javac -d out --module-source-path src --module calc
//javac -d out --module-source-path src --module calc-module
//Please note: Even for single-module, it is always a good practice to keep the module name and module directory name same, so that to compile the code multi-module command can be used.
//"C:\JAVA_UTILS\openjdk-17.0.1\bin\java.exe" --module-path out --module calc/com.udayankhattry.ocp.Calculator
module calc {
}