package com.udayankhattry.ocp;

public class Calculator {
    public static int add(int n1, int n2) {
	return n1 + n2;
    }

    public static void main(String[] args) {
	System.out.println(Calculator.add(20, 40));
    }
}