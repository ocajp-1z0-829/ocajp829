package com.udayankhattry.ocp.test;

public class TestStringUtil {
    public static void main(String[] args) throws Exception {
	//Calling private StringUtil.log() method through reflection
	var c = Class.forName("com.udayankhattry.ocp.StringUtil");
	var m = c.getDeclaredMethod("log");
	m.setAccessible(true);
	m.invoke(c.getDeclaredConstructor().newInstance());
    }
}