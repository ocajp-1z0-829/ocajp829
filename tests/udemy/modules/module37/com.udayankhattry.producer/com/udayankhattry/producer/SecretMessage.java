package com.udayankhattry.producer;

import com.udayankhattry.service.MessageService;

public class SecretMessage implements MessageService {
    public void message() {
	System.out.println("MEETING AT 9PM TODAY");
    }

    public static MessageService provider() {
	return () -> System.out.println("MEETING AT 9AM TOMORROW");
    }
}