//"C:\JAVA_UTILS\openjdk-17.0.1\bin\javac.exe" -d classes --module-source-path module37 -m com.udayankhattry.service,com.udayankhattry.consumer,com.udayankhattry.producer
//"C:\JAVA_UTILS\openjdk-17.0.1\bin\java.exe" -p classes -m com.udayankhattry.consumer/com.udayankhattry.consumer.test.TestService
module com.udayankhattry.producer {
    requires com.udayankhattry.service;
    provides com.udayankhattry.service.MessageService with com.udayankhattry.producer.SecretMessage;
}