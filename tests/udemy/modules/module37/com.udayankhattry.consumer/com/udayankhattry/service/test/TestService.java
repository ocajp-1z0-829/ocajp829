package com.udayankhattry.consumer.test;

import java.util.ServiceLoader;
import com.udayankhattry.service.MessageService;

public class TestService {
    public static void main(String[] args) {
	ServiceLoader<MessageService> sl = ServiceLoader.load(MessageService.class);
	sl.findFirst().ifPresent(m -> m.message());
    }
}