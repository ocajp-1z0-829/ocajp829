open module A {
    requires X;
    requires transitive Y;
    requires static Z; //compile time dependency
}