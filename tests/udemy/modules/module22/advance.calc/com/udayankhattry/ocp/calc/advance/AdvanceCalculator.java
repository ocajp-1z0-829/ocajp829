package com.udayankhattry.ocp.calc.advance;

public class AdvanceCalculator {
    public static int multiply(int... nums) {
	int res = 1;
	for(int i: nums) {
	    res *= i;
	}
	return res;
    }
}