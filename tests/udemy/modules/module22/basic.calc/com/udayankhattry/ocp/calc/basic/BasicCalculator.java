package com.udayankhattry.ocp.calc.basic;

import com.udayankhattry.ocp.calc.advance.AdvanceCalculator;

public class BasicCalculator {
    public static void multiply(int i1, int i2, int i3) {
	System.out.println(AdvanceCalculator.multiply(i1, i2, i3));
    }
}