package com.circus;

public abstract class Animal {
    public abstract void eat();
}