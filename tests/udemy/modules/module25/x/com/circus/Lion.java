package com.circus;

public class Lion extends Animal {
    @Override
    public void eat() {
	System.out.println("Lion eats meat");
    }
}