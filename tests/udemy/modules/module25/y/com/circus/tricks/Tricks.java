package com.circus.tricks;

import com.circus.*;

public class Tricks {
    public static void main(String... args) {
	doTricks(new Lion());
    }

    private static void doTricks(Animal animal) {
	animal.eat();
    }
}