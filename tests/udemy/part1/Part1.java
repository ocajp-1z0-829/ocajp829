package udemy.part1;

import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.Date;

public class Part1 {
    public static void main(String[] args) {
        short s1 = 10;
        //short s2 = 32768;
        final int i3 = 10;
        short s3 = i3;
        byte b3 = i3;
        int i4 = 10;
        //short s4 = i4 * 100;

        String a = null;
        System.out.println(a + null + a);
        System.out.println(a + a + a);
        System.out.println(null + a + a);

        var val = 9;
        System.out.println(val += 10 - -val-- - --val);

        LocalDate date = LocalDate.of(2024, 9, 06);
        System.out.println(date.getMonthValue());
        System.out.println(date.getMonth());
        System.out.println(date);

        LocalDate joiningDate = LocalDate.parse("2006-03-16");
        System.out.println(joiningDate.withDayOfYear(29));

        Period period = Period.of(0, 1000, 0);
        System.out.println(period);

        LocalDateTime obj = LocalDateTime.now();
        System.out.println(obj.getSecond());

        LocalDate d1 = LocalDate.parse("1999-09-09");
        LocalDate d2 = LocalDate.parse("1999-09-09");
        LocalDate d3 = LocalDate.of(1999, 9, 9);
        LocalDate d4 = LocalDate.of(1999, 9, 9);
        System.out.println((d1 == d2) + ":" + (d2 == d3) + ":" + (d3 == d4));

        LocalDate date1 = LocalDate.parse("1947-08-14");
        LocalTime time = LocalTime.MAX;
        System.out.println(date1.atTime(time));

        LocalDate date2 = LocalDate.parse("2000-01-01");
        Period period2 = Period.ofYears(-3000);
        System.out.println(date2.plus(period2));

        LocalDate date3 = LocalDate.of(2000, Month.JANUARY, 1);
        Period period3 = Period.parse("p-30000y");
        System.out.println(date3.plus(period3));

        LocalDate date4 = LocalDate.ofYearDay(2022, 32);
        System.out.println(date4);

        LocalDateTime dt = LocalDateTime.parse("2018-03-16t10:15:30.22");
        System.out.println(dt.toLocalDate() + " " + dt.toLocalTime());

        Period period5 = Period.ofWeeks(100);
        System.out.println(period5);

        LocalTime t1 = LocalTime.now();
        LocalDateTime t2 = LocalDateTime.now();
        System.out.println(Duration.between(t1, t2));

        LocalDate date5 = LocalDate.of(2019, 1, 1);
        LocalTime time5 = LocalTime.of(0, 0);
        ZoneId india = ZoneId.of("Asia/Kolkata");
        ZonedDateTime zIndia = ZonedDateTime.of(date5, time5, india);
        System.out.println(zIndia);

        ZoneId us = ZoneId.of("America/Los_Angeles");
        System.out.println(zIndia.withZoneSameLocal(us));
        System.out.println(zIndia.withZoneSameInstant(us));
    }
}

class Q6 {
    public static void main(String[] args) {
        short s1 = 10;

        final int i3 = 10;
        short s3 = i3;

        final int i5 = 10;
        short s5 = i5 + 100;

        int i7 = 10;
        //short s7 = i7;
    }
}

class Q9 {

    private static void add(double d1, double d2) {
        System.out.println("double version: " + (d1 + d2));
    }

    private static void add(Double d1, Double d2) {
        System.out.println("Double version: " + (d1 + d2));
    }

    public static void main(String[] args) {
        add(10.0, null);
    }
}

class Q11 {
    public static void main(String[] args) {
        extractInt(2.7);
        //extractInt(2); //Required type: Double
    }

    private static void extractInt(Double obj) {
        System.out.println(obj.intValue());
    }
}

class Q13 {
    public static void main(String[] args) {
        System.out.println(Math.floor(1.9));
        System.out.println(Math.round(10.30));
        System.out.println(Math.round(10.50 ));
        System.out.println(Math.round(-10.50 ));
    }
}

class Q14 {
    public static void main(String[] args) {
        var x = Double.valueOf(Math.random() * 10).intValue();
        System.out.println(x);
    }
}

class Q15 {
    public static void main(String[] args) {
        System.out.println(Math.pow(2, -2)); // 1/4
        System.out.println(700 * Math.pow(2, -2));
        System.out.println(700 * (1 / 4));
        System.out.println(700 * (1 / (double)4));
        System.out.println(7 * Math.ceil(24.80));
    }
}

class Q16 {
    public static void main(String[] args) {
        System.out.println(Math.pow(10, Math.ceil(0.50)));
        System.out.println(Math.pow(10, Math.abs(0.50)));
        System.out.println(Math.pow(10, Math.floor(0.50)));
        System.out.println(Math.pow(10, Math.round(0.50)));
        System.out.println(Math.abs(-0.50));
        System.out.println(Math.ceil(0.50));
        System.out.println(Math.floor(0.50));
        System.out.println(Math.round(0.50));
    }
}

class Q20 {
    public static void main(String[] args) {
        System.out.println("1" + "2" + "3" == "1" + "2" + "3");
        final String str = "1";
        System.out.println(str + "2" + "3" == "1" + "2" + "3");
        String strNew = new String("1");
        System.out.println(strNew + "2" + "3" == "1" + "2" + "3");
    }
}

class Q23 {
    public static void main(String [] args) {
        byte var = 127;
        //System.out.println(var = var - 1);
        //System.out.println(var = var + 1);
        System.out.println(++var);
        System.out.println(--var);
        System.out.println(var *= 2);
        System.out.println(var -= 10);
        System.out.println(var += 2);
        System.out.println(var);
    }
}

class Q24 {
    public static void main(String [] args) {
        String text = null;
        //System.out.println(text.repeat(3));
        //System.out.println(null + null + null);
        //System.out.println(null + "null" + null);
        //System.out.println(text *= 3);
        //System.out.println(text += "null"); //nullnull
        //System.out.println(text += "null".repeat(2));
        //System.out.println(text + text + text);
        //text += null;System.out.println((text.concat("null")));
        //text += null;System.out.println((text.concat(null)));
    }
}

class Q26 {
    public static void main(String [] args) {
        String text = "RISE ";
        //RISE + ABOVE
        text = text + (text = "ABOVE ");
        System.out.println(text);
    }
}

class Q27 {
    public static void main(String[] args) {
        StringBuilder sb = new StringBuilder("INHALE ");
        String s = sb.toString() + (sb.append("EXHALE \n\t"));
        System.out.println(s);
        System.out.println(s.strip().length());

        System.out.println('\u00A0');
        System.out.println('\u2007');
        System.out.println('\u202F');
        System.out.println('\u000B');
        System.out.println('\u001C');
        System.out.println('\u001D');
        System.out.println('\u001E');
        System.out.println('\u001F');
        System.out.println('\f');
        System.out.println('\t');
        System.out.println('\n');
        System.out.println('\r');
    }
}

class Q28 {
    public static void main(String [] args) {
        boolean flag = false;
        System.out.println((flag = true) | (flag = false) || (flag = true));
        System.out.println(flag);

        System.out.println((true | false) || (flag = true)); //flag = true
        System.out.println(true || (flag = true)); //flag = true
    }
}

class Q29 {
    public static void main(String [] args) {
        boolean status = true;
        //System.out.println(status = false || status = true | status = false);
        System.out.println(status = false || (status = true) | (status = false));
        System.out.println(status = true || (status = false) | (status = false));
        System.out.println(status);
    }
}

class Q31 {
    public static void main(String[] args) {
        int m = 20;
        int var = --m * m++ + m-- - --m;
        System.out.println("m = " + m);
        System.out.println("var = " + var);
    }
}

class Q32 {
    public static void main(String[] args) {
        int x = 7;
        boolean res = x++ == 7 && ++x == 9 || x++ == 9;
        System.out.println("x = " + x);
        System.out.println("res = " + res);
    }
}

class Q37 {
    public static void main(String [] args) {
        var val = 9;
        System.out.println(val += 10 - -val-- - --val);
    }
}

class Q38 {
    public static void main(String [] args) {
        LocalDate date = LocalDate.parse("2024-1-01");
        System.out.println(date);
    }
}

class Q41 {
    public static void main(String [] args) {
        LocalDate date = LocalDate.of(2068, 4, 15);
        System.out.println(date.getMonth() + ":" + date.getMonthValue());
    }
}

class Q42 {
    public static void main(String [] args) {
        LocalDate newYear = LocalDate.of(2028, 1, 1);
        LocalDate christmas = LocalDate.of(2028, 12, 25);
        boolean flag1 = newYear.isAfter(christmas);
        boolean flag2 = newYear.isBefore(christmas);
        System.out.println(flag1 + ":" + flag2);
    }
}

class Q45 {
    public static void main(String [] args) {
        LocalDate date1 = LocalDate.parse("1980-03-16");
        System.out.println(date1.minusYears(1000000000));
        LocalTime time1 = LocalTime.parse("01:01:01");
        LocalDateTime dateTime1 = LocalDateTime.parse("1980-03-16T" + "01:01:01");
        System.out.println(dateTime1.isEqual(dateTime1));
        LocalDate date2 = LocalDate.parse("1980-03-16");
        System.out.println(date1.equals(date2) + " : " + date1.isEqual(date2));
    }
}

class Q48 {
    public static void main(String [] args) {
        LocalDate joiningDate = LocalDate.parse("2006-03-16");
        System.out.println(joiningDate.withDayOfYear(29)); // 2006-01-29
        System.out.println(joiningDate.withMonth(12)); // 2006-12-16
        System.out.println(joiningDate.withYear(29)); // 0029-03-16

        LocalTime time1 = LocalTime.parse("01:01:01");
        System.out.println(time1.withHour(5)); // 05:01:01
        LocalDateTime dateTime1 = LocalDateTime.parse("1980-03-16T" + "01:01:01");
        System.out.println(dateTime1.withNano(100000)); // 1980-03-16T01:01:01.000100
    }
}

class Q51 {
    public static void main(String [] args) {
        Period period = Period.of(0, 1000, 0);
        System.out.println(period); //P1000M
    }
}

class Q54 {
    public static void main(String [] args) {
        LocalDate date = LocalDate.parse("2000-06-25");
        while(date.getDayOfMonth() >= 20) {
            System.out.println(date);
            date.plusDays(-1); //immutable
        }
    }
}

class Q58 {
    public static void main(String [] args) {
        LocalDate date = LocalDate.parse("1947-08-14");
        LocalTime time = LocalTime.MAX;
        System.out.println(date.atTime(time));
    }
}

class Q64 {
    public static void main(String [] args) {
        LocalDate date = LocalDate.ofYearDay(2022, 32);
        System.out.println(date);
    }
}

class Q66 {
    public static void main(String [] args) {
        LocalDate date = LocalDate.ofEpochDay(0);
        System.out.println(date);
    }
}

class Q67 {
    public static void main(String [] args) {
        System.out.println(Instant.EPOCH);
    }
}

class Q68 {
    public static void main(String[] args) {
        System.out.println(Period.between(LocalDate.now(), LocalDate.parse("2024-11-21")));
        System.out.println(Duration.between(LocalDateTime.now(), LocalDateTime.parse("2024-11-21T01:01:01")));
        System.out.println(Duration.between(LocalTime.now(), LocalTime.parse("01:01:01")));
    }
}

class Q79 {
    public static void main(String[] args) {
        System.out.println(Duration.ofNanos(1000000000));
        System.out.println(Duration.ofMillis(1000000000));
    }
}

class Q80 {
    public static void main(String [] args) {
        LocalDate d1 = LocalDate.now();
        LocalDateTime d2 = LocalDateTime.now();
        //System.out.println(Duration.between(d1, d2)); // DateTimeException
        //System.out.println(Duration.between(d2, d1)); // DateTimeException
    }
}

class Q81 {
    public static void main(String [] args) {
        LocalTime t1 = LocalTime.now();
        LocalDateTime t2 = LocalDateTime.now();
        System.out.println(Duration.between(t1, t2)); //Первый аргумент преобразуется во второй
        System.out.println(Duration.between(t2, t1)); // DateTimeException
    }
}

class Q83 {
    public static void main(String [] args) {
        LocalTime t1 = LocalTime.parse("11:03:15.987");
        System.out.println(t1.plus(22, ChronoUnit.HOURS)
                .equals(t1.plusHours(22)));
        System.out.println(t1.plus(22, ChronoUnit.HOURS));
        System.out.println(t1.plusHours(22));
    }
}

class Q85 {
    public static void main(String [] args) {
        LocalDate date1 = LocalDate.of(2019, 1, 1);
        Duration d = Duration.ofDays(1);
        System.out.println(d);
        System.out.println(date1.plus(d));
    }
}

class Q88 {
    public static void main(String [] args) {
        LocalDate date = LocalDate.of(2018, 11, 4);
        LocalTime time = LocalTime.of(1, 59, 59);
        ZonedDateTime dt = ZonedDateTime.of(date, time, ZoneId.of("America/New_York"));
        System.out.println(dt);
        dt = dt.plusSeconds(1);
        System.out.println(dt);
        System.out.println(dt.getHour() + ":" + dt.getMinute() + ":" + dt.getSecond());
    }
}

class Q91 {
    public static void main(String [] args) {
        LocalDate date = LocalDate.of(2019, 1, 1);
        LocalTime time = LocalTime.of(0, 0);
        ZoneId india = ZoneId.of("Asia/Kolkata");
        ZonedDateTime zIndia = ZonedDateTime.of(date, time, india);

        ZoneId us = ZoneId.of("America/Los_Angeles");
        ZonedDateTime zUS = zIndia.withZoneSameLocal(us);
        System.out.println(zIndia);
        System.out.println(zUS);
        System.out.println(Duration.between(zIndia, zUS)); //Line 15

        zUS = zIndia.withZoneSameInstant(us);
        System.out.println(zIndia);
        System.out.println(zUS);
        System.out.println(Duration.between(zIndia, zUS)); //Line 15

        System.out.println(Duration.between(zUS.toLocalDateTime(), zIndia.toLocalDateTime()));
    }
}

class Q92 {
    public static void main(String[] args) {
        Instant instant = Instant.now();
        LocalDateTime obj = instant.atZone(ZoneId.systemDefault()).toLocalDateTime();
        System.out.println(ZoneId.systemDefault());
        LocalDateTime localDateTime = Instant.ofEpochSecond(10000000).atZone(ZoneId.systemDefault()).toLocalDateTime();
        System.out.println(obj);
        System.out.println(localDateTime);
        System.out.println(Instant.ofEpochMilli(10000000));
    }
}

class Q93 {
    public static void main(String[] args) {
        Date date = new Date();
        LocalDate localDate = null;
        System.out.println(date);
        System.out.println(date.toInstant());
        System.out.println(date.getTime());
        System.out.println(date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());
        System.out.println(Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDate());
    }
}