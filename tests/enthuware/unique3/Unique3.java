package unique3;

import java.sql.Connection;
import java.sql.JDBCType;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.*;
import java.util.function.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Unique3 {
}

class Q1 {
    public static void generateMultiplicationTable(int number) {
        Stream<Integer> sin = Stream.of(1, 2, 3);
        Consumer<Integer> c1 = System.out::print;
        Consumer<Integer> c2 = x -> {
            System.out.println(" * " + number + " = " + x * number);
        };
        sin.forEach(c2.andThen(c1));
        //sin.forEach(c1.andThen(c2));
    }

    public static void main(String[] args) throws Exception {
        generateMultiplicationTable(2);
    }
}

class Q4 {
    public static void main(String[] args) {
        //Map m = new TreeMap();
        //Map<Object, Object> m = new TreeMap<Object, Object>();
        //Map<Object, ?> m = new LinkedHashMap<Object, Object>();
        //Map<Object, ? super ArrayList> m = new LinkedHashMap<Object, ArrayList>();
        Map m = new HashMap();
        m.put("1", new ArrayList());
        m.put(1, new Object());
        m.put(1.0, "Hello");
        System.out.println(m);
    }
}

class Q7Book implements Comparable {
    String isbn;
    String title;

    public Q7Book(String isbn, String title) {
        this.isbn = isbn;
        this.title = title;
    }     //accessors not shown

    public String getIsbn() {
        return isbn;
    }

    public static void main(String[] args) {
        new ArrayList<Q7Book>(List.of(new Q7Book("B", "A"), new Q7Book("A", "B")))
                .stream().sorted().forEach(b -> System.out.println(b.getIsbn()));
    }

    @Override
    public int compareTo(Object o) {
        return this.isbn.compareTo(((Q7Book) o).isbn);
    }
}

class Q8 {
    public static void main(String[] args) {
        Map<String, String> map1 = new HashMap<>();
        map1.put("a", "x");
        map1.put("b", "x"); //INSERT CODE HERE
        BiFunction<String, String, String> f = String::concat;
        map1.merge("b", "y", f);
        map1.merge("c", "y", f);
        System.out.println(map1);
    }
}

class Q14 {
    public static void main(String[] args) {
        List<String> strList = Arrays.asList("a", "aa", "aaa");
        Function<String, Integer> f = x -> x.length();
        Consumer<Integer> c = x -> System.out.print("Len:" + x + " ");
        strList.stream().map(f).forEach(c);
    }
}

class Q16 {
    public static void main(String[] args) {
        DoubleBinaryOperator dbo = (a, b) -> a + b;
        System.out.println(dbo.applyAsDouble(1, 2));

        BinaryOperator<Double> bo = (a, b) -> a + b;
        System.out.println(Stream.of(1., 2.).reduce(bo));

        System.out.println(Stream.of(1., 2.).mapToDouble(i -> i).reduce(0.0, dbo));
    }
}

class Q17 {
    public static void main(String[] args) throws SQLException {
        try (Connection c = null;
             var stmt = c.prepareCall("{call generateDailyGainsReport(?,?)}")) {
            stmt.setObject("PORTFOLIOID", "", JDBCType.VARCHAR);
            stmt.setObject("VALUEDATE", "", JDBCType.DATE);
            boolean hasResult = stmt.execute();
            if (hasResult) {
                ResultSet rs = stmt.getResultSet();
            }
        }
    }
}

interface Q20House {
    public default void lockTheGates() {
        System.out.println("Locking House");
    }
}

interface Q20Office {
    public void lockTheGates();
}

class Q20HomeOffice implements Q20House, Q20Office {

    @Override
    public void lockTheGates() {
        Q20House.super.lockTheGates();
    }
}

class Q20 {
    public static void main(String[] args) {
        Q20Office off = new Q20HomeOffice();
        off.lockTheGates();
        Q20House home = (Q20House) off;
        home.lockTheGates();
    }
}

class Q23 {
    public static void main(String[] args) {
        List<String> letters = new ArrayList<String>();
        letters.addAll(List.of("a", "c", "b"));
        Collections.sort(letters);
        System.out.println(Collections.binarySearch(letters, "c"));
        Collections.reverse(letters);
        System.out.println(Collections.binarySearch(letters, "c", Collections.reverseOrder()));
    }
}

class Q25A {
}

class Q25B extends Q25A {
}

class Q25C extends Q25B {
}

class Q25 {
    public static void main(String[] args) {
        Object o = new Q25C();
        o = new Q25A();
        o = new Q25B();
        System.out.println((o instanceof Q25B) && (!(o instanceof Q25C)));
        System.out.println(!(!(o instanceof Q25B) || (o instanceof Q25C)));
    }
}

interface Q30House {
    public default String getAddress() {
        return "101 Main Str";
    }
}

interface Q30Office {
    public static String getAddress() {
        return "101 Smart Str";
    }
}

interface Q30WFH extends Q30House, Q30Office {
    private boolean isOffice() {
        return true;
    }
}

class Q30HomeOffice implements Q30House, Q30Office {
    public String getAddress() {
        return "R No 1, Home";
    }
}

class Q30 {
    public static void main(String[] args) {
        Q30Office off = new Q30HomeOffice();
        System.out.println(Q30Office.getAddress());
    }
}

class Q35 {
    public static void main(String[] args) {
        Q35A a = new Q35A();
        Q35B b = new Q35B();
        System.out.println(a instanceof Q35B);
        System.out.println(b instanceof Q35A);
        System.out.println(a instanceof Q35T1);
        System.out.println(a instanceof Q35T2);
        System.out.println(b instanceof Q35T1);
        System.out.println(b instanceof Q35T2);
    }
}

class Q35A implements Q35T1, Q35T2 {
}

class Q35B extends Q35A implements Q35T1 {
}

interface Q35T1 {
}

interface Q35T2 {
}

class Q39 {
    public static void main(String[] args) {
        List<Integer> li = List.of(3, 7, 2, 1, 4, 6, 5, 8);
        Spliterator<Integer> sit1 = li.spliterator();
        Spliterator<Integer> sit2 = sit1.trySplit();
        sit1.forEachRemaining(System.out::print);
        System.out.println("");
        sit2.forEachRemaining(System.out::print);
    }
}

class Q40Names {
    private static List<String> list;

    public static List<String> getList() {
        return list;
    }

    public static void setList(List<String> l) {
        list = l;
    }

    public static void printNames(String i) {
        System.out.println(getList());
    }

    public static void main(String[] args) {
        List<String> list = Arrays.asList("Bob Hope", "Bob Dole", "Bob Brown");
        Q40Names n = new Q40Names();
        n.setList(list.stream().collect(Collectors.toList()));
        n.getList().forEach(Q40Names::printNames);
    }
}

class Q42 {
    public static void main(String[] args) {
        List<Integer> values = Arrays.asList(2, 4, 6, 9);
        Predicate<Integer> check = (Integer i) -> {
            System.out.println("Checking");
            return i == 4;
        };
        Predicate even = (i) -> ((Integer) i) % 2 == 0;
        values.stream().filter(check).filter(even).count(); //4
    }
}

class Q44 {
    public static void main(String[] args) {
        System.out.println(getDateString(ZonedDateTime.now()));
        System.out.println(getDateString(LocalDateTime.now()));
    }

    public static String getDateString(TemporalAccessor ldt) {
        return DateTimeFormatter.ISO_ZONED_DATE_TIME.format(ldt);
    }
}

class Q46Account {
    private String id;

    public Q46Account(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Q46Account{" +
                "id='" + id + '\'' +
                '}';
    }
}

class Q46BankAccount extends Q46Account {
    private double balance;

    @Override
    public String toString() {
        return "Q46BankAccount{" +
                "balance=" + balance +
                '}';
    }

    public double getBalance() {
        return balance;
    }

    public Q46BankAccount(String id, double balance) {
        super(id);
        this.balance = balance;
    }      //accessors not shown

    public static void main(String[] args) {
        Map<String, Q46Account> myAccts = new HashMap<>();
        myAccts.put("111", new Q46Account("111"));
        myAccts.put("222", new Q46BankAccount("111", 200.0));
        System.out.println(myAccts);
        Function<String, Q46BankAccount> f = (a) -> new Q46BankAccount(a, 1.);
        BiFunction<String, Q46Account, Q46Account> bif =
                (a1, a2) -> a2 instanceof Q46Account
                        ? new Q46BankAccount(a1, 300.0)
                        : new Q46Account(a1);
        Q46BankAccount ba = (Q46BankAccount) myAccts.get("222");
        System.out.println(ba.getBalance());

        myAccts.computeIfPresent("222", bif);
        ba = (Q46BankAccount) myAccts.get("222");
        System.out.println(ba.getBalance());

        //Q46BankAccount ba2 = (Q46BankAccount) myAccts.get("111");
        //System.out.println(ba2);

        myAccts.put("333", new Q46BankAccount("333", 2.));
        System.out.println(myAccts.get("333"));
        myAccts.computeIfAbsent("333", f);
        System.out.println(myAccts.get("333"));
    }
}

class Q51 {
    public static void main(String args[]) throws Exception {
        Exception e = null;
        throw e;
    }
}

class Q52 {
    public static void main(String[] args) {
        var nums = List.of(1, 2, 3, 4).stream();
        //System.out.println(nums.collect(Collectors.averagingInt(i -> i)));
        System.out.println(nums.parallel().mapToDouble(i -> i).average().getAsDouble());
    }
}

sealed interface Q55Member permits Q55Person { //LINE A
    String role();

    void role(String role);
}

final class Q55Person implements Q55Member { //LINE B
    private String role = "UNKNOWN";

    public String role() {
        return role;
    }

    public void role(String role) {
        this.role = role;
    }
}

record Q55Student(int id, Q55Person person) //LINE C
 {
    public String getRole() {
        return person.role();
    }

    public void setRole(String role) {
        person.role(role);
    } //LINE D
}

class Q55 {
    public static void main(String[] args) {
        Q55Student s = new Q55Student(123, new Q55Person()); //LINE E
        s.setRole("student"); //LINE F
        System.out.println(s);
    }
}