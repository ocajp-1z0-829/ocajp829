package test1;

import foundation4.Foundation4;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Test1 {
}

record Q1Student(int id, String... subjects) {
    @Override
    public int id() {
        return 10;
    }

    @Override
    public boolean equals(Object obj) {
        return false;
    }
}

interface Q1Person {
    int id();

    String name();
}

record Q1Student2(int id, String... subjects) implements Q1Person {
    public String name() {
        return "none";
    }

    public static void main(String[] args) {
        Q1Student q1Student = new Q1Student(1, "2");
        System.out.println(q1Student.id());
        System.out.println(q1Student.subjects()[0]);
    }
}

class Q2 {
    public static void main(String[] args) {
        System.out.println("emptiness".substring(9));
    }
}

class Q6Automobile {
    public void drive() {
        System.out.println("Automobile: drive");
    }
}

class Q6Truck extends Q6Automobile {
    public void drive() {
        System.out.println("Truck: drive");
    }

    public static void main(String args[]) {
        Q6Automobile a = new Q6Automobile();
        Q6Truck t = new Q6Truck();
        a.drive(); //1
        //t = (Q6Truck) a;
        t.drive(); //2
        a = t;     //3
        a.drive(); //4
    }
}

class Q8DaysTest {
    static String[] days = {"monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"};

    public static void main(String[] args) {
        var index = 0;
        for (var day : days) {
            if (index == 3) {
                break;
            } else {
                continue;
            }
            //index++;
            //if (days[index].length() > 3) {
            //  days[index] = day.substring(0, 3);
            //}
        }
        System.out.println(days[index]);
    }
}

class Q9 {
    public static void main(String[] args) {

        String[] sa = {"charlie", "bob", "andy", "dave"};
        //var ls = new ArrayList<String>(Arrays.asList(sa));
        var ls = Arrays.asList(sa);
        ls.sort((var a, var b) -> a.compareTo(b));
        System.out.println(sa[0] + " " + ls.get(0));
    }
}

class Q12TestClass {
    public static void main(String[] args) {
        String s = "/usr/home1/test.txt";
        String d = "/usr/home2/test.txt";   //INSERT CODE HERE
        try {
            Files.move(Paths.get(s), Paths.get(d), StandardCopyOption.REPLACE_EXISTING);
            //Files.delete(Paths.get(s));
        } catch (Exception e) {
        }
        try (FileChannel in = new FileInputStream(s).getChannel(); FileChannel out = new FileOutputStream(d).getChannel()) {
            in.transferTo(0, in.size(), out);
        } catch (Exception e) {
        }
    }
}

class Q15FinallyTest {
    public static void main(String args[]) {
        try {
            if (args.length == 0) return;
            else throw new Exception("Some Exception");
        } catch (Exception e) {
            System.out.println("Exception in Main");
            //throw new Exception();
        } finally {
            System.out.println("The end");
        }
    }
}

class Q16 {
    public static void main(String[] args) {
        Function.identity();
    }
}

class Q17 {
    public static void main(String[] args) {
        System.out.println(Float.isInfinite(0));
        System.out.println(Double.isInfinite(-1));
    }
}

class Q18 {
    public static void main(String[] args) {
        Period p = Period.between(LocalDate.now(), LocalDate.of(2022, Month.SEPTEMBER, 1));
        System.out.println(p);

        Duration d = Duration.between(LocalDate.now(), LocalDate.of(2022, Month.SEPTEMBER, 1));
        System.out.println(d);
    }
}

interface Q20I {
}

class Q20 {
    public static void main(String[] args) {
        java.util.ServiceLoader<Q20I> loader = java.util.ServiceLoader.load(Q20I.class);//may throw a java.util.ServiceConfigurationError
        System.out.println(loader.findFirst());
    }
}

class Q21A {
    public Q21A() {
    }

    public Q21A(int i) {
        System.out.println(i);
    }
}

class Q21B {
    static Q21A s1 = new Q21A(1);
    Q21A a = new Q21A(2);

    Q21B() {
        System.out.println("9");
    }

    public static void main(String[] args) {
        Q21A q21A = new Q21A();
        Q21B b = new Q21B();
        Q21B b2 = new Q21B();
        Q21A a = new Q21A(3);
    }

    static Q21A s2 = new Q21A(4);
}

class Q21C extends Q21B {
    static Q21A s1 = new Q21A(5);
    Q21A a = new Q21A(6);

    Q21C() {
        System.out.println("10");
    }

    public static void main(String[] args) {
        Q21A q21A = new Q21A();
        Q21C b = new Q21C();
        Q21C b2 = new Q21C();
        Q21A a = new Q21A(7);
    }

    static Q21A s2 = new Q21A(8);
}

//

sealed abstract class Q23Car permits Q23SUV, Q23Sedan {
    String name;

    Q23Car(String name) {
        this.name = name;
    }
}

final class Q23SUV extends Q23Car {
    int milage;

    Q23SUV(String name, int milage) {
        super(name);
        this.milage = milage;
    }
}

final class Q23Sedan extends Q23Car {
    double price;

    Q23Sedan(String name, double price) {
        super(name);
        this.price = price;
    }
}

class Q23TestClass {
    public static void main(String args[]) throws Exception {
        Q23Car[] cars = new Q23Car[]{new Q23SUV("Ford", 5000), new Q23Sedan("Honda", 10000.0), new Q23SUV("Chevy", 10000), new Q23Sedan("Toyota", 20000.0)};
        DoubleSummaryStatistics dss = Stream.of(cars).filter(c -> c instanceof Q23SUV).collect(Collectors.summarizingDouble(c -> ((Q23SUV) c).milage));
        System.out.println(dss.getMax());
        System.out.println(dss.getCount());
        String sedans = Stream.of(cars).filter(c -> c instanceof Q23Sedan).collect(Collectors.mapping(c -> c.name, Collectors.joining(",")));
        System.out.println(sedans);
    }
}

class Q24 {
    public static void main(String[] args) {

        for (var i = 0; i < 2; i++) {
            System.out.print(i);
        }

        System.out.println();

        for (int i = 0; i < 2; System.out.print(++i)) ;

        System.out.println();

        for (int i = 0; i++ < 2; System.out.print(i)) ;

        System.out.println();

        int i = 0;
        while (i++ < 2) {
            System.out.print(i);
        }

        System.out.println();

        i = 0;
        do {
            System.out.print(i);
        } while (i++ < 2);

        System.out.println();
        i = 0;
        do {
            System.out.print(i);
        } while (++i < 2);
    }
}

class Q26 {
    public static void main(String[] args) {
        char c = 0;
        System.out.println(c);
    }
}

//
class Q28SomeThrowable extends Throwable {
}

class Q28MyThrowable extends Q28SomeThrowable {
}

class Q28TestClass {
    public static void main(String args[]) throws Q28SomeThrowable {
        try {
            m1();
        } catch (Q28SomeThrowable e) {
            throw e;
        } finally {
            System.out.println("Done");
        }
    }

    public static void m1() throws Q28MyThrowable {
        throw new Q28MyThrowable();
    }
}

//
class Q29 {
    static void m1() {
        try {
            // line1
            throw new IOException();
        } catch (IOException e) {
            throw new SQLException();
            //} catch (SQLException e) {
            //  throw new InstantiationException();
        } finally {
            try {
                throw new CloneNotSupportedException();
            } catch (CloneNotSupportedException e) {
                throw new RuntimeException(e);
            }
            // CloneNotSupportedException is a checked exception.
        }
    }

    public static void main(String[] args) {
        m1();
    }
}

class Q31 {
    public static void main(String[] args) {
        double amount = 200_00.00;
        NumberFormat nf1 = NumberFormat.getCurrencyInstance(Locale.US);
        System.out.println(Locale.US);
        System.out.println(new Locale("US"));
        NumberFormat nf2 = NumberFormat.getCompactNumberInstance(Locale.US, NumberFormat.Style.SHORT);
        System.out.println(nf1.format(amount));
        System.out.println(nf2.format(amount));
    }
}

class Q33A implements Runnable {
    @Override
    public void run() {
    }
}

class Q33B extends Q33A implements Observer {
    @Override
    public void update(Observable o, Object arg) {
    }
}

class Q33 {
    public static void main(String[] args) {
        Q33A a = new Q33A();
        Q33B b = new Q33B();

        Object o = a;
        Runnable r = (Runnable) o;

        //Object o = b; Observer o2 = o;

        o = b;
        r = (Runnable) b;
    }
}

class Q37 {
    public List<? super Number> getList() {
        if (true)
            return new ArrayList<Number>();
        else if (true)
            return new ArrayList<Object>();
        else
            return new ArrayList();
    }
}

class Q38PathTest {
    static Path p1 = Paths.get("C:\\/main/project/Starter.java");

    public static String getData() {
        String data = p1.getName(0).toString();
        return data;
    }

    public static void main(String[] args) {
        System.out.println(getData());
    }
}

class Q39Base {
    public <T> Collection<T> getCollection(T t, Integer n) {
        return new ArrayList<T>();
    }

    //void m(Set<CharSequence> cs){}
    void m(Set<Integer> s) {
    }

}

class Q39Derived extends Q39Base {
    public <T> List<T> getCollection(T t, Integer m) {
        return new ArrayList<T>();
    } //1

    //public <T> Stream<T> getCollection(T t, Integer m){ return new ArrayList<T>(); }; //2
    //public <T> void getCollection(T t, Integer m){ return new ArrayList<T>(); }; //3
    public <T> List<T> getCollection(String t, Integer m) {
        return new ArrayList<T>();
    }

    ;//4

    void m(Set s) {
    }
}

class Q46 {
    public static void main(String[] args) {
        List<Double> dList = Arrays.asList(10.0, 12.0);
        dList.stream().forEach(x -> {
            x += 10;
        });
        dList.stream().forEach(d -> System.out.println(d));

        List<StringBuilder> dList2 = Arrays.asList(new StringBuilder("a"), new StringBuilder("aa"));
        dList2.stream().forEach(x -> x.append("b"));
        dList2.stream().forEach(x -> System.out.println(x));
    }
}

class Q52 {
    public static void main(String[] args) {
        List<Integer> ls = Arrays.asList(10, 47, 33, 23);
        int max = ls.stream().max(Comparator.comparing(a -> a + 1)).get();
        System.out.println(max); //1

        System.out.println(ls.stream().reduce((a, b) -> a > b ? a : b));
        System.out.println(ls.stream().reduce(Integer.MIN_VALUE, (a, b) -> a > b ? a : b));
    }
}

class Q53 {
    public static void main(String[] args) {

        java.time.LocalDate dt = java.time.LocalDate.parse("2015-01-01")
                .minusMonths(1)
                .minusDays(1)
                .plusYears(1);
        System.out.println(dt);
    }
}

sealed class Q54 extends Foundation4 { }
final class Q54Sub extends Q54 {}