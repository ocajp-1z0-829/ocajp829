package test8;

import java.io.*;
import java.text.NumberFormat;
import java.time.*;
import java.time.format.FormatStyle;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.IntFunction;
import java.util.function.IntUnaryOperator;
import java.util.stream.IntStream;

public class Test8 {
}

class Q1Device implements AutoCloseable {
    String header = null;

    public void open() {
        header = "OPENED";
        System.out.println("Device Opened");
    }

    public String read() throws IOException {
        throw new IOException("Unknown");
    }

    public void writeHeader(String str) throws IOException {
        System.out.println("Writing : " + str);
        header = str;
    }

    public void close() {
        if (header == null) throw new RuntimeException("Aaaa");
        header = null;
        System.out.println("Device closed");
    }

    public static void testDevice() {
        try (Q1Device d = new Q1Device()) {
            d.open();
            d.writeHeader("TEST");
            d.close();
            throw new RuntimeException("ZZZ");
        } catch (IOException e) {
            System.out.println("Got Exception");
        }
    }

    public static void main(String[] args) {
        Q1Device.testDevice();
        System.out.println("Bbb");
    }
}

//
class Q8MarkTest {
    public static void main(String[] args) {
        try (Reader r = new BufferedReader(new FileReader("tests/enthuware/test8/test.txt"))) {
            if (r.markSupported()) {
                BufferedReader in = (BufferedReader) r;
                System.out.print(in.readLine());
                in.mark(100);
                System.out.print(in.readLine());
                System.out.print(in.readLine());
                in.reset();
                System.out.print(in.readLine());
                in.reset();
                System.out.println(in.readLine());
            } else {
                System.out.println("Mark Not Supported");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

//

class Q10Device implements AutoCloseable {
    String header = null;

    public Q10Device(String name) throws IOException {
        header = name;
        if ("D2".equals(name)) throw new IOException("Unknown");
        System.out.println(header + " Opened");
    }

    public String read() throws IOException {
        return "";
    }

    public void close() {
        System.out.println("Closing device " + header);
        throw new RuntimeException("RTE while closing " + header);
    }

    public static void main(String[] args) throws Exception {
        try (Q10Device d1 = new Q10Device("D1");
             Q10Device d2 = new Q10Device("D2")) {
            throw new Exception("test");
        }
    }
}
//


interface Q19Identifier {
    int id();
}

/*non-sealed*/ class Q19Person /*permits Q19Student*/ {
}

record Q19Student(int id, String subject) /*extends Q19Person*/ implements Q19Identifier {
    public static final long serialVersionUID = 1L;
    //private String name = "unknown";

    String name() {
        return "unknown";
    }

    public int id() {
        return id;
    }
}

class Q20TestClass {
    public static void main(String[] args) {
        calculate(2);
    }

    public static void calculate(int x) {
        final int i = 4;
        String val;
        switch (x) {
            case 2:
                break;
            case i:
            default:
                val = "def";
        }
        //System.out.println(val);
    }
}

class Q21 {
    public static void main(String[] args) {
        String s1 =
                """                  
                        a \
                        b \t                  
                        c \s                 
                        """;
        System.out.println(s1.length() + " " + s1.split("\\n").length);
    }
}

class Q24 {
    public static void main(String[] args) {
        LocalDate d = LocalDate.now();
        LocalDateTime t = LocalDateTime.now();
        ZonedDateTime z = ZonedDateTime.of(t, ZoneId.systemDefault());
        java.time.format.DateTimeFormatter df = java.time.format.DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM);
        System.out.println(df.format(d));
    }
}

//

class Q25TestClass {
    public static void main(String[] args) throws Exception {
        List<Integer> list = new CopyOnWriteArrayList<>();
        ExecutorService es = Executors.newFixedThreadPool(5);
        CyclicBarrier cb = new CyclicBarrier(2, () -> System.out.println("All Done"));
        IntStream.range(0, 5).forEach(n -> es.execute(() -> {
            try {
                list.add(n);
                cb.await();
            } catch (InterruptedException | BrokenBarrierException e) {
                System.out.println("Exception");
            }
        }));
        es.shutdown();
    }
}
//

class Q27 {
    public static void main(String[] args) {
        Duration d = Duration.ofMillis(1100);
        System.out.println(d.toString());
        d = Duration.ofSeconds(61);
        System.out.println(d);
    }
}

class Q34TestClass {
    public static void main(String args[]) {
        int i = 0;
        boolean bool1 = true;
        boolean bool2 = false;
        boolean bool = false;
        bool = (bool2 & method1(i++)); //1
        System.out.println(bool1);
        System.out.println(bool2);
        System.out.println(bool);
        bool = (bool2 && method1(i++)); //2
        System.out.println(bool1);
        System.out.println(bool2);
        System.out.println(bool);
        bool = (bool1 | method1(i++)); //3
        System.out.println(bool1);
        System.out.println(bool2);
        System.out.println(bool);
        bool = (bool1 || method1(i++)); //4
        System.out.println(bool1);
        System.out.println(bool2);
        System.out.println(bool);
        System.out.println(i);
    }

    public static boolean method1(int i) {
        return i > 0 ? true : false;
    }
}
//

class Q35Outsider {
    public class Q35Insider {
    }
}

class Q35TestClass {
    public static void main(String[] args) {
        var os = new Q35Outsider();        // 1 insert line here
        Q35Outsider.Q35Insider in = os.new Q35Insider();
        Q35Outsider.Q35Insider in2 = new Q35Outsider().new Q35Insider();
    }
}

//
class Q38TestClass {
    public static int operate(IntUnaryOperator iuo) {
        return iuo.applyAsInt(5);
    }

    public static void main(String[] args) {
        IntFunction<IntUnaryOperator> fo = a -> (b -> a - b);  //1
        int x = operate(fo.apply(20)); //2
        System.out.println(x);
    }
}

class Q42 {
    public static void main(String[] args) {
        HashSet<String> keys = new HashSet<>(List.of("a", "b", "c"/*, null*/));
        ArrayList<String> values = new ArrayList<>(Set.of("1", "2", "3"/*, "1"*/));
        Map<String, String> m = new HashMap<>();
        int i = 0;
        for (var key : keys) {
            m.put(key, values.get(i++));
        }
        keys.clear();   //2
        values.clear();
        System.out.println(m.keySet().size() + " " + m.values().size());
    }
}

class Q44 {
    public static void main(String[] args) {
        ArrayList<Integer> source = new ArrayList<Integer>();
        source.addAll(Arrays.asList(1, 2, 3, 4, 5, 6));
        List<Integer> destination = Collections.synchronizedList(new ArrayList<Integer>());
        source.parallelStream()  //1
                .peek(item -> {
                    destination.add(item);
                }) //2
                .forEachOrdered(System.out::print);
        System.out.println("");
        destination.stream().forEach(System.out::print); //4
        System.out.println("");
    }
}

class Q47FileCopier {
    public static void copy(String records1, String records2) {
        try (InputStream is = new FileInputStream(records1);
             OutputStream os = new FileOutputStream(records2);) {  //1
            //if (os == null)
            //os = new FileOutputStream("c:\\default.txt");  //2
            byte[] buffer = new byte[1024];
            int bytesRead = 0;
            while ((bytesRead = is.read(buffer)) != -1) {  //3
                os.write(buffer, 0, bytesRead);
                System.out.println("Read and written bytes " + bytesRead);
            }
        } catch (
                IOException e) { //4
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        copy("tests/enthuware/test8/test.txt", "tests/enthuware/test8/test2.txt");
    }
}

class Q49 {
    public static void main(String[] args) {
        int[][] orig = {{1, 2, 3}, {4, 5, 6, 7}};
        int[][] dup = orig.clone();
        int[] copy = dup[0].clone();
        System.out.println(Arrays.deepToString(orig));
        System.out.println(Arrays.deepToString(dup));
        System.out.println(Arrays.toString(copy));
        System.out.println(orig == dup);
        System.out.println(orig.equals(dup));
        System.out.println(orig[0] == dup[0]);
        System.out.println(dup[0] == copy);
        System.out.println(dup[0].equals(copy));
    }
}
