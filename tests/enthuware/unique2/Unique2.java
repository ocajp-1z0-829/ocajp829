package unique2;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.function.DoubleSupplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Unique2 {
}

class Q1JustLooping {
    private int j;

    void showJ() {
        while (j <= 5) {
            for (int j = 1; j <= 5; ) {
                System.out.print(j + " ");
                j++;
            }
            j++;
        }
    }

    public static void main(String[] args) {
        new Q1JustLooping().showJ();
    }
}

record Q2Book(String title, String isbn) {
    @Override
    public boolean equals(Object o) {
        return (o instanceof Q2Book b && b.isbn().equals(this.isbn()));
    }
}

class Q3TestClass extends Thread {
    class Runner implements Runnable {
        public void run() {
            var t = new Thread[5];
            System.out.println(Arrays.toString(t));
            for (int i = 0; i < t.length; i++)
                System.out.println(t[i]);
        }
    }

    public static void main(String args[]) throws Exception {
        var tc = new Q3TestClass();
        new Thread(tc.new Runner()).start();
    }
}

class Q6Test {
    public int luckyNumber(int seed) {
        if (seed > 10) return seed % 10;
        int x = 0;
        try {
            if (seed % 2 == 0) throw new Exception("No Even no.");
            else return x;
        } catch (Exception e) {
            return 3;
        } finally {
            return 7;
        }
    }

    public static void main(String args[]) {
        int amount = 100, seed = 6;
        switch (new Q6Test().luckyNumber(6)) {
            case 3:
                amount = amount * 2;
            case 7:
                amount = amount * 2;
            case 6:
                amount = amount + amount;
            default:
        }
        System.out.println(amount);
    }
}

class Q11 {
    public List<? extends Number> getList() {
        return new ArrayList();
    }
}


class Q16Book {
    private String title;
    private Double price;

    public Q16Book(String title, Double price) {
        this.title = title;
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public Double getPrice() {
        return price;
    }

    public static void main(String[] args) {
        Q16Book b1 = new Q16Book("Java in 24 hrs", null);
        DoubleSupplier ds1 = b1::getPrice;
        System.out.println(b1.getTitle() + " " + ds1.getAsDouble());
    }
}

class Q18Book {
    protected final int pages = 100;

    final void mA() {
        System.out.println("In B.mA " + pages);
    }
}

class Q18Encyclopedia extends Q18Book {
    private int pages = 200;

    void mB() {
        System.out.println("In E.mB " + pages);
    }

    //void mA() {
    //  System.out.println("In E.mA " + pages);
    //}
}

class Q18 {
    public static void main(String[] args) {
        Q18Book o1 = new Q18Encyclopedia();
        Q18Book o2 = new Q18Book();
        o1.mA();
        //o1.mB();
        o2.mA();
    }
}

class Q22 {
    public static void main(String[] args) {
        int[] a = {'h', 'e', 'l'};
        int[] b = {'h', 'e', 'l', 'l', 'o'};
        int x = Arrays.compare(a, b);
        int y = Arrays.mismatch(a, b);
        System.out.println(x + " " + y);
    }
}

class Q24 {
    public static void main(String[] args) {

        var value = 1000000;
        switch (value) {
            case 1_000_000 -> {
                System.out.println("A million 1");
                break;
            }
            case 1000000_0 -> {
                System.out.println("A million 2");
            }
        }
        int hex = 0xCAFE_BABE;
        float f = 9898_7878.333_333f;
        int bin = 0b1111_0000_1100_1100;
        System.out.println(hex);
        System.out.println(f);
        System.out.println(bin);
    }
}

class FileCopier {
    public static void copy(String records1, String records2) throws IOException {
        try (InputStream is = new FileInputStream(records1); OutputStream os = new FileOutputStream(records2);) {
            byte[] buffer = new byte[1024];
            int bytesRead = 0;
            while ((bytesRead = is.read(buffer)) != -1) {
                os.write(buffer, 0, bytesRead);
                System.out.println("Read and written bytes " + bytesRead);
            }
        } catch (IOException /*| IndexOutOfBoundsException*/ e) {
            e = new FileNotFoundException();
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception {
        copy("/Users/admin/IdeaProjects/ocajp829/tests/enthuware/unique2/test.txt", "/Users/admin/IdeaProjects/ocajp829/tests/enthuware/unique2/test1.txt");
    }
}

class Q34 {
    public static void main(String[] args) {
        String str1 = "one";
        String str2 = "two";
        System.out.println(str1.equals(str1 = str2));
    }
}

class Q35Book {
    private int id;
    private String title;
    private String genre;
    private String author;
    private double price;

    public Q35Book(String title, String genre, String author) {
        this.title = title;
        this.genre = genre;
        this.author = author;
    }

    public String getGenre() {
        return genre;
    }

    public String getAuthor() {
        return author;
    }
}

class Q35 {
    public static void main(String[] args) {
        List<Q35Book> books = Arrays.asList(new Q35Book("There is a hippy on the highway", "Thriller", "James Hadley Chase"), new Q35Book("Coffin from Hongkong", "Thriller", "James Hadley Chase"), new Q35Book("The Client", "Thriller", "John Grisham"), new Q35Book("Gone with the wind", "Fiction", "Margaret Mitchell"));
        Map<String, Map<String, List<Q35Book>>> classified = null;
        classified = books.stream().collect(Collectors.groupingBy(
                //x->x.getGenre(), Collectors.groupingBy(x->x.getAuthor()) //this is fine as well. 
                Q35Book::getGenre, Collectors.groupingBy(Q35Book::getAuthor)));
        System.out.println(classified);

        Map<String, List<String>> collect = books.stream().collect(Collectors.groupingBy(Q35Book::getGenre, Collectors.mapping(Q35Book::getAuthor, Collectors.toList())));

        System.out.println(collect);
    }
}

class Q36 {
    public static void main(String[] args) {
        StringBuilder sb = new StringBuilder("asdf");
        sb.toString();

        Double d = 1.;
        Integer i = 1;
        Number n = 1.;
        System.out.println(d == n.doubleValue());
        System.out.println(i == n.intValue());
        //System.out.println(i==d);
    }
}

class Q41Shape {
}

class Q41 {
    public List<? extends Q41Shape> m4(List<? extends Q41Shape> strList) {
        List<Q41Shape> list = new ArrayList<>();
        list.add(new Q41Shape());
        list.addAll(strList);
        return list;
    }

    public void m5(ArrayList<? extends Q41Shape> strList) {
        List<Q41Shape> list = new ArrayList<>();
        list.add(new Q41Shape());
        list.addAll(strList);
    }
}

class Q42 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("greg", "dave", "don", "ed", "fred");
        Map<Integer, Long> data = names.stream().collect(Collectors.groupingBy(String::length, Collectors.counting()));
        System.out.println(data);
        System.out.println(data.values());
    }
}

class Q43 {
    public static void main(String[] args) {
        Locale la = new Locale("a", "a");
        System.out.println(la);
        Date date = new Date();
        Locale l = Locale.getDefault();
        DateFormat df = DateFormat.getDateInstance();
        System.out.println(l.getCountry() + " " + df.format(date));
    }
}

class Q44 {
    public static void main(String[] args) {
        try {
            Stream<Path> s = null;
            //s = Files.list(Paths.get("/Users/admin/IdeaProjects/ocajp829/tests/enthuware/unique2"));
            //s = Files.walk(Paths.get("c:\\temp\\pathtest"), "test.txt");
            s = Files.find(Paths.get("/Users/admin/IdeaProjects/ocajp829/tests/enthuware/unique2"), Integer.MAX_VALUE, (p, a) -> p.endsWith("test.txt") && a.isRegularFile());
            //s = Files.find(Paths.get("test.txt"));
            //s = Files.list(Paths.get("c:\\temp\\pathtest"), (p, a)->p.endsWith(".txt")&&a.isRegularFile());

            s.forEach(System.out::println);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}

abstract class Q45Bang {
    //abstract void f();
    final void g() {
    }

    //final void h() {}
    protected static int i;
    private int j;
}

final class Q45BigBang extends Q45Bang {
    //Q45BigBang(int n) {m = n;}
    public static void main(String args[]) {
        Q45Bang mc = new Q45BigBang();
    }

    //@Override
    void h() {
    }

    void k() {
        i++;
    }

    //void l() {j++;}
    int m;
}

class Q47 {
    public static void main(String[] args) {
        List<String> listOfWords = new ArrayList<>(List.of("A", "B", "C"));
        var list2 = new ArrayList<String>();
        listOfWords.parallelStream().forEach(s -> {
            if (s.length() == 2) {
                synchronized (list2) {
                    list2.add(s);
                }
            }
        });
        System.out.println(listOfWords);
    }
}

class Q49MyStringComparator implements Comparator {
    public int compare(Object o1, Object o2) {
        int s1 = ((String) o1).length();
        int s2 = ((String) o2).length();
        return s1 - s2;
    }

    static String[] sa = {"d", "bbb", "aaaa"};

    public static void main(String[] args) {
        System.out.println(Arrays.binarySearch(sa, "cc", new Q49MyStringComparator()));
        System.out.println(Arrays.binarySearch(sa, "c", new Q49MyStringComparator()));
    }
}

class Q50 {
    public static void main(String[] args) {
        String str = "10";
        int iVal = 0;
        Double dVal = 0.0;
        try {
            iVal = Integer.parseInt(str, 2);
            if ((dVal = Double.parseDouble(str)) == iVal) {
                System.out.println("Equal");
            }
        } catch (NumberFormatException e) {
            System.out.println("Exception in parsing");
        }
        System.out.println(iVal + " " + dVal);
    }
}

class Q53 {
    public static void main(String[] args) {
        LocalDateTime ld1 = LocalDateTime.of(2022, Month.MARCH, 13, 2, 0);
        ZonedDateTime zd1 = ZonedDateTime.of(ld1, ZoneId.of("US/Eastern"));
        LocalDateTime ld2 = LocalDateTime.of(2022, Month.MARCH, 13, 3, 0);
        ZonedDateTime zd2 = ZonedDateTime.of(ld2, ZoneId.of("US/Eastern"));
        System.out.println(zd1);
        System.out.println(zd2);
        long x = ChronoUnit.HOURS.between(zd1, zd2);
        System.out.println(x);
    }
}

class Q54 {
    public static void main(String[] args) {

        LocalDateTime ldt = LocalDateTime.of(2022, 12, 02, 6, 0, 0);
        //ZonedDateTime nyZdt = ldt.atZone(nyZone);
        //ZonedDateTime laZdt = ldt.atZone(laZone);
        //Duration d = Duration.between(nyZdt, laZdt);
        //System.out.println(d);
    }
}