package test2;

import java.io.*;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Test2 { }

/*
record Q3Journal(int id, String name) {
    public Q3Journal {    //1
        id = id + 1;
    }

    //public Q3Journal(int id, String name) { //2
    //this.id = id;
    //}

    public Q3Journal(String name) { //2
        this(1, "");
    }
}


class Q5Movie {
    static enum Genre {DRAMA, THRILLER, HORROR, ACTION}

    ;

    private Genre genre;
    private String name;
    private char rating = 'R';

    Q5Movie(String name, Genre genre, char rating) {
        this.name = name;
        this.genre = genre;
        this.rating = rating;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public char getRating() {
        return rating;
    }

    public void setRating(char rating) {
        this.rating = rating;
    }
}

class Q5FilteringStuff {
    public static void main(String[] args) {
        List<test3.Q5Movie> movies = Arrays.asList(new test3.Q5Movie("Titanic", test3.Q5Movie.Genre.DRAMA, 'U'), new test3.Q5Movie("Psycho", test3.Q5Movie.Genre.THRILLER, 'U'), new test3.Q5Movie("Oldboy", test3.Q5Movie.Genre.THRILLER, 'R'), new test3.Q5Movie("Shining", test3.Q5Movie.Genre.HORROR, 'U'));
        movies.stream().filter(mov -> mov.getRating() == 'R').peek(mov -> System.out.println(mov.getName())).map(mov -> mov.getName());
    }
}


class Q7A extends Thread {
    //volatile boolean flag = true;
    boolean flag = true;

    public void run() {
        System.out.println("Starting loop");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        //while (flag) {};   //1 This will not work because the flag variable is neither marked volatile nor is being accessed from withing a synchronized block.
        while (!isInterrupted()) {
        }
        ;
        System.out.println("Ending loop");
    }
}

class Q7TestClass {
    public static void main(String args[]) throws Exception {
        test3.Q7A a = new test3.Q7A();
        a.start();
        Thread.sleep(1000);

        a.flag = false;

        a.interrupt();
    }
}

//

class Q8 {
    static Collection unique = new HashSet();
    static Collection ordered = new LinkedList();

    public static void main(String args[]) throws Exception {
        BufferedReader bfr = new BufferedReader(new InputStreamReader(System.in));
        String s = bfr.readLine();
        while (s != null && s.length() > 0) {
            unique.add(s);
            ordered.add(s);
            s = bfr.readLine();
        }
        System.out.println(unique);
        System.out.println(ordered);
    }
}


class Q9LoopTest {
    int k = 5;

    public boolean checkIt(int k) {
        return k-- > 0 ? true : false;
    }

    public void printThem() {
        while (checkIt(k)) {
            System.out.print(k--);
        }
    }

    public static void main(String[] args) {
        new test3.Q9LoopTest().printThem();
    }
}

class Q10 {
    public static void main(String[] args) {
        IntStream is1 = IntStream.range(1, 3);
        System.out.println(is1);
        IntStream is2 = IntStream.rangeClosed(1, 3);
        System.out.println(is2);
        IntStream is3 = IntStream.concat(is1, is2);
        System.out.println(is3);
        //Object val = is3.collect(StringBuilder::new, (a, b) -> a.append(b).append(','), StringBuilder::append);
        Object val = is3.boxed().collect(Collectors.groupingBy(a -> a)).get(3);
        System.out.println(val);
    }
}

class Q11ArrayTest {
    public static void main(String[] args) {
        //var ia[][] = { {1, 2}, null};
        var ia = new int[][]{{1, 2}, null};
        for (int i = 0; i < 2; i++)
            for (int j = 0; j < 2; j++)
                System.out.println(ia[i][j]);
    }
}

class Q15 {
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        var myES = Executors.newSingleThreadExecutor();
        System.out.println(myES.submit(() -> {
        }).get()); //1
        System.out.println(myES.submit(() -> {
        }, "AAA").get()); //1
        System.out.println(myES.submit(() -> 100).get());//2
        //myES.awaitTermination(10000, TimeUnit.MILLISECONDS);
        myES.shutdown();
    }
}

class Q16 {
    public static void main(String[] args) {
        StringBuilder sb = new StringBuilder("");
        System.out.println(sb);
        sb.append((String) null, 0, 1);
        System.out.println(sb);
    }
}

//
class Q17Super {
    static {
        System.out.println("Q17Super");
    }

    static String ID = "QBANK";
}

class Q17Sub extends test3.Q17Super {
    static {
        System.out.print("In Sub");
    }
}

class Q17Test {
    static {
        System.out.println("Q17Test");
    }

    public static void main(String[] args) {
        System.out.println(test3.Q17Sub.ID);
    }
}

//

class Q20TestClass {
    public static void main(String args[]) {
        test3.Q20B b = new test3.Q20B();
        test3.Q20A a = b;
        if (a instanceof test3.Q20B b1) b1.b();
        if (a instanceof test3.Q20C c1) c1.c();
        if (a instanceof test3.Q20D d1) d1.d();
        //if (b instanceof Q20A d1) d1.d();
        //if (b instanceof Q20B d1) d1.d();
        if (b instanceof test3.Q20C d1) d1.a();
        if ((String) null instanceof String) System.out.println("String null");
        ;
        if ("" instanceof String) System.out.println("String \"\"");
        ;
    }
}

class Q20A {
    void a() {
        System.out.println("a");
    }
}

class Q20B extends test3.Q20A {
    void b() {
        System.out.println("b");
    }
}

class Q20C extends test3.Q20B {
    void c() {
        System.out.println("c");
    }
}

class Q20D extends test3.Q20C {
    void d() {
        System.out.println("d");
    }
}

//
class Q25MyCache {
    private CopyOnWriteArrayList<String> cal = new CopyOnWriteArrayList<>();

    public void addData(List<String> list) {
        cal.addAll(list);
    }

    public Iterator getIterator() {
        //List<?> list = new ArrayList<Object>(Arrays.asList("names"));
        return cal.iterator();
    }
}

//

class Q29TestClass {
    public static void main(String[] args) throws Exception {
        List list = new ArrayList();
        list.add("val1"); //1
        list.add(1, "val3"); //3
        list.add(2, "val2"); //2
        System.out.println(list);
    }
}
//

abstract class Q31Base {
    abstract int power();
}

class Q31A extends test3.Q31Base {
    @Override
    int power() {
        return 0;
    }
}

class Q31TestClass {
    static void processBase(test3.Q31Base base) {
        test3.Q31A a = null;
        if (base instanceof test3.Q31A k) {
            a = new test3.Q31A();
            System.out.println(a.power());
        }
        System.out.println("a is " + a);
    }

    public static void main(String args[]) {
        processBase(new test3.Q31A());
    }
}

class Q33 {
    public static void main(String[] args) {
        Deque<Integer> d = new ArrayDeque<>();
        d.push(1);
        d.push(2);
        d.push(3);
        System.out.println(d);
        System.out.println(d.pop());
        System.out.println(d.pollFirst());
        System.out.println(d.poll());
        System.out.println(d.pollLast());
    }
}

//

class Q35Book {
    private int id;
    private String title;
    private String genre;
    private String author;

    public Q35Book(String title, String genre, String author) {
        this.title = title;
        this.genre = genre;
        this.author = author;
    }

    public static class Q35BookFilter {
        public static boolean isFiction(test3.Q35Book b) {
            return b.getGenre().equals("fiction");
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}

class Q35 {
    public static void main(String[] args) {
        List<test3.Q35Book> books = Arrays.asList(
                new test3.Q35Book("30 Days", "fiction", "K Larsen"),
                new test3.Q35Book("Fast Food Nation", "non-fiction", "Eric Schlosser"),
                new test3.Q35Book("Wired", "fiction", "D Richards"));
        books.stream()
                .filter(test3.Q35Book.Q35BookFilter::isFiction) //LINE 10
                .filter((test3.Q35Book b) -> test3.Q35Book.Q35BookFilter.isFiction(b)) //LINE 10
                .forEach((test3.Q35Book b) -> System.out.print(b.getTitle() + ", "));
    }
}

//

class Q38Book {
    private String title;
    private String genre;

    public Q38Book(String title, String genre) {
        this.title = title;
        this.genre = genre;
    }

    public static void main(String[] args) {
        List<test3.Q38Book> books = new ArrayList<>(List.of(new test3.Q38Book("A", "B"), new test3.Q38Book("A", "A")));
        Comparator<test3.Q38Book> c1 = (b1, b2) -> b1.genre.compareTo(b2.genre); //1
        books.stream().sorted(c1.thenComparing(b -> b.title)).forEach(System.out::println); //2
        System.out.println(books);
    }
}

class Q42 {
    public static void main(String[] args) {
        int x = 3;
        //while (false) { x=3; } //UNREACHABLE
        //for( int i = 0; false; i++) x = 3; //UNREACHABLE

        if (false) {
            x = 3;
        }
        do {
            x = 3;
        } while (false);
        for (var i = 0; i < 0; i++) x = 3;
        //final boolean b = false;
        boolean b = false;
        for (int i = 0; b; i++) {
            x = 3;
        }
        ;//Compiles fine
    }
}

//


class Q43Bond  // does not implement Serializable
{
    String ticker = "bac";
    double coupon = 8.3;
    java.time.LocalDate maturity = LocalDate.now();
}

class Q43Portfolio implements Serializable {
    String accountName;
    transient test3.Q43Bond[] bonds = new test3.Q43Bond[]{}; // must be transient
    //because Bond class does not implement Serializable

    private void writeObject(ObjectOutputStream os) throws Exception {
        os.defaultWriteObject();
        os.writeInt(bonds.length);
        //write the state of bond objects
        for (int i = 0; i < bonds.length; i++) {
            os.writeObject(bonds[i].ticker);
            os.writeDouble(bonds[i].coupon);
            os.writeObject(bonds[i].maturity);
        }
    }

    private void readObject(ObjectInputStream os) throws Exception {
        os.defaultReadObject();
        int n = os.readInt();
        this.bonds = new test3.Q43Bond[n];
        //read the state of bond objects.
        for (int i = 0; i < bonds.length; i++) {
            bonds[i] = new test3.Q43Bond();
            bonds[i].ticker = (String) os.readObject();
            bonds[i].coupon = os.readDouble();
            bonds[i].maturity = (java.time.LocalDate) os.readObject();
        }
    }
}

//

class Q44 {
    enum E {
        E, EE, EEE;
        //public E(){}
    }

    ;

    //enum A extends java.lang.Enum {}
    public static void main(String[] args) {
        enum E {E, EE, EEE}
        ;
    }
}
//

class Q46LowBalanceException extends test3.Q46WithdrawalException {  //1
    public Q46LowBalanceException(String msg) {
        super(msg);
    }
}

class Q46WithdrawalException extends Exception { //2
    public Q46WithdrawalException(String msg) {
        super(msg);
    }
}

class Q46Account {
    double balance;

    public void withdraw(double amount) throws test3.Q46WithdrawalException {
        try {
            throw new RuntimeException("Not Implemented");
        } catch (Exception e) {
            throw new test3.Q46LowBalanceException(e.getMessage());
        }
    }

    public static void main(String[] args) {
        try {
            test3.Q46Account a = new test3.Q46Account();
            a.withdraw(100.0);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}

//

class Q48Testclass {
    public static void main(String[] args) {
        NavigableMap<String, String> mymap = new TreeMap<String, String>();
        mymap.put("a", "apple");
        mymap.put("b", "boy");
        mymap.put("c", "cat");
        mymap.put("aa", "apple1");
        mymap.put("bb", "boy1");
        mymap.put("cc", "cat1");
        System.out.println(mymap);
        System.out.println(mymap.pollLastEntry()); //LINE 1
        System.out.println(mymap.pollFirstEntry()); //LINE 2
        NavigableMap<String, String> tailmap = mymap.tailMap("bb", false); //LINE 3
        System.out.println(mymap);
        System.out.println(tailmap);
        System.out.println(tailmap.pollFirstEntry()); //LINE 4
        System.out.println(mymap);
        System.out.println(mymap.size()); //LINE 5
    }
}

//

class Q50Transfer implements Runnable {
    test3.Q50Account from, to;
    double amount;

    public Q50Transfer(test3.Q50Account from, test3.Q50Account to, double amount) {
        this.from = from;
        this.to = to;
        this.amount = amount;
    }

    public void run() {
        synchronized (from) {
            from.setBalance(from.getBalance() - amount);
            synchronized (to) {
                to.setBalance(to.getBalance() + amount);
                System.out.println(Thread.currentThread().getName());
            }
            System.out.println(Thread.currentThread().getName());
        }
    }
}

class Q50Account {
    private String id;
    private double balance;     //constructor and accessor methods not shown,

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public Q50Account(String id, double balance) {
        this.id = id;
        this.balance = balance;
    }

    public static void main(String[] args) {
        var es = Executors.newCachedThreadPool();
        var a1 = new test3.Q50Account("A1", 1000);
        var a2 = new test3.Q50Account("A1", 1000);
        es.submit(new test3.Q50Transfer(a1, a2, 200));
        es.submit(new test3.Q50Transfer(a2, a1, 300));
    }
}

//

class Q51TestClass {
    public static void main(String args[]) {
        int i = 0;
        loop:
        // 1
        {
            System.out.println("Loop Lable line");
            try {
                for (; true; i++) {
                    if (i > 5) break loop;       // 2
                }
            } catch (Exception e) {
                System.out.println("Exception in loop.");
            } finally {
                System.out.println("In Finally");      // 3
            }
        }
    }
}
//

class Q52 {
    public static void main(String[] args) {
        byte starting = 3;
        short firstValue = 5;
        int secondValue = 7;
        int functionValue = (int) (starting / 2 + firstValue / 2 + (int) firstValue / 3) + secondValue / 2;
        System.out.println(functionValue);

        byte b1 = 1;
        byte b2 = 2;
        byte b = (byte) (b1 + b2);
    }
}

interface Q53 {

    sealed class X {
    }

    final class Y extends X {
    }
}
//

interface Q54Processor {
    Iterable process();
}

interface Q54ItemProcessor extends test3.Q54Processor {
    Collection process();
}

interface Q54WordProcessor extends test3.Q54Processor {
    //String process();
}

interface Q54GenericProcessor extends test3.Q54ItemProcessor, test3.Q54WordProcessor {
}
*/