package test13;

import javax.management.AttributeList;
import javax.management.relation.RoleList;
import javax.management.relation.RoleUnresolvedList;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;

public class Test13 {
}

interface Q5I {
}

enum Q5EnumA implements Q5I, Serializable {A, AA, AAA}

class Q5TestClass {
    public enum EnumB {
        B, BB, BBB;

        //public Object clone() {return B;}
    }

    public static enum EnumC {C, CC, CCC}

    ;

    //public static enum EnumD extends EnumC {DDD};
    public Q5TestClass() {
        //System.out.println(EnumC.CC.index());
    }

    public static void main(String[] args) {
        System.out.println(EnumC.valueOf("ccc"));
        System.out.println(EnumC.CCC.name());
        System.out.println(EnumC.CCC.ordinal());
    }
}

class Q8Widget {
    static int MAX;     //1
    static final String CLASS_GUID;   // 2
    final int CLASS_G;

    static {
        MAX = 111;
        CLASS_GUID = "XYZ123";
    }

    Q8Widget() {
        MAX = 111;
        CLASS_G = 0;
        // CLASS_GUID = "XYZ123";
    }

    Q8Widget(int k) {
        MAX = 111;
        CLASS_G = 0;
        //CLASS_GUID = "XYZ123";
    }
}

class Q15Test extends Thread {
    static Object obj1 = new Object();
    static Object obj2 = new Object();

    public void m1() {
        synchronized (obj1) {
            System.out.print("1 ");
            synchronized (obj2) {
                System.out.println("2");
            }
        }
    }

    public void m2() {
        synchronized (obj2) {
            System.out.print("2 ");
            synchronized (obj1) {
                System.out.println("1");
            }
        }
    }

    public void run() {
        m1();
        m2();
    }

    public static void main(String[] args) {
        new Q15Test().start();
        new Q15Test().start();
    }
}

interface Q16Eatable {
    int types = 10;
}

class Q16Food implements Q16Eatable {
    public static int types = 20;
}

class Q16Fruit extends Q16Food implements Q16Eatable {
    public static void main(String[] args) {
        //Q16Fruit.types = 30;
        System.out.println(Q16Eatable.types);
    }
}

interface Q17Classic {
    int version = 1;

    public void read();
}

class Q17MediaReader implements Q17Classic {
    int version = 2;

    public void read() {
        System.out.println(version);
        //System.out.println((Q17Classic)version);
        System.out.println(((Q17Classic) this).version);
        //System.out.println(this.Q17Classic.version);
        System.out.println(Q17Classic.version);
        //System.out.println(Q17MediaReader.version);
    }
}

class Q17ReaderTest {
    public static void main(String[] args) {
        Q17MediaReader mr = new Q17MediaReader();
        mr.read();
    }
}

class Q19TestClass {
    public static void main(String args[]) {
        Q19A a = new Q19B();
        if (a instanceof Q19B b) {
            b.b();
        }
        //System.out.println(b);
    }
}

class Q19A {
    void a() {
        System.out.println("a");
    }
}

class Q19B extends Q19A {
    void b() {
        System.out.println("b");
    }
}
//

class Q22 {
    public static void main(String[] args) {
        System.out.println(LocalDate.now().with(TemporalAdjusters.next(DayOfWeek.SUNDAY)));
        System.out.println(LocalDate.now().with(TemporalAdjusters.nextOrSame(DayOfWeek.SUNDAY)));
        System.out.println(TemporalAdjusters.next(DayOfWeek.SUNDAY).adjustInto(LocalDate.now()));
        System.out.println(TemporalAdjusters.lastDayOfMonth().adjustInto(LocalDate.now()));
    }
}

class Q23 {
    public static void main(String[] args) {
        AttributeList al;
        RoleList rl;
        RoleUnresolvedList rul;
    }
}

class Q25 {
    public static void main(String[] a) throws Exception {
        var p1 = Paths.get("/Users/admin/IdeaProjects/ocajp829/tests/enthuware/test13/test1.txt");
        var p2 = p1.resolve("text2.txt");
        System.out.println(p2);
        p2 = p1.relativize(Path.of("/Users/admin/IdeaProjects/ocajp829/tests/enthuware/test13/test2.txt"));
        System.out.println(p2);
        p2 = p1.resolveSibling("text2.txt");
        System.out.println(p2);
        //var bw = new BufferedWriter(new FileWriter(p2.toFile()));
        //bw.write("hello");
        //bw.close();
    }
}
//

class Q27IOTest {
    public static void main(String[] args) {
        try (BufferedReader bfr = new BufferedReader(new FileReader("tests/enthuware/test13/Test13.java"))) {
            String line = null;
            while ((line = bfr.readLine()) != null) {
                System.out.println(line);
            }
        } catch (/*NoSuchFileException |*/ IOException /*| AccessDeniedException*/ e) {
            e.printStackTrace();
        }
    }
}

//

abstract class Q28AmazingClass {
    void amazingMethod(Collection c) {
        System.out.println("Got collection");
    }
}

class Q28SpecialAmazingClass extends Q28AmazingClass {
    void amazingMethod(List l) {
        System.out.println("Got list");
    }

    public static void main(String[] args) {
        List<String> al = new ArrayList<String>();
        Collection<String> c = al;
        Q28SpecialAmazingClass ac = new Q28SpecialAmazingClass();
        ac.amazingMethod(c);
    }
}

//

class Q30A {
    byte getStatusCode(Object obj) throws NullPointerException {
        if (obj != null) return 127;
        else return -1;
    }
}

class Q30B extends Q30A {
}
//

class Q31ForSwitch {
    public static void main(String args[]) {
        char i;
        LOOP:
        for (i = 0; i < 5; i++) {
            switch (i++) {
                case '0':
                    System.out.println("A");
                case 1:
                    System.out.println("B");
                    break LOOP;
                case 2:
                    System.out.println("C");
                    break;
                case 3:
                    System.out.println("D");
                    break;
                case 4:
                    System.out.println("E");
                case 'E':
                    System.out.println("F");
            }
        }
    }
}

class Q32 {
    public static void main(String[] args) {
        new Q32().outputText(new PrintWriter(System.out), "aaa");
    }

    public void outputText(PrintWriter pw, String text) {
        pw.write(text);
        if (pw.checkError()) System.out.println("exception in writing");
    }

    public void outputText2(PrintWriter pw, String text) {
        pw.printf(text).print("success");
    }
}
//

class Q33PathTest {
    static Path p1 = Paths.get("/Users/admin/IdeaProjects/ocajp829/tests/enthuware/test13/Test13.java");

    public static void main(String[] args) {
        System.out.println(p1.subpath(0, 2));
    }
}

class Q34 {
    public static void main(String[] args) {
        Path p = Paths.get("tests/enthuware/test13/a");
        try {
            var b = Files.deleteIfExists(p);
            System.out.println(b);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

class Q41Student {
    public static enum Grade {A, B, C, D, F}

    private String name;
    private Grade grade;

    public Q41Student(String name, Grade grade) {
        this.name = name;
        this.grade = grade;
    }

    public String getName() {
        return name;
    }

    public Grade getGrade() {
        return grade;
    }

    public String toString() {
        return name + ":" + grade;
    }

    public static void main(String[] args) {
        List<Q41Student> ls = Arrays.asList(new Q41Student("S1", Q41Student.Grade.A), new Q41Student("S2", Q41Student.Grade.A), new Q41Student("S3", Q41Student.Grade.C));
        Map<Grade, Map<String, List<Q41Student>>> grouping =
                ls.stream().collect(
                        Collectors.groupingBy(Q41Student::getGrade,
                                Collectors.groupingBy(Q41Student::getName, Collectors.toList())));
        System.out.println(grouping);

        Map<Q41Student.Grade, List<String>> grouping2 = ls.stream().collect(
                Collectors.groupingBy(Q41Student::getGrade,
                        Collectors.mapping(Q41Student::getName, Collectors.toList())));
        System.out.println(grouping2);
    }
}

class Q42Holder {
    int value = 1;
    Q42Holder link;

    public Q42Holder(int val) {
        this.value = val;
    }

    public static void main(String[] args) {
        final var a = new Q42Holder(5);
        var b = new Q42Holder(10);
        a.link = b;
        b.link = setIt(a, b);
        System.out.println(a.link.value + " " + b.link.value);
    }

    public static Q42Holder setIt(final Q42Holder x, final Q42Holder y) {
        x.link = y.link;
        return x;
    }
}

class Q43 {
    public static void main(String[] args) {
        new Random().doubles(10).forEach(System.out::print);

        System.out.println();
        Random r = new Random();
        DoubleStream rDoubles = r.doubles().limit(10);
        rDoubles.forEach(System.out::print);

        System.out.println();
        DoubleStream.generate(() -> r.nextDouble()).limit(10).forEach(System.out::print);

        System.out.println();
        DoubleStream.generate(r::nextGaussian).limit(10).forEach(System.out::print);
    }
}

class Q46 {
    public static void main(String[] args) {
        String[] p = {"1", "2", "3"};
        List<?> list2 = new ArrayList<>(Arrays.asList(p));
        //System.out.println(list2.add("$"));
    }
}

//

class Q48 {
    public static void main(String[] args) {
        LocalDate d = LocalDate.now();
        Locale loc = new Locale("fr", "FR");

        DateTimeFormatter df = DateTimeFormatter.ofPattern("dd MMM yyyy", loc);
        System.out.println(df.format(d));

        //System.out.println(df.format(d, loc));    }
}}


