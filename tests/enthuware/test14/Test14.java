package test14;

import java.io.*;
import java.sql.*;
import java.text.NumberFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiFunction;
import java.util.function.DoubleFunction;
import java.util.function.Function;
import java.util.stream.Stream;

public class Test14 {
}

class Q2 {
    public static void main(String[] args) {
        String a;
        String b = "B";
        switch (b) {
            case "C" -> a = "1";
            case "B" -> a = "10";
            default -> a = "";
        }
        System.out.println(a);
    }
}
//

class Q4Device implements AutoCloseable {
    String header = null;

    public void open() {
        header = "OPENED";
        System.out.println("Device Opened");
    }

    public String read() throws IOException {
        throw new IOException("Unknown");
    }

    public void writeHeader(String str) throws IOException {
        System.out.println("Writing : " + str);
        header = str;
    }

    public void close() {
        header = null;
        System.out.println("Device closed");
    }

    public static void testDevice() {
        try (Q4Device d = new Q4Device()) {
            d.open();
            d.read();
            d.writeHeader("TEST");
            d.close();
        } catch (IOException e) {
            System.out.println("Got Exception");
        }
    }

    public static void main(String[] args) {
        Q4Device.testDevice();
    }
}

class Q6 {
    public static void main(String[] args) {
        List<Double> dList = Arrays.asList(10.0, 12.0);
        DoubleFunction df = x -> x + 10;
        dList.stream().forEach((a) -> System.out.println(df.apply(a)));
        dList.stream().forEach(d -> System.out.println(d));
    }
}

class Q7StringFromChar {
    public static void main(String[] args) {
        String myStr = "good";
        char[] myCharArr = {'g', 'o', 'o', 'd'};
        String newStr = null;
        newStr += 2;
        System.out.println(newStr);
        for (char ch : myCharArr) {
            newStr = newStr + ch;
        }
        System.out.println(newStr);
        System.out.println((newStr == myStr) + " " + (newStr.equals(myStr)));
    }
}

class Q12 {
    public static void main(String[] args) throws SQLException {

        //Statement stmt = null;
        Connection c = DriverManager.getConnection("jdbc:derby://localhost:1527/sample", "app", "app");
        try (Statement stmt = c.createStatement()) {
            ResultSet rs = stmt.executeQuery("select * from STUDENT");
            while (rs.next()) {
                System.out.println(rs.getString(0));
            }
        } catch (SQLException e) {
            System.out.println("Exception ");
        }
    }
}

class Q14 {
    public static void main(String[] args) {
        List<Integer> lon = List.of(1, 2, 3, 4, 5, 6, 7);
        System.out.println(lon.parallelStream().reduce(5, Integer::sum));
        System.out.println(lon.parallelStream().reduce(0, Integer::sum) + 5);
        System.out.println(lon.parallelStream().reduce(Integer::sum).orElse(5) + 5);
        System.out.println(lon.stream().reduce(0, Integer::sum) + 5);
        System.out.println(lon.stream().reduce(5, (a, b) -> a + b));
    }
}

class Q16TestClass {
    public static void main(String args[]) {
        StringBuilder sb = new StringBuilder("12345678");
        sb.setLength(5);
        sb.setLength(50);
        System.out.println(sb.length());
        System.out.println(sb + "A");
    }
}

class Q17TestClass {
    public static void main(String args[]) {
        int i = 1;
        int[] iArr = {1};
        incr(i);
        incr(iArr);
        System.out.println("i = " + i + "  iArr[0] = " + iArr[0]);
    }

    public static void incr(int n) {
        n++;
    }

    public static void incr(int[] n) {
        n[0]++;
    }
}

class Q18 {
    public static void main(String[] args) {
        double x = 12345.123;
        String str = NumberFormat.getInstance().format(x);
        System.out.println(str);
    }
}

class Q19 {
    public static void main(String[] args) throws IOException {
        var s = "hello";
        byte i = 100;
        var fos = new FileOutputStream("/Users/admin/IdeaProjects/ocajp829/tests/enthuware/test14/Test.txt");
        var dos = new DataOutputStream(fos);
        dos.writeUTF("Av\n");
        dos.writeChars("XYZ");
        dos.flush();
        dos.close();
        fos.close();
        var dis = new DataInputStream(new FileInputStream("/Users/admin/IdeaProjects/ocajp829/tests/enthuware/test14/Test.txt"));
        System.out.println(dis.readChar());
        System.out.println(dis.readByte());
        System.out.println(dis.readUTF());
    }
}

class Q20 {
    public static void main(String[] args) {
        AtomicInteger ai = new AtomicInteger();
        Stream<String> stream = Stream.of("old", "king", "cole", "was", "a", "merry", "old", "soul").parallel();
        stream.filter(e -> {
            ai.incrementAndGet();
            return e.contains("o");
        }).allMatch(x -> x.indexOf("o") > 0);
        System.out.println("AI = " + ai);
    }
}


class Q33Base {
    public <T extends CharSequence> Collection<String> transform(Collection<T> list) {
        return new ArrayList<String>();
    }
}

class Q33Derived extends Q33Base {
    //public Collection<String> transform(Collection list) {
    //  return new HashSet<String>();
    //}

    //public <T extends CharSequence> Collection transform(Collection<T> list) {
    //  return new HashSet<>();
    //}

    //public <T extends CharSequence> List<T> transform(Collection<T> list) {
    //  return new ArrayList<T>();
    //}

    //public <T extends CharSequence> Collection<T> transform(List<T> list) {
    //  return new HashSet<T>();
    //}

    //public <T super String> Collection<T> transform(List<String> list) {
    //  return new HashSet<T>();
    //}

    //public Collection<CharSequence> transform(Collection<CharSequence> list) {
    //  return new HashSet<CharSequence>();
    //}
}

class Q34 {
    public static void main(String[] args) {
        FileInputStream tempFis = null;
        try (FileInputStream fis = new FileInputStream("c:\\temp\\test.text")) {
            System.out.println(fis);
            tempFis = fis;
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                tempFis.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}

class Q38 {
    public static void main(String[] args) {
        System.out.println(new Q38().parseFloat("0"));
    }

    public float parseFloat(String s) {
        float f = 0.0f;
        try {
            f = Float.valueOf(s).floatValue();
            return f;
        } catch (NumberFormatException nfe) {
            System.out.println("Invalid input " + s);
            f = Float.NaN;
            return f;
        } finally {
            System.out.println("finally");
            throw new RuntimeException();
        }

        //return f;
    }
}

class Q51TestClass {
    public static void main(String[] args) {
        int i = 0;
        String s = "";
        //s = null;
        if ((s != null) | (i == s.length())) {
        }
        System.out.println("A");
        //s = null;
        if ((s == null) | (i == s.length())) {
        }
        System.out.println("B");
        //s = null;
        if ((s != null) || (i == s.length())) {
        }
        System.out.println("C");
        s = null;
        if ((s == null) || (i == s.length())) {
        }
        System.out.println("D");
    }
}

class Q52 {
    public static void main(String[] args) {
        BiFunction<Integer, Double, Double> bf = (a, b) -> a / b;
        System.out.println(bf.apply(1, 2.));


    }

    public BiFunction<Integer, String, Double> getBF() {
        return (a, b) -> (double) (a / b.length());
    }


    class Transformer<T> {
        public BiFunction<T, T, T> getMethod() {
            return (a, b) -> null;
        }

        BiFunction<T, T, T> tS = getMethod();
        Transformer<Integer> tI;
    }

    <A, B, C> BiFunction<A, B, C> predicate(Function<A, B> f) {
        return null;
    }
}

class Q55 {
    public static void main(String[] args) {
        double daaa[][][] = new double[3][][];
        var d = 100.0;
        double[][] daa = new double[1][1];
        //daa[1][1] = d;

        System.out.println(Arrays.deepToString(daaa));
        System.out.println(Arrays.deepToString(daa));

        //daaa[0] = d;
        //daaa[0] = daa[0];

        daaa[0] = daa;
        System.out.println(Arrays.deepToString(daaa));

        daa = daaa[0];
        System.out.println(Arrays.deepToString(daa));

        double[] newd = daa[0].clone();
        System.out.println(Arrays.toString(newd));
    }
}