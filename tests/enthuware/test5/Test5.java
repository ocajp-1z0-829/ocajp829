package test5;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.time.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Consumer;

import static java.time.DayOfWeek.FRIDAY;

public class Test5 {
}

//
sealed interface Q2Cacheable permits Q2Value, Q2Result {
    default void clear() {
        System.out.println("clearing cache...");
    }
}
//non-sealed interface Q2Value extends Q2Cacheable { }

//non-sealed abstract class Q2Result implements Q2Cacheable {}

abstract non-sealed interface Q2Value extends Q2Cacheable {
}

sealed abstract class Q2Result implements Q2Cacheable permits Q2IntResult {
}

final class Q2IntResult extends Q2Result {
}
//

class Q4 {
    public static void main(String[] args) throws IOException, FileNotFoundException {
        char[] buf = new char[5];
        try (FileReader fr = new FileReader("tests/enthuware/test5/test1.txt");
             FileWriter fw = new FileWriter("tests/enthuware/test5/test2.txt")) {
            //while (fr.read(buf) != -1) {
            //  fw.write(buf);
            //}
            int count = 0;
            while ((count = fr.read(buf)) != -1) {
                fw.write(buf, 0, count); //use this instead of fw.write(buf);
            }
        }
    }
}

class Q5 {
    public static void main(String[] args) throws SQLException {
        try (Connection c = /*ds.getConnection();*/ null; //assume that ds refers to a DataSource
             PreparedStatement selectPS = c.prepareStatement("select TID, SUBJECT from TEACHER where NAME = ?");
             PreparedStatement insertPS = c.prepareStatement("insert into TEACHER(NAME, TID) VALUES (?, ?)");) {
            selectPS.setString(1, "Ally");
            try (ResultSet rs = selectPS.executeQuery();) {
                while (rs.next()) {
                    insertPS.setObject(1, rs.getObject(2), JDBCType.VARCHAR);
                    insertPS.setObject(2, rs.getObject(1), JDBCType.INTEGER);
                    insertPS.execute(); //1
                }
            }
        }
    }
}

class Q6 {
    public static void main(String[] args) {
        Path p1 = Paths.get("/../temp/test.txt");
        System.out.println(p1.normalize().toUri());
    }
}

class Q8 {
    static List<String> sList = new ArrayList<String>();

    static public void main(String... a) {
        var s = sList.remove(0);
    }
}

class Q13 {
    public static void main(String[] args) {
        Period p = Period.between(LocalDate.now(), LocalDate.of(2022, Month.SEPTEMBER, 1));
        System.out.println(p);
        Duration d = Duration.between(LocalDateTime.now(), LocalDateTime.of(2022, Month.SEPTEMBER, 2, 10, 10));
        System.out.println(d);
    }
}

class Q14 {
    static int someMethod() {
        return 100;
    }

    static class MyWorker {
        Runnable r;

        public MyWorker(Runnable r) {
            this.r = r;
        }
    }

    public static void main(String[] args) {
        //INSERT CODE HERE
        Runnable r = () -> System.out.println("Hello");
        r = () -> {
            someMethod();
        };
        r = () -> someMethod();
        MyWorker w = new MyWorker(r);
        w.r.run();
    }
}

class Q15 {
    public static void main(String[] args) {
        var myfile = Paths.get("test.txt");
        try (var bfr = Files.newBufferedReader(myfile, Charset.forName("US-ASCII"))) {
            String line = null;
            while ((line = bfr.readLine()) != null) {
                System.out.println(line);
            }
        } catch (FileNotFoundException e) {
            System.out.println(e);
            e.toString();
        } catch (NoSuchFileException e) {
            System.out.println(e);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}

class Q19 {
    public static void main(String[] args) {

        Path p1 = Paths.get("photos/../beaches/./calangute/a.txt");
        Path p5 = Paths.get("/photos/../beaches/./calangute/a.txt");
        System.out.println(p1.relativize(p5));
        System.out.println(p1);
        Path p2 = p1.normalize();
        System.out.println(p2);
        Path p3 = p1.relativize(p2);
        System.out.println(p3);
        Path p4 = p2.relativize(p1);
        System.out.println(p4.toString().length());
        System.out.println(p1.getNameCount() + " " + p2.getNameCount() + " " + p3.getNameCount() + " " + p4.getNameCount());
    }
}

class Q21 {
    public static void main(String[] args) {
        //String valueStr = (String) (63 + Integer.valueOf(10));
    }
}

class Q24 {
    public static void main(String[] args) {
        var day = LocalDate.now().with(FRIDAY).getDayOfWeek();
        switch (day) {
            case MONDAY:
                TUESDAY:
                WEDNESDAY:
                THURSDAY:
                FRIDAY:
                System.out.println("working");
            case SATURDAY:
                SUNDAY:
                System.out.println("off");
        }
    }
}

class Q27 {
    public static void main(String[] args) {
        //var _= 10;
        for (var x : System.getProperties().entrySet()) {
            var m = x.getKey();
            x.getValue();
            System.out.println();
            System.out.println(m);
        }
    }
}


class Q28MyException extends Exception {
    private final int code;

    public Q28MyException(int code, Throwable actualEx) {
        super(actualEx);
        this.code = code;
    }

    public Q28MyException(int code, String message, Throwable actualEx) {
        super(message, actualEx);//1    
        this.code = code;
    }

    @Override
    public String getMessage() {
        return String.format("Exception - Code=%d, Msg=%s, OrigMsg=%s", code, super.getMessage(), this.getCause().getMessage()); //2  
    }
}

class Q28TestClass {
    public static void main(String[] args) {
        try {
            throw new Q28MyException(404, "NOT FOUND", new IOException("FILE UNREADABLE")); //3
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}

class Q29 {
    public static void main(String[] args) throws IOException {
        var bfr = new BufferedReader(new FileReader("c:\\temp\\pathtest\\a.java"));
        var bfw = new BufferedWriter(new FileWriter("c:\\temp\\pathtest\\b.java"));
        String line = null;
        while ((line = bfr.readLine()) != null) {
            bfw.append(line);
        }
    }
}


class Q34 {
    public static void main(String[] args) {
        Consumer x = System.out::println;
        x = (m) -> {
        };
        x = (Object msg) -> {
            System.out.println(msg);
        };
        List.of(args).forEach(x);
    }
}


class Q38Nesting {
    public static void main(String args[]) {
        B.C obj = new B().new C();
    }
}

class A {
    char c;

    A(char c) {
        this.c = c;
    }
}

class B extends A {
    char c = 'a';

    B() {
        super('b');
    }

    class C extends A {
        char c = 'c';

        C() {
            super('d');
            System.out.println(B.this.c);
            System.out.println(C.this.c);
            System.out.println(super.c);
        }
    }
}

class Q39 {
    public static void main(String[] args) {
        new Thread(() -> System.out.println());
    }
}

class Q41 {
    private Map<String, Integer> marksObtained = new HashMap<>();
    private ReadWriteLock lock = new ReentrantReadWriteLock();

    public void setMarksInSubject(String subject, Integer marks) {
        lock.writeLock().lock(); //1
        try {
            marksObtained.put(subject, marks);
        } finally {
            lock.writeLock().unlock(); //2
        }
    }

    public double getAverageMarks() {
        lock.readLock().lock(); //3
        double sum = 0.0;
        try {
            for (Integer mark : marksObtained.values()) {
                sum = sum + mark;
            }
            return sum / marksObtained.size();
        } finally {
            lock.readLock().unlock();//4
        }
    }

    public static void main(String[] args) {

        //final Student s = new Student();

        //create one thread that keeps adding marks
        new Thread() {
            public void run() {
                int x = 0;
                while (true) {
                    int m = (int) (Math.random() * 100);
                    //s.setMarksInSubject("Sub " + x, m);
                    x++;
                }
            }
        }.start();

        //create 5 threads that get average marks
        for (int i = 0; i < 5; i++) {
            new Thread() {
                public void run() {
                    while (true) {
                        //double av = s.getAverageMarks();
                        //System.out.println(av);
                    }
                }
            }.start();
        }
    }
}


class Q43B /*extends test5.a.A*/ {
    Q43B() {
    }

    public void print() {
        System.out.println("B");
    }

    public static void main(String[] args) {
        new B();
    }
}

class Q44 {
    public static void myMethod(int x) throws ClassNotFoundException, NoSuchFieldException //Specify throws clause here
    {
        try {
            if (x == 0) {
                throw new ClassNotFoundException();
            } else throw new NoSuchFieldException();
        } catch (RuntimeException e) {
            throw e;
        }
    }
}

class Q49{
    public void main(String[] args) {
        ((Q49)(this)).toString();
        var cin = new Comparable<Integer>() {
            public int compareTo(Integer i1) {
                return "100".compareTo("" + i1);
            }
        };
        System.out.println(cin.compareTo(100));
    }

}

class Q53 {
    public void method1(int i, double d) {
        int j = (i * 30 - 2) / 100;

        POINT1:
        for (; j < 10; j++) {
            var flag = false;
            while (!flag) {
                if (d > 0.5) break POINT1;
            }
        }
        while (j > 0) {
            System.out.println(j--);
            if (j == 4) break /*POINT1*/;
        }
    }

    public static void main(String[] args) {
        Q53 q53 = new Q53();
        q53.method1(10, 20);
    }
}

