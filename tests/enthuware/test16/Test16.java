package test16;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Test16 {
}

class Q2 {
    public static void main(String[] args) throws IOException {
        Path p1 = Paths.get("/Users/admin/IdeaProjects/ocajp829/tests/enthuware/test16/source");
        Path p2 = Paths.get("/Users/admin/IdeaProjects/ocajp829/tests/enthuware/test16/dest/source");
        Files.move(p1, p2, StandardCopyOption.REPLACE_EXISTING);
    }
}

class Q3 {
    public static void main(String[] args) {
        IntStream is1 = IntStream.range(0, 5);
        OptionalDouble x = is1.average();
        System.out.println(x); //3
    }
}

class Q6 {
    public static void main(String[] args) {
        short s = 10;
        char c = (char) s;
        s = (short) c;
    }
}

class Q8 {
    static String[] sa = {"a", "aa", "aaa", "aaaa"};

    static {
        Arrays.sort(sa);
    }

    public static void main(String[] args) {
        String search = "";
        if (args.length != 0) search = args[0];
        System.out.println(Arrays.binarySearch(sa, search));
    }
}

class Q25A {
    public int i = 10;
    private int j = 20;
}

class Q25B extends Q25A {
    private int i = 30;
    public int k = 40;
}

class Q25C extends Q25B {
}

class Q25 {
    public static void main(String args[]) {
        Q25C c = new Q25C();
        //System.out.println(c.i);
        //System.out.println(c.j);
        System.out.println(c.k);
    }
}

class Q27MyException extends Exception {
    private final int code;

    public Q27MyException(Throwable actualEx) {
        super(actualEx);
        code = 0;
    }

    public Q27MyException(int code, Throwable actualEx) {
        super(actualEx);
        this.code = code;
    }

    public Q27MyException(int code, String message, Throwable actualEx) {
        super(message, actualEx);
        this.code = code;
    }

    public String getMessage() {
        return String.format("Exception - Code=%d, Msg=%s, OrigMsg=%s", code, super.getMessage(), this.getCause().getMessage());
    }
}

class Q27 {
    public static void main(String[] args) {
        try {
            throw new Q27MyException(new IOException("NOT FOUND"));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}

class Q31 {
    public static void main(String[] args) {
        //var[ ] a[ ] = new int [5][4];
        //var[ ][ ] a = new int [5][4];
        //var a[] = new int [5][4];
    }
}

class Q34 {
    public static void main(String[] args) {
        var c = 0;
        JACK:
        while (c < 8) {
            JILL:
            {
                System.out.println(c);
                if (c > 3) break JILL;
                else c++;
            }
        }
    }
}
//

interface Q35I {
}

class Q35A implements Q35I {
}

class Q35B extends Q35A {
}

class Q35C extends Q35B {
    public static void main(String[] args) {
        Q35A a = new Q35A();
        Q35B b = new Q35B();
        a = (Q35B) (Q35I) b;
        b = (Q35B) (Q35I) a;
        //a = (Q35I) b;
        Q35I i = (Q35C) a;
    }
}

class Q36TestClass {
    public static void main(String[] args) throws IOException {
        final InputStream fis = new FileInputStream("/Users/admin/IdeaProjects/ocajp829/tests/enthuware/test16/test.txt");
        long l = 0;
        try (fis) {
            l = fis.read();
        } finally {
            // l = fis.read();
        }
        //l = fis.read();
        System.out.println((char) l);
    }
}

class Q37 {
    public static void main(String[] args) {
        Map hm = new ConcurrentHashMap();
        //hm.put(null, "asdf");
        //hm.put("aaa", null);
        hm = new HashMap();
        hm.put(null, "asdf");
        hm.put("aaa", null);
        List list = new ArrayList();
        list.add(null);
        list.add(null);
        list = new CopyOnWriteArrayList();
        list.add(null);
    }
}

class Q40 {
    public static void main(String[] args) {
        List<Integer> primes = Arrays.asList(2, 3, 5, 7, 11, 13, 17);
        Stream<Integer> primeStream = primes.stream();
        Predicate<Integer> test1 = k -> k < 10;
        //long count1 = primeStream.filter(test1).count();
        Predicate<Integer> test2 = k -> k > 10;
        //long count2 = primeStream.filter(test2).count();
        //System.out.println(count1 + " " + count2); //7   Identify correct statements.

        int[] arr = new int[]{10, 20, 30};
        Integer[] carsArray = new Integer[4];
        carsArray = primes.toArray(carsArray);
        long count3 = IntStream.of(primes.stream().mapToInt(i -> i).toArray()).filter(a -> a < 10).count();
        long count4 = IntStream.of(arr).filter(a -> a > 10).count();
        System.out.println(count3);
        System.out.println(count4);

        System.out.println(primeStream.collect(Collectors.partitioningBy(test1, Collectors.counting())));
        // .values().forEach(System.out::print);
    }
}

class Q42MyProcessor {
    int value;

    public Q42MyProcessor() {
        value = 10;
    }

    public Q42MyProcessor(int value) {
        this.value = value;
    }

    public void process() {
        System.out.println("Processing " + value);
    }

    public static void main(String[] args) {

        Supplier<Q42MyProcessor> supp = Q42MyProcessor::new;
        Q42MyProcessor mp = supp.get();
        mp.process();


        Function<Integer, Q42MyProcessor> f = Q42MyProcessor::new;
        Q42MyProcessor m = f.apply(10);
        m.process();
    }
}

class Q44 {
    public static void main(String[] args) {
        {
            // for (var i = 5; i = 0; i--) {}
        }
        {
            var j = 5;
            //  for (int i = 0, j+=5;i < j;i++){
            j--;
        }
    }

    {
        int i, j;
        //for (j = 10; i < j; j--) {
        //  i += 2;
        //}
    }

    {
        var i = 10;
        for (; i > 0; i--) {
        }
    }

    {
        for (int i = 0, j = 10; i < j; i++, --j) {
            ;
        }
    }

    {
        //for (var i = 0, j = 10; i < j; i++, --j) {;}
    }
}
//


class Q47OuterWorld {
    public InnerPeace i = new InnerPeace("none");

    class InnerPeace {
        private String reason = "none";

        InnerPeace(String reason) {
            this.reason = reason;
        }
    }

    public static void main(String[] args) {
        var ip = new Q47OuterWorld().new InnerPeace("yoga");
        var out = new Q47OuterWorld();
        System.out.println(out.i.reason);
    }
}
//

interface Q48I1 {
    void m1() throws java.io.IOException;
}

interface Q48I2 {
    void m1() throws Exception;
}

class Q48 implements Q48I1, Q48I2 {

    @Override
    public void m1() throws FileNotFoundException {
    }
}
//

class Q49Person implements Comparable<Q49Person>{
    private static int count = 0;
    private String id = "0";
    private String interest;
    public Q49Person(String interest) {
        this.interest = interest;
        this.id = "" + ++count;
    }
    public String getInterest() {
        return interest;
    }
    public void setInterest(String interest) {
        this.interest = interest;
    }
    public String toString() {
        return id;
    }

    @Override
    public int compareTo(Q49Person o) {
        return -1;
    }
}

class Q49StudyGroup {
    String name = "MATH";
    TreeSet<Q49Person> set = new TreeSet<Q49Person>();

    public void add(Q49Person p) {
        if (name.equals(p.getInterest())) set.add(p);
    }

    public static void main(String[] args) {
        Q49StudyGroup mathGroup = new Q49StudyGroup();
        mathGroup.add(new Q49Person("MATH"));
        System.out.println("A");
        mathGroup.add(new Q49Person("MATH"));
        System.out.println("B");
        System.out.println(mathGroup.set);
    }
}


