package unique1;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.*;
import java.time.chrono.IsoChronology;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Unique1 {
}

class Q2 {
    public static void main(String[] args) {
        try {
            m1();
        } catch (IndexOutOfBoundsException e) {
            System.out.println("1");
            throw new NullPointerException();
        } catch (NullPointerException e) {
            System.out.println("2");
            return;
        } catch (Exception e) {
            System.out.println("3");
        } finally {
            System.out.println("4");
        }
        System.out.println("END");
    }

    static void m1() {
        System.out.println("m1 Starts");
        throw new IndexOutOfBoundsException("Big Bang ");
    }
}
//

class Q3 {
    public void m1(List<? extends Number> list) {
        Number n = list.get(0);
    }
}
//

class Q6DateTest {
    public static void main(String[] args) {
        LocalDateTime greatDay = LocalDateTime.parse("2022-01-01T17:13:50");
        String greatDayStr = greatDay.format(DateTimeFormatter.ISO_DATE_TIME);
        System.out.println(greatDayStr);
    }
}

class Q9 {
    public static void main(String[] args) {
        StringBuilder sb = new StringBuilder("abcdef");
        Function<Character, String> f = i -> "" + Character.valueOf((char) (i + 1));
        for (int i = 0, k = sb.length(); i < k; i++) {
            sb.replace(i, i + 1, f.apply(sb.charAt(i)));
        }
        System.out.println(sb);
    }
}

interface Q11Pow {
    static void wow() {
        System.out.println("In Pow.wow");
    }
}

abstract class Q11Wow {
    static void wow() {
        System.out.println("In Wow.wow");
    }
}

class Q11Powwow extends Q11Wow implements Q11Pow {
    public static void main(String[] args) {
        Q11Powwow f = new Q11Powwow();
        f.wow();
        Q11Pow.wow();
    }
}
//

class Q14T extends Thread {
    public Q14T(String name) {
        super(name);
    }

    public void run() {
        try {
            for (int i = 0; i < 2; i++) {
                Thread.sleep(1000);
                System.out.println(getName() + " " + getState());
            }
        } catch (InterruptedException ie) {
            System.out.println(getName() + " " + getState() + " ");
        }
    }
}

class Q14TestClass {
    public static void main(String args[]) throws Exception {
        Q14T t1 = new Q14T("T1");
        t1.start();
        Q14T t2 = new Q14T("T2");
        t2.start();
        Thread.sleep(300);
        t1.interrupt();
    }
}

class Q16Student implements Serializable {
    public static final long serialVersionUID = 1;
    public String name;
    public String grade;

    //public String toString() {
    //  return "[" + name + ", " + grade + "]";
    //}

    public String id = "S111";
    public int age = 15;

    public String toString() {
        return "[" + id + ", " + name + ", " + grade + ", " + age + "]";
    }
}

class Q16 {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Q16Student s = new Q16Student();
        s.name = "bob";
        s.grade = "10";

        //FileOutputStream fos = new FileOutputStream("/Users/admin/IdeaProjects/ocajp829/tests/enthuware/unique1/student.ser");
        //ObjectOutputStream os = new ObjectOutputStream(fos);
        //os.writeObject(s);
        //os.close();

        FileInputStream fis = new FileInputStream("/Users/admin/IdeaProjects/ocajp829/tests/enthuware/unique1/student.ser");
        ObjectInputStream is = new ObjectInputStream(fis);
        s = (Q16Student) is.readObject();
        is.close();
        System.out.println("Loaded " + s);
    }
}

class Q18 {
    public static void main(String[] args) {
        Stream<String> ss = Stream.of("a", "b", "c");
        String str = ss.collect(Collectors.joining(",", "-", "+"));
        System.out.println(str);
    }
}

class Q20TestClass {
    public static void main(String[] args) throws Exception {
        ArrayList<Double> al = new ArrayList<>();
        System.out.println(al.indexOf(1.0));
        System.out.println(al.contains("string"));
        //INSERT CODE HERE
    }
}

class Q23Student {
    private Map<String, Integer> marksObtained = new HashMap<String, Integer>();
    private ReadWriteLock lock = new ReentrantReadWriteLock();

    public void setMarksInSubject(String subject, Integer marks) {
        lock.writeLock().lock();
        // 1 INSERT CODE HERE
        try {
            marksObtained.put(subject, marks);
        } finally {
            lock.writeLock().unlock();
            // 2 INSERT CODE HERE
        }

    }

    public double getAverageMarks() {
        return 1.;
        // valid code that computes average
    }
}
//

class Q23 {
    private Map<String, Integer> marksObtained = new HashMap<String, Integer>();
    private ReadWriteLock lock = new ReentrantReadWriteLock();

    public void setMarksInSubject(String subject, Integer marks) {
        lock.writeLock().lock(); //1
        try {
            marksObtained.put(subject, marks);
        } finally {
            lock.writeLock().unlock(); //2
        }
    }

    public double getAverageMarks() {
        lock.readLock().lock(); //3
        double sum = 0.0;
        try {
            for (Integer mark : marksObtained.values()) {
                sum = sum + mark;
            }
            return sum / marksObtained.size();
        } finally {
            lock.readLock().unlock();//4
        }
    }

    public static void main(String[] args) {

        final Q23 s = new Q23();

        //create one thread that keeps adding marks
        new Thread() {
            public void run() {
                int x = 0;
                while (true) {
                    int m = (int) (Math.random() * 100);
                    s.setMarksInSubject("Sub " + x, m);
                    x++;
                }
            }
        }.start();

        //create 5 threads that get average marks
        for (int i = 0; i < 5; i++) {
            new Thread() {
                public void run() {
                    while (true) {
                        double av = s.getAverageMarks();
                        System.out.println(av);
                    }
                }
            }.start();
        }
    }
}
//

class Q25Book {
    private String title;
    private LocalDate releaseDate;

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public Q25Book(String title, LocalDate releaseDate) {
        this.title = title;
        this.releaseDate = releaseDate;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return "Q25Book{" +
                "title='" + title + '\'' +
                ", releaseDate=" + releaseDate +
                '}';
    }

    public static void main(String[] args) {
        var books = new ArrayList<Q25Book>(List.of(
                new Q25Book("The Outsider", LocalDate.of(2019, 1, 1)),
                new Q25Book("Becoming", LocalDate.of(2018, 1, 1)),
                new Q25Book("Uri", LocalDate.of(2017, 1, 1))));
        Predicate<Q25Book> p = b -> b.getReleaseDate().isAfter(IsoChronology.INSTANCE.date(2018, 1, 1));
        System.out.println(IsoChronology.INSTANCE.date(2018, 1, 1));
        Set<String> newBooks = books.stream()
                .collect(Collectors.partitioningBy(p)).get(true).stream().map(Q25Book::getTitle).collect(Collectors.toCollection(TreeSet::new));
        System.out.println(books.stream().collect(Collectors.partitioningBy(p)));
        System.out.println(newBooks);

        newBooks = books.stream()
                .collect(Collectors.partitioningBy(p)).get(true).stream().map(Q25Book::getTitle).collect(Collectors.toSet());
        System.out.println(newBooks);

        newBooks = books.stream()
                .collect(Collectors.filtering(p, Collectors.mapping(Q25Book::getTitle, Collectors.toSet())));
        System.out.println(newBooks);

        Map<Boolean, Set<String>> collect = books.stream()
                .collect(Collectors.partitioningBy(p, Collectors.mapping(Q25Book::getTitle, Collectors.toSet())));
        System.out.println(collect);

        //newBooks = books.stream()
        //.collect(Collectors.groupingBy(p, Collectors.mapping(Q25Book::getTitle, Collectors.toSet())));
    }
}

class Q26 {
    public static void main(String[] args) {
        LocalDateTime ld = LocalDateTime.of(2022, Month.NOVEMBER, 6, 1, 30);
        ZonedDateTime date1 = ZonedDateTime.of(ld, ZoneId.of("US/Eastern"));
        ZonedDateTime date2 = date1.plusHours(1);
        System.out.println(Duration.ofSeconds(date1.getOffset().compareTo(date2.getOffset())).toHours());
        System.out.println(date1);
        System.out.println(date2);
    }
}

class Q29Base {
    public <T> List<T> transform(List<T> list) {
        return new ArrayList<T>();
    }
}

class Q29Derived extends Q29Base {

    //public ArrayList<Number> transform(List<Number> list) { return new ArrayList<Number>(); };
    //public ArrayList<Object> transform(List<Object> list) { return new ArrayList<Object>(); };
    //public <T> Collection<T> transform(List<T> list) { return new ArrayList<T>(); };

    public <T> ArrayList<T> transform(List<T> list) {
        return new ArrayList<T>();
    }

    public <T> Collection<T> transform(Collection<T> list) {
        return new HashSet<T>();
    }
}
//

class Q33 {
    public static void main(String[] args) {
        int[] i[] = {{1, 2}, {1}, {}, {1, 2, 3}};
    }
}

class Q34User {
    Q34Bandwidth bw = new Q34Bandwidth();

    public void consume(int bytesUsed) {
        bw.addUsage(bytesUsed);
    }
}

class Q34Bandwidth {
    private int totalUsage;
    private double totalBill;
    private double costPerByte;

    public void addUsage(int bytesUsed) {
        if (bytesUsed > 0) {
            totalUsage = totalUsage + bytesUsed;
            totalBill = totalBill + bytesUsed * costPerByte;
        }
    }
}

class Q36TestClass {
    public static void main(String[] args) throws Exception {
        LocalDate d = LocalDate.now();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("eeeee d'st day of' MMMMMM yyyy");
        String s = dtf.format(d);
        System.out.println(s);
    }
}

class Q38LowBalanceException extends Q38WithdrawalException {
    public Q38LowBalanceException(String msg) {
        super(msg);
    }
}

class Q38WithdrawalException extends Exception {
    public Q38WithdrawalException(String msg) {
        super(msg);
    }
}

class Q38Account {
    double balance;

    public void withdraw(double amount) throws Q38LowBalanceException {
        //try {
        throw new Q38LowBalanceException("Not Implemented");
        //} catch (WithdrawalException e) {
        //  throw new RuntimeException(e.getMessage());
        //}
    }

    public static void main(String[] args) {
        try {
            Q38Account a = new Q38Account();
            a.withdraw(100.0);
        } catch (Q38WithdrawalException e) {
            System.out.println(e.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}

class Q39PromotionTest {
    public static void main(String args[]) {
        int i = 5;
        float f = 5.5f;
        double d = 3.8;
        char c = 'a';
        if (i == f) c++;
        if (((int) (f + d)) == ((int) f + (int) d)) c += 2;
        System.out.println(c);
    }
}

class Q42Game {
}

class Q42Cricket extends Q42Game {
}

class Q42Instrument {
}

class Q42Guitar extends Q42Instrument {
}

interface Q42Player<E> {
    void play(E e);
}

interface Q42GamePlayer<E extends Q42Game> extends Q42Player<E> {
}

interface Q42MusicPlayer<E extends Q42Instrument> extends Q42Player {
}


class Q42Batsman implements Q42GamePlayer<Q42Cricket> {
    public void play(Q42Cricket o) {
    }
}

class Q42Batsman2 implements Q42GamePlayer<Q42Game> {
    public void play(Q42Game o) {
    }
}

class Q42Bowler implements Q42Player<Q42Guitar> {
    public void play(Q42Guitar o) {
    }
}

class Q42MidiPlayer implements Q42MusicPlayer {
    public void play(Object g) {
    }
}

class Q42MidiPlayer2 implements Q42MusicPlayer<Q42Instrument> {
    public void play(Object g) {
    }
}

class Q44 {
    public static void main(String[] args) {
        Stream<Integer> a1 = Stream.of(1, 5, 3, 1, 4);
        Stream<Integer> a2 = Stream.of(4, 1, 2);
        Stream.concat(a1, a2).peek(System.out::println).parallel().distinct().forEach(System.out::print);
    }
}

class Q45TestClass {
    public static void main(String[] args) {
        calculate(2);
    }

    public static void calculate(int x) {
        final int TWO = 2;
        String s = "a";
        String val = switch (x) {
            case TWO -> s;
            default -> "def";
        };
        s = null;
        System.out.println(val);
    }
}
//

class Q46SwitchTest {
    public static void main(String args[]) {
        for (var i = 0; i < 3; i++) {
            var flag = false;
            switch (i) {
                //flag = true;
            }
            if (flag) System.out.println(i);
        }
    }
}

class Q48 {
    public static void main(String[] args) throws NamingException, SQLException {
        Context ctx = new InitialContext();
        DataSource dataSource = (DataSource) ctx.lookup("java:/comp/env/jdbc/MyLocalDB");
        Connection connection = dataSource.getConnection();
        PreparedStatement stmt = connection.prepareStatement("select * from CUSTOMER where EMAILID=?");
        stmt.setObject(1, "bob@gmail.com"); //LINE 10
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            System.out.println(rs.getString("EMAILID")); //LINE 12
        }
        connection.close();
    }
}

class Q49SuperClass {
    public Q49SuperClass(int a) {
    }

    public Q49SuperClass() {
    }
}

class Q49SubClass extends Q49SuperClass {
    int i, j, k;

    public Q49SubClass(int m, int n) {
        i = m;
        j = m;
    } //1

    public Q49SubClass(int m) {
        super(m);
    } //2
}
//

class Q51 {
    public static void main(String[] args) {
        String String = "";   //This is valid.
        String:
        for (int i = 0; i < 10; i++) //This is valid too!
        {
            for (int j = 0; j < 10; j++) {
                if (i + j > 10) break String;
            }
            System.out.println("hello");
        }
    }
}
//

class Q53Employee {
    int age;
}

class Q53TestClass {
    public static void main(String[] args) {
        Q53Employee e = new Q53Employee();
        int z = 5;
        Supplier<Q53Employee> se = () -> {
            e.age = 40;
            //z = 1;
            //e = null;
            return e;
        };
        //e = null;
        e.age = 50;
        System.out.println(se.get().age);
    }
}
//

enum Q55Coffee {
    ESPRESSO("Very Strong"), MOCHA("Bold"), LATTE("Mild");
    public String strength;

    Q55Coffee(String strength) {
        this.strength = strength;
    }

    public String toString() {
        return strength;
    }

    public static void main(String[] args) {
        List.of(Q55Coffee.values()).stream().forEach(e -> {
            System.out.print(e.name() + ":" + e + ", ");
        });
    }
}









