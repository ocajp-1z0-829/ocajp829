package random;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

public class Random1 {
}

class Q8 {
    public static void main(String[] args) {
        LocalDateTime ld1 = LocalDateTime.of(2022, Month.NOVEMBER, 6, 2, 0);
        ZonedDateTime zd1 = ZonedDateTime.of(ld1, ZoneId.of("US/Eastern"));
        LocalDateTime ld2 = LocalDateTime.of(2022, Month.NOVEMBER, 6, 1, 0);
        ZonedDateTime zd2 = ZonedDateTime.of(ld2, ZoneId.of("US/Eastern"));
        System.out.println(zd1);
        System.out.println(zd2);
        long x = ChronoUnit.HOURS.between(zd1, zd2);
        System.out.println(x);
    }
}

abstract class Q10Base {
    abstract int power();
}

class Q10A extends Q10Base {
    @Override
    int power() {
        return 0;
    }
}

class Q10AA extends Q10A {
    @Override
    int power() {
        return 1;
    }

    void mAA() {
        System.out.println("mAA");
    }
}

class Q10B extends Q10Base {
    @Override
    int power() {
        return 1;
    }
}

class Q10 {
    void processBase(Q10Base base) {
        if (base instanceof Q10A a && a instanceof Q10AA aa) {
            aa.mAA();
        }

        if (base instanceof Q10A a && a.power() == 1) {
            ((Q10AA) a).mAA();
        }

        if (base instanceof Q10B b || base instanceof Q10A a) {
            //if (b != null) System.out.println(b.power());
            //if (a != null) System.out.println(a.power());
        }
    }
}

class Q22 {
    public static void main(String[] args) {
        List<Integer> a = List.of(1, 2, 3);
        System.out.println(a.subList(1, 1));
        System.out.println("abc".substring(1, 1));
    }
}


interface Q4Device {
    public abstract void switchOn();
}

 class Q4Router /* LOCATION 1 */ {    /* LOCATION 2 */
    public void switchOn() {
    }

    public  void reset(){};
}

class Q4MimoRouter extends Q4Router implements Q4Device {    /* LOCATION 3 */
    public void reset(){ } //at location 3
}

class Q7 {
    public static void main(String[] args) {
        //var values = new ArrayList<String>();
        List<String> values = new ArrayList<String>();
        //values.sort();
    }
}