package test4;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.ToDoubleFunction;
import java.util.stream.Collectors;

public class Test4 {
}

class Q1 {
    interface Readable {
    }

    sealed class Document implements Readable permits Book {
    }

    non-sealed class Book extends Document implements Readable {
    }

    final class Journal extends Book {
    }
}

class Q2 {
    public static void main(String[] args) {
        AtomicInteger ai = new AtomicInteger(5); //2 INSERT CODE HERE
        int x = ai.getAndIncrement();
        System.out.println(x);
        x = ai.incrementAndGet();
        System.out.println(x);
        x = ai.addAndGet(1);
        System.out.println(x);
        x = ai.getAndSet(6);
        System.out.println(x);
        System.out.println(ai.get());
    }
}

class Q3 {
    public static void main(String[] args) {
        var values = new ArrayList<String>();
        values.add("A");
        values.forEach((var k) -> System.out.print(k.length()));
        values.forEach(k -> System.out.print(k.length()));
        var k = values.get(0);
        values.add(k);
        for (var value : values) {
            System.out.print(value);
        }
    }
}

class Q5 {
    public static void main(String[] args) {
        Instant start = Instant.parse("2022-06-25T16:13:30.00z");
        System.out.println(start.plus(10, ChronoUnit.HOURS));
        System.out.println(start);
        Duration timeToCook = Duration.ofHours(1);
        Instant readyTime = start.plus(timeToCook);
        readyTime = readyTime.plus(Period.ofWeeks(1));
        System.out.println(readyTime);
        LocalDateTime ltd = LocalDateTime.ofInstant(readyTime, ZoneId.of("GMT+2"));
        System.out.println(ltd);
    }
}

class Q6 {
    public static void main(String[] args) throws IOException {
        RandomAccessFile raf = new RandomAccessFile("tests/enthuware/test4/file.txt", "rw");
        raf.seek(raf.length());
        raf.writeChars("FINAL TEXT");
    }
}

class Q7 implements Callable<String> {
    public String call() throws Exception {
        //do something
        return "Data from callable";
    }
}

interface Q10IHello {
    public int hello(int x, int y);

    public long hello(long x, long y);
}

abstract class Q10Hello implements Q10IHello {
    public int hello(int a, int b) {
        return 0;
    }
}

//

class Q11PathTest {
    static Path p1 = Paths.get("tests/enthuware/test4/file.txt");

    public static String getValue() {
        String x = p1.getName(1).toString();
        String y = p1.subpath(1, 2).toString();
        return x + " : " + y;
    }

    public static void main(String[] args) {
        System.out.println(getValue());
    }
}

class Q12 {
    public static void main(String[] args) {
        List<Integer> ls = Arrays.asList(1, 2, 3);
        double sum = ls.stream().reduce(0, (a, b) -> a + b);
        System.out.println(sum);

        sum = ls.stream().mapToInt(x -> x).sum();
        System.out.println(sum);

        //sum = 0; ls.stream().forEach(a->{ sum=sum+a; });
    }
}

class Q13 {

    public static void main(String[] args) {
        Object[] sa = {100, 100.0, "100"};
        Collections.sort(Arrays.asList(sa), null);
        System.out.println(sa[0] + " " + sa[1] + " " + sa[2]);
    }
}

class Q15 {
    public static boolean isValid(Path p) {
        return p.startsWith("temp") && p.endsWith("clients.dat");
    }

    public static void writeData() {
        var p1 = Paths.get("/temp/records");
        var p2 = p1.resolve("clients.dat");
        System.out.println(p2 + " " + isValid(p2));
    }

    public static void main(String[] args) {
        writeData();
    }
}

class Q16 {
    public static void main(String[] args) {
        int x = 10;
        final int I = 2 * 3;
        switch (x) {
            case I:
                System.out.println(x); //this is valid because I is a compile time constant
                //case x: System.out.println();
        }
    }
}

class Q19 {
    public static void main(String[] args) {
        char[] a = {'h', 'e', 'l', 'l'};
        char[] b = {};
        int x = Arrays.compare(a, b);
        int y = Arrays.mismatch(a, b);
        System.out.println(x + " " + y);
    }
}

class Q23 {
    public static void main(String[] args) {
        Path p1 = Paths.get("tests/enthuware/test4/file.txt");
        Path p2 = Paths.get("tests/enthuware/file.txt");
        Path p3 = p1.relativize(p2);
        System.out.println(p3);
    }
}

class Q25TestOuter {
    public static class TestInner {
        public void sayIt() {
            System.out.println("hello");
        }
    }

    public void main(String[] args) {//call here
        TestInner ti = new TestInner();
        Q25TestOuter.TestInner ti2 = new TestInner();
        Q25TestOuter.TestInner ti3 = new Q25TestOuter.TestInner();
        //Q25TestOuter.TestInner ti4 = new Q25TestOuter().new TestInner();
    }
}

class Q28 {
    public static void main(String[] args) {
        int count = 0, sum = 0;
        do {
            if (count % 3 == 0) continue;
            System.out.println(count);
            sum += count;
        }
        while (count++ < 11);
        System.out.println(sum);
    }
}

class Q30Test {
    int i1;
    static int i2;

    public void method1() {
        int i;       // ... insert statements here
        i = this.i1;
        i = this.i2;
        this.i1 = i2;
    }
}

class Q30 {
    public static void main(String[] args) {
        LocalDateTime ld = LocalDateTime.of(2022, Month.OCTOBER, 31, 10, 0);

        ZonedDateTime date = ZonedDateTime.of(ld, ZoneId.of("US/Eastern"));
        date = date.plus(Duration.ofDays(1));
        System.out.println(date);

        date = ZonedDateTime.of(ld, ZoneId.of("US/Eastern"));
        date = date.plus(Period.ofDays(1));
        System.out.println(date);
    }
}

class Q32InitTest {
    public static void main(String[] args) {
        int a = 10;
        int b = 20;
        a += (a = 4);
        b = b + (b = 5);
        System.out.println(a + ",  " + b);
    }
}

//
class Q33Item {
    private String name;
    private String category;
    private double price;

    @Override
    public String toString() {
        return "Q33Item{" +
                "name='" + name + '\'' +
                ", category='" + category + '\'' +
                ", price=" + price +
                '}';
    }

    public Q33Item(String name, String category, double price) {
        this.name = name;
        this.category = category;
        this.price = price;
    }

    public String getCategory() {
        return category;
    }

    public double getPrice() {
        return price;
    }
}

class Q33 {
    public static void main(String[] args) {
        List<Q33Item> items = Arrays.asList(
                new Q33Item("Pen", "Stationery", 3.0),
                new Q33Item("Pencil", "Stationery", 2.0),
                new Q33Item("Eraser", "Stationery", 1.0),
                new Q33Item("Milk", "Food", 2.0),
                new Q33Item("Eggs", "Food", 3.0));
        ToDoubleFunction<Q33Item> priceF = Q33Item::getPrice; //1
        Map<String, List<Q33Item>> collect = items.stream()
                .collect(Collectors.groupingBy(Q33Item::getCategory));//2
        System.out.println(collect);
        collect.forEach((a, b) -> {
            double av = b.stream().collect(Collectors.averagingDouble(priceF)); //3
            System.out.println(a + " : " + av);
        });
    }
}
//

class Q37 {
    public static void main(String[] args) throws IOException {
        RandomAccessFile raf = new RandomAccessFile("tests/enthuware/test4/file.txt", "rwd");
        //raf.writeChars("hello world");
        raf.writeUTF("hello world");
        raf.close();

        var dis = new DataInputStream(new FileInputStream("tests/enthuware/test4/file.txt"));
        String value = dis.readUTF();
        System.out.print(value);
        dis.close();
    }
}

//

class Q44TestClass {
    public static void main(String args[]) {
        var x = Integer.parseInt(args[0]);
        switch (x) {
            //case x < 5:
            //  System.out.println("BIG");
            //break;
            //case x > 5:
            //  System.out.println("SMALL");
            default:
                System.out.println("CORRECT");
                break;
        }
    }
}

class Q45 {
    public static void main(String[] args) {
        List<Integer> names = Arrays.asList(1, 2, 3);
        System.out.println(names.stream().mapToInt(x -> x).sum());
        //System.out.println(names.stream().forEach((sum, x) -> sum = sum + x));
        System.out.println(names.stream().reduce(0, (a, b) -> a + b));

        System.out.println(names.stream().collect(Collectors.mapping(x -> x, Collectors.summarizingInt(x -> x))).getSum());
        System.out.println(names.stream().collect(Collectors.summarizingInt(x -> x)).getSum());
    }
}

class Q49 {
    public static void main(String[] args) {
        List<? super Float> a1 = new ArrayList<Float>();
        List<? super Float> a2 = new ArrayList<Number>();
        List<? super Float> a3 = new ArrayList<Object>();
    }
}

class Q50 {
    public static void main(String[] args) {
        LocalDate d1 = LocalDate.parse("2022-02-05", DateTimeFormatter.ISO_DATE);
        LocalDate d2 = LocalDate.of(2022, 2, 5);
        LocalDate d3 = LocalDate.now();
        System.out.println(d1);
        System.out.println(d2);
        System.out.println(d3);
    }
}

//

class Q51Writer {
    private static final int LOOPSIZE = 5;

    public synchronized void write(Q51Data... da) {
        for (int i = 0; i < LOOPSIZE; i++) {
            while (!da[0].own(this)) ;
            while (!da[1].own(this)) ;
            da[0].write(i);
            da[1].write(i);
            da[1].release();
            da[0].release();
        }
    }
}

class Q51Data {
    Q51Writer writer;
    int id;

    public Q51Data(int id) {
        this.id = id;
    }

    public synchronized boolean own(Q51Writer w) {
        System.out.println(Thread.currentThread().getName());
        if (writer == null) {
            writer = w;
            return true;
        } else return false;
    }

    public synchronized void release() {
        writer = null;
    }

    public void write(int i) {
        System.out.println("data written W" + i + " D" + id);
    }
}

class Q51TestClass {
    public static void main(String[] args) {
        var w1 = new Q51Writer();
        var w2 = new Q51Writer();
        var d1 = new Q51Data(1);
        var d2 = new Q51Data(2);
        new Thread(() -> {
            w1.write(d1, d2);
        }).start();
        new Thread(() -> {
            w2.write(d1, d2);
        }).start();
    }
}

class Q52 {
    public static void main(String args[]) {
        int var = 20, i = 0;
        do {
            while (true) {
                if (i++ > var) break;
            }
        } while (i < var--);
        System.out.println(var);
    }
}

class Q54 {
    public static void main(String[] args) throws SQLException {

        String qr = "insert into USERINFO values( ?, ?, ?)";
        try (PreparedStatement ps = null;) {
            ps.setObject(1, 1, JDBCType.INTEGER);
            ps.setObject(2, "Ally A", JDBCType.VARCHAR);
            ps.setObject(3, "101 main str", JDBCType.VARCHAR);
            System.out.println(ps.executeUpdate()); //1
            ps.setObject(1, 2, Types.INTEGER);
            ps.setObject(2, "Bob B", Types.VARCHAR);
            System.out.println(ps.executeUpdate()); //2
        }
    }
}

class Q55 {
    public static void main(String[] args) throws SQLException {
        Statement stmt = null;
        ResultSet rs = stmt.executeQuery("select SID, NAME,  GPA from STUDENT");
        while (rs.next()) {
            System.out.println(rs.getString(3));
            System.out.println(rs.getInt("GPA"));
        }
        //connection.close();
    }
}