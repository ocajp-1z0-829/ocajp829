package foundation4;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.*;

import static java.util.concurrent.TimeUnit.SECONDS;

public class Foundation4 {
}

class Q1 {
    public static void main(String[] args) {
        ConcurrentMap<String, String> concurrentMap;
    }
}

class Q8 {
    public static void main(String[] args) {
        Map<String, List<String>> myMap = new HashMap();

        List<String> list = new ArrayList<>();
        list.add("A");
        list.addAll(new ArrayList<>());

        List<String> list2 = new ArrayList<>();
        list2.add("A");
        List<? extends String> list3 = new ArrayList<>();
        list2.addAll(list3);
        System.out.println(list2);
    }
}

class Q10Base extends Thread {
    static int k = 10;
}

class Q10Incrementor extends Q10Base {
    public void run() {
        for (; k > 0; k++) {
            System.out.println("IRunning...");
        }
    }
}

class Q10Decrementor extends Q10Base {
    public void run() {
        for (; k > 0; k--) {
            System.out.println("DRunning...");
        }
    }
}

class Q10TestClass {
    public static void main(String args[]) throws Exception {
        Q10Incrementor i = new Q10Incrementor();
        Q10Decrementor d = new Q10Decrementor();
        i.start();
        d.start();
    }
}

class Q18 {
    public static void main(String[] args) {
        SimpleDateFormat sdf = new SimpleDateFormat("MMMM", Locale.FRANCE);
        System.out.println(sdf.format(new Date()));
    }
}

class Q36 {
    public static void main(String[] args) {
        //System.in o;
        NavigableMap nm = new TreeMap();
        nm.put("A", "A");
        System.out.println(nm.ceilingEntry("A"));
        System.out.println(nm.ceilingKey("A"));
        System.out.println(nm.floorEntry("A"));
        System.out.println(nm.higherEntry("A"));
        System.out.println(nm.higherKey("A"));
        System.out.println(nm.lowerEntry("A"));
        TreeMap tm;
        ConcurrentSkipListMap cslm;
    }
}

class Q38 {
    public /*static*/ CharSequence q38(CharSequence cs) throws IOException {
        System.out.println(cs);
        return new StringBuilder("A");
    }

    public static void main(String[] args) {
        do {
            break;
        } while (true);

        label:
        if (true) {
            System.out.println("break label");
            break label; //this is valid
        }

        switch (1) {
            default:
                break;
        }

        for (; true; ) break;


        switch (1) {
            default -> {
                break;
            }
        }

    }
}

class Q41 extends Q38 {
    @Override
    public String q38(CharSequence cs) throws FileNotFoundException /*, SQLException*/ {
        return "B".toString();
    }

    public static void main(String[] args) {
        Q41 q41 = new Q41();
        try {
            System.out.println(q41.q38("B"));
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}

class Q43TopClass {
    public Q43Inner inner1 = new Q43Inner() {
        private static int value;

        public void doA() {
            System.out.println("doing A");
        }
    };

    public void doA() {
        //inner1.doA();
    }
}

class Q43Inner {
    int value;
}

class Q45 {
    public static void main(String[] args) {
        Set s = Collections.synchronizedSet(new HashSet());
        System.out.println(s);
    }
}

class Q46TestClass implements Runnable {
    volatile int x;

    public void run() {
        x = 5;
    }

    public static void main(String[] args) {
        Q46TestClass tc = new Q46TestClass();
        tc.x = 10;
        new Thread(tc).start(); // 1
        System.out.println(tc.x);
    }
}

class Q49BeeperControl {
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    public void beepForAnHour() {
        final ScheduledFuture<?> beeperHandle =
                scheduler.scheduleAtFixedRate(() -> System.out.println("beep"), 1, 1, SECONDS);
        scheduler.schedule(() -> beeperHandle.cancel(true), 10, SECONDS);
    }
    public static void main(String args[]) {
        new Q49BeeperControl().beepForAnHour();
    }
}

