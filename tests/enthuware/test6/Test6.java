package test6;

import java.io.*;
import java.nio.file.*;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Test6 {
}

interface Q4Device {
    public abstract void switchOn();
}

abstract class Q4Router /* LOCATION 1 */ {    /* LOCATION 2 */
    public void switchOn() {
    }

    public abstract void reset();
}

class Q4MimoRouter extends Q4Router implements Q4Device {

    @Override
    public void reset() {

    }    /* LOCATION 3 */
}

//
class Q6A {
    public void m1() {
        System.out.println("AAAA");
    }
}

class Q6B extends Q6A {
    public void m1() {
        System.out.println("BBBB");
    }
}

class Q6C extends Q6B {
    public void m1() {
        System.out.println("CCCC");
        super.m1();
        //( (Q6A) this ).m1();
    }

    public static void main(String[] args) {
        Q6C q6C = new Q6C();
        q6C.m1();
    }
}

class Q7 {
    public static void main(String[] args) {
        var values = new ArrayList<String>();
        values.sort((a, b) -> a.compareTo(b));
        values.forEach(System.out::println);

        String[] sa = values.toArray(String[]::new);
        //values.sort();
    }
}

enum Q9Card {
    CLUB, SPADE, HEART, DIAMOND;

    public boolean isRed() {
        return switch (this) {
            case HEART, DIAMOND -> true;
            default -> false;
        };
    }
}

class Q9 {
    public static void main(String[] args) {
        Arrays.stream(Q9Card.values()).dropWhile(c -> c.isRed()).forEach(System.out::println);
        System.out.println();
        Arrays.stream(Q9Card.values()).takeWhile(c -> c.isRed()).forEach(System.out::println);
        switch (Q9Card.valueOf("HEART")) {
            case HEART -> System.out.println("heart");
            case CLUB -> System.out.println("club");
            case SPADE -> System.out.println("spade");
            case DIAMOND -> System.out.println("diamond");
            default -> System.out.println("unknown");
        }
    }
}

//
class Q10Point {
    int x, y;
}

class Q10ColoredPoint extends Q10Point {
    int color;
}

class Q10 {
    static void test(Q10ColoredPoint p, Q10Point q) {
        System.out.println("(ColoredPoint, Point)");
    }

    static void test(Q10Point p, Q10ColoredPoint q) {
        System.out.println("(Point, ColoredPoint)");
    }

    static void test(Q10ColoredPoint p, Q10ColoredPoint q) {
        System.out.println("(ColoredPoint, ColoredPoint)");
    }

    public static void main(String[] args) {
        Q10ColoredPoint cp = new Q10ColoredPoint();
        test(cp, cp);
    }
}

class Q11 {
    void probe(Object x) {
        System.out.println("In Object");
    } //3

    void probe(Number x) {
        System.out.println("In Number");
    } //2

    void probe(Integer x) {
        System.out.println("In Integer");
    } //2

    void probe(Long x) {
        System.out.println("In Long");
    } //4

    public static void main(String[] args) {
        double a = 10;
        new Q11().probe(a);
    }
}

class Q12 {
    public static void main(String[] args) {
        char[][] cA1 = {{'a', 'b', 'c'}, {'a', 'b', 'c'}};

        char cA2[][] = new char[3][];
        for (int i = 0; i < cA2.length; i++) cA2[i] = new char[4];

        char cA3[][] = {new char[]{'a', 'b', 'c'}, new char[]{'a', 'b', 'c'}};
        //char cA4[3][2] =new char[][]{{'a', 'b', 'c'}, {'a', 'b', 'c'}};
        //char[][] cA5 = {"1234", "1234", "1234"};

        //var cA6[][] = new char[3][];
        var cA6 = new char[3][];
        for (var i = 0; i < cA6.length; i++) cA6[i] = new char[4];
    }
}

class Q14Base {
    public List<CharSequence> transform(Set<CharSequence> list) {
        return null;
    }
}

class Q14Derived extends Q14Base {
    public ArrayList<CharSequence> transform(Set<CharSequence> list) {
        return new ArrayList<>(super.transform(list));
    }

    public List<Integer> transform(TreeSet<CharSequence> id) {
        return null;
    }
    //public List<StringBuilder> transform(Set<CharSequence> id){return null;}
    //public ArrayList<String> transform(Set<String> id){}
}

class Q16 {
    public static void main(String[] args) {

        LocalTime now = LocalTime.of(11, 15);
        LocalTime gameStart = LocalTime.of(10, 15);
        long timeConsumed = 0;
        long timeToStart = 0;
        if (now.isAfter(gameStart)) {
            timeConsumed = gameStart.until(now, ChronoUnit.HOURS);
        } else {
            timeToStart = now.until(gameStart, ChronoUnit.HOURS);
        }
        System.out.println(timeToStart + " " + timeConsumed);
        System.out.println(ChronoUnit.HOURS.between(now, gameStart));
    }
}

//
class Q17Booby {
    int i;

    public Q17Booby() {
        i = 10;
        System.out.print("Booby");
    }
}

class Q17Dooby extends Q17Booby implements Serializable {
    int j;

    public Q17Dooby() {
        j = 20;
        System.out.print("Dooby");
    }
}

class Q17Tooby extends Q17Dooby {
    int k;

    public Q17Tooby() {
        k = 30;
        System.out.print("Tooby");
    }
}

class Q17TestClass {
    public static void main(String[] args) throws Exception {
        var t = new Q17Tooby();
        t.i = 100;
        var oos = new ObjectOutputStream(new FileOutputStream("tests/enthuware/test6/test.ser"));
        oos.writeObject(t);
        oos.close();

        var ois = new ObjectInputStream(new FileInputStream("tests/enthuware/test6/test.ser"));
        t = (Q17Tooby) ois.readObject();
        ois.close();
        System.out.println(t.i + " " + t.j + " " + t.k);
    }
}

class Q19 {
    public static void main(String[] args) {
        //Collections.binarySearch();
        //Collections.sort();
        //Arrays.sort();
        //Arrays.binarySearch();
    }
}

class Q20Booby {
}

class Q20Dooby extends Q20Booby {
}

class Q26Tooby extends Q20Dooby {
}

class Q20 {
    public static void main(String[] args) {
        var bL = new ArrayList<Q20Booby>();
        var tL = new ArrayList<Q26Tooby>();
        var dL = new ArrayList<Q20Dooby>();
        List<? extends Q20Booby> bV = bL;
        bV = tL;
        bV = dL;
        bV.add(null);
        //bV.add(new Q20Dooby() {});

        List<? super Q20Dooby> dV = bL;
        dV = dL;
        dV = new ArrayList<Object>();
        //dV = tL;
        dV.add(new Q20Dooby() {
        });
        dV.add(new Q26Tooby() {
        });
        //dV.add(new Q20Booby() {});
    }
}

class Q21 {
    public static void main(String[] args) {

        Object t = Integer.valueOf(107);
        // int k = (Integer) t.intValue() / 9;
        // System.out.println(k);
    }
}

//
interface Q24Bozo {
    int type = 0;

    public void jump();
}

class Q24Type1Bozo implements Q24Bozo {
    public Q24Type1Bozo() {
        //type = 1;
        System.out.println(type);
    }

    public void jump() {
        System.out.println("jumping..." + type);
    }

    public static void main(String[] args) {
        Q24Bozo b = new Q24Type1Bozo();
        b.jump();
    }
}

class Q29 {
    int x;

    public static void main(String[] args) {
        //System.out.println(this.x);
    }
}

class Q30 {
    public static void main(String[] args) {
        Float f = null;
        try {
            f = Float.valueOf("12.3");
            String s = f.toString();
            int i = Integer.parseInt(s);
            System.out.println("" + i);
        } catch (Exception e) {
            System.out.println("trouble : " + f);
        }
    }
}

//
class Q31A {
    Q31A() {
        print();
    }

    void print() {
        System.out.print("A ");
    }
}

class Q31B extends Q31A {
    //final
    int i = 4;

    public static void main(String[] args) {
        Q31A a = new Q31B();
        a.print();
    }

    void print() {
        System.out.print(i + " ");
    }
}

class Q34Person {
    String name;
    String dob;

    public Q34Person(String name, String dob) {
        this.name = name;
        this.dob = dob;
    }
}

class Q34MySorter {
    public int compare(Q34Person p1, Q34Person p2) {
        return p1.dob.compareTo(p2.dob);
    }
}

class Q34SortTest {
    public static int diff(Q34Person p1, Q34Person p2) {
        return p1.dob.compareTo(p2.dob);
    }

    public static int diff(Date d1, Date d2) {
        return d1.compareTo(d2);
    }

    public static void main(String[] args) {
        ArrayList<Q34Person> al = new ArrayList<>();
        al.add(new Q34Person("Paul", "01012000"));
        al.add(new Q34Person("Peter", "01011990"));
        al.add(new Q34Person("Patrick", "01012002"));

        Collections.sort(al, (p1, p2) -> p1.dob.compareTo(p2.dob));
        Collections.sort(al, Q34SortTest::diff);
        Collections.sort(al, new Q34MySorter()::compare);
        //Arrays.sort(al, SortTest::diff);
    }
}


class Q35TestClass {
    private class A {
    }

    public static class B {
    }

    public void useClasses() {
        new Q35TestClass.A();
        new Q35TestClass().new A();
        new Q35TestClass.B();
        new A();
        new B();
    }

    public static void useClasses2() {
        new Q35TestClass().new A();
        new Q35TestClass.B();
        new B();
    }

}
//

class Q38 {
    public static void main(String[] args) {
        new Consumer<String>() {
            @Override
            public void accept(String s) {
                DirectoryStream.Filter<String> stringFilter = String::isEmpty;
            }
        };
    }
}

class Q39 {
    public static void main(String[] args) {
        var data = new ArrayList<>();
        data.add("A");
        data.add(100); //1
        data.add("C");
        data.set(0, 200); //2
        System.out.println(data);
        data.remove(2); //3
        data.set(2, 101L);//4
        System.out.println(data);
    }
}

class Q40 {
    public static void main(String[] args) {
        Function<Integer, String> f1 = (a) -> Integer.toHexString(a);
        Function<Integer, String> f2 = Integer::toHexString;
    }
}

//
class Q41MoveTest {
    public static void main(String[] args) throws IOException {
        Path p1 = Paths.get("tests/enthuware/test6/test.ser");
        Path p2 = Paths.get("tests/enthuware/test6/move/test.ser");
        Files.move(p1, p2, StandardCopyOption.ATOMIC_MOVE);
        Files.delete(p1);
        System.out.println(p1.toFile().exists() + " " + p2.toFile().exists());
    }
}

class Q47Movie {
    private String title;
    private String genre;

    public Q47Movie(String title, String genre) {
        this.title = title;
        this.genre = genre;
    }    //accessors not shown

    public static void main(String[] args) {

        Stream<Q47Movie> sm = Stream.of(new Q47Movie("a1", "a"), new Q47Movie("a2", "a"), new Q47Movie("b1", "b"), new Q47Movie("c1", null));

        Map<Optional<String>, Long> gcount = sm.collect(Collectors.groupingBy(movie -> Optional.ofNullable(movie.genre), Collectors.counting()));
        System.out.println(gcount.keySet());
    }
}

class Q48ThreadPoolTest {
    public static void main(String[] args) {
        Runnable r = () -> System.out.println("In Runnable");
        Callable<Integer> c = () -> {
            System.out.println("In Callable");
            return 0;
        };
        var es = Executors.newCachedThreadPool();

        es.submit(c);
        es.submit(r);

        Future<Integer> fi1 = es.submit(c);
        Future fi2 = es.submit(r);

        es.shutdown();
    }
}

class Q49 {
    public static void main(String[] args) {
        List<Integer> ls = Arrays.asList(1, 2, 3);
        Function<Integer, Integer> func = a -> a * a; //1
        System.out.println(ls.stream().map(func).peek(System.out::print).max(Integer::compareTo).get()); //2
    }
}

class Q50Sample implements Q50IInt {
    public static void main(String[] args) {
        Q50Sample s = new Q50Sample();  //1
        int j = s.thevalue;       //2
        int k = Q50IInt.thevalue;    //3
        int l = thevalue;         //4
        int thevalue1 = Q50Sample.thevalue;
    }
}

interface Q50IInt {
    int thevalue = 0;
}
//

class Q52TestClass {
    public static void main(String[] args) throws ParseException {
        double amount = 123456.789;
        Locale fr = new Locale("fr", "FR");
        NumberFormat formatter = NumberFormat.getInstance(fr);
        String s = formatter.format(amount);
        System.out.println(s);
        formatter = NumberFormat.getInstance();
        System.out.println(formatter.format(amount));
        Number amount2 = formatter.parse(s); //123
        System.out.println(amount + " " + amount2);

        NumberFormat shortFormat = NumberFormat.getCompactNumberInstance();
        NumberFormat longFormat = NumberFormat.getCompactNumberInstance(Locale.US, NumberFormat.Style.LONG);
        String ss = shortFormat.format(amount);
        String ls = longFormat.format(amount);
        System.out.println("Short:" + ss);
        System.out.println("Long:" + ls);
        Number amountS = shortFormat.parse(ss);
        Number amountL = longFormat.parse(ls);
        System.out.println("Short Amount: " + amountS + " Long Amount: " + amountL);
    }
}

/**
 * The value 1 of k is saved by the compound assignment operator += before its right-hand operand (k = 4) * (k + 2) is evaluated. Evaluation of this right-hand operand then assigns 4 to k, calculates the value 6 for k + 2, and then multiplies 4 by 6 to get 24. This is added to the saved value 1 to get 25, which is then stored into k by the += operator. An identical analysis applies to the case that uses a[0].   k += (k = 4) * (k + 2);   a[0] += (a[0] = 4) * (a[0] + 2);   k = k + (k = 4) * (k + 2);   a[0] = a[0] + (a[0] = 4) * (a[0] + 2);
 */
class Q54Test {
    public static void main(String[] args) {
        int k = 1;
        int[] a = {1};
        k += (k = 4) * (k + 2);
        a[0] += (a[0] = 4) * (a[0] + 2);
        System.out.println(k + " , " + a[0]);
    }
}


