package unique4;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Unique4 {
}

class Q2 {
    public static void main(String[] args) {
        Locale myloc = new Locale.Builder().setLanguage("hinglish").setRegion("IN").build();
        ResourceBundle msgs = ResourceBundle.getBundle("mymsgs", myloc);
        Enumeration<String> en = msgs.getKeys();
        while (en.hasMoreElements()) {
            String key = en.nextElement();
            String val = msgs.getString(key);
            System.out.println(key + "=" + val);
        }
    }
}

class Q5Calculator {
    public static void main(String[] args) {
        double principle = 100;
        int interestrate = 5;
        double amount = compute(principle, x -> x * interestrate);
    }

    public static double compute(double base, Function<Integer, Integer> func) {
        return func.apply((int) base);
    }
}

class Q6 {
    public static void main(String[] args) {
        Locale locale = new Locale("en", "US");
        ResourceBundle rb = ResourceBundle.getBundle("test.MyBundle", locale);
        Object obj = rb.getObject("key1");
        String[] vals = rb.getStringArray("key2");
    }
}

class Q8AX {
    static int[] x = new int[0];

    static {
        x[0] = 10;
    }

    public static void main(String[] args) {
        var ax = new Q8AX();
    }
}

class Q17 {
    public static void main(String[] args) {
        try {
            new Q17().setSQLMode(DriverManager.getConnection(""), "A");
        } catch (Exception e) {
        }
    }

    public void setSQLMode(Connection c, String mode) throws Exception {
        PreparedStatement stmt = (PreparedStatement) c.createStatement();
        String qr = "SET SESSION sql_mode = " + stmt.enquoteLiteral(mode) + ";";
        System.out.println(qr);
        stmt.setClob(1, new FileReader("") {
        });
        //stmt.execute(qr);
    }
}

class Q20 {
    public void usePrintWriter(PrintWriter pw) throws IOException {
        boolean bval = true;
        pw.print(bval);
        System.out.println();
        pw.flush();
    }

    public static void main(String[] args) throws IOException {
        new Q20().usePrintWriter(
                new PrintWriter("/Users/admin/IdeaProjects/ocajp829/tests/enthuware/unique4/A.txt"));
    }
}

class Q23 {
    public float parseFloat(String s) {
        float f = 0.0f;      // 1
        try {
            f = Float.valueOf(s).floatValue();    // 2
            return f;      // 3
        } catch (NumberFormatException nfe) {
            f = Float.NaN;    // 4
            return f;     // 5
        } finally {
            return f;     // 6
        }
        //return f;    // 7
    }
}
//

class Q24 {
    static Map<String, List<Double>> groupedValues = new HashMap<>();

    public void process1(String name, Double value) {
        List<Double> values = groupedValues.get(name);
        if (values == null) {
            values = new ArrayList<Double>();
            groupedValues.put(name, values);
        }
        values.add(value);
    }

    public void process(String name, Double value) {
        groupedValues.computeIfAbsent(name, (a) -> new ArrayList<Double>()).add(value);
    }

    public static void main(String[] args) {
        new Q24().process("A", 2.);
        System.out.println(groupedValues);
    }
}

class Q29 {
    public static void main(String[] args) {

        Object v1 = IntStream.rangeClosed(10, 15).boxed().filter(x -> x > 12).parallel().findAny();
        Object v2 = IntStream.rangeClosed(10, 15).boxed().filter(x -> x > 12).sequential().findAny();
        System.out.println(v1 + ":" + v2);
    }
}

class Q31TestClass {

    public static int m1(int i) {
        return ++i;
    }

    public static void main(String[] args) {

        int k = m1(args.length);
        k += 3 + ++k;
        System.out.println(k);
    }
}

//
class Q33ItemProcessor extends Thread {  //LINE 1
    CyclicBarrier cb;

    public Q33ItemProcessor(CyclicBarrier cb) {
        this.cb = cb;
    }

    public void run() {
        System.out.println("processed");
        try {
            cb.await();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}

class Q33Merger implements Runnable { //LINE 2
    public void run() {
        System.out.println("Value Merged");
    }
}

class Q33 {
    public static void main(String[] args) throws Exception {
        var m = new Q33Merger();          //LINE 3
        CyclicBarrier cb = new CyclicBarrier(2, m);
        var ip = new Q33ItemProcessor(cb);
        ip.start(); //LINE 4
        cb.await();
    }
}
//

class Q34 {
    public static void main(String[] args) {
        String[] sa = {"charlie", "bob", "andy", "dave"};
        Collections.sort(Arrays.asList(sa), null);
        Arrays.sort(sa, null);
        System.out.println(sa[0]);
    }
}

class Q35TestClass {
    public static void main(String[] args) {
        //while (var k = 5; return k < 7){
        //  System.out.println(k++);
        //}
    }
}

class Q36 {
    public static void main(String[] args) {
        int[][] iaa = {{1, 2}, {3, 4}, {5, 6}};
        long count = Stream.of(iaa).flatMapToInt(IntStream::of)
                .map(i -> i + 1).filter(i -> i % 2 != 0)
                .peek(System.out::print)
                .count();
        System.out.println(count);
    }
}

class Q40 {
    public static void main(String[] args) {
        for (; Math.random() < 0.5; ) {
            System.out.println("true");
        }


        //for (; ; Math.random()) {
        //  System.out.println("true");
        //}

        //for (; ; ) {
        //  Object o = Math.random() < .05 ? System.out : false;
        //System.out.println(o);
        //}

        for (; ; ) {
            if (Math.random() < .05) break;
        }
    }
}

class Q42 {
    public static void main(String[] args) {
        Stream<Integer> strm1 = Stream.of(2, 3, 5, 7, 11, 13, 17, 19); //1
        Stream<Integer> strm2 = strm1.filter(i -> {
            return i > 5 && i < 15;
        });
        Stream<Integer> strm3 = strm2.parallel().filter(i -> i > 5).filter(i -> i < 15).sequential();
        Stream<Integer> strm4 = strm3.collect(Collectors.partitioningBy(i -> {
            return i > 5 && i < 15;
        })).get("true").stream();
        strm4.forEach(System.out::print);
    }
}

class Q43 {
    public static void main(String[] args) {
        var ca = new char[]{'a', 'b', 'c', 'd'};
        var i = 0;
        for (var c : ca) {
            switch (c) {
                case 'a':
                    i++;
                case 'b':
                    ++i;
                case 'c' | 'd':
                    i++;
            }
        }
        System.out.println('c' | 'd');
        System.out.println("i = " + i);
    }
}

class Q45 {
    public static void main(String[] args) {
        DateTimeFormatter sdf = DateTimeFormatter.ofPattern("MM/yy");
        System.out.println(sdf.format(LocalDate.now()));
    }
}

//

sealed interface Q51Member permits Q51Student, Q51Person {
    String role();
}

record Q51Person(String role) implements Q51Member {
}

record Q51Student(int id, Q51Person person) implements Q51Member {
    public String role() {
        return person.role();
    }
}

class Q51 {
    public static void main(String[] args) {
        Q51Member p = new Q51Person("ADMIN");
        System.out.println(p.role());
    }
}
//

class Q52T extends Thread {
    static Lock lock = new ReentrantLock();

    public Q52T(String name) {
        super(name);
    }

    public void run() {
        for (int i = 0; i < 2; i++) {
            if (lock.tryLock()) {
                System.out.println(getName() + " got lock. " + getState());
                //lock.unlock();
            } else {
                System.out.println(getName() + " could not get lock. " + getState());
            }
        }
    }
}

class Q52TestClass {
    public static void main(String args[]) throws Exception {
        Q52T t1 = new Q52T("T1");
        t1.start();
        Q52T t2 = new Q52T("T2");
        t2.start();
        Thread.sleep(1000);
    }
}