package test15;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.NumberFormat;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.function.*;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;

public class Test15 {
}

class Q9 {
    public static void main(String[] args) {
        DoubleStream ds = DoubleStream.of(1.0, 2.0, 3.0);
        //DoubleFunction<IntUnaryOperator> doubleF = m-> n->(int)m+n;
        //DoubleFunction<Double> doubleF = m -> n -> m + n;
        DoubleFunction<DoubleUnaryOperator> doubleF = m -> n -> m + n;
        ds.map(doubleF.apply(5.0)).forEach(System.out::println);

        DoubleFunction<DoubleUnaryOperator> doubleF2 = (m) -> {
            System.out.println("m is " + m);
            return (n) -> {
                System.out.println("n is " + n);
                return m + n;
            };
        };
        ds = DoubleStream.of(1.0, 2.0, 3.0);
        ds.map(doubleF2.apply(5.0)).forEach(System.out::println);
    }
}

class Q16 {
    public static void main(String[] args) {

        Integer i1 = 1;
        Integer i2 = new Integer(1);
        int i3 = 1;
        Byte b1 = 1;
        Long g1 = 1L;
        Double d = 1.0;
        Number n = 1;
        System.out.println(i1 == i2);
        System.out.println(i1 == i3);
        System.out.println(n == i1);
        System.out.println(i1.equals(i2));
        System.out.println(i1.equals(b1));
        System.out.println(d == 1L);
    }
}

class Q17 {
    public static void main(String[] args) {
        writeData();
    }

    public static void writeData() {
        Path p1 = Paths.get("/Users/admin/IdeaProjects/ocajp829/tests/enthuware/test15/test.txt");
        Path p2 = Paths.get("/", p1.subpath(0, 7).toString(), "clients.dat");
        try (var br = new BufferedReader(new FileReader(p1.toFile()));
             var bw = new BufferedWriter(new FileWriter(p2.toFile()))) {
            String line = null;
            while ((line = br.readLine()) != null) {
                bw.write(line);
                bw.newLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

class Q18TestClass {
    public static void m1() throws Exception {
        throw new Exception("Exception from m1");
    }

    public static void m2() throws Exception {
        try {
            m1();
        } catch (
                Exception e) {
            throw e;
        } finally {
            throw new RuntimeException("Exception from finally");
        }
    }

    public static void main(String[] args) {
        try {
            m2();
        } catch (Exception e) {
            Throwable[] ta = e.getSuppressed();
            for (Throwable t : ta) {
                System.out.println(t.getMessage());
            }
        }
    }
}

class Q19Test {
    public static void main(String[] args) {
        String hello = "Hello", lo = "lo";
        String hi = "hi";
        String h = "h";
        String i = "i";
        System.out.print((test15.Q19Other.hello == hello) + " ");    //line 1
        System.out.print((test15.other.Q19Other.hello == hello) + " ");   //line 2
        System.out.print((hello == ("Hel" + "lo")) + " ");           //line 3
        System.out.print((hello == ("Hel" + lo)) + " ");              //line 4
        System.out.println(hello == ("Hel" + lo).intern());          //line 5
        System.out.println(hi == (h + i));
        System.out.println(hi == ("h" + "i"));
    }
}

class Q19Other {
    static String hello = "Hello";
}

class Q20 {
    public static void main(String[] args) {
        Instant now = Instant.now();
        Instant now2 = now.truncatedTo(ChronoUnit.DAYS);
        System.out.println(now2);
    }
}

class Q26TestClass {
    static int val = 10;

    public static int reduce(int val) {
        class Inner {
            public int reduce(int mval) {
                return mval - val;
            }
        }
        //val--;
        return new Inner().reduce(val);
    }

    public static void main(String[] args) {
        reduce(5);
        System.out.println(val);
    }
}

class Q28 {
    public static void main(String[] args) {
        List<String> strList = Arrays.asList("a", "aa", "aaa");
        Function<String, Integer> f = x -> x.length();
        Consumer<String> c = x -> System.out.print("Len:" + x + " ");
        Predicate<String> p = x -> "".equals(x);
        strList.forEach(c);
    }
}

class Q31 {
    public static void main(String[] args) {
        List<String> vals1 = Arrays.asList("a", "b");
        String join1 = vals1.parallelStream().reduce("_", (a, b) -> a.concat(b));
        System.out.println(join1);

        List<String> vals = Arrays.asList("a", "b", "c", "d", "e", "f", "g", "a", "b", "c", "d", "e", "f", "g", "a", "b", "c", "d", "e", "f", "g", "a", "b", "c", "d", "e", "f", "g");
        String join = vals.parallelStream().peek(System.out::println)
                .reduce("_", (a, b) -> {
                    System.out.println("reducing " + a + " and " + b + " Thread: " + Thread.currentThread().getName());
                    return a.concat(b);
                }, (a, b) -> {
                    System.out.println("combining " + a + " and " + b + " Thread: " + Thread.currentThread().getName());
                    return a.concat(b);
                });
        System.out.println(join);
    }
}

interface Q32Carnivore {
    default int calories(List<String> food) {
        return food.size() * 100;
    }

    int eat(List<String> foods);
}

class Q32Tiger implements Q32Carnivore {
    public int eat(List<String> foods) {
        System.out.println("Eating " + foods);
        return foods.size() * 200;
    }
}

class Q32TestClass {
    public static int size(List<String> names) {
        return names.size() * 2;
    }

    public static void process(List<String> names, Q32Carnivore c) {
        c.eat(names);
    }

    public static void main(String[] args) {
        List<String> fnames = Arrays.asList("a", "b", "c");
        Q32Tiger t = new Q32Tiger();
        process(fnames, t::eat);
        process(fnames, t::calories);
        process(fnames, Q32TestClass::size);
        //process(fnames, Q32Carnivore::calories);
        //process(fnames, Q32Tiger::eat);
        //INSERT CODE HERE
    }
}

class Student {
    private String name;
    private int marks;

    public Student(String name, int marks) {
        this.name = name;
        this.marks = marks;
    }

    public int getMarks() {
        return marks;
    }

    public String toString() {
        return name + ":" + marks;
    }

    public static void main(String[] args) {
        List<Student> ls = Arrays.asList(new Student("S1", 20), new Student("S3", 30), new Student("S3", 20));
        Map<Integer, List<Student>> grouping = ls.stream().collect(
                Collectors.groupingBy(Student::getMarks,
                        Collectors.toList()));
        System.out.println(grouping);
    }
}
//

class Q40A extends Thread {
    public void run() {
        System.out.println("Starting loop");
        try {
            //Thread.sleep(10000);
            while (!isInterrupted()) {
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Ending loop");
    }
}

class Q40TestClass {
    public static void main(String args[]) throws Exception {
        Q40A a = new Q40A();
        a.start();
        Thread.sleep(1000);
        a.interrupt();
        System.out.println("Stop");
    }
}

class Q41 {
    public static void main(String[] args) throws IOException {
        String p = "/Users/admin/IdeaProjects/ocajp829/tests/enthuware/test15";
        Stream<Path> lines1 = Files.find(Paths.get(p),
                10, (a, b) -> a.endsWith("txt"));
        lines1.forEach(System.out::println);

        BufferedReader bfr = new BufferedReader(new FileReader(p + "/test.txt"));
        System.out.println(bfr.readLine());

        Stream<Path> lines2 = Files.list(Paths.get(p));
        lines2.forEach(x -> System.out.println(x));

        Stream<String> lines3 = Files.lines(Paths.get(p + "/test.txt"));
        lines3.forEach(System.out::println);

        Stream<String> lines = Files.lines(Paths.get(p + "/test.txt"), Charset.defaultCharset());
        lines.forEach(s -> System.out.println(s));
    }
}

class Q42 {
    public static void main(String[] args) {

        int[] array2D[] = new int[2][2];
        array2D[0][0] = 1;
        array2D[0][1] = 2;
        array2D[1][0] = 3;

        int[] arr = {1, 2};
        int[][] arr2 = {arr, {1, 2}, arr};
        int[][][] arr3 = {arr2};

        int[][][] array3D = {{{0, 1}, {2, 3}, {4, 5}}};

        int[][] array2D2 = new int[][]{{0}, {1}};

        array2D2 = new int[][]{{0, 1, 2, 4}, {5, 6}};
    }
}

class Q43TestClass {
    public static void main(String[] args) throws Exception {
        double amount = 53000.35;
        Locale jp = new Locale("jp", "JP");
        NumberFormat formatter1 = NumberFormat.getCurrencyInstance(jp);
        NumberFormat formatter2 = new DecimalFormat();
        Format formatter3 = NumberFormat.getCurrencyInstance(jp);
        NumberFormat formatter4 = DecimalFormat.getCurrencyInstance(jp);
        NumberFormat formatter5 = NumberFormat.getInstance(jp);
        NumberFormat formatter = new DecimalFormat("#.000");
        System.out.println(formatter.format(amount));
    }
}

class Q44TestClass {
    public static void main(String args[]) {
        Q44A ab = new Q44B();
        Q44A ac = new Q44C();
        if (ac instanceof Q44B b1) {
            b1.b();
            if (b1 instanceof Q44C c1) {
                c1.c();
            }
        } else {
            ac.a();
        }
    }
}

class Q44A {
    void a() {
        System.out.println("a");
    }
}

class Q44B extends Q44A {
    void b() {
        System.out.println("b");
    }
}

class Q44C extends Q44B {
    void c() {
        System.out.println("c");
    }
}

//
class Q47 {
    public static void main(String[] args) throws IOException {
        String p = "/Users/admin/IdeaProjects/ocajp829/tests/enthuware/test15/test.txt";
        Stream<String> lines1 = Files.lines(Paths.get(p));
        lines1.forEach(System.out::println);
        System.out.println();
        List<String> lines = Files.readAllLines(Paths.get(p));
        lines.forEach(System.out::println);
    }
}
//

class Q48 {
    public static void main(String[] args) {
        iCanDoThis();
    }

    public static void iCanDoThis() {
        int x = 2;
        int y = ~x;
        int z = x ^ y;
        System.out.println(z);
        boolean flag = x < y & x > z++;
        if (flag) {
            flag = x > y && x > --z;
        }
        if (z > -1) {
            --z;
        } else z++;
        System.out.println(flag + " " + z);
    }
}

enum Q49Title {
    MR("Mr. "), MRS("Mrs. "), MS("Ms. ");
    private String title;

    private Q49Title(String s) {
        title = s;
    }

    public String format(String first, String last) {
        return title + " " + first + " " + last;
    }
}

enum Q49Title2 {
    DR;
    private Q49Title t = Q49Title.MR;

    public String format(String s) {
        return t.format(s, s);
    }
}

class Q49TestClass {
    void someMethod() {
        //System.out.println(Q49Title.format("Rob", "Miller"));
        System.out.println(Q49Title.MR.format("Rob", "Miller"));
        //System.out.println(MR.format("Rob", "Miller"));

    }

    public static void main(String[] args) {
        new Q49TestClass().someMethod();
    }
}

class Q50 {
    public static void main(String[] args) {
        Collection<Number> col = new HashSet<>();
        col.add(1);
        var list1 = List.of(col);
        col.add(2);
        var list2 = List.copyOf(col);
        System.out.println(list1 + ", " + list2);
    }
}

class Q51 {
    String s = this.toString();
    String s1;
}

class Q52 {
    public static void main(String[] args) {
        var b = false|true;
        System.out.println(b);
    }
}