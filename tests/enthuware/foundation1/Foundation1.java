package foundation1;

import java.util.TreeMap;
import java.util.concurrent.*;
import java.util.function.IntFunction;

public class Foundation1 {
    private Foundation1() {
    }
}


class Q1 {
    public static void main(String[] args) {
        int[] values = {10, 30, 50};
        for (var val : values) {
            var x = 0;
            while (x < values.length) {
                System.out.println(x + " " + val);
                x++;
            }
        }
    }
}

class Q2 {
    public static void main(String[] args) {
        Future<Long> result = null;
        ExecutorService executor = Executors.newFixedThreadPool(10);
        Callable<Long> c = new Callable<Long>() {
            @Override
            public Long call() throws Exception {
                Thread.sleep(5000);
                return 123l;
            }
        };
        try {
            System.out.println(c.call());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        //This call returns immediately.
        result = executor.submit(c);
        //Call to Future.get() blocks until the result is available.
        //So don't call the following line here.
        //System.out.println("Result is "+result.get());
        while (!result.isDone()) {
            try {
                //Do something else and check later
                Thread.sleep(1000);
                System.out.println(Thread.currentThread().getName());
                System.out.println(Thread.currentThread().getState());
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
        //Since result.isDone() is true, the result is available now
        try {
            System.out.println("Result is " + result.get());
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }
    }
}

abstract class Q3TestClass {
    public abstract void m1();

    public void m2() {
        System.out.println("hello");
    }
}

class Q4 {
    public static void main(String[] args) {
        System.out.println("12345".charAt(6));
    }
}

class Q5 {
    public static void main(String[] args) {
        int i = 10;
        byte b = 20;
        //b = i;//will not compile because byte is smaller than
        b = (byte) i; //OK

        final int k = 10;
        b = k; //Okay because k is final and 10 fits into a byte

        final float f = 10.0f;//will not compile
        // because 10.0 is a double even though the value 10.0 fits into a float
        //i = f;//will not compile either even after changing 10.0 to 10.0f.

    }
}

interface Q12 {
    int i = 1;
}

interface Q12I1 {
    public default void m1() {
        System.out.println("in I1.m1");
    }
}

interface Q12I2 {

    public default void m1() {
        System.out.println("in I2.m1");
    }
}

class Q12CI implements Q12I1, Q12I2 {
    @Override
    public void m1() {
        Q12I1.super.m1();
    } //This class will not compile.
}   //This class will compile because it provides its own implementation of m1.

class Q12C2 implements Q12I1, Q12I2 {
    public void m1() {
        System.out.println("in C2.m1");
    }
}

class Q15 {
    public static void main(String[] args) {
        StringBuilder sb = new StringBuilder();

        sb.trimToSize();
        sb.ensureCapacity(1);
        //sb.append(true);
        sb.reverse();
        System.out.println(sb);
        sb.setLength(3);
        System.out.println(sb.toString().length());
    }
}

class Q20 {
    public static void main(String[] args) {
        if (false) ;
        else ;
    }
}

class Q23 {
    Long getValue() {
        Integer i = 10;
        System.out.println(2 + "");
        return 2L;
    }
}

class Q26 {
    public static void main(String[] args) {
        System.out.println(" ".isBlank());
        System.out.println("".isEmpty());
        " 123 45 ".lines().forEach(System.out::print);
        System.out.println();
        System.out.println(" 12345 ".strip());
        System.out.println(" 12345 ".stripIndent());
        System.out.println(" 123 45 ".stripLeading());
        System.out.println(" 123 45 ".stripTrailing());
        System.out.println("123 45".indent(2));

        System.out.println(" 123 45 ".transform((a) -> a + "y").toString());

        System.out.println(" 123 45 ".indexOf("1", 20));

        System.out.println("%s".formatted("5", 1));
    }
}

class Q27 {
    private Q27() {
    }

    public static void main(String[] args) {
        IntFunction<String> a;
    }
}

class Q36OuterClass {

    public class Q36InnerClass {
        static int VAL = 10; //COMPILES FINE
        static String STR = "1234"; //COMPILES FINE
        static Object obj = new Object(); //
        static int val2 = 10; //COMPILES FINE

        static final void method() {} //COMPILES FINE
        static int qmethod() {
            return 1;
        } //COMPILES FINE
    }
}

class Q43 {
    public static void main(String[] args) {
        Byte b = 2;
        Character c = 2;
        Float f = 2.0f;
    }
}

class Q46 {
    public static void main(String[] args) {
        new TreeMap<>();
    }
}

