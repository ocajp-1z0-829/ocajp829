package test11;

import java.io.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Test11 {
}

class Q1 {
    public static void createFile(String name) throws Exception {
        try (OutputStream os = new FileOutputStream(name);) {
            var pw = new PrintWriter(os);
            pw.write(1);
            os.write(99);

            var bos = new BufferedOutputStream(os);
            pw = new PrintWriter(bos);
            pw.print(99);
            pw.print(true);
            //pw.flush();
            //os.flush();
            pw.close();
            os.close();
        }
    }

    public static void main(String[] args) throws Exception {
        createFile("tests/enthuware/test11/test.txt");
    }
}

class Q2 {
    public static void main(String[] args) {
        Integer i = 4;
        System.out.println(i.compareTo(2));
        System.out.println(Integer.compare(1, 2));
    }
}

//
class Q4TestClass {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6};
        int counter = 0;
        for (var value : arr) {
            if (counter >= 5) {
                break;
            } else {
                continue;
            }
            //if (value > 4) {
            //  arr[counter] = value + 1;
            //}
            //counter++;
        }
        System.out.println(arr[counter]);
    }
}
//

class Q6 {
    public static void main(String[] args) throws IOException {

        var raf = new RandomAccessFile("tests/enthuware/test11/test1.txt", "rwd");
        raf.writeChars("hello");
        System.out.println(raf.getFilePointer());
        raf.seek(raf.getFilePointer());
        raf.close();
    }
}

class Q8 {
    public static void main(String[] args) {
        String val1 = "hello";
        StringBuilder val2 = new StringBuilder("world");
        UnaryOperator<String> uo1 = s1 -> s1.concat(val1);
        UnaryOperator<String> uo2 = s -> s.toUpperCase();
        //System.out.println(uo1.apply(uo2.apply(val2)));
    }
}

class Q10 {
    public static void main(String[] args) {
        List<String> books = Arrays.asList(
                new String("Gone with the wind"),
                new String("Gone with the wind"),
                new String("Atlas Shrugged"));
        books.stream().collect(Collectors.toMap((b -> b), b -> b.length(), (b1, b2) -> b1 + b2))
                .forEach((a, b) -> System.out.println(a + " " + b));
    }
}

class Q17 {
    public static void main(String[] args) {
        var tickers = List.of("A", "D", "E", "C", "A");
        var ratio = List.of(1.0, 1.2, 1.5, 1.8, 2.0);
        ratio.forEach(a -> System.out.println(1.0 / a));
        var map1 = IntStream.range(0, tickers.size()).boxed()
                .collect(Collectors.toMap(i -> tickers.get(i), i -> 1.0 / ratio.get(i), (x, y) -> x + y));
        System.out.println(map1);
        //<<----- LINE 1
        var map2 = map1.entrySet().stream().sorted(Map.Entry.comparingByKey())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (x, y) -> x - y, LinkedHashMap::new));
        //<<----- LINE 2
        map2.forEach((var k, var v) -> System.out.printf("%s = %.2f\n", k, v));
    }
}

class Student {
    int marks;
}

class Q23TestClass {
    Student k = new Student();

    public static void main(String[] args) {
        var s = new Student() {
            @Override
            public String toString() {
                return "student obj";
            }

            ;
        };
        var slist = Set.of(new Student());
        for (var i : slist) {
            System.out.println(i.marks);
        }
        slist.removeIf((var s1) -> {
            System.out.println(s1.marks);
            return s1.marks < 0;
        }); //5
    }
}

class T extends Thread {
    public static Lock lock = new ReentrantLock();

    public T(String name) {
        super(name);
    }

    static StringBuilder data = new StringBuilder();

    public void run() {
        if (lock.tryLock()) {
            try {
                lock.lock();
                data.append("hello");
            } finally {
                lock.unlock();
                System.out.println(data);
                lock.unlock();
            }
        } else {
            System.out.println("busy");
        }
    }
}

class Q24TestClass {
    public static void main(String args[]) throws Exception {
        T t1 = new T("T1");
        t1.start();
        try {
            if (t1.lock.tryLock()) {
                System.out.println("не занят");
            } else {
                System.out.println("занят");
            }
            //t1.lock.lock();
            System.out.println(t1.data);
        } finally {
            t1.lock.unlock();
        }
    }
}

class Q26 {
    public static float parseFloat1(String s1) {
        try {
            return Float.parseFloat(s1);
        } catch (NumberFormatException e) {
            System.out.println(e);
            return 0.0f;
        } catch (IllegalArgumentException e) {
            return Float.NaN;
        }
    }

    public static void main(String[] args) {
        System.out.println(parseFloat1("" + Float.NEGATIVE_INFINITY));
        System.out.println(parseFloat1("" + Float.POSITIVE_INFINITY));
        System.out.println(parseFloat1("junk"));
        System.out.println(parseFloat1("-Infinity"));
        System.out.println(parseFloat1("NaN"));
        System.out.println(Float.NaN);
    }
}

class Q28TestClass {
    public static void main(String args[]) {
        try {
            RuntimeException re = null;
            throw re;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

class Q29 {
    public static void main(String[] args) {
        var ca = new char[]{'a', 'b', 'c', 'd', 'e'};
        var i = 0;
        for (var c : ca) {
            switch (c) {
                case 'a' -> i++;
                case 'b' -> ++i;
                case 'c', 'd' -> i++;
            }
        }
        System.out.println("i = " + i);
    }
}

class Q33 {
    public static void main(String[] args) {
        List<String> letters = Arrays.asList("j", "a", "v", "a");
        String word = letters.stream().reduce("", (a, b) -> a.concat(b));
        System.out.println(word);
        System.out.println(letters.stream().reduce("", (a, b) -> a.concat(b)));
    }
}
//

class Q34Boo {
    int boo = 10;

    public Q34Boo(int k) {
        System.out.println("In Boo k = " + k);
        boo = k;
    }
}

class Q34BooBoo extends Q34Boo {
    public Q34BooBoo() {
        super(0);
        System.out.println("In BooBoo k = " + 0);
    }

    public Q34BooBoo(int k) {
        super(k);
        System.out.println("In BooBoo k = " + k);
    }
}

class Q34Moo extends Q34BooBoo implements Serializable {
    int moo = 10;

    public Q34Moo() {
        super(5);
        System.out.println("In Moo");
    }
}

class Q37TestClass {
    public static void main(String[] args) throws Exception {
        var moo = new Q34Moo();
        var fos = new FileOutputStream("tests/enthuware/test11/test.ser");
        var os = new ObjectOutputStream(fos);
        os.writeObject(moo);
        os.close();
        var fis = new FileInputStream("tests/enthuware/test11/test.ser");
        var is = new ObjectInputStream(fis);
        moo = (Q34Moo) is.readObject();
        is.close();
    }
}

class Q40 {
    public static void main(String[] args) {
        DateTimeFormatter df = DateTimeFormatter.ofPattern("eeee");
        LocalDate d = LocalDate.of(2000, 1, 1);
        System.out.println(df.format(d));
    }
}

class Q43TestOuter {
    public void myOuterMethod() {
        new TestInner(); //1
        new Q43TestOuter.TestInner();
        new Q43TestOuter().new TestInner();
        var to = new Q43TestOuter();
        to.new TestInner();
    }

    public class TestInner {
    }

    public static void main(String[] args) {
        var to = new Q43TestOuter();
        //new TestInner();
        to.new TestInner();
        new Q43TestOuter().new TestInner();
        //new TestOuter.TestInner();
    }
}


class Q46TestClass {
    public static void main(String args[]) {
        int k = 0;
        try {
            int i = 5 / k;
        } catch (ArithmeticException e) {
            System.out.println("1");
        } catch (RuntimeException e) {
            System.out.println("2");
            return;
        } catch (Exception e) {
            System.out.println("3");
        } finally {
            System.out.println("4");
        }
        System.out.println("5");
    }
}

class Q47 {
    public static void main(String[] args) {
        boolean b = true | false;
        System.out.println(b);
        b = true || false;
        System.out.println(b);
    }
}

class Q48TestClass {
    public static void main(String args[]) {
        Q49B b = new Q49C();
        Q49A a = b;

        if (a instanceof Q49B b1) b1.a();

        if (a instanceof Q49C c1) c1.c();
    }
}

class Q49A {
    void a() {
        System.out.println("a");
    }
}

class Q49B extends Q49A {
    void b() {
        System.out.println("b");
    }
}

class Q49C extends Q49B {
    void c() {
        System.out.println("c");
    }
}
//

abstract class Q51Base {
    abstract int power();
}

class Q51A extends Q51Base {
    @Override
    int power() {
        return 0;
    }
}

class Q51AA extends Q51A {
    @Override
    int power() {
        return 1;
    }

    void mAA() {
        System.out.println("mAA");
    }
}

class Q51B extends Q51Base {
    @Override
    int power() {
        return 1;
    }
}

class Q51 {
    void processBase(Q51Base base) {
        if (base instanceof Q51A a && a instanceof Q51AA aa) {
            aa.mAA();
        }


        if (base instanceof Q51A a && a.power() == 1) {
            ((Q51AA) a).mAA();
        }

        if (base instanceof Q51B b && base instanceof Q51A a) {
            if (b != null) System.out.println(b.power());
            if (a != null) System.out.println(a.power());
        }

    }
}
//

class TestClass {
    static StringBuffer sb1 = new StringBuffer();
    static StringBuffer sb2 = new StringBuffer();

    public static void main(String[] args) {
        new Thread(new Runnable() {
            public void run() {
                synchronized (sb1) {
                    sb1.append("X");
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    synchronized (sb2) {
                        sb2.append("Y");
                    }
                }
                System.out.println(sb1);
            }
        }).start();
        new Thread(new Runnable() {
            public void run() {
                synchronized (sb2) {
                    sb2.append("Y");
                    synchronized (sb1) {
                        sb1.append("X");
                    }
                }
                System.out.println(sb2);
            }
        }).start();
    }
}