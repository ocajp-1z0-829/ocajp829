package test7;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public class Test7 {
}

class Q2 {
    public static void main(String[] args) {
        System.out.println("     ".isBlank());
        System.out.println("".isEmpty());
    }
}

//
class Q3Data {
    public volatile int d1 = 0;
    public AtomicInteger d2 = new AtomicInteger(0);
}

class Q3TestClass {
    public static void main(String args[]) throws Exception {
        Q3Data data = new Q3Data();
        Runnable r1 = () -> {
            Thread t1 = Thread.currentThread();
            for (; data.d1 < 2; ) {
                System.out.print(t1.getName() + " : " + data.d1 + " ");
                data.d1++;
            }
        };
        Runnable r2 = () -> {
            Thread t1 = Thread.currentThread();
            for (; data.d2.get() < 2; ) {
                System.out.print(t1.getName() + " : " + data.d2.get() + " ");
                data.d2.getAndIncrement();
            }
        };
        Thread t1 = new Thread(r1, "T1");
        Thread t2 = new Thread(r2, "T2");
        t1.start();
        t2.start();
    }
}

//
class Q5TestClass extends Thread {
    private static int threadcounter = 0;

    public void run() {
        try {
            Thread.sleep((long) (new Random().nextDouble() * 10000));
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        threadcounter++;
        System.out.println(threadcounter);
    }

    public static void main(String[] args) throws Exception {
        System.out.println(new Random().nextInt());
        System.out.println(new Random().nextLong());
        System.out.println(new Random().nextDouble());
        for (int i = 0; i < 10; i++) {
            synchronized (Q5TestClass.class) {
                new Q5TestClass().start();
            }
        }
    }
}

class Q10Person implements Serializable {
    String name;

    Q10Person(String name) {
        this.name = name;
    }
}

class Q10Student extends Q10Person {
    String school;
    Q10Pupil p = new Q10Pupil("Aaa", 1);

    public Q10Student(String name, String school) {
        super(name);
        this.school = school;
    }

    @Override
    public String toString() {
        return "Q10Student{" +
                "school='" + school + '\'' +
                ", p=" + p +
                ", name='" + name + '\'' +
                '}';
    }
}

record Q10Pupil(String chGarden, int age) implements Serializable {
}

class Q15TestClass {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Q10Person p = new Q10Student("Bob Dylan", "NYU");
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("tests/enthuware/test7/student.ser"));
             ObjectInputStream ois = new ObjectInputStream(new FileInputStream("tests/enthuware/test7/student.ser"));) {
            oos.writeObject(p);
            oos.flush();
            System.out.println((Q10Student) ois.readObject());
        }
    }
}

class Q16 implements Comparable<String> {
    public static void main(String[] args) {
        var cin = new Comparator<Integer>() {
            public int compare(Integer i1, Integer i2) {
                return i1 - i2;
            }
        };
        System.out.println(cin.compare(1, 2));
    }

    @Override
    public int compareTo(String o) {
        return 0;
    }
}

class Q18Test {
    public static void main(String[] args) {
        var a = new int[]{1, 2, 3, 4};
        int[] b = {2, 3, 1, 0};
        System.out.println(a[(a = b)[3]]);
    }
}


class Q20Outer {
    int i = 10;

    class Inner {
        public void methodA() {
            System.out.println(i);
            System.out.println(Q20Outer.this.i);
            System.out.println(Q20Outer.Inner.this);
            System.out.println(new Q20Outer.Inner());
            System.out.println(new Q20Outer().new Inner());
            System.out.println(new Inner());
            //  System.out.println(this.i);
            //line 1.
        }
    }

    public static void main(String[] args) {
        new Q20Outer().new Inner().methodA();
    }
}

enum Q21Coffee  //1
{
    ESPRESSO("Very Strong"), MOCHA, LATTE; //2
    public String strength; //3

    Q21Coffee() {
    }

    Q21Coffee(String strength) //4
    {
        this.strength = strength; //5
    }

    public String toString() {
        return strength + ordinal();
    } //6

    public static void main(String[] args) {
        System.out.println(MOCHA.strength);
    }
}

class Q23 {
    public static void main(String[] args) {
        String s = """
                Hello      
                World""";
        System.out.println(s.length());

        s = """
                """;
        System.out.println(s.length());
    }
}

class Q24A {
    public List<? super Integer> getList() {
        return null;
    }
}

class Q24B extends Q24A {
    @Override
    //public List<? super Number> getList() {return null;}
    //  public ArrayList<Number> getList(){return null;}
    public ArrayList<Integer> getList() {
        return null;
    }
}

class Q30 {
    public static void main(String[] args) throws Exception {
        try (var fis = new FileInputStream("tests/enthuware/test7/test.txt");
             var isr = new InputStreamReader(fis)) {
            while (isr.ready()) {
                isr.skip(1);
                int i = isr.read();
                char c = (char) i;
                System.out.print(c);
            }
        }
    }
}

class Q33 {
    public static void main(String[] args) throws Exception {
        copy("tests/enthuware/test7/test.txt", "tests/enthuware/test7/test2.txt");
    }

    public static void copy(String fileName1, String fileName2) throws Exception {
        try (InputStream is = new FileInputStream(fileName1); OutputStream os = new FileOutputStream(fileName2);) {
            byte[] buffer = new byte[1024];
            int bytesRead = 0;
            while ((bytesRead = is.read(buffer)) != -1) {
                os.write(buffer, 0, bytesRead);
                System.out.println("Read and written bytes " + bytesRead);
            }
        }
    }
}

class Q35Valuator {
    public static AtomicInteger status = new AtomicInteger(0);

    public static void main(String[] args) {
        int oldstatus = status.get();         /* valid code here */
        boolean newstatus = status.compareAndSet(1, 1);
        System.out.println(status.get());
    }
}

class Q36 {
    public static void main(String[] args) {
        var f = new File("tests/enthuware/test7/d/e");
        f.mkdirs();
        System.out.println(f.getParentFile());
        System.out.println(f.getParent());
    }
}

class Q37Parser {
    public static void main(String[] args) {
        int i = 0;
        try {
            i = Integer.parseInt("1_00");
        } catch (NumberFormatException e) {
            System.out.println("Problem in " + i);
        }
    }
}

class Q38PathTest {
    static Path p1 = Paths.get("/tests/enthuware/test7/d/e");

    public static String getRoot() {
        String root = p1.getRoot().toString();
        return root;
    }

    public static void main(String[] args) {
        System.out.println(getRoot());
    }
}

class Q39 {
    public static void main(String[] args) {
        float f = 0x0123;
        System.out.println(f);

        double f1 = 43e1;
        System.out.println(f1);
    }
}

class Q40Base {
    public <T> Collection<T> transform(Collection<T> list) {
        return new ArrayList<T>();
    }
}

class Q40Derived extends Q40Base {
    //public <T> Collection<T> transform(Collection<T> list) {return new HashSet<String>();}
    public <T> Collection<T> transform(Stream<T> list) {
        return new HashSet<T>();
    }

    //public <T> List<T> transform(Collection<T> list) { return new ArrayList<T>();};
    public <X> Collection<X> transform(Collection<X> list) {
        return new HashSet<X>();
    }

    ;

    //public Collection<CharSequence> transform(Collection<CharSequence> list) {return new HashSet<CharSequence>();}
}

class Q41TestClass {
    void probe(int... x) {
        System.out.println("In ...");
    }  //1

    void probe(Integer x) {
        System.out.println("In Integer");
    } //2

    void probe(long x) {
        System.out.println("In long");
    } //3

    void probe(Long x) {
        System.out.println("In LONG");
    } //4

    public static void main(String[] args) {
        Integer a = 4;
        new Q41TestClass().probe(a); //5
        int b = 4;
        new Q41TestClass().probe(b); //6
    }
}

class Q47TestClass {
    public class A {
    }

    public static class B {
    }

    public static void main(String args[]) {
        class C {
        }
        new Q47TestClass().new A();
        //new Q47TestClass.A();
        //new Q47TestClass().new B();
        new C();
        //new A() {}
    }
}

class Q48 {
    public static void main(String[] args) {
        var numA = new Integer[]{1, 2};
        var list1 = new ArrayList<Integer>(List.of(numA));
        list1.add(null);
        System.out.println(list1);
        var list2 = Collections.unmodifiableList(list1);
        System.out.println(list2);
        list1.set(2, 3);
        System.out.println(list1);
        System.out.println(list2);
        List<List<Integer>> list3 = List.of(list1, list2);
        System.out.println(list3);
    }
}

class Q49FileCopier {
    public static void copy1(Path p1, Path p2) throws Exception {
        Files.copy(p1, p2, StandardCopyOption.COPY_ATTRIBUTES, StandardCopyOption.REPLACE_EXISTING);
    }

    public static void main(String[] args) throws Exception {
        Path p1 = Paths.get("tests/enthuware/test7/test.txt");
        Path p2 = Paths.get("tests/enthuware/test7/test2.txt");
        copy1(p1, p2);
        if (Files.isSameFile(p1, p2)) {
            System.out.println("file copied");
        } else {
            System.out.println("unable to copy file");
        }
    }
}


class Q51PlaceHolder<K, V> {
    private K k;
    private V v;

    public Q51PlaceHolder(K k, V v) {
        this.k = k;
        this.v = v;
    }

    public K getK() {
        return k;
    }

    public static <X> Q51PlaceHolder<X, X> getDuplicateHolder(X x) {
        return new Q51PlaceHolder<X, X>(x, x);
    }

    public static void main(String[] args) {
        Q51PlaceHolder<String, String> ph1 = Q51PlaceHolder.getDuplicateHolder("b"); //1
        Q51PlaceHolder<String, String> ph2 = Q51PlaceHolder.<String>getDuplicateHolder("b"); //2
        //Q51PlaceHolder<String, String> ph3 = Q51PlaceHolder<>.getDuplicateHolder("b"); //3
        //Q51PlaceHolder<> ph4 = new Q51PlaceHolder<String, String>("a", "b"); //4
        Q51PlaceHolder<?, ?> ph5 = new Q51PlaceHolder(10, 10); //5
        Q51PlaceHolder<?, ?> ph6 = new Q51PlaceHolder<Integer, Integer>(10, 10); //5
    } }