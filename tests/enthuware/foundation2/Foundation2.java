package foundation2;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

public class Foundation2 {
}

class Q1 {
    public static void main(String[] args) throws Exception {
        LocalDate d = LocalDate.of(2020, 1, 2);
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy MMM dd");
        System.out.println(d.format(df));
        System.out.println(d.withYear(2024).format(df));
        Integer i;
    }
}

class Q2 {
    public static void main(String[] args) {
    }

    public Object myM() {
        return (int) 1;
    }
}

class Q3TestClass extends Thread {
    static Object lock1 = new Object();
    static Object lock2 = new Object();
    static volatile int i1, i2, j1, j2, k1, k2;

    public void run() {
        while (true) {
            workWithLocks();
            workWithoutLocks();
        }
    }

    void workWithLocks() {
        synchronized (lock1) {
            i1++;
            i2++;
        }
        synchronized (lock2) {
            k1++;
            k2++;
        }
        j1++;
        j2++;
    }

    void workWithoutLocks() {
        if (i1 != i2) System.out.println("i");
        if (j1 != j2) System.out.println("j");
        if (k1 != k2) System.out.println("k");
    }

    public static void main(String args[]) {
        new Q3TestClass().start();
        new Q3TestClass().start();
    }
}

class Q25 {
    public static void main(String[] args) {
        var rlock = new ReentrantLock();
        //var f1 = rlock.lock();
        var f1 = rlock.tryLock();
        System.out.println(f1);
        //var f2 = rlock.lock();
        var f2 = rlock.tryLock();
        System.out.println(f2);
    }
}

class Q31 {
    public static void main(String[] args) {
        int hex = 0xCAFE_BABE;
        System.out.println(hex);
        float f = 9898_7878.333_333f;
        System.out.println(f);
        int bin = 0b1111_0000_1100_1100;
        System.out.println(bin);
    }
}

class Q40TestClass {
    public static void main(String[] args) {
        try {
            System.exit(0);
        } finally {
            System.out.println("finally is always executed!");
        }
    }
}

record Q42Journal(int id, String... values) {
    private static final int i = 0;

    void test() {
    }
}

class Q47 {
    public static void main(String[] args) {
        Q47 q47 = new Q47();
        System.out.println(q47.printMe(1));
    }

    int printMe(int x) {
        System.out.println(~x);
        System.out.println(0 ^ x);
        int y = x ^ ~x;
        return y;
    }

}

class Q52Conversion {
    public static void main(String[] args) {
        int i = 1234567890;
        float f = i;
        System.out.println(f);
        System.out.println(i - (int) f);
    }
}

class Q53 {
    public static void main(String[] args) {
        LocalDate ld = LocalDate.of(2024, Month.SEPTEMBER, 7);
        LocalDateTime ldt = LocalDateTime.of(2015, Month.JANUARY, 1, 21, 10); //9.10 PM
        LocalDate sunday = ld.with(java.time.temporal.TemporalAdjusters.next(DayOfWeek.SUNDAY));
        System.out.println(sunday);

        LocalDate d2 = (LocalDate) DateTimeFormatter.ISO_LOCAL_DATE.parse("2015-01-01"); //will compile
        // but may or may not throw a ClassCastException at runtime. You should do like this -
        d2 = LocalDate.from(DateTimeFormatter.ISO_LOCAL_DATE.parse("2015-01-01"));
        System.out.println(d2);
    }
}

abstract class Q54SomeClass {
    public abstract void m1();
}
class Q54TestClass
{
    public static Q54SomeClass getSomeClass()  //note static
    {
        return new Q54SomeClass()  //This declares as well as instantiates
                // an anonymous class that extends SomeClass
        {
            public void m1() {
            }
        };
    }
}