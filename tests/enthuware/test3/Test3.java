package test3;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.zip.ZipFile;

public class Test3 {
}

class Q1 {
    public static void main(String[] args) {
        Map<String, String> m = new HashMap<>();
        System.out.println(m.put("A", "A"));
        System.out.println(m.put("A", "B"));
    }
}

class Q2 {
    public static void main(String[] args) {
        Stream<Integer> strm1 = Stream.of(2, 3, 5, 7, 11, 13);
        double av = strm1.filter(x -> {
                    if (x > 10) return true;
                    else return false;
                }) //1
                .peek(a -> {
                }) //2
                .collect(Collectors.averagingInt(y -> y)); //3
        System.out.println(av);
    }
}

class Q3 {
    public Connection getConnection1(String url, String userid, String pwd) throws Exception {
        Properties p = new Properties();
        p.setProperty("user", userid);
        p.setProperty("password", pwd);
        DriverManager.registerDriver(DriverManager.getDriver("com.xyz.Driver"));
        return DriverManager.getConnection(url, p);
    }
}

class Q4 {
    public static void main(String[] args) {
        Path p1 = Paths.get("/Users/admin/IdeaProjects/temp/test.txt");
        Path p2 = Paths.get("report.pdf");
        System.out.println(p1.resolve(p2));
        Path p3 = Paths.get("/Users/admin/IdeaProjects/temp/report.pdf");
        System.out.println(p1.resolve(p3));
    }
}

class Q5Base {
    public <T extends Number, Z extends Number> Map<T, Z> getMap(T t, Z z) {
        return new HashMap<T, Z>();
    }

    public List<? extends CharSequence> test(Set<Integer> t) {
        return new ArrayList<>();
    }

    public List<? extends Number> test2(Set<Integer> t) {
        return new ArrayList<>();
    }

    public List<? super Integer> test3(Set<Integer> t) {
        return new ArrayList<>();
    }
}

class Q5Derived extends Q5Base {
    public <T, Z> TreeMap<T, Z> getMap(T t, Z z) {
        return new TreeMap<T, Z>();
    } //1

    public Map<Number, Number> getMap(Number t, Number z) {
        return new TreeMap<Number, Number>();
    }

    @Override
    public ArrayList<CharSequence> test(Set t) {
        super.test(t);
        return null;
    }

    @Override
    public List<? extends Integer> test2(Set t) {
        super.test(t);
        return null;
    }

    @Override
    public List<? super Integer> test3(Set t) {
        super.test(t);
        return null;
    }

    //public Map<Integer, Integer> getMap(Number t, Number z) {
    //  return new HashMap<Integer, Integer>();
    //}
}

//

class Q7 {
    public static void main(String[] args) throws SQLException {
        Connection c = DriverManager.getConnection(""); //get connection
        String qr = "insert into STOCK ( ID, TICKER, LASTPRICE, EXCHANGE ) values( ?, ?, ?, ?)";
        PreparedStatement ps = c.prepareStatement(qr);
        ps.setInt(1, 1);
        ps.setString(2, "APPL");
        //INSERT CODE HERE
        ps.setNull(3, Types.INTEGER);
        ps.setString(4, null);

        //ps.setNull(3, JDBCType.INTEGER);
        //ps.setNull(4, JDBCType.VARCHAR);

        ps.setNull(3, Types.INTEGER);
        ps.setNull(4, Types.VARCHAR);

        int i = ps.executeUpdate();  //1
        System.out.println(i);
    }
}

class Q10TestClass {
    int i;

    public Q10TestClass(int i) {
        this.i = i;
    }

    public String toString() {
        if (i == 0) return null;
        else return "" + i;
    }

    public static void main(String[] args) {
        Q10TestClass t1 = new Q10TestClass(0);
        Q10TestClass t2 = new Q10TestClass(2);
        System.out.println(t2);
        System.out.println("" + t1);
    }
}

//

class Q14 {
    public static void main(String[] args) {
        List<String> vowels = new ArrayList<String>();
        vowels.add("a");
        vowels.add("e");
        vowels.add("i");
        vowels.add("o");
        vowels.add("u");
        Function<List<String>, List<String>> f = list -> list.subList(2, 4);
        System.out.println(f.apply(vowels));
        vowels.forEach(System.out::print);
    }
}

//
class Q15 {
    public static void main(String[] args) throws IOException {
        BufferedWriter resource2 = Files.newBufferedWriter(Path.of(""));
        try (resource2) {
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        //resource2 = null;
    }
}

//

class Q16 {
    public static void main(String[] args) {

        List<String> names = Arrays.asList("Peter", "Paul", "Pascal");
        //Optional<String> ops = names.stream().parallel().allMatch(name -> name != null).filter(name -> name.length() > 6).findAny();
        boolean ops = names.stream().parallel().allMatch(name -> name != null);
        System.out.println(ops);
    }
}

class Q19 {
    public static void main(String[] args) {
        int z = 'z';
        System.out.println(z);
    }

    void test(byte x) {
        switch (x) {
            case 'a' + 10:   // 1
                //case (byte) 256:   // 2
            case 0:     // 3
            default:   // 4
            case 80:    // 5
        }
    }
}

class Q24Switcher {
    public static void main(String[] args) {
        switch (Integer.parseInt(args[1]))  //1
        {
            case 0:
                var b = false; //2
                break;
            case 1:
                b = true; // 3
                break;
        }
        //if(b) System.out.println(args[2]); //4
    }
}
//

class Q25 {
    public static void main(String[] args) {
        IntStream is1 = IntStream.of(1, 3, 5);  //1
        OptionalDouble x = is1.filter(i -> i % 2 == 0).average(); //2
        System.out.println(x); //3

        IntStream is2 = IntStream.of(2, 4, 6); //4
        int y = is2.filter(i -> i % 2 != 0).sum(); //5
        System.out.println(y); //6
    }
}

class Q27 {
    public static void main(String[] args) {
        String sentence = "Life is a box of chocolates, Forrest. You never know what you're gonna get."; //1
        boolean theword = Stream.of(sentence.split("[ ,.]")).anyMatch(w -> w.startsWith("g")); //2
        System.out.println(theword);
        Stream.of(sentence.split("[ ,.]")).forEach(System.out::println);
    }
}

class Q32Data implements Serializable {
    int id;
    transient LocalDate ld;

    private void readObject(ObjectInputStream stream) throws IOException, ClassNotFoundException {
        stream.defaultReadObject();
        ld = LocalDate.of(2020, 1, 1);
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Q32Data q32Data = new Q32Data();
        q32Data.readObject(new ObjectInputStream(new FileInputStream("tests/enthuware/test3/Test3.java")));
    }
}

//

class Q37TestClass {
    int x = 5;

    int getX() {
        return x;
    }

    public static void main(String args[]) throws Exception {
        Q37TestClass tc = new Q37TestClass();
        tc.looper();
        System.out.println(tc.x);
    }

    public void looper() {
        //var x = 0;
        while ((x = getX()) != 0) {
            for (int m = 10; m >= 0; m--) {
                x = m;
            }
        }

    }
}

//

class Q40TestClass {
    public static void main(String args[]) {
        int x = 0;
        labelA:
        for (var i = 10; i < 0; i--) {
            var j = 0;
            labelB:
            while (j < 10) {
                if (j > i) break labelB;
                if (i == j) {
                    x++;
                    continue labelA;
                }
                j++;
            }
            x--;
        }
        System.out.println(x);
    }
}

class Q41 {
    class A {
    }

    class B extends A {
    }

    public static void main(String[] args) {
        B b = new Q41().new B();
        Object a = 4;
        if (a instanceof Integer i) {
            System.out.println(true);
        }
        if (a instanceof Object) {
            System.out.println(true);
        }
        if (b instanceof A) {
            System.out.println(true);
        }
    }
}

class Q44 {
    public static void main(String[] args) {

        List<Integer> ls = Arrays.asList(3, 4, 6, 9, 2, 5, 7);
        System.out.println(ls.stream().reduce(Integer.MIN_VALUE, (a, b) -> a > b ? a : b)); //1
        System.out.println(ls.stream().max(Integer::max).get()); //2
        System.out.println(ls.stream().max(Integer::compare).get()); //3
        System.out.println(ls.stream().max((a, b) -> a > b ? a : b)); //4
    }
}

//

interface Q46Base {
    static void m() {
    }

    void n();

    static void x() {
    }

    void y();
}

interface Q46Sub extends Q46Base {
    default void m() {
    } //WILL NOT COMPILE

    //static void n(); //WILL NOT COMPILE
    static void x() {
    } //VALID, x() of base is hidden

    void y(); //VALID, y() of base is overridden
}

//

class Q50StringFromChar {
    public static void main(String[] args) {
        String myStr = "good";
        char[] myCharArr = {'g', 'o', 'o', 'd'};
        String newStr = "";
        for (char ch : myCharArr) {
            newStr = newStr + ch;
        }
        System.out.println(newStr);
        System.out.println(myStr);
        boolean b1 = newStr == myStr;
        boolean b2 = newStr.equals(myStr);
        System.out.println(b1 + " " + b2);
    }
}

class Q51 {
    public static void main(String[] args) {
        System.out.println("" + true); //1
        System.out.println(true + ""); //1
        System.out.println((Boolean) (null) + ""); //1
        //System.out.println(null + true); //1
        //System.out.println(true + null); //2
        //System.out.println(null + null); //3  Which of the following statements are correct?
    }
}

class Q54 {
    public static void main(String[] args) {
        LocalDateTime ld1 = LocalDateTime.of(2022, Month.NOVEMBER, 6, 2, 0);
        ZonedDateTime zd1 = ZonedDateTime.of(ld1, ZoneId.of("US/Eastern"));
        LocalDateTime ld2 = LocalDateTime.of(2022, Month.NOVEMBER, 6, 1, 0);
        ZonedDateTime zd2 = ZonedDateTime.of(ld2, ZoneId.of("US/Eastern"));
        long x = ChronoUnit.HOURS.between(zd1, zd2);
        System.out.println(zd1);
        System.out.println(zd2);
        System.out.println(x);
    }
}