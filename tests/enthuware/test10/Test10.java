package test10;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class Test10 {
}

class Q1 {
    public static void main(String[] args) throws SQLException {
        Statement statement = DriverManager.getConnection("").createStatement();
    }
}

class Q2PlaceHolder<K, V> {
    private K k;
    private V v;

    public Q2PlaceHolder(K k, V v) {
        this.k = k;
        this.v = v;
    }

    public K getK() {
        return k;
    }

    public static <X> Q2PlaceHolder<X, X> getDuplicateHolder(X x) {
        return new Q2PlaceHolder<X, X>(x, x);
    }
}

class Q3 {
    public static void main(String[] args) {
        List s1 = new ArrayList();
        s1.add("a");
        s1.add("b");
        s1.add("c");
        s1.add("a");
        if (s1.remove("a")) {
            if (s1.remove("a")) {
                s1.remove("b");
            } else {
                s1.remove("c");
            }
        }
        System.out.println(s1);
    }
}

interface Q4Bar {
    void bar();
}

abstract class Q4FooBase {
    private static void bar() {
        System.out.println("In static bar");
    }
}

class Q4Foo extends Q4FooBase implements Q4Bar {
    public void bar() {
    }

    ;
}
//

enum Q5Card {
    HEART, CLUB, SPADE, DIAMOND;

    public boolean isRed() {
        return switch (this) {
            case HEART, DIAMOND -> true;
            default -> false;
        };
    }

    public static void main(String[] args) {
        Arrays.stream(Q5Card.values()).takeWhile(c -> c.isRed()).forEach(System.out::print);
    }
}
//

class Q8Data {
    int value;

    Q8Data(int value) {
        this.value = value;
    }

    public String toString() {
        return "" + value;
    }

    public static void main(String[] args) {
        Q8Data[] dataArr = new Q8Data[]{new Q8Data(1), new Q8Data(2), new Q8Data(3), new Q8Data(4)};
        List<Q8Data> dataList = Arrays.asList(dataArr);
        for (Q8Data element : dataList) {
            System.out.println(dataList.removeIf((Q8Data d) -> {
                return d.value % 2 == 0;
            }));
            System.out.println("Removed " + element + ", ");
        }
    }
}

class Q13 {
    public static void main(String[] args) {
        var numA = new Integer[]{1, 0, 3};
        var list1 = new ArrayList<>(List.of(numA));
        List.copyOf(list1);
        var list2 = Collections.unmodifiableList(list1);
        list2.remove((Integer) 0);
        numA[1] = 2;
        System.out.println(list1 + " " + list2);
        System.out.println(Arrays.toString(numA));
    }
}

class Q17Test extends Thread {
    boolean flag = false;

    public Q17Test(boolean f) {
        flag = f;
    }

    static Object obj1 = new Object();
    static Object obj2 = new Object();

    public void m1() {
        synchronized (obj1) {
            System.out.print("1 ");
            synchronized (obj2) {
                System.out.println("2");
            }
        }
    }

    public void m2() {
        synchronized (obj2) {
            System.out.print("2 ");
            synchronized (obj1) {
                System.out.println("1");
            }
        }
    }

    @Override
    public void run() {
        if (flag) {
            m1();
            m2();
        } else {
            m2();
            m1();
        }
    }

    public static void main(String[] args) {
        new Q17Test(true).start();
        new Q17Test(false).start();
    }
}

//

class Q19Cache {
    static ConcurrentHashMap<String, Object> chm = new ConcurrentHashMap<String, Object>();

    public static void main(String[] args) {
        chm.put("a", "aaa");
        chm.put("b", "bbb");
        chm.put("c", "ccc");
        new Thread() {
            public void run() {
                Iterator<Map.Entry<String, Object>> it = Q19Cache.chm.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry<String, Object> en = it.next();
                    if (en.getKey().equals("a") || en.getKey().equals("b")) {
                        it.remove();
                    }
                }
            }
        }.start();
        new Thread() {
            public void run() {
                Iterator<Map.Entry<String, Object>> it = Q19Cache.chm.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry<String, Object> en = it.next();
                    System.out.print(en.getKey() + ", ");
                }
            }
        }.start();
    }
}
//

class Q21Super {
    static {
        System.out.print("super ");
    }
}

class Q21One {
    static {
        System.out.print("one ");
    }
}

class Q21Two extends Q21Super {
    static {
        System.out.print("two ");
    }
}

class Test {
    public static void main(String[] args) {
        Q21One o = null;
        Q21Two t = new Q21Two();
    }
}
//

class Q22TestClass {
    public static void main(String[] args) {
        TreeSet<Integer> s = new TreeSet<Integer>();
        TreeSet<Integer> subs = new TreeSet<Integer>();
        for (int i = 324; i <= 328; i++) {
            s.add(i);
        }
        subs = (TreeSet) s.subSet(326, true, 330, true);
        subs.add(329);
        System.out.println(s + " " + subs);
    }
}

class Q29 {
    public static void main(String[] args) {
        System.out.println("Hello " + "world" == "Hello world");
    }
}

//
class Q30Onion {
    private String data = "skin";

    private class Layer extends Q30Onion {
        String data = "thegoodpart";

        public String getData() {
            return data;
        }
    }

    public String getData() {
        return new Layer().getData();
    }

    public static void main(String[] args) {
        var o = new Q30Onion();
        System.out.println(o.getData());
    }
}
//

class Q31TestClass {
    public static void main(String[] args) {
        var hasParams = (args == null ? false : true);
        if (hasParams) {
            System.out.println("has params");
        }
        {
            System.out.println("no params");
        }
    }
}

class Q32 {
    public static void main(String[] args) {
        System.out.println(Double.valueOf(Math.pow(2, 16)).intValue() - 1);
    }
}

class Q36Course {
    private String id;
    private String category;

    public Q36Course(String id, String category) {
        this.id = id;
        this.category = category;
    }

    public String toString() {
        return id + " " + category;
    }

    public String getCategory() {
        return category;
    }

    public static void main(String[] args) {
        List<Q36Course> s1 = Arrays.asList(
                new Q36Course("OCAJP", "Java"),
                new Q36Course("OCPJP", "Java"),
                new Q36Course("C#", "C#"),
                new Q36Course("OCEJPA", "Java"));

        Map<String, List<Q36Course>> collect = s1.stream()
                .collect(Collectors.groupingBy(c -> c.getCategory()));
        System.out.println(collect);
        collect.forEach((m, n) -> System.out.println(n));
    }
}

class Q40LineByLineProcessor {
    public void processLines(String fullFilePath) throws Exception {
        BufferedReader handle = new BufferedReader(new FileReader(fullFilePath));
        handle = new BufferedReader(new FileReader(new File(fullFilePath)));
        String str = null;
        while ((str = handle.readLine()) != null) {
            System.out.println("Processing line : " + str);
        }
        handle.close();
    }
}

class Q44 {
    public static void main(String[] args) {
        long g = 012;
        System.out.println(g);
    }
}
//

class Q47TestClass {
    public void method(Object o) {
        System.out.println("Object Version");
    }

    //public void method(java.io.FileNotFoundException s) {
    //  System.out.println("java.io.FileNotFoundException Version");
    //}

    //public void method(java.io.IOException s) {
    //  System.out.println("IOException Version");
    //}

    public void method(String s) {
        System.out.println("String Version");
    }

    //public void method(StringBuffer s) {
    //  System.out.println("StringBuffer Version");
    //}

    public static void main(String args[]) {
        Q47TestClass tc = new Q47TestClass();
        tc.method(null);
    }
}

class Q51 {
    public static void main(String[] args) {
        Integer i1 = 20_00;
        Integer r = 100;
        r = +i1;
        System.out.println(r); //This will print 2100
    }
}