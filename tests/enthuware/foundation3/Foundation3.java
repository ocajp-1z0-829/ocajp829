package foundation3;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Stream;

public class Foundation3 {
}

class Q1A  // outer class
{
    static public class B //static nested class.
            //It can be used in other places: A.B b = new A.B(); There is no outer instance.
    {
    }

    class C //Inner class. It can only be used like this:
            // A.C c = new A().new C(); Outer instance is needed.
    {
    }
}

interface Q1I1 {
    public abstract void mA();

    public static interface InnerI1 {
        public static final int k = 10;

        public abstract void innerA();
    }
}

class Q1MyClass implements Q1I1.InnerI1 {
    @Override
    public void innerA() {

    }
}

record Q2Student(int id, String name) {
    public Q2Student() throws Exception { //a non-canonical constructor
        this(10); //this line or a call to the canonical constructor is required
    }

    public Q2Student(int id) throws Exception { //another non-canonical constructor
        this(id, ""); //this line is required
    }
/*    public Q2Student(int id, String name){ //regular form canonical constructor
        this.id = id; this.name=name;
    }*/
}

class Base {
    final int i = 10;
    static int x = 30;
}

class Sub extends Base {
    int i = 20; //This i hides Base's i.
    static int x = 40; //This x hides Base's x.
}

class Q5 {
    public static void main(String[] args) {
        Sub s = new Sub();
        int k = ((Base) s).i; //assigns 20 to k.
        k = s.i; //assigns 10 to k.
        System.out.println(k);

        Base b = new Sub();
        k = b.i; // assigns 10 to k because
        System.out.println(k);
    }

}

class Q8 {
    void crazyLoop() {
        var c = 0;
        JACK:
        while (c < 8) {
            JILL:
            System.out.println(c);
            if (c > 3) break JACK;
            else {
                c++;
            }
        }
    }

    public static void main(String[] args) {
        Q8 q8 = new Q8();
        q8.crazyLoop();
    }
}

class Q11 {
    public static void main(String[] args) {
        int i = 1 >> 2;
        System.out.println(i);
    }
}

class Q12 {
    public static void main(String[] args) {
        ExecutorService es;
    }
}

class Q18 {
    public static void main(String[] args) {
        Locale myLoc = new Locale("fr", "FR");
        ResourceBundle rb = ResourceBundle.getBundle("appmessages", myLoc);
        rb = ResourceBundle.getBundle("appmessages", new Locale("ch", "CH"));
        rb = ResourceBundle.getBundle("appmessages", Locale.CHINA);
    }
}

class Q20 {
    public static void main(String[] args) {
        System.out.println(Stream.of("a", "b", "c").max(String::compareTo).get());
    }
}

class Q28 {
    public static void main(String[] args) {
        long lv = Integer.valueOf(10);
        System.out.println(lv);
    }
}

class Q29 {
    public static void main(String[] args) {
        ExecutorService s = Executors.newCachedThreadPool();
    }
}

class Q30 {
    public static void main(String[] args) {
        System.out.println(NumberFormat.getCompactNumberInstance().format(2222));
        System.out.println(DateFormat.getDateTimeInstance().format("1011-11-11"));
    }
}

class Q33 {
    public static <E extends CharSequence> List<? super E> doIt(List<E> nums) {
        return nums;
    }

    public static void main(String[] args) {
        List<String> i = new ArrayList<>();
        i.add("A");
        ArrayList<String> in = new ArrayList<>();
        List result = doIt(in);
        System.out.println(result);
        //List result1 = doIt(i);
        //Object result1 = doIt(i);
        List<? super String> result1 = doIt(i);
        System.out.println(result1);
    }
}

class Q34 {
    public static void main(String[] args) {
        Map<String, String> m = new HashMap<>(Map.of("A", "A"));
        System.out.println(m.containsKey("A"));
        System.out.println(m.values());
        System.out.println(m.keySet());
        System.out.println(m.remove("A"));
        Throwable t;
        Error e;
    }
}

class Q37 {
    public static void main(String[] args) {
        int y = switch (args.length) {
            //case 0 -> yield 10; //compilation error, yield is not allowed here
            case 0 -> 10; //OK, return without yield
            //case 1 -> { 20; } //compilation error, yield is required inside the block
            default -> {
                yield 20;
            } //yield is required inside the block };
        };

        char ch = switch (1) {
            //case 0 -> //compilation error, missing expression or block
            case 1, 2, 3 -> 'A'; //OK, will return 'A' when someIntVar is 1, 2, or 3.
            case 4 -> throw new RuntimeException();
            default -> 'B';//OK
        };

        ch = switch (1) {
            case 0: //ok to have empty case statements
            case 1:
            case 2:
            case 3:
                yield 'A'; //yield is required
            default:
                yield 'B';
        };

        int j = switch(2) { //here, someIntVar is the "selector expression"
            case 0: { yield 0; }  //a block containing a yield statement;
            //case 1: 2; //it is a valid expression statement
                case 1: yield 2+3; //this line is NOT valid because 2+3 is not a valid expression statement
            default: yield 5; //using yield as a expression statement directly
        }; //semi-colon is required here

        Integer x = 1;
        Object o = switch(x){
            case 1,2 -> "String";
            default -> 200;
        };

        switch(x) {
            case 1, 2 -> { //case block
                System.out.println("A");
                break;//valid but redundant
            }
            case 3, 4 -> System.out.println("B"); //will not be printed even if the break statement
            //in the previous block is commented out
            //break; //error. break is not allowed outside a case block
            default -> System.out.println("C");
        }
    }
}

class Q39 {
    public static void main(String[] args) {
        System.out.println("12345 ".repeat(2));
        System.out.println(" 123 45 ".strip());
        System.out.println(" 123 45 ".stripIndent());
    }
}

class Q45MultipleReadersSingleWriter {

    private final ArrayList<String> theList = new ArrayList<String>();
    //Note that ReadWriteLock is an interface.
    private final ReadWriteLock rwl = new ReentrantReadWriteLock();
    private final Lock r = rwl.readLock();
    private final Lock w = rwl.writeLock();

    protected class A {}
    public String read(){
        r.lock();
        try{
            System.out.println("reading");
            if(theList.isEmpty()) return null;
            else return theList.get(0);
        }finally{
            r.unlock();
        }
    }

    public void write(String data){
        w.lock();
        try{
            System.out.println("Written "+data);
            theList.add(data);
        }finally{
            w.unlock();
        }
    }
}