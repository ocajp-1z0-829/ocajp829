package test12;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Test12 {
}

class Q1 {
    public static void main(String[] args) {
        ServiceLoader<String> bsLoader = ServiceLoader.load(String.class);
        bsLoader.findFirst().ifPresent(System.out::println);
        List<String> strings = new ArrayList<>();
        strings.stream().findFirst();
    }
}

class Q2 {
    public static void main(String[] args) {
        int[] ia1 = {0, 1, 4, 5};
        int[] ia2 = {0, 1, 1, 5, 6};
        int x = Arrays.compare(ia1, ia2);
        int y = Arrays.mismatch(ia1, ia2);
        System.out.println(x + " " + y);
    }
}

class Q4 {
    public static void main(String[] args) {
        var i = 4;
        int ia[][][] = new int[i][i = 3][i];
        System.out.println(Arrays.deepToString(ia));
        ia[0][0][0] = 1;
        System.out.println(Arrays.deepToString(ia));
        System.out.println(ia.length + ", " + ia[0].length + ", " + ia[0][0].length + ", " + ia[0][0][0]);
    }
}

class Q6Book {
    private String title;
    private Double price;

    public Q6Book(String title, Double price) {
        this.title = title;
        this.price = price;
    }

    public Double getPrice() {
        return price;
    }

    public String getTitle() {
        return title;
    }

    public static void main(String[] args) {
        Q6Book b1 = new Q6Book("Java in 24 hrs", 30.0);
        Q6Book b2 = new Q6Book("Thinking in Java", null);
        Supplier s1 = b1::getPrice;
        System.out.println(b1.getTitle() + " " + s1.get());
        Supplier s2 = b2::getPrice;
        System.out.println(b2.getTitle() + " " + s2.get());
    }
}

class Q7 {
    public static void main(String[] args) throws IOException {
        var fw = new FileWriter("tests/enthuware/test12/test.txt");
        var bfw = new BufferedWriter(fw);
        //bfw.writeUTF("hello");
        bfw.newLine();
        bfw.write("world");  //3
        bfw.close();
        fw.close();

        OutputStreamWriter osw = new OutputStreamWriter(
                new FileOutputStream("utf8.txt"),
                Charset.forName("UTF-8").newEncoder()
        );
    }
}

class Q8 {
    public static void main(String[] args) {
        Path d1 = Paths.get("works");
        Path d2 = d1.resolve("ocpjp/code");
        System.out.println(d1.resolve("ocpjp/code/sample"));
        System.out.println(d1.toAbsolutePath());
        System.out.println(d1);
        System.out.println(d2);
    }
}

class Q11Booby {
}

class Q11Dooby extends Q11Booby {
}

class Q11Tooby extends Q11Dooby {
}

class Q11 {
    public static void main(String[] args) {
        List<? super Q11Booby> bV = null;
        List<? extends Q11Tooby> tV = null;
        bV.add(tV.get(0));
        //tV.add( bV.get(0) );
    }
}

class Q12ResultWithoutBlockingExample {

    public static void main(String[] args) throws Exception {
        //create a thread pool of two threads
        ExecutorService es = Executors.newFixedThreadPool(2);

        MyTask task1 = new MyTask();
        Future<String> result = es.submit(task1);
        System.out.println("Proceeding without blocking... ");
        while (!result.isDone()) {
            try {
                //check later
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
        System.out.println("Result is " + result.get());
        es.shutdown();
    }

    public static class MyTask implements Callable<String> {
        public String call() {
            try {
                //simulate a long running task;
                Thread.sleep(2000);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
            return "Data from " + Thread.currentThread().getName();
        }
    }
}
//

class Q18Book implements Comparable<Q18Book> {
    String isbn;
    String title;

    public Q18Book(String isbn, String title) {
        this.isbn = isbn;
        this.title = title;
    }

    public int compareTo(Q18Book b) {
        return this.isbn.compareTo(b.isbn);
    }

    public String getTitle() {
        return title;
    }

    public static void main(String[] args) {
        List<Q18Book> books = new ArrayList<>();
        Collections.sort(books, (b1, b2) -> b1.getTitle().compareTo(b2.getTitle()));
        Collections.sort(books);
    }
}

class Q19Request {
}

class Q19RequestCollector {
    //Queue<Q19Request> container = new LinkedList<Q19Request>();
    Queue<Q19Request> container2 = new PriorityQueue();

    public synchronized void addRequest(Q19Request r) {
        container2.add(r);
    }

    public synchronized Q19Request getRequestToProcess() {
        return container2.poll();
    }
}

class Q20A1 {
    static int i = 10;

    static {
        System.out.println("A1 Loaded ");
    }
}

class Q20A {
    static {
        System.out.println("A Loaded ");
    }

    public static void main(String[] args) {
        System.out.println(" A should have been loaded");
        Q20A1 a1 = null;
        System.out.println(" A1 should not have been loaded");
        System.out.println(a1.i);
    }
}

class Q22 {
    public static void main(String[] args) {
        Locale.setDefault(Locale.Category.FORMAT, Locale.US);
        Locale.setDefault(Locale.US);
    }
}

class Q23 {
    public static void m1() {
    }

    public static void main(String[] args) {
        short x = 3;
        x = (short) (x + 4.6);
    }
}

class Q26 extends Q23 {
    public static void m1() {
    }

    public static void main(String[] args) {
    }
}

class Q27 {
    enum Coffee {
        ESPRESSO("Very Strong"), MOCHA("Bold"), LATTE("Mild");
        public String strength;

        Coffee(String strength) {
            this.strength = strength;
        }

        public static void main(String[] args) {
            System.out.println(Coffee.ESPRESSO);
            System.out.println(String.valueOf(Coffee.ESPRESSO.strength));
        }

        //public String toString(){return String.valueOf(Coffee.ESPRESSO);}
        public String toString() {
            return Coffee.values()[0].strength;
        }
        /*@Override
        public String toString() {
            return  strength;
        }*/
    }
}

class Q28 {
    public static void main(String[] args) throws Exception {
        new Q28().countLines("tests/enthuware/test12/test.txt");
    }

    public void countLines(String filePath) throws Exception {
        Stream<String> ref = new BufferedReader(new FileReader(filePath)).lines();
        System.out.println(ref.count());

        Stream<String> ref2 = Files.lines(Paths.get(filePath));
        System.out.println(ref2.collect(Collectors.counting()));

    }
}

class Q30 {
    public static void main(String[] args) {
        List<String> strings = new ArrayList<>();
        strings.add("A");
        strings.get(0);

        LinkedList<Object> objects = new LinkedList<>();
        objects.add("B");
        System.out.println(objects.get(0));
    }
}

class Q33 {
    public static void main(String[] args) {
        boolean b1 = false;
        boolean b2 = false;
        if (b2 != (b1 = !b2)) {
            System.out.println("true");
        } else {
            System.out.println("false");
        }
    }
}

class Q36MyGenericClass<T> {
    public <T> String transform(T t) {
        return t.toString() + "-" + t.hashCode();
    }

    public static void main(String[] args) {
        Q36MyGenericClass gc = new Q36MyGenericClass();
        System.out.println(gc.transform(1));
        System.out.println(gc.transform("hello"));
        Q36MyGenericClass<String> gcStr = new Q36MyGenericClass<String>();
        System.out.println(gcStr.transform(1.1));
    }
}

class Q38 {
    public static void main(String[] args) {
        Instant ins = Instant.now();
        ZoneOffset india = ZoneOffset.ofHoursMinutes(5, 30);
        System.out.println(ins.atOffset(india).toZonedDateTime());
    }
}

class Q40Book {
}

class Q40TextBook extends Q40Book {
}

class Q40BookList extends ArrayList<Q40Book> {
    public int count = 0;

    @Override
    public boolean add(Q40Book o) {
        return super.add(o);
    }

    public static void main(String[] args) {
        Q40BookList list = new Q40BookList();
        list.add(new Q40Book());
        list.add(new Q40TextBook());
        //list.add("hello");
        System.out.println(list.size());
        System.out.println(list.count);
    }
}
//

interface Q44Account {
    public default String getId() {
        return "0000";
    }
}

interface Q44PremiumAccount extends Q44Account {
    //public default String getId() {
    //  return Q44Account.super.getId();
    //}
    public String getId();
}

class Q44BankAccount implements Q44PremiumAccount {
    public static void main(String[] args) {
        Q44Account acct = new Q44BankAccount();
        System.out.println(acct.getId());
    }

    @Override
    public String getId() {
        //return Q44Account.super.;
        return "";
    }
}
//

class Booby {
}

class Dooby extends Booby {
}

class Tooby extends Dooby {
}

class TestClass {
    Booby b = new Booby();
    Tooby t = new Tooby();
    Dooby d = new Dooby();

    public void do1(List<? super Dooby> dataList) {
        Object o = dataList.get(0);
        //b = dataList.get(0);
        //t = dataList.get(0);
        //d = dataList.get(0);
        //dataList.add(b);
        dataList.add(t);
        dataList.add(d);
    }

    public void do2(List<? extends Dooby> dataList) {
        b = dataList.get(0);
        d = dataList.get(0);
        //t = dataList.get(0);
        //dataList.add(b);
        //dataList.add(t);
        //dataList.add(d);
    }
}
//

class Q50Discounter {
    static double percent;
    int offset = 10, base = 50;

    public static double calc(double value) {
        //var coupon, offset, base;
        int coupon, offset, base;
        if (percent < 10) {
            coupon = 15;
            offset = 20;
            base = 10;
        }
        return /*coupon * offset * base */ value / 100;
    }

    public static void main(String[] args) {
        System.out.println(calc(100));
    }
}
//

class Q51 {
    public static String hidePhone(String fullPhoneNumber) {
        return new StringBuilder("xxx-xxx-") + fullPhoneNumber.substring(8);
    }

    public static String hidePhone(String fullPhoneNumber, int i) {
        return new StringBuilder(fullPhoneNumber).replace(0, 7, "xxx-xxx-").toString();
    }

    public static String hidePhone(String fullPhoneNumber, boolean b) {
        return "xxx-xxx-" + fullPhoneNumber.substring(8, 12);
    }

    public static void main(String[] args) {
        System.out.println(hidePhone("123-456-7890"));
        System.out.println(hidePhone("123-456-7890", 1));
        System.out.println(hidePhone("123-456-7890", true));
        System.out.println(new StringBuilder("12345").append("67890", 2, 4));
    }
}