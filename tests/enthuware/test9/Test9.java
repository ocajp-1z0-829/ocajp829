package test9;

import test9.a.Q17A;

import java.io.*;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.time.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Test9 {
}

class Q1Counter<Z> {
    Z t;

    public <T extends Z> int count(T[] ta, T t) {
        this.t = t;
        int count = 0;
        for (T x : ta) {
            count = x == t ? count + 1 : count;
        }
        return count;
    }

    public int count2(Z[] ta, Z t) {
        this.t = t;
        int count = 0;
        for (Z x : ta) {
            count = x == t ? count + 1 : count;
        }
        return count;
    }
}

//
interface Q3I1 {
    void m1() throws java.io.IOException;
}

interface Q3I2 {
    void m1() throws java.sql.SQLException;
}

class Q3 implements Q3I1, Q3I2 {
    public static void main(String args[]) throws IOException, SQLException {
        Q3 tc = new Q3();
        Q3I1 i1 = (Q3I1) tc; //This is valid.
        i1.m1();

        Q3I2 i2 = (Q3I2) tc; //This is valid too.
        i2.m1();

    }

    public void m1() {
        System.out.println("Hi there");
    }
}

class Q7Test {
    public static void main(String[] args) {
        int j = 1;
        try {
            int i = doIt() / (j = 2);
        } catch (Exception e) {
            System.out.println(" j = " + j);
        }
    }

    public static int doIt() throws Exception {
        throw new Exception("FORGET IT");
    }
}

//
class Q10TestClass {
    public static boolean checkList(List list, Predicate<List> p) {
        return p.test(list);
    }

    public static void main(String[] args) {
        boolean b = checkList(new ArrayList(), al -> al.isEmpty());
        b = checkList(new ArrayList(), al -> al.add("hello"));
        //checkList(new ArrayList(), (ArrayList al) -> al.isEmpty());
        System.out.println(b);
    }
}

class Q11 {
    public float parseFloat(String s) {
        float f = 0.0f;
        try {
            f = Float.valueOf(s).floatValue();
            return f;
        } catch (NumberFormatException nfe) {
            f = Float.NaN;
            return f;
        } finally {
            f = 10.0f;
            return f;
        }
    }

    public static void main(String[] args) {
        System.out.println(new Q11().parseFloat("0"));
    }
}

class Q14TestClass {
    public static void main(String[] args) throws Exception {
        try (var bfr = new BufferedReader(new InputStreamReader(System.in))) {
            System.out.println("Enter Number:");
            var s = bfr.readLine();
            System.out.println("Your Number is : " + s);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

class Q15 {
    public static void main(String[] args) {
        int i = 2;
        int[][] twoD = {{1, 2, 3}, {4, 5, 6, 7}, {8, 9, 10}};
        System.out.print(twoD[1].length);
        System.out.print(twoD[2].getClass().isArray());
        System.out.print(twoD[1][2]);
        System.out.print(twoD[0] == twoD.clone()[0]);
    }
}

class Q16 {
    public static void main(String[] args) {
        Integer i = Integer.parseInt("10");
        Integer j = Integer.valueOf("10");
        Integer k = new Integer(10);
        System.out.println(i == j);
        System.out.println(i == k);

        Byte b = 1;
        Integer i1 = 1;
        //b == (Byte) i1;
    }
}
//

class Q17B extends Q17A {
    public static String name = "B";

    public Q17B() {
        super();
    }
}

class Q17TestClass {
    public static void main(String[] args) {
        Q17A a = new Q17B();
        // Q17A a1 = new Q17A();
        System.out.println(a.name);
    }
}
//

class Q19Employee {
    int age;
}

class Q19TestClass {
    public static boolean validateEmployee(Q19Employee e, Predicate<Q19Employee> p) {
        return p.test(e);
    }

    public static void main(String[] args) {
        Q19Employee e = new Q19Employee();
        System.out.println(validateEmployee(e, a -> a.age < 10000));
    }
}

class Q21 {
    public static void main(String[] args) {
        var nums = IntStream.range(1, 5);
        double average = nums.mapToObj(i -> i).collect(Collectors.averagingInt(i -> i));
        System.out.println(average);
        //average = nums.collect(Collectors.averagingInt(i->i));
        average = nums.average().getAsDouble();
        System.out.println(average);
        average = nums.parallel().mapToDouble(i -> i).average().getAsDouble();
    }
}

class Q22FileCopier {
    public static void copy(String records1, String records2) {
        try {
            InputStream is = new FileInputStream(records1);
            OutputStream os = new FileOutputStream(records2);
            byte[] buffer = new byte[1024];
            int bytesRead = 0;
            while ((bytesRead = is.read(buffer)) != -1) {
                os.write(buffer, 0, bytesRead);
                System.out.println("Read and written bytes " + bytesRead);
            }
        } catch (IndexOutOfBoundsException | IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        copy("tests/enthuware/test9/test.txt", "tests/enthuware/test9/test2.txt");
    }
}

class Q25InitTest {
    static int si = 10;
    int i;
    final boolean bool;

    {
        bool = (si > 5);
        i = 1000;
    }
}

interface Q27Measurement {
    public int getLength();

    public static int getBreadth() {
        return 0;
    }

    private void helper() {
    }
}

interface Q27Size extends Q27Measurement {
    private void helper() {
    }
}
//

class Q31 {
    public static void main(String[] args) {
        LocalDateTime ldt = LocalDateTime.of(2022, 06, 02, 6, 0, 0);  //Jun 2nd, 6AM.
        ZoneOffset nyOffset = ZoneOffset.ofHoursMinutes(-5, 0);
        ZoneId nyZone = ZoneId.of("America/New_York");
        OffsetDateTime nyOdt = ldt.atOffset(nyOffset);
        ZonedDateTime nyZdt = ldt.atZone(nyZone);
        System.out.println(nyOdt);
        System.out.println(nyZdt);
        Duration d = Duration.between(nyOdt, nyZdt);
        System.out.println(d);

    }
}

class Q32 {
    public static void main(String[] args) {
        Stream<String> ss = Stream.of("a", "b", "c", "d", "e");
        System.out.println(ss);
        Spliterator<String> sit1 = ss.spliterator();
        long s0 = sit1.estimateSize();
        Spliterator<String> sit2 = sit1.trySplit();
        System.out.println(sit2);
        long s1 = sit1.estimateSize();
        long s2 = sit2.estimateSize();
        System.out.println(s0 - (s1 + s2));
    }
}

class Q48A {
    protected void m() throws IOException {
    }
}

class Q48B extends Q48A {
    public void m() {
    }
}

class Q48TestClass {
    public static void main(String[] args) {
        Q48A a = new Q48B();
        ((Q48B) a).m();
        Q48A q = new Q48A();
        //a.m();
    }
}

class Q54TestClass {
    public static void main(String[] args) throws Exception {
        var f = new File("tests/enthuware/test9/test.txt");
        var bfr1 = new BufferedReader(new FileReader(f));
        var bfr2 = new BufferedReader(bfr1);
        System.out.println(bfr2.readLine());
        //var pw = new PrintWriter(new FileReader(f));
    }
}

class Q55 {
    public static void main(String[] a) throws Exception {
        Path dir = Paths.get("tests/enthuware/test9/a");
        try {
            DirectoryStream<Path> ds = Files.newDirectoryStream(dir, "*.{java,gif,jpeg}");
            for (Path p : ds) {
                System.out.println(p);
            }
            List l = new ArrayList();

            Files.newDirectoryStream(dir, "*.{}");
        } catch (
                Exception e) {
            e.printStackTrace();
        }
    }
}