package practiceBook.chapter1;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

public class Chapter1 { }

class Q12 {
    public static void main(String[] args) {
        double ceil = Math.ceil(65);
        int max = Math.max(7, 8);
        double pow = Math.pow(7, 3);
    }
}

class Q15 {
    public static void main(String[] args) {
        var montyPythonDay = LocalDate.of(2023, Month.MAY, 10);
        var aprilFools = LocalDate.of(2023, Month.APRIL, 1);
        var duration = Duration.ofDays(1);
        var result = montyPythonDay.minus(duration);
        System.out.println(result + " " + aprilFools.isBefore(result));
    }
}

class Q18 {
    public static void main(String[] args) {
        var localDate = LocalDate.of(2022, Month.NOVEMBER, 6);
        var localTime = LocalTime.of(1, 0);
        var zone = ZoneId.of("America/New_York");
        var z = ZonedDateTime.of(localDate, localTime, zone);
        var offset = z.getOffset();
        System.out.println(offset);
        for (int i = 0; i < 6; i++)
            z.plusHours(1);
        System.out.println(z.getHour() + " "
                + offset.equals(z.getOffset()));
        System.out.println(z);
        System.out.println(z.getOffset());
    }
}

class Q22 {
    public static void main(String[] args) {
        var text = """
            ant antelope \s \n
            cat "kitten" \
            seal sea lion
            """;
        System.out.print(text);
    }
}

class Q23 {
    public static void main(String[] args) {

    }
}

class Q24 {
    public static void main(String[] args) {
        LocalDate ld = LocalDate.of(2024, 1, 1);
        System.out.println(ld.getYear());
        System.out.println(ld.getEra());

        Period p = Period.of(2024, 1, 1);
        System.out.println(p.getYears());
    }
}

class Q42 {
    public static void main(String[] args) {
        var trainDay = LocalDate.of(2022, 5, 13);
        var time = LocalTime.of(10, 0);
        var zone = ZoneId.of("America/Los_Angeles");
        var zdt = ZonedDateTime.of(trainDay, time, zone);
        var instant = zdt.toInstant();
        instant = instant.plus(1, ChronoUnit.DAYS);
        System.out.println(instant);
    }
}

class Q44 {
    public static void main(String[] args) {
        //var math = new Math();
        var sum = 0;
        System.out.println(sum += Math.min(3, 5));
        System.out.println(sum += Math.floor(1.8));
        System.out.println(sum += Math.round(5.6));
        System.out.println(sum);
    }
}

class Q47 {
    public static void main(String[] args) {
        //var sb = new StringBuilder("radical")
          //      .insert(sb.length(), "robots");
        //System.out.println(sb);
    }
}

class Q54 {
    public static void main(String[] args) {
        System.out.println("prickly".length());
        System.out.println("porcupine".length());
        var phrase = "prickly \n porcupine";
        System.out.println(phrase.length());
        System.out.println(phrase.stripTrailing());
        System.out.println(phrase.stripIndent());
        System.out.println(phrase.stripLeading());
        System.out.println(phrase.indent(1));
        System.out.println(phrase.indent(1).length());
        System.out.println(phrase.indent(0).length());
        System.out.println(phrase.indent(-1).length());
    }
}

class Q57 {
    public static void main(String[] args) {
        Q57 q57 = new Q57();
        System.out.println(q57);
    }
    public boolean isItMyBirthday(LocalDateTime dateTime) {
        LocalDate now = LocalDate.now();
        return now.getMonth() == dateTime.getMonth()
                && now.getDayOfMonth() == dateTime.getDayOfMonth();
    }
}

class Q58 {
    public static void main(String[] args) {
        System.out.println("cheetah\ncub");
        System.out.println("cheetah\\ncub");
        System.out.println("cheetah\ncub".translateEscapes());
        System.out.println("cheetah\\ncub".translateEscapes());
    }
}

class Q60 {
    public static void main(String[] args) {
        var quotes = """
            \"The Quotes that Could\"
            "\"\"
            """;
        System.out.println(quotes);
    }
}

class Q62 {
 public static void main(String[] duplo) {
        var sb = new StringBuilder();
        sb.append("red");
        sb.deleteCharAt(0);
        sb.delete(1, 2);
        System.out.println(sb);
 } }

class Q63 {
    public static void main(String[] args) {
        Boolean.valueOf("8").booleanValue();
        //Character.valueOf('x').byteValue();
        Double.valueOf("9_.3").byteValue();
        Long.valueOf(88).byteValue();
    }
}

class Q65 {
    public static void main(String[] args) {
        var date = LocalDate.of(2022, 5, 13);
        var time = LocalTime.of(10, 0);
        var trainDay = LocalDateTime.of(date, time);
        //var instant = trainDay.toInstant();
        //instant = instant.plus(1, ChronoUnit.DAYS);
        //System.out.println(instant);
    }
}

class Q67 {
    public static void main(String[] args) {
        System.out.println(true ^ true);
        System.out.println(true ^ false);
        System.out.println(false ^ false);
    }
}

class Q69 {
    public static void main(String[] args) {
        var builder = new StringBuilder("54321");
        System.out.println(builder);
        builder.substring(2);
        System.out.println(builder);
        System.out.println(builder.charAt(1));
    }
}