package chapter4;

import java.io.Closeable;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Scanner;

public class Chapter4 {}

class Q2PrintCompany {
    class Printer implements Closeable {
        public void print() { // r1
            System.out.println("This just in!");
        }
        public void close() {}
    }
    public void printHeadlines() {
        //try {Printer p = new Printer()} {     // r2
        try (Printer p = new Printer()) {     // r2
            p.print();
        }
    }
    public static void main(String[] headlines) {
        new Q2PrintCompany().printHeadlines();  // r3
    }}
//

class Q3Remember {
    public static void think() throws IOException {  // k1
        try {
            throw new Exception();
        } catch (RuntimeException r) {}               // k2
        catch (Exception e) {
            //throw new Exception();
            throw new RuntimeException(e);
        }
    }
    public static void main(String[] ideas) throws Exception
    {
        think(); }}

//

class Q5 {
    public static void main(String[] args) {
        String cogsworth = null;
        Integer lumiere  = null;
        Object mrsPotts = null;
        if(lumiere> cogsworth.length()) {
            System.out.println(mrsPotts.toString());
        }
    }
}

//

class Q7 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        try (s) {
            System.out.print(1);
            s.nextLine();
            System.out.print(2);
        } catch (IllegalArgumentException | NullPointerException x)
        {
            s.nextLine();
            System.out.print(3);
        } finally {
            s.nextLine();
            System.out.print(4);
        }
        System.out.print(5);
    }
}

//

class Q8WhaleSharkException extends Exception {
    public Q8WhaleSharkException() {
        super("Friendly shark!");
    }
    public Q8WhaleSharkException(String message) {
        super(new Exception(new Q8WhaleSharkException()));
    }
    public Q8WhaleSharkException(Exception cause) {}

    public static void main(String[] args) {

    }
}
//

class Q9Football {
    public static void main(String officials[]) {
        try {
            System.out.print('W');
            throw new ArrayIndexOutOfBoundsException();
        } catch (RuntimeException r) {
            System.out.print('X');
            try {
                throw r;
            } catch (RuntimeException e) {
                System.out.print('I');
            }
        } catch (Exception e) {
            System.out.print('Y');
        } finally {
            System.out.print('Z');
        }
    }}
//

class Q11Garden implements AutoCloseable {
    private final int g;
    Q11Garden(int g) { this.g = g; }
    public void close() throws Exception {
        System.out.print(g); }}
class Q11Salad {
    public static void main(String[] u) throws Exception {
        var g = new Q11Garden(5);
        try (g;
             var h = new Q11Garden(4);
             var i = new Q11Garden(2)) {
        } finally {
            System.out.println(9);
        }
        //g = null;
    }}

//

class Q13Light {
    public static void main(String[] v) throws Exception {
        try {
            new Q13Light().turnOn();
        } catch (RuntimeException v1) {  // y1
            System.out.println(v1);
            throw new IOException();     // y2
        } finally {
            System.out.println("complete");
        }
    }
    public void turnOn() throws IOException {
        new IOException("Not ready"); // y3
    }}
//

class Q15SpellingException extends RuntimeException {}
class Q15SpellChecker {
    public final static void main(String[] participants) {
        try {
            if(!"cat".equals("kat")) {
                new Q15SpellingException();
            }
        } catch (Q15SpellingException | NullPointerException e) {
            System.out.println("Spelling problem!");
        } catch (/*RuntimeException | */Exception e) {
            System.out.println("Unknown Problem!");
        } finally {
            System.out.println("Done!");
        }
    }
}

//

class Q16 {
    public static void main(String[] args) {
        new ClassCastException();
    }
}

//

class Sleep {
    public static void snore() {
        try {
            String sheep[] = new String[3];
            System.out.print(sheep[3]);
        } catch (RuntimeException | Error e) {
            System.out.print("Awake!");
            throw e;
        } finally {
            //throw new Exception();
        }
    }
    public static void main (String[]sheep){
        new Sleep().snore();
    }
}

//

class Q18ProblemException extends Exception {
    Q18ProblemException(Exception e) { super(e); }
}
class Q18MajorProblemException extends Q18ProblemException {
    Q18MajorProblemException(String message) {
        //super(message);
        super(null);
    }
}
class Q18Unfortunate {
    public static void main(String[] args) throws Exception {
        try {
            System.out.print(1);
            throw new Q18MajorProblemException("Uh oh");
        } catch (Q18ProblemException | RuntimeException e) {
            System.out.print(2);
            try {
                throw new Q18MajorProblemException("yikes");
            } finally {
                System.out.print(3);
            }
        } finally {
            System.out.print(4);
        }}}
//

class Q21MissingMoneyException {}
class Q21MissingFoodException {}
class Q21Problems {
    public void doIHaveAProblem() /*throws Q21MissingMoneyException, Q21MissingFoodException*/ {
        System.out.println("No problems");
    }
    public static void main (String[] s) throws Exception {
        try {
            final Q21Problems p = new Q21Problems();
            p.doIHaveAProblem();
        } catch (Exception e) {
            throw e;
        }
    }
}
//

class Q23Beach {
    class TideException extends Exception {}
    public void surf() throws RuntimeException {
        try {
            throw new TideException();
        //} catch (Exception a | RuntimeException f) {}
        //} catch (TideException | IOException i) {}
        //} catch (TideException | Exception x) {}
        } catch (IllegalStateException | TideException t) {
        } catch (Exception z) {}
    }
}
//

class Q32Fortress {
    public void openDrawbridge() throws Exception {  // p1
        try {
            throw new Exception("Circle");
        } catch (Exception | Error e) {
            System.out.print("Opening!");
        } finally {
            System.out.print("Walls");
        }
    }
    public static void main(String[] moat) {
        try (var e = new Scanner(System.in)) {
            //new Fortress().openDrawbridge();
        }
    }
}
//

class Q34 {
    public static void main(String[] args) {
        String mode = null;
        int grade = (Integer)null;
        Integer average = null;
        if(grade>= average && Integer.parseInt(mode)> 0) {
            System.out.println("You passed!");
        }
    }
}

//

class Q38ProblemException extends Exception {
    Q38ProblemException(Exception e) { super(e); }
}
class Q38MajorProblemException extends Q38ProblemException {
    Q38MajorProblemException(Exception e) { super(e); }
}
class Q38Unfortunate {
    public static void main(String[] args) throws Exception {
        try {
            System.out.print(1);
            throw new Q38MajorProblemException(
                    new IllegalStateException());
        } catch (Q38ProblemException | RuntimeException e) {
            System.out.print(2);
            try {
                throw new Q38MajorProblemException(e);
            } finally {
                System.out.print(3);
            }
        } finally {
            System.out.print(4);
        }}}
//

class Q39Computer {
    public void compute() throws Exception {
        throw new NullPointerException("Does not compute!");
    }
    public static void main(String[] b) throws Exception {
        try {
            new Q39Computer().compute();
        } catch (RuntimeException e) {
            System.out.print("zero");
            throw e;
        } catch (Exception e) {
            System.out.print("one");
            throw e;
        }
    }
}
//

class Q40 {
    public static void main(String[] args) {
        new UnsupportedOperationException();
    }
}

//

class Q41TimeException extends Exception {}
class Q41TimeMachine implements AutoCloseable {
    int v;
    public Q41TimeMachine(int v) {this.v = v;}
    public void close() throws Exception {
        System.out.print(v);
    }
}
class Q41TimeTraveler {
    public static void main(String[] twelve) throws Exception {
        try (var timeSled = new Q41TimeMachine(1);
             var delorean = new Q41TimeMachine(2);
             var tardis = new Q41TimeMachine(3)) {
        } catch (Q41TimeException e) {
            System.out.print(4);
        } finally {
            System.out.print(5);
        }
    }
}
//

abstract class Q45Duck {
    protected int count;
    public abstract int getDuckies() throws IOException;
}
class Q45Ducklings extends Q45Duck {
    private int age;
    public Q45Ducklings(int age) { this.age = age; }
    public int getDuckies() { return this.age/count; }
    public static void main(String[] pondInfo) throws Exception {
        var itQuacks = new Q45Ducklings(5);
        System.out.print(itQuacks.getDuckies());
    }}
//

class Q47Backup {
    public void performBackup() {
        try {
            throw new IOException("Disk not found");  // z1
        } catch (Exception e) {
            try {
                throw new FileNotFoundException("File not found");
            } catch (FileNotFoundException e1) { //z2
                System.out.print("Failed");
            }}}
    public static void main(String[] files) {
        new Q47Backup().performBackup(); //z3
    }}
//

class Q53DatabaseHelper {
    static class MyDatabase implements Closeable {
        public void close() /*throws SQLException*/ {
            System.out.print("2");
        }
        public void write(String data) {}
        public String read() {return null;}
    }
    public static void main(String[] files) throws Exception
    {
        try (MyDatabase myDb = new MyDatabase()) {
            // TODO: Decide what to read/rite
        } finally {
            System.out.print("1");
        }}}

//

class Q54Exception extends Exception {}
class Q54 {
    public static void main(String[] args) {
        try {
            throw new Q54Exception();
        } catch (Q54Exception e) {
            System.out.println(e);
        }
    }
}
//

class Q55Pizza {
    Exception order(RuntimeException e) { //h1
        throw e; //h2
    }
    public static void main(String[] u) {
        var p = new Q55Pizza();
        try {
            p.order(new IllegalArgumentException()); // h3
        } catch(RuntimeException e) { //h4
            System.out.print(e);
        }
    }
}
//

class Q57DiskPlayer implements AutoCloseable {
    public void close() {}
}
class Q57LightCycle {
    public static void main(String[] bits) {
        try (Q57DiskPlayer john = new Q57DiskPlayer()) {
            System.out.println("ping");
            john.close();
        } finally {
            System.out.println("pong");
            //john.close();
        }
        System.out.println("return"); }}

//

class Q58OfficeException extends RuntimeException {
    public Q58OfficeException(Exception e) {
        super(e);
    }
    public static void main(String[] args) throws Exception {
        try {
            throw new Q58OfficeException(new IOException());
        } catch (RuntimeException e) {
            e.printStackTrace();
            System.out.println(e.getCause());
        }}}
//

final class Q59FallenException extends Exception {}
final class Q59HikingGear implements AutoCloseable {
    @Override public void close() throws Exception {
        throw new Q59FallenException();
    }}
class Q59Cliff {
    public final void climb() throws Exception {
        try (Q59HikingGear gear = new Q59HikingGear()) {
            throw new RuntimeException();
        }
    }
    public static void main(String[] rocks) {
        try {
            new Q59Cliff().climb();
        } catch (Throwable t) {
            System.out.println(t); // e1
            System.out.println(Arrays.toString(t.getSuppressed()));
            System.out.println(t.getSuppressed().length);
        }
    }
}