package chapter3;

import java.io.FileNotFoundException;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

import static java.lang.Math.*;

public class Chapter3 {
}

class Q0 {
    private static final DateTimeFormatter dtFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    private static final DateTimeFormatter dtFormatter2 = DateTimeFormatter.ISO_DATE_TIME;

    public static void main(String[] args) {
	System.out.println(dtFormatter.format(LocalDateTime.now()));
	System.out.println(LocalDateTime.now().format(dtFormatter));
	System.out.println(LocalDateTime.now());
	System.out.println(LocalDateTime.now().atOffset(ZoneOffset.UTC).format(dtFormatter));
	System.out.println(LocalDateTime.now().format(dtFormatter2));

	String date = "2024-05-02T13:31:54Z";
	if (date.matches("^\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}Z$")) {
	    System.out.println(date.replace("T", " ").replace("Z", ""));
	}
	String a = null;
	System.out.println(String.join(",", a));
    }
}


enum Q1Color {
	BROWN, YELLOW, BLACK;
    	public static Q1Color DEFAULT = BROWN;
}
record Q1() {
	//String name;
	static int age;
 	//{ age = 10;}
 	static { age = 10;}
}
//

final class Q3Story {
    void recite(int chapter) throws Exception {}
}
class Q3Adventure /*extends Q3Story*/ {
    final void recite(final int chapter) { // g1
	switch(chapter) { // g2
		case 2: System.out.print(9);
		default: System.out.print(3);
	}
    }
    public static void main(String... u) {
	var bedtime = new Q3Adventure();
	bedtime.recite(2);
    }
}
//

interface Q8 {
    default int a() {
	return 1;
    }
}

class Q12Dinosaur {
    class Pterodactyl extends Q12Dinosaur {}
    public void roar() {
	var dino = new Q12Dinosaur();
	new Pterodactyl();
	new Q12Dinosaur.Pterodactyl();
	new Q12Dinosaur().new Pterodactyl();
	dino.new Pterodactyl();
    }
}

class Q14Cars {
    private static void drive() {
	{
	    System.out.println("zoom");
	}
	System.out.println("fast");
    }
    static { System.out.println("faster"); }
    public static void main(String[] args) {
	drive();
	drive();
    }
}

interface Q15 {
    default int a() {
	return 1;
    }
    static void b() {
	System.out.println("interface");
    }
    private static void c() {
	System.out.println("interface");
    }
    public static void d() {
	System.out.println("interface");
    }
}

class Q17 {
    public static String woof(double bark) {
	return "1";/*+bark.toString();*/
    }
    public static String woof(Integer bark) {
	return "2"+bark.toString();
    }
    public static String woof(Object bark) {
	return "3"+bark.toString();
    }
    public static void main(String[] a) {
	System.out.println(woof((short)5)); //1
	System.out.println(woof(new Short("5"))); //35
    } }

interface Q18GameItem {
    int sell();
}
abstract class Q18Vegetable implements Q18GameItem {
    public final int sell() { return 5; }
}
class Q18Turnip extends Q18Vegetable {
    //public final int sell() { return 3; }
    public static void main(String[] expensive) {
	System.out.print(new Q18Turnip().sell());
    } }

//

record Q21Animal(boolean isMammal) {}
record Q21Panda(String name) /*extends Q21Animal*/ {
    public Q21Panda() {
	this("TaiShan");
	System.out.println("Q21Panda");
    }
    public Q21Panda {
	//this.name = name.toLowerCase();
    }
    public static void main(String[] args) {
	System.out.print(new Q21Panda().name());
    } }

//
interface Q25Speak { default int talk() { return 7; } }
interface Q25Sing { default int talk() { return 5; } }
class Q25Performance implements Q25Speak/*, Sing*/ {
    public int talk(String... x) {
	return x.length;
    }
    public static void main(String[] notes) {
	System.out.print(new Q25Performance().talk());
    } }
//

//@FunctionalInterface
interface Q26Pump { public abstract String toString(); }
@FunctionalInterface
interface Q26Bend extends Q26Pump {
    void bend(double tensileStrength);
}
class Q26Robot implements Q26Pump{
    public static final void apply(Q26Bend instruction, double input) {
	instruction.bend(input);
    }
    public static void main(String... future) {
	final Q26Robot r = new Q26Robot();
	r.apply(x -> System.out.print(x+" bent!"), 5);
    } }

sealed class Q33Organ {
    sealed class Q33Heart extends Q33Organ {}
    final class Q33Lung extends Q33Organ {}
    static non-sealed class Q33Stomach extends Q33Organ {}
    final class Q33Valentine extends Q33Heart {}
}

class Q35Penguin {
 private enum Q35Baby { EGG }
 static class Q35Chick {
	 enum Q35Baby { EGG }
 }
 public static void main(String[] args) {
	boolean match = false;
     	Q35Baby egg = Q35Baby.EGG;
	switch (egg) {
	case EGG: match = true;
	}
     System.out.println(match);
 }
}

class Q39 {
    public static void main(String[] args) {
	var var = "var"; // line x1
	var title = "Weather"; // line x2
	//var t = 1, a = 4;
	//var hot = 100, var cold = 20; // line x3
	//var f = 32, int c = 0; // line x4
    }
}

record Q46(int a) implements Serializable {
    //Q46 {}
    Q46(int a) {
	this.a = a;
    }
    Q46() {
	this(1);
    }
    Q46(int i, int j) {
	this();
    }
}

class Q49Bottle {
    private enum Q49Sail { // w1
	TALL {protected int getHeight() {return 100;}},
	SHORT {protected int getHeight() {return 2;}};
	protected abstract int getHeight();
    }
    public static class Q49Ship {
	private enum Q49Sail { // w1
	    TALL {protected int getHeight() {return 100;}},
	    SHORT {protected int getHeight() {return 2;}};
	    protected abstract int getHeight();
	}
	public Q49Sail getSail() {
	    return Q49Sail.TALL;
	} }
    public static void main(String[] stars) {
	var bottle = new Q49Bottle();
	//Q49Ship q = bottle.new Q49Ship(); // w2
	Q49Ship q = new Q49Ship(); // w2
	Q49Ship q1 = new Q49Bottle.Q49Ship(); // w2
	Q49Sail aShort = Q49Sail.SHORT;
	System.out.print(q.getSail());
    } }
//

abstract interface Q57CanSwim {
    public void swim(final int distance);
}
class Q57Turtle {
    final int distance = 2;
    public static void main(String[] seaweed) {
	final int distance = 3;
	Q57CanSwim seaTurtle = new Q57CanSwim(){
	    final int distance = 5;
	    @Override
	    public void swim(final int distance) {
		System.out.print(distance);
	    }
	};
	seaTurtle.swim(7);
    } }
//

interface Q61Building {
    default Double getHeight() { return 1.0; } // m1
}
interface Q61Office {
    public default String getHeight() { return null; } // m2
}
//abstract class Q61Tower implements Q61Building, Q61Office {}
    /*@Override public Double getHeight() {
	return Q61Building.super.getHeight();
    }*/
//} // m3
//class Q61Restaurant extends Q61Tower {
	/*@Override public String getHeight() {
	    return super.getHeight();
	}*/
    //}

//

interface Q76Flavor {
	public static void happy() {
		printFlavor("Rocky road");
	}
	private static void excited() {
		printFlavor("Peanut butter");
	}
	static void printFlavor(String f) {
		System.out.println("Favorite Flavor is: "+f);
	}
	public static void sad() {
		printFlavor("Butter pecan");
	}
}
class IceCream implements Q76Flavor {
    	public static void happy() {
		Q76Flavor.printFlavor("Cherry chocolate chip");
	}

    public static void main(String[] args) {
	happy();
    }
}
//

class Q79 {
    public static void main(String[] args) {
	int Integer = 0; // k1
	//Integer int = 0; // k2
	Integer ++; // k3
	System.out.println(Integer ++);
	//int ++; // k4
	//int var = null; // k5
    }
}

//

class Q81 {
    public enum Snow {
	BLIZZARD, SQUALL, FLURRY;
	@Override
	public String toString() { return "Sunny"; }
    }
    public static void main(String[] modelData) {
	System.out.print(Snow.BLIZZARD.ordinal() + " ");
	System.out.print(Snow.valueOf("flurry".toUpperCase()));
    } }

//
interface Q86Speak {
    public default int getVolume() { return 20; }
}
interface Q86Whisper {
    public default int getVolume() { return 10; }
}
class Q86Debate implements Q86Speak, Q86Whisper {
    public int getVolume() { return 30; }
    public void main() {
	var d = new Q86Debate();
	System.out.println(Q86Whisper.super.getVolume());
	System.out.println(Q86Speak.super.getVolume());
	System.out.println(getVolume());
    }

    public static void main(String[] args) {
	var d = new Q86Debate();
	d.main();
    }
}
//

class Q95Singing {
    private void sing(String key) {}
    public void sing_do(String key, String... harmonies) {
	this.sing(key);
    }
    //public void sing_re(int note, String... sound, String key)
    //{
	//this.sing(key);
    //}
    //public void sing_me(String... keys, String... pitches) {
	//this.sing(key);
    //}
    public void sing_fa(String key, String... harmonies) {
	//this.Q95Singing.sing(key);
    }
    //public void sing_so(int note, String... sound, String key) {
	//this.Q95Singing.sing(key);
    //}
    // public void sing_la(String... keys, String...pitches) {
	//this.Singing.sing(key);
    // }
}

class Q96Matrix {
    private int level = 1;
    class Deep {
	private int level = 2;
	class Deeper {
	    private int level = 5;
	    public void printReality(int level) {
		System.out.print(this.level+" ");
		System.out.print(Q96Matrix.Deep.this.level+" ");
		System.out.print(Deep.this.level);
	    } } }
    public static void main(String[] bots) {
	Q96Matrix.Deep.Deeper simulation = new Q96Matrix().new Deep().new Deeper();
	simulation.printReality(6);
    } }

//

sealed interface Q97Time permits Q97Hour, Q97Minute, Q97Second {}
record Q97Hour() implements Q97Time {}
non-sealed interface Q97Minute extends Q97Time {}
non-sealed class Q97Second implements Q97Time {}
class Q97Micro extends Q97Second {}

//
interface Q98MusicCreator {
    public Number play();
}
abstract class Q98StringInstrument {
    public Long play() {
	return 3L;
    }
}
class Q98Violin extends Q98StringInstrument implements Q98MusicCreator {
    public Long play() {
	return null;
    } }
//

abstract class Q99Triangle {
    abstract String getDescription();
}
abstract class Q99IsoRightTriangle extends Q99RightTriangle { // g1
    public abstract String getDescription();
}
class Q99RightTriangle extends Q99Triangle {
    protected String getDescription() { return "rt"; } // g2
    public static void main(String[] edges) {
	final var shape = new Q99IsoRightTriangle() {
	    @Override
	    public String getDescription() {
		return "irt";
	    }
	}; // g3
	System.out.print(shape.getDescription());
    }
}
//

class Q103 {
    static String b = "b";
    public static void main(String[] args) {
	//String b = "b";
	System.out.println(Arrays.stream("Test".split("")).map(a -> a + b));
	b = null;
    }
}

class Q105 {
    public static void main(String[] args) {
	if (null instanceof Object) {
	    System.out.println("false");
	}
    }
}

//
class Q106Eggs {
    enum Animal {
	CHICKEN(21), PENGUIN(75);
	private int numDays;

	private Animal(int numDays) {
	    this.numDays = numDays;
	}

	public int getNumDays() {
	    return numDays;
	}

	public void setNumDays(int numDays) {
	    this.numDays = numDays;
	}
    }

    public static void main(String[] args) {
	Animal chicken = Animal.CHICKEN;
	chicken.setNumDays(20);
	System.out.print(chicken.getNumDays());
	System.out.print(" ");
	System.out.print(Animal.CHICKEN.getNumDays());
	System.out.print(" ");
	System.out.print(Animal.PENGUIN.getNumDays());
    }
}

//

interface Q107Thunderstorm {
 float rain = 1;
 //char getSeason() { return 'W'; }
 boolean isWet();
 //protected static void hail() {}
 public static void hail1() {}
 private void hail() {}
 public default String location() { return "Home"; }
 private static int getTemp() { return 35; }
}

record Q109() {
    void print() {
	System.out.println("Test");
    }
}

//

interface Q117Tree {
    public static void produceSap() {



	growLeaves();
    }
    //public abstract int getNumberOfRings() {
	//return getNumberOfRings();
    //}
    private static void growLeaves() {
	produceSap();
    }
    public default int getHeight() {
	return getHeight ();
    } }
//

class Woods {
    static class Tree {}
    public static void main(String[] leaves) {
	int heat = 2;
	int water = 10-heat;
	final class Oak extends Tree { // p1
	    public int getWater() {
		return water; // p2
	    }
	}
	System.out.print(new Oak().getWater());
	//water = 0;
    }
}

//

class Q125Problem extends Exception {}
abstract class Q125Danger {
    protected abstract void isDanger() throws Q125Problem; // m1
}
class Q125SeriousDanger extends Q125Danger { // m2
    protected void isDanger() /*throws Exception*/ { // m3
	throw new RuntimeException(); // m4
    }
    public static void main(String[] w) throws Throwable { // m5
	//var sd = new Q125SeriousDanger().isDanger(); // m6
	new Q125SeriousDanger().isDanger(); // m6
    } }

/*protected*/ final class Q126Final {
    private static String s = "S";
    protected final class Inner {
	private String s = "S2";
	public static void main(String[] args) {
	    //System.out.println(s);
	}
    }
}
/*protected*/ abstract class Q126Abstract {
    private static String s = "S";

    public static void main(String[] args) {
	Inner.main(new String[]{});
    }
    protected abstract static class Inner {
	private String s = "S2";

	public static void main(String[] args) {
	    System.out.println(Q126Abstract.s);
	}
    }
}
//

interface Q129Drive {
    int SPEED = 5;
    default Number getSpeed() { return SPEED; }
}
interface Q129Hover {
    Double MAX_SPEED = 10.0;
    default Double getSpeed() { return MAX_SPEED; }
}
class Q129Car implements Q129Drive, Q129Hover {
    @Override
    public Double getSpeed() { return 15.0; }
    public static void main(String[] gears) {
	System.out.print(new Q129Car().getSpeed());
    } }

//
enum Color {
    RED(1, 2) {
	void toSpectrum() {
	}
    }, BLUE(2) {
	void toSpectrum() {
	}

	void printColor() {
	}
    }, ORANGE() {
	void toSpectrum() {
	}
    }, GREEN(4) {
	@Override
	void toSpectrum() {

	}
    };

    Color(int... color) {
    }

    abstract void toSpectrum();

    void printColor() {
    }
}
//

class Q140 {
}
//class _ {
//}

//

class Q146 {
	public static void main(String[] args) {
		System.out.println(round(5.0));
	}
}

sealed class Q148 {}
sealed class Q148_ extends Q148 {}
non-sealed class Q148__ extends Q148_ {}

//

class Q150Earth {
	private abstract class Sky {
		void fall() {
			var e = new Sky() { final static int blue = 1; };
			//var e = new Q150Sunset() extends Sky { };
		}}}
class Q150Sunset { }
//

class Q152Dwarf {
	private final String name;
	public Q152Dwarf() {
		this("Bashful");
	}
	public Q152Dwarf(String name) {
		//name = "Sleepy";
		this.name = "Sleepy";
	}
	public static void main(String[] sound) {
		var d = new Q152Dwarf("Doc");
		System.out.println(d.name); }}

class Q164 {
	final private static int sp = 50;
	final private int sp2 = 50;
	final public static void main(String[] args) {

	}
}

sealed class Q167Fish {
	final class Blobfish extends Clownfish {}
	private non-sealed class Dory extends BlueTang {}
	sealed class Clownfish extends Q167Fish {}
	sealed class BlueTang extends Q167Fish {}
	final class Swordfish extends Q167Fish {}
	private non-sealed class Nemo extends Clownfish {}
}

//

class Q171Fly {
	public Q171Fly Q171Fly() { return new Q171Fly(); }
	public void Q171Fly(int kite) {}
	public int Q171Fly(long kite) { return 1; }
	public static void main(String[] a) {
		var f = new Q171Fly();
		f.Q171Fly(); }}

abstract class Q175School {
	abstract Float getNumTeachers();
	public int getNumStudents() {
		return 10; }}
class Q175HighSchool extends Q175School {
	final Float getNumTeachers() { return 4f; }
	//public int getNumStudents() throws FileNotFoundException {return 20; }
	public static void main(String[] s) throws Exception {
		var school = new Q175HighSchool();
		System.out.print(school.getNumStudents());
	}}

//

class Q179Dragon {
	boolean scaly;
	static final int gold;

	Q179Dragon protectTreasure(int value, boolean scaly) {
		scaly = true;
		return this;
	}
	static void fly(boolean scaly){
		scaly = true;
	}
	int saveTheTreasure(boolean scaly){
		return this.gold;
	}
	static void saveTheDay( boolean scaly){
		//this.gold = 0;
	}
	static {gold = 100;}
}

//

class Husky {
	{ this.food = 10; }
	{ toy = 2;}
	private final int toy;
	private static int food;
	public Husky(int friend) {
		this.food += friend++;
		//this.toy -= friend--;
	}
	public static void main(String... unused) {
		var h = new Husky(2);
		System.out.println(h.food+","+h.toy);
	}}

//
class Q185Link {
	private String name;
	private Q185Link next;

	public Q185Link(String name, Q185Link next) {
		this.name = name;
		this.next = next;
	}

	@Override
	public String toString() {
		return "Q185Link{" +
				"name='" + name + '\'' +
				", next=" + next.name +
				'}';
	}

	public void setNext(Q185Link next) {
		this.next = next;
	}

	public Q185Link getNext() {
		return next;
	}

	public static void main(String... args) {
		var apple = new Q185Link("x", null);
		var orange = new Q185Link("y", apple);
		var banana = new Q185Link("z", orange);
		orange.setNext(banana);
		banana.setNext(orange);
		apple = null;
		banana = null;

		System.out.println(apple);
		System.out.println(orange);
		System.out.println(banana);
	}
}

//

class Q187Complex {
	class Building {}
	final class House extends Building {}
	public void convert() {
		Building b1 = new Building();
		House h1 = new House();
		Building b2 = new House();
		Building b3 = (House) b1;
		House h2 = (House) (Building) h1;
		Building b4 = (Building) b2;
		House h3 = (House) b2;
	}}

//

interface Q188Tasty {
          default void eat() {
		        System.out.print("Spoiled!");
		  }}
class Q188ApplePicking {
	public static void main(String[] food) {
		var apple = new Q188Tasty() {
			@Override
			public void eat() {
				System.out.print("Yummy!");
			}
		};
	}
}

//

interface Q197Horn {
	public Integer play();
}
abstract class Q197Woodwind {
	public Short play() {
		return 3; }
}
//final class Q197Saxophone extends Q197Woodwind implements Q197Horn {
	//public Number play() {
	//	return null;
	//}
//}

//

interface Q205Sport {
	private int play() { return 15; }
}
interface Q205Tennis extends Q205Sport {
	private int play() { return 30; }
}
class Q205Game implements Q205Tennis {
	public int play() { return 1; }
	public static void main(String... ace) {
		System.out.println(new Q205Game().play()); }}
//

interface Q208Ready {
	static int first = 2;
	final short DEFAULT_VALUE = 10;
	Q208GetSet go = new Q208GetSet();
}
class Q208GetSet implements Q208Ready {
	int first = 5;
	static int second = DEFAULT_VALUE;
	public static void main(String[] begin) {
		var r = new Q208Ready() {};
		System.out.print(r.first);
		System.out.print(" " + second);
	}}

//

interface Q210Autobot {}
record Q210Transformer(Boolean matrix) implements Q210Autobot
{
	public boolean isMatrix() {
		return matrix;
	}
	/*abstract*/ void transform() {}
	public Q210Transformer {
		if(matrix == null)
			throw new IllegalArgumentException();
	}
	public static void main(String[] u) {
		//var prime = new Transformer(null) {
		//	public void transform() {}
		//};
		//System.out.print(prime.matrix());
	}
}

class Q213Electricity {
	interface Power {
	}

	public static void main(String[] light) {
		class Source implements Power {}
		final class Super extends Source {}
		//var start = new Super() {};
		var end = new Source() {
			static boolean t = true;
		};
	}
}

//

class Q215Values {
	static int  defaultValue = 8;
	static int  DEFAULT_VALUE;
	public static void main(String[] args) {
		System.out.println("" + defaultValue + DEFAULT_VALUE);
	}}
//

class Q217Stars {
	private int inThe = 4;
	public void Q217Stars() {
		//super();
		//this(1);
	}
	public Q217Stars(int inThe) {
		this.inThe = this.inThe;
	}
	public static void main(String[] endless) {
		System.out.print(new chapter3.Q217Stars(2).inThe);
	}}
//

interface Q219Long {
	Number length();
}
class Q219Elephant {
	public class Trunk implements Q219Long {
		public Number length() { return 6; }
	}
	public class MyTrunk extends Trunk {
		// k1
		// k2
		public Integer length() { return 9; }  // k3
	}
	public static void charge() {
		//System.out.print(new MyTrunk().length());
		System.out.print(new Q219Elephant().new MyTrunk().length());
	}
	public static void main(String[] cute) {
		new Q219Elephant().charge();               // k4
	}}
//

class Q221 {
	public static void main(String[] args) {
		Comparator<String> c1 = (j, k) -> 0;
		System.out.println(c1.compare("1", "2"));
		Comparator<String> c2 = (String j, String k) -> 0;
		System.out.println(c2.compare("1", "2"));
		//Comparator<String> c3 = (var j, String k) -> 0;
		//Comparator<String> c4 = (var j, k) -> 0;
		Comparator<String> c5 = (var j, var k) -> 0;
		System.out.println(c5.compare("1", "2"));
	}
}

//

class Q222Bunny {
	static interface Rabbit { }
	static class FlemishRabbit implements Rabbit { }
	private static void hop(Rabbit r) {
		System.out.print("hop");
	}
	private static void hop(FlemishRabbit r) {
		System.out.print("HOP");
	}
	public static void main(String[] args) {
		Rabbit r1 = new FlemishRabbit();
		FlemishRabbit r2 = new FlemishRabbit();
		hop(r1);
		hop(r2);
	}}
//

class Q223 {
	public static void main(String[] args) {
		var list = new ArrayList<Integer>();
		list.add(10);
		list.add(9);
		list.add(8);

		var num = 9;
		list.removeIf(x -> {int keep = num; return x != keep;});
		System.out.println(list);

		list.removeIf(x -> {int keep = num; return x == keep;});
		System.out.println(list);
	}
}

//

class Q224 {
	public static void main(String[] args) {
		var babies = Arrays.asList("chick", "cygnet", "duckling");
		babies.replaceAll(x -> { var newValue = "baby";
			return newValue; });
		System.out.println(babies);
	}
}

record Q226Light(String type, float lumens) {
	final static String DEFAULT_TYPE = "PAR";
	public Q226Light {
		if(type == null)
			throw new IllegalArgumentException();
		else type = DEFAULT_TYPE;
	}
	public Q226Light(String type) {
		this("", 0.0f);
		System.out.println("Finish");
	}
	public static void main(String[] p) {
		final var bulb = new Q226Light("A");
		System.out.print(bulb.type()); }}
//

class Q227Dress {
	int size = 10;
	/*default*/ int getSize() {
		display();
		return size;
	}
	static private void display() {
		System.out.print("What a pretty outfit!");
	}
	private int getLength() {
		display();
		return 15; }
	private static void tryOn() {
		display();
	}}

abstract sealed class Q229Snack permits Q229Snack.IceCream, Q229Snack.Lollipop, Q229Snack.Toast {
	final static class Toast extends Q229Snack {}
	sealed static class Lollipop extends Q229Snack {}
	final class Treat extends Lollipop {}
	abstract non-sealed class IceCream extends Q229Snack {}
}

