package chapter6;

import java.util.*;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.function.*;
import java.util.stream.*;

public class Chapter6 { }

class Q12 {
    public static void main(String[] args) {
        var empty = Optional.empty();
        var param = Optional.of(null);
        var method = Optional.ofNullable(null);
    }
}

class Q14 {
    public static void main(String[] args) {
        //Function<Integer,Integer> transformer = x -> x;
        //UnaryOperator<Integer> transformer = x -> x;
        ToIntFunction<Integer> transformer = x -> x;
        //IntUnaryOperator transformer = x -> x;
        var prime = List.of(3,1,4,1,5,9)
                .stream()
                .limit(1)
                .peek(s -> {})
                .mapToInt(transformer)
                .peek(s -> {})
                .sum();
        System.out.println(prime);
    }
}

class Q16 {
    public static void main(String[] args) {
        var list = new ArrayList<String>();
        list.add("Monday");
        //list.add(String::new);
        //list.add(() -> new String());
        list.add("Tuesday");
        list.remove(0);
        System.out.println(list.get(0));
    }
}

class Q17 {
    public void mine(BiFunction<Integer,Double,Double> lambda) {
        int k;
        // IMPLEMENTATION OMITTED
    }
    public static void main(String[] debris) {
        new Q17().mine((s,p) -> s+p); }}

class Q20 {
    public static void main(String[] args) {
        ToDoubleBiFunction<Integer, Double> toDoubleBiFunction1 = (Integer a, Double b) -> {int c; return b;};
        ToDoubleBiFunction<Integer, Double> toDoubleBiFunction2 = (h,i) -> (long)h;
        ToDoubleBiFunction<Integer, Double> toDoubleBiFunction3 = (x,y) -> {int z=2; return y/z;};
        //ToDoubleBiFunction<Double, Double> toDoubleBiFunction4 = (double y, double z) -> y + z;
        System.out.println(toDoubleBiFunction1.applyAsDouble(1, 1.0));
        System.out.println(toDoubleBiFunction2.applyAsDouble(1, 2.0));
        System.out.println(toDoubleBiFunction3.applyAsDouble(1, 3.0));
    }
}

class Q21 {
    public static void main(String[] args) {
        List<Integer> list = List.of(1,2,3);
        System.out.println(list.parallelStream().count());
    }
}

class Q22 {
    public static void main(String[] args) {
        Function<Integer, ArrayList> c1 = n -> new ArrayList<>(n);
        Consumer<Integer> c2 = ArrayList::new;
        System.out.println(c1.apply(100));
    }
}

class Q23 {
    public static void main(String[] args) {
        DoubleStream doubleStream = DoubleStream.of(1.0, 2.0, 3.);
        System.out.println(doubleStream.count());
        doubleStream = DoubleStream.of(1.0, 2.0, 3.);
        System.out.println(doubleStream.max());
        doubleStream = DoubleStream.of(1.0, 2.0, 3.);
        System.out.println(doubleStream.min());
        doubleStream = DoubleStream.of(1.0, 2.0, 3.);
        System.out.println(doubleStream.average());
        doubleStream = DoubleStream.of(1.0, 2.0, 3.);
        System.out.println(doubleStream.sum());
    }
}

class Q27 {
    public static void main(String[] args) {
        List<String> l = List.of("One", "Two", "Three");
        System.out.println(l.stream().collect(Collectors.groupingBy(String::length, Collectors.toCollection(HashSet::new))));
    }
}

class Q28 {
    public static void main(String[] args) {
        Set<String> set = new HashSet<>();
        set.add("tire-");
        List<String> list = new LinkedList<>();
        Deque<String> queue = new ArrayDeque<>();
        queue.push("wheel-");
        Stream.of(set.stream(), list.stream(), queue.stream())
            .flatMap(x -> x)
            .forEach(System.out::print);
    }
}

class Q30 {
    private static int AT_CAPACITY = 100;
    public int takeTicket(int currentCount, IntUnaryOperator/*<Integer>*/ counter) {
        return counter.applyAsInt(currentCount);
    }
    public static void main(String[] theater) {
        final Q30 bob = new Q30();
        final int oldCount = 50;
        final int newCount = bob.takeTicket(oldCount,t -> {
            if(t>AT_CAPACITY) {
                throw new RuntimeException("Sorry, max has been reached");
            }
            return t+1;
        });
        System.out.print(newCount); }}

class Q32 {
    public static void main(String[] s) {
        Predicate<String> dash = c -> c.startsWith("-");
        System.out.println(dash.test("–"));

        Consumer clear = x -> System.out.println(x);
        clear.accept("pink");

        Comparator<String> c = (String s1, String t) -> 0;
        System.out.println(c.compare("s", "t"));
    }
}

class Q35 {
    private static void longer(Optional<Boolean> opt) {
        if (opt.isPresent())
            System.out.println("run: " + opt.get());
    }
    private static void shorter(Optional<Boolean> opt) {
        opt.map(x -> "run: " + x)
                .ifPresent(System.out::println);
    }

    public static void main(String[] args) {

    }
}

class Q38 {
    public static void main(String[] args) {
        var list = List.of('c', 'b', 'a');

        list.stream()
            .sorted()
            .findAny()
            .ifPresent(System.out::println);

        System.out.println(list.stream().sorted().findFirst());
    }
}

class Q41 {
    public String concat1(List<String> values) {
        return values.parallelStream()
                .reduce("a",
                        (x,y)->x+y,
                        String::concat);
    }
    public String concat2(List<String> values) {
        return values.parallelStream()
                .reduce((w,z)->z+w).get();
    }
    public static void main(String[] questions) {
        Q41 c = new Q41();
        var list = List.of("Cat","Hat");
        String x = c.concat1(list);
        String y = c.concat2(list);
        System.out.print(x+" "+y);
    }}

class Q42 {
    public static void main(String[] args) {
        BiFunction<Double, Double, Double> bf = (a, b) -> a + b;
        BinaryOperator<Double> bo = (a, b) -> a + b;
        DoubleFunction<Double> df = (a) -> a + 0.5;
        System.out.println(bf.apply(1., 2.));
        System.out.println(bo.apply(1., 2.));
        System.out.println(df.apply(1.));
    }
}

class Q43 {
    public static void main(String[] args) {
        var w = Stream.of("c","a","t").parallel()
                .collect(ConcurrentSkipListSet::new, Set::add, Set::addAll);
        System.out.println(w);
    }
}

class Q48 {
    public static void main(String[] args) {
        var ints = IntStream.of(6, 10);
        var longs = ints.mapToLong(i -> i);
        var first = longs.average().getAsDouble();
        var moreLongs = LongStream.of(6, 10);
        var stats = moreLongs.summaryStatistics();
        var second = stats.getAverage();
        System.out.println("*" + first + "-" + second + "*");
    }
}

class Q49 {
    public static void main(String[] args) {
        DoubleConsumer dc = (c) -> System.out.println(c);
        dc.accept(1f);
        IntFunction inf = (i) -> i;
        System.out.println(inf.apply(1));
        LongSupplier ls = () -> 1;
        System.out.println(ls.getAsLong());
        ObjDoubleConsumer<String> odc = (a, b) -> System.out.println(a + b);
        odc.accept("Test ", 1.0);
    }
}

@FunctionalInterface interface Q53Study {
    abstract int learn(String subject, int duration);
}
class Q53BiologyMaterial implements Q53Study {
    @Override public int learn(String subject, int duration)
    {
        if(subject == null)
            return duration;
        else
            return duration+1;
    }}
class Q53 {
    public static void main(String[] courses) {
        Q53Study s = new Q53BiologyMaterial();  // h1
        s = (x,y) -> {return x==null ? y : y+1;};  // h1
        System.out.print(s.learn(courses[0], Integer.parseInt(courses[1]))); }}

class Q54 {
    public static void main(String[] args) {
        var s = Stream.of("speak", "bark", "meow", "growl");
        BinaryOperator<String> merge = (a, b) -> a;
        var map = s.collect(Collectors.toMap(
                String::length, k -> k, merge));
        System.out.println(map);
        System.out.println(map.size() + " " + map.get(4));
    }
}

class Q59 {
    private int layer;
    public Q59(int layer) {
        super();
        this.layer = layer;
    }
    public static void open(
            UnaryOperator<Q59> task, Q59 doll) {
        //while((doll = task.accept(doll)) != null)
        while((doll = task.apply(doll)) != null)
            System.out.print("X");
    }
    public static void main(String[] wood) {
        open(s -> {
            if(s.layer<=0) return null;
            else return new Q59(s.layer--);
        }, new Q59(5)); }}

class Q60 {
    public static void main(String[] args) {
        Random r = new Random();
        Stream.generate(r::nextDouble)
                .skip(2)
                .limit(4)
                .sorted()
                .peek(System.out::println)
                .forEach(System.out::println);
    }
}

class Q64 {
    public static void main(String[] args) {
        List.of(2,4,6,8)
                //.parallel()
                .parallelStream()
                .forEach(System.out::print);
        System.out.println();
        List.of(2,4,6,8)
                .parallelStream()
                .parallel()
                .forEach(System.out::print);
        System.out.println();
        List.of(2,4,6,8)
                .parallelStream()
                .parallel().parallel().parallel()
                .forEach(System.out::print);
    }
}

class Q70 {
    public static void main(String[] args) {
        var s = Optional.empty();
        System.out.println(s.get());

        var s1 = IntStream.empty();
        System.out.print(s1.average().getAsDouble());

        var s2 = IntStream.of(-1,0, 1);
        System.out.print(s2.average().getAsDouble());
    }
}

class Q73 {
    public static void main(String[] args) {
        IntSupplier is = () -> {return 1/0;};
        System.out.println(is.getAsInt());
    }
}

class Q76 {
    record Blankie(String color) { }
    public static void main(String[] args) {
        var b1 = new Blankie("pink");
        var b2 = new Blankie("blue");
        var list = Arrays.asList(b1, b2);
        list.stream()
                //.filter(Blankie::getColor)
                //.filter(Blankie::color)
                .forEach(System.out::println);
    }}

class Q78 {
    public static void main(String[] args) {
        //Consumer<Object> c1 = ArrayList::new;
        //Consumer<Object> c2 = String::new;
        Consumer<String> c2 = String::new;
        Consumer<Object> c3 = System.out::println;
        //var c4 = ArrayList::new;
        //var c5 = String::new;
        //var c6 = System.out::println;
    }
}

class Q79 {
    public static void main(String[] args) {
        var p = List.of(new StringBuilder("hello"),
                new StringBuilder("goodbye"));
        var q = p.parallelStream().reduce("",
                (w,x) -> w + x,
                (y,z) -> String.valueOf(y.length() + z.length()));
        System.out.print(q);
    }
}

class Q80 {
    public static void main(String[] args) {
        var bools = Stream.of(Boolean.TRUE, null);
        var map = bools
                //.limit(1)    // line k
                .collect(Collectors.partitioningBy(b -> b));
        System.out.println(map);
    }
}

class Q81 {
    public static void main(String[] args) {
        var apples = List.of(1, 2);
        var oranges = List.of(1, 2);
        final var count = Stream.of(apples, oranges)
                //.flatMapToInt(List::stream)
                //.flatMap(List::stream)
                .flatMapToInt(l -> l.stream().mapToInt(n -> n))
                .peek(System.out::print)
                .count();
        System.out.print(count);
    }
}

class Q82 {
    //public static void dawn(DoubleUnaryOperator sunrise) {}
    //public static void dawn(Function<String,String> sunrise) {}
    public static void dawn(IntToLongFunction sunrise) {
        System.out.println(sunrise.applyAsLong(1));
    }
    //public static void dawn(UnaryOperator sunrise) {}
    public static void main(String[] rays) {
        dawn(s -> s+1); }}

class Q84 {
    public static void main(String[] args) {
        IntSummaryStatistics stats = Stream.of(20, 40)
                .mapToInt(i -> i)
                .summaryStatistics();
        long total = stats.getSum();
        long count = stats.getCount();
        long max = stats.getMax();
        System.out.println(total + "-" + count + "-" + max);
    }
}

class Q86 {
    public static void main(String[] args) {
        System.out.println(IntStream.of(1, 2).reduce(0,
                Integer::sum));
        System.out.println(IntStream.of(1, 2).sum());
        System.out.println(Stream.of(1, 2).reduce(0, Integer::sum));
        //System.out.println(Stream.of(1, 2).sum());
    }
}

class Q87 {
    public static void main(String[] args) {
        Stream.generate(() -> 'a')
                .limit(5)
                //.filter(c -> c < 'b')
                //.sorted()
                .findFirst();
                //.ifPresent(System.out::print);
    }
}

class Q88Catch {
    public static void main(String[] args) {
        Optional opt = Optional.empty();
        var message = "";
        try {
            message = x(opt);
        } catch (IllegalArgumentException e) {
            System.out.print("Caught it");
        }
        System.out.print(message);
    }
    private static String x(Optional<String> opt) {
        return opt.orElseThrow();
    }
    private static String y(Optional<String> opt) {
        return opt.orElseThrow(IllegalArgumentException::new);
    }
    private static String z(Optional<String> opt) {
        return opt.orElse("Caught it");
    }}

class Q90 {
    public static void main(String[] args) {
        BiFunction<String, Integer, Integer> biFunction = (a, b) -> 5;
        System.out.println(biFunction.apply("1", 1));

        BinaryOperator<Integer> binaryOperator = (a, b) -> 5;
        System.out.println(binaryOperator.apply(1, 3));

        DoubleFunction<Integer> doubleFunction = a -> 4;
        System.out.println(doubleFunction.apply(5.));

        ToDoubleFunction<Integer> toDoubleFunction = a -> 5;
        System.out.println(toDoubleFunction.applyAsDouble(1));

        ToIntBiFunction<Integer, Integer> toIntBiFunction = (a, b) -> 5;
        System.out.println(toIntBiFunction.applyAsInt(1, 3));
    }
}

class Q91 {
    public static void main(String[] args) {
        var r = new Random();
        var data = Stream.generate(() ->
                        String.valueOf(r.nextInt()))
                .limit(50_000)
                .collect(Collectors.toSet());
        var map = data.parallelStream()
                .peek(System.out::println)
                .collect(Collectors.groupingByConcurrent(String::length));
    }
}

class Q93Magician {
    public static void magic(BinaryOperator<Long> lambda) {
        System.out.println(lambda.apply(3L, 7L));
    }

    public static void main(String[] args) {
        //magic(a -> a);
        magic((b, w) -> (long) w.intValue());
        //magic((Integer b, Integer w) -> (Long) b + w);
    }
}

class Q94 {
    public static void main(String[] args) {
        BooleanSupplier booleanSupplier = () -> 1 > 5;
        System.out.println(booleanSupplier.getAsBoolean());
    }
}

class Q96 {
    public static void main(String[] args) {
        var a = List.of("String");
        a.stream().sorted(Comparator.reverseOrder());
        var s = Stream.of("over the river",
                "through the woods",
                "to grandmother's house we go");
        s.filter(n -> n.startsWith("t"))
                //.sorted(Comparator::reverseOrder)
                .sorted(Comparator.reverseOrder())
                //.sorted()
                //.sorted(String::compareTo)
                .findFirst()
                .ifPresent(System.out::println);
    }
}

class Q97 {
    public static void main(String[] args) {
        var stream = LongStream.of(9);
        stream.mapToDouble(p -> p).forEach(System.out::print);
    }
}

class Q98 {
    private int savingsInCents;
    private static class ConvertToCents {
        static DoubleToIntFunction f = p -> (int) (p*100);
    }
    public static void main(String[] currency) {
        Q98 creditUnion = new Q98();
        creditUnion.savingsInCents = 100;
        double deposit = 1.5;
        creditUnion.savingsInCents +=
                ConvertToCents.f.applyAsInt(deposit);  // j1
        System.out.print(creditUnion.savingsInCents);
        deposit = 0;
    }}

class Q100 {
    public static void main(String[] args) {
        var s = "fish";
        //BiPredicate<String, String> pred = (a, b) -> s.contains(a);
        BiPredicate<String, String> pred = String::contains;
        System.out.println(pred.test("fish", "is"));
    }
}

class Q103 {
    public static void main(String[] args) {
        DoublePredicate doublePredicate = a -> a > 1.;
        System.out.println(doublePredicate.test(2.));
        System.out.println(doublePredicate.test(1.00000001));
    }
}

class Q106 {
    public static void main(String[] args) {
        record Toy(String name){ }
        var toys = Stream.of(
                new Toy("Jack in the Box"),
                new Toy("Slinky"),
                new Toy("Yo-Yo"),
                new Toy("Rubik's Cube"));
        var spliterator = toys.spliterator();
        var batch = spliterator.trySplit();
        batch.tryAdvance(System.out::println);
        var more = batch.tryAdvance(x -> {});
        System.out.println(more);
        more = batch.tryAdvance(x -> {});
        System.out.println(more);
        spliterator.tryAdvance(System.out::println);
        spliterator.tryAdvance(System.out::println);
        spliterator.tryAdvance(System.out::println);
    }
}

class Q108 {
    private Function<String, String> printer1;
    private Consumer<String> printer;
    protected Q108() {
        printer = s -> {System.out.println(s);};
    }
    void printMovies(List<String> movies) {
        movies.forEach(printer);
    }
    public static void main(String[] args) {
        List<String> movies = new ArrayList<>();
        movies.add("Stream 3");
        movies.add("Lord of the Recursion");
        movies.add("Silence of the Lambdas");
        new Q108().printMovies(movies);
    }
}

class Q111 {
    public static void main(String[] args) {
        Stream<Integer> s1 = Stream.of(8, 2);
        Stream<Integer> s2 = Stream.of(10, 20);
        s2 = s1.filter(n -> n> 4);
        s1 = s2.filter(n -> n < 1);
        System.out.println(s1.count());
        System.out.println(s2.count());
    }
}

class Q114 {
    public static void main(String[] args) {
        //DoubleToLongFunction doubleToLongFunction = (Double d) -> d.longValue();
        DoubleToLongFunction doubleToLongFunction = (double d) -> (long) d;
        System.out.println(doubleToLongFunction.applyAsLong(1.5));
    }
}

class Q115 {
    public static void main(String[] args) {
        System.out.println(divide(List.of(1f).parallelStream()));
        System.out.println(divide(List.of(1f, 2f, 3f, 4f).parallelStream()));
        System.out.println(divide(List.of(1f, 2f, 3f, 4f).stream()));
        System.out.println(divide(Set.of(1f, 2f, 3f, 4f).stream()));
    }
    static float divide(Stream<Float> s) {
        return s.reduce(1.0f, (a,b) -> a/b, (a,b) -> a/b);
    }
}

class Q116 {
    public static void main(String[] args) {
        Stream.of("eeny", "meeny", "miny", "moe")
                .collect(Collectors.partitioningBy(x -> x.charAt(0)
                        == 'e'))
                .get(false)
                .stream()
                .collect(Collectors.groupingBy(String::length))
                .get(4)
                .forEach(System.out::println);

        Stream.of("eeny", "meeny", "miny", "moe")
                .filter(x -> x.charAt(0) != 'e')
                .collect(Collectors.groupingBy(String::length))
                .get(4)
                .forEach(System.out::println);

        Stream.of("eeny", "meeny", "miny", "moe")
                .collect(Collectors.groupingBy(x -> x.charAt(0) ==
                        'e'))
                .get(false);
                //.stream()
                //.collect(Collectors.partitioningBy(String::length))
                //.get(4)
                //.forEach(System.out::println);

        Stream.of("eeny", "meeny", "miny", "moe")
                .collect(Collectors.groupingBy(x -> x.charAt(0) ==
                        'e'))
                .get(false)
                .stream()
                .collect(Collectors.groupingBy(String::length))
                .get(4)
                .forEach(System.out::println);

        Stream.of("eeny", "meeny", "miny", "moe")
                .collect(Collectors.partitioningBy(x -> x.charAt(0)
                        == 'e'))
                .get(false)
                .stream()
                .collect(Collectors.partitioningBy(x -> x.length() ==
                        4))
                .get(true)
                .forEach(System.out::println);
    }
}

class Q117 {
    public static void main(String[] args) {
        IntStream integerIntStream = IntStream.of(1, 2, 3);
        integerIntStream.parallel().findFirst().ifPresent(System.out::println);
    }
}

class Q120 {
    public static void main(String[] args) {
        DoubleBinaryOperator doubleBinaryOperator = (a, b) -> a + b;
        System.out.println(doubleBinaryOperator.applyAsDouble(1, 2.));
        ToDoubleBiFunction<Integer,Double> toDoubleBiFunction = (a, b) -> a + b;
        System.out.println(toDoubleBiFunction.applyAsDouble(1, 2.));
        UnaryOperator<Integer> unaryOperator = a -> a + 1;
        System.out.println(unaryOperator.apply(10));

        BinaryOperator<Double> binaryOperator = (a, b) -> a + b;
        System.out.println(binaryOperator.apply(1., 2.));
        BiFunction<Integer,Integer,Double> biFunction = (a, b) -> (double) a + b;
        System.out.println(biFunction.apply(1, 2));
        System.out.println(unaryOperator.apply(10));

        Function<Double,Integer> function = (a) -> (int) (a + 1);
        System.out.println(function.apply(1.));
        BiFunction<Integer,Double,Integer> biFunctionInt = (a, b) -> (int) (a + b);
        System.out.println(biFunctionInt.apply(1, 2.));
        DoubleToIntFunction doubleToIntFunction = a -> (int) (a + 1);
        System.out.println(doubleToIntFunction.applyAsInt(1.));

        System.out.println(biFunction.apply(1, 2));
        BinaryOperator<Integer> binaryOperatorInt = (a, b) -> a + b;
        System.out.println(binaryOperatorInt.apply(1, 2));
        IntUnaryOperator intUnaryOperator = a -> a + 1;
        System.out.println(intUnaryOperator.applyAsInt(1));
    }
}

record Q121Boss(String name) {
    @Override public String toString() {
        return name.toUpperCase();
    } }
class Q121Initech {
    public static void main(String[] reports) {
        final List<Q121Boss> bosses = new ArrayList(8);
        bosses.add(new Q121Boss("Peter"));
        bosses.add(new Q121Boss("Samir"));
        bosses.add(new Q121Boss("Michael"));
        bosses.removeIf(s -> s.toString().equalsIgnoreCase("samir"));
        System.out.print(bosses);
    }}

interface Q121Serval {
    static void printName(String name) {}
    boolean cat(String name);

    public static void main(String[] args) {
        Q121Serval q121Serval = name -> true;
        System.out.println(q121Serval.cat("Name"));
    }
}

class Q123 {
    public static void main(String[] args) {
        //Comparator<String> c1 = s -> false;
        //Comparator<String, String> c2 = (s1, s2) -> false;
        //Predicate<String> p1 = String s -> false;
        Predicate<String> p2 = (String s) -> false;
        //Supplier<String> s1 = String s -> false;
        //Supplier<String> s2 = (String s) -> false;
    }
}

class Q124 {
    public static void main(String[] args) {
        record Pet(int age) {}
        record PetSummary(long count, int sum) {}
        var summary = Stream.of(new Pet(2), new Pet(5), new
                        Pet(8))
                .collect(Collectors.teeing(
                        Collectors.counting(),
                        Collectors.summingInt(Pet::age),
                        PetSummary::new));
        System.out.println(summary);
    }
}

class Q125 {
    public static void main(String[] args) {
        IntStream intStream = IntStream.of(1, 2, 3);
        IntSummaryStatistics intSummaryStatistics = intStream.summaryStatistics();
        System.out.println(intSummaryStatistics.getCount());
        System.out.println(intSummaryStatistics.getMax());
        System.out.println(intSummaryStatistics.toString());
    }
}

class Q126Sheep {}
 class Q126Dream {
    int MAX_SHEEP = 10;
    int sheepCount;
    public void countSheep(Consumer<Q126Sheep> backToSleep) {
        while(sheepCount<MAX_SHEEP) {
            backToSleep.accept(new Q126Sheep());
            // TODO: Apply lambda
            sheepCount++;
        } }
    public static void main(String[] dark) {
        new Q126Dream().countSheep(System.out::println);
    }}

class Q129 {
    public static void main(String[] args) {
        var pears = List.of(1, 2, 3, 4, 5, 6);
        final var sum = pears.stream()
                .skip(1)
                .limit(3)
                .flatMap(s -> Stream.of(s))
                .flatMapToInt(s -> IntStream.of(s))
                .skip(1)
                .boxed()
                .mapToDouble(s -> s)
                .sum();
        System.out.print(sum);
    }
}

class Q133HideAndSeek {
    public static void main(String[] args) {
        var hide = Stream.of(true, false, true);
        //var found = hide.filter(b -> b).anyMatch();
        var found = hide.filter(b -> b).allMatch(b -> b);
        System.out.println(found);
    }}

class Q135 {
    public static void main(String[] args) {
        Predicate<String> pred1 = (final String s) -> s.isEmpty();
        //Predicate<String> pred2 = (final s) -> s.isEmpty();
        Predicate<String> pred3 = (final var s) -> s.isEmpty();
        Predicate<String> pred4 = (String s) -> s.isEmpty();
        Predicate<String> pred5 = (var s) -> s.isEmpty();
    }
}

class Q136DogSearch {
    void reduceList(List<String> names,
                    Predicate<String> tester) {
        names.removeIf(tester);
    }
    public static void main(String[] treats) {
        int MAX_LENGTH = 2;
        Q136DogSearch search = new Q136DogSearch();
        List<String> names = new ArrayList<>();
        names.add("Lassie");
        names.add("Benji");
        names.add("Brian");
        search.reduceList(names, d -> d.length()>MAX_LENGTH);
        System.out.print(names.size());
    }}

class Q137 {
    public static void main(String[] args) {
        var p = List.of(1,3,5);
        var q = p.parallelStream().reduce(0f,
                (w,x) -> w.floatValue() + x.floatValue(),
                (y,z) -> y.floatValue() + z.floatValue());
        System.out.println(q);
    }
}

class Q138 {
    public static void main(String[] args) {
        Set<String> set = new HashSet<>();
        set.add("tire-");
        List<String> list = new LinkedList<>();
        Deque<String> queue = new ArrayDeque<>();
        queue.push("wheel-");
        Stream.of(set, list, queue)
                //.flatMap(x -> x.stream())
                .flatMap(Collection::stream)
                .forEach(System.out::print);
    }
}

class Q139PrintNegative {
          public static void main(String[] args) {
                List<String> list = new ArrayList<>();
                list.add("-5");
                list.add("0");
                list.add("5");
                list.removeIf(e -> Integer.valueOf(e) < 0);
                list.forEach(x -> System.out.println(x));
         }}

class Q140 {
    public static void main(String[] args) {
        //IntFunction<Integer> f1 =(Integer f) -> f;
        IntFunction<Integer> f2 = (int v) -> null;
        IntFunction<String> f3 = s -> String.valueOf(s);
        //IntFunction<Integer> f4 = () -> 5;
        //IntFunction<Integer> f5 = () -> Integer.valueOf(9);
    }
}

class Q146Compete {
    public static void main(String[] args) {
        Stream<Integer> is = Stream.of(8, 6, 9);
        Comparator<Integer> c = (a, b) -> a - b;
        is.sorted(c).forEach(System.out::print); }}

class Q148Warehouse {
    private int quantity = 40;
    private final BooleanSupplier stock;
    {
        stock = () -> quantity>0;
    }
    public void checkInventory() {
        if(stock.getAsBoolean())
            System.out.print("Plenty!");
        else
            System.out.print("On Backorder!");
    }
    public static void main(String[] widget) {
        final Q148Warehouse w13 = new Q148Warehouse();
        w13.checkInventory(); }}

record Q149Thermometer(String feelsLike, double temp) {
    @Override public String toString() { return feelsLike; }
    public static void main(String[] season) {
        var readings = List.of(new Q149Thermometer("HOT!",72),
                new Q149Thermometer("Too Cold!",0),
                new Q149Thermometer("Just right!",72));
        readings
                .parallelStream()               // k1
                .collect(Collectors.groupingByConcurrent(
                        Q149Thermometer::temp))          // k2
                .forEach((a, b) -> System.out.println(b));  // k3
    }}

class Q150 {
    public static void main(String[] args) {
        var odds = IntStream.iterate(1, a -> a+2);
        //var odds = Stream.iterate(1, a -> a+2);
        var evens = IntStream.iterate(2, a -> a+2);
        //var evens = Stream.iterate(2, a -> a+2);
        var sum = IntStream.concat(odds, evens).limit(3)
                .peek(System.out::println)
                //.mapToInt(a -> a)
                .sum();
        System.out.println(sum);
    }
}