package practice2;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.partitioningBy;

public class Practice2 {
    private Practice2() {
    }
}

class Q1Plan {
    ScheduledExecutorService s = Executors.newScheduledThreadPool(10);

    public void planEvents() {
        Runnable r1 = () -> System.out.print("Check food");
        Runnable r2 = () -> System.out.print("Check drinks");
        Runnable r3 = () -> System.out.print("Take out trash");
        s.scheduleWithFixedDelay(r1, 1, 1, TimeUnit.HOURS);
        s.scheduleAtFixedRate(r2, 1, 1, TimeUnit.SECONDS);  //
        s.execute(r3);                                      //
        //s.shutdownNow();
    }

    public static void main(String[] args) {
        new Q1Plan().planEvents();
    }
}
//

class Q2 {
    public void print(double t) {
        System.out.println(NumberFormat.getCompactNumberInstance().format(t));
        System.out.println(
                NumberFormat.getCompactNumberInstance(
                        Locale.getDefault(), NumberFormat.Style.SHORT).format(t));
        System.out.println(NumberFormat.getCurrencyInstance().format(t));
    }

    public static void main(String[] args) {
        new Q2().print(960_010);
    }
}

class Q3 {
    public static void main(String[] args) throws SQLException {
        var url = "jdbc:hsqldb:file:birds";
        var sql = "SELECT name FROM peacocks WHERE name = ?";
        try (var conn = DriverManager.getConnection(url);
             var stmt = conn.prepareStatement(sql)) {
            stmt.setString(1, "Feathers");
            try (var rs = stmt.executeQuery()) {
                //while (rs.hasNext()) {
                System.out.println(rs.next());
                //}
            }
        }
    }
}
//

record Goat(String food) {
    public static void main(String[] args) {
        var goats = List.of(
                new Goat("can"),
                new Goat("hay"),
                new Goat("shorts"),
                new Goat("hay"));
        goats.stream()
                .collect(groupingBy(Goat::food))
                .entrySet()
                .stream()
                .filter(e -> e.getValue().size() == 2)
                .map(e -> e.getKey())
                .collect(partitioningBy(e -> e.isEmpty()))
                .get(false)
                .stream()
                .sorted()
                .forEach(System.out::print);
    }
}
//

abstract class Q5TShirt {
    abstract int insulate();

    public Q5TShirt() {
        System.out.print("Starting...");
    }
}

class Q5Wardrobe {
    abstract class Sweater extends Q5TShirt {
        int insulate() {
            return 5;
        }
    }

    private void dress() {
        class Jacket extends Sweater {  // v1
            int insulate() {
                return 10;
            }
        }
        ;
        final Q5TShirt outfit = new Jacket() {  // v2
            int insulate() {
                return 20;
            }
        };
        System.out.println("Insulation:" + outfit.insulate());
    }

    public static void main(String... snow) {
        new Q5Wardrobe().dress();
    }
}
//

record Bee(boolean gender, String species) {
    Bee(boolean gender) {
        this(gender, "Honeybee");
    }

    public Bee {
        gender = gender;
        species = species;
    }

    //@Override
    public String getSpecies() {
        return species;
    }

    @Override
    public String toString() {
        return species;
    }
}
//

class Q10 {
    public static void main(String[] args) {
        var trainDay = LocalDate.of(2022, 5, 14);
        var time = LocalTime.of(10, 0);
        var zone = ZoneId.of("America/Los_Angeles");
        var zdt = ZonedDateTime.of(trainDay, time, zone);
        var instant = zdt.toInstant();
        instant = instant.plus(1, ChronoUnit.DAYS);
        System.out.println(instant);
    }
}
//

class Q15 {
    public static void main(String[] args) {
        boolean hasEggs = switch (1) {
            case 0 -> true;
            default -> false;
        };
        System.out.print(hasEggs);

        hasEggs = switch (0) {
            case 0 -> {
                yield true;
            }
            default -> {
                yield false;
            }
        };
        System.out.print(hasEggs);
    }
}

class Q16 {
    public static void main(String[] args) {
        int sum = IntStream.of(4, 6, 8)
                .boxed()
                //.parallel()
                .mapToInt(x -> x).sum();
        System.out.print(sum);
    }
}

class Q18 {
    public static void main(String[] args) {
        List list = Arrays.asList("Sunny");
        method(list);
    }

    private static void method(Collection<?> x) {
        x.forEach(a -> {
        });
    }
}

record Q19Coin(boolean heads) {
    public boolean equals(Object obj) {
        if (!(obj instanceof Q19Coin coin)) {
            return false;
        }
        return heads == coin.heads;
    }
}

interface Q21ApplyFilter {
    void filter(List<String> input);
}

class Q21FilterBobs {
    static Function<String, String> first = s ->
    {
        System.out.println(s);
        return s;
    };
    static Predicate<String> second = t -> "bob".equalsIgnoreCase(t);

    public void process(Q21ApplyFilter a, List<String> list) {
        a.filter(list);
    }

    public static void main(String[] contestants) {
        final List<String> people = new ArrayList<>();
        people.add("Bob");
        people.add("bob");
        people.add("Tina");
        people.add("Louise");
        final Q21FilterBobs f = new Q21FilterBobs();
        f.process(q -> {
            q.removeIf(second);
            q.forEach(a -> System.out.println(a));
        }, people);
    }
}
//

class Q22 {
    public static void main(String[] args) {
        List<?> n = new ArrayList<String>();
        //List<? extends RuntimeException> o = new ArrayList<Exception>();
        List<? super RuntimeException> p = new ArrayList<Exception>();
        //List<T> q = new ArrayList<?>();
        //List<T extends RuntimeException> r = new ArrayList<Exception>();
        //List<T super RuntimeException> s = new ArrayList<Exception>();
    }
}

class Q23 {
    static int[][] game = new int[5][5];

    public static void main(String[] args) {
        System.out.println(Arrays.deepToString(game));
        game[3][3] = 6;
        Object[] obj = game;
        System.out.println(Arrays.deepToString(obj));
        obj[3] = 'X';
        System.out.println(game[3][3]);
    }
}

class Q26CountSheep {
    private static AtomicInteger counter = new AtomicInteger();
    private final Object lock = new Object();

    public synchronized int increment1() {
        return counter.incrementAndGet();
    }

    public static synchronized int increment2() {
        return counter.getAndIncrement();
    }

    public int increment3() {
        synchronized (lock) {
            return counter.getAndIncrement();
        }
    }
}
//

class Q26Transport {
    static interface Vehicle {
    }

    static class Bus implements Vehicle {
    }

    public static void main(String[] args) {
        Bus bus = new Bus();
        Q26Transport.Vehicle d = new Bus();
        System.out.println(null instanceof Bus);
        System.out.println(bus instanceof Vehicle);
        System.out.println(bus instanceof Bus);
        //System.out.println(bus instanceof ArrayList);
        System.out.println(bus instanceof Collection);
    }
}
//

class Q28 {
    public static void main(String[] args) {
        var dice = new LinkedList<Integer>();
        dice.offer(3);
        dice.offer(2);
        dice.offer(4);
        dice.stream().filter(n -> n != 4).forEach(System.out::println);
    }
}

class Q32 {
    public static void main(String[] args) throws SQLException {
        var url = "jdbc:hsqldb:file:birds";
        var sql = "SELECT name FROM peacocks WHERE name = ?";
        try (var conn = DriverManager.getConnection(url);
             var stmt = conn.prepareStatement(sql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY)) {
            stmt.setString(1, "Feathers");
            //try (var rs = stmt.execute()) {
            var rs = stmt.executeQuery();
            System.out.println(rs.next());
            //}
        }
    }
}

class Q35ReadEverything {
    public void readFile(Path p) {
        try {
            Files.readAllLines(p)
                    .parallelStream()
                    .forEach(System.out::println);
        } catch (Exception e) {
        }
    }

    public void read(Path directory) throws Exception {
        Files.walk(directory)
                .filter(p -> Files.isRegularFile(p))
                .forEach(x -> readFile(x));
    }

    public static void main(String[] b) throws Exception {
        Path p = Paths.get("/Users/admin/IdeaProjects/ocajp829/tests/practiceBook/practice2");
        new Q35ReadEverything().read(p);
    }
}

class Q35 {
    public static void main(String[] args) {
        var x = new ArrayDeque<>();
        x.offer(18);
        x.peek();
        x.pop();
        x.poll();
        x.peekFirst();
    }
}

class Q36InitOrder {
    { System.out.print("1"); }
    static { System.out.print("2"); }
    public Q36InitOrder() {
        System.out.print("3");
    }
    public static void callMe() {
        System.out.print("4");
    }
    public static void main(String[] args) {
        callMe();
        callMe();
        System.out.print("5"); }}
//

abstract class Q38CarbonStructure {
    protected long count;
    public abstract Number getCount() throws IOException; //q1
    public Q38CarbonStructure(int count) {
        this.count = count;
    }
}
class Q38Diamond extends Q38CarbonStructure {
    public Q38Diamond() { super(15); }
    public Long getCount() throws FileNotFoundException { //q2
        return count;
    }
    public static void main(String[] cost) {
        try {
            final Q38CarbonStructure ring = new Q38Diamond(); // q3
            System.out.print(ring.getCount()); // q4
        } catch (IOException e) {
            e.printStackTrace(); }}}

class Q43{
    public static void main(String[] args) {
        var list = Arrays.asList("flower", "seed", "plant");
        Collections.sort(list);
        Collections.reverse(list);
        var result = Collections.binarySearch(list,
                list.get(0), Comparator.reverseOrder());
        System.out.println(result);
    }
}

class Q45 {
    public static void main(String[] args) {
        record Toy(String name){ }
        var toys = Stream.of(
                new Toy("Jack in the Box"),
                new Toy("Slinky"),
                new Toy("Yo-Yo"),
                new Toy("Rubik's Cube"));
        var spliterator = toys.spliterator();
        var batch1 = spliterator.trySplit();
        var batch2 = spliterator.trySplit();
        var batch3 = spliterator.trySplit();
        batch1.forEachRemaining(System.out::print);
        batch2.forEachRemaining(System.out::print);
        //batch3.forEachRemaining(System.out::print);
        spliterator.forEachRemaining(System.out::print);}
}
//

class Q46 {
    public void openDrawbridge() throws Exception {
        try {
            throw new Exception("Problem");
        } catch (IOException e) {
            throw new IOException();
        } catch (Exception e) {
            try {
                throw new IOException();
            } catch (Exception ex) {
            } finally {
                System.out.println("Almost done");
            }
        } finally {
            throw new RuntimeException("Unending problem");
        }}
    public static void main(String[] moat) throws Exception {
        new Q46().openDrawbridge();
    }
}

