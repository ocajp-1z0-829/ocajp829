package chapter8;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Chapter8 { }

class Q1 {
    void talk() {
        try {
            Thread.sleep(10_000);
        } catch (InterruptedException e) {
            System.out.println("How rude!");
        }}
    public static void main(String[] args) {
        var s = new Q1();
        var t = new Thread(() -> s.talk());
        //t.run();
        t.start();
        System.out.println(t.getState());
        t.interrupt();
        System.out.println(t.getState());
        t.interrupt();
        System.out.println(t.getState());
        t.interrupt();
        System.out.println(t.getState());
    }
}

class Q2 {
    public static void main(String[] args) {
        Callable c = new Callable() {
            @Override
            public Object call() throws Exception {
                System.out.print("X");
                return 10;
            }

            public Object run() {
                System.out.print("X");
                return 10;
            }
        };
        var s = Executors.newScheduledThreadPool(1);
        for(int i=0; i<10; i++) {
            Future f = s.submit(c);
            try {
                System.out.print(f.get());
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            } catch (ExecutionException e) {
                throw new RuntimeException(e);
            }
        }
        s.shutdown();
        System.out.print("Done!");
    }
}

class Q4 {
    private AtomicBoolean coin = new AtomicBoolean(false);
    void flip() {
        coin.compareAndSet(!coin.get(), coin.get());
    }
    public static void main(String[] gamble) throws Exception
    {
        var luck = new Q4();
        ExecutorService s = Executors.newCachedThreadPool();
        for(int i=0; i<1000; i++) {
            s.execute(() -> luck.flip());
        }
        s.shutdown();
        Thread.sleep(5000);
        System.out.println(luck.coin.get());
    }
}

class Q6Dance {
    int count = 0;
    synchronized int step() { return count++; }
    public static void main(String[] args) throws InterruptedException {
        var waltz = new Q6Dance();
        var dancers = new ArrayList<Thread>();
        for(int i=0; i<10; i++)
            dancers.add(new Thread(() -> waltz.step()));
        for(Thread dancer : dancers)
            dancer.start();
        dancers.forEach(d -> d.interrupt());
        Thread.sleep(5_000);
        System.out.print(waltz.count); }
}

class Q7Padlock {
    private Lock lock = new ReentrantLock();
    public void lockUp() {
        if (lock.tryLock()) {
            lock.lock(); //если 2 лока то анлока должно быть тоже 2
            System.out.println("Locked!");
            lock.unlock();
            //lock.unlock();
        }
    }
    public static void main(String... unused) throws Exception
    {
        var gate = new Q7Padlock();
        for(int i=0; i<5; i++) {
            new Thread(() -> gate.lockUp()).start();
            Thread.sleep(1_000);
        }
    }
}

class Q8TicketTaker {
    long ticketsSold;
    final AtomicInteger ticketsTaken;

    public Q8TicketTaker() {
        ticketsSold = 0;
        ticketsTaken = new AtomicInteger(0);
    }

    public void performJob() {
        IntStream.iterate(1, p -> p + 1)
                .parallel()
                .limit(100)
                .forEach(i -> ticketsTaken.getAndIncrement());
        IntStream.iterate(1, q -> q + 1)
                .parallel()
                .limit(500)
                .forEach(i -> ++ticketsSold);
        System.out.print(ticketsTaken + " " + ticketsSold);
    }

    public static void main(String[] matinee) {
        new Q8TicketTaker().performJob();
    }
}

class Q9 {
    public static void main(String[] args) {
        var original = List.of(1,2,3,4,5);
        var copy1 = new CopyOnWriteArrayList<Integer>(original);
        for(Integer w : copy1)
            copy1.remove(w);
        var copy2 = Collections.synchronizedList(original);
        for(Integer w : copy2)
            System.out.print(w);
            //copy2.remove(w);
        var copy3 = new ArrayList<Integer>(original);
        for(Integer w : copy3)
            System.out.print(w);
        //copy3.remove(w);
        var copy4 = new ConcurrentLinkedQueue<Integer>(original);
        for(Integer w : copy4)
            copy4.remove(w);
    }
}

class Q11Bull {
    void charge() {
        IntStream.range(1,6)
                .parallel()
                //.forEach(System.out::print);
                .forEachOrdered(System.out::print);
    }
    public static void main(String[] args) {
        var b = new Q11Bull();
        b.charge(); }}

class Q12TpsReport {
    public void submitReports() {
        var s = Executors.newCachedThreadPool();
        Future bosses = s.submit(() ->
                System.out.print("1"));
                s.shutdown();
        try {
            System.out.print(bosses.get());
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }
    }
    public static void main(String[] memo) { new Q12TpsReport().submitReports();}
}

class Q13 {
    public static void main(String[] args) {
        ExecutorService e1 = Executors.newFixedThreadPool(1);
        ExecutorService e2 = Executors.newScheduledThreadPool(1);
        ExecutorService e3 = Executors.newSingleThreadExecutor();
        ExecutorService e4 = Executors.newSingleThreadScheduledExecutor();
    }
}

class Q14CartoonCat {
    private void await(CyclicBarrier c) {
        try {
            c.await();
        } catch (Exception e) {}
    }
    public void march(CyclicBarrier c) {
        var s = Executors.newSingleThreadExecutor(); // 0
        //var s = Executors.newCachedThreadPool(); // 3
        for(int i=0; i<12; i++)
            s.execute(() -> await(c));
        s.shutdown();
    }
    public static void main(String... strings) {
        new Q14CartoonCat().march(new CyclicBarrier(4,
                () -> System.out.println("Ready")));
    }}

class Q17Circus {
    private static int seal = 0;
    private static volatile int tiger = 0;
    private static AtomicInteger lion = new AtomicInteger(0);
    public static void main(String[] tent) {
        Stream.iterate(0, x -> x + 1)
                .parallel()
                .limit(100)
                .forEach(q -> {seal++; tiger++; lion.incrementAndGet();});
        System.out.println(seal + "," + tiger + ","+ lion); }}

class Q18Race {
    ExecutorService service =
            Executors.newFixedThreadPool(8);
    public static int sleep() {
        try { Thread.sleep(1000); } catch (Exception e) {}
        return 1;
    }
    public void hare() {
        try {
            Callable<Integer> c = Q18Race::sleep;
            final var r = List.of(c,c,c);
            var results = service.invokeAll(r);
            System.out.println("Hare won the race!");
        } catch (Exception e) {e.printStackTrace();}
    }
    public void tortoise() {
        try {
            Callable<Integer> c = () -> sleep();
            final var r = List.of(c,c,c);
            Integer result = service.invokeAny(r);
            System.out.println("Tortoise won the race!");
        } catch (Exception e) {e.printStackTrace();}
    }
    public static void main(String[] p) throws Exception {
        var race = new Q18Race();
        race.service.execute(() -> race.hare());
        race.service.execute(() -> race.tortoise());
        race.service.awaitTermination(2, TimeUnit.SECONDS);
        race.service.shutdown();

    }}

class Q19 {
    public static void main(String[] args) {
        //ConcurrentSkipList;
        new ConcurrentSkipListSet();
        new CopyOnWriteArrayList();
        new ConcurrentSkipListMap();
        new ConcurrentLinkedQueue();
        new LinkedBlockingQueue();
    }
}

class Q20Accountant {
    public static void completePaperwork() {
        System.out.print("[Filing]");
    }
    public static double getPi() {
        return 3.14159;
    }
    public static void main(String[] args) throws Exception
    {
        ExecutorService x =
                Executors.newSingleThreadExecutor();
        Future<?> f1 = x.submit(() -> completePaperwork());
        Future<Object> f2 = x.submit(() -> getPi());
        System.out.print(f1.get()+" "+f2.get());
        x.shutdown();
    }}

class Q21Clock {
    private AtomicLong bigHand = new AtomicLong(0);
    void incrementBy10() {
        bigHand.getAndSet(bigHand.get() + 10);
    }
    public static void main(String[] c) throws Exception {
        var smartWatch = new Q21Clock();
        ExecutorService s = Executors.newCachedThreadPool();
        for(int i=0; i<100; i++) {
            s.submit(() -> smartWatch.incrementBy10()).get();
        }
        s.shutdown();
        s.awaitTermination(10, TimeUnit.SECONDS);
        System.out.println(smartWatch.bigHand.get());
    }}

class Q22Riddle {
    public void sleep() {
        try { Thread.sleep(5000); } catch (Exception e) {}
    }
    public String getQuestion(Q22Riddle r) {
        synchronized(this) {
            sleep();
            if(r != null) r.getAnswer(null);
            return "How many programmers does it take "
                    + "to change a light bulb?";
        }}
    public synchronized String getAnswer(Q22Riddle r) {
        sleep();
        if(r != null) r.getAnswer(null);
        return "None, that's a hardware problem";
    }
    public static void main(String... ununused) {
        var r1 = new Q22Riddle();
        var r2 = new Q22Riddle();
        var s = Executors.newFixedThreadPool(2);
        s.submit(() -> r1.getQuestion(r2));
        s.execute(() -> r2.getAnswer(r1));
        s.shutdown();
    }}

class Q23 {
    public static void main(String[] args) {
        ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
//                .scheduleAtFixedRate();
//                .scheduleWithFixedDelay();
    }
}

class Q24Athlete {
    int stroke = 0;
    public synchronized void swimming() {stroke++; }
    private int getStroke() {
        synchronized(this) { return stroke; }
    }
    public static void main(String... laps) throws InterruptedException {
        ExecutorService s = Executors.newFixedThreadPool(10);
        Q24Athlete a = new Q24Athlete();
        for(int i=0; i<1000; i++) {
            s.execute(() -> a.swimming());
        }
        s.shutdown();
        //s.awaitTermination(2, TimeUnit.SECONDS);
        System.out.print(a.getStroke()); }
}

class Q26ThreadSafeList {
    private List<Integer> data = new ArrayList<>();
    public synchronized void addValue(int value) {
        data.add(value);
    }
    public int getValue(int index) {
        return data.get(index);
    }
    public int size() {
        synchronized(Q26ThreadSafeList.class) {
            return data.size();
        }}

    public static void main(String[] args) throws InterruptedException {
        Q26ThreadSafeList list = new Q26ThreadSafeList();
        new Thread(() -> list.addValue(1)).start();
        new Thread(() -> list.addValue(2)).start();
        new Thread(() -> list.addValue(3)).start();
        new Thread(() -> list.addValue(4)).start();
        Thread.sleep(1000);
        System.out.println(list.data);
        System.out.println(list.size());
        System.out.println(list.getValue(0));
    }
}

class Q27 {
    public static void main(String[] args) {
        print1(1);
        print2(new AtomicInteger(1));
    }
    public static synchronized void print1(int counter) {
        System.out.println(counter--);
        System.out.println(++counter);
    }
    public static synchronized void print2(AtomicInteger
                                                   counter) {
        System.out.println(counter.getAndDecrement());
        System.out.println(counter.incrementAndGet());
    }
}

class Q28Bounce {
    public static void main(String... legend) {
        List.of(1,2,3,4).stream()
                .forEach(System.out::println);
        List.of(1,2,3,4).parallelStream()
                .forEach(System.out::println);
        List.of(1,2,3,4).parallelStream()
                .forEachOrdered(System.out::println);
    }}

class Q29 {
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        ScheduledExecutorService t = Executors.newSingleThreadScheduledExecutor();
        Future result = t.submit(() -> System.out.println("result"));
        t.invokeAll(List.of());
        t.scheduleAtFixedRate(() -> {return;},5, 2, TimeUnit.MINUTES).get();
        t.shutdown();
    }
}

class Q30Boat {
    private void waitTillFinished(CyclicBarrier c) {
        try {
            c.await();
            System.out.print("W");
        } catch (Exception e) {}
    }
    public void row(ExecutorService s) {
        var cb = new CyclicBarrier(5);
        IntStream.iterate(1, i-> i+1)
                .limit(15)
                .forEach(i -> s.submit(() ->
                        waitTillFinished(cb)));
    }
    public static void main(String[] oars) {
        ExecutorService service = null;
        try {
            service = Executors.newCachedThreadPool();
            new Q30Boat().row(service);
        } finally {
            service.isShutdown();
            service.shutdown();
        }}}

class Q32 {
    public static void main(String[] args) {
        var db = Collections.synchronizedList(new ArrayList<>());
        IntStream.range(1,6)
                .parallel()
                .map(i -> {db.add(i); return i;})
                .forEachOrdered(System.out::print);  // p1
        System.out.println();
        db.forEach(System.out::print);
    }
}

class Q33Television {
    private static Lock myTurn = new ReentrantLock();
    public void watch() {
        try {
            //if (myTurn.lock(5, TimeUnit.SECONDS)) {
            if (myTurn.tryLock(5, TimeUnit.SECONDS)) {
                System.out.println("TV Time");
                myTurn.unlock();
            }
        } catch (InterruptedException e) {}
    }
    public static void main(String[] t) throws Exception {
        var newTv = new Q33Television();
        for (int i = 0; i < 3; i++) {
            new Thread(() -> newTv.watch()).start();
            Thread.sleep(10*1000);
        }
    }}

class Q34 {
    public static void main(String[] args) {
        var original = new ArrayList<Integer>(List.of(1,2,3));
        var copy1 = new ArrayList<Integer>(original);
        //for(Integer q : copy1)
            copy1.add(1);
        var copy2 = new CopyOnWriteArrayList<Integer>(original);
        for(Integer q : copy2)
            copy2.add(2);
        System.out.println(copy2);
        var copy3 = new LinkedBlockingQueue<Integer>(original);
        //for(Integer q : copy3) {
            copy3.offer(3);
            System.out.println(copy3);
        //}
        var copy4 = Collections.synchronizedList(original);
        //for(Integer q : copy4)
            copy4.add(4);
    }
}

class Q36Bank {
    static int cookies = 0;
    public synchronized void deposit(int amount) {
        cookies += amount;
    }
    public static synchronized void withdrawal(int amount) {
        cookies -= amount;
    }
    public static void main(String[] amount) throws
            Exception {
        var teller = Executors.newScheduledThreadPool(50);
        Q36Bank bank = new Q36Bank();
        for(int i=0; i<25; i++) {
            teller.submit(() -> bank.deposit(5));
            teller.submit(() -> bank.withdrawal(5));
        }
        teller.shutdown();
        teller.awaitTermination(10, TimeUnit.SECONDS);
        System.out.print(bank.cookies);
    }
}

class Q37SearchList<T> {
    private List<T> data;
    private boolean foundMatch = false;
    public Q37SearchList(List<T> list) {
        this.data = list;
    }
    public void exists(T v,int start, int end) {
        if(end-start==0) {}
        else if(end-start==1) {
            foundMatch = foundMatch ||
                    v.equals(data.get(start));
        } else {
            final int middle = start + (end-start)/2;
            new Thread(() -> exists(v,start,middle)).start();
            new Thread(() -> exists(v,middle,end)).start();
        }
    }
    public static void main(String[] a) throws Exception {
        List<Integer> data = List.of(1,2,3,4,5,6);
        Q37SearchList<Integer> t = new Q37SearchList<Integer>(data);
        t.exists(5, 0, data.size());
        System.out.print(t.foundMatch); }}

class Q38 {
    public static void main(String[] args) {
        var mitchsWorkout = new CopyOnWriteArrayList<Integer>();
        List.of(1,5,7,9).stream().parallel()
                .forEach(mitchsWorkout::add);
        mitchsWorkout
                .forEach(System.out::print);  // q1
        List.of(1,5,7,9).stream().parallel()
                .forEachOrdered(System.out::print);  // q2
    }
}

class Q39Moon {
    private volatile AtomicLong green = new AtomicLong(0);
    private volatile int cheese = 0;
    private volatile int rocket = 0;
    private void takeOff() {
        Stream.iterate(0, x -> x +
                1).parallel().limit(1000).forEach(q -> {
            green.getAndIncrement();
            cheese++;
            synchronized (this) {
                ++rocket; }
        });
        System.out.println(green + "," + cheese + "," +
                rocket);
    }
    public static void main(String[] tent) {
        new Q39Moon().takeOff(); }}

class Q40 {
    public static void main(String[] args) {

        var drink = List.of("coke", "soda", "pop");
        System.out.print(drink.parallelStream()
                .parallel()
                .reduce(0, (c1, c2) -> c1 + c2.length(), (s1, s2) -> s1
                        + s2));
        System.out.print(drink.stream()
                .reduce(0, (c1, c2) -> c1 + c2.length(), (s1, s2) -> s1
                        + s2));
    }
}