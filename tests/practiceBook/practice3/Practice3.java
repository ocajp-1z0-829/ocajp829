package practice3;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Practice3 {
}

class Q2Legos {
    public static void main(String[] args) {
        var ok = true;
        if (ok) {
            var sb = new StringBuilder();
            sb.append("red");
            sb.deleteCharAt(0);
            sb.delete(1, 1);
            System.out.println(sb);
            System.out.print(sb.substring(0, 0));
        }
    }
}

class Q4 {
    private static double getNumber() {
        return .007;
    }

    public static void main(String[] args) {
        Supplier<Double> s = Q4::getNumber;
        double d = s.get();
        System.out.println(d);
    }
}

class Q5 {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        var e = Executors.newSingleThreadExecutor();
        Runnable r1 = () -> Stream.of(1, 2, 3).parallel();
        Callable r2 = () -> Stream.of(4, 5, 6).parallel();
        Future<?> f1 = e.submit(r1);  // x1
        Future<Stream<Integer>> f2 = e.submit(r2);  // x2
        var r = Stream.of(Stream.of(f1.get()), f2.get())
                .flatMap(p -> p)                         // x3
                .parallel()                              // x4
                //.peek(System.out::println)
                .filter(Objects::nonNull)
                .collect(Collectors.groupingByConcurrent(i -> (Integer) i % 2 == 0));
        System.out.println(r);
        System.out.print(r.get(false).size() + " " + r.get(true).size());

        e.shutdown();
    }
}

interface Q8DoubleBass {
    void strum();

    default int getVolume() {
        return 5;
    }
}

interface Q8BassGuitar {
    void strum();

    default int getVolume() {
        return 10;
    }
}

abstract class Q8ElectricBass implements Q8DoubleBass, Q8BassGuitar {
    @Override
    public void strum() {
        System.out.print("X");
    }

    @Override
    public int getVolume() {
        return Q8DoubleBass.super.getVolume();
    }
}

class Q8RockBand {
    public static void main(String[] strings) {
        final class MyElectricBass extends Q8ElectricBass {
            public int getVolume() {
                return 30;
            }

            public void strum() {
                System.out.print("Y");
            }
        }
    }
}
//

interface Q15HasHue {
    String getHue();
}

enum Q15COLORS implements Q15HasHue {
    red {
        public String getHue() {
            return "FF0000";
        }
    }, green {
        public String getHue() {
            return "00FF00";
        }
    };

    private Q15COLORS() {
    }
}

class Q15Book {
    static void main(String[] pencils) {
    }
}

final class Q15ColoringBook extends Q15Book {

    final void paint(Q15COLORS c) {
        System.out.print("Painting: " + c.getHue());
    }

    final public static void main(String[] crayons) {
        new Q15ColoringBook().paint(Q15COLORS.green);
    }
}
//

class Q17 {
    public static void main(String[] args) {
        var localDate = LocalDate.of(2022, Month.NOVEMBER, 6);
        var localTime = LocalTime.of(1, 0);
        var zone = ZoneId.of("America/New_York");
        var z = ZonedDateTime.of(localDate, localTime, zone);
        System.out.println(z);
        var offset = z.getOffset();
        for (int i = 0; i < 6; i++)
            z = z.plusHours(1);
        System.out.println(z);
        System.out.print(z.getHour() + " " + offset.equals(z.getOffset()));
    }
}

class Q19 {
    public static void main(String[] args) throws SQLException {
        String sql = "{call update_data(?)}";
        try (Connection conn = DriverManager.getConnection("");
             CallableStatement cs = conn.prepareCall(sql)) {
            //Statement cs = conn.prepareCall(sql)) {
            cs.setInt(1, 6);
            var rs = cs.execute();
        }
    }
}
//

class Q21 {
    public void countPets() {
        record Pet(int age) {
        }
        record PetSummary(long count, int sum) {
        }
        var summary = Stream.of(new Pet(2), new Pet(5), new
                        Pet(8))
                .collect(Collectors.teeing(
                        Collectors.counting(),
                        Collectors.summingInt(Pet::age),
                        PetSummary::new));
        System.out.println(summary);
    }

    public static void main(String[] args) {
        new Q21().countPets();
    }
}

class Q25 {
    public static void main(String[] args) {
        String cheese = ServiceLoader.load(String.class)
                .stream()
                .map(ServiceLoader.Provider::get)
                .findFirst()
                .orElse("As");
        System.out.println(cheese);
    }
}

abstract class Q26 {
    boolean pass;

    protected abstract boolean passed();

    static class JavaProgrammerCert extends Q26 {
        private Q26 part1;
        private Q26 part2;

        public boolean passed() {
//            return part1.pass && part2.pass;
            return passed();
        }
    }

    public static void c() {
        System.out.println(new JavaProgrammerCert().passed());
    }

    public static void main(String[] args) {
        System.out.println();
    }
}
//

class Q27 {
    public static void main(String[] args) {
        var smart = """
                barn owl\n\n
                wise\n\n
                """;
        var clever = """
                     barn owl\n\n
                 wise
                """;
        var sly = """
                barn owl\n\n
                wise""";
        System.out.println(smart.equals(smart.indent(0)));
        System.out.println("start");
        System.out.println(smart);
        System.out.println("end");
        System.out.println("start");
        System.out.println(smart.strip());
        System.out.println("end");
        System.out.println(smart.equals(smart.strip()));
        System.out.println(smart.equals(smart.stripIndent()));

        System.out.println(clever.equals(clever.indent(0)));
        System.out.println(clever.equals(clever.strip()));
        System.out.println(clever.equals(clever.stripIndent()));

        System.out.println(sly.equals(sly.indent(0)));
        System.out.println(sly.equals(sly.strip()));
        System.out.println(sly.equals(sly.stripIndent()));
    }
}

class Big {
    public Big(boolean stillIn) {
        super();
    }
}

class Trouble extends Big {
    public Trouble() {
        super(true);
    }

    public Trouble(int deep) {
        super(false);
        //this();
    }

    public Trouble(String now, int... deep) {
        this(3);
    }

    public Trouble(long deep) {
        this("check", (int) deep);
    }

    public Trouble(double test) {
        super(test > 5 ? true : false);
    }
}

class Q31 {
    public static void main(String[] args) {
        Path x = Paths.get(".", "song", "..", "/note");
        Path y = Paths.get("/dance/move.txt");
        System.out.println(x.normalize());
        System.out.println(x.resolve(y));
        System.out.println(y.resolve(x));
    }
}

class Q33 {
    public static void main(String[] args) {
        var list = Arrays.asList("0", "1", "01", "10");
        Collections.reverse(list);
        Collections.sort(list);
        System.out.println(list);
    }
}

class Q36 {
    public static void main(String[] args) {
        int[] crossword[] = new int[10][20];
        for (int i = 0; i < crossword.length; i++)
            for (int j = 0; j < crossword.length; j++)
                crossword[i][j] = 'x';
        System.out.println(crossword.length);
        System.out.println(Arrays.deepToString(crossword));
    }
}

class Q37 {
    public static void main(String[] args) {
        String names = Stream.of(
                        "bald eagle", "pronghorn", "puma", "sea lion")

                .collect(Collectors.toUnmodifiableSet())
                .stream()
                .collect(Collectors.groupingBy(s -> s.contains(" ")))
                .entrySet()
                .stream()
                .filter(e -> e.getKey())
                .map(Map.Entry::getValue)
                .flatMap(List::stream)
                .sorted(Comparator.reverseOrder())

                .collect(Collectors.joining(", "));
        System.out.println(names);
    }
}

class Q38 {
    public static void main(String[] args) {
        LocalDate dogDay = LocalDate.of(2022, 8, 26);
        var x = DateTimeFormatter.ISO_DATE;
        System.out.println(x.format(dogDay));
        var y = DateTimeFormatter.ofPattern("Dog Day: dd/MM/yy");
        System.out.println(y.format(dogDay));
        var z = DateTimeFormatter.ofPattern("Dog Day: dd/MM/yy");
        System.out.println(dogDay.format(z));
    }
}

class Q39 {
    public static void main(String[] args) {
        //long min1 = 123.0, max1 = 987L;
        final long min2 = 1_2_3, max2 = 9____________8____________7;
        //long min3 = 123, int max3 = 987;
        long min4 = 123L, max4 = 987;
        //long min5 = 123_, max5 = _987;
    }
}

class Q41 {
    public static void main(String[] args) {
        char[][] letters = new char[][]{
                new char[]{'a', 'e', 'i', 'o', 'u'},
                new char[]{'a', 'e', 'o', 'u'}};
        var x = Arrays.mismatch(letters[0], letters[0]);
        var y = Arrays.compare(letters[0], letters[0]);
        var z = Arrays.compare(letters[0], letters[1]);
        System.out.print(x + " " + y + " " + z);

    }
}

class Q42Cruise implements Serializable {
    private int numPassengers = 1;
    private transient String schedule = "NONE";

    {
        numPassengers = 2;
    }

    public Q42Cruise() {
        this.numPassengers = 3;
        this.schedule = "Tropical Island";
    }

    public static void main(String... p) throws Exception {
        final String f = "/Users/admin/IdeaProjects/ocajp829/tests/practiceBook/practice3/ship.txt";
        try (var o = new ObjectOutputStream(
                new FileOutputStream(f))) {
            Q42Cruise c = new Q42Cruise();
            c.numPassengers = 4;
            c.schedule = "Casino";
            o.writeObject(c);
        }
        try (var i = new ObjectInputStream(
                new FileInputStream(f))) {
            Q42Cruise c = (Q42Cruise) i.readObject();
            System.out.print(c.numPassengers + "," +
                    c.schedule);
        }
    }
}
//

class Q43Coin {
    enum Side {HEADS, TAILS}

    ;

    public static void main(String[] args) {
        var sides = Side.values();
        for (var s : sides)
            for (int i = sides.length - 1; i > 0; i -= 2)
                System.out.print(s + " " + sides[i]);
        System.out.println("a");
    }
}

class Q45 {
    public static void main(String[] args) {
        var m = new TreeMap<Integer, Integer>();
        m.put(1, 4);
        m.put(2, 8);

        m.putIfAbsent(2, 10);
        m.putIfAbsent(3, 9);

        m.replaceAll((k, v) -> k + 1);

        m.entrySet().stream()
                .sorted(Comparator.comparing(Map.Entry::getKey))
                .limit(1)
                .map(Map.Entry::getValue)
                .forEach(System.out::println);
    }
}

class Music {
    void make() throws IOException {
        throw new UnsupportedOperationException();
    }}
class Sing extends Music {
    public void make() {
        System.out.println("do-re-mi-fa-so-la-ti-do");
    }}

