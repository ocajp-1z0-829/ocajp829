package practice1;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;
import java.util.function.DoubleConsumer;
import java.util.function.IntUnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Practice1 {
}

class Q1StreamOfStreams {
    public static void main(String[] args) {
        var result = Stream.of(getNums(9, 8), getNums(22, 33))  // c1
                .flatMap(x -> x)
                .map(a -> a)
                //.filter(x -> !x.isEmpty())
                //.get()
                ;
        System.out.println(result);
    }

    private static Stream<Integer> getNums(int num1, int num2) {
        return Stream.of(num1, num2);
    }
}
//

class Q2InformationException extends Exception {
}

class Q2LackOfInformationException
        extends Q2InformationException {
    //public Q2LackOfInformationException() {super("");}

    public Q2LackOfInformationException(String s) {
        this(new Exception(s));
    }

    public Q2LackOfInformationException(Exception c) {  // t3
        super();
    }

    public String getMessage() throws ArithmeticException {
        return "lackOf";
    }
}
//

class Q6 {
    public static void main(String[] args) {
        IntUnaryOperator iuo1 = v -> {
            System.out.print("Hello !");
            return 2 % 1;
        };

        //IntUnaryOperator iuo2 = (Integer w) -> w.intValue();
        IntUnaryOperator iuo3 = (int j) -> (int) 30L;
        //IntUnaryOperator iuo4 = (int q) -> q / 3.1;
        //IntUnaryOperator iuo5 = (long x) -> (int) x;
        IntUnaryOperator iuo6 = z -> z;
    }
}
//

class Q9Roller<E extends Q9Wheel> {
    public void roll(E e) {
    }
}

class Q9Wheel {
}

class Q9CartWheel extends Q9Wheel {
}

class RollingContest {
    Q9Roller<Q9CartWheel> wheel1 = new Q9Roller<Q9CartWheel>();
    //Q9Roller<Q9Wheel> wheel2 = new Q9Roller<Q9CartWheel>();
    Q9Roller<? extends Q9Wheel> wheel3 = new Q9Roller<Q9CartWheel>();
    Q9Roller<? extends Q9Wheel> wheel4 = new Q9Roller<Q9Wheel>();
    //Q9Roller<? super Q9Wheel> wheel5 = new Q9Roller<Q9CartWheel>();
    Q9Roller<? super Q9Wheel> wheel6 = new Q9Roller<Q9Wheel>();
}

class Q13 {
    public static void main(String[] args) {
        ServiceLoader s = ServiceLoader.load(String.class);
    }
}

class Q17 {
    public static void main(String[] args) {
        LocalDateTime pi = LocalDateTime.of(2022, 3, 14, 1,
                59);
        DateTimeFormatter formatter = DateTimeFormatter
                .ofPattern("m.ddhh'MM'");
        System.out.print(formatter.format(pi));
    }
}


class Q19Kitchen {
    class Mixer {
        class Spinner {
        }
    }

    public void bake() {
        var a = new Q19Kitchen().new Mixer().new Spinner();
        //Mixer.Spinner b = Mixer.new Spinner();
        //var c = new Spinner();
        var d = new Mixer().new Spinner();
        Q19Kitchen.Mixer.Spinner e = new Q19Kitchen().new Mixer().new Spinner();
        //Spinner f = new Kitchen().new Mixer().new Spinner();
    }
}

class Q22 {
    public static void main(String[] args) {
        Double tea = null;
        Object coffee = (Integer) null;
        String juice = "";
        if (coffee instanceof Integer x) {
            System.out.println(x.intValue());
        } else if (tea < juice.length()) {
            System.out.println(tea);
        }
    }
}

abstract class Q24Phantom {
    public void bustLater(DoubleConsumer buster, double
            value) {
        buster.accept(value);
    }
}

class Q24Ghost extends Q24Phantom {
    public void bustNow(Consumer<Double> buster, double
            value) {
        buster.accept(value);
    }

    public static void main(String[] args) {
        new Q24Ghost().call();
    }

    void call() {
        var value = 10.0;
        bustNow(System.out::print, value);
        //bustNow(w -> System.out::println, value);
        bustNow(v -> System.out.print(v), value);
        //bustNow(u -> System.out.println((long)u), value);
        bustNow(g -> {
            System.out.println();
        }, value);
        bustNow(a -> {
            System.out.println(a.intValue());
        }, value);
        bustLater(System.out::print, value);
        //bustLater(w -> System.out::println, value);
        bustLater(u -> System.out.println((long) u), value);
        bustLater(g -> {
            System.out.println();
        }, value);
        //bustLater(a -> {System.out.println(a.intValue());}, value);
    }
}

class Q26Jump {
    private static void await(CyclicBarrier b) {
        try {
            b.await();
        } catch (Exception e) {
        }
    }

    public static void main(String[] chalk) {
        ExecutorService s = Executors.newFixedThreadPool(4);
        final var b = new CyclicBarrier(4,
                () -> System.out.print("Jump!"));
        for (int i = 0; i < 10; i++)
            s.execute(() -> await(b));
        s.shutdown();
    }
}

class Q28 {
    public static void printLength() {
        record Earthworm(int length) {
            Earthworm {
                length = 4;
            }
        }
        var worm = new Earthworm(6);
        System.out.println(worm.length());
    }

    public static void main(String[] args) {
        printLength();
    }
}
//

class Q32TieShoes {
    private volatile Lock shoes = new ReentrantLock();

    public void tie() {
        try {
            if (shoes.tryLock()) {
                System.out.println("Tie!");
                shoes.unlock();
            }
        } catch (Exception e) {
        }
    }

    public static void main(String... unused) {
        var gate = new Q32TieShoes();
        for (int i = 0; i < 10; i++) {
            var t = new Thread(() -> gate.tie());
            t.start();
            t.interrupt();
        }
    }
}

class Q33 {
    public static void main(String[] args) {
        var bed = List.of((short) 2, (short) 5);
        var pillow = bed.parallelStream().reduce(0,
                (a, b) -> (int) (b.doubleValue() + a.doubleValue()),
                (c, d) -> (int) (d.doubleValue() + c.doubleValue()));
        System.out.println(pillow);
    }
}

interface Q34Plant {
    default String grow() {
        return "Grow!";
    }
}

interface Q34Living {
    public default String grow() {
        return "Super Growing!";
    }
}

class Q34Tree implements Q34Plant, Q34Living {  // m1
    public String grow() {
        return Q34Plant.super.grow();
    }

    public static void main(String[] leaves) {
        Q34Plant p = new Q34Tree();
        System.out.print(((Q34Living) p).grow());
    }
}
//

class Q39RemoveMe<T> {
    private List<T> values;

    public Q39RemoveMe(T... values) {
        this.values = Arrays.stream(values)
                .collect(Collectors.toList());
    }

    public void remove(T value) {
        values.remove(value);
    }

    public static void main(String[] args) {
        var integer = new Q39RemoveMe<Integer>(2, 7, 1, 8);
        var longs = new Q39RemoveMe<Long>(2L, 7L, 1L, 8L);
        integer.remove(1);
        longs.remove(1L);

        System.out.println(integer.values);
        System.out.println(longs.values);

        var values = new ArrayList<Integer>();
        values.add(2);
        values.add(7);
        values.add(1);
        values.add(8);
        values.remove(1);
        System.out.println(values);
    }
}

class Q42 {
    public static void main(String[] args) {
        var localDate = LocalDate.of(2022, 11, 6);
        var localTime = LocalTime.of(1, 0);
        var zone = ZoneId.of("America/New_York");
        var z = ZonedDateTime.of(localDate, localTime, zone);
        var offset = z.getOffset();
        for (int i = 0; i < 6; i++)
            z = z.plusHours(1);
        System.out.println(z.getHour() + " " + offset.equals(z.getOffset()));
    }
}

record Q49Runner(int numberMinutes) { }
class Q49 {
    public static void main(String[] args) {
        var match = Optional.ofNullable(161);  // line z
        var runners = Stream.of(new Q49Runner(183),
                new Q49Runner(161), new Q49Runner(201));
        var opt = runners.map(Q49Runner::numberMinutes)
                .filter(m -> match.get().equals(m))
                .peek(System.out::println)
                .count();
    }
}


