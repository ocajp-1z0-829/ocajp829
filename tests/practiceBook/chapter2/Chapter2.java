package practiceBook.chapter2;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Chapter2 {
}

class Q7 {
    public static void main(String[] args) {
	var quest = new String[3];
	System.out.print(quest.length);
	for(var zelda : quest) {
	    System.out.print(" " + zelda);
	}

	System.out.println();
	var quest2 = new int[] {3};
	System.out.print(quest2.length);
	for(var zelda : quest2) {
	    System.out.println(" " + zelda);
	}

	var quest3 = List.of(3);
	System.out.print(quest3.size());
	for(var zelda : quest3) {
	    System.out.print(" " + zelda);
	}
    }
}

class Q8 {
    public static void main(String[] args) {
	var numPenguins = 0;
	switch (numPenguins) {
	default:
		case 0 : System.out.println("no penguins");
		case 1 : System.out.println("one penguin");
	}

    }
}

class Q10 {
    public static void main(String[] args) {
	int magicNumber = 7;
	var ok = switch (magicNumber) {
	    case 7 -> true; //break;
	    case 9 -> { yield true; }
	    case 11 -> {yield true; }
	    case 13 /*:*/ -> { yield true; }
	    default -> false;
	};
    }
}

class Q12 {
    public static void main(String[] args) {
	Q12 q12 = new Q12();
	q12.zero(0);
    }
    public void zero(Object number) {
	if (number instanceof Integer n /*|| Math.abs(n) == 0*/)
		System.out.println("zero");
	else if (number instanceof Integer n && Math.abs(n) == 0) {
	    System.out.println("zero 2");
	} else {
	    System.out.println("non-zero");
	}
    }

}

class Q13 {
    public static void main(String[] input) {
	var plan = 1;
	plan = plan++ + --plan;
	if (plan == 1) {
	    System.out.print("Plan A");
	} else {
	    if (plan == 2)
		System.out.print("Plan B");
	}
	// } else System.out.print("Plan C");}
    }
}

class Q16 {
    public static void main(String[] args) {
	Q16 q16 = new Q16();
	q16.perform(new String[]{"1", "2", "3"});

    }
    public void perform(String[] circus) {
	for (int i=circus.length-1; i>=0; i--)
	    System.out.print(circus[i]);
	System.out.println();

	for (int i=circus.length; i>0; i--)
	    System.out.print(circus[i-1]);

	System.out.println();
	for (var i=0; i<circus.length; i++)
	    System.out.print(circus[circus.length-i-1]);
    }
}

class Q18 {
    public static void main(String[] args) {
	final var GOOD = 100;
	var score = 10;
	switch (score) {
	default:
	    	case 1 : System.out.print("1-");
	    	case -1 : System.out.print("2-"); break;
		case 4,5 : System.out.print("3-");
		case 6 : System.out.print("4-");
		case 9 : System.out.print("5-");
	}

    }
}

class Q20 {
    public static void main(String[] args) {
	int colorOfRainbow = 10;
	final var red = 5;
	switch(colorOfRainbow) {
	default:
	    System.out.print("Home");
	    break;
	case red:
	    System.out.print("Away");
	} } }

class Q21 {
    public static void main(String[] args) {
	Q21 q21 = new Q21();
	q21.meow(new ArrayList<>());
    }
    public void meow(Collection<String> kitties) {
	if (kitties instanceof List c) {
	    System.out.println("L " + c.size());
	} else if (kitties instanceof Map c) {
	    c = new TreeMap<>(); // x1
	    System.out.println("M " + c.size());
	} else {
	    //System.out.println("E " + c.size());
	}
    }
}

class Q22 {
    public static void main(String[] args) {
	magic();
    }
	public static void magic() {
	     do {
		 int trick = 0;
		 LOOP: do {
		     trick++;
		     System.out.println(trick);
		     } while (trick < 2 /*2--*/);
		 //continue LOOP;
		 } while (1> 2);
	     //System.out.println(trick);
	     }
}

class Q25 {
    public static void main(String[] args) {
	while (!false) {

	}
    }

    public static void main1() {
	do {

	}while (true);
    }

    public static void main2() {
	for (;;){}
    }
}

class Q26 {
    public static void main(String[] args) {
	Integer value = 123;
	if(value instanceof Integer) {}
	//if(value instanceof Integer data) {} // DOES NOT COMPILE

	Q26 q26 = new Q26();
	q26.p(1);
	q26.p(LocalDate.now());
    }

    void printIntegersOrNumbersGreaterThan5(Number number) {
	if (!(number instanceof Integer data) || data.compareTo(5) > 0)
	//System.out.print(data);
	    System.out.println();
    }


    public void m(Object obj) {
	if (obj instanceof LocalDate date)
	    System.out.println(date);
	else
	    System.out.println();
	    //System.out.println(date);
    }
    public void n(Object obj) {
	if (obj instanceof LocalDate date)
	    return;
	else
	    System.out.println();
	    //System.out.println(date);
    }
    public void o(Object obj) {
	//if (!obj instanceof LocalDate date)
	    return;
	//else
	  //  System.out.println(date);
    }
    public void p(Object obj) {

	if (obj instanceof String) {
	    System.out.println("Str ");
	}
	else if (!(obj instanceof LocalDate date)) {
	    return;
	}
	else if (obj instanceof Number) {
	    System.out.println(date);
	} else {
	    System.out.println("else " + date);
	}
    }
    public void q(Object obj) {
	//if (!obj instanceof LocalDate date)
	    return;
	//System.out.println(date);
    }
    public void r(Object obj) {
	if (!(obj instanceof LocalDate date))
	    return;
	System.out.println(date);
    }

    public void r3(Object obj) {
	if (!(obj instanceof LocalDate date))
	//System.out.println(date);
	//System.out.println(date);
	    System.out.println();
    }

    public void r2(Object obj) {
	if (obj instanceof LocalDate date)
	    return;
	//System.out.println(date);
    }
}

class Q27 {
    public static void main(String[] args) {
	for (int k=0; k < 5; k++) {
	    System.out.print(k); //01234
	}
	System.out.println();
	for (int k=1; k <= 5; k++) {
	    System.out.print(k); //12345
	}
	System.out.println();
	int k=0;
	do {
	    System.out.print(k); //012345
	} while(k++ < 5);

	System.out.println();
	k=0;
	while (k++ < 5) {
	    System.out.print(k); //12345
	}
    }
}

class Q34 {
    public static void main(String[] args) {
	do {

	} while (1>2);

	while (2<3);
    }
}

class Q36 {
    public static void main(String[] args) {
	var nycTour = new String[] { "Downtown", "Uptown", "Brooklyn" };
	var times = new String[] { "Day", "Night" };
	for (int i=0, j=1; i<nycTour.length && j<times.length; i++, j++)
		System.out.println(nycTour[i] + "-" + times[j]);

	for (int i=1, j=0; i<nycTour.length && j<times.length; i++, j++)
	    	System.out.println(nycTour[i] + "-" + times[j]);
    }
}

class Q38 {
    public static void main(String[] args) {
	 int secret = 0;
	 for (int i = 0; i < 10; i++)
	     while (i < 10)
		 if (i == 5)
		     System.out.println("if");
		 else {
		     System.out.println("in");
		     System.out.println("else");
		 }
	 switch (secret) {
	 	case 0: System.out.println("zero");
	     }
    }
}