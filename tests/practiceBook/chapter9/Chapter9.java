package chapter9;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * javac -d classes Chapter9.java
 * java -classpath ./classes chapter9/Q2Itinerary
 */
public class Chapter9 {}

class Q1 {
    public static void main(String[] args) throws IOException {
        var oldPath = Paths.get("tests/practiceBook/chapter9/mouse.txt");
        var newPath = Paths.get("tests/practiceBook/chapter9/rat.txt");
        Files.move(oldPath, newPath, StandardCopyOption.REPLACE_EXISTING);
    }
}

class Q2Itinerary {
    private List<String> activities = new ArrayList<>();
    private static Q2Itinerary getItinerary(String name) {
        return null;
    }
    public static void printItinerary() throws Exception {
        Console c = System.console();
        final String name = c.readLine("What is your name?");
        final var stuff = getItinerary(name);
        stuff.activities.forEach(s -> c.printf(s));
    }
    public static void main(String[] h) throws Exception {
        printItinerary(); }}

class Q3 {
    public static void main(String[] args) {
        String fn = "tests/practiceBook/chapter9/icecream.txt";
        try (var w = new BufferedWriter(new FileWriter(fn));
             var s = System.out) {
            w.write("ALERT!");
            w.flush();
            w.write('!');
            System.out.print("1");
        } catch (IOException e) {
            System.out.print("2");
        } finally {
            System.out.print("3");
        }
    }
}

class Q4Resume {
    public void writeResume() throws Exception {
        var f1 = Path.of("tests/practiceBook/chapter9/templates/proofs");
        Files.createDirectories(f1);
        var f2 = Path.of("tests/practiceBook/chapter9/templates/test");
        Files.createDirectory(f2); // k1
        try(var w = Files.newBufferedWriter(
                Path.of(f2.toString(), "draft.txt"))) {
            w.append("My dream job");
            w.flush(); }
        Files.delete(f1);
        Files.delete(f2);
        //f1.delete(f1);
        //f2.delete(f2); // k2
        }

    public static void main(String[] args) throws Exception {
        Q4Resume resume = new Q4Resume();
        resume.writeResume();
    }
}

class Q6 {
    public static void main(String[] args) {

        Path p1 = Path.of("./found/../keys");
        Path p2 = Paths.get("/lost/blue.txt");
        System.out.println(p1.normalize().resolve(p2));
        System.out.println(p2.resolve(p1.normalize()));
    }
}

class Q8 {
    public static void main(String[] args) throws IOException {
        var p1 = Path.of("rat.txt");
        var p2 = Path.of("tests/practiceBook/chapter9/home/ho");
        var p3 = Path.of("tests/practiceBook/chapter9");
        Files.createDirectories(p2);
        Files.copy(p3.resolve(p1),p2, StandardCopyOption.REPLACE_EXISTING);
    }
}

class Q9 {
    public static void main(String[] args) throws IOException {
        var p = Path.of("tests/practiceBook/chapter9/home/ho");
        //Files.readAllLines(p)
        Files.lines(p)
                .filter(s -> s.contains("eggs"))
                .collect(Collectors.toList())
                .forEach(System.out::println);
    }
}

class Q10Vegetable implements Serializable {
    private Integer size = 1;
    private transient String name = "Red";
    { size = 3; name = "Purple"; }
    public Q10Vegetable() { this.size = 2; name = "Green"; }
    public static void main(String[] love) throws Throwable
    {
// Write data
try (var o = new ObjectOutputStream(
        new FileOutputStream("tests/practiceBook/chapter9/healthy.txt"))) {
final var v = new Q10Vegetable();
v.size = 4;
        o.writeObject(v);
}
// Read data
        try (var o = new ObjectInputStream(
        new FileInputStream("tests/practiceBook/chapter9/healthy.txt"))) {
var v = (Q10Vegetable) o.readObject();
   System.out.print(v.size + "," + v.name);
}}}

class Q13 {
    public static void main(String[] args) throws IOException, URISyntaxException {
        var path = Paths.get("tests/practiceBook/chapter9/healthy.txt");
        //var rel = Paths.get(new URI(".backup"));
        var rel = Paths.get(".backup/txt");
        //var view = Files.readAttributes(path, BasicFileAttributes.class);
        var view = Files.getFileAttributeView(path, BasicFileAttributeView.class);
        System.out.println(Files.createDirectories(rel.relativize(rel)));
        if(view.readAttributes().size()> 0 && view.readAttributes().isDirectory())
            view.setTimes(null,null,null);
        System.out.println(view.name());
        System.out.println(view.readAttributes().lastModifiedTime());
        System.out.println(Files.deleteIfExists(path));
    }
}

class Q15 {
    public int getPrize(byte[] luck) throws Exception {
        try (InputStream is = new ByteArrayInputStream(luck))
        {
            System.out.println(is.read(new byte[2]));
            if (!is.markSupported()) return -1;
            is.mark(5);
            System.out.println(is.read());
            System.out.println(is.read());
            System.out.println(is.skip(3));
            is.reset();
            return is.read();
        } }
    public static void main(String[] sticky) throws
            Exception {
        final Q15 p = new Q15();
        final var luck = new byte[] { 1, 2, 3, 4, 5, 6, 7 };
        System.out.print(p.getPrize(luck));
    }
}

class Q15Valve implements Serializable {
    private int chambers = -1;
    private transient Double size = null;
    private static String color;
    public Q15Valve() { this.chambers = 3; color = "BLUE"; }
    public static void main(String[] love) throws Throwable {
        try (var o = new ObjectOutputStream(
                new FileOutputStream("scan.txt"))) {
            final Q15Valve v = new Q15Valve();
            v.chambers = 2;
            v.size = 10.0;
            v.color = "RED";
            o.writeObject(v);
        }
        new Q15Valve();
        try (var i = new ObjectInputStream(
                new FileInputStream("scan.txt"))) {
            Q15Valve v = (Q15Valve) i.readObject();
            System.out.print(v.chambers + "," + v.size + "," + v.color);
        }
    }
    { chambers = 4; } }

class Q16 {
    public static void main(String[] args) throws IOException {
        Path p = Path.of("/", "objC", "bin");
        System.out.println(p.resolve("objC/forward/Sort.java"));
        System.out.println(p.resolve("../backwards/../forward/Sort.java"));
        System.out.println(p.resolve("../forward/./Sort.java"));
        System.out.println(p.resolve("../java/./forward/Sort.java"));
        System.out.println(p.resolve("../../java/Sort.java"));
        System.out.println(p.resolve(".././forward/Sort.java"));
    }
}

/**
 * javac -d classes Chapter9.java
 * java -classpath ./classes chapter9/Q18Rose /Users/admin/IdeaProjects/ocajp829/tests/practiceBook/chapter9
 */
class Q18Rose {
    public void tendGarden(Path p) throws Exception {
        Files.walk(p,1)
                .map(q -> {
                    try {
                        return q.toRealPath();
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                })
                .forEach(System.out::println);
    }
    public static void main(String... thorns) throws Exception
    {
        new Q18Rose().tendGarden(Paths.get(thorns[0])); }}

class Q20 {
    public static void main(String[] args) throws IOException {
        final var luck = new byte[] { 2, 3, 5, 7 };
        try (InputStream is = new ByteArrayInputStream(luck)) {
            jumpAround(is);
        }

    }
    private static void jumpAround(InputStream is) throws IOException
    {
        try (is) {
            is.skip(1);
            is.read();
            is.skip(1);
            is.mark(400);
            is.skip(1);
            is.reset();
            System.out.print(is.read());
        } }
}

class Q21 {
    public static void main(String[] args) throws IOException {
        var p1 =
                Path.of("/tea/earlgrey/..",".").resolve(Path.of("hot.txt"));
        var p2 = new
                File("/./tea/./earlgrey/./.././hot.txt").toPath();
        //System.out.print(Files.isSameFile(p1,p2));
        System.out.print(" ");
        System.out.print(p1.normalize().equals(p2.normalize()));
        System.out.print(" ");
        //System.out.print(Files.mismatch(p1,p2));
    }
}

class Q23Sneaker {
    public void setupInventory(Path d) throws Exception {
        Path suggestedPath = Paths.get("sneakers");
        if(Files.isSameFile(suggestedPath, d)           // j1
                && !Files.exists(suggestedPath))
            Files.createDirectories(d);                  // j2
    }
    public static void main(String[] socks) throws Exception
    {
        Path w = new File("/stock/sneakers").toPath();  // j3
        new Q23Sneaker().setupInventory(w); }}

class Q24 {
    public static void main(String[] args) throws IOException {
        Path p = Paths.get("tests/practiceBook/chapter9/");
        Files.walk(p)
                .map(z -> z.toAbsolutePath().toString())
                .filter(s -> s.endsWith(".java"))
                .collect(Collectors.toList()).forEach(System.out::println);
        Files.find(p, Integer.MAX_VALUE, (w,a) -> w.toAbsolutePath().toString().endsWith(".java"))
                .collect(Collectors.toList()).forEach(System.out::println);
    }
}

class Q27 {
    public static void main(String[] args) throws Exception {
        Q27 q27 = new Q27();
        q27.brew();
    }
    void brew() throws Exception {
        final var m = Path.of("tests/practiceBook/chapter9");
        Files.walk(m)
                .filter(Files::isDirectory)
                .peek(System.out::println)
                .forEach((a) -> System.out.println(Files.isDirectory(a)));
    }
}

class Q29 {
    public static void main(String[] args) throws IOException {
        Console c = System.console();
        c.readLine();
        c.readPassword();
        c.reader().read();
    }
}

class Q33 {
    public static void main(String[] args) throws IOException {
        long start = System.currentTimeMillis();
        var retriever = new BufferedReader(new
                InputStreamReader(System.in));
        try(retriever; var husky = System.err) {
            var fetch = retriever.readLine();
            System.out.printf("%s fetched in %5.1f seconds",fetch, // v1
                    (System.currentTimeMillis()-start)/1000.0);
        }
        var fetchAgain = retriever.readLine();
        System.out.println(fetchAgain + " fetched again!");
    }
}

class Q34Exterminate {
    public void deleteTree(Path q) throws IOException {
        if (!Files.isDirectory(q))
            Files.delete(q);
        else {
            //Files.list(q).forEach(this::deleteTree);
            Files.delete(q); }}}

    class Q35 {
        static void copyFile(String source, String target) throws
                Exception {
            try (var is = new FileInputStream(source);
                 OutputStream os = new FileOutputStream(target)) {
                byte[] data = new byte[123];
                int chirps;
                while ((chirps = is.read(data))> 0)
                    os.write(data, 0, chirps);
                while ((chirps = is.read())> 0)
                    os.write(chirps);
            }
        }

        public static void main(String[] args) throws Exception {
            copyFile("tests/practiceBook/chapter9/icecream.txt", "tests/practiceBook/chapter9/icecream1.txt");
        }
}

class Q37 {
    public static void main(String[] args) {
        var halleysComet =
                Path.of("stars/./rocks/../m1.meteor").subpath(1, 5).normalize();
        System.out.println(Path.of("stars/./rocks/../m1.meteor").subpath(1, 5));
        System.out.println(Path.of("stars/./rocks/../m1.meteor").subpath(1, 5).normalize());

        var lexellsComet = Paths.get("./stars/../solar/");
        System.out.println(lexellsComet.subpath(1, 3));
        System.out.println(lexellsComet.subpath(1, 3)
                .resolve("m1.meteor").normalize());

        System.out.print(halleysComet.equals(lexellsComet) ? "Same!" : "Different!");
    }
}

class Q39 {
    public static void main(String[] args) throws IOException {
        Files.move(
                Paths.get("tests/practiceBook/chapter9/icecream.txt"),
                Paths.get("tests/practiceBook/chapter9/home/tt"),
                StandardCopyOption.ATOMIC_MOVE);
    }
}

class TheCount {
        public static Stream<String> readLines(Path p) {
            try {
                return Files.lines(p);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        public static long count(Path p) throws Exception {
        return Files.list(p)
                //Files.walk(p)
                .filter(w -> Files.isRegularFile(w))
                .flatMap(s -> readLines(s))
                .count();
        }
        public static void main(String[] d) throws Exception {
            System.out.print(count(Paths.get("tests/practiceBook/chapter9/home")));
        }
}