package chapter5;

import java.util.*;

public class Chapter5 {}

class Q2 {
    public static void main(String[] args) {
        //[]String lions = new String[];
        //String[] tigers = new String[1] {"tiger"};
        String bears[] = new String[] {};
        //String ohMy [] = new String[0] {};
    }
}

class Q3Q3<Q3Q3> {}
class Q3N<Q3N> {}
class Q3Object<Object> {}

class Q4 {
    public static void main(String[] args) {
        //List<String> strings = new ArrayList<?>();
        var ints = new HashSet<Integer>();
        Double dbl = 5.0;
        ints.add(2);
        ints.add(null);
    }
}

class Q6 {
    public static void main(String[] args) {
        var q = new ArrayDeque<String>();
        q.offer("snowball");
        q.offer("minnie");
        q.offer("sugar");
        System.out.print(q.peek() + " " + q.peek() + " " +
                q.size());
        //System.out.println(q.poll());
        //System.out.println(q.pop());
    }
}

class Q8 {
    static int[][] game;
    public static void main(String[] args) {
        game[3][3] = 6;
        System.out.println(Arrays.deepToString(game));
        Object[] obj = game;
        //game[3][3] = "X";
        System.out.println(game[3][3]);
    }
}

class Q9 {
    public static void main(String[] args) {
        Comparator<String> comparator = (a, b) -> compare(a, b);
        System.out.println(comparator.compare("1", "111"));
    }
    public static int compare(String s1, String s2) {
        return s2.length() - s1.length();
    }
}

class Q14 {
    public static void main(String[] args) {
        var list = new ArrayList<Integer>();
        list.add(56);
        list.add(56);
        list.add(3);
        var set = new TreeSet<Integer>(list);
        System.out.print(set.size());
        System.out.print(" ");
        System.out.print(set.iterator().next());
    }
}

class Q15Copier {
public static void main(String... original) {
    String[] copy = original;
    //Arrays.linearSort(original);
    //Arrays.search(original, "");
    System.out.println(/*original.size() + */ " " + original[0]);
}}

class Q16 {
    public static void main(String[] args) {
        var chars = new HashSet<Character>();
        //var chars = new ArrayList<Character>();
        chars.add('a');
        chars.add(Character.valueOf('b'));
        //chars.set(1, 'c');
        chars.remove(0);
        System.out.print(chars.size() + " " + chars.contains('b'));
    }
}

record Q17Magazine(String name) implements Comparable<Q17Magazine>{
    public int compareTo(Q17Magazine m) {
        return name.compareTo(m.name);
    } }
 class Q17Newsstand {
    public static void main(String[] args) {
        var set = new TreeSet<Q17Magazine>();
        set.add(new Q17Magazine("highlights"));
        set.add(new Q17Magazine("Newsweek"));
        set.add(new Q17Magazine("highlights"));
        System.out.println(set);
        System.out.println(set.iterator().next()); }}

class Q20 {
    public static void main(String[] args) {
        var nums = new HashSet<Long>();
        nums.add((long) Math.min(5, 3));
        nums.add(Math.round(3.14));
        nums.add((long) Math.pow(4,2));
        System.out.println(nums);
    }
}

class Q21 {
    public static void main(String[] args) {
        var x = new ArrayDeque<Integer>();
        x.offer(18);
        x.offer(5); //to the end
        System.out.println(x);
        x.push(13); //to the beginning
        System.out.println(x);
        System.out.println(x.pop() + " " + x.poll());
        System.out.println(x.poll());
        System.out.println(x.poll());
    }
}

class Q24 {
    public static void throwOne(Collection<? extends RuntimeException> coll) {
        var iter = coll.iterator();
        if (iter.hasNext())
            throw iter.next();
    }

    public static void main(String[] args) {
        throwOne((Collection) List.of(NullPointerException.class));
    }
}

class Q25 {
    public static void main(String[] args) {
        int[][] nums1 = new int[2][1];
        int[] nums2[] = new int[2][1];
        int[] nums3[] = new int[][] { { 0 }, { 0 } };
        int[] nums4[] = new int[][] { { 0, 0 } };
        System.out.println(Arrays.deepToString(nums1));
        System.out.println(Arrays.deepToString(nums2));
        System.out.println(Arrays.deepToString(nums3));
        System.out.println(Arrays.deepToString(nums4));
    }
}

//
interface Q27Comic<S> {
    void draw(S s);
}
class Q27ComicClass<S> implements Q27Comic<S> {
    public void draw(S s) {
        System.out.println(s);
    }
}
class Q27SnoopyClass implements Q27Comic<Q27Snoopy> {
    public void draw(Q27Snoopy s) {
        System.out.println(s);
    }
}
class Q27SnoopyComic implements Q27Comic<Q27Snoopy> {
    //public void draw(S s) {
      //  System.out.println(s);
    //}

    @Override
    public void draw(Q27Snoopy s) {
        System.out.println(s);
    }
}
class Q27Snoopy {
    public static void main(String[] args) {
        Q27Comic<Q27Snoopy> s1 = s -> System.out.println(s);
        s1.draw(new Q27Snoopy());
        Q27Comic<Q27Snoopy> s2 = new Q27ComicClass<>();
        s2.draw(new Q27Snoopy());
        Q27Comic<Q27Snoopy> s3 = new Q27SnoopyClass();
        s3.draw(new Q27Snoopy());
        Q27Comic<Q27Snoopy> s4 = new Q27SnoopyComic();
        s4.draw(new Q27Snoopy());
    }}
//

class Q30 {
    public static void main(String[] args) {

        String[] array = {"Natural History", "Science"};
        var museums = Arrays.asList(array);
        museums.set(0, "Art");
        System.out.println(museums.contains("Art"));
    }
}

class Q31Wash<T> {
    T item;
    public void clean(T item) {
        System.out.println("Clean " + item);
    }
}

class Q31LaundryTime {
    public static void main(String[] args) {
        var wash = new Q31Wash<String>();
        wash = new Q31Wash<>();
        Q31Wash wash2 = new Q31Wash();
        wash2 = new Q31Wash<String>();
        Q31Wash<String> wash3 = new Q31Wash<>();

        wash.clean("socks");
    }
}

class Q33 {
    public static void main(String[] args) {
        var list = Arrays.asList("alpha", "beta", "gamma");
        Collections.sort(list, (s, t) -> t.compareTo(s));
        System.out.println(list.get(0));
        Collections.sort(list, Comparator.reverseOrder());
        System.out.println(list.get(0));
        Collections.sort(list, Comparator.comparing((String s) -> s.charAt(0)).reversed());
        System.out.println(list.get(0));
    }
}

class Q34 {
    public static void main(String[] args) {
        //float[] lion = new float[];
        float[] tiger = new float[1];
        //float[] bear = new[] float;
        //float[] ohMy = new[1] float;
    }
}

class Q35 {
    public static void main(String[] args) {
        var matrix = new String[1][2];
        matrix[0][0] = "Don't think you are, know you are."; // m1
        matrix[0][1] = "I'm trying to free your mind Neo"; // m2
        matrix[1][0] = "Is all around you "; // m3
        matrix[1][1] = "Why oh why didn't I take the BLUE pill?"; //m4
    }
}

class Q37 {
    public static void main(String[] args) {
        var os = new String[] { "Mac", "Linux", "Windows" };
        Arrays.sort(os);
        System.out.println(Arrays.binarySearch(os, "RedHat"));
        System.out.println(Arrays.binarySearch(os, "Mac"));
    }
}

class Q38 {
    public static void main(String[] args) {
        var names = new HashMap<String, String>();
        names.put("peter", "pan");
        names.put("wendy", "darling");
        var first = names.entrySet();        // line x1
        //System.out.println(first.getKey());  // line x2
    }
}

class Q39 {
    public static void main(String[] args) {
        var q = new ArrayDeque<String>();
        q.offerFirst("snowball");
        q.offer("sugar");
        q.offerLast("minnie");
        System.out.println(q.poll());
        System.out.println(q.removeFirst());
        System.out.println(q.size());
    }
}

class Q41 {
    public static void main(String[] args) {
        var set = new HashSet<>();
        var set2 = new HashSet<Object>();
        //HashSet<> set3 = new HashSet<Object>();
        HashSet<Object> set4 = new HashSet<>();
        HashSet<Object> set5 = new HashSet<Object>();
    }
}

class Q42 {
    public static void main(String[] args) {
        var ints = new int[] {3,1,4};
        var others = new int[] {3,7,1,8};
        System.out.println(Arrays.compare(ints, others));
        System.out.println(Arrays.compare(others, ints));
        System.out.println(Arrays.compare(others, others));
        System.out.println(Arrays.equals(ints, others));
    }
}

class Q43 {
    public static void main(String[] args) {
        var list = List.of("alpha", "beta", "gamma");
        Collections.sort(list, Comparator.comparing(String::length));
        System.out.println(list.get(0));
    }
}

class Q46 {
    public static void main(String[] args) {
        Comparator<Integer> c = (x, y) -> y - x;
        var ints = Arrays.asList(3, 1, 4);
        Collections.sort(ints, c);
        System.out.println(Collections.binarySearch(ints, 1));
    }
}

class Q49 {
    public static void main(String[] args) {
        args = null;
        Arrays.sort(args);
        System.out.println(Arrays.toString(args));
        System.out.println(args[0]);
    }
}

class Q50Mammal {}
class Q50Bat extends Q50Mammal {}
class Q50Cat extends Q50Mammal {}
class Q50Sat {}
//class Q50Fur<? extends Q50Mammal> {
  //  void clean() { // line R
    //    var bat = new Q50Fur<Q50Bat>();  // line S
      //  var cat = new Q50Fur<Q50Cat>();  // line T
        //var sat = new Q50Fur<Q50Sat>();  // line U
    //}}

class Q51 {
    static int[][] game = new int[6][6];
    public static void main(String[] args) {
        game[3][3] = 6;
        System.out.println(game[3][3]);
        Object[] obj = game;
        System.out.println(obj[3]);
        System.out.println(Arrays.deepToString(obj));
        obj[3] = "X"; //int[] != String
        System.out.println(game[3][3]);
    }
}

class Q55 {
    public static void main(String[] args) {
        List<Character> chars = new ArrayList<>();
        chars.add('a');
        chars.add('b');
        chars.clear();
        chars.remove(0);
        //System.out.print(chars.isEmpty() + " " + chars.length());
        System.out.print(chars.isEmpty() + " " + chars.size());
    }
}

class Q56 {
    private static <S, A, B, T extends Collection<U> , U> U add(T list, U element) {
        list.add(element);
        return element;
    }
    public static void main(String[] args) {
        var values = new ArrayList<String>();
        add(values, "duck");
        add(values, "duck");
        add(values, "goose");
        System.out.println(values); }
}

class Q57 {
    public static void main(String[] args) {
        String[] os = new String[] { "Mac", "Linux", "Windows" };
        System.out.println(Arrays.binarySearch(os, "Linux"));
        System.out.println(Collections.binarySearch(List.of(os), "Linux"));
        Arrays.sort(os);
        System.out.println(Arrays.binarySearch(os, "Linux"));
        System.out.println(Collections.binarySearch(List.of(os), "Linux"));
    }
}

class Q62 {
    public static void main(String[] args) {

    }
    private void output(Collection<?> x){
        x.forEach(System.out::println);
    }
    private void output(LinkedList<?> x){
        x.forEach(System.out::println);
    }
    private void output(TreeMap<?,?> x){
        x.keySet().forEach(System.out::println);
        x.values().forEach(System.out::println);
        System.out.println(x.entrySet());
    }
}

class Q63 {
    public static void getExceptions(Collection<? super Exception> coll) {
        coll.add(new RuntimeException());
        coll.add(new Exception());
    }
    public static void main(String[] args) {
        getExceptions(new ArrayList<>(Arrays.asList(IllegalArgumentException.class)));
    }
}

class Q75 {
    public static void main(String[] args) {
        sortAndSearch("seed", "flower");
    }
    private static void sortAndSearch(String... args) {
        var one = args[1];
        Comparator<String> comp = (x, y) -> -y.compareTo(x);
        Arrays.sort(args, comp);
        var result = Arrays.binarySearch(args, one, comp);
        System.out.println(result);
    }
}

class Q78 {
    public static void main(String[] args) {
        var names = new HashMap<String, String>();
        names.put("peter", "pan");
        names.put("wendy", "darling");

        //String w = names.getOrDefault("peter");
        String x = names.getOrDefault("peter", "x");
        String y = names.getOrDefault("john", "x");
        System.out.println(x);
        System.out.println(y);
    }
}

class Q79 {
    public static void main(String[] args) {
        List<String> list = List.of("Mary", "had", "a", "little", "lamb");
        Set<String> set = new HashSet<>(list);
        set.addAll(list);
        for(String sheep : set)
            if (sheep.length()> 1)
               set.remove(sheep);
        System.out.println(set);
    }
}

class Q80 {
    public static void getExceptions(Collection<? super Exception> coll) {
        coll.add(new RuntimeException());
        coll.add(new Exception());
    }
}