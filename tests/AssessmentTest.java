import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.util.*;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

class AssessmentTest {
}

class Q1 {
    public static void main(String[] args) {
        final int score1 = 8, score2 = 3;
        char myScore = 7;
        var goal = switch (myScore) {
            //default -> {if(10>score1) yield "unknown";} //else missed
            default -> {
                if(10>score1) yield "unknown";
                else yield "else";
            }
            case score1 -> "great";
            case 2, 4, 6 -> "good";
            //case score2, 0 -> {"bad";} //yeald missed
            case score2, 0 -> {yield "bad";}
        };
        System.out.println(goal);
    }
}

class Q3 {
    static volatile List<Integer> dataVolatile = new ArrayList<>();

    public static void main(String[] args) {
        test1(); //parallel
        test2(); //volatile
        test3(); //forEach
        test4(); //forEachOrdered
        test5(); //synchronized
    }

    private static void test1() {
        List<Integer> data = new ArrayList<>();
        IntStream.range(0,100).parallel().forEach(s -> data.add(s));
        System.out.println(data.size()); // 99, 100, 97
    }

    private static void test2() {
        IntStream.range(0,100).parallel().forEach(s -> dataVolatile.add(s));
        System.out.println(dataVolatile.size()); // 99, 100, 97
    }

    private static void test3() {
        List<Integer> data = new ArrayList<>();
        IntStream.range(0,100).forEach(s -> data.add(s));
        System.out.println(data.size()); // 100
    }

    private static void test4() {
        List<Integer> data = new ArrayList<>();
        IntStream.range(0,100).parallel().forEachOrdered(s -> data.add(s));
        System.out.println(data.size()); // 100
    }

    private static void test5() {
        List<Integer> data = new ArrayList<>();
        synchronized (data) {
            IntStream.range(0,100).parallel().forEachOrdered(s -> data.add(s));
        }
        System.out.println(data.size()); // 100
    }
}

class Q4 {
    public static void main(String[] args) {
        Predicate<String> empty = String::isEmpty;
        Predicate<String> notEmpty = empty.negate();
        var result = Stream.generate(() -> "")
                .limit(100)
                .filter(notEmpty)
                .collect(Collectors.groupingBy(k -> k))
                .entrySet()
                .stream()
                .map(Map.Entry::getValue)
                .flatMap(Collection::stream)
                .collect(Collectors.partitioningBy(notEmpty));
        System.out.println(result);
    }
}

class Q7 {
    public static void main(String[] args) {
        int[] array = {6,9,8};
        System.out.println("B" + Arrays.binarySearch(array,9));
        System.out.println("C" + Arrays.compare(array, new int[] {6, 9, 8}));
        System.out.println("M" + Arrays.mismatch(array, new int[] {6, 9, 8}));
    }
}

class Q8 {
    public static void main(String[] args) {
        Predicate<Integer> r = (i) -> Integer.getInteger("10", 5) > i;
        Predicate<Integer> x = r.negate();
        Consumer<Integer> y = (j) -> System.out.println("Consumer " + j);
        Runnable y1 = () -> System.out.println("Run");
        Comparator<Integer> z = (a, b) -> a - b;

        System.out.println(r.test(5));
        System.out.println(x.test(5));
        y.accept(5);
        y1.run();
        System.out.println(z.compare(10, 11));
    }
}

class Q10 {
    public static interface HasTail {
        //private int getTailLength(); //PRIVATE without body ERROR
    }
    abstract static class Puma implements HasTail {
        String getTailLength() { return "4"; }
    }
    public static class Cougar implements HasTail {
        public static void main(String[] args) {
            var puma = new Puma() {};
            System.out.println(puma.getTailLength()); //4
        }
        public int getTailLength(int length) { return 2; }
    }
}

class Q12 {
    public static void main(String[] args) throws SQLException {
       Connection a = DriverManager.getConnection( "url", "userName", "password");
       PreparedStatement b = a.prepareStatement("");
       ResultSet c = b.executeQuery();
        if (c.next()) System.out.println(c.getString(1));
    }
}

class Q14 implements Serializable {
    private String name;
    private transient Integer age;
    // Getters/setters omitted
    public static void main(String[] args) {
        try(var os = new ObjectOutputStream(
                new BufferedOutputStream(
                new FileOutputStream("tests/birds.dat")))) {
            Q14 q14 = new Q14();
            q14.age = 1;
            q14.name = "Name";
            os.writeObject(q14);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        try(var is = new ObjectInputStream(
                    new BufferedInputStream(
                    new FileInputStream("tests/birds.dat")))) {
            Q14 b = (Q14) is.readObject();
            System.out.println(b.age);
            System.out.println(b.name);
    } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}

/**
 * javac ../AssessmentTest.java -d ../classes
 * java -classpath "../classes:sqlite-jdbc-3.46.0.0.jar:slf4j-api-2.0.0.jar" Q16
 */
//TODO:
class Q16 {
    public static void main(String[] args) {

        var sql = "INSERT INTO people VALUES(?, ?, ?)";
        //try (Connection conn = DriverManager.getConnection( "jdbc:sqlite:/Users/admin/Library/DBeaverData/workspace6/.metadata/sample-database-sqlite-1/Chinook.db");
        try (Connection conn = DriverManager.getConnection( "jdbc:postgresql://localhost:5432/testdb", "sa", "sa");
             var ps = conn.prepareStatement(sql,
                     ResultSet.TYPE_SCROLL_SENSITIVE,
                     ResultSet.CONCUR_UPDATABLE
             )) {
            conn.setAutoCommit(false);

            ps.setInt(1, 1);
            ps.setString(2, "Joslyn");
            ps.setString(3, "NY");
            ps.executeUpdate();
            //conn.commit();

            Savepoint sp = conn.setSavepoint();
            ps.setInt(1, 2);
            ps.setString(2, "Kara");
            ps.executeUpdate();
            conn.rollback(sp);
            System.out.println("OK");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}

class Q17 {
    public static void main(String[] args) {
        try {
            System.out.println(Files.mismatch(Path.of("birds.dat"), Paths.get("birds.dat")));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

//final class Q18_1 {}
class Q18_Amphibian {}
abstract class Q18_Tadpole extends Q18_Amphibian {}
class Q18 {
    public static void main(String... args) {
        var tadpoles = new ArrayList<Q18_Tadpole>();
        for (var amphibian : tadpoles) {
            Q18_Tadpole tadpole1 = amphibian;
            Q18_Amphibian tadpole2 = amphibian;
            Object tadpole3 = amphibian;
        }
    }
}

class FeedingSchedule {
    public static void main(String[] args) {
        var x = 5;
        var j = 0;
        var ii = 0;
    OUTER: for (var i = 0; i < 3;) INNER:
            do {
                i++;
                x++;
                ii = i;
                if (x> 10) break INNER;
                x += 4;
                j++;
            } while (j <= 2);
            System.out.println(x);
            System.out.println(j);
            System.out.println(ii);
    }
}

class Q20 {
    public static void main(String[] args) {
        var pooh = """
            "Oh, bother." -Pooh """.indent(1);
        System.out.print(pooh);

        pooh = "\n\"Oh, bother.\" -Pooh\n";
        System.out.println(pooh);

        pooh = "\n \"Oh, bother.\" -Pooh\n";
        System.out.println(pooh);

        pooh = " \"Oh, bother.\" -Pooh\n";
        System.out.println(pooh);

        pooh = "\n\"Oh, bother.\" -Pooh";
        System.out.println(pooh);

        pooh = "\n \"Oh, bother.\" -Pooh";
        System.out.println(pooh);

        pooh = " \"Oh, bother.\" -Pooh";
        System.out.println(pooh);
    }
}

class Q22 {
    public static void main(String[] args) {
        var treeMap = new TreeMap<Character, Integer>();
        System.out.println(treeMap.put('k', 10)); //null
        System.out.println(treeMap.put('k', 2)); //10
        System.out.println(treeMap.put('m', 3)); //null
        System.out.println(treeMap.put('M', 4)); //null
        treeMap.replaceAll((k, v) -> v + v);
        System.out.println(treeMap);
        treeMap.keySet()
                .forEach(k -> System.out.print(treeMap.get(k)));
    }
}

class Q23 {
    public static void main(String[] args) {
        //System.out.println(test(i::equals(5)));
        //System.out.println(test(i -> {i == 5;}));
        //System.out.println(test((int i) -> i == 5)); //Integer needed
        //System.out.println(test((int i) -> {return i == 5;})); //Integer needed
        System.out.println(test((i) -> i == 5));
        System.out.println(test((i) -> {return i == 5;}));
    }
    private static boolean test(Function<Integer, Boolean> b) {
        return b.apply(5);
    }
}

class Q24 {
    public static void main(String[] args) {
        var s1 = "Java";
        var s2 = "Java";
        var s3 = s1.indent(1).strip();
        var s4 = s3.intern();
        var sb1 = new StringBuilder();
        sb1.append("Ja").append("va");
        System.out.println(s1 == s2); //true
        System.out.println(s1.equals(s2)); //true
        System.out.println(s1 == s3); //false
        System.out.println(s1 == s4); //true
        System.out.println(sb1.toString() == s1); //false
        System.out.println(sb1.toString().equals(s1)); //true
    }
}

class Q26 {
    private static void magic(Stream<Integer> s) {
        Optional o = s
                .filter(x -> x < 5)
                //.mapToInt(a -> a).max()
                .limit(3)
                .max((x, y) -> x-y);
        System.out.println(o.get());
    }
    public static void main(String[] args) {
        //magic(Stream.empty()); //ERROR on get
        magic(Stream.iterate(1, x -> x++)); //1
        magic(Stream.iterate(1, x -> ++x)); //3
        //magic(Stream.of(5, 10)); //ERROR on get
    }
}

class Q27 {
    record Music() {
        //final int score = 10;
        static final int score = 10;
    }
    record Song(String lyrics) {
        Song {
            //this.lyrics = lyrics + "Hello World";
            lyrics = lyrics + "Hello World";
        } }
    sealed class Dance {}
    record March() {
        //@Override String toString() { return null; } }
        @Override
        public String toString() { return null; } }
    //class Ballet extends Dance {}
    final class Ballet extends Dance {}

    public static void main(String[] args) {
        System.out.println(new Music());
        System.out.println(new Song(""));
        System.out.println(new Q27().new Dance());
        System.out.println(new March());
        System.out.println(new Q27().new Ballet());
    }
}

class Q28 {
    public static void main(String[] args) {
        //int monday = 3 + 2.0;
        double tuesday = 5_6L;
        //boolean wednesday = 1 > 2 ? !true;
        short thursday = (short)Integer.MAX_VALUE;
        //long friday = 8.0L;
        //var saturday = 2_.0;
        System.out.println(tuesday);
        System.out.println(thursday);
        System.out.println(Integer.MAX_VALUE%Short.MAX_VALUE);
    }
}

class Q29 {
    public static void main(String[] args) {
        //final var cb = new CyclicBarrier(3, () -> System.out.println("Clean!")); // u1 3 барьера на 1 поток newSingleThreadExecutor
        final var cb = new CyclicBarrier(1, () -> System.out.println("Clean!")); // u1
        ExecutorService service = Executors.newSingleThreadExecutor();
        try {
            IntStream.iterate(1, i -> ++i)
            //IntStream.generate(() -> 1)
                    .limit(12)
                    .parallel()
                    .peek((a) -> System.out.println(a))
                    .forEach(i -> service.submit(() -> cb.await())); // u2
        } finally {
            service.shutdown();
        }
    }
}