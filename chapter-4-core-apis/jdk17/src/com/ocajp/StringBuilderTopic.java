package com.ocajp;

import java.util.Locale;

public class StringBuilderTopic {

  public static void main(String[] args) {
    System.out.println("Hello me".startsWith("m", 6));
    System.out.println("Hello me".indexOf("m", 6));

    internPool();

    var x = "Hello World";
    var y = "Hello World".trim(); //строка уже есть в пулле
    var z = " Hello World".trim(); //на этапе компиляции строки нет в пулле, в рунтайм она становится такой же как в
    // пулле
    System.out.println(x == y); // true
    System.out.println(x == z); // false

    var singleString = "hello world";
    var concat = "hello ";
    concat += "world"; //Calling += is just like calling a method and results in a new String
    System.out.println(singleString == concat); // false

    muchСonsumedUnefficientMethod();
    efficientMethod();

    System.out.println("---");
    var sb3 = new StringBuilder("abcdef");
    sb3.delete(1, 3); // sb = adef
    sb3.deleteCharAt(5); // exception

    System.out.println("---");
    /**
     *
     */
    var sb2 = new StringBuilder("animals");
    sb2.insert(7, "-"); // sb = animals
    sb2.insert(0, "-"); // sb = -animals
    sb2.insert(4, "-"); // sb = -ani-mals
    System.out.println(sb2);

    System.out.println("---");
    var stringBuilder = new StringBuilder().append(1).append('c');
    stringBuilder.append("-").append(true);
    System.out.println(stringBuilder); // 1c-true

    System.out.println("---");
    var sb = new StringBuilder("animals");
    /**
     * substring() returns a String rather than a StringBuilder.
     * That is why sb is not changed
     */
    String sub = sb.substring(sb.indexOf("a"), sb.indexOf("al"));
    int len = sb.length();
    char ch = sb.charAt(6);
    System.out.println(sub + " " + len + " " + ch);

    System.out.println("---");

    System.out.println("1aa".indexOf('a'));
    StringBuilder a = new StringBuilder("abc");
    StringBuilder b = a.append("de");
    b = b.append("f").append("g");
    System.out.println("a=" + a);
    System.out.println("b=" + b);
  }

  /**
   * Каждую итерацию создает новый объект new String.
   *
   * The empty String on line 10 is instantiated, and then line 12 appends an "a". However,
   * because the String object is immutable, a new String object is assigned to alpha, and the
   * "" object becomes eligible for garbage collection. The next time through the loop, alpha is
   * assigned a new String object, "ab", and the "a" object becomes eligible for garbage collection. The next
   * iteration assigns alpha to "abc", and the "ab" object becomes eligible for
   * garbage collection, and so on.
   * This sequence of events continues, and after 26 iterations through the loop, a total of 27
   * objects are instantiated, most of which are immediately eligible for garbage collection.
   */
  public static void muchСonsumedUnefficientMethod() {
    String alpha = "";
    for (char current = 'a'; current <= 'z'; current++) {
      alpha += current;
    }
    System.out.println(alpha);
  }

  /**
   * Не создает new String каждую итерацию.
   *
   * The call to append() on line
   * 17 adds a character to the StringBuilder object each time through the for loop, appending the value of current
   * to the end of alpha. This code reuses the same StringBuilder
   * without creating an interim String each time.
   */
  public static void efficientMethod() {
    StringBuilder alpha = new StringBuilder();
    for (char current = 'a'; current <= 'z'; current++) {
      alpha.append(current);
    }
    System.out.println(alpha);
  }

  /**
   * Указать Java использовать пул строк. Метод intern() будет использовать объект в пуле строк, если таковой
   * присутствует
   * If the literal is not yet in the string pool, Java will add it at this time.
   */
  public static void internPool () {
    var name = "Hello World";
    var name2 = new String("Hello World").intern();
    System.out.println(name == name2); // true


    var first = "rat" + 1;
    var second = "r" + "a" + "t" + "1";
    var third = "r" + "a" + "t" + new String("1"); //no compile-time constant, does not point to a reference in the
    // string pool.
    System.out.println(first == second); //true
    System.out.println(first == second.intern()); //true
    System.out.println(first == third); //false

    /**
     * the intern() call looks in the string pool. Java notices that first points to the
     * same String and prints true
     */
    System.out.println(first == third.intern()); //true
    System.out.println("---");
  }
}

class SbRepeat {
  public static void main(String[] args) {
    String str = new String("0123456789r");
    System.out.println(str.indexOf("r", 10));
    System.out.println(str.startsWith("r", 10));
    StringBuilder sb1 = new StringBuilder(str);
    System.out.println(sb1.append("___", 0, 1));
    System.out.println(sb1.insert(1, "-_~", 0, 3));

    StringBuilder sb2 = new StringBuilder(str);
    System.out.println(sb1.equals(sb2));
    System.out.println(sb1.compareTo(sb2) == 0);
  }
}