package com.ocajp;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

/**
 * Greenwich Mean Time is a time zone in Europe that is used as time zone zero when discussing offset
 * UTC uses the same time zone zero as GMT
 *
 * February 29 exists only in a leap year. Leap years are years that are a multiple of 4 or 400, but not other
 * multiples of 100. For example, 2000 and
 * 2016 are leap years, but 2100 is not
 */
public class DateTimeTopic {
  public static void main(String[] args) {
    var time1 = LocalTime.of(6, 15); // hour and minute
    System.out.println(time1);
    var time2 = LocalTime.of(6, 15, 30); // + seconds
    System.out.println(time2);
    var time3 = LocalTime.of(6, 15, 30, 200); // + nanoseconds
    System.out.println(time3);

    System.out.println(LocalDate.now());//2024-02-03
    System.out.println(LocalTime.now());//23:57:57.712875700
    System.out.println(LocalDateTime.now()); //2024-02-03T23:57:57.712875700
    /**
     * 2024-02-03T23:57:57.714876300+03:00[Europe/Moscow]
     * 2021–10–25T09:13:07.769–05:00[America/New_York]
     */
    System.out.println(ZonedDateTime.now());
  }
}

/**
 * public static ZonedDateTime of(int year, int month,
 *  int dayOfMonth, int hour, int minute, int second,
 *  int nanos, ZoneId zone)
 * public static ZonedDateTime of(LocalDate date, LocalTime time,
 *  ZoneId zone)
 * public static ZonedDateTime of(LocalDateTime dateTime, ZoneId zone)
 */
class ZonedDateTimeTopic {
  public static void main(String[] args) {
    var date1 = LocalDate.of(2022, Month.JANUARY, 20);
    var time1 = LocalTime.of(6, 15); // hour and minute
    var dateTime1 = LocalDateTime.of(2022, Month.JANUARY, 20, 6, 15, 30);

    //System.out.println(date1.until(dateTime1));
    System.out.println(date1.withMonth(4)); //2022-04-20
    var zone = ZoneId.of("US/Eastern");
    System.out.println(zone);
    var zoned1 = ZonedDateTime.of(2022, 1, 20,
        6, 15, 30, 200, zone);
    System.out.println(zoned1);
    var zoned2 = ZonedDateTime.of(date1, time1, zone);
    System.out.println(zoned2);
    var zoned3 = ZonedDateTime.of(dateTime1, zone);
    System.out.println(zoned3);
  }
}

class DateErrors {
  public static void main(String[] args) {
    //var d = new LocalDate(); // DOES NOT COMPILE
    //var d = LocalDate.of(2022, Month.JANUARY, 32) // DateTimeException
    var date = LocalDate.of(2024, Month.JANUARY, 20);
    //date = date.plusMinutes(1); // DOES NOT COMPILE
  }
}

class LeapYear {
  public static void main(String[] args) {
    var date1 = LocalDate.of(2022, Month.JANUARY, 20);
    System.out.println(date1); // 2022–01–20
    date1 = date1.plusDays(2);
    System.out.println(date1); // 2022–01–22
    date1 = date1.plusWeeks(1);
    System.out.println(date1); // 2022–01–29
    date1 = date1.plusMonths(1);
    System.out.println(date1); // 2022–02–28 //month значит на 1 месяц вперед, февраль 2022 не высокосный, поэтому 28
    date1 = date1.plusYears(5);
    System.out.println(date1); // 2027–02–28


    var date = LocalDate.of(2024, Month.JANUARY, 20);
    var time = LocalTime.of(5, 15);
    var dateTime = LocalDateTime.of(date, time);
    System.out.println(dateTime); // 2024–01–20T05:15
    dateTime = dateTime.minusDays(1);
    System.out.println(dateTime); // 2024–01–19T05:15
    dateTime = dateTime.minusHours(10);
    System.out.println(dateTime); // 2024–01–18T19:15
    dateTime = dateTime.minusSeconds(30);
    System.out.println(dateTime); // 2024–01–18T19:14:30


    var dateTimeChained = LocalDateTime.of(date, time)
        .minusDays(1).minusHours(10).minusSeconds(30);
    System.out.println(dateTimeChained);
  }
}

/**
 * Period принимает int
 * You cannot chain methods when creating a Period.
 * Only the last method is used because the Period.of methods are static methods.
 * Besides Period.plus chains
 */
class PeriodTopic {
  public static void main(String[] args) {
    var annually = Period.ofYears(1); // every 1 year
    var quarterly = Period.ofMonths(3); // every 3 months
    var everyThreeWeeks = Period.ofWeeks(3); // every 3 weeks
    var everyOtherDay = Period.ofDays(2); // every 2 days
    var everyYearAndAWeek = Period.of(1, 0, 7); // every year and 7 days
    var everyYearAndAWeek2 = Period.of(1, 1,  7);
    System.out.println(everyYearAndAWeek.plus(everyYearAndAWeek).plus(everyYearAndAWeek2));

    var wrong = Period.ofYears(1).ofWeeks(1); // every week //Static member 'java.time.Period.ofWeeks(int)' accessed via instance reference
    System.out.println(wrong); //P7D

    var wrong2 = Period.ofYears(1);
    wrong2 = Period.ofWeeks(1);
    System.out.println(wrong2); //P7D

    var date = LocalDate.of(2022, 1, 30);
    var time = LocalTime.of(6, 15);
    var dateTime = LocalDateTime.of(date, time);
    var period = Period.ofMonths(1);
    System.out.println(date.plus(period)); // 2022-02-28
    System.out.println(dateTime.plus(period)); // 2022-02-28T06:15
    //System.out.println(time.plus(period)); // UnsupportedTemporalTypeException: Unsupported unit: Months
  }
}

/**
 * Duration принимает long
 */
class DurationTopic {
  public static void main(String[] args) {
    var daily = Duration.ofDays(1); // PT24H
    var hourly = Duration.ofHours(1); // PT1H
    System.out.println(hourly);
    var everyMinute = Duration.ofMinutes(1); // PT1M
    var everyTenSeconds = Duration.ofSeconds(10); // PT10S
    var everyMilli = Duration.ofMillis(1); // PT0.001S
    var everyNano = Duration.ofNanos(1); // PT0.000000001S

    daily = Duration.of(1, ChronoUnit.DAYS);
    hourly = Duration.of(1, ChronoUnit.HOURS);
    everyMinute = Duration.of(1, ChronoUnit.MINUTES);
    everyTenSeconds = Duration.of(10, ChronoUnit.SECONDS);
    everyMilli = Duration.of(1, ChronoUnit.MILLIS);
    everyNano = Duration.of(1, ChronoUnit.NANOS);
    var halfDays = Duration.of(3, ChronoUnit.HALF_DAYS);
    System.out.println(halfDays); //PT36H

    var date = LocalDate.of(2022, 1, 20);
    var time = LocalTime.of(6, 15);
    var dateTime = LocalDateTime.of(date, time);
    var duration = Duration.ofHours(6);
    System.out.println(dateTime.plus(duration)); // 2022–01–20T12:15
    System.out.println(time.plus(duration)); // 12:15
    //System.out.println(date.plus(duration)); // UnsupportedTemporalTypeException: Unsupported unit: Seconds

    date = LocalDate.of(2022, 1, 20);
    time = LocalTime.of(1, 15);
    dateTime = LocalDateTime.of(date, time);
    duration = Duration.ofHours(23);
    System.out.println(dateTime.plus(duration)); // 2022–01–21T00:15
    System.out.println(time.plus(duration)); // 00:15
    //System.out.println(date.plus(duration)); // UnsupportedTemporalTypeException: Unsupported unit: Seconds

    /**
     * Since we are working with a LocalDate, we are required to use Period. Duration
     * has time units in it
     */
    date = LocalDate.of(2022, 5, 25);
    var period = Period.ofDays(1);
    var durationOfDays = Duration.ofDays(1);
    var durationOfZeroDays = Duration.ofDays(0);
    System.out.println(date.plus(period)); // 2022–05–26
    System.out.println(date.plus(durationOfZeroDays)); // 2022-05-25
    System.out.println(durationOfZeroDays); //PT0S
    System.out.println(durationOfDays); //PT24H
    System.out.println(date.plus(durationOfDays)); // Unsupported unit: Seconds
  }
}

class ChronoUnitTopic {
  public static void main(String[] args) {
    var one = LocalTime.of(5, 15);
    var two = LocalTime.of(6, 30);
    var date = LocalDate.of(2016, 1, 20);
    var dateTime = LocalDateTime.of(date, one);
    System.out.println(ChronoUnit.HOURS.between(one, two)); // 1
    System.out.println(ChronoUnit.MINUTES.between(one, two)); // 75
    System.out.println(ChronoUnit.SECONDS.between(one, two)); // 4500
    System.out.println(ChronoUnit.NANOS.between(one, two)); // 4500000000000
    //System.out.println(ChronoUnit.MINUTES.between(date, date)); // UnsupportedTemporalTypeException: Unsupported
    // unit: Minutes
    //System.out.println(ChronoUnit.MINUTES.between(one, date)); // DateTimeException: Unable to obtain LocalTime from
    // TemporalAccessor: 2016-01-20 of type java.time.LocalDate
    System.out.println(ChronoUnit.DAYS.between(dateTime.plusDays(5), dateTime)); // -5
    System.out.println(ChronoUnit.DAYS.between(dateTime, dateTime.plusDays(5))); // 5


    LocalTime time = LocalTime.of(3,12,45);
    System.out.println(time); // 03:12:45
    LocalTime truncated = time.truncatedTo(ChronoUnit.MINUTES);
    System.out.println(truncated); // 03:12
  }
}

/**
 * The Instant class represents a specific moment in time in the GMT time zone
 */
class InstantTopic {
  public static void main(String[] args) throws InterruptedException {
    var now = Instant.now();
    Thread.sleep(100);
    // do something time consuming
    var later = Instant.now();
    var duration = Duration.between(now, later);
    System.out.println(now);
    System.out.println(later);
    System.out.println(duration.toMillis()); // Returns number milliseconds


    var date = LocalDate.of(2022, 5, 25);
    var time = LocalTime.of(11, 55, 00);
    var dt = LocalDateTime.of(date, time);
    var zone = ZoneId.of("US/Eastern");
    System.out.println(time); //11:55
    System.out.println(dt); //2022-05-25T11:55
    var zonedDateTime = ZonedDateTime.of(date, time, zone);
    var instant = zonedDateTime.toInstant(); // 2022–05–25T15:55:00Z
    System.out.println(zonedDateTime); // 2022–05–25T11:55–04:00[US/Eastern]
    System.out.println(instant); // 2022-05-25T15:55:00Z
  }
}

/**
 * 13.03.2022 перевод времени на час, поэтому 2:30 часов не будет, если в 1:30 + 1 час
 * Java is smart enough to know that there is no 2:30 a.m. that night and switches over to
 * the appropriate GMT offset.
 */
class SpringForward {
  public static void main(String[] args) {
    System.out.println(Month.MARCH);
    var date = LocalDate.of(2022, Month.MARCH, 13);
    var time = LocalTime.of(1, 30);
    var zone = ZoneId.of("US/Eastern");
    var dateTime = ZonedDateTime.of(date, time, zone);
    System.out.println(dateTime); // 2022–03-13T01:30-05:00[US/Eastern]
    System.out.println(dateTime.getHour()); // 1
    System.out.println(dateTime.getOffset()); // -05:00
    dateTime = dateTime.plusHours(1);
    System.out.println(dateTime); // 2022–03-13T03:30-04:00[US/Eastern]
    System.out.println(dateTime.getHour()); // 3
    System.out.println(dateTime.getOffset()); // -04:00

    System.out.println("\n" + Month.NOVEMBER);
    date = LocalDate.of(2022, Month.NOVEMBER, 6);
    time = LocalTime.of(1, 30);
    zone = ZoneId.of("US/Eastern");
    dateTime = ZonedDateTime.of(date, time, zone);
    System.out.println(dateTime); // 2022-11-06T01:30-04:00[US/Eastern]
    System.out.println(dateTime.getOffset());
    dateTime = dateTime.plusHours(1);
    System.out.println(dateTime); // 2022-11-06T01:30-05:00[US/Eastern]
    System.out.println(dateTime.getOffset());
    dateTime = dateTime.plusHours(1);
    System.out.println(dateTime); // 2022-11-06T02:30-05:00[US/Eastern]
    System.out.println(dateTime.getOffset());

    date = LocalDate.of(2022, Month.MARCH, 13);
    time = LocalTime.of(2, 30);
    zone = ZoneId.of("US/Eastern");
    dateTime = ZonedDateTime.of(date, time, zone);
    System.out.println(dateTime); // 2022–03–13T03:30–04:00[US/Eastern]
  }
}