package com.ocajp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * An ArrayList internally uses an array to store all its elements.
 * Whenever you add an element to an ArrayList, it checks whether the array can
 * accommodate the new value. If it can’t, ArrayList creates a larger array, copies all the
 * existing values to the new array, and then adds the new value at the end of the array. If
 * you frequently add elements to an ArrayList, it makes sense to create an ArrayList
 * with a bigger capacity because the previous process isn’t repeated for each ArrayList insertion.
 */
public class TestArrayList {
  public static void main(String[] args) {
    double one = Math.pow(1,2);
    //int two = Math.round(1.0); //Provided: long
    int two = Math.round(1.0f);
    //float three = Math.random(); //Provided: double
    double three = Math.random();
    var doubles = new double[] { one, two, three};
    System.out.println(Arrays.toString(doubles));

    String [] names = {"Tom", "Dick", "Harry"};
    //List<String> list = names.asList(); //Cannot resolve method 'asList' in 'String'
    var other = Arrays.asList(names);
    other.set(0, "Sue");
    System.out.println(other);

    System.out.println("------------");

    List<Integer> integers = new ArrayList<>();
    integers.add(Integer.parseInt("5")); //autoboxing to Integer
    integers.add(Integer.valueOf("6")); //return Integer, no need autoboxing
    integers.add(7); //autoboxing to Integer
    integers.add(null);
    for (int i : integers) { //NullPointerException.  Cannot invoke "java.lang.Integer.intValue()" because the return value of "java.util.Iterator.next()" is null
      System.out.println(i);
    }

    System.out.println("------------");


    String[] d = new String[] {"2", "3"};
    String[] a = new String[] {"2", "3"};
    System.out.println(d.equals(a)); //false
    System.out.println(Arrays.equals(d, a)); //true
    List<String> gorillas = new ArrayList<>(); //List<String> gorillas = new ArrayList<String>()
    //gorillas.remove(0); //IndexOutOfBoundsException: Index 0 out of bounds for length 0
    for (var koko : gorillas) { // String koko
      System.out.println(koko);
    }
    var monkeys = new ArrayList<>(); //ArrayList<Object> monkeys = new ArrayList<Object>()
    for (var albert : monkeys) { //Object alber
      System.out.println(albert);
    }

    List chimpanzees = new ArrayList<Integer>(); //List chimpanzees = new ArrayList<Integer>()
    for (var ham : chimpanzees) { //Object ham
      System.out.println(ham);
    }
  }
}

class Q13 {
  public static void main(String[] args) {
    doIt1();
    doIt2();
  }

  /**
   * при ссылках на другие массивы, когда делается изменение объекта
   * то меняется в обоих массивах.
   * Перемещение объектов и добавление удаление новых не меняет первый массив
   * То есть содержимое не зеркалируется
   */
  public static void doIt1() {
    List<StringBuilder> sbListOne = new ArrayList<>();
    sbListOne.add(new StringBuilder("Anna"));
    sbListOne.add(new StringBuilder("Ada"));
    sbListOne.add(new StringBuilder("Bob"));

    List<StringBuilder> sbListTwo = new ArrayList<>(sbListOne);
    sbListOne.add(null);
    sbListOne.get(1).reverse();
    sbListOne.remove(0);
    System.out.println(sbListOne); //[adA, Bob, null]
    System.out.println(sbListTwo); //[Anna, adA, Bob]
  }

  public static void doIt2() {
    List<String> listOne = new ArrayList<>();
    listOne.add("Anna");
    listOne.add("Ada");
    listOne.add("Bob");

    List<String> listTwo = new ArrayList<>(listOne);
    String strTemp = listOne.get(0);
    listOne.set(0, listOne.get(listOne.size()-1));
    listOne.set(listOne.size()-1, strTemp);
    System.out.println(listOne); //[Bob, Ada, Anna]
    System.out.println(listTwo); //[Anna, Ada, Bob]
  }
}

/**
 * List не имеет метода clone
 * ArrayList имеет метод clone
 * Option (e) is correct. Calling clone() on an ArrayList will create a separate reference variable that stores the
 * same number of elements as the ArrayList to be cloned.
 * But each individual ArrayList element will refer to the same object; that is, the individual ArrayList elements
 * aren’t cloned.
 */
class Clone {
  public static void main(String[] args) {
    //listOne.clone(); //'clone()' has protected access in 'java.lang.Object'
    ArrayList listOne = new ArrayList();
    listOne.add("Test");
    ArrayList listTwo = (ArrayList) listOne.clone();
    System.out.println(listOne.hashCode()); //2603217
    System.out.println(listTwo.hashCode()); //2603217
    System.out.println(listOne == listTwo); //false
    System.out.println(listOne.equals(listTwo)); //true
    listTwo.set(0, "Tested");
    System.out.println(listOne);
    System.out.println(listTwo);
    listOne = null;
    System.out.println(listOne);
    System.out.println(listTwo);
  }
}

/**
 * удаление по объекту вернет удаленный объект, как и set()
 * удаление по индексу вернет булеан
 */
class Remove {
  public static void main(String[] args) {
    ArrayList listOne = new ArrayList();
    System.out.println(listOne.add("Test"));
    System.out.println(listOne.remove(0)); //Test

    System.out.println(listOne.remove("Test")); //false

    listOne.add("New");
    System.out.println(listOne.add(listOne.set(0, "TestedNew")));
    System.out.println(listOne);
  }
}
