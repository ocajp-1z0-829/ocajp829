package com.ocajp;

public class Mathem {
  public static void main(String[] args) {
    System.out.println("MIN and MAX");
    int first = Math.max(3, 7); // 7
    double second = Math.min(7.0, -9); // -9.0
    System.out.println("max " + first + "; min " + second);

    /**
     * if > .5 then 1
     * double -> long
     * float -> int
     */
    System.out.println("Rounding Numbers");
    long low = Math.round(123.45); // 123
    long high = Math.round(123.50); // 124
    int fromFloat = Math.round(123.45f); // 123
    //int fromDouble = Math.round(123.45);
    //int fromDouble = Math.round(123.45);
    //int fromDouble = Math.round(123.45);
    System.out.println(low + " " + high +  " " + fromFloat);

    /**
     * The ceil() method takes a double value. If it is a whole number, it returns the same
     * value. If it has any fractional value, it rounds up to the next whole number. By contrast, the
     * floor() method discards any values after the decimal
     */
    System.out.println("Determining the Ceiling and Floor");
    double c = Math.ceil(3.14); // 4.0
    System.out.println(Math.ceil(4.99)); //5.0 получает следующее целое значение
    System.out.println(Math.ceil(1)); //1.0 получает следующее целое значение
    double f = Math.floor(3.14); // 3.0
    System.out.println(Math.floor(4.99)); //4.0 отсекает после запятой
    System.out.println(Math.floor(1)); //1.0
    System.out.println(c + " " + f);

    System.out.println("Calculating Exponents");
    double squared = Math.pow(5, 2); // 25.0
    System.out.println(squared);
    //int squaredInt = Math.pow(5, 2); //Required type: int, Provided: double

    /**
     * The random() method returns a value greater than or equal to 0 and less than 1
     */
    System.out.println("Generating Random Numbers");
    double num = Math.random();
    System.out.println(num); //0.5181455681241078

  }
}
