package com.ocajp;

import java.util.Arrays;

public class TestArrays {
  public static void main(String[] args) {
    //int[][] java = new int[][]; //Array initializer expected
    int i[][] = {{}, new int[2] };
    long l = 10;
    byte b = 10;
    char c = 'c';
    System.out.println(Arrays.deepToString(i)); //
    //i[0][1] = l; //Required type: int
    //i[0][0] = b; //Index 0 out of bounds for length 0
    i[1][0] = c;
    //i[0][1] = (int) l; //Index 1 out of bounds for length 0

    //[]int arr1; //Unexpected token
    int[] arr1;
    int[] arr2 = new int[3];
    char[] arr3 = {'a', 'b'};
    arr1 = arr2;
    //arr1 = arr3; //Required type: int[]

    int[][] a = {{0}, {1,2}, {3,4,5},{6,7,8,9}, {10,11,12,13,14}};
    System.out.println(a[4][2]);
    //System.out.println(a[2][4]); //Index 4 out of bounds for length 3
    System.out.println(a.getClass().isArray());

    int[][] aa = new int[0][4]; //not true
    //aa[0] = new int[2];// Index 0 out of bounds for length 0
  }
}

class Test50 {
  public static void call() {
    mainer(new String[]{"Cat"});
  }
  public static void mainer(String... args) {
    System.out.println(Arrays.toString(args));
  }

  public static void main(String[] args) {
    call();

    int[][][] i, j;
    i = new int[][][]{};
    j = new int[][][]{};

    //String... ars = args; //Not a statement
    String b[] = new String[]{};
    System.out.println(Arrays.toString(b));
    //System.out.println(b[0]); //Array index is out of bounds

    int[][] arr = new int[][]{{0,0}};
    System.out.println(arr.length);
    System.out.println(Arrays.deepToString(arr));
  }
}

class Sudoku {
  static int[][] game = new int[6][6];

  public static void main(String[] args) {
    game[3][3] = 6;
    Object obj = game; //OK
    Object[] objs = game; //OK
    objs[3] = "X"; //Storing element of type 'java.lang.String' to array of 'int[]' elements will produce 'ArrayStoreException'
    System.out.println(game[3][3]);

  }
}

class Sudoku2 {
  static int[][] game;

  public static void main(String[] args) {
    game[3][3] = 6;
    Object obj = game; //OK
    Object[] objs = game; //OK
    //game[3][3] = "X"; //Required type: int
    System.out.println(game[3][3]);
  }
}