package com.ocajp;

import java.util.Arrays;

public class ArraysTopic {
  public static void main(String[] args) {
    String s = "dsa";
    int[] numAnimals;
    int [] numAnimals2;
    int []numAnimals3;
    int numAnimals4[];
    int numAnimals5 [];
    int[]numAnimals6;
    //var d[] = new int[]{3}; //'var' is not allowed as an element type of an array
    int[] ids, types;
    int idsArr[], typesInt; //first is array, second is regular  integer
    idsArr = new int[]{1, 2, 3};
    typesInt = 4;
    System.out.println(Arrays.toString(idsArr) + " " + typesInt);
    String[] strings = { "stringValue" };
    Object[] objects = strings;
    String[] againStrings = (String[]) objects;
    /**
     * String[] only allows String objects, and
     * StringBuilder is not a String.
     */
    //againStrings[0] = new StringBuilder(); // DOES NOT COMPILE Required type: String
    objects[0] = new StringBuilder(); // Careful At runtime, the code throws an ArrayStoreException

  }
}

class VarArray {
  public static void main(String[] args) {
    var birds = new String[6];
    System.out.println(birds.length); //Понимает что массив, выводит 6

    var numbers = new int[10];
    for (int i = 0; i < numbers.length; i++)
      numbers[i] = i + 5;
    System.out.println(Arrays.toString(numbers));
  }
}

/**
 * Numbers sort before
 * letters, and uppercase sorts before lowercase.
 */
class Sort {
  public static void main(String[] args) {
    String[] strings = { "10", "9", "100" };
    Arrays.sort(strings);
    for (String s : strings)
      System.out.print(s + " "); //10 100 9
  }
}

/**
 * Java also provides a convenient way to search, but only if the array is already sorted.
 * Target element found in sorted array Index of match
 * Target element not found in sorted array Negative value showing one smaller than the
 *  negative of the index, where a match needs to be inserted to preserve sorted order
 *Take note of the fact that line 3 is a sorted array. If it wasn’t, we couldn’t apply either of
 * the other rules. Line 4 searches for the index of 2. The answer is index 0. Line 5 searches for
 * the index of 4, which is 1.
 * Line 6 searches for the index of 1. Although 1 isn’t in the list, the search can determine
 * that it should be inserted at element 0 to preserve the sorted order. Since 0 already means
 * something for array indexes, Java needs to subtract 1 to give us the answer of –1. Line 7
 * is similar. Although 3 isn’t in the list, it would need to be inserted at element 1 to preserve
 * the sorted order. We negate and subtract 1 for consistency, getting –1 –1, also known as –2.
 * Finally, line 8 wants to tell us that 9 should be inserted at index 4. We again negate and subtract 1, getting –4
 * –1, also known as –5
 *
 * Unsorted array A surprise; this result is undefined
 */
class Searching {
  public static void main(String[] args) {
    int[] numbers = {2,4,6,8};
    System.out.println(Arrays.binarySearch(numbers, 2)); // 0
    System.out.println(Arrays.binarySearch(numbers, 4)); // 1
    System.out.println(Arrays.binarySearch(numbers, 1)); // -1
    System.out.println(Arrays.binarySearch(numbers, 3)); // -2
    System.out.println(Arrays.binarySearch(numbers, 9)); // -5

    int[] unsortedNumbers = new int[] {3,2,1};
    System.out.println(Arrays.binarySearch(unsortedNumbers, 2)); //1
    System.out.println(Arrays.binarySearch(unsortedNumbers, 3)); //-4
  }
}

/**
 * A negative number means the first array is smaller than the second.
 * A zero means the arrays are equal.
 * A positive number means the first array is larger than the second
 */
class Comparing {
  public static void main(String[] args) {
    System.out.println(Arrays.compare(new int[] {1}, new int[] {2})); //-1 первый меньше
    System.out.println(Arrays.compare(new int[] {1,3 }, new int[] {2})); //-1 первый меньше
    System.out.println(Arrays.compare(new int[] {1,2 }, new int[] {1})); //1 первый больше
    System.out.println(Arrays.compare(new int[] {1,2 }, new int[] {1, 2})); //0 равны
    System.out.println(Arrays.compare(new int[] {2,3 }, new int[] {2})); //1 первый больше
  }
}

/**
 * If the arrays are equal, mismatch() returns -1
 * If mismatch return index 0 or other
 */
class Mismatch {
  public static void main(String... args) {
    System.out.println(args.length);
    System.out.println(Arrays.mismatch(new int[] {1}, new int[] {1})); //-1
    System.out.println(Arrays.mismatch(new String[] {"a"}, new String[] {"A"})); //0 the first index mismatch
    System.out.println(Arrays.mismatch(new int[] {1, 2}, new int[] {1, 2, 3})); //2 the last index mismatch
    System.out.println(Arrays.mismatch(new int[] {1, 2}, new int[] {})); //0
  }
}

class MultiDimensional {
  public static void main(String[] args) {
    int[][] vars1; // 2D array
    int vars2 [][]; // 2D array
    int[] vars3[]; // 2D array
    int[] vars4 [], space [][]; // a 2D AND a 3D array


    /**
     * 0 0
     * 0 0
     * 0 0
     */
    var twoD = new int[3][2];
    for(int i = 0; i < twoD.length; i++) {
      for(int j = 0; j < twoD[i].length; j++)
        System.out.print(twoD[i][j] + " "); // print element
      System.out.println(); // time for a new row
    }

    for(int[] inner : twoD) {
      for(int num : inner) {
        System.out.print(num + " ");
        break;
      }
      System.out.println();
    }

    int [][] arr = new int[4][];
    System.out.println(arr[0][0]); //NullPointerException: Cannot load from int array because "arr[0]" is null
    arr[0] = new int[5];
    System.out.println(arr[0][0]); //0
    arr[1] = new int[3];
  }
}