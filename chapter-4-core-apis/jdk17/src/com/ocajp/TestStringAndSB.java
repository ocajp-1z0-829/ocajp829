package com.ocajp;

import java.util.function.Predicate;

public class TestStringAndSB {
  public static void main(String[] args) {
    StringBuilder puzzle = new StringBuilder("Java");
    StringBuilder s = new StringBuilder(puzzle);
    System.out.println(s);
    //System.out.println(puzzle.reverse());
    //System.out.println(puzzle.append("vaJ$").substring(0, 4)); //substing is ignored
    System.out.println(puzzle.append("vaJ$").delete(0, 3).deleteCharAt(puzzle.length() - 1)); //"JavavaJ$".delete(0,
    // 3).deleteCharAt(4)
    //System.out.println(puzzle.append("vaJ$").delete(0, 3).deleteCharAt(puzzle.length()));
    //StringIndexOutOfBoundsException: index 5, length 5

    StringBuilder sb = new StringBuilder("rumble");
    sb.append(4).deleteCharAt(3);
    System.out.println(sb);
    System.out.println(sb.length());
    sb.delete(3, sb.length() - 1); //"rumle4".delete(3, 5) = rum4
    System.out.println(sb);

    "a".replace("a", "b");
    //"a".replace('a', "b"); //Cannot resolve method 'replace(char, java.lang.String)'
    "a".replace('a', 'b');
    //"a".replace(0, "a".length(), "b"); //Cannot resolve method 'replace(int, int, java.lang.String)'
    "a".replace(new StringBuilder('a'), ""); // replace(@NotNull CharSequence target, @NotNull CharSequence replacement
    "a".replace(new StringBuilder('a'), new StringBuilder("b"));
    //new StringBuilder("a").replace("", "b"); //'replace(int, int, java.lang.String)' in 'java.lang.StringBuilder'
    // cannot be applied to '(java.lang.String, java.lang.String)'
    //new StringBuilder("a").replace('a', 'b'); //'replace(int, int, java.lang.String)' in 'java.lang.StringBuilder'
    // cannot be applied to '(char, char)'
    new StringBuilder("a").replace(0,1,"b");


    var string = "12345";
    var builder = new StringBuilder("12345");
    //builder.appendCodePoint(97);
    //builder.appendCodePoint('\u0097');
    //builder.appendCodePoint('a'); //12345a
    System.out.println(builder);
    System.out.println(builder.charAt(4)); //5
    //System.out.println(builder.replace(2, 4, "6")); //1265
    //System.out.println(builder.replace(2, 4, "6").charAt(3)); //5
    //System.out.println(builder.replace(2, 5, "6")); //126
    //System.out.println(builder.replace(2, 5, "6").charAt(2)); //6
    //System.out.println(string.charAt(5)); //String index out of range: 5
    System.out.println(string.replace("123", "1")); //145
    System.out.println(string.replace("123", "1").charAt(2)); //5

    var letters = new StringBuilder("abcdefg");
    System.out.println(letters.substring(0));
    System.out.println(letters.substring(1, 2));
    System.out.println(letters.substring(2, 2));
    //System.out.println(letters.substring(6, 5)); //.StringIndexOutOfBoundsException start 6, end 5, length 7
    System.out.println(letters.delete(1, 2));
  }
}


class Test50StrSB {
  public static void main(String[] args) {
    /**
     * The sb reference doesn’t exist until after the chained
     * calls complete
     */
    //StringBuilder sb = new StringBuilder("radical").insert(sb.length(), "robots"); //'sb' mightn't initialized

    StringBuilder sb = new StringBuilder("radical");
    sb.substring(2);
    //StringBuilder sb2 = sb.substring(2); //Required type: StringBuilder
    System.out.println(sb);

    Predicate<String> pred = (String s) -> true;

    String line = new String("-");
    String anotherLine = line.concat("-");
    System.out.println(line == anotherLine);
    System.out.println("-");
    System.out.println(line.length());

  }
}