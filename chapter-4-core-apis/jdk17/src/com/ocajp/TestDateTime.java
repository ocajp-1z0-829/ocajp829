package com.ocajp;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;

public class TestDateTime {
}

/**
 * The dateTime1 object has a time of 1:30 per initialization. The dateTime2 object
 * is an hour later. However, there is no 2:30 when springing ahead, setting the time to 3:30.
 * Option A is correct because it is an hour later. Option D is also correct because the hour of
 * the new time is 3. Option E is not correct because we have changed the time zone offset due
 * to daylight saving time.
 */
class Q20 {
  public static void main(String[] args) {
    var date = LocalDate.of(2022, Month.MARCH, 13);
    var time = LocalTime.of(1, 30);
    var zone = ZoneId.of("US/Eastern");
    var dateTime1 = ZonedDateTime.of(date, time, zone);
    var dateTime2 = dateTime1.plus(1, ChronoUnit.HOURS);
    long diff = ChronoUnit.HOURS.between(dateTime1, dateTime2);
    int hour = dateTime2.getHour();
    boolean offset = dateTime1.getOffset() == dateTime2.getOffset();
    System.out.println("diff = " + diff);
    System.out.println("hour = " + hour);
    System.out.println("offset = " + offset);
  }
}

/**
 * The compare() method returns a positive integer when the arrays are different and
 * the first is larger. This is the case for option A since the element at index 1 comes first alphabetically. It is
 * not the case for option C because the s4 is longer or for option E because the
 * arrays are the same.
 * The mismatch() method returns a positive integer when the arrays are different in a position index 1 or greater.
 * This is the case for options B and D since the difference is at index 1.
 * It is not the case for option F because there is no difference.
 *
 *     public static int compareTo(byte[] value, byte[] other, int len1, int len2) {
 *         int lim = Math.min(len1, len2);
 *         for (int k = 0; k < lim; k++) {
 *             if (value[k] != other[k]) {
 *                 return getChar(value, k) - getChar(other, k);
 *             }
 *         }
 *         return len1 - len2;
 */
class Q19 {
  public static void main(String[] args) {
    String[] s1 = { "Camel", "Peacock", "Llama"};
    String[] s2 = { "Camel", "Llama", "Peacock"};
    String[] s3 = { "Camel"};
    String[] s4 = { "Camel", null};
    System.out.println(Arrays.compare(new String[]{"123834343", "334"}, new String[]{"899"})); //сравнивает первый символ
    // итого 8-1=-7
    System.out.println(Arrays.compare(s1, s2));  //MNOP == 4
    System.out.println(Arrays.mismatch(s1, s2)); //1 разница в 1 индексе
    System.out.println(Arrays.compare(s3, s4)); //-1 первый массив меньше
    System.out.println(Arrays.mismatch(s3, s4)); //1 разница в 1 индексе
    System.out.println(Arrays.compare(s4, s4)); // 0 массивы равны
    System.out.println(Arrays.mismatch(s4, s4)); //-1 нет несопоставлений
  }
}

/**
 * First, notice that the indent() call adds a blank space to the beginning of
 * numbers, and stripLeading() immediately removes it.
 *
 * There are 11 characters in base because there are two escape characters. The \n
 * counts as one character representing a new line, and the \\ counts as one character representing a backslash.
 * This makes option B one of the answers. The indent() method adds
 * two characters to the beginning of each of the two lines of base. This gives us four additional characters.
 * However, the method also normalizes by adding a new line to the end if
 * it is missing. The extra character means we add five characters to the existing 11, which is
 * option G. Finally, the translateEscapes() method turns any text escape characters into
 * actual escape characters, making \\t into \t. This gets rid of one character, leaving us with
 * 10 characters matching option A.
 */
class Q16 {
  public static void main(String[] args) {
    var base = "ewe\nsheep\\t";
    int length = base.length();
    System.out.println(base);
    int indent = base.indent(2).length();
    System.out.println(base.indent(2));
    int translate = base.translateEscapes().length();
    System.out.println(base.translateEscapes());
    var formatted = "%s %s %s".formatted(length, indent, translate);
    System.out.format(formatted);
  }
}

/**
 * The code compiles fine. Line 3 points to the String in the string pool. Line 4 calls
 * the String constructor explicitly and is therefore a different object than s. Line 5 checks
 * for object equality, which is true, and so it prints one. Line 6 uses object reference equality,
 * which is not true since we have different objects. Line 7 calls intern(), which returns the
 * value from the string pool and is therefore the same reference as s. Line 8 also compares references but is true
 * since both references point to the object from the string pool. Finally, line
 * 9 is a trick. The string Hello is already in the string pool, so calling intern() does not
 * change anything. The reference t is a different object, so the result is still false.
 */
class Q4 {
  public static void main(String[] args) {
    var s = "Hello";
    var t = new String(s);
    if ("Hello".equals(s)) System.out.println("one");
    if (t == s) System.out.println("two");
    if (t.intern() == s) System.out.println("three");
    if ("Hello" == s) System.out.println("four");
    if ("Hello".intern() == t) System.out.println("five");
  }
}

/**
 * Option B throws an exception because there is no March 40. Option E also throws
 * an exception because 2023 isn’t a leap year and therefore has no February 29. Option F
 * doesn’t compile because the enum should be named Month, rather than MonthEnum. Option
 * D is correct because it is just a regular date and has nothing to do with daylight saving time.
 * Options A and C are correct because Java is smart enough to adjust for daylight saving time.
 */
class Q3 {
  public static void main(String[] args) {
    var zone = ZoneId.of("US/Eastern");
    var date1 = LocalDate.of(2022, 3, 13);
    var date2 = LocalDate.of(2022, 11, 6);
    var date3 = LocalDate.of(2022, 11, 7);
    //var date4 =  LocalDate.of(2023, 2, 29); //Feb не высокосный год //DateTimeException: Invalid date 'February 29'
    // as '2023' is not a leap year
    var time = LocalTime.of(2, 15);
    var z1 = ZonedDateTime.of(date1, time, zone);
    System.out.println(z1);
    var z2 = ZonedDateTime.of(date2, time, zone);
    System.out.println(z2);
    var z3 = ZonedDateTime.of(date3, time, zone);
    System.out.println(z3);



  }
}
