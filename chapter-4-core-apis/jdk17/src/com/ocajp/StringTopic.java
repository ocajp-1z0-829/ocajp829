package com.ocajp;

public class StringTopic {
  public static void main(String[] args) {

    System.out.println(" ".isEmpty()); // false
    System.out.println("".isEmpty()); // true
    System.out.println(" ".isBlank()); // true
    System.out.println("".isBlank()); // true

    //String name = """Fluffy"""; //Illegal text block start: missing new line after opening quotes
    String name = """
        Fluffy""";
    System.out.println(name); //Fluffy

    var s = "5";
    s += 4;
    System.out.println(s);
    s = "";
  }
}

/**
 * whitespace consists of spaces along with the \t (tab) and \n (newline) characters.
 * Other characters, such as \r (carriage return), are also included in what gets trimmed.
 * The strip() method does everything that trim() does, but it supports Unicode. char ch = '\u2000';
 */
class StripAndTrim {
  public static void main(String[] args) {
    System.out.println("abc".strip()); // abc
    System.out.println("\t a b c\n".strip()); // a b c
    System.out.println("\t a b c\r".strip()); // a b c
    System.out.println("\t a b c\r".trim()); // a b c
    String text = " abc\t ";
    System.out.println(text.trim().length()); // 3
    System.out.println(text.strip().length()); // 3
    System.out.println(text.stripLeading().length()); // 5
    System.out.println(text.stripTrailing().length());// 4
    System.out.println(("\t test " + '\u2000').strip());// test
  }
}

class IndentAndStripIndent {
  public static void main(String[] args) {
    /**
     * counts the six characters in block, which are the three letters, the blank space
     * before b, and the \n after a and b
     */
    var block = """
        a
         b
        c"""; //length = 6

    /**
     * the six characters in block, which are the three letters, the blank space
     * before b, and the \n after a and b. Line 18 counts the nine characters in concat, which are
     * the three letters, one blank space before a, two blank spaces before b, one blank space before
     * c, and the \n after a and b
     */
    var concat =  " a\n"
                + "  b\n"
                + " c"; //length = 9
    System.out.println(block);
    System.out.println(block.length()); // 6
    System.out.println(concat.length()); // 9
    System.out.println(block.indent(1)); // add a single blank space to each of the three lines in block, and add
    // empty at the end
    System.out.println(block.indent(1).length()); // 10

    /**
     * remove one whitespace character from each of the three lines of concat
     */
    System.out.println(concat.indent(-1)); //удалить все пробелы, что слева, 7 это нижний отступ
    System.out.println(concat.indent(-1).length()); // 7

    /**
     * remove four whitespace characters from the same three lines.
     * Since there are not four whitespace characters, Java does its best
     */
    System.out.println(concat.indent(-4).length()); // 6

    System.out.println("---");
    System.out.println(concat);
    System.out.println(concat.stripIndent());
    /**
     * All of the lines have at least one
     * whitespace character. Since they do not all have two whitespace characters, the method only
     * gets rid of one character per line. Since no new line is added by stripIndent(), the length
     * is six, which is three less than the original nine.
     */
    System.out.println(concat.stripIndent().length()); // 6

    System.out.println("---");
    System.out.println(block.length()); //6
    block += "\r\n";
    System.out.println(block.length()); //8
    System.out.println(block.strip().length()); //6
    System.out.println(block.trim().length()); //6
    String str = "" + '\u2000' + block;
    System.out.println(str);
    System.out.println(str.strip().length()); //6
    System.out.println(str.trim().length()); //7
  }
}

/**
 * This method can be used for escape
 * sequences such as \t (tab), \n (new line), \s (space), \" (double quote), and \' (single quote.)
 */
class TranslatingEscapes {
  public static void main(String[] args) {
    var str = "1\\t2";
    System.out.println(str);
    System.out.println(str.translateEscapes()); // 1 2
  }
}

class Format {
  public static void main(String[] args) {
    var name = "Kate";
    var orderId = 5;
    // All print: Hello Kate, order 5 is ready
    System.out.println("Hello "+name+", order "+orderId+" is ready");
    System.out.println(String.format("Hello %s, order %d is ready",
        name, orderId));
    System.out.println("Hello %s, order %d is ready"
        .formatted(name, orderId));
    System.out.println("Kate".formatted(orderId));

    var score = 90.25;
    var total = 100;
    System.out.println("%s:%n Score: %f out of %d"
        .formatted(name, score, total));

    //var str = "Food: %d tons".formatted(2.0); // IllegalFormatConversionException

    var pi = 3.14159265359;
    System.out.format("[%f]",pi); // [3.141593]
    System.out.format("[%12.8f]",pi); // [ 3.14159265]
    System.out.format("[%012f]",pi); // [00003.141593]
    System.out.format("[%12.2f]",pi); // [ 3.14]
    System.out.format("[%.3f]",pi); // [3.142]

  }
}