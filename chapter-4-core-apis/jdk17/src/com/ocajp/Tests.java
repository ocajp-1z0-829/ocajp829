package com.ocajp;

import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;

public class Tests {
}

class Q2 {
    public static void main(String[] args) {
        int[][] scores5 = new int[5][]; // [null, null, null, null, null]
        System.out.println(Arrays.deepToString(scores5));
        int[][] scores52 = new int[5][2]; // [[0, 0], [0, 0], [0, 0], [0, 0], [0, 0]]
        System.out.println(Arrays.deepToString(scores52));
        Object[][][] cubbies325 = new Object[3][2][5];
        System.out.println(Arrays.deepToString(cubbies325)); // [[[null, null, null, null, null], [null, null, null, null, null]], [[null, null, null, null, null], [null, null, null, null, null]], [[null, null, null, null, null], [null, null, null, null, null]]]
        Object[][][] cubbies305 = new Object[3][0][5];
        System.out.println(Arrays.deepToString(cubbies305)); // [[], [], []]
        Object[][][] cubbies3 = new Object[3][][];
        System.out.println(Arrays.deepToString(cubbies3)); // [null, null, null]
        //String beans[] = new beans[6];
        java.util.Date[] dates[] = new java.util.Date[2][]; // [null, null]
        System.out.println(Arrays.deepToString(dates));
        //int[][] types = new int[];
        //int[][] java = new int[][];
    }
}

class Q3_ {
    public static void main(String[] args) {
        var zone = ZoneId.of("US/Eastern");
        var time = LocalTime.of(2, 15);
        var date = LocalDate.of(2022, 3, 13);
        var z = ZonedDateTime.of(date, time, zone);
        System.out.println(z);
        date = LocalDate.of(2022, 11, 6);
        z = ZonedDateTime.of(date, time, zone);
        System.out.println(z);
        date = LocalDate.of(2022, 11, 7);
        z = ZonedDateTime.of(date, time, zone);
        System.out.println(z);

        System.out.println(ZoneId.systemDefault());
        System.out.println(ZoneId.getAvailableZoneIds());
    }
}

class Q4_ {
    public static void main(String[] args) {
        if (new String("Hello").intern() == "Hello") System.out.println("three");
        var s = "Hello";
        var t = new String(s);
        if ("Hello".equals(s)) System.out.println("one");
        if (t == s) System.out.println("two");
        if (t.intern() == s) System.out.println("three");
        if ("Hello" == s) System.out.println("four");
        if ("Hello".intern() == t) System.out.println("five");
    }
}

class Q6 {
    public static void main(String[] args) {
        double one = Math.pow(1, 2);
        //int two = Math.round(1.0); // long expected
        int two = Math.round(1.0f);
        //float three = Math.random(); // double expected
        var doubles = new double[] {one, 1, 0.5};
        System.out.println(Arrays.toString(doubles));
    }
}

class Q8_ {
    public static void main(String[] args) {
        var string = "12345";
        var builder = new StringBuilder("12345");
        System.out.println(builder.charAt(4));
        System.out.println(builder.replace(2, 4, "6"));
        builder = new StringBuilder("12345");
        System.out.println(builder.replace(2, 4, "6").charAt(3));
        builder = new StringBuilder("12345");
        System.out.println(builder.replace(2, 50, "6"));
        builder = new StringBuilder("12345");
        System.out.println(builder.replace(2, 5, "6").charAt(2));
        //System.out.println(string.charAt(5));
        //string.length
        System.out.println(string.replace("123", "1"));
        System.out.println(string.replace("123", "1").charAt(2));
    }
}

class Q9 {
    public static void main(String[] args) {
        int[] cubbies1 = new int[3];
        int[] cubbies2 = new int[3];
        System.out.println(cubbies2.equals(cubbies1)); //false
        //System.out.println(Arrays.deepEquals(cubbies2, cubbies1)); //true

        System.out.println(Arrays.compare(cubbies1, cubbies2) == 0);

        System.out.println(Arrays.mismatch(new String[]{"a"},new String[]{"a"}) == -1);
    }
}

class Q10 {
    public static void main(String[] args) {
        int one = Math.min(5, 3);
        System.out.println(one);
        long two = Math.round(5.5);
        System.out.println(two);
        double three = Math.floor(6.9);
        System.out.println(three);
        double four = Math.ceil(6.1);
        System.out.println(four);
        var doubles = new double[] {one, two, three, four};
        System.out.println(Arrays.toString(doubles));
    }
}

class Q11 {
    public static void main(String[] args) {
        var date = LocalDate.of(2022, 4, 3);
        date.plusDays(2);
        //date.plusHours(3);
        System.out.println(date.getYear() + " " + date.getMonth()
                + " " + date.getDayOfMonth());
    }
}

class Q12_1 {
    public static void main(String[] args) {
        var block = """ 
                        a
                         b 
                        c""";
        var concat =  " a\n"
                    + "  b\n"
                    + " c";
        System.out.println(block.length());
        System.out.println(concat.length());
        System.out.println(block.indent(1).length());
        System.out.println(concat.indent(-1).length());
        System.out.println(concat.indent(-4).length());
        System.out.println(concat.stripIndent().length());
// 6 // 9 // 10 // 7 // 6 // 6

        var str = "1\\t2";
        System.out.println(str); // 1\t2
        System.out.println(str.translateEscapes()); // 1 2
    }
}

class Q12 {
    public static void main(String[] args) {
        var numbers = "012345678".indent(1);
        numbers = numbers.stripLeading(); System.out.println(numbers.substring(1, 3)); System.out.println(numbers.substring(7, 7)); System.out.print(numbers.substring(7));
    }
}

class Q14 {
    public static void main(String[] args) {
        var date = LocalDate.now();
        var time = LocalTime.now();
        var dateTime = LocalDateTime.now();
        var zoneId = ZoneId.systemDefault();
        var zonedDateTime = ZonedDateTime.of(dateTime, zoneId);
        Instant instant = Instant.now();
        System.out.println(instant);
        instant = dateTime.toInstant(ZoneOffset.ofHours(6));
        System.out.println(instant);
        System.out.println(zonedDateTime.toInstant());

        var zonedDateTimeCustom = ZonedDateTime.of(2022, 01, 01, 01, 01, 01, 0, zoneId);
        System.out.println(zonedDateTimeCustom);
        var localDateTimeCustom = LocalDateTime.of(2022, 01, 01, 01, 01);
        System.out.println(localDateTimeCustom);

        System.out.println(localDateTimeCustom.equals(localDateTimeCustom));
        System.out.println(localDateTimeCustom.isBefore(dateTime));
        System.out.println(localDateTimeCustom.isEqual(localDateTimeCustom));

        LocalDateTime localDateTime = LocalDateTime.of(2022, Month.NOVEMBER, 15, 17, 30);
        localDateTime = localDateTime.minusDays(5).plusHours(3).minusSeconds(45);
        System.out.println(localDateTime);
        System.out.println();

        var duration = Duration.between(localDateTime, localDateTimeCustom);
        System.out.println(duration);
    }
}

class Q15 {
    public static void main(String[] args) {
        var arr = new String[] { "PIG", "pig", "123"};
        Arrays.sort(arr);
        System.out.println(Arrays.toString(arr));
        System.out.println(Arrays.binarySearch(arr, "Pippa")); // -2 -1 = -3
    }
}

class Q16_ {
    public static void main(String[] args) {
        var base = "ewe\nsheep\\t";
        int length = base.length(); //11
        /**
         *   ewe
         *   sheep\\t
         *
         */
        int indent = base.indent(2).length(); //16
        int translate = base.translateEscapes().length(); //10
        var formatted = "%s %s %s".formatted(length, indent, translate);
        System.out.format(formatted); //11 16 10
    }
}

class Q20_ {
    public static void main(String[] args) {
        var date = LocalDate.of(2022, Month.MARCH, 13);
        var time = LocalTime.of(1, 30);
        var zone = ZoneId.of("US/Eastern");
        var dateTime1 = ZonedDateTime.of(date, time, zone);
        var dateTime2 = dateTime1.plus(1, ChronoUnit.HOURS);
        System.out.println(dateTime1 + " " + dateTime2);
        long diff = ChronoUnit.HOURS.between(dateTime1, dateTime2);
        int hour = dateTime2.getHour();
        boolean offset = dateTime1.getOffset() == dateTime2.getOffset(); System.out.println("diff = " + diff);
        System.out.println("hour = " + hour);
        System.out.println("offset = " + offset);
    }
}
