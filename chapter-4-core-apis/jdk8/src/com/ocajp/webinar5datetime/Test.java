package com.ocajp.webinar5datetime;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import javax.xml.crypto.Data;

/**
 * The LocalDateTime class does not provide the isLeapYear() method.
 * The LocalTime class does not provide the isEqual() method.
 * The Period class does not provide the withWeeks() method, but does provide
 * the ofWeeks() static method.
 * Both the Period and LocalTime classes do not provide the plusWeeks()
 * method.
 *
 * The DateTimeFormatter class provides factory methods to obtain both
 * predefined and customized formatters.
 * The styles defined by the java.time.format.FormatStyle enum type
 * are locale sensitive.
 * The ofLocalizedDate() method of the DateTimeFormatter class
 * returns a formatter that is based on a format style (a constant of the
 * FormatStyle enum type) passed as an argument to the method.
 * The pattern "yy-mm-dd" cannot be used to create a formatter that can format
 * a LocalDate object. The letter m stands for minutes of the hour, which is not a
 * part of a date.
 */
public class Test {}

class Q2 {
  public static void main(String[] args) {
    LocalDate date = LocalDate.parse("2018-04-30", DateTimeFormatter.ISO_LOCAL_DATE);
    date.plusDays(2);
    //date.plusHours(); //it has no method to add hours and the code does not compile.

    System.out.println(date.getYear() + " " + date.getMonth() + " " + date.getDayOfMonth()); //2018 APRIL 30
    System.out.println(date);
  }
}

/**
 * 2015-05-10T11:22:33
 * P1Y2M3D
 * 2014-03-07T11:22:33
 * Localized(SHORT,)
 * 3/7/14
 * Localized(,SHORT)
 * 11:22 AM
 */
class Q5 {
  public static void main(String[] args) {
    LocalDateTime d = LocalDateTime.of(2015, 5, 10, 11, 22, 33);
    System.out.println(d);
    Period p = Period.of(1, 2, 3);
    System.out.println(p);
    d = d.minus(p);
    System.out.println(d);
    DateTimeFormatter ld = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT);
    System.out.println(ld);
    System.out.println(d.format(ld));

    DateTimeFormatter lt = DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT);
    System.out.println(lt);
    System.out.println(d.format(lt));
  }
}

/**
 *
 * Period does not allow chaining. Only the last Period method called counts, so only
 * the two years are subtracted.
 *
 * 2015-05-10T11:22:33
 * P2Y
 * 2013-05-10T11:22:33
 * Localized(SHORT,SHORT)
 * 5/10/13 11:22 AM
 */
class Q6 {
  public static void main(String[] args) {
    LocalDateTime d = LocalDateTime.of(2015, 5, 10, 11, 22, 33);
    System.out.println(d);
    Period p = Period.ofDays(1).ofYears(2);
    System.out.println(p);
    d = d.minus(p);
    System.out.println(d);
    DateTimeFormatter ldt = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT);
    System.out.println(ldt);
    System.out.println(d.format(ldt));
  }
}

/**
 * The date reference never gets updated, as the return value is ignored. If it had
 * been updated, the correct answer would have been (c). The
 * LocalDate.getMonth() method returns a Month enum constant—in this
 * case, Month.MARCH. The LocalDate.getMonthValue() method returns
 * the month as a value between 1 and 12—in this case, 3.
 */
class Q8 {
  public static void main(String[] args) {
    LocalDate date = LocalDate.of(2016, 3, 1);
    date.withMonth(4); //The date reference never gets updated, as the return value is ignored
    System.out.println();
    System.out.println(date.getYear() + " " + date.getMonth() + " " + date.getDayOfMonth()); //2016 MARCH 1
  }
}

/**
 * The calculation of p1.plus(p2).plus(p1) proceeds as follows:
 * P1Y1M1D + P2Y12M30D ==> P3Y13M31D + P1Y1M1D ==> P4Y14M32D
 */
class Q9 {
  public static void main(String[] args) {
    Period p1 = Period.of(1,1,1);
    Period p2 = Period.of(2,12,30);
    p1 = p1.plus(p2).plus(p1);
    System.out.println(p1);
  }
}

/**
 * The between() and until() methods return a Period, which can be
 * negative. The isAfter(), isBefore(), between(), and until() methods
 * are strict in the sense that the end date is excluded. The compareTo() method
 * returns 0 if the two dates are equal, a negative value if date1 is less than date2,
 * and a positive value if date1 is greater than date2.
 *
 *   int compareTo0(LocalDate var1) {
 *     int var2 = this.year - var1.year;
 *     if (var2 == 0) {
 *       var2 = this.month - var1.month;
 *       if (var2 == 0) {
 *         var2 = this.day - var1.day;
 *       }
 *     }
 *
 *     return var2;
 *   }
 */
class Q11 {
  public static void main(String[] args) {
    LocalDate date1 = LocalDate.of(2015, 8, 19);
    LocalDate date2 = LocalDate.of(2015, 10, 23);

    System.out.println(date2.isAfter(date1)); //true
    System.out.println(date1.isAfter(date2)); //false
    System.out.println(date2.isBefore(date1)); //false
    System.out.println(date1.isBefore(date2)); //true
    System.out.println(Period.between(date2, date1).isNegative()); //P-2M-4D //true от 23.10.2015 до 19.08.2015
    System.out.println(Period.between(date1, date2).isNegative()); //P2M4D //false от 19.08.2015 до 23.10.2015
    System.out.println(date2.until(date1).isNegative()); //P-2M-4D //true от 23.10.2015 до 19.08.2015
    System.out.println(date1.until(date2).isNegative()); //P2M4D //false от 19.08.2015 до 23.10.2015
    System.out.println(date1.compareTo(date2)); //-2 //true отняли месяц до числа не дошли
    System.out.println(date2.compareTo(date1)); // 2 //false отняли месяц до числа не дошли

    if (date2.isAfter(date1)) {
      System.out.println("date2 is after date1");
    }

  }
}

class Q12 {
  public static void main(String[] args) {
    /**
     * The styles defined by the java.time.format.FormatStyle enum type
     * are locale sensitive.
     */
    FormatStyle f = FormatStyle.SHORT;

    /**
     * The ofLocalizedDate() method of the DateTimeFormatter class
     * returns a formatter that is based on a format style (a constant of the
     * FormatStyle enum type) passed as an argument to the method.
     */
    System.out.println(DateTimeFormatter.ofLocalizedDate(f));

    /**
     * The pattern "yy-mm-dd" cannot be used to create a formatter that can format
     * a LocalDate object. The letter m stands for minutes of the hour, which is not a
     * part of a date.
     */
    DateTimeFormatter.ofPattern("yy-mm-dd"); //Lowercase 'mm' (minute) pattern is used: probably 'MM' (month) was
    DateTimeFormatter.ofPattern("YY-MM-dd"); //Uppercase 'YY' (week year) pattern is used: probably 'yy' (year) was
    DateTimeFormatter.ofPattern("yy-MM-DD"); //Uppercase 'DD' (day of year) pattern is used: probably 'dd' (day of
    // month) was intended
    // intended
    // intended
  }
}

/**
 * (a), (b), (c): The input string matches the pattern. The input string specifies the
 * mandatory parts of both a date and a time, needed by the respective method to
 * construct either a LocalTime, a LocalDate, or a LocalDateTime.
 * To use the pattern for formatting, the temporal object must provide the parts
 * corresponding to the pattern letters in the pattern. The LocalTime object in (d)
 * does not have the date part required by the pattern. The LocalDate object in (e)
 * does not have the time part required by the pattern. Both (d) and (e) will throw an
 * UnsupportedTemporalTypeException. Only the LocalDateTime
 * object in (f) has both the date and time parts required by the pattern.
 */
class Q13 {
  public static void main(String[] args) {
    String pattern = "MM/dd/yyyy 'at' HH:mm:ss";
    String inputStr = "02/29/2015 at 00:15:30";
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern(pattern);

    LocalTime time = LocalTime.parse(inputStr, dtf);
    System.out.println(time); //00:15:30

    LocalDate date = LocalDate.parse(inputStr, dtf);
    System.out.println(date); //2015-02-28

    LocalDateTime dateTime = LocalDateTime.parse(inputStr, dtf);
    System.out.println(dateTime); //2015-02-28T00:15:30

    System.out.println(LocalTime.MIDNIGHT); //00:00
    //String timeStr = LocalTime.MIDNIGHT.format(dtf); //UnsupportedTemporalTypeException: Unsupported field: MonthOfYear

    //String dateStr = LocalDate.of(1015, 12, 25).format(dtf); //UnsupportedTemporalTypeException: Unsupported field:
    // HourOfDay

    String dateTimeStr = LocalDateTime.of(2015, 12, 24, 0, 0).format(dtf);
    System.out.println(dateTimeStr); //12/24/2015 at 00:00:00
  }
}

/**
 * The input string matches the pattern. It specifies the date-based values that can be
 * used to construct a LocalDate object in (b), based on the date-related pattern
 * letters in the pattern. No time-based values can be interpreted from the input string,
 * as this pattern has only date-related pattern letters. (a) and (c), which require a time
 * part, will throw a DateTimeParseException.
 * To use the pattern for formatting, the temporal object must provide values for the
 * parts corresponding to the pattern letters in the pattern. The LocalTime object in
 * (d) does not have the date part required by the pattern. (d) will throw an
 * UnsupportedTemporalTypeException. The LocalDate object in (e) has
 * the date part required by the pattern, as does the LocalDateTime object in (f). In
 * (f), only the date part of the LocalDateTime object is formatted
 */
class Q14 {
  public static void main(String[] args) {
    String pattern = "'Date:' dd|MM|yyyy";
    String inputStr = "Date: 02|12|2015";
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern(pattern);
    System.out.println(dtf);
    //LocalTime time = LocalTime.parse(inputStr, dtf); //DateTimeParseException: Text 'Date: 02|12|2015' could not be
    // parsed: Unable to obtain LocalTime from TemporalAccessor: {},ISO resolved to 2015-12-02 of type
    LocalDate date = LocalDate.parse(inputStr, dtf);
    System.out.println(date); //2015-12-02
    //LocalDateTime dateTime = LocalDateTime.parse(inputStr, dtf); //DateTimeParseException: Text 'Date: 02|12|2015'
    // could not be parsed: Unable to obtain LocalDateTime from TemporalAccessor: {},ISO resolved to 2015-12-02 of type
    //String timeStr = LocalTime.MIDNIGHT.format(dtf); //DateTimeParseException: Text 'Date: 02|12|2015' could not be
    // parsed: Unable to obtain LocalDateTime from TemporalAccessor: {},ISO resolved to 2015-12-02 of type
    String dateStr = LocalDate.of(2015, 12, 24).format(dtf);
    System.out.println(dateStr); //Date: 24|12|2015
    String dateTimeStr = LocalDateTime.of(2015, 12, 24, 0, 0).format(dtf);
    System.out.println(dateTimeStr); //Date: 24|12|2015
  }
}

/**
 * (a), (b), (c), (d) and (f) result in a DateTimeParseException when parsing.
 * (a): The pattern letter h represents hour in the day, but requires AM/PM
 * information to resolve the hour in a 24-hour clock (i.e., pattern letter a), which is
 * missing.
 * (b): The pattern letter M is interpreted correctly as month of the year (value 5).
 * Matching the pattern letter h is the problem, as explained for (a).
 * (c), (d): The pattern letter a cannot be resolved from the input string, as an AM/PM
 * marker is missing in the input string.
 * (e): The parse succeeds, with the LocalTime object having the value 09:05.
 * Formatting this object with the formatter results in the output string: 5 minutes
 * past 9.
 * (f): The letter pattern mm cannot be resolved, as the minutes value has only one
 * digit (i.e., 5) in the input string.
 * (g): The parse succeeds, with the resulting LocalTime object having the value
 * 09:00. The month value 5 is ignored. Formatting this object with the formatter
 * results in an UnsupportedTemporalTypeException, because now the
 * pattern letter M requires a month value, which is not part of a LocalTime object
 */
class Q15 {
  public static void main(String[] args) {
    //The pattern letter h represents hour in the day, but requires AM/PM
    //information to resolve the hour in a 24-hour clock (i.e., pattern letter a), which is
    //missing. Working ex.: "m' minutes past 'ha" and inputStr = "5 minutes past 9AM"
    String patternmh = "m' minutes past 'h";
    //The pattern letter M is interpreted correctly as month of the year (value 5).
    //Matching the pattern letter h is the problem, as explained for (a).
    String patternMh = "M' minutes past 'h";
    //The pattern letter a cannot be resolved from the input string, as an AM/PM
    //marker is missing in the input string.
    String patternmha = "m' minutes past 'ha";
    //The pattern letter a cannot be resolved from the input string, as an AM/PM
    //marker is missing in the input string.
    String patternmHa = "m' minutes past 'Ha";
    //The parse succeeds, with the LocalTime object having the value 09:05.
    //Formatting this object with the formatter results in the output string: 5 minutes
    //past 9.
    String patternmH = "m' minutes past 'H";
    //The letter pattern mm cannot be resolved, as the minutes value has only one
    //digit (i.e., 5) in the input string.
    String patternmmH = "mm' minutes past 'H";
    //The parse succeeds, with the resulting LocalTime object having the value
    //09:00. The month value 5 is ignored. Formatting this object with the formatter
    //results in an UnsupportedTemporalTypeException, because now the
    //pattern letter M requires a month value, which is not part of a LocalTime object
    String patternMH = "M' minutes past 'H";

    String inputStr = "5 minutes past 9";
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(patternmh);
    LocalTime time;
    //time = LocalTime.parse(inputStr, formatter); //DateTimeParseException: Text '5 minutes past 9' could
    //System.out.println(time);
    //System.out.println(time.format(formatter));
    // not be parsed: Unable to obtain LocalTime from TemporalAccessor: {HourOfAmPm=9, MinuteOfHour=5},ISO of type

    formatter = DateTimeFormatter.ofPattern(patternMh); //DateTimeParseException
    //time = LocalTime.parse(inputStr, formatter);

    formatter = DateTimeFormatter.ofPattern(patternmha); //DateTimeParseException
    //time = LocalTime.parse(inputStr, formatter);

    formatter = DateTimeFormatter.ofPattern(patternmHa); //DateTimeParseException
    //time = LocalTime.parse(inputStr, formatter);

    formatter = DateTimeFormatter.ofPattern(patternmH);
    time = LocalTime.parse(inputStr, formatter);
    System.out.println(time); //09:05
    System.out.println(time.format(formatter)); //5 minutes past 9

    formatter = DateTimeFormatter.ofPattern(patternmmH); //DateTimeParseException
    //time = LocalTime.parse(inputStr, formatter);

    formatter = DateTimeFormatter.ofPattern(patternMH);
    time = LocalTime.parse(inputStr, formatter);
    System.out.println(time); //09:00
    //System.out.println(time.format(formatter)); //UnsupportedTemporalTypeException: Unsupported field: MonthOfYear
  }
}

/**
 *
 * Time and Date можно делать format с ofLocalizedDateTime.
 *
 * (a): The formatter will format a LocalTime object, or the time part of a
 * LocalDateTime object, but not a LocalDate object, as it knows nothing about
 * formatting the date part.
 * (b): The formatter will format a LocalDate object, or the date part of a
 * LocalDateTime object, but not a LocalTime object, as it knows nothing about
 * formatting the time part.
 * (c): The formatter will format a LocalDateTime object, but not a LocalDate
 * object or a LocalTime object, as it will format only temporal objects with both
 * date and time parts.
 * The program throws a
 * java.time.temporal.UnsupportedTemporalTypeException in all
 * cases.
 */
class Q16 {
  public static void main(String[] args) {
    DateTimeFormatter dtfTime = DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT);
    DateTimeFormatter dtfDate = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT);
    DateTimeFormatter dtfDateTime = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT);

    String timeStr = LocalTime.NOON.format(dtfTime);
    System.out.println(timeStr); //12:00 PM

    String dateStr = LocalDate.of(2015, 12, 24).format(dtfDate);
    System.out.println(dateStr); //12/24/15

    String dateTimeStr = LocalDateTime.of(2015, 12, 24, 12, 0).format(dtfDateTime); //dtfDate и dtfTime OK
    System.out.println(dateTimeStr); //12/24/15 12:00 PM


  }
}