package com.ocajp.webinar5datetime;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

/**
 * Все классы localdata не имеют публичных конструкторов
 * Immutable java.time
 */
public class Webinar5 {
  public static void main(String[] args) {

    //LocalDate ld = new LocalDate(); //'LocalDate(int, int, int)' has private access in 'java.time.LocalDate'

    System.out.println(LocalTime.MIN); //00:00
    System.out.println(LocalTime.MAX); //23:59:59.999999999
    System.out.println(LocalTime.MIDNIGHT); //00:00
    System.out.println(LocalTime.NOON); //12:00

    System.out.println(LocalDate.now()); //2024-02-02
    System.out.println(LocalTime.now()); //21:19:54.258
    System.out.println(LocalDateTime.now()); //2024-02-02T21:19:54.258

    System.out.println("\nLocalDate.of");
    LocalDate localDate1 = LocalDate.of(2015, Month.JANUARY, 20);
    System.out.println(localDate1); //2015-01-20
    LocalDate localDate2 = LocalDate.of(2015, 1, 20);
    System.out.println(localDate2); //2015-01-20

    System.out.println("\nLocalTime");
    LocalTime localTime1 = LocalTime.of(6, 15); //hour and min
    System.out.println(localTime1); //06:15
    LocalTime localTime2 = LocalTime.of(6, 15, 30); //hour and min + seconds
    System.out.println(localTime2); //06:15:30
    LocalTime localTime3 = LocalTime.of(6, 15, 30, 200); //hour and min + seconds and nanoseconds
    System.out.println(localTime3); //06:15:30.000000200

    System.out.println("\nLocalDateTime");
    LocalDateTime dateTime1 = LocalDateTime.of(2015, Month.JANUARY, 20, 6,  15, 30);
    System.out.println(dateTime1); //2015-01-20T06:15:30
    LocalDateTime dateTime2 = LocalDateTime.of(localDate1, localTime1);
    System.out.println(dateTime2); //2015-01-20T06:15

    System.out.println("\nErrors");
    //LocalTime.of(2015, 1, 20, 6,  15); //Cannot resolve method 'of(int, int, int, int, int)' лишние дни
    //LocalDate.of(2015, 1, 20, 6,  15); //Cannot resolve method 'of(int, int, int, int, int)' лишние часы
    //LocalDateTime.of(2015, 1, 20); //Cannot resolve method 'of(int, int, int)' не хватает времени
    //System.out.println(LocalTime.of(2015, 1, 20)); //.DateTimeException: Invalid value for HourOfDay (valid values 0
    // - 23): 2015
    //System.out.println(LocalDate.of(1, 1, 32));//DateTimeException: Invalid value for DayOfMonth (valid values 1 -
    // 28/31): 32

    //LocalTime.of(120, 12); //DateTimeException: Invalid value for HourOfDay (valid values 0 - 23): 120
    //LocalTime.of(9986545781, 12); //Integer number too large

    System.out.println("\n");

    LocalDate date = LocalDate.of(2014, Month.JANUARY, 20);
    System.out.println(date); //2014-01-20
    date = date.plusDays(2);
    date.plusDays(2); //Result of 'LocalDate.plusDays()' is ignored
    System.out.println(date); //2014-01-22
    date = date.plusWeeks(1);
    System.out.println(date); //2014-01-29
    date = date.plusMonths(1);
    System.out.println(date); //2014-02-28
    date = date.minusYears(5);
    System.out.println(date); //2009-02-28
  }
}

/**
 * Period не поддерживает цепочки, надо использовать Period.of(1, 0, 7);
 * plus() работает с LocalDate и LocalDateTime
 * time.plus(period) //UnsupportedTemporalTypeException
 */
class Periods {
  public static void main(String[] args) {
    Period annually = Period.ofYears(1); //every 1 year
    System.out.println(annually); //P1Y
    Period quarterly = Period.ofMonths(3); //every 3 month
    System.out.println(quarterly); //P3M
    Period everyThreeWeeks = Period.ofWeeks(3); //every 3 weeks
    System.out.println(everyThreeWeeks); //P21D
    Period everyOtherDay = Period.ofDays(2); //every 2 days
    System.out.println(everyOtherDay); //P2D
    Period everyYearAndAWeek= Period.of(1, 0, 7); //every year and 7 days /YYYY/MM/DD
    System.out.println(everyYearAndAWeek); //P1Y7D

    Period period6 = Period.ofDays(-15);
    System.out.println(period6); //P-15D
    System.out.println("test");
    LocalDate start = LocalDate.of(2015, Month.JANUARY, 1);
    LocalDate end = LocalDate.of(2015, Month.MARCH, 30);
    while (start.isBefore(end)) {
      System.out.println("get " + start);
      start = start.plus(everyThreeWeeks);
    }

    LocalDate date = LocalDate.of(0, 1, 1); //0000-01-01
    System.out.println(date);
    LocalDate date2 = LocalDate.of(0, 0, 0);// Invalid value for DayOfMonth (valid values 1 - 28/31): 0
    System.out.println(date2); //DateTimeException: Invalid value for MonthOfYear (valid values 1 - 12): 0
    LocalTime time = LocalTime.of(0, 0, 0);
    System.out.println(time);
    time.plus(everyOtherDay); //.UnsupportedTemporalTypeException: Unsupported unit: Days
  }
}

class Getters {
  public static void main(String[] args) {
    LocalDate date = LocalDate.of(2020, Month.JANUARY, 20);
    System.out.println(date.getDayOfWeek()); //MONDAY
    System.out.println(date.getMonth()); //JANUARY
    System.out.println(date.getYear()); //2020
    System.out.println(date.getDayOfYear()); //20
  }
}

/**
 * Все должны начинаться с ISO_*
 */
class DateTimeFormatterClass {
  public static void main(String[] args) {
    LocalDate date = LocalDate.of(2020, Month.JANUARY, 20);
    LocalTime time = LocalTime.of(11, 12, 34);
    LocalDateTime dateTime = LocalDateTime.of(date, time);
    System.out.println(date.format(DateTimeFormatter.ISO_LOCAL_DATE)); //2020-01-20
    System.out.println(time.format(DateTimeFormatter.ISO_LOCAL_TIME)); //11:12:34
    System.out.println(dateTime.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)); //2020-01-20T11:12:34
  }
}

class Styles {
  public static void main(String[] args) {
    System.out.println("ofLocalizedDate(FormatStyle.FULL)");
    LocalDate date = LocalDate.of(2021, Month.JANUARY, 20);
    LocalTime time = LocalTime.of(11, 12, 34);
    LocalDateTime dateTime = LocalDateTime.of(date, time);
    DateTimeFormatter fullDateTime = DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL);
    System.out.println(fullDateTime.format(dateTime)); //Monday, January 20, 2020
    System.out.println(fullDateTime.format(date)); //Monday, January 20, 2020
    //System.out.println(fullDateTime.format(time)); //UnsupportedTemporalTypeException: Unsupported field: DayOfWeek
    System.out.println("ofLocalizedDate(FormatStyle.LONG)");
    DateTimeFormatter longDateTime = DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG);
    System.out.println(longDateTime.format(dateTime)); //January 20, 2020
    System.out.println(longDateTime.format(date)); //January 20, 2020
    //System.out.println(longDateTime.format(time)); //UnsupportedTemporalTypeException: Unsupported field: MonthOfYear
    System.out.println("ofLocalizedDate(FormatStyle.MEDIUM);");
    DateTimeFormatter mediumDateTime = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM);
    System.out.println(mediumDateTime.format(dateTime)); //Jan 20, 2020
    System.out.println(mediumDateTime.format(date)); //Jan 20, 2020
    //System.out.println(mediumDateTime.format(time)); //UnsupportedTemporalTypeException: Unsupported field:
    // MonthOfYear
    System.out.println("ofLocalizedDate(FormatStyle.SHORT)");
    DateTimeFormatter shortDateTime = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT);
    System.out.println(shortDateTime.format(dateTime)); //1/20/20 D/M/Y
    System.out.println(shortDateTime.format(date)); //1/20/20 D/M/Y
    //System.out.println(shortDateTime.format(time)); //UnsupportedTemporalTypeException: Unsupported field: MonthOfYear

    System.out.println("format(shortDateTime))");
    System.out.println(date.format(shortDateTime));
    System.out.println(dateTime.format(shortDateTime));
    //System.out.println(time.format(shortDateTime)); //UnsupportedTemporalTypeException: Unsupported field: MonthOfYear

    /**
     * DateTimeFormatter.ofLocalizedDateTime(FormatStyle.LONG); не работает с FormatStyle.LONG, FormatStyle.FULL
     */
    System.out.println("format(ofLocalizedDateTime))");
    DateTimeFormatter mediumLocalizedDateTime = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT);
    System.out.println(dateTime.format(mediumLocalizedDateTime)); //Jan 20, 2021 11:12:34 AM MEDIUM and 1/20/21 11:12 AM
    // SHORT
    ///System.out.println(date.format(fullLocalizedDateTime)); //UnsupportedTemporalTypeException: Unsupported field:
    // ClockHourOfAmPm
    //System.out.println(time.format(fullLocalizedDateTime)); //UnsupportedTemporalTypeException: Unsupported field:
    // DayOfWeek
    DateTimeFormatter mediumLocalizedTime = DateTimeFormatter.ofLocalizedTime(FormatStyle.MEDIUM);
    System.out.println(dateTime.format(mediumLocalizedTime)); //11:12:34 AM MEDIUM
    System.out.println(time.format(mediumLocalizedTime)); //11:12 AM SHORT
    //System.out.println(date.format(mediumLocalizedTime)); //UnsupportedTemporalTypeException: Unsupported field:
    // ClockHourOfAmPm
  }
}

/**
 *  DateTimeFormatter.ofPattern(""); принимает любой кастомный формат дат и парсит в обычную дату
 */
class Parser {
  public static void main(String[] args) {
    DateTimeFormatter f = DateTimeFormatter.ofPattern("MM dd yyyy");
    LocalDate date = LocalDate.parse("01 02 2015", f); //2015-01-02
    DateTimeFormatter fReverse = DateTimeFormatter.ofPattern("mm yyyy DD");
    LocalDate reverse = LocalDate.parse("01 2015 02", fReverse);
    System.out.println(reverse);
    System.out.println(f.format(LocalDateTime.now()));
    LocalTime time = LocalTime.parse("11:22"); //11:22
    System.out.println(date);
    System.out.println(time);

    System.out.println(LocalDate.now().plus(Period.of(0,0,0)));
    System.out.println(Period.of(0, 0, 0)); //P0D
    System.out.println(LocalDate.of(2016, Month.JUNE, 13).format(DateTimeFormatter.ISO_LOCAL_DATE));
    System.out.println(LocalDate.parse("2016-06-13", DateTimeFormatter.ISO_DATE));
  }
}