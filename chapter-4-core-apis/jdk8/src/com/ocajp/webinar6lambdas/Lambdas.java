package com.ocajp.webinar6lambdas;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * This is because, for every public method in Object class, there is an implicit abstract and public method declared
 * in every interface which does not have direct super interfaces. This is the standard Java Language Specification
 * which states like this,
 *
 * “If an interface has no direct superinterfaces, then the interface implicitly declares a public abstract member
 * method m with signature s, return type r, and throws clause tcorresponding to each public instance method m with
 * signature s, return type r, and throws clause t declared in Object, unless a method with the same signature, same
 * return type, and a compatible throws clause is explicitly declared by the interface.”
 *
 * You can go through the Java language Specification 9.2 about interface members
 */
public class Lambdas {
  public static void main(String[] args) {
    List list = new ArrayList();
    /**
     *  Required type: ArrayList, provided List
     *  Cannot resolve method 'isEmpty' in 'Object'
     */
    System.out.println(check(list, al -> ((List)al).isEmpty()));

    System.out.println("---------------------------");

    List<String> bunnies = new ArrayList<>();
    bunnies.add("long ear");
    bunnies.add("floppy");
    bunnies.add("hoppy");
    System.out.println(bunnies); //[long ear, floppy, hoppy]
    bunnies.removeIf(s -> s.charAt(0) != 'h');
    System.out.println(bunnies); //[hoppy]

  }

  static String check(ArrayList list, Predicate<List> p) { //Required type: ArrayList, provided List
    return p.test(list) ? "Empty" : "Not empty";
  }

  //static String check(List list, Predicate<List> p) {
    //return p.test(list) ? "Empty" : "Not empty";
  //}

  static String check(List list, Predicate p) {
    return p.test(list) ? "Empty" : "Not empty";
  }

  //static String check(List list, Predicate<ArrayList> p) {
    //return p.test(list) ? "Empty" : "Not empty"; //Required type:ArrayList
  //}
}

class RQ12A98 {
  public static final String lock1 = "Brinks";
  private static String lock2 = "Yale";
  public static void main(String[] args) {
    Predicate<Object> p;
    //p = lock -> {boolean p = lock.equals("Master"); return p;}; //Variable 'p' is already defined in the scope
    p = lock -> {return lock.toString().equals("YALE");}; //OK
    System.out.println(p.test("YALE"));

    //p = lock -> { (args.length > 0) ? lock.equals(args[0]) : false; }; //отсутствует return

    p = lock -> { return lock.equals(lock1); };

    System.out.println(p.test("YALE"));

    p = lock -> { return lock.equals(lock2); };

    System.out.println(p.test("YALE"));

    p = lock2 -> { return lock2.equals(RQ12A98.lock2); }; //затенение lock2

    System.out.println(p.test("YALE"));

  }
}

/**
 * Локальные переменные в лямбда должны быть эффективно финальные, строки не должны меняться даже после лямбды
 */
class LocalsOnly {
  public static void main(String[] args) {
    StringBuilder banner = new StringBuilder("love ");
    LocalsOnly instance = new LocalsOnly();
    Predicate<String> p = instance.getPredicate(banner);
    System.out.println(p.test("never dies!") + " " + banner); //true love never dies!
  }

  public Predicate<String> getPredicate(StringBuilder banner) {
    List<String> words = new ArrayList<>();
    words.add("Otto");
    words.add("ADA");
    words.add("Alyla");
    words.add("Bob");
    words.add("HannaH");
    words.add("Java");

    //banner = new StringBuilder(); //Not effectively final
    //words = new ArrayList<>(); //Not effectively final

    return str -> {
      //String banner = "Don't redeclare me!"; //Variable 'banner' is already defined in the scope
      //String[] words = new String[6]; //Variable 'words' is already defined in the scope
      System.out.println("List: " + words); //Variable used in lambda expression should be final or effectively final
      /**
       * love never dies!
       */
      banner.append(str); //Variable used in lambda expression should be final or effectively final
      return str.length() > 5;
    };
  }
}

/**
 * Локальные переменные в лямбда должны быть эффективно финальные, строки не должны меняться даже после лямбды
 *
 * prints Gav says loudly that she is black
 */
class Crow {
  private String color;
  public void caw(String name) {
    String volume = "loudly";
    //name = "Caty"; //Variable used in lambda expression should be final or effectively final
    color = "black";

    /**
     * Consumer потребляет но ничего не возвращает, void
     * Supplier наоборот, ничего не принимает, что-то возвращает
     */
    Consumer<String> consumer = s -> System.out.println(name + " says " + volume + " that she is " + color);
    //volume = "softly"; //Variable used in lambda expression should be final or effectively final
    consumer.accept("no words");
  }

  public static void main(String[] args) {
    Crow crow = new Crow();
    crow.caw("Gav");
  }
}

/**
 * prints Gav says loudly that she is null
 */
class CrowFinal {
  private String color;
  public void caw(final String name) {
    final String volume = "loudly";

    /**
     * Consumer потребляет но ничего не возвращает, void
     * Supplier наоборот, ничего не принимает, что-то возвращает
     */
    Consumer<String> consumer = s -> System.out.println(name + " says " + volume + " that she is " + color);
    consumer.accept("no words");
  }

  public static void main(String[] args) {
    CrowFinal crow = new CrowFinal();
    crow.caw("Gav");
  }
}

@FunctionalInterface
interface Funky1 {
  void absMethod1(String s);
}
@FunctionalInterface
interface Funky2 {
  String absMethod2(String s);
}
class RQ12A99 {
  public static void main(String[] args) {
    Funky1 p1;
    p1 = s -> System.out.println(s); //void
    p1 = s -> s.length(); //здесь void метод может игнорировать возвращаемый тип int
    p1 = s -> s.toLowerCase(); //здесь void метод может игнорировать возвращаемый тип String
    p1 = s -> { s.toUpperCase();}; //void
    //p1 = s -> { return s.toUpperCase();}; //Unexpected return value т.к. метод возвращает void

    Funky2 p2;
    //p2 = s -> System.out.println(s); //Bad return type in lambda expression: void cannot be converted to String
    // p2 = s -> s.length(); //Bad return type in lambda expression: int cannot be converted to String
    p2 = s -> s.toUpperCase();
    //p2 = s -> { s.toUpperCase();}; //Missing return statement
    p2 = s -> { return s.toUpperCase();};

    //System.out.println(p1.absMethod1("Test")); //Cannot resolve method 'println(void)'
    p1.absMethod1("Test");
    System.out.println(p2.absMethod2("Test")); //TEST
  }
}