package com.ocajp;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class BigDecimalTest {
  public static void main(String[] args) {
    BigDecimal value = new BigDecimal(1.111);
    BigDecimal div = new BigDecimal(3);
    System.out.println("" + value.divide(div, MathContext.DECIMAL32).setScale(0, RoundingMode.UP));
    value = value.setScale(0, BigDecimal.ROUND_UP);
    System.out.println("" + value);
  }
}
