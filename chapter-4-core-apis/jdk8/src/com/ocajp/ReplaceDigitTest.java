package com.ocajp;

public class ReplaceDigitTest {
  public static void main(String[] args) {
    String str1 = "2222222";
    String str2 = "- говорит Пушкин";
    String str3 = "Домашний кинотеатр";
    String str4 = "English кинотеатр";

    if (!Character.isLetter(str1.charAt(0))) {
      String replace = "A_" + str1;
      System.out.println(replace);
    }

    if (!Character.isLetter(str2.charAt(0))) {
      String replace = "A_" + str2;
      System.out.println(replace);
    }

    if (!Character.isLetter(str3.charAt(0))) {
      String replace = "A_" + str3;
      System.out.println(replace);
    } else {
      System.out.println(str3);
    }
    System.out.println(Character.isLetter(str4.charAt(0))); // true
  }
}
