package com.ocajp.webinar4arraylist;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArrayListTopic {
  public static void main(String[] args) {
    ArrayList<Integer> integers1 = new ArrayList();
    ArrayList<Integer> integers = new ArrayList<>();
    integers.add(10); integers.add(100); integers.add(1000);

    ArrayList<String> strings = new ArrayList<>();
    System.out.println(strings); //[]
    //System.out.println(strings.get(0)); //IndexOutOfBoundsException: Index: 0, Size: 0

    /**
     * RefType ok компилируется успешно
     */
    ArrayList<String> strings2 = new ArrayList(integers); //не надо <> нас это не вознует,
    // а new ArrayList<>(integers) уже не пройдет
    System.out.println(strings2.get(0)); //ClassCastException: java.lang.Integer cannot be cast to java.lang.String

    //ArrayList<Object> objects = new ArrayList<String>(); //Required type: ArrayList<Object>

  }
}

class ArrAsList {
  public static void main(String[] args) {
    List<String> listOld = new ArrayList<>();
    listOld.add("hawk");
    listOld.add("robin");
    Object[] objectArray = listOld.toArray();
    String[] stringArray = listOld.toArray(new String[5]); //Required type: String[] //[hawk, robin, null, null, null]
    System.out.println(Arrays.toString(stringArray));
    stringArray = listOld.toArray(new String[0]); //Required type: String[] //[hawk, robin]
    System.out.println(Arrays.toString(stringArray));
    listOld.clear();
    System.out.println(listOld.size());
    System.out.println(objectArray.length);
    System.out.println(stringArray.length);

    System.out.println();
    String[] array = {"hawk", "robin"};
    List<String> list1 = Arrays.asList(array);
    System.out.println(list1.size());
    list1.set(1, "test");
    array[0] = "new";
    System.out.println(Arrays.toString(array));
    list1.remove(1); //.UnsupportedOperationException: Index: 1, Size: 0 //потому что это прототип array под капотом
    List<String> list2 = Arrays.asList("one", "two");

  }
}

/**
 * Удаление мб по индексу и по объекту.
 * Для корректного удаления объектов из листа требуется переопределить equals у объекта
 * Удаление StringBuilder из листа по значению строки, это проблема.
 */
class Del {
  public static void main(String[] args) {
    List<String> birds = new ArrayList<>();
    birds.add("hawk");
    birds.add("hawk");
    System.out.println(birds.remove("cardinal")); //false
    System.out.println(birds.remove("hawk")); //true
    System.out.println(birds.remove(0)); //hawk
    System.out.println(birds); //[]

    List<Integer> integers = new ArrayList<>();
    integers.add(10); integers.add(20); integers.add(1);
    System.out.println(integers.remove(1)); //20
    System.out.println(integers.remove(new Integer(0)));// false //удалит не по индексу а по объекту //'List<String>'
    // may not contain objects of type 'Integer'
    System.out.println(integers); //[10, 1]
    //integers.add((byte) 1); //Required type: Integer
  }
}

/**
 * Во время итерации по foreach нельзя удалять. Но StringBuilder.append работает
 * В цикле for можно удалять.
 */
class BeautyDel {
  public static void main(String[] args) {
    List<String> list = new ArrayList<>();
    list.add("Anna");
    list.add("Ada");
    list.add("Ada");
    list.add("Bob");
    list.add("Bob");
    list.add("Adda");
    for (String s : list) {
      //list.remove(s); //.ConcurrentModificationException
    }

    for (int i = 0; i < list.size(); /* empty */) {
      if (list.get(i).length() <= 3) {
        list.remove(i);
      } else {
        ++i;
      }
    }
    System.out.println(list); //[Anna, Adda]
  }
}

class IndexDelShift {
  public static void main(String[] args) {
    List<String> list = new ArrayList<>();
    list.add("Anna");
    list.add("Ada");
    list.add("Bob");
    list.add("Bob");
    for (int i = 0; i < list.size(); ++i) {
      if (list.get(i).equals("Bob")) {
        list.remove(i);//после удаления происходит сдвиг второго Боба влево, итого 3 элемента, ++i 3 индекс уже не
        // меньше list.size()
      }
    }
    System.out.println(list); //[Anna, Ada, Bob]
  }
}

class Set {
  public static void main(String[] args) {
    List<Integer> integers = new ArrayList<>(10);
    System.out.println(integers);
    integers.add(10); integers.add(20); integers.add(1);
    System.out.println(integers);
    System.out.println(integers.set(0, 11)); //10 обновил по индексу
    //System.out.println(integers.set(4, 11)); //IndexOutOfBoundsException: Index: 4, Size: 3
    System.out.println(integers);
  }
}


class Clear {
  public static void main(String[] args) {
    List<Integer> integers = new ArrayList<>(100);
    integers.add(10); integers.add(20); integers.add(1);
    System.out.println(integers);
    integers.clear();
    System.out.println(integers);
  }
}

/**
 * contain имеет ту же проблему что equal, нужно переопределять
 */
class Contain {
  public static void main(String[] args) {
    List<String> list = new ArrayList<>();
    list.add("hello");
    System.out.println(list.contains(111)); //'List<String>' may not contain objects of type 'Integer'
  }
}

/**
 * Сравнивает списки в том же порядке должны быть true
 */
class Eq {
  public static void main(String[] args) {
    List<String> strings1 = new ArrayList<>();
    List<String> strings2 = new ArrayList<>();
    System.out.println(strings1.equals(strings2)); //true
  }
}