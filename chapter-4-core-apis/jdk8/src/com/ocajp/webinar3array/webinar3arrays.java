package com.ocajp.webinar3array;

import java.util.Arrays;

/**
 * Длина верхнего измерения должна быть заполнена
 * String[] strMultiArray[] = new String[2][];
 * , () не мешают компиляции при объявлении массива
 * ClassCastException возвращается если невозможно отсортировать массив
 */
public class webinar3arrays {
  public static void main(String[] args) {
    Integer i = 2;
    String[] strArray = new String[i]; //NPE if Integer is null
    String[] strMultiArray[] = new String[2][];
    //String[] strMultiArrayNotAllowed[] = new String[][2]; //allowed

    Topping[] pizzaToppings = {new Topping(), new Topping(),};
    System.out.println(pizzaToppings.length);

    //int[][] arr2D = {{ 0, 1, 2, 3} {4, 5}};//',' expected
    int[][] arr2D = new int[2][2];
    arr2D[0][0] = 0;
    arr2D[0][1] = 1;
    arr2D[1][0] = 2;
    arr2D[1][1] = 3;
    System.out.println(Arrays.deepToString(arr2D)); //[[0, 1], [2, 3]]
    //int[][][] arr3D = {{0, 1}, {2, 3}, {4, 5}}; //arr2D
    int[] arr = {(0), (1), (2), (3), (4)};
    int[][][] arr3D = new int[2][2][2];
    arr3D[0][0] = arr3D[0][1] = arr;
    arr3D[1][0] = arr3D[0][1] = arr;
    System.out.println(Arrays.deepToString(arr3D)); //[[[0, 1, 2, 3, 4], [0, 1, 2, 3, 4]], [[0, 1, 2, 3, 4], [0, 0]]]
    //int[][] arr2D2 = {0, 1}; //Required type: int[]
  }
}

class ArrayTest {
  public static void main(String[] args) {
    String[][][] array = new String[][][] {{{"a", "b", "c"}, {"d", "e", "f"}}, {{"g"}, null}, {{"y"}},
        {{"z", "Q"}, {}}};
    System.out.println(array[1][1]);
  }
}

class Topping {

}

/**
 * equals сравнивает ссылки в Arrays
 * deepEquals сравнивает начинки, содержимое
 * hashCode и deepHashCode также
 */
class Eq {
  public static void main(String[] args) {
    int[] i1[] = {{1, 2, 3,}, {0, 0, 0}};
    int[][] i2 = {{1, 2, 3,}, {0, 0, 0}};
    int i3[][] = new int[2][3];
    System.arraycopy(i1, 0, i3, 0, i3.length); //копирует содержание, адрес массива

    System.out.println(i1); //[[I@4554617c
    System.out.println(i2); //[[I@74a14482
    System.out.println(i3); //[[I@1540e19d

    System.out.println("System.arraycopy(i1,0,i3,0,i3.length);");
    System.out.println();
    System.out.println(Arrays.asList(i1));
    System.out.println(Arrays.asList(i2));
    System.out.println(Arrays.asList(i3));
    System.out.println("Arrays.equals(i1, i2) is " + Arrays.equals(i1, i2)); //false
    System.out.println("Arrays.equals(i1, i3) is " + Arrays.equals(i1, i3)); //true
    System.out.println("Arrays.equals(i2, i3) is " + Arrays.equals(i2, i3)); //false
    System.out.println("i2.hashCode() == i3.hashCode() is " + (Arrays.hashCode(i2) == Arrays.hashCode(i3))); //false
    System.out.println(
        "i2.deepHashCode() == i3.deepHashCode() is " + (Arrays.deepHashCode(i2) == Arrays.deepHashCode(i3))); //true
    System.out.println("Arrays.deepEquals(i1, i2) is " + Arrays.deepEquals(i1, i2)); //true //сравнивает содержимое

    int[][] array2D = {{}};
    Object arrayObj = array2D;
    //int[] ints = (int[]) arrayObj; // ClassCastException. Оживает двумерный а дали одномерный массив


    int[][] arr1 = new int[2][];
    //arr[0][0] = 0; //NPE
    arr1[0] = new int[3];
    arr1[0][0] = 0;
    System.out.println(Arrays.deepToString(arr1)); //[[0, 0, 0], null]

    int[][] arr2 = new int[2][0];
    System.out.println(Arrays.deepToString(arr2)); //[[], []]
    arr2[0][0] = 0; //AIOOBE 0
    System.out.println(arr2[0].length); //0
    arr2[0] = new int[3];
    arr2[0][0] = 0;
    System.out.println(arr2[0].length); //3
  }
}