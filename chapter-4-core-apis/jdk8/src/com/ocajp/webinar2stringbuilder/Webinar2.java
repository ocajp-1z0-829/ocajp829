package com.ocajp.webinar2stringbuilder;

/**
 * StringBuilder equals сравнивает ссылки на объект, не переопределяет его из класса Object
 * Сравнивать нужно sb1.toString().equals(sb2.toString())
 * StringBuilder null не принимает
 */
public class Webinar2 {
  public static void main(String[] args) {
    Number n = 1.;
    //Double d = 1; //Provided: int
    Double d = 1.;
    System.out.println("Ковариантные операнды: " + (n == d)); //ковариантные операнды
    System.out.println("Ковариантные операнды: " + (n.equals(d))); //ковариантные операнды
    System.out.println("Ковариантные операнды: " + (n.equals(1.f))); //ковариантные операнды
    double dd = 1;
    //Double d2 = 1.f; //Provided: float
    double dd2 = 1.f;

    Number number = -127;
    Integer integer = -127;
    Byte b = 1;
    System.out.println("Ковариантные операнды целые: " + (number == integer)); //true так как в pool -128... 127
    number = (byte) 1;
    System.out.println("Ковариантные операнды целые: " + (number == b));  //true так как в pool -128... 127

    StringBuilder one = new StringBuilder();
    StringBuilder two = new StringBuilder();
    System.out.println(one == two);//false
    System.out.println(one.equals(two));//false
    System.out.println(one.toString().equals(two.toString())); //true
    StringBuilder three = one.append("a");
    System.out.println(one.toString().equals(two.toString())); //false
    System.out.println(one == three);
    String s = "a";
    //System.out.println(s == three); //Operator '==' cannot be applied to 'java.lang.String', 'java.lang.StringBuilder'


    String str = "_";
    StringBuilder stringBuilder = new StringBuilder('Z');
    System.out.println("Capacity: " + stringBuilder.capacity() + " " + stringBuilder + ";");
    System.out.println(str = (str.replace(new StringBuilder('Z'), new StringBuilder("^")))); //нашел "" и заменил на ^

    stringBuilder.append(str);
    String str2 = "Test insert";
    //stringBuilder.insert(5, str2, 2, 100); //Exception IndexOutOfBoundsException: dstOffset 5
    stringBuilder.insert(2, str2, 5, 11);
    System.out.println(stringBuilder);
    stringBuilder.replace(3, 100, ""); //отличается от метода в String, к концу прикрепляется
    System.out.println(stringBuilder);

  }


}
class TheFifthElement {
  public static void main(String[] args) {
    StringBuilder stringBuilder = new StringBuilder("1");
    stringBuilder.append("2");
    stringBuilder.append("3");
    stringBuilder.append("4");
    System.out.println(stringBuilder);
    //stringBuilder.replace(3, 2, "Leeloo"); //StringIndexOutOfBoundsException: start > length()
    stringBuilder.replace(4, 4, "Leeloo");
    stringBuilder.replace(4, 100, "Leeloo");
    System.out.println(stringBuilder);
    System.out.println(stringBuilder.reverse());
  }
}

class Capacity {
  public static void main(String[] args) {
    String java = "Java"; //4
    StringBuilder stringBuilder = new StringBuilder(java); //capacity = default 16 + 4
    System.out.println("Емкость: " + stringBuilder.capacity()); //20
    stringBuilder.ensureCapacity(32); //capacity = (stringBuilder.capacity() * 2) + 2, if (42 > 32) ? 42 : 32
    System.out.println("Емкость: " + stringBuilder.capacity()); //42
    System.out.println("Длина: " + stringBuilder.length()); //4
    stringBuilder.setLength(2); // Все что после 2 символов заменяется на '\u0000'
    System.out.println("Огрызок: " + stringBuilder); //Ja
    System.out.println("Емкость: " + stringBuilder.capacity()); //42
    System.out.println("Длина: " + stringBuilder.length()); //2
    stringBuilder.trimToSize(); //убрать незаполненные capacity
    System.out.println("Емкость: " + stringBuilder.capacity()); //2

    stringBuilder.replace(2, 100, "Empty"); //JaEmpty
    System.out.println("Емкость: " + stringBuilder.capacity()); //7
    System.out.println(stringBuilder);
    stringBuilder.delete(2, 100); //Ja
    System.out.println("Емкость: " + stringBuilder.capacity()); //7
    System.out.println(stringBuilder);

    String str = null;
    StringBuilder sb = new StringBuilder(str += str); //16 + 4 + 4 = 24
    sb.delete(0, sb.length());
    System.out.println(sb.capacity());
  }


}

class AnoherExam {
  public static void main(String[] args) {
    StringBuilder sb = new StringBuilder("Passed");
    System.out.print(sb + ": "); //Passed:
    System.out.println(sb.replace(0,4,"Fail") == sb.delete(0,666).insert(0, "Failed")); //true
  }
}

class TestNull {
  public static void main(String[] args) {
    StringBuilder sb = new StringBuilder();
    //sb.append(null); //Ambiguous method call. Both
    //sb.append((char[]) null); //NPE
    System.out.println(sb);
    char[] chars = new char[0];
    chars = null;
    //sb.append(chars); //NPE
    System.out.println(sb);
    sb.append((String) null);
    System.out.println(sb); //null

    Object obj = null;
    sb.append(obj);
    System.out.println(sb);
    System.out.println(sb.length());
  }
}

class Concat {
  public static void main(String[] args) {
    int[] arr = {1,2,3,4,5};
    String str = null;
    for (int e: arr) {str += e;} //String concatenation '+=' in loop
    System.out.println(str); //null12345
  }
}

class Eq {
  public static void main(String[] args) {
    StringBuilder sb1 = new StringBuilder("1Z0-808");
    String str1 = sb1.toString();
    String str2 = str1;
    String str3 = new String(str1); //новый объект в хипе
    String str4 = sb1.toString(); //новый объект в хипе
    String str5 = "1Z0-808"; // String pool
    System.out.println(str1 == str2);
    System.out.println(str1 == str3);
    System.out.println(str1 == str4);
    System.out.println(str1 == str5);
  }
}