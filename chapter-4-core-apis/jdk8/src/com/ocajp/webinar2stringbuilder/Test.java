package com.ocajp.webinar2stringbuilder;

public class Test {
  public static void main(String[] args) {
    //StringBuilder sb1 = new StringBuilder(new String(256)); //Cannot resolve constructor 'String(int)'
    //StringBuilder sb2 = StringBuilder.setCapacity(256); //это не статика
    //StringBuilder sb3 = StringBuilder.setLength(256); //это не статика
    StringBuilder sb4 = new StringBuilder(256); //capacity = 256
    sb4.delete(0, sb4.length());
    sb4.insert(0, "Hello ");
    //sb4.insert(" Reloaded", 1); //comperr не там индекс
  }
}

class AccountMasker {
  public static void main(String[] args) {
    String accNum = "12345 12345 12345 13579";
    System.out.println(maskAccount(accNum.replace(" ", "")));
  }

  private static String maskAccount(String accNum) {
    String lead = "***************";

    //return (((new StringBuilder(accNum)))).substring(15, 20).toString(); //13579

    //return lead + accNum.substring(15,20); //***************13579

    /**
     * Вставляемая строка - lead
     * 1 - индекс с какого вставляем
     * 2 - длина
     * [1, 2)
     */
    //return new StringBuilder(accNum).append(lead, 1,2).toString(); //12345123451234513579
    //return new StringBuilder(accNum).append(lead, 15,20).toString(); //exception, start 15, end 20, s.length() 15

    //return new StringBuilder(accNum).insert(0, lead).toString(); //***************12345123451234513579
    return new StringBuilder(lead).append(accNum, 15, 20).toString(); //***************13579
  }
}
