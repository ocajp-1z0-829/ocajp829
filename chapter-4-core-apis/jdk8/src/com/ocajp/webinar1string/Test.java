package com.ocajp.webinar1string;

import com.ocajp.webinar1string.test.SecretAgent;

public class Test extends SecretAgent {
  public static void main(String[] args) {
    String s1 = "purr";
    String s2 = "";

    s1.toUpperCase();
    s1.trim();
    s1.substring(1, 3);
    s1 += "two";

    s2 += 2;
    s2 += 'c';
    s2 += false;

    if (s2 == "2cfalse") {
      System.out.println("==");
    }

    if (s2.equals("2cfalse")) {
      System.out.println("equals");
    }

    System.out.println(s1.length());

    System.out.println(agentCountProtected);

    Test test = new Test();

    char c = 97;
    System.out.println(c);
    //System.out.println("Hello".charAt(c)); //String index out of range: 97




  }

  Test() {
    System.out.println(new String()); //empty
    //new String(1); //Cannot resolve constructor 'String(int)'
    System.out.println(new String(new char[] {'a', 'b'})); //ab
    System.out.println(new String("ab")); //ab

    CharSequence charSequence = new String("ab");
    //new String(charSequence); //Cannot resolve constructor 'String(java.lang.CharSequence)'
    StringBuilder stringBuilder = new StringBuilder();
    System.out.println(new String(stringBuilder)); //empty
    //new String('a'); //Cannot resolve constructor 'String(char)'
    //new String(new Object()); //Cannot resolve constructor 'String(java.lang.Object)'
    //new String("ab", 0, 1); //Cannot resolve constructor 'String(java.lang.String, int, int)'
    System.out.println(new String(new char[]{'a', 'b'}, 0, 1)); //a

  }
}
