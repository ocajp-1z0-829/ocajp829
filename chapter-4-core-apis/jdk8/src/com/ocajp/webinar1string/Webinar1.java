package com.ocajp.webinar1string;

public class Webinar1 {
  public static void main(String[] args) {
    String s1 = "Hello";
    String s2 = "Hello";
    System.out.println(s1 + " " + s2);

    s1 = s1.concat(s2);
    System.out.println(s1 + " " + s2);

    new String();
    System.out.println();
    test();
  }

  public static void test() {
    String hello = "Hello", lo = "lo";
    System.out.print((hello == "Hello") + " "); //true
    System.out.print((OtherOutter.hello == hello) + " "); //true
    System.out.print((Other.hello == hello) + " "); //true
    OtherOutter other = new OtherOutter();
    System.out.print((OtherOutter.Other.hello == hello) + " "); //true
    System.out.print((hello == ("Hel"+"lo")) + " "); //true
    System.out.print((hello == ("Hel"+lo)) + " "); //false
    System.out.println(hello == ("Hel"+lo).intern()); //true
  }
}

class OtherOutter {
  static String hello = "Hello";

  static class Other {
    static String hello = "Hello";
  }
}

class Other { static String hello = "Hello"; }

