package com.ocajp.jdk17;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexTest {
  public static void main(String[] args) {
    String str = "Кот Собака <a href=\"http://Test\">ссылке</a>";
    Pattern pattern = Pattern.compile("\\<.*\\>(.*?)\\</a\\>");
    Matcher matcher = pattern.matcher(str);
    if (matcher.find())
    {
      System.out.println(matcher.group(1));
    }
  }
}
