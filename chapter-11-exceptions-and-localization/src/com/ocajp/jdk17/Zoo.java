package com.ocajp.jdk17;

import java.io.Closeable;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.ResourceBundle;

public class Zoo {
  public static void printWelcomeMessage(Locale locale) {
     var rb = ResourceBundle.getBundle("Zoo", locale);
     System.out.println(rb.getString("hello")
         + ", " + rb.getString("open"));
    String format = rb.getString("helloByName");
    System.out.print(MessageFormat.format(format, "Tammy", "Henry"));
     }
 public static void main(String[] args) {
     var us = new Locale("en", "US");
     var france = new Locale("fr", "FR");
     printWelcomeMessage(us); // Hello, The zoo is open
     printWelcomeMessage(france); // Bonjour, Le zoo est ouvert
     }

}

class RB {
  public static void main(String[] args) {
    var us = new Locale("en");
    Locale.setDefault(us);
    System.out.println(Locale.getDefault());
    ResourceBundle rb = ResourceBundle.getBundle("Zoo");
    rb.keySet().stream()
        .map(k -> k + ": " + rb.getString(k))
        .forEach(System.out::println);
  }
}

class FamilyCar {
  static class Door implements AutoCloseable {
    public void close() {
      System.out.print("D");
    } }
  static class Window implements Closeable {
    public void close() {
      System.out.print("W");
      throw new RuntimeException();
    } }
  public static void main(String[] args) {
    var d = new Door();
    try (d; var w = new Window()) {
      System.out.print("T");
    } catch (Exception e) {
      System.out.print("E");
    } finally {
      System.out.print("F");
    } } }

class SnowStorm {
  static class WalkToSchool implements AutoCloseable {
    public void close() {
      throw new RuntimeException("flurry");
    } }
  public static void main(String[] args) {
    WalkToSchool walk1 = new WalkToSchool();
    try (walk1; WalkToSchool walk2 = new WalkToSchool()) {
      throw new RuntimeException("blizzard"); //это RE подавляет RE из WalkToSchool
    } catch(Exception e) {
      System.out.println(e.getMessage()
          + " " + e.getSuppressed().length); //blizzard 2
      for (Throwable s : e.getSuppressed()) {
        System.out.println(s.getMessage()); //flurry flurry
      }
    }
  } }

record Wallet(double money) {
  private String openWallet() {
    Locale.setDefault(Locale.Category.DISPLAY,
        new Locale.Builder().setRegion("us").build());
    String format = NumberFormat.getCurrencyInstance().format(money);
    Locale.setDefault(Locale.Category.FORMAT,
        new Locale.Builder().setLanguage("en").build());
    format = NumberFormat.getCurrencyInstance().format(money);
    format = NumberFormat.getCurrencyInstance(Locale.GERMANY)
        .format(money);
    return format;
  }
  public void printBalance() {
    System.out.println(openWallet());
  }
  public static void main(String... unused) {
    new Wallet(2.4).printBalance();
    transform("Test");
  }
  static void rollOut() throws ClassCastException {}
  public static void transform(String c) {
    try {
      rollOut();
    } catch (IllegalArgumentException | Error | NullPointerException | ClassCastException f) {
    }
  }
}