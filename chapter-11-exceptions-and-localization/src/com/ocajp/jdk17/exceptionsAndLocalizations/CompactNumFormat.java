package com.ocajp.jdk17.exceptionsAndLocalizations;

import java.text.NumberFormat;
import java.text.NumberFormat.Style;
import java.util.Locale;
import java.util.stream.Stream;

public class CompactNumFormat {
  public static void main(String[] args) {
    var formatters = Stream.of(
        NumberFormat.getCompactNumberInstance(), //7M
        NumberFormat.getCompactNumberInstance(Locale.getDefault(), NumberFormat.Style.SHORT), //7M
        NumberFormat.getCompactNumberInstance(Locale.getDefault(), NumberFormat.Style.LONG), //7 million
        NumberFormat.getCompactNumberInstance(Locale.GERMAN, Style.SHORT), //7 Mio.
        NumberFormat.getCompactNumberInstance(Locale.GERMAN, Style.LONG), //7 Millionen
        NumberFormat.getNumberInstance()); //7,123,456
    //formatters.map(s -> s.format(7_123_456)).forEach(System.out::println);

    /**
     * 315M
     * 315M
     * 315 million
     * 315 Mio.
     * 315 Millionen
     * 314,900,000
     */
    //formatters.map(s -> s.format(314_900_000)).forEach(System.out::println);

    formatters.map(s -> s.format(3_000)).forEach(System.out::println);

  }
}
