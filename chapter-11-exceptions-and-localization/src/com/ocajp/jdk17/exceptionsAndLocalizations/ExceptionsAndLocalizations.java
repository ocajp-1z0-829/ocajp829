package com.ocajp.jdk17.exceptionsAndLocalizations;

public class ExceptionsAndLocalizations {
  public static void main(String[] args) {

  }

  public void bad() {
    try {
      eatCarrot();
      //} catch (NoMoreCarrotsException e) { // DOES NOT COMPILE Exception 'com.ocajp.jdk17.exceptionsAndLocalizations.NoMoreCarrotsException' is
    } catch (Exception e) {
      // never thrown in the corresponding try block
      System.out.print("sad rabbit");
    }
  }
  private void eatCarrot() {}

}

class NoMoreCarrotsException extends Exception {}

class CanNotHopException extends Exception {}
class Hopper {
  public void hop() throws CanNotHopException {}
}
class Bunny extends Hopper {
  public void hop() {} // This is fine
}

