package com.ocajp.jdk17.exceptionsAndLocalizations;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;

public class Test {
  public Exception ex() throws Exception {return new Exception();}
  public RuntimeException re() throws RuntimeException {return new RuntimeException();}
  public Error e() throws Error {return new Error();}

  public void whatHappensNext() throws IOException {
    System.out.println("it's ok");
    //throw new IllegalArgumentException();
    //throw new IOException();
    throw new RuntimeException();
  }

  public static void main_() {
    try {
      System.out.println("work real hard");
    } catch (IllegalArgumentException e) { //(IOException e) { //Exception 'java.io.IOException' is never thrown in the corresponding
    }  catch (StackOverflowError e) {
    } catch (RuntimeException e) {}
  }

  public static void main(String args) {
    System.out.print("a");
    try {
      System.out.print("b");
      throw new IllegalArgumentException();
    } catch (IllegalArgumentException e) {
      System.out.print("c");
      try {
        throw new RuntimeException("1");
      } catch (RuntimeException e1) {
        System.out.println("?");
      }
    } catch (RuntimeException e) {
      System.out.println("d");
      throw new RuntimeException("2");
    } finally {
      System.out.print("e");
      throw new RuntimeException("3");
    }
  }

  static int a =1;
  static int b =1;
  public static int main() {
    try {
      return a/0;
    } catch (ClassCastException e) {
      return 10;
    } catch (RuntimeException e) {
      return 20;
    } finally {
      throw new NullPointerException();
    }
  }

  public static void mainer() {
    try {
      System.out.println("try");
      return;
    } catch (ClassCastException e) {
      System.out.println("catch1");
      return;
    } catch (RuntimeException e) {
      System.out.println("catch2");
      return;
    } finally {
      System.out.println("finally");
      return;
    }
  }

  public static void main(String[] args) {
    mainer();
    System.out.println(LocalDate.parse("2022-04-30", DateTimeFormatter.ISO_LOCAL_DATE));
  }

}

/*
class Problem extends Exception {
  public Problem() {}
}
*/

class BookTest {
  public static void main(String[] args) {

  }

  public void whatHappensNext() throws IOException {
     // INSERT CODE HERE
    System.out.println("it's ok");
    //throw new IllegalArgumentException();
    //throw new java.io.IOException();
    //throw new RuntimeException();
  }
}

class Problem extends Exception {
  public Problem() {
  }
}
class YesProblem extends Problem {
}
class MyDatabase {
  public static void connectToDatabase() throws Problem {
    throw new YesProblem();
  }
  public static void main(String[] c) throws Exception {
    //connectToDatabase();
    print(100_102.2);
  }

  public static void print(double t) {
    System.out.println(NumberFormat.getCompactNumberInstance().format(t)); //100K
    System.out.println(NumberFormat.getCompactNumberInstance(Locale.getDefault(), NumberFormat.Style.SHORT).format(t)); //100K
    System.out.println(NumberFormat.getCurrencyInstance().format(t)); //$100,102.20
  }

  FileReader p = null;
  public void tryAgain(String s) {
    try (FileReader r = null; FileReader p=new FileReader("")){
      System.out.print("X");
      throw new IllegalArgumentException();
    } catch(Exception e){
      System.out.print("A");
      try {
        throw new FileNotFoundException();
      } catch (FileNotFoundException ex) {
        ex.printStackTrace();
      }
    } finally{
      System.out.print("O");
    }
  }
}

class Format {
  public static void main(String[] args) {
    String patternCorrect = "#,###,000.0#";
    String pattern1 = "#,###.0";
    String pattern2 = "0,000.0#";
    String pattern3 = "##.#";
    var message = DoubleStream.of(5.21, 8.49, 1234)
        .mapToObj(v -> new DecimalFormat(patternCorrect).format(v))
        .collect(Collectors.joining("> <"));
    System.out.println("<"+message+">"); //<005.21> <008.49> <1,234.0>
    var message2 = DoubleStream.of(5.21, 8.49, 1234)
        .mapToObj(v -> new DecimalFormat(pattern1).format(v))
        .collect(Collectors.joining("> <"));
    System.out.println("<"+message2+">"); //<5.2> <8.5> <1,234.0>
    var message3 = DoubleStream.of(5.21, 8.49, 1234)
        .mapToObj(v -> new DecimalFormat(pattern2).format(v))
        .collect(Collectors.joining("> <"));
    System.out.println("<"+message3+">"); //<0,005.21> <0,008.49> <1,234.0>
    var message4 = DoubleStream.of(5.21, 8.49, 12555555534.4555)
        .mapToObj(v -> new DecimalFormat(patternCorrect).format(v))
        .collect(Collectors.joining("> <"));
    System.out.println("<"+message4+">"); //<5.2> <8.5> <1234>
  }
}

class StuckTurkeyCage implements AutoCloseable {
  public void close() throws IOException {
    throw new FileNotFoundException("Cage not closed");
  }
  public static void main(String[] args) {
    try (StuckTurkeyCage t = new StuckTurkeyCage()) {
      var huey = (String)null;
      Integer dewey = null;
      Object louie = null;
      if(louie == huey.substring(dewey.intValue())) {
        System.out.println("Quack!");
      }
      System.out.println("put turkeys in");
    } catch (Exception e) {
      System.out.println("Hi");
      e.printStackTrace();
    }
  }
}

class Q14 {
  public static void main(String[] args) {
    var huey = (String)null;
    Integer dewey = null;
    Object louie = null;
    if(louie == huey.substring(dewey.intValue())) {
      System.out.println("Quack!"); }
  }
}

class Q15 {
  public static void main(String[] args) {
    System.out.println(new Locale.Builder().setLanguage("yw").setRegion("PM").getClass());
    System.out.println(new Locale.Builder().setLanguage("yw").setRegion("PM").build().getClass());
  }
}

class Q16 {
  public static void main(String[] args) {
    var f = DateTimeFormatter.ofPattern("hh' o''clock'");
    System.out.println(f.format(LocalDateTime.now()));
    System.out.println(f.format(LocalTime.now()));
    System.out.println(f.format(ZonedDateTime.now()));
    System.out.println((ZonedDateTime.now()));
  }
}

class Q21 {
  static class SneezeException extends Exception {}
  static class SniffleException extends SneezeException {}
  public static void main(String[] args) {
    try {
      throw new SneezeException();
    } catch (SneezeException e) {
    } finally {}
  } }

class Q22 {
  public static void main(String[] args) {
    try {
      LocalDateTime book = LocalDateTime.of(2022, 4, 5, 12, 30, 20);
      System.out.print(book.format(DateTimeFormatter.ofPattern("m"))); // minute
      System.out.print(book.format(DateTimeFormatter.ofPattern("z"))); //zone ERROR
      System.out.print(DateTimeFormatter.ofPattern("y").format(book)); // year
    } catch (Throwable e) {
    }
  }
}

class Q24 {
  static class WalkToSchool implements AutoCloseable {
    public void close() {
      throw new RuntimeException("flurry");
    }}
  public static void main(String[] args) {
    WalkToSchool walk1 = new WalkToSchool();
    try (walk1; WalkToSchool walk2 = new WalkToSchool()) {
      throw new RuntimeException("blizzard");
    } catch(Exception e) {
      System.out.println(e.getMessage()
              + " " + e.getSuppressed().length);
    }
    //walk1 = null;
  }}

record Q25(double money) {
  private String openWallet() {
    Locale.setDefault(Locale.Category.DISPLAY, new Locale.Builder().setRegion("us").build());
    Locale.setDefault(Locale.Category.FORMAT, new Locale.Builder().setLanguage("en").build());
          return NumberFormat.getCurrencyInstance(Locale.GERMANY).format(money); }
  public void printBalance() { System.out.println(openWallet());
  }
  public static void main(String... unused) { new Q25(2.4).printBalance();
  }}