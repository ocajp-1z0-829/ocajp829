package com.ocajp.jdk17.exceptionsAndLocalizations;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Locale;

public class LocalClass {

  public static void main(String[] args) {
    Locale locale = Locale.getDefault();
    System.out.println(locale);

    System.out.println(Locale.GERMAN); // de
    System.out.println(Locale.GERMANY); // de_DE

    System.out.println(new Locale("fr")); // fr
    System.out.println(new Locale("hi", "IN")); // hi_IN
    System.out.println(new Locale("ru", "RU")); // ru_RU

    Locale l1 = new Locale.Builder()
        .setLanguage("en")
        .setRegion("US")
        .build();
    Locale l2 = new Locale.Builder()
        .setRegion("US")
        .setLanguage("en")
        .build();
    System.out.println(l1 + " " + l2);

    System.out.println(Locale.getDefault()); // en_US
    Locale locale2 = new Locale("fr");
    Locale.setDefault(locale2);
    System.out.println(Locale.getDefault()); // fr
    locale2 = new Locale("en_US");
    Locale.setDefault(locale2);

    int attendeesPerYear = 3_200_000;
    int attendeesPerMonth = attendeesPerYear / 12;
    var us = NumberFormat.getInstance(Locale.US);
    System.out.println(us.format(attendeesPerMonth)); // 266,666
    var gr = NumberFormat.getInstance(Locale.GERMANY);
    System.out.println(gr.format(attendeesPerMonth)); // 266.666
    var ca = NumberFormat.getInstance(Locale.CANADA_FRENCH);
    System.out.println(ca.format(attendeesPerMonth)); // 266 666

    double price = 48;
    var myLocale = NumberFormat.getCurrencyInstance();
    System.out.println(myLocale.format(price));

    double successRate = 0.802;
    us = NumberFormat.getPercentInstance(Locale.US);
    System.out.println(us.format(successRate)); // 80%
    gr = NumberFormat.getPercentInstance(Locale.GERMANY);
    System.out.println(gr.format(successRate)); // 80 %

    String s = "40.45";
    var en = NumberFormat.getInstance(Locale.US);
    try {
      System.out.println(en.parse(s)); // 40.45
    } catch (ParseException e) {
      e.printStackTrace();
    }
    var fr = NumberFormat.getInstance(Locale.FRANCE);
    try {
      System.out.println(fr.parse(s)); // 40
    } catch (ParseException e) {
      e.printStackTrace();
    }

    System.out.println(Arrays.toString(NumberFormat.getAvailableLocales()));

    String income = "$92,807.99";
    var cf = NumberFormat.getCurrencyInstance(Locale.US);
    double value = 0;
    try {
      value = (Double) cf.parse(income);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    System.out.println(value); // 92807.99

  }
}
