package com.ocajp.jdk17.exceptionsAndLocalizations;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class Formatting {
  public static void main(String[] args) {
    double d = 1234.567;
    NumberFormat f1 = new DecimalFormat("###,###,###.0");
    System.out.println(f1.format(d)); // 1,234.6

    NumberFormat f2 = new DecimalFormat("000,000,000.00000");
    System.out.println(f2.format(d)); // 000,001,234.56700

    NumberFormat f3 = new DecimalFormat("Your Balance $#,###,###.##");
    System.out.println(f3.format(d)); // Your Balance $1,234.57

    var dt = LocalDateTime.of(2022, Month.OCTOBER, 20, 16, 15, 30);
    var zt = ZonedDateTime.of(2022, 10, 20, 16, 15, 30, 0, ZoneId.of("Moscow"));
    var formatter1 = DateTimeFormatter.ofPattern("MM/dd/yyyy hh:mm:ss");
    System.out.println(dt.format(formatter1)); // 10/20/2022 06:15:30
    var formatter2 = DateTimeFormatter.ofPattern("MM_yyyy_-_dd");
    System.out.println(dt.format(formatter2)); // 10_2022_-_20
    var formatter3 = DateTimeFormatter.ofPattern("h:mm z");
    //System.out.println(dt.format(formatter3)); // DateTimeException Unable to extract ZoneId from temporal
    // 2022-10-20T06:15:30

    System.out.println(zt.format(formatter3));

  }
}
