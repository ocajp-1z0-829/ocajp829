package com.ocajp.jdk17.exceptionsAndLocalizations;

import java.util.Properties;

public class Proper {
  public static void main(String[] args) {
    var props = new Properties();
    props.setProperty("name", "Our zoo");
    props.setProperty("open", "10am");

    System.out.println(props.getProperty("camel")); // null
    System.out.println(props.getProperty("camel", "Default value")); // Bob

    props.get("open"); // 10am
    //props.get("open", "The zoo will be open soon"); // DOES NOT COMPILE 'get(java.lang.Object)' in 'java.util
    // .Properties' cannot be applied to '(java.lang.String, java.lang.String)'

  }
}
