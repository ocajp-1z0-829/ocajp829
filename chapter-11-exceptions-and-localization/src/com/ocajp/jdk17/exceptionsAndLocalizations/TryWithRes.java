package com.ocajp.jdk17.exceptionsAndLocalizations;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

/**
 * Behind the scenes, the compiler replaces a try-with-com.ocajp.jdk17.resources block with a try and finally
 * block. We refer to this “hidden” finally block as an implicit finally block since it is created
 * and used by the compiler automatically. You can still create a programmer-defined finally
 * block when using a try-with-com.ocajp.jdk17.resources statement; just be aware that the implicit one will be
 * called first.
 */
public class TryWithRes {
  public void readFile(String file) throws IOException{
    try (FileInputStream is = new FileInputStream("myfile.txt")) {
      System.out.println(is.available());
      // Read file data
    }

    try (Scanner s = new Scanner(System.in);
         var m = new MyFileClass(1);) {
      s.nextLine();
    } catch (Exception e) {
      //s.nextInt(); // DOES NOT COMPILE
    } finally {
      //s.nextInt(); // DOES NOT COMPILE
    }
  }

  public static void main() {
    try (MyFileClass n = new MyFileClass(1);
         var m = new MyFileClass(2);) {
      System.out.println("Try Block");
    } catch (Exception e) {
      System.out.println("catch Block");
      //s.nextInt(); // DOES NOT COMPILE
    } finally {
      System.out.println("finally Block");
      //s.nextInt(); // DOES NOT COMPILE
    }
  }

  public static void main(String xyz) {
    final var bookReader = new MyFileClass(4);
    MyFileClass movieReader = new MyFileClass(5);
    try (bookReader;
         var tvReader = new MyFileClass(6);
         movieReader) {
      System.out.println("Try Block");
    } finally {
      System.out.println("Finally Block");
    }
  }

  public static void main(String[] args) throws IOException {
    var writer = new Scanner(System.in);
    writer.findInLine("This write is permitted but a really bad idea!");
    try (writer) {
      writer.locale();
    } finally {
      writer.findInLine("This write will fail!"); // IllegalStateException Scanner closed
    }
  }

}

class JammedTurkeyCage implements AutoCloseable {
  public static void main(String[] args) {
    try (JammedTurkeyCage t = new JammedTurkeyCage()) {
      System.out.println("Put turkeys in");
    } catch (IllegalStateException e) {
      System.out.println("Caught: " + e.getMessage());
    }
  }

  public void close() throws IllegalStateException {
    throw new IllegalStateException("Cage door does not close");
  }
}

class MyFileClass implements AutoCloseable {
  private final int num;
  public MyFileClass(int num) { this.num = num; }
  @Override public void close() {
    System.out.println("Closing: " + num);
  } }
