package com.ocajp.jdk17.exceptionsAndLocalizations;

import static java.time.format.FormatStyle.SHORT;

import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class DateTimeFormatterClass {
  public static void print(DateTimeFormatter dtf,
                           LocalDateTime dateTime, Locale locale) {
    System.out.println(dtf.format(dateTime) + " --- "
        + dtf.withLocale(locale).format(dateTime));
  }
  public static void main(String[] args) {
    Locale.setDefault(new Locale("en", "US"));
    var italy = new Locale("it", "IT");
    var dt = LocalDateTime.of(2022, Month.OCTOBER, 20, 15, 12, 34);
    // 10/20/22 --- 20/10/22
    print(DateTimeFormatter.ofLocalizedDate(SHORT),dt,italy);
    // 3:12 PM --- 15:12
    print(DateTimeFormatter.ofLocalizedTime(SHORT),dt,italy);
    // 10/20/22, 3:12 PM --- 20/10/22, 15:12
    print(DateTimeFormatter.ofLocalizedDateTime(SHORT,SHORT),dt,italy);
  }
}

class CategoryClass {
  public static void printCurrency(Locale locale, double money) {
    System.out.println(NumberFormat.getCurrencyInstance().format(money) + ", " + locale.getDisplayLanguage());
  }
  public static void main(String[] args) {
    var spain = new Locale("es", "ES");
    var money = 1.23;
    // Print with default locale
    Locale.setDefault(new Locale("en", "US"));
    printCurrency(spain, money); // $1.23, Spanish
    // Print with selected locale display
    Locale.setDefault(Locale.Category.DISPLAY, spain);
    printCurrency(spain, money); // $1.23, español
    // Print with selected locale format
    Locale.setDefault(Locale.Category.FORMAT, spain);
    printCurrency(spain, money); // 1,23 €, español
  }
}
