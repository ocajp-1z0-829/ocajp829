package com.ocajp.jdk17.book;

public class Test {
  public static void main(String[] args) {
    try {
      System.out.println('A');
      throw new RuntimeException("Out of bounds");
    } catch (ArrayIndexOutOfBoundsException aioobe) {
      System.out.println('B');
      //throw new Exception("Checked exception must be handled");
    } finally {
      System.out.println('C');
    }
  }
}

/*
 * // anteater
 */
// bear
// // cat
// /* dog */
/* elephant */
/*
 *  ferret */
// */
