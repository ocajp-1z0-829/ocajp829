package com.ocajp.jdk8;

@FunctionalInterface //Multiple non-overriding abstract methods found in interface com.ocajp.webinar6lambdas.StrPredicate
public interface StrPredicate {
  boolean test(String str);
  //String reverse(String str); //Multiple non-overriding abstract methods found in interface com.ocajp
  // .webinar6lambdas.StrPredicate
  default void msg(String str) {
    System.out.println(str);
  }
  static void info() {
    System.out.println("Testing");
  }
  @Override
  String toString(); //From Object
}

