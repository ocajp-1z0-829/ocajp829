package com.ocajp.jdk8;

/**
 * It's not FunctionalInterface cause overrided, default static methods can't be FunctionalInterface
 */
//@FunctionalInterface //No target method found
public interface StrFormat {
  @Override
  String toString();
}
