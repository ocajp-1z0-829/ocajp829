package com.ocajp.jdk17.functions;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * A Supplier is used when you want to generate or supply values without taking any input
 */
@FunctionalInterface
interface Supplier<T> {
  T get();
}

/**
 * The core functional interfaces in Table 8.4 are provided in the java.util.function
 * package. We cover generics in the next chapter, but for now, you just need to know that
 * <T> allows the interface to take an object of a specified type. If a second type parameter is
 * needed, we use the next letter, U. If a distinct return type is needed, we choose R for return as
 * the generic type.
 */
public class FuncInterfaces {
}

class SupplierImpl {
  public static void main(String[] args) {
    Supplier<LocalDate> s1 = LocalDate::now;
    Supplier<LocalDate> s2 = () -> LocalDate.now();
    LocalDate d1 = s1.get();
    LocalDate d2 = s2.get();
    System.out.println(d1); // 2022-02-20
    System.out.println(d2); // 2022-02-20

    Supplier<StringBuilder> sb1 = StringBuilder::new;
    Supplier<StringBuilder> sb2 = () -> new StringBuilder();
    System.out.println(sb1.get()); // Empty string
    System.out.println(sb2.get()); // Empty string

    Supplier<ArrayList<String>> s3 = ArrayList::new;
    ArrayList<String> a1 = s3.get();
    System.out.println(s3);
    System.out.println(a1); // []
  }
}

/**
 * You use a Consumer when you want to do something with a parameter but not return
 * anything. BiConsumer does the same thing, except that it takes two parameters
 */
@FunctionalInterface
interface Consumer<T> {
  void accept(T t);
  // omitted default method
}
@FunctionalInterface
interface BiConsumer<T, U> {
  void accept(T t, U u);
  // omitted default method
}
class ConsumerImpl {
  public static void main(String[] args) {
    Consumer<String> c1 = System.out::println;
    Consumer<String> c2 = x -> System.out.println(x);
    c1.accept("Annie"); // Annie
    c2.accept("Annie"); // Annie

    var map = new HashMap<String, Integer>();
    BiConsumer<String, Integer> b1 = map::put;
    BiConsumer<String, Integer> b2 = (k, v) -> map.put(k, v);

    b1.accept("chicken", 7);
    b2.accept("chick", 1);
    System.out.println(map); //{chicken=7, chick=1}
  }
}

/**
 * Predicate is often used when filtering or matching. Both are common operations. A BiPredicate
 * is just like a Predicate, except that it takes two parameters instead of one.
 */
@FunctionalInterface
interface Predicate<T> {
  boolean test(T t);
  // omitted default and static methods
}
@FunctionalInterface
interface BiPredicate<T, U> {
  boolean test(T t, U u);
  // omitted default methods
}
class PredicateImpl {
  public static void main(String[] args) {
    Predicate<String> p1 = String::isEmpty;
    Predicate<String> p2 = x -> x.isEmpty();
    System.out.println(p1.test("")); // true
    System.out.println(p2.test("")); // true

    BiPredicate<String, String> b1 = String::startsWith;
    BiPredicate<String, String> b2 =
        (string, prefix) -> string.startsWith(prefix);
    System.out.println(b1.test("chicken", "chick")); // true
    System.out.println(b2.test("chicken", "chick")); // true
    System.out.println(b1.test("Test", "est")); //false
  }
}

/**
 * A Function is responsible for turning one parameter into a value of a potentially different
 * type and returning it. Similarly, a BiFunction is responsible for turning two parameters into a
 * value and returning it
 */
@FunctionalInterface
interface Function<T, R> {
  R apply(T t);
  // omitted default and static methods
}
@FunctionalInterface
interface BiFunction<T, U, R> {
  R apply(T t, U u);
  // omitted default method
}
class FuncImpl {
  public static void main(String[] args) {
    BiFunction<String, String, String> b1 = String::concat;
    BiFunction<String, String, String> b2 =
        (string, toAdd) -> string.concat(toAdd);
    System.out.println(b1.apply("baby ", "chick")); // baby chick
    System.out.println(b2.apply("baby ", "chick")); // baby chick

    Function<String, Integer> f1 = String::length;
    Function<String, Integer> f2 = x -> x.length();
    System.out.println(f1.apply("cluck")); // 5
    System.out.println(f2.apply("cluck")); // 5
    System.out.println(f2.apply(null)); // NPE
  }
}

/**
 * UnaryOperator and BinaryOperator are special cases of a Function. They require all
 * type parameters to be the same type. A UnaryOperator transforms its value into one of the
 * same type. For example, incrementing by one is a unary operation. In fact, UnaryOperator
 * extends Function. A BinaryOperator merges two values into one of the same type. Adding two numbers is a binary
 * operation. Similarly, BinaryOperator extends BiFunction.
 */
@FunctionalInterface
interface UnaryOperator<T> extends Function<T, T> {
  //T apply(T t); // UnaryOperator
  // omitted static method
}
@FunctionalInterface
interface BinaryOperator<T> extends BiFunction<T, T, T> {
  //T apply(T t1, T t2); // BinaryOperator
  // omitted static methods
}
class UnaryAndBinaryOperatorImpl {
  public static void main(String[] args) {
    UnaryOperator<String> u1 = String::toUpperCase;
    UnaryOperator<String> u2 = x -> x.toUpperCase();
    System.out.println(u1.apply("chirp")); // CHIRP
    System.out.println(u2.apply("chirp")); // CHIRP

    BinaryOperator<String> b1 = String::concat;
    BinaryOperator<String> b2 = (string, toAdd) -> string.concat(toAdd);
    System.out.println(b1.apply("baby ", "chick")); // baby chick
    System.out.println(b2.apply("baby ", "chick")); // baby chick
  }
}