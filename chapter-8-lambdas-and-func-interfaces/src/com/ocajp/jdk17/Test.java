package com.ocajp.jdk17;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.function.*;

/**
 * Абстрактные методы с сигнатурами, которые содержатся в общедоступных методах java.lang.Объект не учитывается при подсчете абстрактного метода для функционального интерфейса.
 */
public class Test {
  public void method() {
    Predicate<StringBuilder> sb = (StringBuilder s) -> s.isEmpty();
    x((var x) -> {}, (var x, var y) -> false);
  }
  public void x(Consumer<String> x, BinaryOperator<Boolean> y) {}

  public static void main(String[] args) {
    System.out.println(0%2); //0
    Consumer<String> x1 = (var x) -> {};
    BinaryOperator<Boolean> y1 = (var x, var y) -> false;
    //Predicate<String> pr = (StringBuilder s) -> s.isEmpty(); //Incompatible parameter types in lambda expression:
    // expected String but found StringBuilder

    Set<?> set = Set.of("lion", "tiger", "bear");
    var s = Set.copyOf(set);
    //Consumer<Object> consumer = (s) -> System.out.println(s); //Variable 's' is already defined in the scope
    //Consumer<Object> consumer = () -> System.out.println(s); //Incompatible parameter types in lambda expression: wrong number of parameters: expected 1 but found 0
    Consumer<Object> consumer = System.out::println;
    s.forEach(consumer);

    BiConsumer<String, String> m = (a, b) -> System.out.println("Consumer " + a + " " + b);
    BiConsumer<String, String> n = (a, b) -> System.out.println("Consumer " + a + " " + b);
    BiConsumer<String, String> y = m.andThen(n);
    y.accept("1", "2");

    Supplier<String> x = String::new;
    System.out.println(x.get());

    UnaryOperator<String> z = a -> a + a;
    System.out.println(z.apply("10"));


    int length = 3;
    for (int i = 0; i<3; i++) {
      if (i%2 == 0) {
        Supplier<Integer> supplier = () -> length; // A
        System.out.println(supplier.get()); // B
      } else {
        int j = i;
        Supplier<Integer> supplier = () -> j; // C
        System.out.println(supplier.get()); // D
      }
    }

    /**
     * The a.compose(b) method calls the Function parameter b before the reference
     * Function variable a. In this case, that means that we multiply by 3 before adding 4. This
     * gives a result of 7, making option A correct
     */
    Function<Integer, Integer> st = a -> a + 4; // 2 step
    Function<Integer, Integer> t = a -> a * 3; // 1 step
    Function<Integer, Integer> c = st.compose(t);
    System.out.print(c.apply(1));
  }
}

class Hyena {
  private int age = 1;
  public static void main(String[] args) {
    var p = new Hyena();
    double height = 10;
    int age = 1;
    testLaugh(p, var -> p.age <= 10);
    testLaugh(p, h -> h.age < 5);
    testLaugh(p, h -> h.age < 5);
    //testLaugh(p, shenzi -> age==2); //Variable used in lambda expression should be final or effectively final
    //testLaugh(p, age==1); //Required type:Predicate<com.ocajp.jdk17.Hyena>
    //testLaugh(p, p -> true); //Variable 'p' is already defined in the scope
    //testLaugh(p, shenzi -> age==1); //Variable used in lambda expression should be final or effectively final

    age = 2;
  }
  static void testLaugh(Hyena panda, Predicate<Hyena> joke) {
    var r = joke.test(panda) ? "hahaha" : "silence";
    System.out.print(r);
  }
}

/**
 * The code does not compile because the lambdas are assigned to var. The compiler does
 * not have enough information to determine they are of type Predicate<String>
 */
class Fantasy {
  public static void scary(String animal) {
    //var dino = s -> "dino".equals(animal);
    //var dragon = s -> "dragon".equals(animal);
    //var combined = dino.or(dragon);
    //System.out.println(combined.test(animal));
  }
  public static void main(String[] args) {
    scary("dino");
    scary("dragon");
    scary("unicorn");
  }
}

class Q7 {
  public static void main(String[] args) {
    Q7 q7 = new Q7();
    q7.method();
    //var var = (var x, var y) -> false; //Cannot infer type: lambda expression requires an explicit target type
  }
  public void method() {
    x((var x) -> {}, (var x, var y) -> false);
  }
  public void x(Consumer<String> x, BinaryOperator<Boolean> y) {}
}

class Q11 {
  public static void main(String[] args) {
    scary("dino");
    scary("dragon");
    scary("unicorn");
  }
  public static void scary(String animal) {
    Predicate<String> dino = s -> "dino".equals(animal);
    Predicate<String>  dragon = s -> "dragon".equals(animal);
    var combined = dino.or(dragon);
    System.out.println(combined.test(animal)); // true true false
  }
}

class Q12 {
  String check(List list, Predicate p) {
    return p.test(list) ? "Empty" : "Populated";
  }
  String checkListList(List list, Predicate<List> p) {
    return p.test(list) ? "Empty" : "Populated";
  }
  String checkList(List list, Predicate<ArrayList> p) {
    //return p.test(list) ? "Empty" : "Populated"; //Required type:ArrayList
    return "error";
  }
  String checkArrayList(ArrayList list, Predicate<ArrayList> p) {
    return p.test(list) ? "Empty" : "Populated"; //Required type:ArrayList
  }
  String checkImp(List list, Predicate p){ return p.test((ArrayList)list)? "Empty" : "Populated"; }
  void run() {
    //ArrayList list = new ArrayList();
    //System.out.println(check(list, list -> list.isEmpty()));

    ArrayList arrayList = new ArrayList();
    System.out.println(check(arrayList, myList -> arrayList.isEmpty()));

    List list = new ArrayList();
    System.out.println(checkList(list, myList -> myList.isEmpty())); //Cannot resolve method 'isEmpty' in 'Object'

    List mist = new ArrayList();
    //System.out.println(checkArrayList(mist, listInner -> listInner.isEmpty())); //Required type: ArrayList

    System.out.println(checkListList(list, myList -> myList.isEmpty()));

    System.out.println(checkArrayList((ArrayList)mist, listImp -> listImp.isEmpty()));

    //System.out.println(checkImp(arrayList, listImp -> listImp.isEmpty()));
  }

  public static void main(String[] args) {
    new Q12().run();
  }
}

class Q12Two {
  public static void main(String[] args) {
    //BinaryOperator tComparator = (Integer w, var c) -> 39;
    Function<Integer, Integer> s = a -> a + 4;
    Function<Integer, Integer> t = a -> a * 3;
    Function<Integer, Integer> c = s.andThen(t);
    Function<Integer, Integer> d = s.compose(t);
    System.out.print(c.apply(1));
  }
}

class Q15 {
  private int age = 1;
  public static void main(String[] args) {
    var p = new Q15();
    double height = 10;
    int age = 1;
    //testLaugh(p, p -> true); age = 2; //Variable 'p' is already defined in the scope
    //testLaugh(p, h -> h.age < 5); age = 2;
    //testLaugh(p, var -> p.age <= 10); age = 2;
  }
  static void testLaugh(Q15 panda, Predicate<Q15> joke) {
    var r = joke.test(panda) ? "hahaha" : "silence";
    System.out.print(r); }
}