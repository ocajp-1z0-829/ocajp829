package com.ocajp.jdk17;

/**
 * There are four formats for method references:
 * ■ static methods
 * ■ Instance methods on a particular object
 * ■ Instance methods on a parameter to be determined at runtime
 * ■ Constructors
 */
interface LearnToSpeak {
  void speak(String sound);
}

/**
 * Calling static Methods
 */
@FunctionalInterface
interface Converter {
  long round(double num);
}

@FunctionalInterface
interface Converter2 {
  int round(float num);
}

/**
 * Calling Instance Methods on a Particular Object
 */
interface StringStart {
  boolean beginningCheck(String prefix);
}

interface StringChecker {
  boolean check();
}

public class MethRef {
}

class DuckHelper {
  public static void teacher(String name, LearnToSpeak trainer) {
    // Exercise patience (omitted)
    trainer.speak(name);
  }
}

class Duckling {
  public static void makeSound(String sound) {
    //LearnToSpeak learner = s -> System.out.println(s);
    LearnToSpeak learner = System.out::println;
    DuckHelper.teacher(sound, learner);
  }
}
class Conv {
  public static void main(String[] args) {
    Converter methodRef = Math::round;
    Converter2 lambda = x -> Math.round(x);
    System.out.println(methodRef.round(100.5));
    System.out.println(lambda.round(100.1f));
  }
}
class StrStart {
  public static void main(String[] args) {
    var str = "Zoo";
    StringStart methodRef = str::startsWith;
    StringStart lambda = s -> str.startsWith(s);

    System.out.println(methodRef.beginningCheck("A")); // false
    System.out.println(lambda.beginningCheck("Z")); // false
  }
}

class StrChecker {
  public static void main(String[] args) {
    var str = "";
    StringChecker methodRef = str::isEmpty;
    StringChecker lambda = () -> str.isEmpty();

    System.out.print(methodRef.check()); // true


    var str2 = "";
    StringChecker lambda2 = () -> str.startsWith("Zoo");

    //StringChecker methodReference1 = str::startsWith; // DOES NOT COMPILE
    //StringChecker methodReference2 = str::startsWith("Zoo"); // DOES NOT COMPILE
  }
}

/**
 * Calling Instance Methods on a Parameter
 */
interface StringParameterChecker {
  boolean check(String text);
}
class StrParamChecker {
  public static void main(String[] args) {
    StringParameterChecker methodRef = String::isEmpty;
    StringParameterChecker lambda = s -> s.isEmpty();

    System.out.println(methodRef.check("Zoo")); // false
  }
}

/**
 * The first one will always be the instance of the object for instance methods. Any
 * others are to be method parameters
 */
interface StringTwoParameterChecker {
  boolean check(String text, String prefix);
}
class StrTwoParamChecker {
  public static void main(String[] args) {
    StringTwoParameterChecker methodRef = String::startsWith;
    StringTwoParameterChecker lambda = (s, p) -> s.startsWith(p);

    System.out.println(methodRef.check("Zoo", "Z")); // true
  }
}


/**
 * Calling Constructors
 */
interface EmptyStringCreator {
  String create();
}
class EmptyStrCreator {
  public static void main(String[] args) {
    EmptyStringCreator methodRef = String::new;
    EmptyStringCreator lambda = () -> new String();

    var myString = methodRef.create(); //""
    System.out.println(myString.equals("Snake")); // false
  }
}
interface StringCopier {
  String copy(String value);
}
class StrCopier {
  public static void main(String[] args) {
    StringCopier methodRef = String::new;
    StringCopier lambda = x -> new String(x);

    var myString = methodRef.copy("Zebra"); //"Zebra"
    System.out.println(myString.equals("Zebra")); // true
  }
}
