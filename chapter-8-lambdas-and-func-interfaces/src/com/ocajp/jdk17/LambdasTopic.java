package com.ocajp.jdk17;

import java.util.function.BooleanSupplier;
import java.util.function.Consumer;

/**
 * Ни у лямбды, ни у var недостаточно информации, чтобы определить, какой тип
 * функционального интерфейса следует использовать.
 * var запрещены
 */
public class LambdasTopic {
  //int toString() {
    //'toString()' in 'com.ocajp.jdk17.LambdasTopic' clashes with 'toString()' in 'java.lang.Object';
    // attempting to assign weaker access privileges ('package-private'); was 'public'
    //return 1;
  //}

  public static void main(String[] args) {
    Consumer consumer = s -> {};
    System.out.println(consumer);
    BooleanSupplier booleanSupplier = () -> true;
    System.out.println(booleanSupplier);
    //var invalid = (Animal a) -> a.canHop(); //Cannot infer type: lambda expression requires an explicit target type

  }
}


/**
 * All four of these are valid interfaces, but not all of them are functional interfaces. The
 * Dash interface is a functional interface because it extends the Sprint interface and inherits
 * the single abstract method sprint(). The Skip interface is not a valid functional interface because it has two
 * abstract methods: the inherited sprint() method and the declared
 * skip() method.
 */
@FunctionalInterface
interface Dash extends Sprint {}

//@FunctionalInterface //Multiple non-overriding abstract methods found in interface com.ocajp.jdk17.Skip
interface Skip extends Sprint {
  void skip();
}

/**
 * The Sleep interface is also not a valid functional interface. Neither snore() nor getZzz()
 * meets the criteria of a single abstract method. Even though default methods function like
 * abstract methods, in that they can be overridden in a class implementing the interface, they
 * are insufficient for satisfying the single abstract method requirement.
 */
//@FunctionalInterface//No target method found
interface Sleep {
  private void snore() {}
  default int getZzz() { return 1; }
}

/**
 * Finally, the Climb interface is a functional interface. Despite defining a slew of methods, it
 * contains only one abstract method: reach().
 */
@FunctionalInterface
interface Climb {
  void reach();
  default void fall() {}
  static int getBackUp() { return 100; }
  private static boolean checkHeight() { return true; }
}

//@FunctionalInterface //No target method found
interface Soar {
  abstract String toString();
}

//@FunctionalInterface //No target method found
interface SoarInt {
  //abstract int toString(); //'toString()' in 'com.ocajp.jdk17.SoarInt' clashes with 'toString()' in 'java.lang
  // .Object'; attempting to use incompatible return
}

@FunctionalInterface
interface Dive {
  String toString(); //Object
  public boolean equals(Object o); //Object
  public abstract int hashCode(); //Object
  public void dive(); //FunctionalInterface
}

//@FunctionalInterface//Multiple non-overriding abstract methods found in interface com.ocajp.jdk17.Hibernate
interface Hibernate {
  String toString();
  public boolean equals(Hibernate o);//not Objects
  public abstract int hashCode();
  public void rest();//
}
