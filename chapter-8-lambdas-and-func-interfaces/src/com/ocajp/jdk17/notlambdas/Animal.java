package com.ocajp.jdk17.notlambdas;

public record Animal(String species, boolean canHop, boolean canSwim) { }
