package com.ocajp.jdk17.notlambdas;

public class CheckIfHopper implements CheckTrait {
  @Override
  public boolean test(Animal a) {
    return a.canHop();
  }
}
