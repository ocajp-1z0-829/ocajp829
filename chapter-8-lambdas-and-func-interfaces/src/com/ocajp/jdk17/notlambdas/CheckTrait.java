package com.ocajp.jdk17.notlambdas;

/**
 * Since we are passing a lambda instead, Java tries to map our lambda to the abstract
 * method declaration in the CheckTrait interface
 */
public interface CheckTrait {
  boolean test(Animal a);
}
