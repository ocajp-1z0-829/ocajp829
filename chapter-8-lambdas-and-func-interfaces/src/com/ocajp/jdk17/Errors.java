package com.ocajp.jdk17;

import java.util.function.BiFunction;

public class Errors {
  public static void main(String[] args) {
    //needs to remove var from x or add it to y
    //(var x, y) -> "Hello" // DOES NOT COMPILE
    BiFunction f = (x, y) -> "Hello"; // Object, Object
    f = (var x, var y) -> "Hello"; // Object, Object

    // need to use the type or var consistently
    //(var x, Integer y) -> true // DOES NOT COMPILE
    f = (var x, var y) -> true; // Object, Object
    f = (Object x, Object y) -> true; // Object, Object


    //3 elems and need to use the type or var consistently
    //(String x, var y, Integer z) -> true // DOES NOT COMPILE

    //needs to remove Integer from x or add a type to y
    //(Integer x, y) -> "goodbye"// DOES NOT COMPIL
    f = (x, y) -> "goodbye";
    f = (var x, var y) -> "goodbye";


  }
}
