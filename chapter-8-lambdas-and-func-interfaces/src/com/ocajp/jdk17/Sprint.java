package com.ocajp.jdk17;

@FunctionalInterface
abstract interface Sprint {
  void sprint(int speed);
}
