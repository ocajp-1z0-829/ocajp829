package com.ocajp.jdk17;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;
import java.util.function.DoubleBinaryOperator;
import java.util.function.DoubleConsumer;
import java.util.function.DoubleFunction;
import java.util.function.DoublePredicate;
import java.util.function.DoubleSupplier;
import java.util.function.DoubleToIntFunction;
import java.util.function.DoubleToLongFunction;
import java.util.function.DoubleUnaryOperator;
import java.util.function.Function;
import java.util.function.IntBinaryOperator;
import java.util.function.IntConsumer;
import java.util.function.IntFunction;
import java.util.function.IntPredicate;
import java.util.function.IntSupplier;
import java.util.function.IntToDoubleFunction;
import java.util.function.IntToLongFunction;
import java.util.function.IntUnaryOperator;
import java.util.function.LongBinaryOperator;
import java.util.function.LongConsumer;
import java.util.function.LongFunction;
import java.util.function.LongPredicate;
import java.util.function.LongSupplier;
import java.util.function.LongToDoubleFunction;
import java.util.function.LongToIntFunction;
import java.util.function.LongUnaryOperator;
import java.util.function.ObjDoubleConsumer;
import java.util.function.ObjIntConsumer;
import java.util.function.ObjLongConsumer;
import java.util.function.Predicate;
import java.util.function.ToDoubleFunction;
import java.util.function.ToIntBiFunction;
import java.util.function.ToIntFunction;
import java.util.function.ToLongBiFunction;
import java.util.function.ToLongFunction;

public class FuncInterfacesMethods {
}


/**
 * andThen(изначально переданное значение)
 */
class ConsumerImplMethod {
  public static void main(String[] args) {
    Consumer<String> c1 = x -> System.out.print("1: " + x);
    Consumer<String> c2 = x -> System.out.print(",2: " + x);

    Consumer<String> combined = c1.andThen(c2);
    combined.accept("Annie"); // 1: Annie,2: Annie
  }
}

class PredicateImplMethod {
  public static void main(String[] args) {
    Predicate<String> egg = s -> s.contains("egg");
    Predicate<String> brown = s -> s.contains("brown");

    Predicate<String> brownEggs1 = s -> s.contains("egg") && s.contains("brown");
    Predicate<String> otherEggs1 = s -> s.contains("egg") && !s.contains("brown");

    Predicate<String> brownEggs2 = egg.and(brown);
    Predicate<String> otherEggs2 = egg.and(brown.negate());

    System.out.println(brownEggs2.test("eggbrown")); //true
    System.out.println(otherEggs2.test("eggyellow")); //true

    Predicate<String> p1 = x -> true;
    Predicate<String> p2 = (var x) -> true;
    Predicate<String> p3 = (String x) -> true;
  }
  public void counts(List<Integer> list) {
    list.sort((final var x, @Deprecated var y) -> x.compareTo(y));
  }
}

/**
 * compose(использует результат исполнения первой функции)
 */
class FuncImplMethod {
  public static void main(String[] args) {
    Function<Integer, Integer> before = x -> x + 1; //4
    Function<Integer, Integer> after = x -> x * 2; //8
    Function<Integer, Integer> combined = after.compose(before);
    System.out.println(combined.apply(3)); // 8
  }
}

class BooleanSupplierImpl {
  public static void main(String[] args) {
    BooleanSupplier b1 = () -> true;
    BooleanSupplier b2 = () -> Math.random() > .5;
    System.out.println(b1.getAsBoolean()); // true
    System.out.println(b2.getAsBoolean()); // false
  }
}

class IntFuncImpl {
  public static void main(String[] args) {
    var d = 1.0;
    DoubleToIntFunction f1 = x -> 1;
    ToIntFunction f2 = x -> 1;
    f1.applyAsInt(d); //comperr

    //Любой тип из дабла
    ObjDoubleConsumer<Long> l1 = (x, y) -> System.out.println(x + y);
    l1.accept(1L, 2.9); //3.9
  }
}

class IntConsumerImpl {
  public static void main(String[] args) {
    IntConsumer intConsumer = (a) -> System.out.println(a);
    intConsumer.accept(5);
  }
}

class DoubleConsumerImpl {
  public static void main(String[] args) {
    AtomicInteger a = new AtomicInteger(4);
    DoubleConsumer doubleConsumer = c -> a.set((int)c);
    doubleConsumer.accept(10.9);
    System.out.println(a.get());
  }
}

class LongConsumerImpl {
  public static void main(String[] args) {
    LongConsumer longConsumer = System.out::println;
    longConsumer.accept(15);
  }
}

class DoubleSupplierImpl {
  public static void main(String[] args) {
    //DoubleSupplier doubleSupplier = 14.0; //нельзя
    //DoubleSupplier doubleSupplier = () -> new Random(); //Bad return type in lambda expression: Random cannot be
    // converted to double
    DoubleSupplier doubleSupplier = Math::random;
    System.out.println(doubleSupplier.getAsDouble());
  }
}

class IntSupplierImpl {
  public static void main(String[] args) {
    IntSupplier intSupplier = () -> new Integer(1);
    System.out.println(intSupplier.getAsInt());
  }
}

class LongSupplierImpl {
  public static void main(String[] args) {
    LongSupplier longSupplier = () -> new Long(1);
    System.out.println(longSupplier.getAsLong());
  }
}

class DoublePredicateImpl {
  public static void main(String[] args) {
    DoublePredicate doublePredicate = (a) -> true;
    System.out.println(doublePredicate.test(4));

    IntPredicate intPredicate = a -> a > 3;
    System.out.println(intPredicate.test(33));

    LongPredicate longPredicate = (a) -> true;
    System.out.println(longPredicate.test(1));
  }
}

class DoubleFuncImple {
  public static void main(String[] args) {
    DoubleFunction<String> doubleDoubleFunction = String::valueOf;
    System.out.println(doubleDoubleFunction.apply(3000));

    IntFunction<String> intFunction = String::valueOf;
    System.out.println(intFunction.apply(3000));

    LongFunction<String> longFunction = String::valueOf;
    System.out.println(longFunction.apply(3000));
  }
}

class OperatorImpl {
  public static void main(String[] args) {
    DoubleUnaryOperator doubleUnaryOperator = a -> a + 4;
    System.out.println(doubleUnaryOperator.applyAsDouble(1));

    IntUnaryOperator intUnaryOperator = a -> a + 4;
    System.out.println(intUnaryOperator.applyAsInt(2));

    LongUnaryOperator longUnaryOperator = a -> (long) (a + Math.random() * 10);
    System.out.println(longUnaryOperator.applyAsLong(3));

    DoubleBinaryOperator doubleBinaryOperator = (a, b) -> {return a + Math.random() / b;};
    System.out.println(doubleBinaryOperator.applyAsDouble(1, 1));

    IntBinaryOperator intBinaryOperator = (a, b) -> {return (int) (a + Math.random() / b);};
    System.out.println(intBinaryOperator.applyAsInt(1, 1));

    LongBinaryOperator longBinaryOperator = (a, b) -> {return (int) (a + Math.random() / b);};
    System.out.println(longBinaryOperator.applyAsLong(2, 1));
  }
}

class PrimSpecFuncImpl {
  public static void main(String[] args) {
    ToIntFunction<String> stringToIntFunc = string -> Integer.valueOf(string); // Integer::valueOf
    System.out.println(stringToIntFunc.applyAsInt("5")); //5

    ToDoubleFunction<String> stringToDoubleFunction = string -> string.length();
    System.out.println(stringToDoubleFunction.applyAsDouble("Test5" + "      "));

    ToLongFunction<Double> doubleToLongFunction = doub -> doub.longValue();
    System.out.println(doubleToLongFunction.applyAsLong(100.3));

    ToIntBiFunction<Double, Long> doubleLongToIntBiFunction = (a, b) -> (int) (a + b);
    System.out.println(doubleLongToIntBiFunction.applyAsInt(10.5, 22L));

    ToLongBiFunction<String, String> stringStringToLongBiFunction = (s1, s2) -> s1.length() + s2.length();
    System.out.println(stringStringToLongBiFunction.applyAsLong("12345", "test"));

    ToIntBiFunction<String, Double> stringDoubleToIntBiFunction = (s1, d2) -> Double.valueOf(s1 + d2).intValue();
    System.out.println(stringDoubleToIntBiFunction.applyAsInt("3", 3.4));

    DoubleToIntFunction doubleToIntFunction = s -> Double.valueOf(s).intValue();
    System.out.println(doubleToIntFunction.applyAsInt(1.0));

    DoubleToLongFunction doubleToLongFunction2 = s -> Double.valueOf(s).longValue();
    System.out.println(doubleToLongFunction2.applyAsLong(1.0));

    IntToDoubleFunction intToDoubleFunction = s -> s + 4;
    System.out.println(intToDoubleFunction.applyAsDouble(1));

    IntToLongFunction intToLongFunction = s -> s + 4;
    System.out.println(intToLongFunction.applyAsLong(1));

    LongToDoubleFunction longToDoubleFunction = s -> s + 4;
    System.out.println(longToDoubleFunction.applyAsDouble(1));

    LongToIntFunction longToIntFunction = s -> (int) (s + 4);
    System.out.println(longToIntFunction.applyAsInt(1));

    ObjDoubleConsumer<Object> objDoubleConsumer = (o, d) -> System.out.println(Double.parseDouble((String) o));
    objDoubleConsumer.accept("10", 3.55);

    ObjIntConsumer<Object> objIntConsumer = (o, d) -> System.out.println(Double.parseDouble((String) o));
    objIntConsumer.accept("10", 3);

    ObjLongConsumer<String> objLongConsumer = (s, d) -> System.out.println(String.valueOf(d) + 500 + s);
    objLongConsumer.accept("", 4500);
  }
}