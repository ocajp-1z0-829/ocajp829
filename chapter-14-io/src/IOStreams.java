import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;

public class IOStreams {
    public static void main(String[] args) {
        Charset usAsciiCharset = Charset.forName("US-ASCII");
        System.out.println(usAsciiCharset);
        Charset utf8Charset = Charset.forName("UTF-8");
        Charset utf16Charset = Charset.forName("UTF-16");

        try (var br = new BufferedReader(new FileReader("io/zoo-data.txt"))) {
            while (br.ready())
            System.out.println(br.readLine());
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        try (var ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream("io/zoo-data.txt")))) {
            System.out.print(ois.readObject());
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}

class StreamBaseClasses {
    public static void main(String[] args) throws IOException {
        //new BufferedInputStream(new FileReader("z.txt")); // DOES NOT COMPILE Required type: InputStream
        //new BufferedWriter(new FileOutputStream("z.txt")); // DOES NOT COMPILE Required type: Writer
        //new ObjectInputStream(new FileOutputStream("z.txt")); // DOES NOT COMPILE Required type: InputStream
        //new BufferedInputStream(new InputStream()); // DOES NOT COMPILE 'InputStream' is abstract; cannot be instantiated
    }
}

class ReadingAndWritingFiles {
    public static void main(String[] args) {
        ReadingAndWritingFiles readingAndWritingFiles = new ReadingAndWritingFiles();

        //readingAndWritingFiles.copyByOneByteStream();
    }

    void copyByOneByteStream(InputStream in, OutputStream out) throws IOException {
        int b;
        while ((b = in.read()) != -1) {
            out.write(b);
        } }
    void copyByOneCharacterStream(Reader in, Writer out) throws IOException {
        int b;
        while ((b = in.read()) != -1) {
            out.write(b);
        } }

    void copyStream(InputStream in, OutputStream out) throws IOException {
        int batchSize = 1024;
        var buffer = new byte[batchSize];
        int lengthRead;
        while ((lengthRead = in.read(buffer, 0, batchSize)) > 0) {
            out.write(buffer, 0, lengthRead);
            out.flush();
        }
    }

    void copyTextFile(File src, File dest) throws IOException {
        try (var reader = new BufferedReader(new FileReader(src));
             var writer = new BufferedWriter(new FileWriter(dest))) {
            String line = null;
            while ((line = reader.readLine()) != null) {
                writer.write(line);
                writer.newLine();
            }
        }
    }

    void copyTextFileWithPrintWriter(File src, File dest) throws IOException {
        try (var reader = new BufferedReader(new FileReader(src));
             var writer = new PrintWriter(new FileWriter(dest))) {
            String line = null;
            while ((line = reader.readLine()) != null)
                writer.println(line);
        }
    }

    private void copyPathAsString(Path input, Path output) throws IOException {
        String string = Files.readString(input);
        Files.writeString(output, string);
    }
    private void copyPathAsBytes(Path input, Path output) throws IOException {
        byte[] bytes = Files.readAllBytes(input);
        Files.write(output, bytes);
    }
    private void copyPathAsLines(Path input, Path output) throws IOException {
        List<String> lines = Files.readAllLines(input);
        Files.write(output, lines);
    }

    private void readLazily(Path path) throws IOException {
        try (Stream<String> s = Files.lines(path)) {
            s.filter(f -> f.startsWith("WARN:"))
                    .map(f -> f.substring(5))
                    .forEach(System.out::println);
        }

        Files.readAllLines(Paths.get("birds.txt")).forEach(System.out::println);
        Files.lines(Paths.get("birds.txt")).forEach(System.out::println);
    }

    private void copyPath(Path input, Path output) throws IOException {
        try (var reader = Files.newBufferedReader(input);
             var writer = Files.newBufferedWriter(output)) {
            String line = null;
            while ((line = reader.readLine()) != null) {
                writer.write(line);
                writer.newLine();
            }} }
}

class MarkingData {
    public static void main(String[] args) throws FileNotFoundException, IOException {
        InputStream is = new FileInputStream("io/LION.txt"); //mark is not supported
        MarkingData markingData = new MarkingData();
        markingData.readData(is);
    }

    public void readData(InputStream is) throws IOException {
        System.out.print((char) is.read()); // L
        if (is.markSupported()) {
            is.mark(100); // Marks up to 100 bytes
            System.out.print((char) is.read()); // I
            System.out.print((char) is.read()); // O
            is.reset(); // Resets stream to position before I
        }
        System.out.print((char) is.read()); // I
        System.out.print((char) is.read()); // O
        System.out.print((char) is.read()); // N
    }
}