import model.BabyChimpanzee;
import model.Chimpanzee;

import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Tests {
}

/**
 * javac -d out src/Tests.java
 * java -classpath "out" Q3
 */
class Q3 {
    public static void main(String[] args) {
        System.out.println(FileSystems.getDefault().getPath("chapter-14-io/tests"));
        String line;
        var c = System.console();
        Writer w = c.writer();
        try (w) {
            if ((line = c.readLine("Enter your name: ")) != null)
                w.append(line);
            w.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

class Q4 {
    public static void main(String[] args) {
        Q4 q4 = new Q4();
        q4.removeBadFile(Path.of("io/empty")); //Success or DirectoryNotEmptyException
    }
    public void removeBadFile(Path path) {
        if(Files.isDirectory(path)) {
            try {
                System.out.println(Files.deleteIfExists(path) ? "Success": "Try Again");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}

class Q5 {
    public static void main(String[] args) {
        Path path = Path.of("io/empty");
        try (var z = Files.walk(path)) {
            boolean b = z
                    //.filter((p,a) -> a.isDirectory() && !path.equals(p)) // x
                    .findFirst().isPresent(); // y
            System.out.print(b ? "No Sub": "Has Sub");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        System.out.println();

        try (var z = Files.find(path, 3, (p,a) -> a.isDirectory() && !path.equals(p))) {
            boolean b = z.findFirst().isPresent();
            System.out.print(b ? "No Sub": "Has Sub");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

class Q6 {
    static class Bird {
        protected transient String name;
        public void setName(String name) { this.name = name; }
        public String getName() { return name; }
        public Bird() {
            this.name = "Matt"; }

        @Override
        public String toString() {
            return "Bird{" +
                    "name='" + name + '\'' +
                    '}';
        }
    }
    static class Eagle extends Bird implements Serializable {
        {
            this.name = "Olivia";
        }

        public Eagle() {
            this.name = "Bridget";
        }

        @Override
        public String toString() {
            return "Eagle{" +
                    "name='" + name + '\'' +
                    '}';
        }

        public static void main(String[] args) {
            var e = new Eagle();
            e.name = "Adeline";
        }
    }

    static void saveToFile(List<Eagle> eagles, File dataFile) {
        try (var out = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(dataFile)))) {
            for (Eagle eagle : eagles)
                out.writeObject(eagle);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    static List<Eagle> readFromFile(File dataFile) throws IOException, ClassNotFoundException {
        var eagles = new ArrayList<Eagle>();
        try (var in = new ObjectInputStream(new BufferedInputStream(new FileInputStream(dataFile)))) {
            while (true) {
                var object = in.readObject();
                if (object instanceof Eagle eagle)
                    eagles.add(eagle);
            }
        } catch (EOFException e) { // File end reached
            System.out.println("End of file");
        }
        return eagles;
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        var eagles = new ArrayList<Eagle>();
        eagles.add(new Eagle());
        eagles.add(new Eagle());
        File dataFile = new File("eagles.data");
        saveToFile(eagles, dataFile);
        var chimpanzeesFromDisk = readFromFile(dataFile);
        System.out.println(chimpanzeesFromDisk);
    }
}

class Q8 {
    public static void main(String[] args) throws IOException {
        Path p = Path.of("tests", "food-schedule.csv");
        printData(p);
    }
    static void printData(Path path) throws IOException {
        Files.lines(path) // r1
            .flatMap(p -> Stream.of(p.split(","))) // r2
            .map(q -> q.toUpperCase()) // r3
            .forEach(System.out::println);
    }
}

class Q9 {
    public static void main(String[] args) throws Exception {
        copyFile(new File("tests", "food-schedule.csv"), new File("tests", "copied.csv"));
    }

    public static void copyFile(File file1, File file2) throws Exception {

        try (var reader = new InputStreamReader(new FileInputStream(file1));
                var writer = new FileWriter(file2)) {
            char[] buffer = new char[10];
            int len = 0;
            while ((len = reader.read(buffer)) != -1) {
                writer.write(buffer); //0, len надо
                writer.flush();
                System.out.println("read len: " + len + "; buffer len " + buffer.length);
                // n1 }
            }
        }
    }
}

class Q10 {
    public static void main(String[] args) {
        //new Path("jaguar.txt"); //Path is abstract type that should be instantiated using a factory method.
        System.out.println(FileSystems.getDefault().getPath("puma.txt")); //puma.txt
        //Path.get("cats","lynx.txt"); //static method in the Path interface is of(), not get()
        System.out.println(new File("tiger.txt").toPath()); //tiger.txt
        //new FileSystem().getPath("lion"); //FileSystem is abstract type that should be instantiated using a factory method.
        //Paths.getPath("ocelot.txt"); //static method in the Paths class is get(), not getPath()
        System.out.println(Path.of(Path.of(".").toUri())); ///Users/admin/IdeaProjects/chapter-14-io/chapter-14-io/.
    }
}

class Q11 {
    public static void main(String[] args) throws IOException {
        var is = new BufferedInputStream(new FileInputStream("z.txt"));
        InputStream wrapper1 = new BufferedInputStream(is);
        InputStream wrapper2 = new ObjectInputStream(is);
        try (wrapper1) {}
        try (wrapper2) {}
    }
}

class Q12 {
    public static void main(String[] args) throws IOException {
        var p = Paths.get("tests", "backups");
        var a = Files.readAttributes(p, BasicFileAttributes.class);
        Files.createDirectory(p.resolve(".backup"));
        if(a.size()>0 && a.isDirectory()) {
            System.out.println(a.size());
           // a.setTimes(null,null,null); //read only
        }
    }
}

class Q14 {
    public static void main(String[] args) throws IOException {
        var p1 = Path.of("/zoo/./bear","../food.txt");
        p1.normalize().relativize(Path.of("/lion")); //immutable
        System.out.println(p1); // /zoo/./bear/../food.txt

        var p2 = Paths.get("/zoo/animals/bear/koala/food.txt");
        System.out.println(p2.subpath(1,3).getName(1)); // bear

        var p3 = Path.of("/pets/../cat.txt");
        var p4 = Paths.get("./dog.txt");
        System.out.println(p4.resolve(p3)); // /pets/../cat.txt
    }
}

class Q15 {
    public static void main(String[] args) throws IOException {
        //new File("/weather", "winter", "snow.dat");
        System.out.println(new File("/weather/winter/snow.dat"));
        //new File("/weather/winter", new File("snow.dat"));
        System.out.println(new File("weather", "/winter/snow.dat"));
        System.out.println(new File(new File("/weather/winter"), "snow.dat"));
        System.out.println(Path.of("/weather/winer/snow.dat").toFile());
    }
}

class Q16 {
    public static void main(String[] args) throws IOException {
        System.out.println(new File(".").getAbsolutePath());
        echo();
    }
    private static void echo() throws IOException {
        var o = new FileWriter("chapter-14-io/tests/new-zoo.txt");
        try (var f = new FileReader("chapter-14-io/tests/zoo-data.txt");
             var b = new BufferedReader(f); o) {
            o.write(b.readLine());
        }
        o.write("");
    }
}

class Q18 {
    public static void main(String[] args) throws IOException {
        try (var reader = new BufferedReader(new FileReader("tests/reader.txt"))) {
            var sb = new StringBuilder();
            sb.append((char)reader.read());
            reader.mark(10);
            for(int i=0; i<2; i++) {
                sb.append((char)reader.read());
                reader.skip(2); }
            reader.reset();
            reader.skip(0);
            sb.append((char)reader.read());
            System.out.println(sb.toString()); //PEOE
        }
    }
}

class Q19 {
    public static void main(String[] args) throws IOException {
        var p1 = Path.of("tests",".").resolve(Path.of("reader.txt"));
        var p2 = new File("tests/././actions/../reader.txt").toPath();
        System.out.print(Files.isSameFile(p1,p2));
        System.out.print(" ");
        System.out.print(p1.equals(p2));
        System.out.print(" ");
        System.out.print(Files.mismatch(p1,p2));
    }
}

class Q20 {
    public static void main(String[] args) throws IOException {
        Files.move(Path.of("tests/copied.csv"), Paths.get("tests","backups"), StandardCopyOption.ATOMIC_MOVE, LinkOption.NOFOLLOW_LINKS);
    }
}

class Q21 {
    public static void main(String[] args) throws IOException {
        var f = Path.of("chapter-14-io/tests");
        try (var m = Files.find(f, 0, (p,a) -> a.isSymbolicLink())) { // y1
            m.map(s -> s.toString())
                    .collect(Collectors.toList())
                    .stream()
                    .filter(s -> s.toString().endsWith(".txt")) // y2
                    .forEach(System.out::println);
        }
    }
}

class Q22 {
    static class Zebra implements Serializable {
        private transient String name = "George";
        private static String birthPlace = "Africa";
        private transient Integer age;
        List<Zebra> friends = new java.util.ArrayList<>();
        private Object stripes = new Object();
        {age = 10;}
        public Zebra() {
            this.name = "Sophia";
        }
        static Zebra writeAndRead(Zebra z) {return null;}

        public static void main(String[] args) {
            var zebra = new Zebra();
            zebra = writeAndRead(zebra);
        }
    }

    static void saveToFile(List<Zebra> eagles, File dataFile) {
        try (var out = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(dataFile)))) {
            for (Zebra eagle : eagles)
                out.writeObject(eagle);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    static List<Zebra> readFromFile(File dataFile) throws IOException, ClassNotFoundException {
        var eagles = new ArrayList<Zebra>();
        try (var in = new ObjectInputStream(new BufferedInputStream(new FileInputStream(dataFile)))) {
            while (true) {
                var object = in.readObject();
                if (object instanceof Zebra eagle)
                    eagles.add(eagle);
            }
        } catch (EOFException e) { // File end reached
            System.out.println("End of file");
        }
        return eagles;
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        var eagles = new ArrayList<Zebra>();
        eagles.add(new Zebra());
        eagles.add(new Zebra());
        File dataFile = new File("zebra.data");
        saveToFile(eagles, dataFile);
        var chimpanzeesFromDisk = readFromFile(dataFile);
        System.out.println(chimpanzeesFromDisk);
    }
}

class Q23 {
    public static void main(String[] args) throws IOException {
        var x = Path.of("chapter-14-io/tests/backups/..");
        System.out.println(x.toRealPath()); // /chapter-14-io/tests
        System.out.println(x.toRealPath().getParent()); // /chapter-14-io
        System.out.println();
        Files.walk(x.toRealPath().getParent()) // u1
                .map(p -> p.toAbsolutePath().toString()) // u2
                .filter(s -> s.endsWith(".java"))
                .forEach(System.out::println);
    }
}

class Q24 {
    public static void main(String[] args) throws IOException {
        var x = Path.of("tests/zoo-data.txt");
        copyIntoFlipDirectory(x);
    }
    static void copyIntoFlipDirectory(Path source) throws IOException {
        var dolphinDir = Path.of("tests/createdDir");
        dolphinDir = Files.createDirectories(dolphinDir);
        var n = Paths.get("new-zoo.txt");
        //Files.copy(source, dolphinDir); //FileAlreadyExistsException
        Files.copy(source, dolphinDir.resolve(n), StandardCopyOption.REPLACE_EXISTING);
        //Files.copy(source, dolphinDir, StandardCopyOption.REPLACE_EXISTING ); //change Dir -> file
        //Files.copy(source, dolphinDir.resolve(n)); //FileAlreadyExistsException
    }
}