import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Перемещение по каталогу
 * a depth-first search and a breadth-first search - поиск в глубину и поиск в ширину.
 */
public class TraversingDirectory {
    public static void main(String[] args) throws IOException {
        TraversingDirectory rd = new TraversingDirectory();
        var size = rd.getPathSize(Path.of("io"));
        System.out.println((double) size / 1024 + " KB");
        System.out.format("Total Size: %.2f megabytes", (size/1000000.0));
        System.out.println();
        rd.find();
    }

    private long getSize(Path p) {
        try {
            return Files.size(p);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
    public long getPathSize(Path source) throws IOException {
        try (var s = Files.walk(source, 0)) {
            return s.parallel()
                    .filter(p -> !Files.isDirectory(p))
                    .mapToLong(this::getSize)
                    .sum();
    } }

    public void find() throws IOException {
        Path path = Paths.get("io");
        long minSize = 1;
        try (var s = Files.find(path, 10,
                (p, a) -> a.isRegularFile()
                        && p.toString().endsWith(".txt")
                        && a.size() > minSize)) {
            s.forEach(System.out::println);
        }
    }

}
