import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.nio.file.attribute.PosixFileAttributes;
import java.util.stream.Stream;

public class FilesAndPaths {
    public static void main(String[] args) {
        System.out.print(System.getProperty("file.separator"));
        File zooFile1 = new File("/home/tiger/data/stripes.txt");
        File zooFile2 = new File("/home/tiger", "data/stripes.txt");
        File parent = new File("/home/tiger");
        File zooFile3 = new File(parent, "data/stripes.txt");
        File zooFile4 = new File((String) null, "data/stripes.txt");
        System.out.println(zooFile4 + " " + zooFile4.exists()); // /data/stripes.txt false

        Path zooPath1 = Path.of("/home/tiger/data/stripes.txt");
        Path zooPath2 = Path.of("/home", "tiger", "data", "stripes.txt");
        Path zooPath3 = Paths.get("/home/tiger/data/stripes.txt");
        Path zooPath4 = Paths.get("/home", "tiger", "data", "stripes.txt");
        System.out.println(zooPath4 + " " + Files.exists(zooPath1)); // /home/tiger/data/stripes.txt false

        Path zooPath5 = FileSystems.getDefault() .getPath("/home/tiger/data/stripes.txt");
        Path zooPath6 = FileSystems.getDefault() .getPath("/home", "tiger", "data", "stripes.txt");
        System.out.println(FileSystems.getDefault()); //sun.nio.fs.MacOSXFileSystem@28a418fc
        System.out.println(zooPath5 + " " + zooPath6);
        // /home/tiger/data/stripes.txt /home/tiger/data/stripes.txt

    }
}

class Funcs {
    public static void main(String[] args) {
        File file = new File((String) null, "data/stripes.txt");
        File newFile = new File((String) null, "data/stripes2.txt");
        Path path = Path.of("/home", "tiger", "data", "stripes.txt");
        Path newPath = Path.of("/home", "tiger", "data", "stripes2.txt");
        System.out.println("File:");
        System.out.println(file.getName()); //stripes.txt
        System.out.println(file.getParent()); //data
        System.out.println(file.isAbsolute()); //false
        System.out.println(file.delete()); //false
        System.out.println(file.exists()); //false
        System.out.println(file.getAbsolutePath()); // /Users/admin/IdeaProjects/chapter-14-io/chapter-14-io/data/stripes.txt
        System.out.println(file.isDirectory()); //false
        System.out.println(file.isFile()); //false
        System.out.println(file.lastModified()); //0
        System.out.println(file.length()); //0
        System.out.println(file.listFiles()); //null
        System.out.println(file.mkdir()); //false
        System.out.println(file.mkdirs()); //true
        System.out.println(file.renameTo(newFile)); //true

        System.out.println("Path: ");
        System.out.println(path.getFileName()); //stripes.txt
        System.out.println(path.getParent()); // /home/tiger/data
        System.out.println(path.isAbsolute()); //true
        try {
            System.out.println(Files.deleteIfExists(path)); //false
            //System.out.println(Files.getLastModifiedTime(path)); //NoSuchFileException
            //System.out.println(Files.size(path)); //NoSuchFileException
            //System.out.println(Files.list(path)); //NoSuchFileException
            //System.out.println(Files.createDirectory(path)); //NoSuchFileException
            //System.out.println(Files.createDirectories(path)); //java.nio.file.FileSystemException: /home/tiger: Operation not supported
            //System.out.println(Files.move(path, path)); //NoSuchFileException
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        System.out.println(Files.exists(path)); //false
        System.out.println(path.toAbsolutePath()); // /home/tiger/data/stripes.txt
        System.out.println(Files.isDirectory(path)); //false
        System.out.println(Files.isRegularFile(path)); //false
    }
}

class FileTest {
    public static void main(String[] args) {
        io();
    }
    public static void io() {
        var file = new File("C:\\data\\zoo.txt");
        if (file.exists()) {
            System.out.println("Absolute Path: " + file.getAbsolutePath());
            System.out.println("Is Directory: " + file.isDirectory());
            System.out.println("Parent Path: " + file.getParent());
            if (file.isFile()) {
                System.out.println("Size: " + file.length());
                System.out.println("Last Modified: " + file.lastModified()); }
            else {
                for (File subfile : file.listFiles()) {
                    System.out.println(" " + subfile.getName());
                } } } }
            }

class PathTest {
    public static void main(String[] args) throws IOException {
        nio();
    }
    public static void nio() throws IOException {
        var path = Path.of("C:\\data\\zoo.txt");
        if (Files.exists(path)) {
            System.out.println("Absolute Path: " + path.toAbsolutePath());
            System.out.println("Is Directory: " + Files.isDirectory(path));
            System.out.println("Parent Path: " + path.getParent());
            if (Files.isRegularFile(path)) {
                System.out.println("Size: " + Files.size(path));
                System.out.println("Last Modified: "
                        + Files.getLastModifiedTime(path));
            } else {
                try (Stream<Path> stream = Files.list(path)) {
                    stream.forEach(p ->
                            System.out.println(" " + p.getFileName()));
                }} } }
}

class OptionsTest {
    public static void main(String[] args) {
        Path path = Paths.get("schedule.xml");
        boolean exists = Files.exists(path, LinkOption.NOFOLLOW_LINKS);
        System.out.println(exists);
        System.out.println(StandardCopyOption.ATOMIC_MOVE);
        System.out.println(StandardOpenOption.TRUNCATE_EXISTING);
        System.out.println(FileVisitOption.FOLLOW_LINKS);
    }
    static void copy(Path source, Path target) throws IOException {
        Files.move(source, target,
            LinkOption.NOFOLLOW_LINKS,
            StandardCopyOption.ATOMIC_MOVE
                //StandardOpenOption.APPEND, FileVisitOption.FOLLOW_LINKS
        ); }
}

class ImmutableTest {
    public static void main(String[] args) {
        Path p = Path.of("whale");
        p.resolve("krill"); // no action
        System.out.println(p); // whale

        System.out.println(Path.of("/zoo/../home").resolve("test").getParent()); // /zoo/../home
        System.out.println(Path.of("/zoo/test/../home").normalize()); // /zoo/home
        System.out.println(Path.of("/zoo/../home").getParent().normalize()); // /
        System.out.println(Path.of("/zoo/../home").normalize().getParent()); // /

        Path path = Paths.get("/land/hippo/harry.happy");
        System.out.println("The Path Name is: " + path);
        for (int i = 0; i < path.getNameCount(); i++)
            System.out.println(" Element " + i + " is: " + path.getName(i));

        p = Paths.get("/mammal/omnivore/raccoon.image");
        System.out.println("Path is: " + p);
        for (int i = 0; i < p.getNameCount(); i++) {
            System.out.println(" Element " + i + " is: " + p.getName(i));
        }
        System.out.println();
        System.out.println("subpath(0,3): " + p.subpath(0, 3));
        System.out.println("subpath(1,2): " + p.subpath(1, 2));
        System.out.println("subpath(1,3): " + p.subpath(1, 3));
        //var q = p.subpath(0, 4); // IllegalArgumentException
        //var x = p.subpath(1, 1); // IllegalArgumentException
    }
}

class PathElementsTest {
    public static void printPathInformation(Path path) {
        System.out.println("Filename is: " + path.getFileName());
        System.out.println(" Root is: " + path.getRoot());
        Path currentParent = path;
        while((currentParent = currentParent.getParent()) != null)
            System.out.println(" Current parent is: " + currentParent);
        System.out.println();
    }

    public static void main(String[] args) {
        printPathInformation(Path.of("zoo"));
        printPathInformation(Path.of("/zoo/armadillo/shells.txt"));
        printPathInformation(Path.of("./armadillo/../shells.txt"));
        System.out.println(Path.of("./armadillo/../shells.txt").normalize());

        Path path1 = Path.of("/cats/../panther");
        Path path2 = Path.of("food");
        System.out.println(path1.resolve(path2));
        Path path3 = Path.of("/turkey/food/test/fifty");
        System.out.println(path3.resolve("/tiger/cage"));

        System.out.println();
        var path4 = Path.of("fish.txt"); //the file itself counts as one level
        var path5 = Path.of("friendly/birds.txt");
        System.out.println(path4.relativize(path5)); // ../friendly/birds.txt
        System.out.println(path5.relativize(path4)); // ../../fish.txt
        System.out.println();
        Path path6 = Paths.get("C:\\habitat");
        Path path7 = Paths.get("C:\\sanctuary\\raven\\poe.txt");
        System.out.println(path6.relativize(path7));
        System.out.println(path7.relativize(path6));

        System.out.println();
        Path path8 = Paths.get("/primate/chimpanzee");
        Path path9 = Paths.get("bananas.txt");
        //path8.relativize(path9); // IllegalArgumentException 'other' is different type of Path

        System.out.println();
        Path path10 = Paths.get("C:\\primate\\chimpanzee");
        Path path11 = Paths.get("D:\\storage\\bananas.txt");
        //path10.relativize(path11); // IllegalArgumentException

        System.out.println();
        try {
            System.out.println(Paths.get("data/stripes2.txt").toRealPath()); //Users/admin/IdeaProjects/chapter-14-io/chapter-14-io/data/stripes2.txt
            //System.out.println(Paths.get("/zebra/food.txt").toRealPath());
            //System.out.println(Paths.get(".././food.txt").toRealPath());
            System.out.println(Paths.get(".").toRealPath()); // /Users/admin/IdeaProjects/chapter-14-io/chapter-14-io
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        System.out.println(Paths.get(".").getNameCount());

        System.out.println();
        try {
            //Files.createDirectory(Path.of("bison/field")); //NoSuchFileException: /bison/field
            //Files.createDirectories(Path.of("/bison/field/pasture/green")); //FileSystemException: /bison: Read-only file system
            System.out.println(Files.createDirectories(Path.of("bison/field/pasture/green")));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        //try {
            //System.out.println(Files.copy(Paths.get("data/stripes2.txt"), Paths.get("data/stripes.txt"))); //FileAlreadyExistsException
            //Files.copy(Paths.get("/turtle"), Paths.get("/turtleCopy")); //NoSuchFileException
        //} catch (IOException e) {
          //  throw new RuntimeException(e);
        //}
        //copyPath(Path.of("data/data"), Paths.get("data/data1"));

        try (var is = new FileInputStream("data/data/test1.txt")) { // Write I/O stream data to a file
            //Files.copy(is, Paths.get("data/data/test2.txt"), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        //try {
            //Files.copy(Paths.get("data/data/test2.txt"), System.out);
        //} catch (IOException e) {
            //throw new RuntimeException(e);
        //}

        try {
           // Files.move(Path.of("data/data"), Path.of("data/zoo-new"), StandardCopyOption.REPLACE_EXISTING);
            //Files.delete(Paths.get("/vulture/feathers.txt"));
            System.out.println(Files.deleteIfExists(Paths.get("/pigeon")));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        //try {
            //System.out.println(Files.isSameFile( Path.of("/animals/cobra"), Path.of("/animals/snake"))); //NoSuchFileException
            //System.out.println(Files.isSameFile( Path.of("/animals/monkey/ears.png"), Path.of("/animals/wolf/ears.png"))); //NoSuchFileException
        //} catch (IOException e) {
          //  throw new RuntimeException(e);
        //}

        //try {
            //System.out.println(Files.mismatch( Path.of("/animals/monkey.txt"), Path.of("/animals/wolf.txt")));
        //} catch (IOException e) {
            //throw new RuntimeException(e);
        //}

    }

    public static void copyPath(Path source, Path target) { try {
        Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING);
        if(Files.isDirectory(source))
            try (Stream<Path> s = Files.list(source)) {
                s.forEach(p -> copyPath(p,
                    target.resolve(p.getFileName()))); }
    } catch(IOException e) { // Handle exception
        throw new RuntimeException(e);
    } }
}

class Attributes {
    public static void main(String[] args) {

        System.out.println();

        File file = new File("io/links/LION");
        System.out.println("exists: " + file.exists());
        System.out.println("isDirectory: " + file.isDirectory());
        System.out.println("isFile: " + file.isFile());
        System.out.println("canRead: " + file.canRead());
        System.out.println("canWrite: " + file.canWrite());
        System.out.println("isHidden: " + file.isHidden());
        System.out.println("canExecute: " + file.canExecute());
        System.out.print(Files.isDirectory(file.toPath()));
        System.out.print(Files.isSymbolicLink(file.toPath()));
        System.out.print(Files.isRegularFile(Paths.get(file.toString()))); //falsetruefalse
    }
}

/**
 * Read-only
 */
class AttributeView {
    public static void main(String[] args) throws IOException {
        var path = Paths.get("io/links/alias");
        BasicFileAttributes data = Files.readAttributes(path, BasicFileAttributes.class);
        System.out.println("Is a directory? " + data.isDirectory());
        System.out.println("Is a regular file? " + data.isRegularFile());
        System.out.println("Is a symbolic link? " + data.isSymbolicLink());
        System.out.println("Size (in bytes): " + data.size());
        System.out.println("Last modified: " + data.lastModifiedTime());
        System.out.println();
        PosixFileAttributes dataMac = Files.readAttributes(path, PosixFileAttributes.class);
        System.out.println("Is a directory? " + dataMac.isDirectory());
        System.out.println("Is a regular file? " + dataMac.isRegularFile());
        System.out.println("Is a symbolic link? " + dataMac.isSymbolicLink());
        System.out.println("Size (in bytes): " + dataMac.size());
        System.out.println("Last modified: " + dataMac.lastModifiedTime());
    }
}

/**
 * Updatable
 */
class UpdatableAttributes {
    public static void main(String[] args) throws IOException {
        var path = Paths.get("io/links/alias");
        // Read file attributes
        BasicFileAttributeView view = Files.getFileAttributeView(path, BasicFileAttributeView.class);
        BasicFileAttributes attributes = view.readAttributes();
        // Modify file last modified time
        FileTime lastModifiedTime = FileTime.fromMillis(attributes.lastModifiedTime().toMillis() + 10_000);
        view.setTimes(lastModifiedTime, null, null);
    }
}