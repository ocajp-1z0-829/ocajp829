import java.io.*;
import java.util.*;

public class UserInteractions {
    public static void main(String[] args) throws IOException {
        try (var in = new FileInputStream("zoo.txt")) {
            System.out.println("Found file!");
        } catch (FileNotFoundException e) {
            System.err.println("File not found!");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        var reader = new BufferedReader(new InputStreamReader(System.in));
        String userInput = reader.readLine();
        System.out.println("You entered: " + userInput);

        Scanner scanner = new Scanner(System.in);
        System.out.println("You entered: " + scanner.next());
    }
}

class Sout {
    public static void main(String[] args) throws IOException {
        try (var out = System.out) {}
        System.out.println("Hello");

        try (var err = System.err) {}
        System.err.println("Hello");

        var reader = new BufferedReader(new InputStreamReader(System.in));
        try (reader) {}
        String data = reader.readLine(); // IOException
        System.out.println(data);
    }
}

class ConsoleTest {
    public static void main(String[] args) {
        //Console c = new Console(); // DOES NOT COMPILE

        Console console = System.console();
        if (console != null) {
            String userInput = console.readLine();
            console.writer().println("You entered: " + userInput);
        } else {
            System.err.println("Console not available");
        }
    }
}

class ConsoleFormatted {
    public static void main(String[] args) {
        Console console = System.console();
        if (console == null) {
            throw new RuntimeException("Console not available");
        } else {
            //try (PrintWriter pr = console.writer()) {
                //pr.println("Welcome to Our Zoo!");
            //}
            console.writer().println("Welcome to Our Zoo!");
            console.format("It has %d animals and employs %d people", 391, 25);
            console.writer().println();
            console.printf("The zoo spans %5.1f acres", 128.91);
            console.writer().println();
            console.writer().format(new Locale("fr", "CA"), "Hello World");
        }
    }
}

class ConsoleReadPasswords {
    public static void main(String[] args) {
        Console console = System.console();
        if (console == null) {
            throw new RuntimeException("Console not available");
        } else {
            String name = console.readLine("Please enter your name: ");
            console.writer().format("Hi %s", name);
            console.writer().println();
            console.format("What is your address? ");
            String address = console.readLine();
            char[] password = console.readPassword("Enter a password " + "between %d and %d characters: ", 5, 10);
            char[] verify = console.readPassword("Enter the password again: ");
            console.printf("Passwords " + (Arrays.equals(password, verify) ? "match" : "do not match"));
        }
    }
}