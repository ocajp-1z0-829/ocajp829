import model.BabyChimpanzee;
import model.Chimpanzee;
import model.Gorilla;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

class DeserializationBabyChimpanzeeTestImpl {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        SerializationChimpanzeeTest serializationTest = new SerializationChimpanzeeTest();

        var chimpanzees = new ArrayList<Chimpanzee>();
        chimpanzees.add(new BabyChimpanzee());
        chimpanzees.add(new BabyChimpanzee());
        File dataFile = new File("babychimpanzee.data");
        serializationTest.saveToFile(chimpanzees, dataFile);
        var chimpanzeesFromDisk = serializationTest.readFromFile(dataFile);
        System.out.println(chimpanzeesFromDisk);
    }
}

class DeserializationChimpanzeeTestImpl {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        SerializationChimpanzeeTest serializationTest = new SerializationChimpanzeeTest();

        var chimpanzees = new ArrayList<Chimpanzee>();
        chimpanzees.add(new Chimpanzee("Ham", 2, 'A'));
        chimpanzees.add(new Chimpanzee("Enos", 4, 'B'));
        File dataFile = new File("chimpanzee.data");
        serializationTest.saveToFile(chimpanzees, dataFile);
        var chimpanzeesFromDisk = serializationTest.readFromFile(dataFile);
        System.out.println(chimpanzeesFromDisk);
    }
}

class SerializationGorillaTestImpl {
    public static void main(String[] args) {
        SerializationGorillaTest serializationTest = new SerializationGorillaTest();
        var gorillas = new ArrayList<Gorilla>();
        gorillas.add(new Gorilla("Grodd", 5, false));
        gorillas.add(new Gorilla("Ishmael", 8, true));
        File dataFile = new File("gorilla.data");

        serializationTest.saveToFile(gorillas, dataFile);
        List<Gorilla> gorillasFromDisk = null;
        try {
            gorillasFromDisk = serializationTest.readFromFile(dataFile);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        System.out.print(gorillasFromDisk);
    }
}

public class SerializationTest {
}

class SerializationChimpanzeeTest {
    static void saveToFile(List<Chimpanzee> chimpanzees, File dataFile) {
        try (var out = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(dataFile)))) {
            for (Chimpanzee chimpanzee : chimpanzees)
                out.writeObject(chimpanzee);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    static List<Chimpanzee> readFromFile(File dataFile) throws IOException, ClassNotFoundException {
        var chimpanzees = new ArrayList<Chimpanzee>();
        try (var in = new ObjectInputStream(new BufferedInputStream(new FileInputStream(dataFile)))) {
            while (true) {
                var object = in.readObject();
                if (object instanceof Chimpanzee c)
                    chimpanzees.add(c);
            }
        } catch (EOFException e) { // File end reached
        }
        return chimpanzees;
    }
}

class SerializationGorillaTest {
    static void saveToFile(List<Gorilla> gorillas, File dataFile) {
        try (var out = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(dataFile)))) {
            for (Gorilla gorilla : gorillas)
                out.writeObject(gorilla);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    static List<Gorilla> readFromFile(File dataFile) throws IOException, ClassNotFoundException {
        var gorillas = new ArrayList<Gorilla>();
        try (var in = new ObjectInputStream(new BufferedInputStream(new FileInputStream(dataFile)))) {
            while (true) {
                var object = in.readObject();
                if (object instanceof Gorilla g)
                    gorillas.add(g);
            }
        } catch (EOFException e) { // File end reached
        }
        return gorillas;
    }
}