package com.ocajp;

public class Main {

    public static void main(String[] args) {
        System.out.print(null instanceof Integer); // false
        Object noObjectHere = null;
        System.out.print(noObjectHere instanceof String); // false

        //System.out.print(null instanceof null); // DOES NOT COMPILE Not a statement

      //int note = 1 * 2 + (long)3; // Required type int Provided long
      //short melody = (byte)(double)(note *= 2);
      //double song = melody;
      //float symphony = (float)((song == 1_000f) ? song * 2L : song);

      int ticketsTaken = 1;
      int ticketsSold = 3;
      ticketsSold += 1 + ticketsTaken++;
      ticketsTaken *= 2;
      ticketsSold += (long)1;
	// write your code here
      System.out.println();
      int pig = (short)4;
      pig = pig++;
      long goat = (int)2;
      goat -= 1.0;
      System.out.print(pig + " - " + goat);
      System.out.println();
      Object boolObj = true;
      boolean boolPrim = (boolean) boolObj;
      System.out.println(boolPrim);

    }
    //static int _() { //As of Java 9, '_' is a keyword, and may not be used as an identifier
      //  return 1;
    //}



}

class CandyCounter {
 static long addCandy(double fruit, float vegetables) {
        //return (int)fruit+vegetables; // NOT COMPILE
        return (int)fruit+(int)vegetables;
        }

     public static void main(String[] args) {
        System.out.print(addCandy(1.4, 2.4f) + ", ");
        System.out.print(addCandy(1.9, (float)4) + ", ");
        System.out.print(addCandy((long)(int)(short)2, (float)4)); } }