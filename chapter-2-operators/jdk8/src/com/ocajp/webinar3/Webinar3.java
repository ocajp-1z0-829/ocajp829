package com.ocajp.webinar3;

public class Webinar3 {
  public static void main(String[] args) {
    int value = 3; // Stored as 0011
    int complement = ~value; // Stored as 1100
    System.out.println(value); // 3
    System.out.println(complement); // -4
    System.out.println(-1*value - 1); // -4
    System.out.println(-1*complement - 1); // 3

    double egg = 2.0 / 9;
    System.out.println(egg);
    //float egg = 2.0 / 9; // DOES NOT COMPILE Provided double
    //int tadpole = (int)5 * 2L; // DOES NOT COMPILE Provided long
    //short frog = 3 - 2.0; // DOES NOT COMPILE Provided: double

    //int fish = 1.0; // DOES NOT COMPILE
    //short bird = 1921222; // DOES NOT COMPILE Provided: int
    //int mammal = 9f; // DOES NOT COMPILE
    //long reptile = 192_301_398_193_810_323; // DOES NOT COMPILE Integer number too large


    String strVal = "" + 2016;//2016
    System.out.println(strVal);
    String theName = " Uranium";// Uranium
    System.out.println(theName);
    theName = " Pure" + theName;// Pure Uranium
    System.out.println(theName);
    String trademark1 = 100 + "%" + theName;//100% Pure Uranium
    System.out.println(trademark1);
    String trademark2 = 100 + '%' + theName;//137 Pure Uranium
    System.out.println(trademark2);
    System.out.println("2 * 2 = " + 2 * 2);//2 * 2 = 4

    String concat = null + "-a-" + true;
    System.out.println(concat);

    String str = "";
    //str = null + 'a'; //Operator '+' cannot be applied to 'null', 'char так как неясно какой тип
    str = (String) null + 'a';
    str += 'a';
    //str += null + 'a'; //Operator '+' cannot be applied to 'null', 'char' так как неясно какой тип
    str += (String) null + 'a';
    str = str + null + 'a';

    Object sobj = "";
    String strs = "";
    //strs = sobj + 'a';
    strs = (String) sobj + 'a';

    System.out.println();
    boolean bear = false;
    boolean polar = (bear = true);
    System.out.println(polar); //true
    System.out.println(bear); //true

    System.out.println();
    int a, b, c;
    a = b = c = 5;
    //boolean illegal1 = a == b == c; //illegal Operator '==' cannot be applied to 'boolean', 'int'
    //boolean illegal2 = (a == b) == c; //Operator '==' cannot be applied to 'boolean', 'int'

    boolean valid2 = a == b && b == c;
    boolean valid3 = a == b == true;
    System.out.println(valid2 + " " + valid3);

    System.out.println();
    Integer iRef = 10;
    boolean b1 = iRef == null;
    boolean b2 = iRef == 10;
    boolean b3 = null == iRef;
    //boolean b4 = null == 10; //Operator '==' cannot be applied to 'null', 'int'
    System.out.println(b1 + " " + b2 + " " + b3);

    System.out.println();
    System.out.println(10 + 5 == 4 + 11);
    //aSystem.out.println(10 + (5 == 4) + ""); //10 + false + 11
    System.out.println("" + (5 <= 4) + "");
    System.out.println("" + 10 + 5 == 4 + 11 + ""); //105 == 15
    //System.out.println("" + 10 + 5 == 4 + 11); //Operator '==' cannot be applied to 'java.lang.String', 'int'

    System.out.println(null instanceof Object);
    System.out.println(null instanceof String);
    System.out.println(null instanceof Number);
    Integer t = 4;
    System.out.println(t instanceof Number);//Inconvertible types; cannot cast 'java.lang.Integer' to 'java.lang.String'
    //System.out.println(t instanceof String);//Inconvertible types; cannot cast 'java.lang.Integer' to 'java.lang
    // .String'
    //System.out.println(null instanceof null);

    System.out.println(5 * (1 % 2));
    System.out.println(1 % 2);
    System.out.println((2 * 4) % 3);

    int ticketsTaken = 1;
    int ticketsSold = 3;
    ticketsSold += 1 + ticketsTaken++;
    ticketsTaken *= 2;
    ticketsSold += (long)1;
    // write your code here

    int pig = (short)4;
    pig = pig++;
    long goat = (int)2;
    goat -= 1.0;
    System.out.print(pig + " - " + goat);

  }
}

class Exception {
  public static void main(String[] args) {
    //throw new java.lang.Exception();
    _();
    if (5 >100)
      throw new RuntimeException();
    else {
      System.out.println("Ogo it's wworking");
    }
  }

  static int _() throws RuntimeException {
    return 1;
  }
}