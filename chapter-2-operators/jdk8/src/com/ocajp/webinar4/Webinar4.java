package com.ocajp.webinar4;

public class Webinar4 {
  public static void main(String[] args) {
    int i = 29;
    int j = 28;
    //int minValue = 1 < j ? 1 : Double.MIN_VALUE; //Provided: double
    double minValue = 1 < j ? 1 : Double.MIN_VALUE;
    System.out.println(minValue);

    if (1>0);
    //while (true);
  }
}

class A {
  static int test(int a) {
    return a*a;
  }

  public static void main(String[] args) {
    int res = 10, a = 0;
    if ((res > 10 ? test(a) : --res) < 10)
      System.out.println(res);//9
    for ( ; Math.random()<.5? true : false ;) { //пока будет true будет печатать
      System.out.println("for");
    }

    Integer i = null;
    Integer integer1 = false ? 1 : i; //NPE потому что происходит автобоксинг int
    System.out.println(false ? 1 : i);

    Integer integer3 = false ? 1 : false ? 2 : null; //NPE потому что происходит автобоксинг int
    System.out.println(false ? 1 : false ? 2 : null);

    System.out.println();
  }
}

class Test {
  public static void main(String[] args) {
    int pig = (short) 4;
    pig = pig++;
    System.out.println(pig);
    long goat = (int) 2;
    //goat = 1.0; //Required type: long, Provided: double
    goat -= 1.0;
    System.out.println(goat);

    int x = 10;
    int y = x--; //10
    int z = --y; //9
    int a = x++; //9
    //int b = x < y ? x < z ? x : y < z ? y : z;
    //System.out.println(b);

    boolean a1 = false;
    int n = 0;
    boolean b1 = a1 = true ? n > 2 ? true : false : false;
    System.out.println(b1);

    int i, j = 0;
    //(i < j) ? i : j;

    Boolean buy = new Boolean(true);
    Boolean sell = new Boolean(true);

    System.out.print(buy == sell);

    boolean buyPrim = buy.booleanValue();
    System.out.print(!buyPrim);
    System.out.print(buy && sell);
  }
}
