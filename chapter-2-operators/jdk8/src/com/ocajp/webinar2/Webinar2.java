package com.ocajp.webinar2;

public class Webinar2 {
  public static void main(String[] args) {
    System.out.println(-Integer.MIN_VALUE);
    System.out.println(Math.abs(Integer.MIN_VALUE));
    //System.out.println(Math.negateExact(Integer.MIN_VALUE)); //java.lang.ArithmeticException: integer overflow

    short h = 40;
    //h = h + 2; //Provided: int

    h = (short) (h + 2);

    //h = (short) h + (short) 2; //Provided: int

    //h = (short) h + 2; //Provided: int

    final short s = 10;
    Byte aByte = s; //final кастится без ошибок если размер соответствует
    //Integer aInteger = s; //проблема с кастом в Integer, с int отработает

    Integer i = null; //Unboxing of 'i' may produce 'NullPointerException'
    //int j = i; //NPE at runtime

    Integer integer = 200;
    integer += aByte;
    //aByte = (Byte) (aByte + integer); //Inconvertible types; cannot cast 'int' to 'java.lang.Byte'
    aByte = (byte) (aByte + integer);
    //aByte += integer; //cannot cast int to Byte
    byte b = 0;
    b += integer; //(byte) (b + integer)
    System.out.println(b);

    short[] shorts = new short[] {2015, 2016, 2017};
    int x = 2;
    shorts[x] += 1;
    shorts[x] = (short) (shorts[x] + 1); //whithout cast it is not working



  }
}

class MemorySaver {
  public static void main(String[] args) {
    int a = 123;
    int b = 321;
    System.out.println("a=" + a + ", b=" + b);

    a = a + b;
    b = a - b;
    a = a - b;
    System.out.println("a=" + a + ", b=" + b);

    a = Integer.MAX_VALUE;
    b = Integer.MIN_VALUE;
    a = a + b; //2147483647 + (-2147483648) = -1
    b = a - b; //-1 - (-2147483648)
    a = a - b;
    System.out.println("a=" + a + ", b=" + b);
  }
}

class CandyCounter {
  static long addCandy(double fruit, float vegetables) {
    //return (int)fruit+vegetables; //первое кастует в int, а второе нет, поэтому вернется float
    return (int) fruit + (int) vegetables;
  }

  public static void main(String[] args) {
    System.out.print(addCandy(1.4, 2.4f) + "-");
    System.out.print(addCandy(1.9, (float) 4) + "-");
    System.out.print(addCandy((long) (int) (short) 2, (float) 4));
  }
}

class Boo {
  public static void main(String[] args) {
    int a, b = 0;
    boolean boo;
    a = (boo = true) ? b = 10 : 11;
    System.out.println(a + " " + boo + " " + b);
  }
}