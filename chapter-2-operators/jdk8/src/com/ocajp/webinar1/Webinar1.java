package com.ocajp.webinar1;

/**
 * int это самый младший тип данных для операнда, в результате все что ниже получают возвышение.
 * C числами унирные операторы не работают. на final переменных тоже не работает инкремент
 *
 */

public class Webinar1 {

  public static void main(String[] args) {
    int cookies = 4;

    /**
     * first decrement cookies to 3,
     * then multiply the resulting value by 2,
     * and finally add 3
     */
    double reward = 3 + 2 * --cookies;
    System.out.print("Zoo animal receives: " + reward + " reward points");

    System.out.println();
    int b = 10;
    System.out.println((b=3) + b); //6
    System.out.println(b); //3

    System.out.println();
    System.out.println(b++); //3
    System.out.println(b); //4
    System.out.println(--b); //3

    System.out.println();
    System.out.println(+5); //5

    System.out.println();
    System.out.println(-b); // -3
    System.out.println(-(-b)); // 3

    byte value = + - + - - - -10; //(-(-10)) is 10
    //byte value2 = + value; //Provided: int
    System.out.println(value); // 10


    //int a2 = (- 1 -); //операнда справа нет, оператор есть
    int b2 = (+ + 1);
    int c2 = (+-+-+-1);
    //int d2 = (--1); //литерал единица не сработает
    //int e2 = (1 * * 1); //операнд отсутствует
    int f2 = (- -1);
    System.out.println(b2 + " " + c2 + " " + f2);

    System.out.println();
    int i = 2+-+-+-10;
    //int i2 = 2++-+-+-10; //компилятор думает что 2++ унарный оператор
    //int i3 = 2+--+-+-10; // 2+(--+-+-10) итого 2++ опять инкремент
    int i4 = (- -1-3 * 10 / 5-1); //1 - 3 * 10 / 5 - 1 = 1 - 6 - 1 = -6
    System.out.println(i4); //-6

    final int x = 5;
    //int y = x++; //cannot assign a value to final variable x

    //int z = (++(++i)); //ассоциативности нет

    System.out.println();
    int a = 0;
    System.out.println(a++ - a--); //0 - -1 = -1
    System.out.println(a); //0 так как сначала вычислили потом вернули обратно

    System.out.println();
    int a2 = 10;
    a2 = a2++ + a2 + a2-- - a2-- + ++a2; //10 + 11 + 11 - 10 + 10 = 32
    System.out.println(a2);

    System.out.println();
    int ik = 10;
    int k = ++ik + -ik; //((++ik) + (-ik))
    System.out.println(k);

    byte c = 3;
    c = ++ c; //increment не участвует в возвышении типов до int
    //c = + + c; //Provided: int
    System.out.println(c); //4


    double xx = 4.5;
    xx = xx + ++xx;
    System.out.println(xx); //10.0

    System.out.println();
    int i1 = 0;
    i1 = i1++;
    System.out.println(i1); //0 потому что инкремент был постфикс и присвоился к i1, ++i1 вернуло бы 1
    int i2 = 0;
    i2++;
    System.out.println(i2);

    System.out.println();
    int[] arr = new int[1];
    int index = 2;
    arr[--index] = 1/--index; //arr[1] = 1/0; //ArithmeticException: / by zero
  }
}
