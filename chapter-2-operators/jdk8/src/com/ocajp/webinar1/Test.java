package com.ocajp.webinar1;

public class Test {
  public static void main(String[] args) {
    int[] array = {4, 8, 16};
    int i = 1;
    array[++i] = -i;
    System.out.println(array[0] + array[1] + array[2]);

  }
}

class CompareValues {
  public static void main(String[] args) {
    int x = 0;
    while (x++ < 10) {}
    String message = x > 10 ? "Greater than" : "false";
    System.out.println(message+","+x); //Greater than,11

    int a = 0;
    System.out.println(a + a++ - a-- + " " + a);
    System.out.println(a);
  }
}
